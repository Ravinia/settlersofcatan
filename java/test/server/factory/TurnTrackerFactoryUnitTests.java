package server.factory;

import org.junit.Test;

import server.courier.serializer.TurnTrackerInfo;
import server.exceptions.PlayerNotInGameException;
import server.factories.TurnTrackerFactory;
import server.model.game.state.TurnOrder;
import shared.definitions.Phase;

/**
 * @author Lawrence
 */
public class TurnTrackerFactoryUnitTests extends AdvancedFactoryHelper
{
    @Test
    public void test_correctStartPlayer() throws PlayerNotInGameException
    {
        TurnTrackerInfo info = TurnTrackerFactory.convertTurnTracker(model);
        assert info.getCurrentPlayerIndex() == model.getIndexOfPlayer(RED_PLAYER);
        assert info.getCurrentPlayerIndex() != model.getIndexOfPlayer(BLUE_PLAYER);
    }

    @Test
    public void test_startsInPhase1() throws PlayerNotInGameException
    {
        TurnTrackerInfo info = TurnTrackerFactory.convertTurnTracker(model);
        assert info.getCurrentPhase().equals(Phase.ROUND1);
    }

    @Test
    public void test_changedPhase() throws PlayerNotInGameException
    {
        ((TurnOrder)game.getTurnOrder()).setCurrentPhase(Phase.PLAYING);

        TurnTrackerInfo info = TurnTrackerFactory.convertTurnTracker(model);
        assert info.getCurrentPhase().equals(Phase.PLAYING);
    }

    @Test
    public void test_changeCurrentPlayer() throws PlayerNotInGameException
    {
        ((TurnOrder)game.getTurnOrder()).setCurrentPlayerIndex(1);

        TurnTrackerInfo info = TurnTrackerFactory.convertTurnTracker(model);
        assert info.getCurrentPlayerIndex() == model.getIndexOfPlayer(BLUE_PLAYER);
        assert info.getCurrentPlayerIndex() != model.getIndexOfPlayer(RED_PLAYER);
    }
}
