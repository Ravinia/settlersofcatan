package server.factory;

import org.junit.Test;
import server.courier.serializer.ChatInfo;
import server.courier.serializer.LogInfo;
import server.factories.ChatLogFactory;
import server.model.game.chatlog.ChatLog;
import server.model.game.chatlog.Message;
import server.model.game.chatlog.MessageImp;

/**
 * @author Lawrence
 */
public class ChatLogFactoryTests extends FactoryHelper
{
    private static final Message MESSAGE_01 = new MessageImp("Red","hello");
    private static final Message MESSAGE_02 = new MessageImp("Blue","hello");
    private static final Message MESSAGE_03 = new MessageImp("Orange","hello");
    private static final Message MESSAGE_04 = new MessageImp("White","hello");

    //----------------------Chat Tests---------------------------
    @Test
    public void test_chats_sameOrder()
    {
        ChatLog chatLog = game.getChatLog();
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_02);
        chatLog.addChat(MESSAGE_03);
        chatLog.addChat(MESSAGE_04);

        ChatInfo chats = ChatLogFactory.convertChat(game);

        assert chats.getMessage(0).equals(MESSAGE_01);
        assert chats.getMessage(1).equals(MESSAGE_02);
        assert chats.getMessage(2).equals(MESSAGE_03);
        assert chats.getMessage(3).equals(MESSAGE_04);
    }

    @Test
    public void test_chats_iteratorWorks()
    {
        ChatLog chatLog = game.getChatLog();
        addLotsOfChats(chatLog);

        ChatInfo chats = ChatLogFactory.convertChat(game);
        for (Message message : chats)
        {
            assert message.equals(MESSAGE_01);
        }
    }

    @Test
    public void test_chats_initiallyEmpty()
    {
        ChatInfo chats = ChatLogFactory.convertChat(game);
        assert chats.size() == 0;
    }

    @Test
    public void test_chats_notLogs()
    {
        ChatLog chatLog = game.getChatLog();
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_02);
        chatLog.addLog(MESSAGE_03);
        chatLog.addLog(MESSAGE_04);

        ChatInfo chats = ChatLogFactory.convertChat(game);
        assert chats.size() == 0;
    }

    //----------------------Log Tests---------------------------
    @Test
    public void test_logs_sameOrder()
    {
        ChatLog chatLog = game.getChatLog();
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_02);
        chatLog.addLog(MESSAGE_03);
        chatLog.addLog(MESSAGE_04);

        LogInfo logs = ChatLogFactory.convertLog(game);

        assert logs.getMessage(0).equals(MESSAGE_01);
        assert logs.getMessage(1).equals(MESSAGE_02);
        assert logs.getMessage(2).equals(MESSAGE_03);
        assert logs.getMessage(3).equals(MESSAGE_04);
    }

    @Test
    public void test_logs_iteratorWorks()
    {
        ChatLog chatLog = game.getChatLog();
        addLotsOfLogs(chatLog);

        LogInfo logs = ChatLogFactory.convertLog(game);
        for (Message message : logs)
        {
            assert message.equals(MESSAGE_01);
        }
    }

    @Test
    public void test_logs_notChats()
    {
        ChatLog chatLog = game.getChatLog();
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_02);
        chatLog.addChat(MESSAGE_03);
        chatLog.addChat(MESSAGE_04);

        LogInfo logs = ChatLogFactory.convertLog(game);
        assert logs.size() == 0;
    }

    //Helper Methods
    private void addLotsOfChats(ChatLog chatLog)
    {
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
        chatLog.addChat(MESSAGE_01);
    }

    private void addLotsOfLogs(ChatLog chatLog)
    {
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
        chatLog.addLog(MESSAGE_01);
    }
}
