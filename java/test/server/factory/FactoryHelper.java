package server.factory;

import org.junit.Before;

import server.exceptions.CatanServerException;
import server.model.game.PlayableGame;
import server.model.game.PlayableGameImp;
import server.model.player.Player;
import server.model.player.ServerPlayer;
import shared.definitions.CatanColor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract class that should be inherited by the unit tests in this folder
 * @author Lawrence
 */
public abstract class FactoryHelper
{
    protected static final String RED_PLAYER_NAME = "Red";
    protected static final String BLUE_PLAYER_NAME = "Blue";
    protected static final String ORANGE_PLAYER_NAME = "Orange";
    protected static final String WHITE_PLAYER_NAME = "White";

    protected PlayableGame game;
    protected static final Player RED_PLAYER = new ServerPlayer(1,RED_PLAYER_NAME);
    protected static final Player BLUE_PLAYER = new ServerPlayer(2,BLUE_PLAYER_NAME);
    protected static final Player ORANGE_PLAYER = new ServerPlayer(3,ORANGE_PLAYER_NAME);
    protected static final Player WHITE_PLAYER = new ServerPlayer(4,WHITE_PLAYER_NAME);

    @Before
    public void initialize() throws CatanServerException
    {
        game = new PlayableGameImp(1,"fun",getPlayers(), getColorMap());
    }

    //Helper Methods
    protected static List<Player> getPlayers()
    {
        List<Player> result = new ArrayList<>();
        result.add(RED_PLAYER);
        result.add(BLUE_PLAYER);
        result.add(ORANGE_PLAYER);
        result.add(WHITE_PLAYER);
        return result;
    }

    private static Map<Player, CatanColor> getColorMap()
    {
        Map<Player,CatanColor> colors = new HashMap<>();
        colors.put(RED_PLAYER, CatanColor.RED);
        colors.put(BLUE_PLAYER, CatanColor.BLUE);
        colors.put(ORANGE_PLAYER, CatanColor.ORANGE);
        colors.put(WHITE_PLAYER, CatanColor.WHITE);
        return colors;
    }
}
