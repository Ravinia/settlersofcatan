package server.factory;

import org.junit.Test;
import server.courier.serializer.PlayerInfo;
import server.factories.PlayerFactory;
import server.model.game.resources.ResourceHandImp;
import shared.definitions.CatanColor;
import static server.model.game.resources.BuildPool.*;

/**
 * @author Lawrence
 */
public class PlayerFactoryTests extends AdvancedFactoryHelper
{
    @Test
    public void test_playerWithDefaultValues() throws Exception
    {
        PlayerInfo player = PlayerFactory.convertPlayer(model.getPlayers().get(0), model);
        assert player.hasDiscarded();
        assert !player.hasPlayedDevCardThisTurn();
        assert player.getId() == 1;
        assert player.getIndex() == 0;
        assert player.getName().equals("Red");

        assert player.getNumRemainingCities() == DEFAULT_CITY_COUNT;
        assert player.getNumRemainingSettlements() == DEFAULT_SETTLEMENT_COUNT;
        assert player.getNumRemainingRoads() == DEFAULT_ROAD_COUNT;

        assert player.getNumMonumentsPlayed() == 0;
        assert player.getNumSoldiersPlayed() == 0;
        assert player.getVictoryPoints() == 0;

        assert player.getResourceHand().equals(new ResourceHandImp());

        assert player.getColor() == CatanColor.RED;
    }
}
