package server.factory;

import org.junit.Test;
import server.courier.serializer.BankInfo;
import server.factories.BankFactory;
import server.model.game.bank.Bank;
import server.model.game.bank.BankImp;

/**
 * @author Lawrence
 */
public class BankFactoryUnitTests extends FactoryHelper
{
    @Test
    public void test_InitialValue()
    {
        Bank bank = game.getBank();
        BankInfo result = BankFactory.convertBank(game);
        assert result.getBrick() == 19;
        assert result.getOre() == 19;
        assert result.getSheep() == 19;
        assert result.getWheat() == 19;
        assert result.getWood() == 19;
    }

    @Test
    public void test_AlteredValues()
    {
        BankImp bank = (BankImp)game.getBank();
        bank.setBrick(9);
        bank.setOre(10);
        bank.setSheep(0);
        bank.setWheat(11);
        bank.setWood(19);

        BankInfo result = BankFactory.convertBank(game);
        assert result.getBrick() == 9;
        assert result.getOre() == 10;
        assert result.getSheep() == 0;
        assert result.getWheat() == 11;
        assert result.getWood() == 19;
    }


}
