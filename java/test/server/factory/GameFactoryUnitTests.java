package server.factory;

import org.junit.Test;
import server.courier.serializer.GameInfo;
import server.courier.serializer.PregamePlayerInfo;
import server.exceptions.UninitializedDataException;
import server.factories.GameFactory;
import server.model.game.PregameGame;
import server.model.game.PregameGameImp;
import shared.definitions.CatanColor;

import java.util.List;

/**
 * @author Lawrence
 */
public class GameFactoryUnitTests extends AdvancedFactoryHelper
{
    private static final int EMPTY_GAME_ID = 2;
    private static final int FULL_GAME_ID = 3;

    private static final String EMPTY_GAME_NAME = "empty game";
    private static final String FULL_GAME_NAME = "full game";

    PregameGame emptyGame = new PregameGameImp(EMPTY_GAME_ID,EMPTY_GAME_NAME);

    @Test
    public void test_PregameConvert_empty() throws UninitializedDataException
    {
        GameInfo info = GameFactory.convertPregameGame(emptyGame);
        assert info.getGameID() == EMPTY_GAME_ID;
        assert info.getGameName().equals(EMPTY_GAME_NAME);
        assert info.getPlayers().size() == 0;
    }

    @Test
    public void test_PregameConvert_full() throws UninitializedDataException
    {
        GameInfo info = GameFactory.convertPregameGame(getFullPregameGame());
        assert info.getGameID() == FULL_GAME_ID;
        assert info.getGameName().equals(FULL_GAME_NAME);
        assert info.getPlayers().size() == 4;

        List<PregamePlayerInfo> players = info.getPlayers();
        assert players.get(0).getColor().equals(CatanColor.RED);
        assert players.get(1).getColor().equals(CatanColor.BLUE);
        assert players.get(2).getColor().equals(CatanColor.ORANGE);
        assert players.get(3).getColor().equals(CatanColor.WHITE);
    }

    @Test
    public void test_PlayableGame() throws UninitializedDataException
    {
        GameInfo info = GameFactory.convertPlayableGame(game);
        assert info.getGameID() == DEFAULT_GAME_ID;
        assert info.getGameName().equals(DEFAULT_GAME_NAME);
        assert info.getPlayers().size() == 4;

        List<PregamePlayerInfo> players = info.getPlayers();
        assert getPlayer(1,players).getColor().equals(CatanColor.RED);
        assert getPlayer(1,players).getName().equals(RED_PLAYER_NAME);
        assert getPlayer(1,players).getId() == RED_PLAYER.getId();
        assert getPlayer(2,players).getColor().equals(CatanColor.BLUE);
        assert getPlayer(3,players).getColor().equals(CatanColor.ORANGE);
        assert getPlayer(4,players).getColor().equals(CatanColor.WHITE);
    }

    private PregameGame getFullPregameGame()
    {
        PregameGame result = new PregameGameImp(FULL_GAME_ID,FULL_GAME_NAME);
        result.addPlayer(RED_PLAYER, CatanColor.RED);
        result.addPlayer(BLUE_PLAYER, CatanColor.BLUE);
        result.addPlayer(ORANGE_PLAYER, CatanColor.ORANGE);
        result.addPlayer(WHITE_PLAYER, CatanColor.WHITE);
        return result;
    }

    private PregamePlayerInfo getPlayer(int id, List<PregamePlayerInfo> players)
    {
        for (PregamePlayerInfo player : players)
        {
            if (player.getId() == id)
                return player;
        }
        return null;
    }
}
