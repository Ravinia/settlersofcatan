package server.factory;

import org.junit.Before;

import server.exceptions.CatanServerException;
import server.model.GameModelImp;
import server.model.board.ServerBoard;
import server.model.game.PlayableGame;
import server.model.game.PregameGame;
import shared.definitions.CatanColor;

/**
 * Abstract class for doing factory tests that require the entire game model, not just the game object
 * @author Lawrence
 */
public abstract class AdvancedFactoryHelper extends FactoryHelper
{
    protected GameModelImp model;
    protected ServerBoard board;

    protected static final int DEFAULT_GAME_ID = 1;
    protected static final String DEFAULT_GAME_NAME = "fun";

    @Override
    @Before
    public void initialize() throws CatanServerException
    {
        try
        {
            model = new GameModelImp(false, false, false, DEFAULT_GAME_ID, DEFAULT_GAME_NAME);

            PregameGame pre = (PregameGame)model.getGame();
            pre.addPlayer(RED_PLAYER, CatanColor.RED);
            pre.addPlayer(BLUE_PLAYER, CatanColor.BLUE);
            pre.addPlayer(ORANGE_PLAYER, CatanColor.ORANGE);
            pre.addPlayer(WHITE_PLAYER, CatanColor.WHITE);

            model.startGame();
            game = (PlayableGame) model.getGame();
            board = (ServerBoard) model.getBoard();

        }
        catch (CatanServerException ignore)
        {}
    }

}
