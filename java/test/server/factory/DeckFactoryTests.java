package server.factory;

import org.junit.Test;
import server.courier.serializer.DeckInfo;
import server.factories.DeckFactory;
import server.model.game.bank.Deck;
import shared.definitions.DevCardType;

/**
 * @author Lawrence
 */
public class DeckFactoryTests extends FactoryHelper
{
    @Test
    public void test_defaultStartValues()
    {
        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 2;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buyMonopoly()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.MONOPOLY);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 1;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buyAllMonopoly()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.MONOPOLY);
        deck.buyCard(DevCardType.MONOPOLY);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 0;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buyRoadBuilding()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.ROAD_BUILD);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 2;
        assert deckInfo.getRoadBuilding() == 1;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buyYearOfPlenty()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.YEAR_OF_PLENTY);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 2;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 1;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buyMonument()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.MONUMENT);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 2;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 4;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buySoldier()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.SOLDIER);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 2;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 13;
    }

    @Test
    public void test_buyTooMany_Monopoly()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.MONOPOLY);
        deck.buyCard(DevCardType.MONOPOLY);
        deck.buyCard(DevCardType.MONOPOLY);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 0;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buyTooMany_RoadBuilding()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.ROAD_BUILD);
        deck.buyCard(DevCardType.ROAD_BUILD);
        deck.buyCard(DevCardType.ROAD_BUILD);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 2;
        assert deckInfo.getRoadBuilding() == 0;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buyTooMany_YearOfPlenty()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.YEAR_OF_PLENTY);
        deck.buyCard(DevCardType.YEAR_OF_PLENTY);
        deck.buyCard(DevCardType.YEAR_OF_PLENTY);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 2;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 0;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buyTooMany_Monument()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.MONUMENT);
        deck.buyCard(DevCardType.MONUMENT);
        deck.buyCard(DevCardType.MONUMENT);
        deck.buyCard(DevCardType.MONUMENT);
        deck.buyCard(DevCardType.MONUMENT);
        deck.buyCard(DevCardType.MONUMENT);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 2;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 0;
        assert deckInfo.getSoldier() == 14;
    }

    @Test
    public void test_buyTooMany_Soldier()
    {
        Deck deck = game.getDeck();
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);
        deck.buyCard(DevCardType.SOLDIER);

        DeckInfo deckInfo = DeckFactory.convertDeck(game);
        assert deckInfo.getMonopoly() == 2;
        assert deckInfo.getRoadBuilding() == 2;
        assert deckInfo.getYearOfPlenty() == 2;
        assert deckInfo.getMonument() == 5;
        assert deckInfo.getSoldier() == 0;
    }
}
