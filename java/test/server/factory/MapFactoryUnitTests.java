package server.factory;

import org.junit.Test;
import server.courier.serializer.HexInfo;
import server.courier.serializer.MapInfo;
import server.exceptions.DesertException;
import server.exceptions.PlayerNotInGameException;
import server.factories.MapFactory;
import server.model.board.placeable.*;
import shared.definitions.HexType;
import shared.exceptions.CatanException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.HexLocation;

import java.util.ArrayList;
import java.util.List;

import static server.model.board.LocationConstants.*;

/**
 * @author Lawrence
 */
public class MapFactoryUnitTests extends AdvancedFactoryHelper
{
    @Test
    public void test_defaultMap() throws PlayerNotInGameException
    {
        MapInfo info = MapFactory.convertMap(model);
        assert info.getCities().size() == 0;
        assert info.getSettlements().size() == 0;
        assert info.getRoads().size() == 0;

        assert info.getRobberLocation().equals(DESERT_HEX);
        verifyDefaultHexes(info);
    }

    @Test
    public void test_addBoardPieces() throws PlayerNotInGameException, InvalidObjectTypeException, InvalidRobberLocationException
    {
        List<Placeable> pieces = new ArrayList<>();
        pieces.add(new RoadPiece(CENTER_N_EDGE, RED_PLAYER));
        pieces.add(new SettlementPiece(CENTER_NE_VERTEX, RED_PLAYER));
        pieces.add(new CityPiece(VERTEX_1_N1_NE, BLUE_PLAYER));
        board.updateBoardObjects(pieces);

        MapInfo info = MapFactory.convertMap(model);
        assert info.getCities().size() == 1;
        assert info.getSettlements().size() == 1;
        assert info.getRoads().size() == 1;

        assert info.getRoads().get(0).getLocation().equals(CENTER_N_EDGE);
        assert info.getSettlements().get(0).getOwnerIndex() == model.getIndexOfPlayer(RED_PLAYER);

        assert info.getSettlements().get(0).getLocation().equals(CENTER_NE_VERTEX);
        assert info.getSettlements().get(0).getOwnerIndex() == model.getIndexOfPlayer(RED_PLAYER);

        assert info.getCities().get(0).getLocation().equals(VERTEX_1_N1_NE);
        assert info.getCities().get(0).getOwnerIndex() == model.getIndexOfPlayer(BLUE_PLAYER);

        assert info.getRobberLocation().equals(DESERT_HEX);
        verifyDefaultHexes(info);
    }

    @Test
    public void test_moveRobber() throws CatanException
    {
        board.updateBoardObjects(new RobberPiece(CENTER_HEX));

        MapInfo info = MapFactory.convertMap(model);
        assert info.getCities().size() == 0;
        assert info.getSettlements().size() == 0;
        assert info.getRoads().size() == 0;

        assert info.getRobberLocation().equals(CENTER_HEX);
        verifyDefaultHexes(info);
    }

    //Helper Functions
    private void verifyDefaultHexes(MapInfo info)
    {
        List<HexInfo> hexes = info.getHexes();
        verifyHex(hexes.get(0),HEX_0_N1, HexType.WOOD, 4);
        verifyHex(hexes.get(1),HEX_1_0, HexType.SHEEP, 10);
        verifyHex(hexes.get(2),HEX_N1_N1, HexType.SHEEP, 10);
        verifyHex(hexes.get(3),CENTER_HEX, HexType.WHEAT, 11);
        verifyHex(hexes.get(4),HEX_1_1, HexType.BRICK, 8);
        verifyHex(hexes.get(5),HEX_N1_0, HexType.BRICK, 5);
        verifyHex(hexes.get(6),HEX_0_1, HexType.WOOD, 3);
        verifyHex(hexes.get(7),HEX_N2_0, HexType.WHEAT, 9);
        verifyHex(hexes.get(8),HEX_N1_1, HexType.ORE, 6);
        verifyHex(hexes.get(9),HEX_0_2, HexType.DESERT, 0);
        verifyHex(hexes.get(10),HEX_N2_1, HexType.SHEEP, 12);
        verifyHex(hexes.get(11),HEX_N1_2, HexType.BRICK, 4);
        verifyHex(hexes.get(12),HEX_N2_2, HexType.WOOD, 11);
        verifyHex(hexes.get(13),HEX_2_N2, HexType.WOOD, 6);
        verifyHex(hexes.get(14),HEX_1_N2, HexType.ORE, 3);
        verifyHex(hexes.get(15),HEX_2_N1, HexType.WHEAT, 2);
        verifyHex(hexes.get(16),HEX_0_N2, HexType.WHEAT, 8);
        verifyHex(hexes.get(17),HEX_1_N1, HexType.SHEEP, 9);
        verifyHex(hexes.get(18),HEX_2_0, HexType.ORE, 5);
    }

    private void verifyHex(HexInfo hex, HexLocation location, HexType resource, int token)
    {
        assert hex.getLocation().equals(location);
        if (!hex.isDesert())
        {
            try
            {
                assert hex.getResource().equals(resource);
                assert hex.getNumber() == token;
            }
            catch (DesertException ignore)
            { }
        }
    }
}
