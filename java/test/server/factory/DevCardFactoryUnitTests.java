package server.factory;

import org.junit.Test;
import server.courier.serializer.DevCardHandInfo;
import server.factories.DevelopmentCardFactory;
import server.model.game.developmentcard.DevelopmentCardHand;
import shared.definitions.DevCardType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Lawrence
 */
public class DevCardFactoryUnitTests
{
    @Test
    public void test_emptyHands()
    {
        DevelopmentCardHand hand = new DevelopmentCardHand();

        DevCardHandInfo info = DevelopmentCardFactory.convertHand(hand);
        assert info.getNewDevCards().getMonopoly() == 0;
        assert info.getNewDevCards().getMonument() == 0;
        assert info.getNewDevCards().getSoldier() == 0;
        assert info.getNewDevCards().getYearOfPlenty() == 0;
        assert info.getNewDevCards().getRoadBuilding() == 0;

        assert info.getOldDevCards().getMonopoly() == 0;
        assert info.getOldDevCards().getMonument() == 0;
        assert info.getOldDevCards().getSoldier() == 0;
        assert info.getOldDevCards().getYearOfPlenty() == 0;
        assert info.getOldDevCards().getRoadBuilding() == 0;
    }

    @Test
    public void test_fullHand_new()
    {
        DevelopmentCardHand hand = new DevelopmentCardHand(getDefaultValues(), getDeckValues());

        DevCardHandInfo info = DevelopmentCardFactory.convertHand(hand);
        assert info.getNewDevCards().getMonopoly() == 2;
        assert info.getNewDevCards().getMonument() == 4;
        assert info.getNewDevCards().getSoldier() == 14;
        assert info.getNewDevCards().getYearOfPlenty() == 2;
        assert info.getNewDevCards().getRoadBuilding() == 2;

        assert info.getOldDevCards().getMonopoly() == 0;
        assert info.getOldDevCards().getMonument() == 0;
        assert info.getOldDevCards().getSoldier() == 0;
        assert info.getOldDevCards().getYearOfPlenty() == 0;
        assert info.getOldDevCards().getRoadBuilding() == 0;
    }

    @Test
    public void test_fullHand_old()
    {
        DevelopmentCardHand hand = new DevelopmentCardHand(getDeckValues(), getDefaultValues());

        DevCardHandInfo info = DevelopmentCardFactory.convertHand(hand);
        assert info.getNewDevCards().getMonopoly() == 0;
        assert info.getNewDevCards().getMonument() == 0;
        assert info.getNewDevCards().getSoldier() == 0;
        assert info.getNewDevCards().getYearOfPlenty() == 0;
        assert info.getNewDevCards().getRoadBuilding() == 0;

        assert info.getOldDevCards().getMonopoly() == 2;
        assert info.getOldDevCards().getMonument() == 4;
        assert info.getOldDevCards().getSoldier() == 14;
        assert info.getOldDevCards().getYearOfPlenty() == 2;
        assert info.getOldDevCards().getRoadBuilding() == 2;
    }

    @Test
    public void test_fullHand_both()
    {
        DevelopmentCardHand hand = new DevelopmentCardHand(getDeckValues(), getDeckValues());

        DevCardHandInfo info = DevelopmentCardFactory.convertHand(hand);
        assert info.getNewDevCards().getMonopoly() == 2;
        assert info.getNewDevCards().getMonument() == 4;
        assert info.getNewDevCards().getSoldier() == 14;
        assert info.getNewDevCards().getYearOfPlenty() == 2;
        assert info.getNewDevCards().getRoadBuilding() == 2;

        assert info.getOldDevCards().getMonopoly() == 2;
        assert info.getOldDevCards().getMonument() == 4;
        assert info.getOldDevCards().getSoldier() == 14;
        assert info.getOldDevCards().getYearOfPlenty() == 2;
        assert info.getOldDevCards().getRoadBuilding() == 2;
    }

    //Helper Methods
    public Collection<DevCardType> getDeckValues()
    {
        List<DevCardType> cards = new ArrayList<>();
        cards.add(DevCardType.MONOPOLY);
        cards.add(DevCardType.MONOPOLY);
        cards.add(DevCardType.YEAR_OF_PLENTY);
        cards.add(DevCardType.YEAR_OF_PLENTY);
        cards.add(DevCardType.ROAD_BUILD);
        cards.add(DevCardType.ROAD_BUILD);
        cards.add(DevCardType.MONUMENT);
        cards.add(DevCardType.MONUMENT);
        cards.add(DevCardType.MONUMENT);
        cards.add(DevCardType.MONUMENT);

        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        cards.add(DevCardType.SOLDIER);
        return cards;
    }

    public Collection<DevCardType> getDefaultValues()
    {
        return new ArrayList<DevCardType>();
    }
}
