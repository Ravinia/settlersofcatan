package server.model.player;

import org.junit.Test;
import server.courier.serializer.PregamePlayerInfo;
import server.courier.serializer.PregamePlayerInfoImp;
import shared.definitions.CatanColor;

import static server.model.player.DefaultPlayer.*;

/**
 * @author Lawrence
 */
public class DefaultPlayerUnitTests
{
    @Test
    public void player_equals_defaultPlayer()
    {
        Player player = new ServerPlayer(-1, "Bob");
        assert player.equals(DEFAULT_PLAYER);
    }

    @Test
    public void defaultPlayer_equals_player()
    {
        Player player = new ServerPlayer(-1, "Bob");
        assert DEFAULT_PLAYER.equals(player);
    }

    @Test
    public void playerInfo_equals_defaultPlayer()
    {
        PregamePlayerInfo player = new PregamePlayerInfoImp("Bob", -1, CatanColor.BLUE);
        assert player.equals(DEFAULT_PLAYER);
    }

    @Test
    public void defaultPlayer_equals_playerInfo()
    {
        PregamePlayerInfo player = new PregamePlayerInfoImp("Bob", -1, CatanColor.BLUE);
        assert DEFAULT_PLAYER.equals(player);
    }
}
