package server.model.board;

import org.junit.Before;
import org.junit.Test;
import server.model.board.placeable.Road;
import server.model.board.placeable.RoadPiece;
import server.model.player.Player;
import server.model.player.ServerPlayer;
import shared.exceptions.CatanException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.EdgeLocation;

import static org.junit.Assert.fail;
import static server.model.board.LocationConstants.*;

/**
 * @author Lawrence
 */
public class LongestRoadUnitTests
{
    private Board board = null;
    private static final Player RED_PLAYER = new ServerPlayer(1,"Red");
    private static final Player BLUE_PLAYER = new ServerPlayer(2,"Blue");
    private static final Player GREEN_PLAYER = new ServerPlayer(3,"Green");

    @Before
    public void initialize() throws CatanException
    {
        board = new ServerBoard(false, false, false);
    }

    @Test
    public void test_straightRoad()
    {
        addRoad(CENTER_NE_EDGE);
        addRoad(CENTER_N_EDGE);
        addRoad(CENTER_NW_EDGE);
        addRoad(EDGE_1_0_N);
        addRoad(EDGE_1_0_NE);

        assert board.getLongestRoadSize(RED_PLAYER) == 5;
    }

    @Test
    public void test_notStraightRoad()
    {
        addRoad(CENTER_NE_EDGE);
        addRoad(CENTER_N_EDGE);
        addRoad(CENTER_NW_EDGE);
        addRoad(EDGE_N1_0_NE);
        addRoad(EDGE_1_N1_NW);

        int result = board.getLongestRoadSize(RED_PLAYER);
        assert result == 3;
    }

    @Test
    public void test_loop()
    {
        addRoad(CENTER_NE_EDGE);
        addRoad(CENTER_N_EDGE);
        addRoad(CENTER_NW_EDGE);
        addRoad(EDGE_0_1_N);
        addRoad(EDGE_1_0_NW);
        addRoad(EDGE_N1_1_NE);
        addRoad(EDGE_N1_1_N);

        assert board.getLongestRoadSize(RED_PLAYER) == 7;
    }

    @Test
    public void test_multiplePlayersStretch()
    {
        addRoad(CENTER_NE_EDGE,RED_PLAYER);
        addRoad(CENTER_N_EDGE,RED_PLAYER);
        addRoad(CENTER_NW_EDGE,RED_PLAYER);
        addRoad(EDGE_1_0_N,BLUE_PLAYER);
        addRoad(EDGE_1_0_NE,BLUE_PLAYER);

        assert board.getLongestRoadSize(RED_PLAYER) == 3;
        assert board.getLongestRoadSize(BLUE_PLAYER) == 2;
    }


    //Helper Methods
    private void addRoad(EdgeLocation edge)
    {
        addRoad(edge, RED_PLAYER);
    }

    private void addRoad(EdgeLocation edge, Player player)
    {
        try
        {
            Road road = new RoadPiece(edge, player);
            board.updateBoardObjects(road);
        }
        catch (InvalidObjectTypeException | InvalidRobberLocationException e)
        {
            fail(e.getMessage());
        }
    }
}
