package server.model.board;

import org.junit.Before;
import org.junit.Test;
import server.model.board.immutable.Hex;
import server.model.board.immutable.ports.Port;
import shared.definitions.HexType;
import shared.definitions.PortType;
import shared.exceptions.*;
import shared.locations.HexLocation;
import shared.locations.PortLocation;

import static server.model.board.LocationConstants.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Unit tests for the server board.
 * Note: in order to accurately assess the randomness of the values, all locations must be tested.
 * @author Lawrence
 */
public class BoardInitializationUnitTests
{
    Board board = null;

    @Before
    public void initialize()
    {
        board = null;
    }

    @Test
    public void test_nonRandomHexes() throws InvalidLocationException, InvalidObjectTypeException, InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(false, false, false);

        assert isResourceType(0,0,HexType.WHEAT);
        assert isResourceType(0,1,HexType.WOOD);
        assert isResourceType(0,2,HexType.DESERT);
        assert isResourceType(0,-1,HexType.WOOD);
        assert isResourceType(0,-2,HexType.WHEAT);
        assert isResourceType(-1,2,HexType.BRICK);
        assert isResourceType(-1,1,HexType.ORE);
        assert isResourceType(-1,0,HexType.BRICK);
        assert isResourceType(-1,-1,HexType.SHEEP);
        assert isResourceType(-2,2,HexType.WOOD);
        assert isResourceType(-2,1,HexType.SHEEP);
        assert isResourceType(-2,0,HexType.WHEAT);
        assert isResourceType(1,1,HexType.BRICK);
        assert isResourceType(1,0,HexType.SHEEP);
        assert isResourceType(1,-1,HexType.SHEEP);
        assert isResourceType(1,-2,HexType.ORE);
        assert isResourceType(2,-2,HexType.WOOD);
        assert isResourceType(2,-1,HexType.WHEAT);
        assert isResourceType(2,0,HexType.ORE);
    }

    @Test
    public void test_RandomHexes() throws InvalidLocationException, InvalidObjectTypeException, InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(true, false, false);
        List<Boolean> booleans = new ArrayList<>();

        booleans.add(isResourceType(0,0,HexType.WHEAT));
        booleans.add(isResourceType(0,1,HexType.WOOD));
        booleans.add(isResourceType(0,2,HexType.DESERT));
        booleans.add(isResourceType(0,-1,HexType.WOOD));
        booleans.add(isResourceType(0,-2,HexType.WHEAT));
        booleans.add(isResourceType(-1,2,HexType.BRICK));
        booleans.add(isResourceType(-1,1,HexType.ORE));
        booleans.add(isResourceType(-1,0,HexType.BRICK));
        booleans.add(isResourceType(-1,-1,HexType.SHEEP));
        booleans.add(isResourceType(-2,2,HexType.WOOD));
        booleans.add(isResourceType(-2,1,HexType.SHEEP));
        booleans.add(isResourceType(-2,0,HexType.WHEAT));
        booleans.add(isResourceType(1,1,HexType.BRICK));
        booleans.add(isResourceType(1,0,HexType.SHEEP));
        booleans.add(isResourceType(1,-1,HexType.SHEEP));
        booleans.add(isResourceType(1,-2,HexType.ORE));
        booleans.add(isResourceType(2,-2,HexType.WOOD));
        booleans.add(isResourceType(2,-1,HexType.WHEAT));
        booleans.add(isResourceType(2,0,HexType.ORE));

        assert !isAllTrue(booleans);
    }

    @Test
    public void test_nonRandomTokens() throws InvalidLocationException, InvalidObjectTypeException, InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(false, false, false);

        assert isTokenValue(0,0,11);
        assert isTokenValue(0,1,3);
        assert isTokenValue(0,2,-1);
        assert isTokenValue(0,-1,4);
        assert isTokenValue(0,-2,8);
        assert isTokenValue(-1,2,4);
        assert isTokenValue(-1,1,6);
        assert isTokenValue(-1,0,5);
        assert isTokenValue(-1,-1,10);
        assert isTokenValue(-2,2,11);
        assert isTokenValue(-2,1,12);
        assert isTokenValue(-2,0,9);
        assert isTokenValue(1,1,8);
        assert isTokenValue(1,0,10);
        assert isTokenValue(1,-1,9);
        assert isTokenValue(1,-2,3);
        assert isTokenValue(2,-2,6);
        assert isTokenValue(2,-1,2);
        assert isTokenValue(2,0,5);
    }

    @Test
    public void test_RandomTokens() throws InvalidLocationException, InvalidObjectTypeException, InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(false, false, true);
        List<Boolean> booleans = new ArrayList<>();

        booleans.add(isTokenValue(0,0,11));
        booleans.add(isTokenValue(0,1,3));
        booleans.add(isTokenValue(0,2,-1));
        booleans.add(isTokenValue(0,-1,4));
        booleans.add(isTokenValue(0,-2,8));
        booleans.add(isTokenValue(-1,2,4));
        booleans.add(isTokenValue(-1,1,6));
        booleans.add(isTokenValue(-1,0,5));
        booleans.add(isTokenValue(-1,-1,10));
        booleans.add(isTokenValue(-2,2,11));
        booleans.add(isTokenValue(-2,1,12));
        booleans.add(isTokenValue(-2,0,9));
        booleans.add(isTokenValue(1,1,8));
        booleans.add(isTokenValue(1,0,10));
        booleans.add(isTokenValue(1,-1,9));
        booleans.add(isTokenValue(1,-2,3));
        booleans.add(isTokenValue(2,-2,6));
        booleans.add(isTokenValue(2,-1,2));
        booleans.add(isTokenValue(2,0,5));

        assert !isAllTrue(booleans);
    }

    @Test
    public void test_nonRandomPorts() throws InvalidObjectTypeException, InvalidLocationException, InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(false, false, false);
        List<PortLocation> ports = LocationConstants.getPortLocationsList();

        assert isPortType(ports.get(0),PortType.WOOD);
        assert isPortType(ports.get(1),PortType.THREE);
        assert isPortType(ports.get(2),PortType.WHEAT);
        assert isPortType(ports.get(3),PortType.ORE);
        assert isPortType(ports.get(4),PortType.THREE);
        assert isPortType(ports.get(5),PortType.SHEEP);
        assert isPortType(ports.get(6),PortType.THREE);
        assert isPortType(ports.get(7),PortType.THREE);
        assert isPortType(ports.get(8),PortType.BRICK);
    }

    @Test
    public void test_RandomPorts() throws InvalidObjectTypeException, InvalidLocationException,
                                            InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(false, true, false);
        List<PortLocation> ports = LocationConstants.getPortLocationsList();
        List<Boolean> booleans = new ArrayList<>();

        booleans.add(isPortType(ports.get(0), PortType.WOOD));
        booleans.add(isPortType(ports.get(1),PortType.THREE));
        booleans.add(isPortType(ports.get(2),PortType.WHEAT));
        booleans.add(isPortType(ports.get(3),PortType.ORE));
        booleans.add(isPortType(ports.get(4),PortType.THREE));
        booleans.add(isPortType(ports.get(5),PortType.SHEEP));
        booleans.add(isPortType(ports.get(6),PortType.THREE));
        booleans.add(isPortType(ports.get(7),PortType.THREE));
        booleans.add(isPortType(ports.get(8),PortType.BRICK));

        assert !isAllTrue(booleans);
    }

    @Test
    public void test_RobberStartsOnDesert_Default()
            throws InvalidLocationException, InvalidObjectTypeException, InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(false, false, false);
        assert board.getRobberLocation() == DESERT_HEX;
    }

    @Test
    public void test_RobberStartsOnDesert_Random()
            throws InvalidLocationException, InvalidObjectTypeException, InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(true, true, true);

        HexLocation robber = board.getRobberLocation();
        HexLocation desert = ((ServerBoard)board).getDesertHex().getLocation();
        assert robber.equals(desert);
    }

    //Helper Methods
    private boolean isAllTrue(Collection<Boolean> booleans)
    {
        for (Boolean bool : booleans)
        {
            if (!bool)
                return false;
        }
        return true;
    }

    private boolean isPortType(PortLocation location, PortType expected) throws InvalidLocationException
    {
        Port port = board.getPort(location);
        return port.getPortType() == expected;
    }

    private boolean isTokenValue(int x, int y, int value) throws InvalidLocationException
    {
        HexLocation loc = new HexLocation(x, y);
        Hex hex = board.getHex(loc);
        if (value == -1)
            return hex.getToken() == null;

        return hex.getToken().getValue() == value;
    }

    private boolean isResourceType(int x, int y, HexType type) throws InvalidLocationException
    {
        HexLocation loc = new HexLocation(x, y);
        Hex hex = board.getHex(loc);
        return hex.getType().equals(type);
    }
}
