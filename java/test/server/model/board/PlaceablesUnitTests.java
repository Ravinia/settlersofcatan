package server.model.board;

import shared.exceptions.InvalidRobberLocationException;
import org.junit.Before;
import org.junit.Test;
import server.exceptions.PlaceablesNotInitializedException;
import server.model.board.placeable.*;
import server.model.player.Player;
import server.model.player.ServerPlayer;
import shared.definitions.PieceType;
import shared.exceptions.CatanException;
import shared.exceptions.InvalidLocationException;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.fail;
import static server.model.board.LocationConstants.*;

/**
 * @author Lawrence
 */
public class PlaceablesUnitTests
{
    private Board board = null;
    private static final Player RED_PLAYER = new ServerPlayer(1,"Red");
    private static final Player BLUE_PLAYER = new ServerPlayer(2,"Blue");

    @Before
    public void initialize() throws CatanException
    {
        board = new ServerBoard(false, false, false);
    }

    @Test
    public void test_MoveRobber() throws CatanException
    {
        Robber robber = new RobberPiece(CENTER_HEX);
        board.updateBoardObjects(robber);

        HexLocation location = board.getRobberLocation();
        assert location.equals(CENTER_HEX);
    }

    @Test(expected = InvalidRobberLocationException.class)
    public void test_CannotMoveRobberToSameLocation() throws CatanException
    {
        Robber robber = new RobberPiece(DESERT_HEX);
        board.updateBoardObjects(robber);
    }

    @Test
    public void test_AddRoad() throws CatanException
    {
        Road road = new RoadPiece(CENTER_N_EDGE, RED_PLAYER);
        board.updateBoardObjects(road);

        Road result = board.getRoad(CENTER_N_EDGE);
        assert result.getLocation().equals(CENTER_N_EDGE);
        assert result.getPlayer().equals(RED_PLAYER);
        assert result.getType().equals(PieceType.ROAD);
        assert result.equals(road);
    }

    @Test
    public void test_AddSettlement() throws CatanException
    {
        Settlement settlement = new SettlementPiece(CENTER_NE_VERTEX, RED_PLAYER);
        board.updateBoardObjects(settlement);

        Settlement result = board.getSettlement(CENTER_NE_VERTEX);
        assert result.getLocation().equals(CENTER_NE_VERTEX);
        assert result.getPlayer().equals(RED_PLAYER);
        assert result.getType().equals(PieceType.SETTLEMENT);
        assert !result.isCity();
        assert result.getVictoryPoints() == 1;
        assert result.equals(settlement);
    }

    @Test
    public void test_AddCity() throws CatanException
    {
        Settlement settlement = new CityPiece(CENTER_NE_VERTEX, RED_PLAYER);
        board.updateBoardObjects(settlement);

        Settlement result = board.getSettlement(CENTER_NE_VERTEX);
        assert result.getLocation().equals(CENTER_NE_VERTEX);
        assert result.getPlayer().equals(RED_PLAYER);
        assert result.getType().equals(PieceType.CITY);
        assert result.isCity();
        assert result.getVictoryPoints() == 2;
        assert result.equals(settlement);
    }

    @Test
    public void test_AddCityOnSettlement() throws CatanException
    {
        Settlement settlement = new SettlementPiece(CENTER_NE_VERTEX, RED_PLAYER);
        board.updateBoardObjects(settlement);

        Settlement result = board.getSettlement(CENTER_NE_VERTEX);
        assert result.getLocation().equals(CENTER_NE_VERTEX);
        assert result.getPlayer().equals(RED_PLAYER);
        assert result.getType().equals(PieceType.SETTLEMENT);
        assert !result.isCity();
        assert result.getVictoryPoints() == 1;
        assert result.equals(settlement);

        settlement = new CityPiece(CENTER_NE_VERTEX, RED_PLAYER);
        board.updateBoardObjects(settlement);

        result = board.getSettlement(CENTER_NE_VERTEX);
        assert result.getLocation().equals(CENTER_NE_VERTEX);
        assert result.getPlayer().equals(RED_PLAYER);
        assert result.getType().equals(PieceType.CITY);
        assert result.isCity();
        assert result.getVictoryPoints() == 2;
        assert result.equals(settlement);
        assert ((ServerBoard)board).getNumberOfSettlements() == 1;
    }

    @Test
    public void test_UpdateBoardObjects() throws PlaceablesNotInitializedException
    {
        try
        {
            board.getSettlement(CENTER_NE_VERTEX);
            //should throw an exception at this point
            fail();
        }
        catch (InvalidLocationException ignore)
        {}
        try
        {
            SettlementPiece red = new SettlementPiece(CENTER_NE_VERTEX, RED_PLAYER);
            SettlementPiece blue = new CityPiece(VERTEX_N1_1_NE, BLUE_PLAYER);
            List<Placeable> placeables = new ArrayList<>();
            placeables.add(red);
            placeables.add(blue);
            board.updateBoardObjects(placeables);

            Settlement settlement = board.getSettlement(CENTER_NE_VERTEX);
            assert !settlement.isCity();
            assert settlement.getPlayer().equals(RED_PLAYER);

            settlement = board.getSettlement(VERTEX_N1_1_NE);
            assert settlement.isCity();
            assert settlement.getPlayer().equals(BLUE_PLAYER);
        }
        catch (Exception ignore)
        {
            //should NOT throw an exception during this part
            fail();
        }
    }

    @Test
    public void test_getEdgesWithRoad_allValid() throws CatanException
    {
        Road road1 = new RoadPiece(CENTER_N_EDGE, RED_PLAYER);
        Road road2 = new RoadPiece(CENTER_NE_EDGE, RED_PLAYER);
        Road road3 = new RoadPiece(CENTER_NW_EDGE, RED_PLAYER);
        List<Placeable> roads = new ArrayList<>();
        roads.add(road1);
        roads.add(road2);
        roads.add(road3);

        board.updateBoardObjects(roads);
        List<EdgeLocation> edges = new ArrayList<>();
        edges.add(CENTER_N_EDGE);
        edges.add(CENTER_NE_EDGE);
        edges.add(CENTER_NW_EDGE);

        Collection<EdgeLocation> result = ((ServerBoard)board).getEdgesWithRoad(RED_PLAYER, edges);
        assert result.size() == 3;
        assert result.contains(CENTER_N_EDGE);
        assert result.contains(CENTER_NE_EDGE);
        assert result.contains(CENTER_NW_EDGE);
    }

    @Test
    public void test_getEdgesWithRoad_someInvalid_WrongPlayer() throws CatanException
    {
        Road road1 = new RoadPiece(CENTER_N_EDGE, RED_PLAYER);
        Road road2 = new RoadPiece(CENTER_NE_EDGE, RED_PLAYER);
        Road road3 = new RoadPiece(CENTER_NW_EDGE, BLUE_PLAYER);
        List<Placeable> roads = new ArrayList<>();
        roads.add(road1);
        roads.add(road2);
        roads.add(road3);

        board.updateBoardObjects(roads);
        List<EdgeLocation> edges = new ArrayList<>();
        edges.add(CENTER_N_EDGE);
        edges.add(CENTER_NE_EDGE);
        edges.add(CENTER_NW_EDGE);

        Collection<EdgeLocation> result = ((ServerBoard)board).getEdgesWithRoad(RED_PLAYER, edges);
        assert result.size() == 2;
        assert result.contains(CENTER_N_EDGE);
        assert result.contains(CENTER_NE_EDGE);
        assert !result.contains(CENTER_NW_EDGE);
    }

    @Test
    public void test_getEdgesWithRoad_someInvalid_NoRoadThere() throws CatanException
    {
        Road road1 = new RoadPiece(CENTER_N_EDGE, RED_PLAYER);
        Road road2 = new RoadPiece(CENTER_NE_EDGE, RED_PLAYER);
        List<Placeable> roads = new ArrayList<>();
        roads.add(road1);
        roads.add(road2);

        board.updateBoardObjects(roads);
        List<EdgeLocation> edges = new ArrayList<>();
        edges.add(CENTER_N_EDGE);
        edges.add(CENTER_NE_EDGE);
        edges.add(CENTER_NW_EDGE);

        Collection<EdgeLocation> result = ((ServerBoard)board).getEdgesWithRoad(RED_PLAYER, edges);
        assert result.size() == 2;
        assert result.contains(CENTER_N_EDGE);
        assert result.contains(CENTER_NE_EDGE);
        assert !result.contains(CENTER_NW_EDGE);
    }

    @Test
    public void test_getEdgesWithRoad_allInvalid_WrongPlayer() throws CatanException
    {
        Road road1 = new RoadPiece(CENTER_N_EDGE, RED_PLAYER);
        Road road2 = new RoadPiece(CENTER_NE_EDGE, RED_PLAYER);
        Road road3 = new RoadPiece(CENTER_NW_EDGE, RED_PLAYER);
        List<Placeable> roads = new ArrayList<>();
        roads.add(road1);
        roads.add(road2);
        roads.add(road3);

        board.updateBoardObjects(roads);
        List<EdgeLocation> edges = new ArrayList<>();
        edges.add(CENTER_N_EDGE);
        edges.add(CENTER_NE_EDGE);
        edges.add(CENTER_NW_EDGE);

        Collection<EdgeLocation> result = ((ServerBoard)board).getEdgesWithRoad(BLUE_PLAYER, edges);
        assert result.size() == 0;
    }

    @Test
    public void test_getEdgesWithRoad_allInvalid_NoRoads() throws CatanException
    {
        List<EdgeLocation> edges = new ArrayList<>();
        edges.add(CENTER_N_EDGE);
        edges.add(CENTER_NE_EDGE);
        edges.add(CENTER_NW_EDGE);

        Collection<EdgeLocation> result = ((ServerBoard)board).getEdgesWithRoad(RED_PLAYER, edges);
        assert result.size() == 0;
    }

    @Test
    public void test_getEdgesWithRoad_allInvalid_EmptyList() throws CatanException
    {
        List<EdgeLocation> edges = new ArrayList<>();

        Collection<EdgeLocation> result = ((ServerBoard)board).getEdgesWithRoad(RED_PLAYER, edges);
        assert result.size() == 0;
    }
}
