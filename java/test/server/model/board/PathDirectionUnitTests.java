package server.model.board;

import org.junit.Before;
import org.junit.Test;
import server.model.board.placeable.PathDirection;
import server.model.board.placeable.RoadPath;
import shared.exceptions.CatanException;
import shared.locations.EdgeLocation;

import static server.model.board.LocationConstants.*;
/**
 * @author Lawrence
 */
public class PathDirectionUnitTests
{
    private Board board = null;

    @Before
    public void initialize() throws CatanException
    {
        board = new ServerBoard(false, false, false);
    }

    @Test
    public void test_east_Up()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(EDGE_N1_0_NE, CENTER_N_EDGE));
        assert direction == PathDirection.East;
    }

    @Test
    public void test_east_Down()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(CENTER_NW_EDGE, CENTER_N_EDGE));
        assert direction == PathDirection.East;
    }

    @Test
    public void test_west_Up()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(EDGE_1_N1_NW, CENTER_N_EDGE));
        assert direction == PathDirection.West;
    }

    @Test
    public void test_west_Down()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(CENTER_NE_EDGE, CENTER_N_EDGE));
        assert direction == PathDirection.West;
    }

    @Test
    public void test_NW_Left()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(EDGE_1_0_NW, CENTER_NE_EDGE));
        assert direction == PathDirection.NorthWest;
    }

    @Test
    public void test_NW_Right()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(EDGE_1_0_N, CENTER_NE_EDGE));
        assert direction == PathDirection.NorthWest;
    }

    @Test
    public void test_SE_Left()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(CENTER_N_EDGE, CENTER_NE_EDGE));
        assert direction == PathDirection.SouthEast;
    }

    @Test
    public void test_SE_Right()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(EDGE_1_N1_NW, CENTER_NE_EDGE));
        assert direction == PathDirection.SouthEast;
    }

    @Test
    public void test_SW_Left()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(CENTER_N_EDGE, CENTER_NW_EDGE));
        assert !(direction == PathDirection.SouthEast);
        assert direction == PathDirection.SouthWest;

    }

    @Test
    public void test_SW_Right()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(EDGE_N1_0_NE, CENTER_NW_EDGE));
        assert direction == PathDirection.SouthWest;
    }

    @Test
    public void test_NE_Left()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(EDGE_N1_1_N, CENTER_NW_EDGE));
        assert direction == PathDirection.NorthEast;
    }

    @Test
    public void test_NE_Right()
    {
        PathDirection direction = PathDirection.getPathDirection(toPath(EDGE_N1_1_NE, CENTER_NW_EDGE));
        assert direction == PathDirection.NorthEast;
    }

    @Test
    public void test_longPath()
    {
        RoadPath path = new RoadPath(EDGE_N1_1_N);
        path = path.getNewPath(CENTER_NW_EDGE);
        path = path.getNewPath(CENTER_N_EDGE);
        path = path.getNewPath(CENTER_NE_EDGE);
        path = path.getNewPath(EDGE_1_0_N);

        PathDirection direction = PathDirection.getPathDirection(path);
        assert direction == PathDirection.East;
    }

    @Test
    public void test_validDirection_east()
    {
        RoadPath path = toPath(EDGE_N1_0_NE, CENTER_N_EDGE);
        assert path.isValidDirection(CENTER_NE_EDGE);
        assert path.isValidDirection(EDGE_1_N1_NW);
        assert !path.isValidDirection(CENTER_NW_EDGE);
    }

    @Test
    public void test_validDirection_west()
    {
        RoadPath path = toPath(EDGE_1_N1_NW, CENTER_N_EDGE);
        assert path.isValidDirection(CENTER_NW_EDGE);
        assert path.isValidDirection(EDGE_N1_0_NE);
        assert !path.isValidDirection(CENTER_NE_EDGE);
    }

    @Test
    public void test_validDirection_northwest()
    {
        RoadPath path = toPath(EDGE_1_0_NW, CENTER_NE_EDGE);
        assert path.isValidDirection(CENTER_N_EDGE);
        assert path.isValidDirection(EDGE_1_N1_NW);
        assert !path.isValidDirection(EDGE_1_0_NW);
    }

    @Test
    public void test_validDirection_northeast()
    {
        RoadPath path = toPath(EDGE_N1_1_N, CENTER_NW_EDGE);
        assert path.isValidDirection(CENTER_N_EDGE);
        assert path.isValidDirection(EDGE_N1_0_NE);
        assert !path.isValidDirection(EDGE_N1_1_NE);
        assert !path.isValidDirection(EDGE_N1_1_N);
    }

    @Test
    public void test_validDirection_southeast()
    {
        RoadPath path = toPath(CENTER_N_EDGE, CENTER_NE_EDGE);
        assert path.isValidDirection(EDGE_1_0_N);
        assert path.isValidDirection(EDGE_1_0_NW);
        assert !path.isValidDirection(EDGE_1_N1_NW);
    }

    @Test
    public void test_validDirection_southwest()
    {
        RoadPath path = toPath(CENTER_N_EDGE, CENTER_NW_EDGE);
        assert path.isValidDirection(EDGE_N1_1_N);
        assert path.isValidDirection(EDGE_N1_1_NE);
        assert !path.isValidDirection(EDGE_N1_0_NE);
    }

    //Helper Methods
    private RoadPath toPath(EdgeLocation loc1, EdgeLocation loc2)
    {
        RoadPath path = new RoadPath(loc1);
        path = path.getNewPath(loc2);
        return path;
    }
}
