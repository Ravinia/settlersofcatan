package server.model.board;

import org.junit.Before;
import org.junit.Test;
import server.model.board.immutable.Edge;
import server.model.board.immutable.Hex;
import server.model.board.immutable.Token;
import server.model.board.placeable.RoadPath;
import shared.definitions.HexType;
import shared.exceptions.*;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;

import java.util.Collection;

import static org.junit.Assert.fail;
import static server.model.board.LocationConstants.*;
import static server.model.board.LocationConstants.DESERT_HEX;

/**
 * @author Lawrence
 */
public class ImmutablesUnitTests
{
    Board board = null;

    @Before
    public void initialize()
    {
        board = null;
    }

    @Test
    public void test_GetNeighboringHex()
    {
        try
        {
            board = new ServerBoard(false, false, false);
            Hex hex = board.getNeighboringHex(board.getHex(CENTER_HEX), EdgeDirection.North);
            if (hex == null)
                fail();
            assert hex.getLocation().equals(HEX_0_N1);
            assert hex.getType() == HexType.WOOD;
            assert hex.getToken().getValue() == 4;
        }
        catch (InvalidLocationException | InvalidObjectTypeException |
                InvalidPortTypeException | NotInitializedException e)
        {
            fail(e.getMessage());
        }
        catch (NullPointerException e)
        {
            fail("null object encountered");
        }
    }

    @Test
    public void test_GetDefaultDesertHex()
    {
        try
        {
            board = new ServerBoard(false, false, false);
            Hex hex = board.getHex(DESERT_HEX);
            assert hex.getType() == HexType.DESERT;
            assert hex.getToken() == null;
        }
        catch (InvalidLocationException | InvalidObjectTypeException |
                InvalidPortTypeException | NotInitializedException  e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void test_GetWaterHex()
    {
        try
        {
            board = new ServerBoard(false, false, false);
            Hex hex = board.getHex(NORTH_WATER_HEX);
            assert hex.getType() == HexType.WATER;
            assert hex.getToken() == null;
        }
        catch (InvalidLocationException | InvalidObjectTypeException |
                InvalidPortTypeException | NotInitializedException  e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void test_GetHex() throws InvalidLocationException, InvalidObjectTypeException,
            InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(false, false, false);
        Hex hex = board.getHex(CENTER_HEX);
        if (hex == null)
            fail();
        assert hex.getLocation() == CENTER_HEX;
        assert hex.getType() == HexType.WHEAT;
        assert hex.getToken().getValue() == 11;
    }

    @Test
    public void test_GetEdge() throws InvalidLocationException, InvalidObjectTypeException,
            InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(false, false, false);
        Edge edge = board.getEdge(CENTER_NE_EDGE);
        if (edge == null)
            fail();
    }

    @Test
    public void test_GetToken() throws InvalidLocationException, InvalidObjectTypeException,
            InvalidPortTypeException, NotInitializedException
    {
        board = new ServerBoard(false, false, false);
        Token token = board.getToken(CENTER_HEX);
        if (token == null)
            fail();
    }

    @Test (expected = InvalidLocationException.class)
    public void test_GetDesertToken_throwsException() throws InvalidLocationException, InvalidObjectTypeException, InvalidPortTypeException
    {
        try
        {
            board = new ServerBoard(false, false, false);
        }
        catch (NotInitializedException e)
        {
            fail(e.getMessage());
        }
        board.getToken(DESERT_HEX);
    }

    @Test (expected = InvalidLocationException.class)
    public void test_GetWaterToken_throwsException() throws InvalidLocationException, InvalidObjectTypeException, InvalidPortTypeException
    {
        try
        {
            board = new ServerBoard(false, false, false);
        }
        catch (NotInitializedException e)
        {
            fail(e.getMessage());
        }
        board.getToken(NORTH_WATER_HEX);
    }

    @Test
    public void test_GetDesertHexFunction() throws CatanException
    {
        board = new ServerBoard(false, false, false);
        Hex desert = ((ServerBoard)board).getDesertHex();
        assert desert.getLocation().equals(DESERT_HEX);
    }

    @Test
    public void test_GetValidAdjacentEdges_AllValid() throws CatanException
    {
        board = new ServerBoard(false, false, false);
        Collection<EdgeLocation> edges = ((ServerBoard)board).getValidAdjacentEdges(toPath(CENTER_N_EDGE));
        assert edges.size() == 4;
        assert edges.contains(CENTER_NE_EDGE);
        assert edges.contains(CENTER_NW_EDGE);
        assert edges.contains(EDGE_N1_0_NE);
        assert edges.contains(EDGE_1_N1_NW);
    }

    @Test
    public void test_GetValidAdjacentEdges_SomeInvalid() throws CatanException
    {
        board = new ServerBoard(false, false, false);
        Collection<EdgeLocation> edges = ((ServerBoard)board).getValidAdjacentEdges(toPath(EDGE_0_N2_N));
        assert edges.size() == 2;
        assert edges.contains(EDGE_0_N2_NE);
        assert edges.contains(EDGE_0_N2_NW);
    }

    @Test
    public void test_GetValidAdjacentEdges_AllInvalid() throws CatanException
    {
        board = new ServerBoard(false, false, false);
        Collection<EdgeLocation> edges = ((ServerBoard)board).getValidAdjacentEdges(toPath(INVALID_EDGE));
        assert edges.size() == 0;
    }


    //Helper Methods
    private static RoadPath toPath(EdgeLocation edge)
    {
        return new RoadPath(edge);
    }
}
