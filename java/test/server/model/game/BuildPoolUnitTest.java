package server.model.game;

import org.junit.Test;
import server.model.game.resources.BuildPool;

/**
 * @author Lawrence
 */
public class BuildPoolUnitTest
{
    @Test
    public void test_defaultBuildPool()
    {
        BuildPool pool = new BuildPool();
        assert pool.equals(new BuildPool());
    }

    @Test
    public void test_MinusOneOnAll()
    {
        BuildPool pool1 = new BuildPool();
        pool1.placeSettlement();
        pool1.placeSettlement();
        pool1.placeCity();
        pool1.placeRoad();

        BuildPool pool2 = new BuildPool();
        pool2.placeSettlement();
        pool2.placeSettlement();
        pool2.placeCity();
        pool2.placeRoad();

        assert pool1.getSettlements() == pool2.getSettlements();
        assert  pool1.getCities() == pool2.getCities();
        assert pool1.getRoads() == pool2.getRoads();

        assert pool1.getSettlements() == BuildPool.DEFAULT_SETTLEMENT_COUNT - 1;
        assert pool1.getCities() == BuildPool.DEFAULT_CITY_COUNT - 1;
        assert pool1.getRoads() == BuildPool.DEFAULT_ROAD_COUNT - 1;

        assert pool1.equals(pool2);
    }
}
