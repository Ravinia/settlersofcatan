package server.model.game;

import org.junit.Test;

import server.factory.AdvancedFactoryHelper;
import server.model.game.state.TurnOrder;

/**
 * @author Lawrence
 */
public class TurnOrderUnitTests extends AdvancedFactoryHelper
{
    @Test
    public void test_TurnOrderPreserved()
    {
        assert model.getPlayers().get(0).equals(RED_PLAYER);
        assert model.getPlayers().get(1).equals(BLUE_PLAYER);
        assert model.getPlayers().get(2).equals(ORANGE_PLAYER);
        assert model.getPlayers().get(3).equals(WHITE_PLAYER);
    }

    @Test
    public void test_nextPlayer()
    {
        assert game.getTurnOrder().getCurrentPlayer() == RED_PLAYER;
        assert game.getTurnOrder().getNextPlayer() == BLUE_PLAYER;
    }

    @Test
    public void test_nextPlayer_cycles()
    {
        ((TurnOrder)game.getTurnOrder()).setCurrentPlayerIndex(3);

        assert game.getTurnOrder().getCurrentPlayer() == WHITE_PLAYER;
        assert game.getTurnOrder().getNextPlayer() == RED_PLAYER;
    }
}
