package server.handler.user;

import server.facade.UserFacade;
import server.facade.mocks.MockUserFacade;
import server.handler.HandlerHelper;

public class UserHandlerHelper extends HandlerHelper
{
    protected UserFacade facade = new MockUserFacade();
}
