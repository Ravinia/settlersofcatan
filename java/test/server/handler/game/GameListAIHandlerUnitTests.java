package server.handler.game;

import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * @author Lawrence
 */
public class GameListAIHandlerUnitTests extends GameHandlerHelper
{
    private static final String EXPECTED_JSON = "[\"Type 1\",\"Type 2\",\"Type 3\"]";

    @Test
    public void test_getsAIList() throws IOException
    {
        GameListAI handler = new GameListAI(facade, jsonDeserializer, httpDeserializer, serializer);

        handler.handle(exchange);

        assert exchange.getResponseCode() == HttpURLConnection.HTTP_OK;
        String result = exchange.getResult();
        String expected = EXPECTED_JSON;
        assert result.trim().equals(expected.trim());
    }

}
