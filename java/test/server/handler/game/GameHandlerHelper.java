package server.handler.game;

import server.facade.GameFacade;
import server.facade.mocks.MockGameFacade;
import server.handler.HandlerHelper;

/**
 * @author Lawrence
 */
public class GameHandlerHelper extends HandlerHelper
{
    protected GameFacade facade = new MockGameFacade();
}
