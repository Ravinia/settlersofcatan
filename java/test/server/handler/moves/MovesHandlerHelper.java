package server.handler.moves;

import server.facade.mocks.MockMovesFacade;
import server.facade.mocks.ModelType;
import server.handler.HandlerHelper;
import server.serializer.MockHttpDeserializer;
import shared.exceptions.CatanException;

/**
 * @author Lawrence
 */
public abstract class MovesHandlerHelper extends HandlerHelper
{
    protected MockMovesFacade facade;
    protected MockHttpDeserializer httpDeserializer = new MockHttpDeserializer();

    public MovesHandlerHelper() throws CatanException
    {
        facade = new MockMovesFacade();
    }

    protected void setExpectedModel(ModelType model)
    {
        facade.setResultModel(model);
        httpDeserializer.setResultModel(model);
    }
}
