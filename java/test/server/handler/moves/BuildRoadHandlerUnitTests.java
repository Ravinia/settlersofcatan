package server.handler.moves;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import server.facade.mocks.ModelType;
import shared.exceptions.CatanException;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * @author Lawrence
 */
public class BuildRoadHandlerUnitTests extends MovesHandlerHelper
{

    public BuildRoadHandlerUnitTests() throws CatanException
    {
        super();
    }

    @Test
    public void test_BuildRoad_free() throws IOException
    {
        setExpectedModel(ModelType.ROAD);
        httpDeserializer.serializeBuildRoadInfo(true);

        MovesBuildRoad handler = new MovesBuildRoad(facade, jsonDeserializer, httpDeserializer, serializer);
        handler.handle(exchange);

        assert exchange.getResponseCode() == HttpURLConnection.HTTP_OK;
        String result = exchange.getResult();
        JSONObject model = new JSONObject(result);
        JSONObject deck = model.getJSONObject("deck");
        JSONObject map = model.getJSONObject("map");
        JSONArray playersArray = model.getJSONArray("players");
        JSONObject log = model.getJSONObject("log");
        JSONObject chat = model.getJSONObject("chat");
        JSONObject bank = model.getJSONObject("bank");
        JSONObject turnTracker = model.getJSONObject("turnTracker");


        assert bank.getInt("ore") == 19;
        assert bank.getInt("wood") == 19;
        assert bank.getInt("wheat") == 19;
        assert bank.getInt("brick") == 19;
        assert bank.getInt("sheep") == 19;

        assert deck.getInt("yearOfPlenty") == 2;
        assert deck.getInt("soldier") == 14;
        assert deck.getInt("monopoly") == 2;
        assert deck.getInt("monument") == 5;
        assert deck.getInt("roadBuilding") == 2;

        JSONArray roads = map.getJSONArray("roads");
        int owner = roads.getJSONObject(0).getInt("owner");
        JSONObject location = roads.getJSONObject(0).getJSONObject("location");
        int x = location.getInt("x");
        int y = location.getInt("y");
        String dir = location.get("direction").toString();

        assert owner == 0;
        assert x == 0;
        assert y == 0;
        assert dir.equals("N");
    }
}
