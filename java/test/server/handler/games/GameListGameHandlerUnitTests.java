package server.handler.games;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.junit.Test;

import shared.exceptions.CatanException;


public class GameListGameHandlerUnitTests extends GamesHandlerHelper
{
	private static final String EXPECTED_JSON = "[ { \"id\": 2, \"title\": \"AWESOMENESS\", \"players\": [ { \"id\": 0, \"color\": \"BLUE\", \"name\": \"Sam\" }, { \"id\": 1, \"color\": \"RED\", \"name\": \"Brooke\" }, { \"id\": 2, \"color\": \"YELLOW\", \"name\": \"Mark\" } ] }, { \"id\": 3, \"title\": \"COOLness\", \"players\": [ { \"id\": 0, \"color\": \"BROWN\", \"name\": \"Sam\" }, { \"id\": 1, \"color\": \"BLUE\", \"name\": \"Brooke\" }, { \"id\": 2, \"color\": \"PUCE\", \"name\": \"Mark\" }, { \"id\": 3, \"color\": \"PURPLE\", \"name\": \"Jack\" } ] } ]";

	public GameListGameHandlerUnitTests() throws CatanException
    {
        super();
    }
	@Test
	public void test_getGameList() throws IOException
    {
        GamesList handler = new GamesList(facade, jsonDeserializer, httpDeserializer, serializer);
//        Headers h = exchange.getResponseHeaders();
//        String cookie = this.httpDeserializer.buildUserCookie("Sam", "Sam", 0).split("catan.user=")[1];
//        try
//        {
//        h.add("Cookie", cookie);
//        }
//        catch (Exception e)
//        {
//        	e.printStackTrace();
//        }
        handler.handle(exchange);
        
        assert exchange.getResponseCode() == HttpURLConnection.HTTP_OK;
        String result = exchange.getResult();
        String expected = EXPECTED_JSON;
        assert result.trim().equals(expected.trim());
    }
}
