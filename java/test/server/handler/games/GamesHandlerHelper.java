package server.handler.games;

import server.facade.GamesFacade;
import server.facade.mocks.MockGamesFacade;
import server.handler.HandlerHelper;
import server.serializer.MockHttpDeserializer;
import shared.exceptions.CatanException;

public class GamesHandlerHelper extends HandlerHelper
{
    protected GamesFacade facade = new MockGamesFacade();
    protected MockHttpDeserializer httpDeserializer = new MockHttpDeserializer();
    
    public GamesHandlerHelper() throws CatanException
    {
        facade = new MockGamesFacade();
    }
}

