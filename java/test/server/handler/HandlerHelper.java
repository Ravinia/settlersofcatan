package server.handler;

import server.model.player.Player;
import server.model.player.ServerPlayer;
import server.serializer.*;

/**
 * @author Lawrence
 */
public abstract class HandlerHelper
{
    protected ServerJsonDeserializer jsonDeserializer = new ServerJsonDeserializerImp();
    protected ServerHttpDeserializer httpDeserializer = new ServerHttpDeserializerImp();
    protected ServerSerializer serializer = new ServerSerializerImp();

    protected ExchangeMock exchange = new ExchangeMock();

    protected static final String RED_PLAYER_NAME = "Red";
    protected static final String BLUE_PLAYER_NAME = "Blue";
    protected static final String ORANGE_PLAYER_NAME = "Orange";
    protected static final String WHITE_PLAYER_NAME = "White";
    protected static final Player RED_PLAYER = new ServerPlayer(1,RED_PLAYER_NAME);
    protected static final Player BLUE_PLAYER = new ServerPlayer(2,BLUE_PLAYER_NAME);
    protected static final Player ORANGE_PLAYER = new ServerPlayer(3,ORANGE_PLAYER_NAME);
    protected static final Player WHITE_PLAYER = new ServerPlayer(4,WHITE_PLAYER_NAME);
}
