package server.handler;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpPrincipal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.URI;

/**
 * @author Lawrence
 */
public class ExchangeMock extends HttpExchange
{
    private int actualCode = -1;
    private int expectedResponseLength = 0;
    private boolean checkLength = false;
    private String result = "";

    private OutputStream stream = new OutputStream()
    {
        StringWriter writer = new StringWriter();

        @Override
        public void write(int b) throws IOException
        {
            writer.write(b);
        }

        @Override
        public void write(byte[] b) throws IOException
        {
            writer.write(new String(b));
        }

        @Override
        public void close() throws IOException
        {
            result = writer.toString();
        }
    };

    //Debug Methods
    public String getResult()
    {
        return result;
    }

    //Overridden Methods
    @Override
    public void sendResponseHeaders(int i, long l) throws IOException
    {
        actualCode = i;
    }

    @Override
    public OutputStream getResponseBody()
    {
        return stream;
    }

    @Override
    public int getResponseCode()
    {
        return actualCode;
    }

    //Not Used Methods
    @Override
    public Headers getRequestHeaders()
    {
        return new Headers();
    }

    public Headers getResponseHeaders()
    {
    	return new Headers();
    }

    public URI getRequestURI()
    {
        return null;
    }

    public String getRequestMethod()
    {
        return null;
    }

    public com.sun.net.httpserver.HttpContext getHttpContext()
    {
        return null;
    }

    public void close()
    {

    }

    public InputStream getRequestBody()
    {
        return null;
    }

    public InetSocketAddress getRemoteAddress()
    {
        return null;
    }

    public InetSocketAddress getLocalAddress()
    {
        return null;
    }

    public String getProtocol()
    {
        return null;
    }

    public Object getAttribute(String s)
    {
        return null;
    }

    public void setAttribute(String s, Object o)
    {

    }

    public void setStreams(InputStream inputStream, OutputStream outputStream)
    {

    }

    public HttpPrincipal getPrincipal()
    {
        return null;
    }
}
