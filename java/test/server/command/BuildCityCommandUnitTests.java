package server.command;

import org.junit.Test;
import server.command.moves.BuildCityCommand;
import server.model.board.placeable.SettlementPiece;
import server.model.game.resources.BuildPool;
import shared.exceptions.CatanException;

import static server.model.board.LocationConstants.CENTER_NE_VERTEX;

/**
 * @author Lawrence
 */
public class BuildCityCommandUnitTests extends CommandHelper
{
    @Test
    public void test_PlaceCity_newLocation() throws CatanException
    {
        BuildCityCommand command = new BuildCityCommand(RED_PLAYER_INDEX,CENTER_NE_VERTEX,game.getID(),serverModel);

        game.getResourceHandOfPlayer(RED_PLAYER).setWood(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setWheat(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setSheep(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setBrick(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setOre(5);

        assert board.getSettlements().size() == 0;
        assert game.getResourceHandOfPlayer(RED_PLAYER).equals(FIVE_HAND);
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(new BuildPool());

        command.executeCommand();

        assert !game.getResourceHandOfPlayer(RED_PLAYER).equals(FIVE_HAND);
        assert game.getResourceHandOfPlayer(RED_PLAYER).getWood() == 5;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getBrick() == 5;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getSheep() == 5;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getWheat() == 3;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getOre() == 2;

        BuildPool pool = new BuildPool();
        pool.placeCity();
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(pool);

        assert board.getSettlements().size() == 1;
        assert board.getSettlement(CENTER_NE_VERTEX).getPlayer().equals(RED_PLAYER);
        assert board.getSettlement(CENTER_NE_VERTEX).isCity();
    }

    @Test
    public void test_PlaceCity_onSettlement() throws CatanException
    {
        BuildCityCommand command = new BuildCityCommand(RED_PLAYER_INDEX,CENTER_NE_VERTEX,game.getID(),serverModel);

        game.getResourceHandOfPlayer(RED_PLAYER).setWood(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setWheat(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setSheep(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setBrick(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setOre(5);

        board.updateBoardObjects(new SettlementPiece(CENTER_NE_VERTEX, RED_PLAYER));

        assert board.getSettlements().size() == 1;
        assert !board.getSettlement(CENTER_NE_VERTEX).isCity();
        assert game.getResourceHandOfPlayer(RED_PLAYER).equals(FIVE_HAND);
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(new BuildPool());

        command.executeCommand();

        assert !game.getResourceHandOfPlayer(RED_PLAYER).equals(FIVE_HAND);
        assert game.getResourceHandOfPlayer(RED_PLAYER).getWood() == 5;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getBrick() == 5;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getSheep() == 5;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getWheat() == 3;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getOre() == 2;

        BuildPool pool = new BuildPool();
        pool.placeCity();
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(pool);

        assert board.getSettlements().size() == 1;
        assert board.getSettlement(CENTER_NE_VERTEX).getPlayer().equals(RED_PLAYER);
        assert board.getSettlement(CENTER_NE_VERTEX).isCity();
    }
}
