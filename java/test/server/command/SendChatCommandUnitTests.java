package server.command;

import org.junit.Test;
import server.command.moves.SendChatCommand;
import server.exceptions.CatanServerException;
import server.model.game.chatlog.Message;

import java.util.List;

/**
 * @author Lawrence
 */
public class SendChatCommandUnitTests extends CommandHelper
{
    private static final String MESSAGE_01 = "Hello World";
    private static final String MESSAGE_02 = "Bob the Builder";
    private static final String MESSAGE_03 = "Wear a hat";
    private static final String MESSAGE_04 = "Preggo!";
    private static final String MESSAGE_05 = "GULLIBLE";
    private static final String MESSAGE_06 = "Poop";
    private static final String MESSAGE_07 = "asfaf394ur094glkdsjf94urojjg;lkdsjf;lkds";
    private static final String MESSAGE_08 = "guhh, this is not helpful";
    private static final String MESSAGE_09 = "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111";
    private static final String MESSAGE_10 = "But wait, there's more!";
    private static final String MESSAGE_11 = "Wear swimsuits";
    private static final String MESSAGE_12 = "Chainmail";
    private static final String MESSAGE_13 = "Eat cheese";
    private static final String MESSAGE_14 = "IS this a MESSAGE??";

    @Test
    public void test_sendChat() throws CatanServerException
    {
        SendChatCommand command = new SendChatCommand(RED_PLAYER_INDEX, MESSAGE_01, RED_PLAYER.getId(), serverModel);

        List<Message> chats = game.getChatLog().getChats();
        assert chats.size() == 0;

        command.executeCommand();

        assert chats.size() == 1;
        assert chats.get(0).getMessage().equals(MESSAGE_01);
        assert chats.get(0).getSource().equals(RED_PLAYER.getName());
    }

    @Test
    public void test_sendManyChats() throws CatanServerException
    {
        SendChatCommand command_01 = new SendChatCommand(RED_PLAYER_INDEX, MESSAGE_01, game.getID(), serverModel);
        SendChatCommand command_02 = new SendChatCommand(BLUE_PLAYER_INDEX, MESSAGE_02, game.getID(), serverModel);
        SendChatCommand command_03 = new SendChatCommand(WHITE_PLAYER_INDEX, MESSAGE_03, game.getID(), serverModel);
        SendChatCommand command_04 = new SendChatCommand(RED_PLAYER_INDEX, MESSAGE_04, game.getID(), serverModel);
        SendChatCommand command_05 = new SendChatCommand(ORANGE_PLAYER_INDEX, MESSAGE_05, game.getID(), serverModel);
        SendChatCommand command_06 = new SendChatCommand(RED_PLAYER_INDEX, MESSAGE_06, game.getID(), serverModel);
        SendChatCommand command_07 = new SendChatCommand(BLUE_PLAYER_INDEX, MESSAGE_07, game.getID(), serverModel);
        SendChatCommand command_08 = new SendChatCommand(ORANGE_PLAYER_INDEX, MESSAGE_08, game.getID(), serverModel);
        SendChatCommand command_09 = new SendChatCommand(WHITE_PLAYER_INDEX, MESSAGE_09, game.getID(), serverModel);
        SendChatCommand command_10 = new SendChatCommand(RED_PLAYER_INDEX, MESSAGE_10, game.getID(), serverModel);
        SendChatCommand command_11 = new SendChatCommand(RED_PLAYER_INDEX, MESSAGE_11, game.getID(), serverModel);
        SendChatCommand command_12 = new SendChatCommand(BLUE_PLAYER_INDEX, MESSAGE_12, game.getID(), serverModel);
        SendChatCommand command_13 = new SendChatCommand(BLUE_PLAYER_INDEX, MESSAGE_13, game.getID(), serverModel);
        SendChatCommand command_14 = new SendChatCommand(ORANGE_PLAYER_INDEX, MESSAGE_14, game.getID(), serverModel);

        List<Message> chats = game.getChatLog().getChats();
        assert chats.size() == 0;

        command_01.executeCommand();

        assert chats.size() == 1;
        assert chats.get(0).getMessage().equals(MESSAGE_01);
        assert chats.get(0).getSource().equals(RED_PLAYER.getName());

        command_02.executeCommand();
        command_03.executeCommand();
        command_04.executeCommand();
        command_05.executeCommand();
        command_06.executeCommand();
        command_07.executeCommand();
        command_08.executeCommand();
        command_09.executeCommand();
        command_10.executeCommand();
        command_11.executeCommand();
        command_12.executeCommand();
        command_13.executeCommand();
        command_14.executeCommand();

        assert chats.size() == 14;
        assert chats.get(0).getMessage().equals(MESSAGE_01);
        assert chats.get(0).getSource().equals(RED_PLAYER.getName());
        assert chats.get(1).getMessage().equals(MESSAGE_02);
        assert chats.get(1).getSource().equals(BLUE_PLAYER.getName());
        assert chats.get(2).getMessage().equals(MESSAGE_03);
        assert chats.get(2).getSource().equals(WHITE_PLAYER.getName());
        assert chats.get(3).getMessage().equals(MESSAGE_04);
        assert chats.get(3).getSource().equals(RED_PLAYER.getName());
        assert chats.get(4).getMessage().equals(MESSAGE_05);
        assert chats.get(4).getSource().equals(ORANGE_PLAYER.getName());
        assert chats.get(5).getMessage().equals(MESSAGE_06);
        assert chats.get(5).getSource().equals(RED_PLAYER.getName());
        assert chats.get(6).getMessage().equals(MESSAGE_07);
        assert chats.get(6).getSource().equals(BLUE_PLAYER.getName());
        assert chats.get(7).getMessage().equals(MESSAGE_08);
        assert chats.get(7).getSource().equals(ORANGE_PLAYER.getName());
        assert chats.get(8).getMessage().equals(MESSAGE_09);
        assert chats.get(8).getSource().equals(WHITE_PLAYER.getName());
        assert chats.get(9).getMessage().equals(MESSAGE_10);
        assert chats.get(9).getSource().equals(RED_PLAYER.getName());
        assert chats.get(10).getMessage().equals(MESSAGE_11);
        assert chats.get(10).getSource().equals(RED_PLAYER.getName());
        assert chats.get(11).getMessage().equals(MESSAGE_12);
        assert chats.get(11).getSource().equals(BLUE_PLAYER.getName());
        assert chats.get(12).getMessage().equals(MESSAGE_13);
        assert chats.get(12).getSource().equals(BLUE_PLAYER.getName());
        assert chats.get(13).getMessage().equals(MESSAGE_14);
        assert chats.get(13).getSource().equals(ORANGE_PLAYER.getName());
    }
}
