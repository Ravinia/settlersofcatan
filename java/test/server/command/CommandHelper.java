package server.command;

import org.junit.Before;
import server.exceptions.CatanServerException;
import server.factory.AdvancedFactoryHelper;
import server.model.GameModel;
import server.model.GameModelImp;
import server.model.ServerModelImp;
import server.model.board.ServerBoard;
import server.model.game.PlayableGame;
import server.model.game.PregameGame;
import server.model.game.resources.ResourceHand;
import server.model.game.resources.ResourceHandImp;
import server.model.player.ServerUser;
import server.model.player.ServerUserImp;
import shared.definitions.CatanColor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lawrence
 */
public abstract class CommandHelper extends AdvancedFactoryHelper
{
    protected static final String RED_PLAYER_USERNAME = RED_PLAYER_NAME;
    protected static final String RED_PLAYER_PASSWORD = RED_PLAYER_USERNAME;
    protected static final String BLUE_PLAYER_USERNAME = BLUE_PLAYER_NAME;
    protected static final String BLUE_PLAYER_PASSWORD = BLUE_PLAYER_USERNAME;
    protected static final String ORANGE_PLAYER_USERNAME = ORANGE_PLAYER_NAME;
    protected static final String ORANGE_PLAYER_PASSWORD = ORANGE_PLAYER_USERNAME;
    protected static final String WHITE_PLAYER_USERNAME = WHITE_PLAYER_NAME;
    protected static final String WHITE_PLAYER_PASSWORD = WHITE_PLAYER_USERNAME;

    protected static final int RED_PLAYER_INDEX = 0;
    protected static final int BLUE_PLAYER_INDEX = 1;
    protected static final int ORANGE_PLAYER_INDEX = 2;
    protected static final int WHITE_PLAYER_INDEX = 3;

    protected ServerModelImp serverModel;

    protected ResourceHand FIVE_HAND = new ResourceHandImp(5,5,5,5,5);
    protected ResourceHand EMPTY_HAND = new ResourceHandImp();

    @Override
    @Before
    public void initialize() throws CatanServerException
    {
        //Setup Playable Game Model
        model = new GameModelImp(false, false, false, 1, "fun");
        PregameGame pre = (PregameGame)model.getGame();
        pre.addPlayer(RED_PLAYER, CatanColor.RED);
        pre.addPlayer(BLUE_PLAYER, CatanColor.BLUE);
        pre.addPlayer(ORANGE_PLAYER, CatanColor.ORANGE);
        pre.addPlayer(WHITE_PLAYER, CatanColor.WHITE);
        model.startGame();
        game = (PlayableGame) model.getGame();
        board = (ServerBoard) model.getBoard();

        List<GameModel> games = new ArrayList<>();
        games.add(model);

        serverModel = new ServerModelImp(games, getUsers());
    }

    //Helper Methods
    private static List<ServerUser> getUsers()
    {
        List<ServerUser> users = new ArrayList<>();
        users.add(new ServerUserImp(RED_PLAYER_USERNAME, RED_PLAYER_PASSWORD,0));
        users.add(new ServerUserImp(BLUE_PLAYER_USERNAME, BLUE_PLAYER_PASSWORD,1));
        users.add(new ServerUserImp(ORANGE_PLAYER_USERNAME, ORANGE_PLAYER_PASSWORD,2));
        users.add(new ServerUserImp(WHITE_PLAYER_USERNAME, WHITE_PLAYER_PASSWORD,3));
        return users;
    }
}
