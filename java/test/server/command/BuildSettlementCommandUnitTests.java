package server.command;

import org.junit.Test;
import server.command.moves.BuildSettlementCommand;
import server.model.game.resources.BuildPool;
import shared.exceptions.CatanException;

import static server.model.board.LocationConstants.*;

/**
 * @author Lawrence
 */
public class BuildSettlementCommandUnitTests extends CommandHelper
{
    @Test
    public void test_PlaceSettlement_free() throws CatanException
    {
        BuildSettlementCommand command = new BuildSettlementCommand(RED_PLAYER_INDEX,CENTER_NE_VERTEX,true,game.getID(),serverModel);

        assert board.getSettlements().size() == 0;
        assert game.getResourceHandOfPlayer(RED_PLAYER).equals(EMPTY_HAND);
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(new BuildPool());

        command.executeCommand();

        assert board.getSettlements().size() == 1;
        assert board.getSettlement(CENTER_NE_VERTEX).getPlayer().equals(RED_PLAYER);
        assert !board.getSettlement(CENTER_NE_VERTEX).isCity();

        assert game.getResourceHandOfPlayer(RED_PLAYER).equals(EMPTY_HAND);

        BuildPool pool = new BuildPool();
        pool.placeSettlement();
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(pool);
    }

    @Test
    public void test_PlaceSettlement_notFree() throws CatanException
    {
        BuildSettlementCommand command = new BuildSettlementCommand(RED_PLAYER_INDEX,CENTER_NE_VERTEX,false,game.getID(),serverModel);

        game.getResourceHandOfPlayer(RED_PLAYER).setWood(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setWheat(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setSheep(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setBrick(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setOre(5);

        assert board.getSettlements().size() == 0;
        assert game.getResourceHandOfPlayer(RED_PLAYER).equals(FIVE_HAND);
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(new BuildPool());

        command.executeCommand();

        assert board.getSettlements().size() == 1;
        assert board.getSettlement(CENTER_NE_VERTEX).getPlayer().equals(RED_PLAYER);
        assert !board.getSettlement(CENTER_NE_VERTEX).isCity();

        assert !game.getResourceHandOfPlayer(RED_PLAYER).equals(FIVE_HAND);
        assert game.getResourceHandOfPlayer(RED_PLAYER).getWood() == 4;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getBrick() == 4;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getSheep() == 4;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getWheat() == 4;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getOre() == 5;

        BuildPool pool = new BuildPool();
        pool.placeSettlement();
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(pool);
    }
}
