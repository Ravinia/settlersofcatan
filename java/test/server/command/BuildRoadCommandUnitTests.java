package server.command;

import org.junit.Test;
import server.command.moves.BuildRoadCommand;
import server.model.game.resources.BuildPool;
import shared.exceptions.CatanException;

import static server.model.board.LocationConstants.CENTER_N_EDGE;

/**
 * @author Lawrence
 */
public class BuildRoadCommandUnitTests extends CommandHelper
{
    @Test
    public void test_PlaceRoad_free() throws CatanException
    {
        BuildRoadCommand command = new BuildRoadCommand(RED_PLAYER_INDEX, CENTER_N_EDGE, true, game.getID(), serverModel);

        assert board.getRoads().size() == 0;
        assert game.getResourceHandOfPlayer(RED_PLAYER).equals(EMPTY_HAND);
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(new BuildPool());

        command.executeCommand();

        assert board.getRoads().size() == 1;
        assert board.getRoad(CENTER_N_EDGE).getPlayer().equals(RED_PLAYER);

        assert game.getResourceHandOfPlayer(RED_PLAYER).equals(EMPTY_HAND);

        BuildPool pool = new BuildPool();
        pool.placeRoad();
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(pool);
    }

    @Test
    public void test_PlaceRoad_notFree() throws CatanException
    {
        BuildRoadCommand command = new BuildRoadCommand(RED_PLAYER_INDEX, CENTER_N_EDGE, false, game.getID(), serverModel);

        game.getResourceHandOfPlayer(RED_PLAYER).setWood(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setWheat(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setSheep(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setBrick(5);
        game.getResourceHandOfPlayer(RED_PLAYER).setOre(5);

        assert board.getRoads().size() == 0;
        assert game.getResourceHandOfPlayer(RED_PLAYER).equals(FIVE_HAND);
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(new BuildPool());

        command.executeCommand();

        assert board.getRoads().size() == 1;
        assert board.getRoad(CENTER_N_EDGE).getPlayer().equals(RED_PLAYER);

        BuildPool pool = new BuildPool();
        pool.placeRoad();
        assert game.getBuildPoolOfPlayer(RED_PLAYER).equals(pool);

        assert !game.getResourceHandOfPlayer(RED_PLAYER).equals(FIVE_HAND);
        assert game.getResourceHandOfPlayer(RED_PLAYER).getWood() == 4;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getBrick() == 4;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getSheep() == 5;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getWheat() == 5;
        assert game.getResourceHandOfPlayer(RED_PLAYER).getOre() == 5;
    }
}
