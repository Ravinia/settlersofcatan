package server.command;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import server.command.games.LoadGameCommand;
import server.command.games.SaveGameCommand;
import server.exceptions.CatanServerException;
import server.model.GameModel;
import server.model.game.Game;

public class SaveLoadTest extends CommandHelper {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() 
	{
		try {
			GameModel originalModel = this.serverModel.getGame(1);
			SaveGameCommand sgc = new SaveGameCommand(1,"fun",this.serverModel);
			sgc.executeCommand();
			this.serverModel.removeGameModel(originalModel);
			this.serverModel.createGame(true, true, false, "fancypants");
			this.serverModel.createGame(true, true, false, "rewagas");
			this.serverModel.createGame(true, true, false, "fancypfdsasfdants");
			this.serverModel.createGame(true, true, false, "fancyaserepants");
			this.serverModel.createGame(true, true, false, "ghtgrt");
			for(Game g: this.serverModel.getGames())
			{
				assertTrue(!g.getName().equals("fun"));
			}
			LoadGameCommand lgc = new LoadGameCommand("fun", this.serverModel);
			lgc.executeCommand();
			
			assertTrue(originalModel.getID() == this.serverModel.getGame(1).getID());
			String temp = this.serverModel.getGame(5).getGame().getName();
			assertTrue(originalModel.getGame().getName().equals(this.serverModel.getGame(5).getGame().getName()));
			assertTrue(originalModel.getPlayerByIndex(0).getName().equals(this.serverModel.getGame(5).getPlayerByIndex(0).getName()));
			assertTrue(originalModel.getPlayerByIndex(1).getId() == this.serverModel.getGame(5).getPlayerByIndex(1).getId());
			assertTrue(originalModel.getPlayerByIndex(2).getIdentity().equals(this.serverModel.getGame(5).getPlayerByIndex(2).getIdentity()));
			assertTrue(originalModel.getPlayerByIndex(3).getName().equals(this.serverModel.getGame(5).getPlayerByIndex(3).getName()));
			
			assert(originalModel.getBoard().equals(this.serverModel.getGame(1).getBoard()));
			assert(this.serverModel.getGame(1).isSameVersion(originalModel.getGame().getGameVersion()));
		} catch (IOException | ClassNotFoundException | CatanServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
