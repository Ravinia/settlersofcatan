package client.deserializer;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;

import shared.definitions.PortType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import client.exceptions.DeserializationException;
import shared.exceptions.InvalidLocationException;
import client.model.ClientModel;
import client.model.game.Bank;
import client.model.game.PlayableGame;
import client.model.game.TradeOffer;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.Hex;
import client.model.game.board.immutable.ports.Port;
import shared.locations.PortLocation;
import client.model.game.chatlog.ChatLog;
import client.model.game.chatlog.Message;
import client.model.game.developmentcard.Development;
import client.model.game.player.BuildPool;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;
import shared.definitions.Phase;
import client.model.game.state.TurnTracker;
import client.proxyserver.MockProxyImp;
import client.serializer.Deserializer;
import client.serializer.DeserializerImp;


public class DeserializerTests
{
	MockProxyImp mockProxy;
	Logger logger;
	Deserializer deserializer;
	PlayableGame game;
	Board board;

    @Before
    public void init() throws JSONException, DeserializationException
    {
    	this.deserializer = new DeserializerImp();
    	this.mockProxy = new MockProxyImp();
    	this.logger = Logger.getLogger("DeserializerTestLogger");
    	this.loadClientFromModel();
    }
    
    public void loadClientFromModel() throws DeserializationException
    {
    	HttpResponse mockResponse = this.mockProxy.getGameModel();
		assertNotNull(mockResponse);
		logger.info(mockResponse.toString());
		logger.info(deserializer.getHttpBody(mockResponse));
    	ClientModel modelFromMock = this.deserializer.deserializeClientModel(mockResponse, mockProxy);
    	
    	assertNotNull(modelFromMock);
    	this.board = modelFromMock.getBoard();
    	assertNotNull(this.board);
    	this.game = modelFromMock.getGame();
    	assertNotNull(this.game);
    }
    
    @Test
    public void testHexAndNumberPlacement() throws InvalidLocationException
    {
    	Hex testHex = this.board.getHex(new HexLocation(0, -2));
		logger.info(testHex.toString());
    	assertNotNull(testHex);
    	assertEquals(testHex.getType().toString(), "WHEAT");
    	assertEquals(testHex.getToken().getValue(), 2);
    	
    	Hex testHex2 = this.board.getHex(new HexLocation(2,0));
    	logger.info(testHex2.toString());
    	assertNotNull(testHex2);
    	assertEquals(testHex2.getType().toString(), "ORE");
    	assertEquals(testHex2.getToken().getValue(), 5);
    }
    
    @Test
    public void testCityPlacement() throws InvalidLocationException
    {
    	VertexLocation loc = new VertexLocation(new HexLocation(0,1), VertexDirection.SouthEast);
    	loc = loc.getNormalizedLocation();
    	assertTrue(this.board.hasSettlement(loc));
    }
    
    @Test
    public void testPortPlacement() throws InvalidLocationException
    {
    	HexLocation hex = new HexLocation(2,1);
    	EdgeLocation edge = new EdgeLocation(hex, EdgeDirection.NorthWest);
    	VertexLocation west = new VertexLocation(hex, VertexDirection.West).getNormalizedLocation();
    	VertexLocation nw = new VertexLocation(hex, VertexDirection.NorthWest);
    	PortLocation loc = new PortLocation(edge, west, nw);
    	Port port = this.board.getPort(loc);
    	assertTrue(port.getPortType().equals(PortType.THREE));
    }

    @Test
    public void testSettlementPlacement() throws InvalidLocationException
    {
    	VertexLocation loc = new VertexLocation(new HexLocation(0,0), VertexDirection.SouthEast);
    	loc = loc.getNormalizedLocation();
    	assertTrue(this.board.hasSettlement(loc));
    }
    
    @Test
    public void testRoadPlacement() throws InvalidLocationException
    {
    	EdgeLocation loc = new EdgeLocation(new HexLocation(2,0), EdgeDirection.SouthEast);
    	loc = loc.getNormalizedLocation();
    	assertTrue(this.board.hasRoad(loc));
    }
    
    @Test 
    public void testRobberLocation()
    {
    	HexLocation robberLoc = this.board.getRobberLocation();
    	assertEquals(robberLoc.getX(), -1);
    	assertEquals(robberLoc.getY(), 2);
    }
        
    @Test
    public void testGetPlayer()
    {
    	assertNotNull(this.game.getPlayer(12));
    }
    @Test
    public void testPlayerResourceHand()
    {
    	Player player = this.game.getPlayer(12);
    	ResourceHand hand = player.getRecHand();
    	assertEquals(hand.getBrick(), 3);
    	assertEquals(hand.getOre(), 0);
    	assertEquals(hand.getWood(), 10);
    	assertEquals(hand.getSheep(), 4);
    	assertEquals(hand.getWheat(), 1);
    }
    
    @Test
    public void testNewDevCardsOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	Collection<Development> hand = player.getDevHand().getNonPlayableDevCards();
    	int yearOfPlenty = 0;
    	int monopoly = 0;
    	int soldier = 0;
    	int roadBuilding = 0;
    	int monument = 0;
    	
		for (Iterator<Development> iterator = hand.iterator(); iterator.hasNext();) 
		{
			Development card = (Development) iterator.next();
			switch(card.getDevCardType())
			{
				case MONOPOLY: monopoly++;
					break;
				case SOLDIER: soldier++;
					break;
				case YEAR_OF_PLENTY: yearOfPlenty++;
					break;
				case ROAD_BUILD: roadBuilding++;
					break;
				case MONUMENT: monument++;
					break;
			}
		}
		
		assertEquals(monopoly, 1);
		assertEquals(yearOfPlenty, 3);
		assertEquals(soldier, 0);
		assertEquals(roadBuilding, 4);
		assertEquals(monument, 0);
    }
    
    @Test
    public void testOldDevCardsOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	Collection<Development> hand = player.getDevHand().getAllPlayableCards();
    	int yearOfPlenty = 0;
    	int monopoly = 0;
    	int soldier = 0;
    	int roadBuilding = 0;
    	int monument = 0;
    	
		for (Iterator<Development> iterator = hand.iterator(); iterator.hasNext();) {
			Development card = (Development) iterator.next();
			
			switch(card.getDevCardType())
			{
				case MONOPOLY: monopoly++;
					break;
				case SOLDIER: soldier++;
					break;
				case YEAR_OF_PLENTY: yearOfPlenty++;
					break;
				case ROAD_BUILD: roadBuilding++;
					break;
				case MONUMENT: monument++;
					break;
			}
		}
		
		assertEquals(monopoly, 0);
		assertEquals(yearOfPlenty, 1);
		assertEquals(soldier, 3);
		assertEquals(roadBuilding, 1);
		assertEquals(monument, 1);
    	
    }
    
    @Test
    public void testBuildPoolOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	BuildPool buildPool = player.getBuildPool();
    	assertEquals(buildPool.getCities(), 4);
    	assertEquals(buildPool.getRoads(), 15);
    	assertEquals(buildPool.getSettlements(), 5);
    }
    
    @Test
    public void testVictoryPointsOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	assertEquals(player.getVictoryPoints(), 3);
    }
    
    @Test
    public void testSoldiersOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	assertEquals(player.getPlayedSoldiers(), 2);
    }
    
    @Test
    public void testMonumentsOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	assertEquals(player.getPlayedMonuments(), 1);
    }
    
    @Test
    public void testPlayedDevCardThisTurnOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	assertFalse(player.isPlayedDevCard());
    }
    
    @Test
    public void testHasDiscardedOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	assertTrue(player.isDiscarded());
    }
    
    @Test
    public void testIndexOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	assertEquals(player.getIndex(), 0);
    }
    
    @Test
    public void testNameOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	assertEquals(player.getName(), "JAM");
    }
    
    @Test
    public void testColorOfPlayer()
    {
    	Player player = this.game.getPlayer(12);
    	assertEquals(player.getColor().toString().toLowerCase(), "brown");
    }
    
    @Test
    public void testChat()
    {
    	ChatLog chatLog = this.game.getChatLog();
    	List<Message> chats = chatLog.getChats();
    	assertNotNull(chats);
    	Message message = chats.get(0);
    	assertNotNull(message);
    	assertEquals(message.getMessage(), "This is my message");
    	assertEquals(message.getSource(), "JAM");
    }
    
    @Test
    public void testLog()
    {
    	ChatLog chatLog = this.game.getChatLog();
    	List<Message> logs = chatLog.getLogs();
    	assertNotNull(logs);
    	Message message = logs.get(0);
    	assertNotNull(message);
    	assertEquals(message.getMessage(), "JAM upgraded to a city");
    	assertEquals(message.getSource(), "JAM");
    	
    }
    
    @Test
    public void testBank()
    {
    	Bank bank = this.game.getBank();
    	assertEquals(bank.getBrick(), 24);
    	assertEquals(bank.getOre(), 27);
    	assertEquals(bank.getWheat(), 26);
    	assertEquals(bank.getSheep(), 24);
    	assertEquals(bank.getWood(), 24);
    }
    
    @Test
    public void testVersionNumber()
    {
    	assertEquals(this.game.getVersion(), 3);
    }
    
    @Test
    public void testWinner()
    {
    	assertNull(this.game.getWinner());
    }
    
    @Test
    public void testTurnTracker()
    {
    	TurnTracker turnTracker = this.game.getTurnTracker();
    	
    	assertEquals(turnTracker.getIndexOfPlayerWhoseTurnItIs(), 0);
    	assertEquals(turnTracker.getIndexOfPlayerWithLargestArmy(), -1);
    	assertEquals(turnTracker.getIndexOfPlayerWithLongestRoad(), 2);
    	
    	assertEquals(turnTracker.getCurrentPhase(), Phase.ROUND1);
    	
    }
    
    @Test
    public void testOffer()
    {
    	TradeOffer offer = this.game.getOffer();
    	assertNotNull(offer);
    	assertEquals(offer.getSenderIndex(), 0);
    	assertEquals(offer.getReceiverIndex(),1);
    	
    	ResourceHand resources = offer.getResources();
    	assertEquals(resources.getBrick(), 0);
    	assertEquals(resources.getOre(), 1);
    	assertEquals(resources.getSheep(), 3);
    	assertEquals(resources.getWheat(), 1);
    	assertEquals(resources.getWood(), 0);
    	
    }
}
