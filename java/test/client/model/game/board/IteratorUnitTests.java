package client.model.game.board;

import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.GameBoard;
import client.model.game.board.immutable.Hex;
import org.junit.Before;
import org.junit.Test;
import shared.definitions.HexType;

/**
 * @author Lawrence
 */
public class IteratorUnitTests
{
    private Board board;

    @Before
    public void initialize()
    {
        board = new GameBoard(true);
    }

    @Test
    public void test_HexIterator()
    {
        int i = 0;
        for (Hex hex : board)
        {
            assert hex.getLocation() != null;
            assert hex.getType() != null;
            if (hex.getType() != HexType.DESERT && hex.getType() != HexType.WATER)
                assert hex.getToken() != null;

            i++;
        }
        assert i != 0;
    }
}
