/**
 * 
 */
package client.model.game.board;

import static org.junit.Assert.fail;

import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import client.model.game.board.movable.Road;
import client.model.game.board.immutable.*;
import client.model.game.board.movable.Settlement;
import org.junit.Before;
import org.junit.Test;

import static client.model.game.board.LocationConstants.*;
import static client.model.game.player.PlayerConstants.*;

import shared.definitions.HexType;
import shared.definitions.PieceType;
import shared.locations.*;

/**
 * Unit tests for the Board interface
 * @author Lawrence
 *
 */
public class BoardUnitTests
{
	Board board;

	@Before
	public void initialize()
	{
		board = new GameBoard(true);
	}

	@Test
	public void test_GetHex()
	{
        try
        {
            Hex hex = board.getHex(CENTER_HEX);
            if (hex == null)
                fail();
            assert hex.getLocation() == CENTER_HEX;
            assert hex.getType() == HexType.WHEAT;
            assert hex.getToken().getValue() == 11;
        }
        catch (Exception ignore)
        {
            fail();
        }
	}

    @Test
    public void test_GetEdge()
    {
        try
        {
            Edge edge = board.getEdge(CENTER_NE_EDGE);
            if (edge == null)
                fail();
        }
        catch (Exception ignore)
        {
            fail();
        }
    }

    @Test
    public void test_GetVertex()
    {
        try
        {
            Vertex vertex = board.getVertex(CENTER_NE_VERTEX);
            if (vertex == null)
                fail();
        }
        catch (Exception ignore)
        {
            fail();
        }
    }

    @Test
    public void test_GetRoad()
    {
        try
        {
            board.updateBoardObjects(getDefaultStartPieces());
            Road road = board.getRoad(CENTER_N_EDGE);
            if (road == null)
                fail();
            assert road.getType() == PieceType.ROAD;
            assert road.getLocation() == CENTER_N_EDGE;
            assert road.getPlayer() == RED_PLAYER;
        }
        catch (Exception ignore)
        {
            fail();
        }
    }

    @Test
    public void test_GetSettlement()
    {
        try
        {
            board.updateBoardObjects(getDefaultStartPieces());
            Settlement settlement = board.getSettlement(CENTER_NE_VERTEX);
            if (settlement == null)
                fail();
            assert settlement.getType() == PieceType.SETTLEMENT;
            assert settlement.getLocation() == CENTER_NE_VERTEX;
            assert settlement.getPlayer() == RED_PLAYER;
        }
        catch (Exception ignore)
        {
            fail();
        }
    }

    @Test (expected = InvalidLocationException.class)
    public void test_GetRoad_NoRoadAtLoc() throws InvalidLocationException
    {
        board.getRoad(CENTER_NE_EDGE);
    }

    @Test (expected = InvalidLocationException.class)
    public void test_GetRoad_InvalidLocation() throws InvalidLocationException, InvalidObjectTypeException
    {
        board.updateBoardObjects(getDefaultStartPieces());
        board.getRoad(CENTER_NE_EDGE);
    }

    @Test (expected = InvalidLocationException.class)
    public void test_GetSettlement_NoSettlementAtLoc() throws InvalidLocationException
    {
        board.getSettlement(CENTER_NE_VERTEX);
    }

    @Test (expected = InvalidLocationException.class)
    public void test_GetSettlement_InvalidLocation() throws InvalidLocationException, InvalidObjectTypeException
    {
        board.updateBoardObjects(getDefaultStartPieces());
        board.getSettlement(INVALID_VERTEX);
    }
	
	@Test
	public void test_GetToken()
	{
        try
        {
            Token token = board.getToken(CENTER_HEX);
            if (token == null)
                fail();
        }
        catch (Exception ignore)
        {
            fail();
        }
	}

    @Test (expected = InvalidLocationException.class)
    public void test_GetDesertToken_throwsException() throws InvalidLocationException
    {
        board.getToken(DESERT_HEX);
    }

    @Test (expected = InvalidLocationException.class)
    public void test_GetWaterToken_throwsException() throws InvalidLocationException
    {
        board.getToken(NORTH_WATER_HEX);
    }

    @Test
    public void test_GetNeighboringHex()
    {
        try
        {
            Hex hex = board.getNeighboringHex(board.getHex(CENTER_HEX), EdgeDirection.North);
            if (hex == null)
                fail();
            assert hex.getLocation().equals(HEX_0_N1);
            assert hex.getType() == HexType.WOOD;
            assert hex.getToken().getValue() == 4;
        }
        catch (InvalidLocationException e)
        {
            fail(e.getMessage());
        }
        catch (NullPointerException e)
        {
            fail("null object encountered");
        }
    }

    @Test
    public void test_GetDesertHex()
    {
        try
        {
            Hex hex = board.getHex(DESERT_HEX);
            assert hex.getType() == HexType.DESERT;
            assert hex.getToken() == null;
        }
        catch (InvalidLocationException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void test_GetWaterHex()
    {
        try
        {
            Hex hex = board.getHex(NORTH_WATER_HEX);
            assert hex.getType() == HexType.WATER;
            assert hex.getToken() == null;
        }
        catch (InvalidLocationException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void test_RobberStartsOnDesert()
    {
        assert board.getRobberLocation() == DESERT_HEX;
    }

    @Test
    public void test_UpdateBoardObjects()
    {
        try
        {
            board.getSettlement(CENTER_NE_VERTEX);
            //should throw an exception at this point
            fail();
        }
        catch (InvalidLocationException ignore)
        {}
        try
        {
            board.updateBoardObjects(getDefaultStartPieces());
            Settlement settlement = board.getSettlement(CENTER_NE_VERTEX);

            assert !settlement.isCity();
            assert settlement.getPlayer() == RED_PLAYER;
        }
        catch (Exception ignore)
        {
            //should NOT throw an exception during this part
            fail();
        }

    }
	
}
