package client.model.game.board;

import client.model.game.board.immutable.ValidLocationDistributor;
import org.junit.Test;
import shared.locations.HexLocation;

/**
 * Tests the ValidLocationDistributor
 * @author Lawrence
 */
public class DistributorUnitTests extends ValidLocationDistributor
{
    public DistributorUnitTests()
    {
        super();
    }

    @Test
    public void test_validHexLocations()
    {
        assert isValidHexLocation(new HexLocation(0, 0));
        assert isValidHexLocation(new HexLocation(0, 1));
        assert isValidHexLocation(new HexLocation(0, 2));
        assert isValidHexLocation(new HexLocation(0, -1));
        assert isValidHexLocation(new HexLocation(0, -2));
        assert isValidHexLocation(new HexLocation(0, -3));
        assert isValidHexLocation(new HexLocation(0, 3));
        assert isValidHexLocation(new HexLocation(-3, 1));
        assert isValidHexLocation(new HexLocation(3, -1));
        assert isValidHexLocation(new HexLocation(2, 0));

    }

    @Test
    public void test_invalidHexLocations()
    {
        assert !isValidHexLocation(new HexLocation(4, 3));
        assert !isValidHexLocation(new HexLocation(1, 3));
        assert !isValidHexLocation(new HexLocation(42, 3));
        assert !isValidHexLocation(new HexLocation(Integer.MAX_VALUE, Integer.MAX_VALUE));
        assert !isValidHexLocation(new HexLocation(Integer.MIN_VALUE, Integer.MIN_VALUE));
    }

    @Test
    public void test_validLandLocations()
    {
        assert isLandHex(new HexLocation(0, 0));
        assert isLandHex(new HexLocation(1, 1));
        assert isLandHex(new HexLocation(2, -2));
        assert isLandHex(new HexLocation(0, -2));
        assert isLandHex(new HexLocation(-2, 1));
        assert isLandHex(new HexLocation(-2, 0));
        assert isLandHex(new HexLocation(-2, 2));
        assert isLandHex(new HexLocation(-1, 2));
        assert isLandHex(new HexLocation(0, 2));
    }

    @Test
    public void test_invalidLandLocations_Water()
    {
        assert !isLandHex(new HexLocation(0, -3));
        assert !isLandHex(new HexLocation(1, -3));
        assert !isLandHex(new HexLocation(-2, -1));
        assert !isLandHex(new HexLocation(0, 3));
        assert !isLandHex(new HexLocation(1, 2));
        assert !isLandHex(new HexLocation(2, 1));
        assert !isLandHex(new HexLocation(-2, -1));
        assert !isLandHex(new HexLocation(-1, -2));
    }

    @Test
    public void test_invalidLandLocations_BadHex()
    {
        assert !isLandHex(new HexLocation(10, -3));
        assert !isLandHex(new HexLocation(31, -3));
        assert !isLandHex(new HexLocation(-2, -2));
        assert !isLandHex(new HexLocation(2, 2));
    }

    @Test
    public void test_validWaterLocations()
    {
        assert isWaterHex(new HexLocation(0, -3));
        assert isWaterHex(new HexLocation(1, -3));
        assert isWaterHex(new HexLocation(3, 0));
        assert isWaterHex(new HexLocation(1, 2));
        assert isWaterHex(new HexLocation(-3, 2));
        assert isWaterHex(new HexLocation(-3, 3));
        assert isWaterHex(new HexLocation(3, -3));
        assert isWaterHex(new HexLocation(-2, -1));
        assert isWaterHex(new HexLocation(2, 1));
    }

    @Test
    public void test_invalidWaterLocations_Land()
    {
        assert !isWaterHex(new HexLocation(0, 0));
        assert !isWaterHex(new HexLocation(1, 1));
        assert !isWaterHex(new HexLocation(2, -2));
        assert !isWaterHex(new HexLocation(0, -2));
        assert !isWaterHex(new HexLocation(-2, 1));
        assert !isWaterHex(new HexLocation(-2, 0));
        assert !isWaterHex(new HexLocation(-2, 2));
        assert !isWaterHex(new HexLocation(-1, 2));
        assert !isWaterHex(new HexLocation(0, 2));
    }

    @Test
    public void test_invalidWaterLocations_BadHex()
    {
        assert !isWaterHex(new HexLocation(10, -3));
        assert !isWaterHex(new HexLocation(31, -3));
        assert !isWaterHex(new HexLocation(-2, -2));
        assert !isWaterHex(new HexLocation(2, 2));
    }

    @Test
    public void test_noDuplicateEdges()
    {
        assert getEdges().size() == 72;
    }

    @Test
    public void test_noDuplicateVertices()
    {
        assert getVertices().size() == 54;
    }

}
