package client.model.game.permissions;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import shared.definitions.CatanColor;
import client.model.game.Deck;
import client.model.game.DeckImp;
import client.model.game.developmentcard.Development;
import client.model.game.player.BuildPool;
import client.model.game.player.DevCardHand;
import client.model.game.player.DevelopmentCardHand;
import client.model.game.player.GamePlayer;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;

public class BuyPermissionsTests
{
	private BuyPermissions buyPermissions;
	private ResourceHand rh;
	Collection<Development> newCards = new ArrayList<Development>();
	Collection<Development> oldCards = new ArrayList<Development>();
	DevelopmentCardHand devHand = new DevCardHand(oldCards, newCards);
	BuildPool buildPool = new BuildPool();
	BuildPool buildPoolEmpty = new BuildPool(0,0,0);
	Deck deck = new DeckImp(1,1,1,1,1);
	Deck deckEmpty = new DeckImp(0,0,0,0,0);
	private Player player = new GamePlayer("mainPlayer", 100, 0, devHand, rh, buildPool, CatanColor.BLUE, 
			false, 0, false, 0, 0);
	
	
	/**
	 * Each of these tests check these three cases:
	 * Exactly right amount of resources
	 * Not enough resources
	 * Too many resources
	 * Player doesn't have it in his build pool & server.model.player doesn't
	 * Deck is empty of devcards or deck has devcards
	 */
	@Test
	public void canBuyRoad()
    {
		rh = new ResourceHand(0,1,1,0,0);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(true, buyPermissions.canBuyRoad());
		
		rh = new ResourceHand(0,0,0,0,0);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(false, buyPermissions.canBuyRoad());
		
		rh = new ResourceHand(1,3,2,4,1);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(true, buyPermissions.canBuyRoad());
		
		player =new GamePlayer("mainPlayer", 100, 0, devHand, rh, buildPoolEmpty, CatanColor.BLUE, 
				false, 0, false, 0, 0);
		rh = new ResourceHand(5,5,5,5,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertTrue(!buyPermissions.canBuyRoad());
		
		player =new GamePlayer("mainPlayer", 100, 0, devHand, rh, buildPool, CatanColor.BLUE, 
				false, 0, false, 0, 0);
		rh = new ResourceHand(5,5,5,5,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertTrue(buyPermissions.canBuyRoad());
	}

	@Test
	public void canBuySettlement()
    {
		rh = new ResourceHand(1,1,1,0,1);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(true, buyPermissions.canBuySettlement());
		
		rh = new ResourceHand(0,0,0,0,0);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(false, buyPermissions.canBuySettlement());
		
		rh = new ResourceHand(1,3,2,4,1);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(true, buyPermissions.canBuySettlement());
		
		player =new GamePlayer("mainPlayer", 100, 0, devHand, rh, buildPoolEmpty, CatanColor.BLUE, 
				false, 0, false, 0, 0);
		rh = new ResourceHand(5,5,5,5,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertTrue(!buyPermissions.canBuySettlement());
		
		player =new GamePlayer("mainPlayer", 100, 0, devHand, rh, buildPool, CatanColor.BLUE, 
				false, 0, false, 0, 0);
		rh = new ResourceHand(5,5,5,5,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertTrue(buyPermissions.canBuySettlement());
	}

	@Test
	public void canBuyCity()
    {
		rh = new ResourceHand(0,0,0,3,2);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(true, buyPermissions.canBuyCity());
		
		rh = new ResourceHand(0,0,0,0,0);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(false, buyPermissions.canBuyCity());
		
		rh = new ResourceHand(1,2,2,4,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(true, buyPermissions.canBuyCity());
		
		player =new GamePlayer("mainPlayer", 100, 0, devHand, rh, buildPoolEmpty, CatanColor.BLUE, 
				false, 0, false, 0, 0);
		rh = new ResourceHand(5,5,5,5,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertTrue(!buyPermissions.canBuyCity());
		
		player =new GamePlayer("mainPlayer", 100, 0, devHand, rh, buildPool, CatanColor.BLUE, 
				false, 0, false, 0, 0);
		rh = new ResourceHand(5,5,5,5,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertTrue(buyPermissions.canBuyCity());
	}

	@Test
	public void canBuyDevCard()
    {
		
		rh = new ResourceHand(1,0,0,1,1);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(true, buyPermissions.canBuyDevCard());
		
		rh = new ResourceHand(0,0,0,0,0);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(false, buyPermissions.canBuyDevCard());
		
		rh = new ResourceHand(3,2,2,4,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertEquals(true, buyPermissions.canBuyDevCard());
		
		player =new GamePlayer("mainPlayer", 100, 0, devHand, rh, buildPool, CatanColor.BLUE, 
				false, 0, false, 0, 0);
		rh = new ResourceHand(5,5,5,5,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deckEmpty);
		assertTrue(!buyPermissions.canBuyDevCard());
		
		player =new GamePlayer("mainPlayer", 100, 0, devHand, rh, buildPool, CatanColor.BLUE, 
				false, 0, false, 0, 0);
		rh = new ResourceHand(5,5,5,5,5);
		buyPermissions = new BuyPermissionsImp(rh, player, deck);
		assertTrue(buyPermissions.canBuyDevCard());
	}
}