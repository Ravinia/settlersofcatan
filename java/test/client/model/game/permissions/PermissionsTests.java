package client.model.game.permissions;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.exceptions.InvalidLocationException;
import client.model.game.Bank;
import client.model.game.BankImp;
import client.model.game.PlayableGame;
import client.model.game.PlayableGameImp;
import client.model.game.board.LocationConstants;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.GameBoard;
import client.model.game.developmentcard.Development;
import client.model.game.developmentcard.Monopoly;
import client.model.game.developmentcard.MonopolyCard;
import client.model.game.developmentcard.Monument;
import client.model.game.developmentcard.MonumentCard;
import client.model.game.developmentcard.RoadBuilding;
import client.model.game.developmentcard.RoadBuildingCard;
import client.model.game.developmentcard.Soldier;
import client.model.game.developmentcard.SoldierCard;
import client.model.game.developmentcard.YearOfPlenty;
import client.model.game.developmentcard.YearOfPlentyCard;
import client.model.game.player.BuildPool;
import client.model.game.player.DevCardHand;
import client.model.game.player.DevelopmentCardHand;
import client.model.game.player.GamePlayer;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;
import shared.definitions.Phase;
import client.model.game.state.State;
import client.model.game.state.StateImp;
import client.model.game.state.TurnOrder;
import client.model.game.state.TurnOrderImp;

public class PermissionsTests
{
	Monument monument1 = new MonumentCard(false);
	Monopoly monopoly1 = new MonopolyCard(false);
	Soldier soldierCard1 = new SoldierCard(false);
	YearOfPlenty yearOfPlenty1 = new YearOfPlentyCard(false);
	RoadBuilding roadBuilding1 = new RoadBuildingCard(false);
	BuildPool buildPool = new BuildPool();
	ResourceHand recHand = new ResourceHand();
	Collection<Development> newCards = new ArrayList<Development>();
	Collection<Development> oldCards = new ArrayList<Development>();
	DevelopmentCardHand devHand = new DevCardHand(oldCards, newCards);
	PlayableGame game = new PlayableGameImp();
	Player mainPlayer = new GamePlayer("mainPlayer", 100, 0, devHand, recHand, buildPool, CatanColor.BLUE, 
			false, 0, false, 0, 0);
	Player otherPlayer = new GamePlayer("otherPlayer", 200, 1, devHand, recHand, buildPool, CatanColor.RED, 
			false, 0, false, 0, 0);
	Player placeHolder1 = new GamePlayer("placeHolder1", 300, 2, devHand, recHand, buildPool, CatanColor.ORANGE, 
			false, 0, false, 0, 0);
	Player placeHolder2 = new GamePlayer("placeHolder2", 400, 3, devHand, recHand, buildPool, CatanColor.WHITE, 
			false, 0, false, 0, 0);
	State state = new StateImp(Phase.WAIT_PHASE);
	TurnOrder turnOrder;
	Bank bankPlenty = new BankImp(5,5,5,5,5);
	Bank bankBroke = new BankImp();
	Bank bankOne = new BankImp(1,1,1,1,1);
    Board board = new GameBoard(true);
	Permissions permissions;

	@Before
	public void setUp() throws Exception 
	{
		board = new GameBoard(true);
		board.updateBoardObjects(LocationConstants.getDefaultStartPieces());
		ArrayList<Player> order= new ArrayList<Player>();
		order.add(mainPlayer); order.add(otherPlayer); order.add(placeHolder1); order.add(placeHolder2);
		turnOrder = new TurnOrderImp(order, mainPlayer.getIndex());
		
		mainPlayer.playedDevCard(false);
		permissions = new PermissionsImp(game, mainPlayer, turnOrder, bankPlenty, board);
		permissions.getOwnTurnOrder().setCurrentPhase(Phase.PLAYING);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	/**
	 * Testing correct phase control.
	 */
	@Test
	public void testPlayDevCardPhase() 
	{	
		permissions.getOwnTurnOrder().setCurrentPhase(Phase.WAIT_PHASE);
		permissions.getPlayer().getDevHand().addNewCard(monument1);
		assertTrue(!permissions.canPlayDevCard(monument1));
		
		permissions.getOwnTurnOrder().setCurrentPhase(Phase.PLAYING);
		assertTrue(permissions.canPlayDevCard(monument1));
		
		permissions.getOwnTurnOrder().setCurrentPlayer(otherPlayer.getIndex());
		assertTrue(!permissions.canPlayDevCard(monument1));
	}
	
	/**
	 * the server.model.player tries to play a card he doesn't have
	 */
	@Test
	public void testPlayDevCardNoCard()
	{
		permissions.getPlayer().getDevHand().addOldCard(soldierCard1);
		assertTrue(!permissions.canPlayDevCard(this.monument1));
		assertTrue(!permissions.canPlayDevCard(this.monopoly1));
	}
	
	/**
	 * The server.model.player tries to play a normal development card and a monument card after he has already
	 * played one during this turn.
	 */
	@Test
	public void playDevCardAlreadyPlayedOne()
	{
		permissions.getPlayer().playedDevCard(true);
		permissions.getPlayer().getDevHand().addOldCard(this.monopoly1);
		permissions.getPlayer().getDevHand().addNewCard(this.monument1);
		assertTrue(!permissions.canPlayDevCard(this.monopoly1));
		assertTrue(permissions.canPlayDevCard(this.monument1));
	}
	/**
	 * Playing a card when it is not old enough to be played
	 */
	@Test
	public void playDevCardThatIsTooNew()
	{
		permissions.getPlayer().getDevHand().addNewCard(monopoly1);
		assertTrue(!permissions.canPlayDevCard(this.monopoly1));
	}
	/**
	 * Tests if you can play year of plenty with bank not having enough resources.
	 * first test when bank is empty and server.model.player asks two resources of different types.
	 * second test when bank has one of each resource and server.model.player asks two of the same type.
	 * third test when bank has one of each resource and server.model.player asks two of different types.
	 * fourth test when bank has enough resources and server.model.player asks two of same type.
	 */
	@Test
	public void testCanPlayYearOfPlentyInsufficientBank() ///come back to this can't figure it now
	{
		this.yearOfPlenty1.pickResources(ResourceType.BRICK, ResourceType.ORE);
		permissions.setBank(bankBroke);
		permissions.getPlayer().getDevHand().addOldCard(yearOfPlenty1);
		assertTrue(!permissions.canPlayDevCard(yearOfPlenty1));
		
		this.yearOfPlenty1.pickResources(ResourceType.BRICK, ResourceType.BRICK);
		permissions.getPlayer().getDevHand().addOldCard(yearOfPlenty1);
		permissions.setBank(this.bankOne);
		assertTrue(!permissions.canPlayDevCard(yearOfPlenty1));
		
		this.yearOfPlenty1.pickResources(ResourceType.BRICK, ResourceType.SHEEP);
		permissions.getPlayer().getDevHand().addOldCard(yearOfPlenty1);
		permissions.setBank(this.bankOne);
		assertTrue(permissions.canPlayDevCard(yearOfPlenty1));
		
		this.yearOfPlenty1.pickResources(ResourceType.BRICK, ResourceType.BRICK);
		permissions.getPlayer().getDevHand().addOldCard(yearOfPlenty1);
		permissions.setBank(this.bankPlenty);
		assertTrue(permissions.canPlayDevCard(yearOfPlenty1));
		
	}

	/**
	 * Testing the canPlaySoldier development card
	 * 1.Target Player has no cards
	 * 2.robber to same location
	 * 3.robber to different location
	 * 
	 */
	@Test
	public void testCanPlaySoldier() 
	{
		soldierCard1.setDesiredLocation(new HexLocation(0,1));
		soldierCard1.setRobeeIndex(1);
		permissions.getPlayer().getDevHand().addOldCard(soldierCard1);
		assertTrue(!permissions.canPlayDevCard(soldierCard1));
		
		otherPlayer.getRecHand().addBrick(1);
		soldierCard1.setDesiredLocation(permissions.getBoard().getRobberLocation());
		assertTrue(!permissions.canPlayDevCard(soldierCard1));
		
		soldierCard1.setDesiredLocation(new HexLocation(0,1));
		assertTrue(permissions.canPlayDevCard(soldierCard1));
	}

	/**Testing if a server.model.player can play the roadbuilding development card
	 * 1.Both Roads valid & connected
	 * 2.Both Roads valid & not connected
	 * 3.Both roads invalid, connected to each other but not an owned road.
	 * 4.Both roads invalid, not connected to each other or an owned road.
	 * 5.Either road is placed in a place that has a road
	 * 6.Player doesn't have two roads in build pool.
	 */
	@Test
	public void testCanPlayRoadBuilding() //try to figure how to use board constants for this test...not currently working
	{
		try
        {
			roadBuilding1.pickRoadLocations(new EdgeLocation(new HexLocation(2,-1), EdgeDirection.North).getNormalizedLocation(), 
					new EdgeLocation(new HexLocation(2,-1), EdgeDirection.NorthWest).getNormalizedLocation());
			permissions.getPlayer().getDevHand().addOldCard(roadBuilding1);
			assertTrue(permissions.canPlayDevCard(roadBuilding1));
			
			roadBuilding1.pickRoadLocations(new EdgeLocation(new HexLocation(1,1), EdgeDirection.NorthEast).getNormalizedLocation(), 
					new EdgeLocation(new HexLocation(2,-1), EdgeDirection.North).getNormalizedLocation());
			permissions.getPlayer().getDevHand().addOldCard(roadBuilding1);
			assertTrue(permissions.canPlayDevCard(roadBuilding1));
			
			roadBuilding1.pickRoadLocations(new EdgeLocation(new HexLocation(0,-2), EdgeDirection.North).getNormalizedLocation(), 
					new EdgeLocation(new HexLocation(0,-2), EdgeDirection.NorthWest).getNormalizedLocation());
			permissions.getPlayer().getDevHand().addOldCard(roadBuilding1);
			assertTrue(!permissions.canPlayDevCard(roadBuilding1));
			
			roadBuilding1.pickRoadLocations(new EdgeLocation(new HexLocation(2,-2), EdgeDirection.NorthWest).getNormalizedLocation(), 
					new EdgeLocation(new HexLocation(0,2), EdgeDirection.North).getNormalizedLocation());
			permissions.getPlayer().getDevHand().addOldCard(roadBuilding1);
			assertTrue(!permissions.canPlayDevCard(roadBuilding1));
			
			roadBuilding1.pickRoadLocations(new EdgeLocation(new HexLocation(1,1), EdgeDirection.North).getNormalizedLocation(), 
					new EdgeLocation(new HexLocation(0,0), EdgeDirection.NorthWest).getNormalizedLocation());
			permissions.getPlayer().getDevHand().addOldCard(roadBuilding1);
			assertTrue(!permissions.canPlayDevCard(roadBuilding1));
			
			permissions.getPlayer().getBuildPool().setAll(0,0,0);
			roadBuilding1.pickRoadLocations(new EdgeLocation(new HexLocation(0,0), EdgeDirection.NorthEast).getNormalizedLocation(), 
					new EdgeLocation(new HexLocation(1,-1), EdgeDirection.NorthWest).getNormalizedLocation());
			permissions.getPlayer().getDevHand().addOldCard(roadBuilding1);
			assertTrue(!permissions.canPlayDevCard(roadBuilding1));
			
		}
        catch (InvalidLocationException e)
        {
			e.printStackTrace();
		}
	}

	/**Testing if a server.model.player can discard cards. Cases are:
	 * 1.not enough cards
	 * 2.wrong phase
	 * 3.both are correct to discard
	 */
	@Test
	public void testCanDiscardCards() 
	{
		permissions.getOwnTurnOrder().setCurrentPhase(Phase.DISCARDING);
		assertTrue(!permissions.canDiscardCards(permissions.getPlayer().getRecHand()));
		
		permissions.getOwnTurnOrder().setCurrentPhase(Phase.PLAYING);
		permissions.getPlayer().getRecHand().addBrick(10);
		assertTrue(!permissions.canDiscardCards(permissions.getPlayer().getRecHand()));
		
		permissions.getOwnTurnOrder().setCurrentPhase(Phase.DISCARDING);
		assertTrue(permissions.canDiscardCards(permissions.getPlayer().getRecHand()));
	}

	/**Testing whether or not a server.model.player can roll. Cases are:
	 * 1.wrong phase
	 * 2.wrong turn
	 * 3.both correct.
	 */
	@Test
	public void testCanRollNumber() 
	{
		permissions.getOwnTurnOrder().setCurrentPhase(Phase.PLAYING);
		assertTrue(!permissions.canRollNumber());
		
		permissions.getOwnTurnOrder().setCurrentPhase(Phase.ROLLING);
		permissions.getOwnTurnOrder().setCurrentPlayer(otherPlayer.getIndex());
		assertTrue(!permissions.canRollNumber());
		
		permissions.getOwnTurnOrder().setCurrentPlayer(mainPlayer.getIndex());
		assertTrue(permissions.canRollNumber());
	}

	/**Testing if a server.model.player can end their turn. Cases are:
	 * 1.phase is correct
	 * 2.wrong phase
	 * 
	 */
	@Test
	public void testCanFinishTurn() 
	{
		assertTrue(permissions.canFinishTurn());
		
		permissions.getOwnTurnOrder().setCurrentPhase(Phase.WAIT_PHASE);
		assertTrue(!permissions.canFinishTurn());
	}

}
