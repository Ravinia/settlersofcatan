package client.model.game.permissions;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import client.model.game.MaritimeTradeOffer;
import client.model.game.TradeOffer;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.GameBoard;
import client.model.game.developmentcard.Development;
import client.model.game.player.BuildPool;
import client.model.game.player.DevCardHand;
import client.model.game.player.DevelopmentCardHand;
import client.model.game.player.GamePlayer;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;
import shared.definitions.Phase;
import client.model.game.state.TurnOrder;
import client.model.game.state.TurnOrderImp;

public class TradePermissionsTest {

	
	TradeOffer offer1 = new TradeOffer(0,1, new ResourceHand(1,-1,0,0,0)); //Trade one brick for one sheep
	TradeOffer offer2 = new TradeOffer(0,1, new ResourceHand(1,0,0,0,0)); //give resources to server.model.player for nothing
	TradeOffer offer3 = new TradeOffer(0,1, new ResourceHand(3,3,3,2,-4)); //offer more resources than offerer has
	TradeOffer offer4 = new TradeOffer(1,0, new ResourceHand(1,1,0,0,-3));//accept trade when not enough resources
	TradeOffer offer5 = new TradeOffer(1,0, new ResourceHand(1,1,1,-2,0));//accept trade with enough resources
	TradeOffer offer6 = new TradeOffer(1,3, new ResourceHand(-1,-1,-1,-1,-1)); //could accept but it's not for active server.model.player
	
	ResourceHand recHand0 = new ResourceHand();
	ResourceHand recHand1 = new ResourceHand(1,1,1,1,1);
	ResourceHand recHand2 = new ResourceHand(2,2,2,2,2);
	ResourceHand recHand3 = new ResourceHand(3,3,3,3,3);
	ResourceHand recHand4 = new ResourceHand(4,4,4,4,4);
	
	Board board = new GameBoard(true);
	MaritimeTradeOffer maritimeOffer1 = new MaritimeTradeOffer(2,1,ResourceType.SHEEP, ResourceType.BRICK);
	MaritimeTradeOffer maritimeOffer2 = new MaritimeTradeOffer(8,2, ResourceType.SHEEP, ResourceType.BRICK);
	Collection<Development> newCards = new ArrayList<Development>();
	Collection<Development> oldCards = new ArrayList<Development>();
	DevelopmentCardHand devHand = new DevCardHand(oldCards, newCards);
	BuildPool buildPool = new BuildPool();
	Player mainPlayer = new GamePlayer("mainPlayer", 100, 0, devHand, recHand0, buildPool, CatanColor.BLUE, 
			false, 0, false, 0, 0);
	Player otherPlayer = new GamePlayer("otherPlayer", 200, 1, devHand, recHand0, buildPool, CatanColor.RED, 
			false, 0, false, 0, 0);
	Player placeHolder1 = new GamePlayer("placeHolder1", 300, 2, devHand, recHand0, buildPool, CatanColor.ORANGE, 
			false, 0, false, 0, 0);
	Player placeHolder2 = new GamePlayer("placeHolder2", 400, 3, devHand, recHand0, buildPool, CatanColor.WHITE, 
			false, 0, false, 0, 0);
	
	TurnOrder turnOrder;
	TradePermissions permissions;
	
	@Before
	public void setUp() throws Exception {
		
		ArrayList<Player> order= new ArrayList<Player>();
		order.add(mainPlayer); order.add(otherPlayer); order.add(placeHolder1); order.add(placeHolder2);
		turnOrder = new TurnOrderImp(order, mainPlayer.getIndex()); 
		turnOrder.setCurrentPlayer(mainPlayer.getIndex());
		turnOrder.setCurrentPhase(Phase.PLAYING);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	/**Testing if a server.model.player can offer a Domestic Trade. Cases are:
	 * 1.TradeOffer == null
	 * 2.successful simple trade
	 * 3.wrong phase
	 * 4.giving resources away for nothing
	 * 5.offer more resources than the server.model.player can
	 */
	@Test
	public void testCanOfferDomesticTrade() {
		
		otherPlayer.setRecHand(recHand4);
		
		permissions = new TradePermissionsImp(null, turnOrder, maritimeOffer1);
		assertTrue(!permissions.canOfferDomesticTrade());
		
		permissions = new TradePermissionsImp(offer1, turnOrder, maritimeOffer1);
		turnOrder.getThisPlayer().setRecHand(recHand1);
		assertTrue(permissions.canOfferDomesticTrade());
		
		turnOrder.setCurrentPhase(Phase.WAIT_PHASE);
		assertTrue(!permissions.canOfferDomesticTrade());
		
		turnOrder.setCurrentPhase(Phase.PLAYING);
		permissions = new TradePermissionsImp(offer2, turnOrder, maritimeOffer1);
		turnOrder.getThisPlayer().setRecHand(recHand4);
		assertTrue(!permissions.canOfferDomesticTrade());
		
		permissions = new TradePermissionsImp(offer3, turnOrder, maritimeOffer1);
		turnOrder.getThisPlayer().setRecHand(recHand2);
		assertTrue(!permissions.canOfferDomesticTrade());
		
		
	}

	/**Testing if a server.model.player can accept a trade
	 * 1.server.model.player has resources
	 * 2.server.model.player doesn't have the resources
	 * 3.trade not meant for this server.model.player
	 */
	@Test
	public void testCanAcceptTrade() {
		
		permissions = new TradePermissionsImp(offer5, turnOrder, maritimeOffer1);
		turnOrder.setCurrentPhase(Phase.WAIT_PHASE); //maybe need twice
		
		otherPlayer.setRecHand(recHand4);
		turnOrder.getThisPlayer().setRecHand(recHand4);
		assertTrue(permissions.canAcceptTrade());
		
		turnOrder.getThisPlayer().setRecHand(recHand2);
		permissions = new TradePermissionsImp(offer4, turnOrder, maritimeOffer1);
		assertTrue(!permissions.canAcceptTrade());
		
		permissions = new TradePermissionsImp(offer4, turnOrder, maritimeOffer1);
		assertTrue(!permissions.canAcceptTrade());
	}

	/**Testing if a server.model.player can inititiate maritime trade
	 * 1.server.model.player has resources
	 * 2.server.model.player doesn'thave the resources
	 */
	@Test
	public void testCanMaritimeTrade() {
		
		turnOrder.getThisPlayer().setRecHand(recHand4);
		permissions = new TradePermissionsImp(offer5, turnOrder, maritimeOffer1);
		assertTrue(permissions.canMaritimeTrade(maritimeOffer1));
		
		permissions = new TradePermissionsImp(offer5, turnOrder, maritimeOffer2);
		assertTrue(!permissions.canMaritimeTrade(maritimeOffer2));
	}

}