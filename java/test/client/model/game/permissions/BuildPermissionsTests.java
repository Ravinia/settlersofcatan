package client.model.game.permissions;

import static client.model.game.board.LocationConstants.getDefaultStartPieces;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.exceptions.InvalidObjectTypeException;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.GameBoard;
import client.model.game.board.movable.Placeable;
import client.model.game.board.movable.Road;
import client.model.game.board.LocationConstants;
import shared.definitions.Phase;
import client.model.game.state.State;
import client.model.game.state.StateImp;
import static client.model.game.player.PlayerConstants.*;

public class BuildPermissionsTests
{
	BuildPermissions buildPermissions;
	State state;
	Board board;
	
	//TODO: Add tests to check building permissions when in the wrong state?
	@Before
	public void init()
	{
		//Sets up the default game board
		board = new GameBoard(true);
		state = new StateImp();
		state.setPhase(Phase.PLAYING);
		try
        {
			board.updateBoardObjects(getDefaultStartPieces());
		}
        catch (InvalidObjectTypeException e)
        {
			fail();
		}
		buildPermissions = new BuildPermissionsImp(board, RED_PLAYER, state);
	}
	
	//Testing the canPlaceRoad method
	@Test
	public void roadPlacementTest()
	{
		
		//Placing a road where a road already exists
		assertEquals(false, buildPermissions.canPlaceRoad(new EdgeLocation(LocationConstants.CENTER_HEX, EdgeDirection.North)));
		assertEquals(false, buildPermissions.canPlaceRoad(new EdgeLocation(LocationConstants.HEX_N2_2, EdgeDirection.NorthEast)));
		
		//Placing a road on an empty space which is not adjacent to the building server.model.player's roads
		assertEquals(false, buildPermissions.canPlaceRoad(new EdgeLocation(LocationConstants.CENTER_HEX, EdgeDirection.South)));
		assertEquals(false, buildPermissions.canPlaceRoad(new EdgeLocation(LocationConstants.HEX_2_0, EdgeDirection.SouthEast)));
		assertEquals(false, buildPermissions.canPlaceRoad(new EdgeLocation(LocationConstants.HEX_N1_0, EdgeDirection.NorthWest)));
		
		//Placing a road next to another road owned by the building server.model.player
		//Note: all of these tests fail currently
		assertEquals(true, buildPermissions.canPlaceRoad(new EdgeLocation(LocationConstants.CENTER_HEX, EdgeDirection.NorthWest)));
		assertEquals(true, buildPermissions.canPlaceRoad(new EdgeLocation(LocationConstants.CENTER_HEX, EdgeDirection.NorthEast)));
		assertEquals(true, buildPermissions.canPlaceRoad(new EdgeLocation(LocationConstants.CENTER_HEX, EdgeDirection.SouthWest)));
		assertEquals(true, buildPermissions.canPlaceRoad(new EdgeLocation(LocationConstants.HEX_N2_1, EdgeDirection.NorthEast)));
		
		//System.out.println("Payday, you're a winner");
	}
	
	//Testing the canPlaceSettlement method
	@Test
	public void settlementPlacementTest()
	{
		//Placing a settlement where a settlement already exists
		assertEquals(false, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(0, 0), VertexDirection.NorthEast)));
		assertEquals(false, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(-1, 1), VertexDirection.NorthEast)));
		assertEquals(false, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(-1, 2), VertexDirection.NorthWest)));
		
		//Placing a settlement too near an existing settlement, with roads connecting it
		assertEquals(false, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(0, -1), VertexDirection.SouthWest)));
		assertEquals(false, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(-1, 0), VertexDirection.SouthWest)));
		
		//Placing a settlement at a proper distance, but without connecting roads
		assertEquals(false, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(0, 0), VertexDirection.SouthEast)));
		assertEquals(false, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(-1, 0), VertexDirection.West)));
		
		//Add connecting roads to allow for settlement placement
		List<Placeable> boardPieces = LocationConstants.getDefaultStartPieces();
		boardPieces.add(new Road(new EdgeLocation(LocationConstants.HEX_N1_1, EdgeDirection.NorthEast), RED_PLAYER));
		boardPieces.add(new Road(new EdgeLocation(LocationConstants.HEX_0_1, EdgeDirection.North), RED_PLAYER));
		boardPieces.add(new Road(new EdgeLocation(LocationConstants.HEX_N1_1, EdgeDirection.NorthWest), RED_PLAYER));
		boardPieces.add(new Road(new EdgeLocation(LocationConstants.HEX_N2_1, EdgeDirection.NorthEast), RED_PLAYER));
		try
        {
			board.updateBoardObjects(boardPieces);
		}
        catch (InvalidObjectTypeException e)
        {
			fail();
		}
		
		//Placing a settlement at a proper distance with connecting roads - should succeed
		assertEquals(true, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(0, 0), VertexDirection.SouthEast)));
		assertEquals(true, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(-2, 1), VertexDirection.NorthEast)));
		
		//Placing a settlement too near an existing settlement again
		assertEquals(false, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(-1, 1), VertexDirection.West)));
		assertEquals(false, buildPermissions.canPlaceSettlement(new VertexLocation(new HexLocation(0, 0), VertexDirection.SouthWest)));
	}
	
	//Testing the canPlaceCity method
	@Test
	public void cityPlacementTest()
	{
		//Placing a city on someone else's settlement
		assertEquals(false, buildPermissions.canPlaceCity(new VertexLocation(new HexLocation(-1, 2), VertexDirection.NorthWest)));
		assertEquals(false, buildPermissions.canPlaceCity(new VertexLocation(new HexLocation(2, -1), VertexDirection.NorthEast)));
		
		//Placing a city where no settlement exists
		assertEquals(false, buildPermissions.canPlaceCity(new VertexLocation(new HexLocation(-1, 0), VertexDirection.East)));
		assertEquals(false, buildPermissions.canPlaceCity(new VertexLocation(new HexLocation(0, 1), VertexDirection.NorthWest)));
		assertEquals(false, buildPermissions.canPlaceCity(new VertexLocation(new HexLocation(0, 2), VertexDirection.NorthWest)));
		
		//Placing a city on your own settlement - should succeed
		assertEquals(true, buildPermissions.canPlaceCity(new VertexLocation(new HexLocation(0, 0), VertexDirection.NorthEast)));
		assertEquals(true, buildPermissions.canPlaceCity(new VertexLocation(new HexLocation(-1, 1), VertexDirection.NorthEast)));
	}
}
