package client.proxyserver;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import client.model.game.PregameGame;
import client.model.game.PregameGameImp;

import org.apache.http.HttpResponse;
import org.junit.Test;

import client.model.game.player.ResourceHand;
import client.model.pregamemodel.user.Password;
import client.model.pregamemodel.user.Username;
import client.serializer.Deserializer;
import client.serializer.DeserializerImp;
import client.serializer.Serializer;
import client.serializer.SerializerImp;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;

/**
 * 
 * @author Jameson
 * 
 * The tests in this suite merely test that the communication between the proxy and the server is successful.
 * Each test is twofold: 
 * 1. Create a valid request to send to the server.
 * 2. Ensure that the server understood the request by checking its response code, or the response body if needed.
 */
public class ProxyServerTests
{

	
	Logger logger = Logger.getLogger("ServerProxyTestLogger");
	Deserializer deserializer = new DeserializerImp();
	Serializer serializer = new SerializerImp();
	ProxyServerImp proxyServer = new ProxyServerImp("localhost","8081", deserializer, serializer);

	String VERIFICATION_STRING = "correct command";
    
    /**
     * Sets up for testing by logging in as JAM, making a new game, and then joining it.
     */
    private void advancedSetup()
    {
    	Username username = new Username("JAM");
		Password password = new Password("jam");
		try {
			proxyServer.register(username, password);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		HttpResponse response = null;
		try {
			response = proxyServer.login(username, password);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		logger.info(response.toString());
		logger.info(deserializer.getHttpBody(response));
		assertEquals(200, response.getStatusLine().getStatusCode());
		PregameGame game = new PregameGameImp("Jammin Game", true, true, true);	
    	proxyServer.createGame(game);
		try {
			proxyServer.joinGame(3, CatanColor.BROWN);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    @Test
	public void register()
	{
    	SecureRandom random = new SecureRandom();
    	String name = new BigInteger(130, random).toString(32);

		Username username = new Username(name);
		Password password = new Password(name);
		HttpResponse response = null;
		try {
			response = proxyServer.register(username, password);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
		assertNotNull(response);
		logger.info(response.toString());
		logger.info(deserializer.getHttpBody(response));
		assertEquals(200, response.getStatusLine().getStatusCode());
	}
	
    @Test
	public void login()
	{
		Username username = new Username("JAM");
		Password password = new Password("jam");
		try {
			proxyServer.register(username, password);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
		HttpResponse response = null;
		try {
			response = proxyServer.login(username, password);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
		assertNotNull(response);
		logger.info(response.toString());
		logger.info(deserializer.getHttpBody(response));
		assertEquals(200, response.getStatusLine().getStatusCode());
	}
   
    @Test
	public void createGame()
	{
    	advancedSetup();
    	
    	PregameGame game = new PregameGameImp("Jammin Game", true, true, true);
    	
    	HttpResponse response = proxyServer.createGame(game);
		assertNotNull(response);
    	logger.info(response.toString());
    	logger.info(deserializer.getHttpBody(response));
		assertEquals(200, response.getStatusLine().getStatusCode());
	}
    
    @Test
    public void getGamesList()
	{
    	advancedSetup();
    	
		HttpResponse response = proxyServer.getGamesList();
		assertNotNull(response);
		logger.info(response.toString());
		logger.info(deserializer.getHttpBody(response));
		assertEquals(200, response.getStatusLine().getStatusCode());
	}

    @Test
	public void joinGame()
	{
    	advancedSetup();
    	
    	PregameGame game = new PregameGameImp("Bonsibi Game", true, true, true);
    	proxyServer.createGame(game);
    	
    	HttpResponse response = null;
		try {
			response = proxyServer.joinGame(3, CatanColor.BROWN);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
		
		assertNotNull(response);
    	logger.info(response.toString());
    	logger.info(deserializer.getHttpBody(response));
	}
    
    @Test
	public void getGameModel()
	{
    	advancedSetup();
    	
    	logger.info("REQUESTING GAME MODEL");
    	HttpResponse response = proxyServer.getGameModel();
		assertNotNull(response);
		logger.info(response.toString());
		logger.info(deserializer.getHttpBody(response));
		assertEquals(200, response.getStatusLine().getStatusCode());
	}

//	@Test
//	public void resetGame()
//	{
//    	advancedSetup();
//    	
//    	HttpResponse response = proxyServer.resetGame();
//		assertNotNull(response);
//		logger.info(response.toString());
//		logger.info(deserializer.getHttpBody(response));
//		assertEquals(200, response.getStatusLine().getStatusCode());
//	}

//	@Test
//	public void getGameCommands()
//	{
//    	advancedSetup();
//    	
//    	HttpResponse response = proxyServer.getGameCommands();
//		assertNotNull(response);
//		logger.info(response.toString());
//		logger.info(deserializer.getHttpBody(response));
//		assertEquals(200, response.getStatusLine().getStatusCode());
//	}
//
//	@Test
//	public void putGameCommands()
//	{
//    	advancedSetup();
//    	
//    	List<Command> commands = new ArrayList<Command>();
//    	Command command = null;
//		try
//        {
//			command = new CommandImp("Death");
//		}
//        catch (InvalidCommandException e)
//        {
//			e.printStackTrace();
//			assert false;
//		}
//    	commands.add(command);
//    	HttpResponse response = proxyServer.putGameCommands(commands);
//		assertNotNull(response);
//		logger.info(response.toString());
//		logger.info(deserializer.getHttpBody(response));
//		assertEquals(404, response.getStatusLine().getStatusCode());
//	}

//	@Test
//	public void getAIList()
//	{
//    	advancedSetup();
//    	
//    	HttpResponse response = proxyServer.getAIList();
//		assertNotNull(response);
//		logger.info(response.toString());
//		logger.info(deserializer.getHttpBody(response));
//		assertEquals(200, response.getStatusLine().getStatusCode());
//	}

//	@Test
//	public void addAI()
//	{
//    	advancedSetup();
//    	
//    	HttpResponse response = proxyServer.addAI("LARGEST_ARMY");
//		assertNotNull(response);
//		logger.info(response.toString());
//		logger.info(deserializer.getHttpBody(response));
//		assertEquals(200, response.getStatusLine().getStatusCode());
//	}

//	@Test
//	public void changeLogLevel()
//	{
//    	advancedSetup();
//    	
//    	HttpResponse response = proxyServer.changeLogLevel(LogLevel.FINE);
//		assertNotNull(response);
//		logger.info(response.toString());
//		logger.info(deserializer.getHttpBody(response));
//		assertEquals(200, response.getStatusLine().getStatusCode());
//	}

	@Test
	public void sendChat()
	{
    	advancedSetup();
    	
		HttpResponse response = proxyServer.sendChat(1, "hey");
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void acceptTrade()
	{
    	advancedSetup();
    	
		HttpResponse response = proxyServer.acceptTrade(1, true);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void discardCards()
	{
    	advancedSetup();
    	
    	ResourceHand rh = new ResourceHand();
    	HttpResponse response = proxyServer.discardCards(1, rh);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void rollNumber()
	{
    	advancedSetup();
    	
    	HttpResponse response = proxyServer.rollNumber(1, 4);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void buildRoad()
	{
    	advancedSetup();
    	
    	EdgeLocation el = new EdgeLocation(new HexLocation(1,1), EdgeDirection.North);
    	HttpResponse response = proxyServer.buildRoad(1, el, true);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void buildSettlement()
	{
    	advancedSetup();
    	
    	VertexLocation el = new VertexLocation(new HexLocation(1,1), VertexDirection.West);
    	HttpResponse response = proxyServer.buildSettlement(1, el, true);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void buildCity()
	{
    	advancedSetup();
    	
    	VertexLocation el = new VertexLocation(new HexLocation(1,1), VertexDirection.West);
    	HttpResponse response = proxyServer.buildCity(1, el);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void offerTrade()
	{
    	advancedSetup();
    	
    	Map<ResourceType, Integer> resources = new HashMap<ResourceType, Integer>();
    	resources.put(ResourceType.BRICK, 2);
    	resources.put(ResourceType.WHEAT, -1);
    	HttpResponse response = proxyServer.offerTrade(1, resources, 2);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void maritimeTrade()
	{
    	advancedSetup();
    	
    	HttpResponse response =
    			proxyServer.maritimeTrade(1, 2, ResourceType.BRICK, ResourceType.WHEAT);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void finishTurn()
	{
    	advancedSetup();
    	
		HttpResponse response = proxyServer.finishTurn(1);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void buyDevCard()
	{
    	advancedSetup();
    	
		HttpResponse response = proxyServer.finishTurn(1);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void playRoadBuilding()
	{
    	advancedSetup();
    	
		EdgeLocation el1 = new EdgeLocation(new HexLocation(1,1), EdgeDirection.North);
		EdgeLocation el2 = new EdgeLocation(new HexLocation(1,1), EdgeDirection.NorthEast);
    	HttpResponse response = proxyServer.playRoadBuilding(1, el1, el2);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void playYearOfPlenty()
	{
    	advancedSetup();
    	
		HttpResponse response =
    			proxyServer.playYearOfPlenty(1, ResourceType.BRICK, ResourceType.WHEAT);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void playSoldier()
	{
    	advancedSetup();
    	
    	HttpResponse response = proxyServer.playSoldier(1, new HexLocation(1,1), 3);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

	@Test
	public void playMonopoly()
	{
    	advancedSetup();
    	
		HttpResponse response = proxyServer.playMonopoly(1, ResourceType.BRICK);
		assertNotNull(response);
		logger.info(response.toString());
		String body = deserializer.getHttpBody(response);
		logger.info(body);
		boolean correct = body.contains(VERIFICATION_STRING) || body.contains("deck");
		assertEquals(true, correct);
	}

}
