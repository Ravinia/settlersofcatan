package client.poller;

import java.util.Timer;
import java.util.TimerTask;

import org.junit.Before;
import org.junit.Test;

public class PollerTests
{
	private static final int DELAY_TIME = 3;
	Timer timer;
	
	@Before
	public void init()
	{
		timer = new Timer();
	}
	
	@Test
	public void TimerTest()
	{
		timer.schedule(new TaskSchedule(), DELAY_TIME * 1000, DELAY_TIME * 1000);
	}
	
	static class TaskSchedule extends TimerTask
	{
		static int count = 0;
		@Override
		public void run()
        {
			if(count == 3)
			{
				System.out.println("life is hard");
				count  = 0;
			}
			else
            {
				System.out.println("whattup, yo");
				count++;
			}
		}
	}
}
