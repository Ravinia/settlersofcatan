package client.exceptions;

import shared.exceptions.CatanException;

/**
 * An exception thrown if a server.model.player attempts to buy a road, settlement, city, or
 * devcard when they don't have enough resources to do so.
 *  
 * @author Lawrence Thatcher
 *
 */
public class InsufficientResourcesException extends CatanException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9138334770391501800L;
	
}
