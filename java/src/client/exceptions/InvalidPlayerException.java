/**
 * 
 */
package client.exceptions;

import shared.exceptions.CatanException;

/**
 * Exception thrown when a server.model.player is the target of an action that is not valid on that server.model.player
 * @author Lawrence Thatcher
 *
 */
public class InvalidPlayerException extends CatanException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -115038762202269132L;

}
