package client.exceptions;

import shared.exceptions.CatanException;

public class BuyDevCardException extends CatanException
{
	private static final long serialVersionUID = 5347002214176276815L;

	public BuyDevCardException(String message)
    {
        super("Cannot buy dev card:\n" + message);
    }
	public BuyDevCardException(String message, Exception e)
    {
        super("Cannot buy dev card:\n" + message, e);
    }

}
