package client.exceptions;

import shared.exceptions.CatanException;

/**
 * Exception to be thrown internally when a controller should not be updated
 */
public class DoNotUpdateException extends CatanException
{
}
