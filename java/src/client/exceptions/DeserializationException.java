package client.exceptions;

import shared.exceptions.CatanException;

/**
 * Exception thrown when an error occurs in the deserialization process.
 * @author Lawrence
 */
public class DeserializationException extends CatanException
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 2075414956187901283L;

	public DeserializationException(Exception e)
    {
        super("An error occurred during the deserialization process:", e);
    }
}
