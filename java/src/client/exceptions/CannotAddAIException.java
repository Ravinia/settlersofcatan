package client.exceptions;

import shared.exceptions.CatanException;

public class CannotAddAIException  extends CatanException
{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5347002214176276815L;

	public CannotAddAIException(String message)
    {
        super("Cannot add this server.model.player:\n" + message);
    }
	public CannotAddAIException(String message, Exception e)
    {
        super("Cannot add this server.model.player:\n" + message, e);
    }
	
	
}
