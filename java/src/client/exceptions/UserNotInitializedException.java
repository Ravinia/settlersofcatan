package client.exceptions;

import shared.exceptions.NotInitializedException;

/**
 * Thrown when user not initialized in model
 * @author Lawrence
 */
public class UserNotInitializedException extends NotInitializedException
{
    public UserNotInitializedException()
    {
        super("The user has not yet been initialized. Make sure the user has logged in first.");
    }
}
