package client.exceptions;

import shared.exceptions.CatanException;

public class BuildRoadException extends CatanException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5347002214176276815L;

	public BuildRoadException(String message)
    {
        super("Cannot build road:\n" + message);
    }
	public BuildRoadException(String message, Exception e)
    {
        super("Cannot build road:\n" + message, e);
    }

}
