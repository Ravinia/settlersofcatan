package client.exceptions;

import client.proxyserver.ServerResponse;
import shared.exceptions.CatanException;

/**
 * thrown when you get a server response you weren't expecting
 * @author Lawrence
 */
public class UnexpectedResponseException extends CatanException
{
    public UnexpectedResponseException(ServerResponse expected, ServerResponse actual)
    {
        super("unexpected server response. Expected [" + expected.toString() + "]. Actual [" + actual.toString() + "]");
    }
}
