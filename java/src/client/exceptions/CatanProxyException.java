/**
 * 
 */
package client.exceptions;

import shared.exceptions.CatanException;

/**
 * Exception thrown when the server returns an unexpected result
 * 
 * @author Lawrence Thatcher
 *
 */
public class CatanProxyException extends CatanException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8023171231032254518L;

}
