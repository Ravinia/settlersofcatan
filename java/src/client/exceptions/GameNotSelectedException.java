package client.exceptions;

import shared.exceptions.CatanException;

/**
 * Thrown when a game object value is referenced but not game has been selected
 * @author Lawrence
 */
public class GameNotSelectedException extends CatanException
{
}
