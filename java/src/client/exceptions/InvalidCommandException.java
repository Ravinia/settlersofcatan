/**
 * 
 */
package client.exceptions;

import shared.exceptions.CatanException;

/**
 * Exception thrown when a server command is not valid
 * 
 * @author Lawrence Thatcher
 *
 */
public class InvalidCommandException extends CatanException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5441003077210497496L;

}
