package client.exceptions;

import shared.exceptions.CatanException;

/**
 * Exception thrown when defining or using an invalid development card operation or type.
 * @author Lawrence
 */
public class InvalidDevelopmentCardException extends CatanException
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 6392802545157840507L;

	public InvalidDevelopmentCardException(String card)
    {
        super("Card type [" + card + "] is not a known development card type.");
    }
}
