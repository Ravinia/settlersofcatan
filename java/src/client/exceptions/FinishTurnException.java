package client.exceptions;

import shared.exceptions.CatanException;

public class FinishTurnException extends CatanException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5347002214176276815L;

	public FinishTurnException(String message)
    {
        super("Cannot finish turn:\n" + message);
    }
	public FinishTurnException(String message, Exception e)
    {
        super("Cannot finish turn:\n" + message, e);
    }

}
