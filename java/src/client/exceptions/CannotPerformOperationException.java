package client.exceptions;

import shared.exceptions.CatanException;

/**
 * @author Lawrence
 */
public class CannotPerformOperationException extends CatanException
{
    public CannotPerformOperationException(String message)
    {
        super(message);
    }

    public CannotPerformOperationException(String message, Exception e)
    {
        super(message, e);
    }
}
