package client.exceptions;

import shared.exceptions.CatanException;

/**
 * The exception thrown on the login screen for bad login info.
 * 
 * @author Lawrence Thatcher
 *
 */
public class LoginException extends CatanException
{
	private static final long serialVersionUID = 801236612872585872L;

	public LoginException(Exception e)
    {
        super("Unable to Login: ",e);
    }
}
