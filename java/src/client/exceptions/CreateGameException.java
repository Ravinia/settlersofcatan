package client.exceptions;

import shared.exceptions.CatanException;

public class CreateGameException extends CatanException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5342504135463651057L;

	public CreateGameException(String message)
    {
        super(message);
    }
}
