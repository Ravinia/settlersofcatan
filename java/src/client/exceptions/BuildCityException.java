package client.exceptions;

import shared.exceptions.CatanException;

public class BuildCityException extends CatanException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5347002214176276815L;

	public BuildCityException(String message)
    {
        super("Cannot build city:\n" + message);
    }
	public BuildCityException(String message, Exception e)
    {
        super("Cannot build city:\n" + message, e);
    }

}