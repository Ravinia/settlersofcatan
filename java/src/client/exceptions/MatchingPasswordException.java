package client.exceptions;

import shared.exceptions.CatanException;

public class MatchingPasswordException extends CatanException
{
	private static final long serialVersionUID = -3059619502441435002L;
	
	public MatchingPasswordException()
    {
        super("The passwords you entered did not match.");
    }
}
