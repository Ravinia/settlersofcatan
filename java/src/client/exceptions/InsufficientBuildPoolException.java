/**
 * 
 */
package client.exceptions;

import shared.exceptions.CatanException;

/**
 * An exception thrown whenever the server.model.player attempts to buy a road, settlement, or city
 * when they  have already reached their limit of these placeables on the board.
 * @author Lawrence Thatcher
 *
 */
public class InsufficientBuildPoolException extends CatanException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6976963675788965450L;

}
