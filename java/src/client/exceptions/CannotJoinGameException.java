package client.exceptions;

import shared.exceptions.CatanException;

/**
 * The exception thrown to the user when either the game is full or
 * if the user possibly attempts to rejoin a game which they did not originally belong
 * @author Eric Hatch
 * @author Lawrence Thatcher
 */
public class CannotJoinGameException extends CatanException
{
	private static final long serialVersionUID = 5638670575632179023L;

    public CannotJoinGameException(String message)
    {
        super(message);
    }
}
