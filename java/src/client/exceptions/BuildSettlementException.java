package client.exceptions;

import shared.exceptions.CatanException;

public class BuildSettlementException extends CatanException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5347002214176276815L;

	public BuildSettlementException(String message)
    {
        super("Cannot build settlement:\n" + message);
    }
	public BuildSettlementException(String message, Exception e)
    {
        super("Cannot build settlement:\n" + message, e);
    }

}
