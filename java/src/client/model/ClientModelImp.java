package client.model;

import java.util.Observable;
import java.util.Observer;

import client.exceptions.DeserializationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.player.Cookie;
import shared.exceptions.NotInitializedException;
import client.exceptions.UserNotInitializedException;
import client.facade.BuildFacade;
import client.facade.BuildFacadeImp;
import client.facade.DevCardFacade;
import client.facade.DevCardFacadeImp;
import client.facade.GameFacade;
import client.facade.GameFacadeImp;
import client.facade.GameHub;
import client.facade.GameHubInfo;
import client.facade.LoginFacade;
import client.facade.LoginFacadeImp;
import client.facade.PlayerFacade;
import client.facade.PlayerFacadeImp;
import client.gui.join.JoinGameController;
import client.gui.join.JoinGameControllerImp;
import client.gui.map.MapController;
import client.gui.map.MapControllerImp;
import client.gui.map.state.DiscardingState;
import client.gui.map.state.MapState;
import client.gui.map.state.PlacingRoad1;
import client.gui.map.state.PlacingRoad2;
import client.gui.map.state.PlacingSettlement1;
import client.gui.map.state.PlacingSettlement2;
import client.gui.map.state.PlayingState;
import client.gui.map.state.RobbingState;
import client.gui.map.state.RollingState;
import client.gui.map.state.Setup1State;
import client.gui.map.state.Setup2State;
import client.gui.map.state.StateHolder;
import client.gui.map.state.WaitingState;
import client.model.game.PlayableGame;
import client.model.game.PlayableGameImp;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.GameBoard;
import client.model.game.permissions.Permissions;
import client.model.game.permissions.PermissionsImp;
import client.model.game.player.Player;
import shared.definitions.Phase;
import client.model.game.state.TurnTracker;
import client.model.pregamemodel.user.Password;
import client.model.pregamemodel.user.User;
import client.model.pregamemodel.user.UserImp;
import client.model.pregamemodel.user.Username;
import client.poller.Poller;
import client.proxyserver.ProxyServer;
import client.proxyserver.ProxyServerImp;
import client.serializer.Deserializer;
import client.serializer.DeserializerImp;

/**
 * The implementation of the Client Model
 * @author Lawrence
 */
public class ClientModelImp extends Observable implements ClientModel, Observer, StateHolder
{
    private PlayableGame game;
    private Board board;
    private MapState currentState = new WaitingState(this);
    private MapControllerImp mapController;
    
    private Poller poller;

    private ProxyServer proxy;
    private Deserializer deserializer;
    private GameFacade manager;
    private PlayerFacade playerFacade;

    public ClientModelImp()
    {
    	game = new PlayableGameImp();
        board = new GameBoard(false);
        deserializer = new DeserializerImp();
    }

    public ClientModelImp(ProxyServer proxy)
    {
        game = new PlayableGameImp();
        board = new GameBoard(false);

        this.proxy = proxy;
        this.deserializer = new DeserializerImp();
        this.manager = new GameFacadeImp(this.proxy, this.deserializer);
    }

    @Override
    public void initializePoller(JoinGameController jgc)
    {
        poller = new Poller(proxy, this, deserializer);
        poller.addObserver((JoinGameControllerImp)jgc);
    }

    @Override
    public User getLocalUser() throws UserNotInitializedException
    {
        try
        {
            ProxyServerImp p = (ProxyServerImp) proxy;
            Cookie c = p.getUserCookie();

            Username username = new Username(c.getPlayerName());
            Password password = new Password(c.getPassword());
            int id = c.getPlayerID();

            return new UserImp(username, password, id);
        }
        catch (NullPointerException ex)
        {
            throw new UserNotInitializedException();
        }
    }

    @Override
    public int getLocalUserID() throws UserNotInitializedException
    {
        return getLocalUser().getID();
    }

    @Override
    public PlayableGame getGame()
    {
        return game;
    }

    @Override
    public void setGame(PlayableGame game)
    {
        this.game = game;
    }

    @Override
    public Board getBoard()
    {
        return board;
    }

    @Override
    public void setBoard(Board board)
    {
        this.board = board;
    }

    @Override
    public ProxyServer getProxy() throws NotInitializedException
    {
        if (proxy == null)
            throw new NotInitializedException("the Proxy has not been initialized in this client model object");
        return proxy;
    }

    @Override
    public Deserializer getDeserializer()
    {
        if (deserializer != null)
            return deserializer;
        else
            return new DeserializerImp();
    }

    @Override
    public GameHub getGameHub()
    {
        try
        {
            return new GameHubInfo(this);
        }
        catch (NotInitializedException e)
        {
            return null;
        }
    }

    @Override
    public LoginFacade getLoginFacade()
    {
        try
        {
            return new LoginFacadeImp(this);
        }
        catch (NotInitializedException e)
        {
            return null;
        }
    }

    @Override
    public void updateModelFromServer() throws NotInitializedException, DeserializationException, InvalidObjectTypeException
    {
        if (manager == null)
            throw new NotInitializedException("the Proxy has not been initialized in this client model object");

        ClientModel newModel = manager.getGameModel();
        updateModel(newModel);
    }
    
    @Override
    public void updateFromPoller()
    {
    	this.setChanged();
    	notifyObservers();
    }

    @Override
    public void updateModel(ClientModel model) throws InvalidObjectTypeException
    {
        game.updateGame(model.getGame());
        board.updateBoard(model.getBoard());
        this.setChanged();
        notifyObservers();
    }

    @Override
    public void notifyObservers()
    {
        updateCurrentState();
        super.notifyObservers();
    }

    @Override
    public void update(Observable o, Object arg)
    {
        notifyObservers();
    }

	@Override
	public PlayerFacade getPlayerFacade()
    {
		if(playerFacade != null)
			return playerFacade;
		
		try
        {
			playerFacade = new PlayerFacadeImp(this);
			return playerFacade;
		}
        catch (NotInitializedException e)
        {
			return null;
		}
	}

	@Override
	public Permissions getPermissions()
    {
		return new PermissionsImp(this.game, this.game.getMyPlayer(), this.game.getTurnOrder(), this.game.getBank(), this.board);
	}

    @Override
    public MapState getCurrentState()
    {
        return currentState;
    }

    @Override
    public void updateCurrentState()
    {
        try
        {
            TurnTracker tracker = game.getTurnTracker();
            Player me = game.getMyPlayer();
            boolean myTurn = (tracker.getIndexOfPlayerWhoseTurnItIs() == me.getIndex());
            Phase phase = tracker.getCurrentPhase();
            
            if (!myTurn && phase != Phase.DISCARDING)
                currentState = new WaitingState(this);
            else
            {
                switch (phase)
                {
                    case ROUND1:
                    	if(me.getBuildPool().getRoads() == 15)
                    	{
                    		setState(new PlacingRoad1(this, mapController));
                    	}
                    	else if(me.getBuildPool().getRoads() == 14 && me.getBuildPool().getSettlements() == 5)
                    	{
                    		setState(new PlacingSettlement1(this, mapController));
                    	}
                    	else
                    	{
                    		setState(new Setup1State(this, mapController));
                    	}
                        break;
                    case ROUND2:
                        if(me.getBuildPool().getRoads() == 14)
                        {
                        	setState(new PlacingRoad2(this, mapController));
                        }
                        else if(me.getBuildPool().getRoads() == 13 && me.getBuildPool().getSettlements() == 4)
                        {
                        	setState(new PlacingSettlement2(this, mapController));
                        }
                        else
                        {
                        	setState(new Setup2State(this, mapController));
                        }
                        break;
                    case DISCARDING:
                        setState(new DiscardingState(this));
                        break;
                    case PLAYING:
                        setState(new PlayingState(this, mapController));
                        break;
                    case ROBBING:
                        setState(new RobbingState(this, mapController));
                        break;
                    case ROLLING:
                        setState(new RollingState(this));
                        break;
                    default:
                        setState(new WaitingState(this));
                        break;
                }
            }
        }
        catch (NullPointerException e)
        {
            //Do nothing - don't change the state
            e.printStackTrace();
        }
    }

    private void setState(MapState newState)
    {
        if (!(currentState.getClass().isInstance(newState)))
        {
            currentState = newState; //set current state to the newly created state
        }
        //else keep current state the same
    }
    
    public BuildFacade getBuildFacade()
    {
    	return new BuildFacadeImp(this.proxy, this.game.getMyPlayer(), this);
    }
    
    public DevCardFacade getDevCardFacade()
    {
    	return new DevCardFacadeImp(this);
    }
    
    public void setMapController(MapController controller)
    {
    	this.mapController = (MapControllerImp) controller;
    }
}
