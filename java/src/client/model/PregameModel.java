package client.model;

import client.proxyserver.ProxyServer;
import client.serializer.Deserializer;
import client.serializer.Serializer;

/**
 * Later will become the primary data holder for things having to do with the pregame setup
 * Objects like games and other pregame error checking
 * @author Eric
 *@author Lawrence
 */
public interface PregameModel
{
    /**
     * Gets the proxy server
     * @return the proxy server
     */
    public ProxyServer getProxy();

    /**
     * Gets the deserializer
     * @return the deserializer
     */
    public Deserializer getDeserializer();

    /**
     * Gets the deserializer
     * @return the deserializer
     */
    public Serializer getSerializer();
}
