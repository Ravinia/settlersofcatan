package client.model;

import client.exceptions.DeserializationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.NotInitializedException;
import client.exceptions.UserNotInitializedException;
import client.facade.BuildFacade;
import client.facade.DevCardFacade;
import client.facade.GameHub;
import client.facade.LoginFacade;
import client.facade.PlayerFacade;
import client.gui.join.JoinGameController;
import client.gui.map.MapController;
import client.gui.map.state.StateHolder;
import client.model.game.PlayableGame;
import client.model.game.board.immutable.Board;
import client.model.game.permissions.Permissions;
import client.model.pregamemodel.user.User;
import client.proxyserver.ProxyServer;
import client.serializer.Deserializer;

/**
 * Later this will be The primary data holder for the client's higher data objects.
 * Objects like the Game, and sub-models.
 * @author Eric
 * @author Lawrence
 */
public interface ClientModel extends StateHolder
{
    /**
     * Gets the local user
     * @return the local user
     */
    @Deprecated
    public User getLocalUser() throws UserNotInitializedException;

    /**
     * Gets the ID of the local user
     * @return the ID
     * @throws UserNotInitializedException
     */
    @Deprecated
    public int getLocalUserID() throws UserNotInitializedException;

    /**
     * Gets the game object.
     * @return the game object
     */
    public PlayableGame getGame();

    /**
     * Sets the game object
     * @param game the game object
     */
    public void setGame(PlayableGame game);

    /**
     * Initializes and starts the Poller
     */
    public void initializePoller(JoinGameController jgc);

    /**
     * Gets the board
     * @return the board object
     */
    public Board getBoard();

    /**
     * Sets the Board object
     * @param board the board object
     */
    public void setBoard(Board board);

    /**
     * Gets the Proxy reference
     * @return the proxy
     */
    public ProxyServer getProxy() throws NotInitializedException;

    /**
     * Gets the deserializer
     * @return the deserializer
     */
    public Deserializer getDeserializer();

    /**
     * Creates a Game Hub facade based on the info in the model
     * @return the game hub facade
     */
    public GameHub getGameHub();

    /**
     * Creates a Login Facade based on the info in the model
     * @return the login facade
     */
    public LoginFacade getLoginFacade();
    
    /**
     * Creates a server.model.player facade based on the info in the model.
     * @return the server.model.player facade
     */
    public PlayerFacade getPlayerFacade();

    /**
     * Retrieves the latest version of the model from the server and internally updates its information.
     */
    public void updateModelFromServer() throws NotInitializedException, DeserializationException, InvalidObjectTypeException;

    /**
     * Updates the model with new model info, and merges them together
     * @param model a ClientModel object containing all of the new model info
     */
    public void updateModel(ClientModel model) throws InvalidObjectTypeException;
    
    public Permissions getPermissions();

	public void updateFromPoller();
	
	public BuildFacade getBuildFacade();
	
	public DevCardFacade getDevCardFacade();
	
	public void setMapController(MapController controller);
}
