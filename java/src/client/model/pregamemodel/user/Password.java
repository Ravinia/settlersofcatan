package client.model.pregamemodel.user;

/**
 * A place to later hold passwords and provide verification on it being
 * a valid password.
 * 
 * @author eric
 * @author lawrence
 */
public class Password {
	
	private String password;
	
	/**
	 * A default constructor detailing that a String
	 * must exist for a password object to be created
	 * 
	 * @param password
	 */
	public Password(String password)
    {
		setPassword(password);
	}
	
	/**
	 * @return the password
	 */
	public String getPassword()
    {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
    {
		this.password = password;
	}

	/**
	 * checks whether it is a valid password
	 * 
	 * @return  true/false
	 */
	static boolean isValid()
    {
		return false;
	}
}
