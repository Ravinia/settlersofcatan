package client.model.pregamemodel.user;

/**
 * mainly a receptacle for later storage of username and password
 * and the verification of the combination.
 * 
 * @author Eric
 * @auhtor Lawrence
 */
public interface User 
{
	/**
	 * @return the username
	 */
	public Username getUsername();
	/**
	 * @param username the username to set
	 */
	public void setUsername(Username username);
	/**
	 * @return the password
	 */
	public Password getPassword();
	/**
	 * @param password the password to set
	 */
	public void setPassword(Password password);

    /**
     * @return the User's ID
     */
    public int getID();

    /**
     * Gets the user's name
     * @return the name
     */
    public String getName();
}
