package client.model.pregamemodel.user;

/**
 * A place to later hold passwords and provide username
 * validity checks.
 * 
 * @author eric
 * @author lawrence
 */
public class Username
{
	
	private String username;
	
	
	/**
	 * default constructor stating that a User object must have
	 * a string to be created
	 * 
	 * @param username
	 */
	public Username(String username)
    {
		setUsername(username);
	}
	
	/**
	 * checks whether this is a valid username
	 * 
	 * @param name
	 * @return
	 */
	static boolean isValid(String name)
	{
		return false;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername()
    {
		return username;
	}
	
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username)
    {
		this.username = username;
	}
	
}
