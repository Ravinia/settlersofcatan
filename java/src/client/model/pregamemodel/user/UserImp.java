package client.model.pregamemodel.user;

public class UserImp implements User
{
	private Username username;
	private Password password;
    private int id;
	
	public UserImp(Username username, Password password, int id)
	{
		this.username = username;
		this.password = password;
        this.id = id;
	}
	
	@Override
	public Username getUsername()
    {
		return username;
	}

	@Override
	public void setUsername(Username username)
    {
		this.username = username;
	}

	@Override
	public Password getPassword()
    {
		return password;
	}

	@Override
	public void setPassword(Password password)
    {
		this.password = password;
	}

    @Override
    public int getID()
    {
        return id;
    }

    @Override
    public String getName()
    {
        return username.getUsername();
    }

}
