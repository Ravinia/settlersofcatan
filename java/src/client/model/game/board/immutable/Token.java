/**
 * 
 */
package client.model.game.board.immutable;

/**
 * @author Lawrence
 *
 */
public class Token
{
    private int tokenValue;

    public Token(int tokenValue)
    {
        this.tokenValue = tokenValue;
    }

	/**
	 * Gets the numerical value on this token
	 * @return number value (2 - 12)
	 */
	public int getValue()
    {
        return tokenValue;
    }
}
