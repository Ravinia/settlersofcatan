package client.model.game.board.immutable;

import shared.exceptions.BoardAlreadyInitializedException;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import client.model.game.board.immutable.ports.Port;
import shared.locations.PortLocation;
import client.model.game.board.movable.Placeable;
import client.model.game.board.movable.Road;
import client.model.game.board.movable.Settlement;
import client.model.game.player.Player;
import shared.definitions.PortType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;

import java.util.Collection;


/**
 * The Board stores all the information for physical objects and their current placement in
 * the game.
 * 
 * Implemented by GameBoard
 * 
 * @author Eric
 *@author Lawrence
 */
public interface Board extends Iterable<Hex>
{
    public Collection<Road> getRoads();

    public Collection<Settlement> getSettlements();

    /**
     * Initializes the board with all the hexes and ports
     * @param boardPieces a collection of Immutable board pieces objects containing
     *                    all of the Hexes and Ports
     */
    public void initializeBoard(Collection<Immutable> boardPieces)
            throws BoardAlreadyInitializedException, InvalidLocationException, InvalidObjectTypeException;

	/**
	 * Retrieves the hex at a specified location
	 * @param hex the HexLocation object that specifies the unique location of the hex
	 * @return the Hex object
	 */
	public Hex getHex(HexLocation hex) throws InvalidLocationException;
	
	/**
	 * Retrieves a unique edge at a specified location.
     * Does not accept water edges or edges outside the map
	 * @param edge the location of the edge to get
	 * @return the Edge object
	 */
	public Edge getEdge(EdgeLocation edge) throws InvalidLocationException;
	
	/**
	 * Retrieves a unique vertex at a specified location
     * Does not accept water vertices or vertices outside of the map
	 * @param vertex the location of the vertex to get
	 * @return the Vertex object
	 */
	public Vertex getVertex(VertexLocation vertex) throws InvalidLocationException;
	
	/**
	 * Retrieves the token for a specified hex
	 * @param hex the hex to get the token of
	 * @return the Token object
	 */
	public Token getToken(HexLocation hex) throws InvalidLocationException;

    /**
     * Whether or not there is a settlement on this vertex
     * @param vertex the location to look for a settlement
     * @return true if there is, false otherwise
     */
    public boolean hasSettlement(VertexLocation vertex);

	/**
	 * Retrieves a settlement at a specified vertex
	 * @param vertex the vertex where the settlement should be
	 * @return the Settlement object
	 */
	public Settlement getSettlement(VertexLocation vertex) throws InvalidLocationException;

    /**
     * Whether or not there is a road placed on this edge
     * @param edge the edge where the road should be
     * @return true if there is, false otherwise
     */
    public boolean hasRoad(EdgeLocation edge);

	/**
	 * Retrieves a road at a specified edge
	 * @param edge the edge where the road should be
	 * @return the Road object
	 */
	public Road getRoad(EdgeLocation edge) throws InvalidLocationException;
	
	/**
	 * Returns the specified neighbor of a hex
	 * @param hex the hex
	 * @param direction the direction of the neighbor
	 * @return the neighboring hex
	 */
	public Hex getNeighboringHex(Hex hex, EdgeDirection direction) throws InvalidLocationException;

    /**
     * Updates all the movable board pieces on the board
     * @param boardPieces a collection of pieces that represent the current state of the board
     */
    public void updateBoardObjects(Collection<Placeable> boardPieces) throws InvalidObjectTypeException;

    /**
     * Returns the hex location of the hex that the robber is at
     * @return location of the robber
     */
    public HexLocation getRobberLocation();

    /**
     * Checks whether the given location is a valid land hex
     * @param location the hex location to test
     * @return true if it is, false otherwise
     */
    public boolean isLandHex(HexLocation location);

    /**
     * Gets all the ports a given server.model.player is on
     * @param player the server.model.player
     * @return collection of port types
     * @throws InvalidLocationException
     */
    public Collection<PortType> getPlayersPorts(Player player) throws InvalidLocationException;


    /**
     * Updates the board with the new information
     * @param board the updated board
     */
    public void updateBoard(Board board) throws InvalidObjectTypeException;
    
    /**
     * Gets the specified port
     * @param port the location of the port to get
     * @return the port
     * @throws InvalidLocationException
     */
    public Port getPort(PortLocation port) throws InvalidLocationException;

    public Collection<Port> getPorts() throws InvalidLocationException;

}
