package client.model.game.board.immutable;

import shared.exceptions.InvalidLocationException;
import client.model.game.board.movable.Road;
import client.model.game.player.Player;
import shared.locations.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A place for the later storage of data pertaining to an edge---mainly roads.
 * 
 * @author Eric
 * @author Lawrence
 */
public class Edge  extends AdjacentEdgeFinder
{
    private EdgeLocation location;

    /**
     * A valid edge on a game board
     * @param location edge location of the edge
     * @param board the parent board that the edge is on
     */
    public Edge(EdgeLocation location, Board board)
    {
        super(board);
        this.location = location;
    }

    public boolean adjacentToSettlement() throws InvalidLocationException
    {
        List<Vertex> neighbors = getTwoAdjacentVertices();
        for (Vertex vertex : neighbors)
        {
            if (vertex.hasSettlement())
                return true;
        }
        return false;
    }

    /**
     * Whether or not the specified server.model.player owns a road on any of the edges adjacent to this edge
     * @param player the server.model.player
     * @return true if they do, false otherwise
     */
    public boolean ownsAnAdjacentRoad(Player player)
    {
        Collection<Edge> edges = getAdjacentEdges();
        for (Edge edge : edges)
        {
            if (edge.hasRoad(player))
                return true;
        }
        return false;
    }

    /**
     * Checks to see if a given edge location is adjacent to the current edge
     * @param location edge location of the edge
     * @return true if the specified location is adjacent to this edge, false otherwise
     * @throws InvalidLocationException
     */
    public boolean isAdjacentToEdge(EdgeLocation location) throws InvalidLocationException
    {
        Edge neighbor = board.getEdge(location);
        Collection<Edge> edges = neighbor.getAdjacentEdges();
        //iterate through all of the neighbors of the proposed edge, and if any of them are this edge, return true
        for (Edge edge : edges)
        {
            if (edge.getLocation().equals(this.location))
                return true;
        }
        return false;
    }

    //Helper Methods

    /**
     * Returns true if the specified server.model.player owns a road on this edge
     * @param player the server.model.player
     * @return false if there is no road on this location, or if there is a road, but the road does not belong to the server.model.player
     */
    protected boolean hasRoad(Player player)
    {
        try
        {
            if (hasRoad())
            {
                Road road = board.getRoad(location);
                return road.getPlayer().equals(player);
            }
            else
                return false;
        }
        catch (InvalidLocationException ignore)
        {
            return false;
        }
    }

    /**
     * Returns whether or not there is a road on this edge
     * @return true if there is a road belonging to any server.model.player on this edge, false otherwise
     */
    protected boolean hasRoad()
    {
        return board.hasRoad(location);
    }

    @Override
    public boolean equals(Object edge)
    {
        if (edge instanceof Edge)
        {
            Edge e = (Edge) edge;
            return e.getLocation().equals(location);
        }
        else
            return false;
    }

    /**
     * Gets the two vertices that surround this edge
     * @return a list of two vertices
     */
    public List<Vertex> getTwoAdjacentVertices() throws InvalidLocationException
    {
        HexLocation hex = location.getHexLoc();
        EdgeDirection direction = location.getDir();
        List<VertexLocation> vertices = new ArrayList<VertexLocation>();
        if (direction == EdgeDirection.North)
        {
            vertices.add(new VertexLocation(hex, VertexDirection.NorthEast));
            vertices.add(new VertexLocation(hex, VertexDirection.NorthWest));
        }
        else if (direction == EdgeDirection.NorthEast)
        {
            vertices.add(new VertexLocation(hex, VertexDirection.NorthEast));
            vertices.add((new VertexLocation(hex, VertexDirection.East)).getNormalizedLocation());
        }
        else //direction == NorthWest
        {
            vertices.add((new VertexLocation(hex, VertexDirection.West)).getNormalizedLocation());
            vertices.add(new VertexLocation(hex, VertexDirection.NorthWest));
        }

        List<Vertex> result = new ArrayList<Vertex>();
        result.add(board.getVertex(vertices.get(0)));
        result.add(board.getVertex(vertices.get(1)));

        return result;
    }


    private Collection<Edge> getAdjacentEdges()
    {
        EdgeLocation loc = location.getNormalizedLocation();
        Collection<Edge> neighbors = new ArrayList<Edge>();
        HexLocation hex = loc.getHexLoc();

        if (loc.getDir() == EdgeDirection.North)
        {
            addAdjacentEdge(neighbors, hex, EdgeDirection.NorthEast);
            addAdjacentEdge(neighbors, hex, EdgeDirection.NorthWest);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.North), EdgeDirection.SouthEast);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.North), EdgeDirection.SouthWest);
        }
        else if (loc.getDir() == EdgeDirection.NorthWest)
        {
            addAdjacentEdge(neighbors, hex, EdgeDirection.North);
            addAdjacentEdge(neighbors, hex, EdgeDirection.SouthWest);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthWest), EdgeDirection.NorthEast);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthWest), EdgeDirection.South);
        }
        else    //EdgeDirection.NorthEast
        {
            addAdjacentEdge(neighbors, hex, EdgeDirection.North);
            addAdjacentEdge(neighbors, hex, EdgeDirection.SouthEast);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthEast), EdgeDirection.NorthWest);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthEast), EdgeDirection.South);
        }
        return neighbors;
    }

    //Getters
    /**
     * Gets the edge's location
     * @return the EdgeLocation of the edge
     */
    public EdgeLocation getLocation()
    {
        return location;
    }
}
