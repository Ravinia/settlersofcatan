package client.model.game.board.immutable.ports;

import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidPortTypeException;
import client.model.game.board.immutable.Board;
import shared.definitions.PortType;
import shared.locations.EdgeLocation;

/**
 * Factory for converting raw information into a Port object
 * @author Lawrence
 */
public class PortFactory
{
    private Board board;

    public PortFactory(Board board)
    {
        this.board = board;
    }

    public Port convert(String resource, int ratio, EdgeLocation location) throws InvalidPortTypeException, InvalidLocationException
    {
        PortType type = getPortType(resource);
        Port port;
        switch (type)
        {
            case WOOD:
                port = new WoodPort(board, location);
                break;
            case BRICK:
                port = new BrickPort(board, location);
                break;
            case SHEEP:
                port = new SheepPort(board, location);
                break;
            case WHEAT:
                port = new WheatPort(board, location);
                break;
            case ORE:
                port = new OrePort(board, location);
                break;
            case THREE:
                port = new GenericPort(board, location);
                break;
            default:
                throw new InvalidPortTypeException(resource);
        }

        if (port.ratio() != ratio)
        {
            AbstractPort p = (AbstractPort) port;
            p.setRatio(ratio);
            port = p;
        }

        return port;
    }

    public Port convert(String resource,EdgeLocation location) throws InvalidPortTypeException, InvalidLocationException
    {
        PortType type = getPortType(resource);
        Port port;
        switch (type)
        {
            case WOOD:
                port = new WoodPort(board, location);
                break;
            case BRICK:
                port = new BrickPort(board, location);
                break;
            case SHEEP:
                port = new SheepPort(board, location);
                break;
            case WHEAT:
                port = new WheatPort(board, location);
                break;
            case ORE:
                port = new OrePort(board, location);
                break;
            case THREE:
                port = new GenericPort(board, location);
                break;
            default:
                throw new InvalidPortTypeException(resource);
        }
        return port;
    }

    private static PortType getPortType(String type) throws InvalidPortTypeException
    {
        if (type == null)
            return PortType.THREE;
        type = type.toLowerCase();
        type = type.trim();
        if (type.equals("wood"))
            return PortType.WOOD;
        else if (type.equals("wheat"))
            return PortType.WHEAT;
        else if (type.equals("brick"))
            return PortType.BRICK;
        else if (type.equals("sheep"))
            return PortType.SHEEP;
        else if (type.equals("ore"))
            return PortType.ORE;
        else if (type.equals("three") || type.equals("generic"))
            return PortType.THREE;
        else
            throw new InvalidPortTypeException(type);
    }
}
