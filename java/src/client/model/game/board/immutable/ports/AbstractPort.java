package client.model.game.board.immutable.ports;

import shared.exceptions.InvalidLocationException;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.Vertex;
import client.model.game.player.Player;
import shared.locations.EdgeLocation;
import shared.locations.PortLocation;

import java.util.List;

/**
 * Abstract port class for containing some of the common functions of the port objects
 */
public abstract class AbstractPort implements Port
{
	private Board board;
    private PortLocation location = null;
    private int ratio = 2;

    public AbstractPort(Board board, EdgeLocation edge) throws InvalidLocationException
    {
        this.board = board;
        EdgeLocation normalizedEdge = edge.getNormalizedLocation();
        List<Vertex> adjacent = board.getEdge(normalizedEdge).getTwoAdjacentVertices();

        adjacent.get(0).setPort(this);
        adjacent.get(1).setPort(this);

        location = new PortLocation(edge, adjacent.get(0).getLocation(), adjacent.get(1).getLocation());
    }

    @Override
    public PortLocation getLocation()
    {
        return location;
    }

    @Override
    public Player getOwner() throws InvalidLocationException
    {
        Vertex vertex1 = board.getVertex(location.getVertex1());
        Vertex vertex2 = board.getVertex(location.getVertex2());

        if (vertex1.hasSettlement())
            return board.getSettlement(vertex1.getLocation()).getPlayer();
        else if (vertex2.hasSettlement())
            return board.getSettlement(vertex2.getLocation()).getPlayer();
        else
            return null;
    }

    @Override
    public boolean ownsPort(Player player) throws InvalidLocationException
    {
        return getOwner() == player;
    }

    @Override
    public int ratio()
    {
        return ratio;
    }

    protected void setRatio(int ratio)
    {
        this.ratio = ratio;
    }
    
    @Override
    public boolean equals(Object o)
    {
    	if (!(o instanceof Port))
    		return false;
    	Port port = (Port)o;
    	return this.location.equals(port.getLocation());
    }

}
