package client.model.game.board.immutable.ports;

import shared.exceptions.InvalidLocationException;
import client.model.game.board.immutable.Board;
import shared.definitions.PortType;
import shared.locations.EdgeLocation;

public class GenericPort extends AbstractPort implements Port
{
    public GenericPort(Board board, EdgeLocation location) throws InvalidLocationException
    {
        super(board, location);
        this.setRatio(3);
    }

	@Override
	public PortType getPortType()
    {
		return PortType.THREE;
	}
}
