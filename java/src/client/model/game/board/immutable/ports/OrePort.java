package client.model.game.board.immutable.ports;

import shared.exceptions.InvalidLocationException;
import client.model.game.board.immutable.Board;
import shared.definitions.PortType;
import shared.locations.EdgeLocation;

public class OrePort extends AbstractPort implements Port
{
    public OrePort(Board board, EdgeLocation location) throws InvalidLocationException
    {
        super(board, location);
    }

	@Override
	public PortType getPortType()
    {
		return PortType.ORE;
	}
}
