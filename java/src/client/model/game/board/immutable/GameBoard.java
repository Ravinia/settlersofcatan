/**
 * 
 */
package client.model.game.board.immutable;

import static client.model.game.board.LocationConstants.DESERT_HEX;
import static client.model.game.board.LocationConstants.getDefaultHexes;
import static client.model.game.board.LocationConstants.getWaterHexes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import shared.definitions.HexType;
import shared.definitions.PortType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.exceptions.BoardAlreadyInitializedException;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import client.model.game.board.immutable.ports.Port;
import shared.locations.PortLocation;
import client.model.game.board.movable.Placeable;
import client.model.game.board.movable.Road;
import client.model.game.board.movable.Robber;
import client.model.game.board.movable.Settlement;
import client.model.game.player.Player;

/**
 * Implementation of the Board interface.
 * @author Lawrence
 *
 */
public class GameBoard extends Observable implements Board, Iterable<Hex>
{
	private ValidLocationDistributor distributor = new ValidLocationDistributor();
    private Map<HexLocation, Hex> hexBoard = new HashMap<HexLocation, Hex>();
    private Map<PortLocation, Port> ports = new HashMap<PortLocation, Port>();
    private Map<EdgeLocation, Road> roads  = new HashMap<EdgeLocation, Road>();
    private Map<VertexLocation, Settlement> settlements = new HashMap<VertexLocation, Settlement>();
    private HexLocation robberLocation = null;

    private int radius;
    private boolean initialized = false;

	/**
	 * Default constructor that initializes a blank game board.
     * If the boolean flag is set to true, it initializes the board with the default settings.
     * @param useDefault should only be set to true for testing and debugging purposes.
	 */
	public GameBoard(boolean useDefault)
	{
        if (useDefault)
        {
            hexBoard = getDefaultHexes(this);
            robberLocation = DESERT_HEX;
            initialized = true;
        }
	}

    /**
     * Initializes a blank game board, and sets the radius.
     * @param radius the radius of the board
     */
    public GameBoard(int radius)
    {
        this.radius = radius;
    }

    //Initializer
    @Override
    public void initializeBoard(Collection<Immutable> boardPieces)
            throws BoardAlreadyInitializedException, InvalidLocationException, InvalidObjectTypeException
    {
        if (initialized)
            throw new BoardAlreadyInitializedException();

        for (Immutable piece : boardPieces)
        {
            handleImmutable(piece);
        }

        notifyObservers();
    }

    //Board Queries
    @Override
	public Hex getHex(HexLocation hex)  throws InvalidLocationException
	{
        if (!distributor.isValidHexLocation(hex)) //is outside the map
        {
            throw new InvalidLocationException(hex, "The location given is outside the bounds of the board");
        }
        else if (hexBoard.containsKey(hex))  //is a land hex
        {
            Hex result = hexBoard.get(hex);
            result.updateBoardReference(this);
            return result;
        }
        else                            //is a water hex
        {
            return new Hex(hex, HexType.WATER, null, this);
        }
	}

	@Override
	public Edge getEdge(EdgeLocation edge)  throws InvalidLocationException
	{
		if (!distributor.isValidEdge(edge))
            throw new InvalidLocationException(edge);

        return new Edge(edge, this);
	}

	@Override
	public Vertex getVertex(VertexLocation vertex) throws InvalidLocationException
	{
		if (!distributor.isValidVertex(vertex))
            throw new InvalidLocationException(vertex);

        return new Vertex(vertex, this);
	}

	@Override
	public Token getToken(HexLocation hex) throws InvalidLocationException
	{
        Token token = getHex(hex).getToken();
        if (token == null)
            throw new InvalidLocationException(hex, "There is no token on this location");
        else
            return token;

	}

    @Override
    public boolean hasSettlement(VertexLocation vertex)
    {
        return settlements.containsKey(vertex);
    }

    @Override
	public Settlement getSettlement(VertexLocation vertex) throws InvalidLocationException
	{
        if (distributor.isValidVertex(vertex))
        {
            if (settlements.containsKey(vertex))
                return settlements.get(vertex);
            else
                throw new InvalidLocationException(vertex, "There is no settlement at this location");
        }
        else
            throw new InvalidLocationException(vertex);
	}

    @Override
    public boolean hasRoad(EdgeLocation edge)
    {
        return roads.containsKey(edge);
    }

    @Override
	public Road getRoad(EdgeLocation edge) throws InvalidLocationException 
	{
        if (distributor.isValidEdge(edge))
        {
            if (roads.containsKey(edge))
                return roads.get(edge);
            else
                throw new InvalidLocationException(edge, "There is no road at this location");
        }
        else
            throw new InvalidLocationException(edge);
	}

	@Override
	public Hex getNeighboringHex(Hex hex, EdgeDirection direction) throws InvalidLocationException
	{
		HexLocation neighborLoc = hex.getNeighborLocation(direction);
        if (!distributor.isValidHexLocation(neighborLoc))
            throw new InvalidLocationException(hex, "The location given is outside the bounds of the board");
		return getHex(neighborLoc);
	}

    @Override
    public HexLocation getRobberLocation()
    {
        return robberLocation;
    }

    @Override
    public boolean isLandHex(HexLocation location)
    {
        return distributor.isLandHex(location);
    }

    @Override
    public Collection<Port> getPorts() throws InvalidLocationException
    {
        return this.ports.values();
    }

    @Override
    public Port getPort(PortLocation port) throws InvalidLocationException
    {
        try
        {
            return ports.get(port);
        }
        catch (Exception e)
        {
            throw new InvalidLocationException(e);
        }
    }

    @Override
    public Collection<PortType> getPlayersPorts(Player player) throws InvalidLocationException
    {
        Set<PortLocation>  locations = this.ports.keySet();
        Collection<PortType> thisPlayerPorts = new ArrayList<PortType>();
        for(PortLocation i: locations)
        {
            if(this.ports.get(i).getOwner() != null)
            {
                if(this.ports.get(i).getOwner() == player)
                {
                    thisPlayerPorts.add(this.ports.get(i).getPortType());
                }
            }
        }

        return thisPlayerPorts;
    }

    @Override
    public Collection<Road> getRoads()
    {
        return roads.values();
    }

    @Override
    public Collection<Settlement> getSettlements()
    {
        return settlements.values();
    }

    //Updates
    @Override
    public void updateBoardObjects(Collection<Placeable> boardPieces) throws InvalidObjectTypeException
    {
        for (Placeable piece : boardPieces)
        {
            handlePlacable(piece);
        }
        notifyObservers();
    }

    @Override
    public void updateBoard(Board b) throws InvalidObjectTypeException
    {
        GameBoard board = (GameBoard) b;
        if (!initialized)
        {
            hexBoard = board.hexBoard;
            addWaterHexes();
            ports = board.ports;
            initialized = true;
        }
        this.updateBoardObjects(board.getPlaceables());
        notifyObservers();
    }

    @Override
    public void notifyObservers()
    {
        super.notifyObservers();
    }

    //Helper Methods
    private void handlePlacable(Placeable piece) throws InvalidObjectTypeException
    {
        switch (piece.getType())
        {
            case ROBBER:
                robberLocation = (HexLocation) piece.getLocation();
                break;
            case ROAD:
                EdgeLocation roadLoc = (EdgeLocation)piece.getLocation();
                roadLoc = roadLoc.getNormalizedLocation();
                roads.put(roadLoc, (Road)piece);
                break;
            case SETTLEMENT:
                VertexLocation settlementLoc = (VertexLocation)piece.getLocation();
                settlementLoc = settlementLoc.getNormalizedLocation();
                settlements.put(settlementLoc, (Settlement)piece);
                break;
            case CITY:
                VertexLocation cityLoc = (VertexLocation)piece.getLocation();
                cityLoc = cityLoc.getNormalizedLocation();
                settlements.put(cityLoc, (Settlement)piece);
                break;
            default:
                throw new InvalidObjectTypeException(piece);
        }
    }

    private void handleImmutable(Immutable piece) throws InvalidObjectTypeException
    {
        if (piece instanceof Hex)
        {
            Hex hex = (Hex) piece;
            hexBoard.put(hex.getLocation(), hex);
        }
        else if (piece instanceof Port)
        {
            Port port = (Port) piece;
            ports.put(port.getLocation(), port);
        }
        else
            throw new InvalidObjectTypeException(piece);
    }

    protected Collection<Placeable> getPlaceables()
    {
        Collection<Placeable> placeables = new ArrayList<Placeable>(roads.values());
        placeables.addAll(settlements.values());
        Robber robber = new Robber(robberLocation);
        placeables.add(robber);
        return placeables;
    }

    private void addWaterHexes()
    {
        for (HexLocation location : getWaterHexes())
        {
            Hex hex = new Hex(location, HexType.WATER, null, this);
            hexBoard.put(location, hex);
        }
    }

    //Iterator Methods
    @Override
    public Iterator<Hex> iterator()
    {
        return hexBoard.values().iterator();
    }
}
