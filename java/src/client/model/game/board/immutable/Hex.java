package client.model.game.board.immutable;

import shared.exceptions.InvalidLocationException;
import client.model.game.player.Player;
import shared.definitions.HexType;
import shared.locations.EdgeDirection;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;


/**
 * a place for the later storage of data pertaining to a whole hex on the board
 * things like hextype--pasture,water,desert..., chit...
 * 
 * @author Eric
 * @author Lawrence
 */
public class Hex implements Immutable
{
	private HexLocation location;
	private HexType type;
	private Token token;
    private Board board;
	
	public Hex(HexLocation location, HexType type, Token token, Board board)
	{
		this.location = location;
		this.type = type;
		this.token = token;
        this.board = board;
	}
	
	//Methods
	/**
	 * Returns the hex location value of the hex's neighbor on the specified side
	 * @param direction which side of the hex to look for its neighbor
	 * @return the hex location of the neighbor. This value might be an invalid location.
	 */
	public HexLocation getNeighborLocation(EdgeDirection direction)
	{
		return location.getNeighborLoc(direction);
	}

    /**
     * Gets all of the players who have a settlement on this hex
     * @return set of all the players
     * @throws InvalidLocationException
     */
    public Collection<Player> getPlayersOnThisHex() throws InvalidLocationException
    {
        Collection<Player> players = new HashSet<Player>();
        Collection<Vertex> vertices = getVertices();
        for (Vertex vertex : vertices)
        {
            if (vertex.hasSettlement())
            {
               Player player = board.getSettlement(vertex.getLocation()).getPlayer();
                players.add(player);
            }
        }
        return players;
    }

    //Helper Methods
    private Collection<Vertex> getVertices() throws InvalidLocationException
    {
        Collection<Vertex> vertices = new ArrayList<Vertex>();
        Collection<VertexLocation> locations = new ArrayList<VertexLocation>();
        for (VertexDirection direction : VertexDirection.values())
        {
            VertexLocation loc = new VertexLocation(this.location, direction);
            loc = loc.getNormalizedLocation();
            locations.add(loc);
        }
        for (VertexLocation loc : locations)
        {
            vertices.add(board.getVertex(loc));
        }
        return vertices;
    }

    protected void updateBoardReference(Board board)
    {
        this.board = board;
    }
	
	//Getters
	/**
	 * @return the location
	 */
	public HexLocation getLocation() 
	{
		return location;
	}
	/**
	 * @return the type of Hex
	 */
	public HexType getType() 
	{
		return type;
	}
	/**
	 * @return the token
	 */
	public Token getToken() 
	{
		return token;
	}
	
}
