package client.model.game.board.immutable;

import shared.exceptions.InvalidLocationException;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;

import java.util.Collection;

/**
 * Abstract class for common edge finding functions
 */
public abstract class AdjacentEdgeFinder
{
    protected Board board;

    public AdjacentEdgeFinder(Board board)
    {
        this.board = board;
    }

    protected Collection<Edge> addAdjacentEdge(Collection<Edge> neighbors, HexLocation hex, EdgeDirection direction)
    {
        Edge neighbor = getAdjacentEdge(hex, direction);
        if (neighbor != null)
            neighbors.add(neighbor);
        return neighbors;
    }

    protected Edge getAdjacentEdge(HexLocation hex, EdgeDirection direction)
    {
        try
        {
            EdgeLocation loc = new EdgeLocation(hex, direction);
            loc = loc.getNormalizedLocation();
            return board.getEdge(loc);
        }
        catch (InvalidLocationException ignore)
        {
            return null;
        }
    }
}
