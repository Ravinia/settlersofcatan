package client.model.game.board.immutable;

/**
 * Abstract interface for including parts of the board that do not change over time.
 */
public interface Immutable
{
}
