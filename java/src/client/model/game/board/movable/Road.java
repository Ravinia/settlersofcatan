package client.model.game.board.movable;

import client.model.game.player.Player;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;

/**
 * The place where a road sets its location but primarily for easily checking where the road is located
 * @author eric
 * 
 */
public class Road implements Placeable
{
    private EdgeLocation location;
    Player owner;

    public Road(EdgeLocation location, Player owner)
    {
        this.location = location;
        this.owner = owner;
    }

    @Override
    public PieceType getType()
    {
        return PieceType.ROAD;
    }

    /**Returns the edge location of this road
	 * @return EdgeLocation where the road is located
	 */
	public EdgeLocation getLocation()
    {
        return location;
    }

    @Override
    public Player getPlayer()
    {
        return owner;
    }
}
