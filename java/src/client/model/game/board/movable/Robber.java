package client.model.game.board.movable;

import client.model.game.player.Player;
import shared.definitions.PieceType;
import shared.locations.HexLocation;
import shared.locations.Location;

/**
 * Robber object.
 * Primarily used for transporting as data from the deserializer.
 * @author Lawrence
 */
public class Robber implements Placeable
{
    HexLocation location;

    public Robber(HexLocation location)
    {
        this.location = location;
    }

    @Override
    public PieceType getType()
    {
        return PieceType.ROBBER;
    }

    @Override
    public Location getLocation()
    {
        return location;
    }

    @Override
    public Player getPlayer()
    {
        return null;
    }
}
