package client.model.game.board.movable;

import client.model.game.player.Player;
import shared.definitions.PieceType;
import shared.locations.VertexLocation;

/**
 * Implementation of Settlement as a settlement, not a city
 * @author Eric
 * @author Lawrence
 *
 */
public class SettlementPiece implements Settlement
{
    private VertexLocation location;
    Player owner;

    public SettlementPiece(VertexLocation location, Player owner)
    {
        this.location = location;
        this.owner = owner;
    }

    @Override
    public PieceType getType()
    {
        return PieceType.SETTLEMENT;
    }

	public VertexLocation getLocation()
    {
        return location;
    }

    @Override
    public Player getPlayer()
    {
        return owner;
    }

    public boolean isCity()
    {
        return false;
    }

    @Override
    public int getVictoryPoints()
    {
        return 1;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SettlementPiece other = (SettlementPiece) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}
}
