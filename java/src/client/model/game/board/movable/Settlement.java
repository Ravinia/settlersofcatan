package client.model.game.board.movable;

import shared.locations.VertexLocation;

/**
 * The place where a settlement sets its location but primarily for easily checking where the settlement is located
 * also can check whether the settlement is actually upgraded to a city
 * @author Lawrence
 * @author Eric
 */
public interface Settlement extends Placeable
{
    public VertexLocation getLocation();

    /**
     * Whether it is a city or a settlement
     * @return true if city
     */
    public boolean isCity();

    /**
     * Returns the number of victory points this settlement/city is worth
     * @return 1 if settlement, 2 if city
     */
    public int getVictoryPoints();
}
