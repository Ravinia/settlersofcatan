package client.model.game.board.movable;

import client.model.game.player.Player;
import shared.definitions.PieceType;
import shared.locations.VertexLocation;

/**
 * City implementation of the Settlement interface as a city
 * @author Lawrence
 */
public class CityPiece extends SettlementPiece
{
    public CityPiece(VertexLocation location, Player owner)
    {
        super(location, owner);
    }

    @Override
    public PieceType getType()
    {
        return PieceType.CITY;
    }

    @Override
    public boolean isCity()
    {
        return true;
    }

    @Override
    public int getVictoryPoints()
    {
        return 2;
    }
}
