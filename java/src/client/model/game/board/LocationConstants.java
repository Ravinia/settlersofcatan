package client.model.game.board;

import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.Hex;
import client.model.game.board.immutable.Token;
import client.model.game.board.movable.Placeable;
import client.model.game.board.movable.Road;
import client.model.game.board.movable.SettlementPiece;
import shared.definitions.HexType;
import shared.locations.*;

import static client.model.game.player.PlayerConstants.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class that contains some common constants to be used in setup and testing.
 *
 * @author Lawrence
 */
public abstract class LocationConstants
{
    public static final HexLocation CENTER_HEX = new HexLocation(0, 0);
    public static final HexLocation HEX_0_N1 = new HexLocation(0, -1);
    public static final HexLocation HEX_0_N2 = new HexLocation(0, -2);
    public static final HexLocation HEX_0_1 = new HexLocation(0, 1);
    public static final HexLocation HEX_0_2 = new HexLocation(0, 2);
    public static final HexLocation HEX_1_N2 = new HexLocation(1, -2);
    public static final HexLocation HEX_1_N1 = new HexLocation(1, -1);
    public static final HexLocation HEX_1_0 = new HexLocation(1, 0);
    public static final HexLocation HEX_1_1 = new HexLocation(1, 1);
    public static final HexLocation HEX_2_N2 = new HexLocation(2, -2);
    public static final HexLocation HEX_2_N1 = new HexLocation(2, -1);
    public static final HexLocation HEX_2_0 = new HexLocation(2, 0);
    public static final HexLocation HEX_N1_N1 = new HexLocation(-1, -1);
    public static final HexLocation HEX_N1_0 = new HexLocation(-1, 0);
    public static final HexLocation HEX_N1_1 = new HexLocation(-1, 1);
    public static final HexLocation HEX_N1_2 = new HexLocation(-1, 2);
    public static final HexLocation HEX_N2_0 = new HexLocation(-2, 0);
    public static final HexLocation HEX_N2_1 = new HexLocation(-2, 1);
    public static final HexLocation HEX_N2_2 = new HexLocation(-2, 2);

    public static final HexLocation DESERT_HEX = HEX_0_2;

    public static final HexLocation NORTH_WATER_HEX = new HexLocation(0, -3);
    public static final HexLocation WATER_N1_N2 = new HexLocation(-1, -2);
    public static final HexLocation WATER_N2_N1 = new HexLocation(-2, -1);
    public static final HexLocation WATER_N3_0 = new HexLocation(-3, 0);
    public static final HexLocation WATER_N3_1 = new HexLocation(-3, 1);
    public static final HexLocation WATER_N3_2 = new HexLocation(-3, 2);
    public static final HexLocation WATER_N3_3 = new HexLocation(-3, 3);
    public static final HexLocation WATER_N2_3 = new HexLocation(-2, 3);
    public static final HexLocation WATER_N1_3 = new HexLocation(-1, 3);
    public static final HexLocation WATER_0_3 = new HexLocation(0, 3);
    public static final HexLocation WATER_1_2 = new HexLocation(1, 2);
    public static final HexLocation WATER_2_1 = new HexLocation(2, 1);
    public static final HexLocation WATER_3_0 = new HexLocation(3, 0);
    public static final HexLocation WATER_3_N1 = new HexLocation(3, -1);
    public static final HexLocation WATER_3_N2 = new HexLocation(3, -2);
    public static final HexLocation WATER_3_N3 = new HexLocation(3, -3);
    public static final HexLocation WATER_2_N3 = new HexLocation(2, -3);
    public static final HexLocation WATER_1_N3 = new HexLocation(1, -3);

    public static final EdgeLocation CENTER_NE_EDGE = new EdgeLocation(CENTER_HEX, EdgeDirection.NorthEast);
    public static final EdgeLocation CENTER_N_EDGE = new EdgeLocation(CENTER_HEX, EdgeDirection.North);
    public static final EdgeLocation EDGE_1_1_N = new EdgeLocation(HEX_1_1, EdgeDirection.North);
    public static final EdgeLocation EDGE_N1_1_N = new EdgeLocation(HEX_N1_1, EdgeDirection.North);
    public static final EdgeLocation EDGE_0_N1_N = new EdgeLocation(HEX_0_N1, EdgeDirection.North);
    public static final EdgeLocation EDGE_N2_0_NE = new EdgeLocation(HEX_N2_0, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_N2_2_NE = new EdgeLocation(HEX_N2_2, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_1_N1_NE = new EdgeLocation(HEX_1_N1, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_2_N1_NE = new EdgeLocation(HEX_2_N1, EdgeDirection.NorthEast);
    public static final EdgeLocation INVALID_EDGE = new EdgeLocation(NORTH_WATER_HEX, EdgeDirection.North);

    
    public static final VertexLocation CENTER_NE_VERTEX = new VertexLocation(CENTER_HEX, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_N1_1_NE = new VertexLocation(HEX_N1_1, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_1_N1_NE = new VertexLocation(HEX_1_N1, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_2_N1_NE = new VertexLocation(HEX_2_N1, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_1_1_NE = new VertexLocation(HEX_1_1, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_N1_2_NW = new VertexLocation(HEX_N1_2, VertexDirection.NorthWest);
    public static final VertexLocation VERTEX_N2_0_NE = new VertexLocation(HEX_N2_0, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_0_N1_NW = new VertexLocation(HEX_0_N1, VertexDirection.NorthWest);
    public static final VertexLocation INVALID_VERTEX = new VertexLocation(NORTH_WATER_HEX, VertexDirection.NorthEast);

    /**
     * Retrieves the default game setup. Used for testing purposes.
     * Note: this is turned 90 degrees from the Catan game rules, since
     * the map on the CS web page has a different orientation from their method
     * @return list of hexes
     */
    public static Map<HexLocation, Hex> getDefaultHexes(Board board)
    {
        Map<HexLocation, Hex> hexMap = new HashMap<HexLocation, Hex>();

        hexMap.put(HEX_N2_2, new Hex(HEX_N2_2, HexType.WOOD, new Token(11), board));
        hexMap.put(HEX_N2_1, new Hex(HEX_N2_1, HexType.SHEEP, new Token(12), board));
        hexMap.put(HEX_N2_0, new Hex(HEX_N2_0, HexType.WHEAT, new Token(9), board));
        hexMap.put(HEX_N1_2, new Hex(HEX_N1_2, HexType.BRICK, new Token(4), board));
        hexMap.put(HEX_N1_1, new Hex(HEX_N1_1, HexType.ORE, new Token(6), board));
        hexMap.put(HEX_N1_0, new Hex(HEX_N1_0, HexType.BRICK, new Token(5), board));
        hexMap.put(HEX_N1_N1, new Hex(HEX_N1_N1, HexType.SHEEP, new Token(10), board));
        hexMap.put(HEX_0_2, new Hex(HEX_0_2, HexType.DESERT, null, board));
        hexMap.put(HEX_0_1, new Hex(HEX_0_1, HexType.WOOD, new Token(3), board));
        hexMap.put(CENTER_HEX, new Hex(CENTER_HEX, HexType.WHEAT, new Token(11), board));
        hexMap.put(HEX_0_N1, new Hex(HEX_0_N1, HexType.WOOD, new Token(4), board));
        hexMap.put(HEX_0_N2, new Hex(HEX_0_N2, HexType.WHEAT, new Token(8), board));
        hexMap.put(HEX_1_1, new Hex(HEX_1_1, HexType.BRICK, new Token(8), board));
        hexMap.put(HEX_1_0, new Hex(HEX_1_0, HexType.SHEEP, new Token(10), board));
        hexMap.put(HEX_1_N1, new Hex(HEX_1_N1, HexType.SHEEP, new Token(9), board));
        hexMap.put(HEX_1_N2, new Hex(HEX_1_N2, HexType.ORE, new Token(3), board));
        hexMap.put(HEX_2_0, new Hex(HEX_2_0, HexType.ORE, new Token(5), board));
        hexMap.put(HEX_2_N1, new Hex(HEX_2_N1, HexType.WHEAT, new Token(2), board));
        hexMap.put(HEX_2_N2, new Hex(HEX_2_N2, HexType.WOOD, new Token(6), board));

        return hexMap;
    }

    public static List<Placeable> getDefaultStartPieces()
    {
        List<Placeable> pieces = new ArrayList<Placeable>();

        // get roads
        // Note: these were mixed up with northeast and northwest switched around,
        // so I'm changing them. For now, the modified lines are commented out with
        // the changes written below them. --Isaac
        pieces.add(new Road(CENTER_N_EDGE, RED_PLAYER));
        pieces.add(new Road(EDGE_N1_1_N, RED_PLAYER));
        pieces.add(new Road(EDGE_1_1_N, BLUE_PLAYER));
        pieces.add(new Road(EDGE_2_N1_NE, BLUE_PLAYER));
        pieces.add(new Road(EDGE_0_N1_N, ORANGE_PLAYER));
        pieces.add(new Road(EDGE_N2_0_NE, ORANGE_PLAYER));
        pieces.add(new Road(EDGE_1_N1_NE, WHITE_PLAYER));
        pieces.add(new Road(EDGE_N2_2_NE, WHITE_PLAYER));
        
        pieces.add(new SettlementPiece(CENTER_NE_VERTEX, RED_PLAYER));
        pieces.add(new SettlementPiece(VERTEX_N1_1_NE, RED_PLAYER));
        pieces.add(new SettlementPiece(VERTEX_1_1_NE, BLUE_PLAYER));
        pieces.add(new SettlementPiece(VERTEX_2_N1_NE, BLUE_PLAYER));
        pieces.add(new SettlementPiece(VERTEX_N2_0_NE, ORANGE_PLAYER));
        pieces.add(new SettlementPiece(VERTEX_0_N1_NW, ORANGE_PLAYER));
        pieces.add(new SettlementPiece(VERTEX_N1_2_NW, WHITE_PLAYER));
        pieces.add(new SettlementPiece(VERTEX_1_N1_NE, WHITE_PLAYER));

        return pieces;
    }

    public static List<HexLocation> getWaterHexes()
    {
        List<HexLocation> hexes = new ArrayList<HexLocation>();

        hexes.add(NORTH_WATER_HEX);
        hexes.add(WATER_0_3);
        hexes.add(WATER_N1_N2);
        hexes.add(WATER_N2_N1);
        hexes.add(WATER_N3_0);
        hexes.add(WATER_N3_1);
        hexes.add(WATER_N3_2);
        hexes.add(WATER_N3_3);
        hexes.add(WATER_N2_3);
        hexes.add(WATER_N1_3);
        hexes.add(WATER_0_3);
        hexes.add(WATER_1_2);
        hexes.add(WATER_2_1);
        hexes.add(WATER_3_0);
        hexes.add(WATER_3_N1);
        hexes.add(WATER_3_N2);
        hexes.add(WATER_3_N3);
        hexes.add(WATER_2_N3);
        hexes.add(WATER_1_N3);

        return hexes;
    }
}
