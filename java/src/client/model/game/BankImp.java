package client.model.game;

/**
 * Implementation of the Bank interface.
 * @author Eric
 */
public class BankImp implements Bank
{
	
	private int brick;
	private int wood;
	private int sheep;
	private int wheat;
	private int ore;

    public BankImp()
    {
        brick = 0;
        wood = 0;
        sheep = 0;
        wheat = 0;
        ore = 0;
    }
	
	public BankImp(int brick, int wood, int sheep, int wheat, int ore)
    {
		this.brick = brick;
		this.wood = wood;
		this.sheep = sheep;
		this.wheat = wheat;
		this.ore = ore;
	}

	/**
	 * @return the brick
	 */
	public int getBrick() {
		return brick;
	}

	/**
	 * @return the wood
	 */
	public int getWood() {
		return wood;
	}

	/**
	 * @return the sheep
	 */
	public int getSheep() {
		return sheep;
	}

	/**
	 * @return the wheat
	 */
	public int getWheat() {
		return wheat;
	}

	/**
	 * @return the ore
	 */
	public int getOre() {
		return ore;
	}
}
