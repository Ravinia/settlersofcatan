package client.model.game;

public class DeckImp implements Deck {
	
	private int yearOfPlentyCount;
	private int monopolyCount;
	private int soldierCount;
	private int roadBuildingCount;
	private int monumentCount;
	
	
	public DeckImp(int yearPlenty, int monopoly, int soldier, int roadBuilding, int monument)
    {
		this.yearOfPlentyCount = yearPlenty;
		this.monopolyCount = monopoly;
		this.soldierCount = soldier;
		this.roadBuildingCount = roadBuilding;
		this.monumentCount = monument;
	}
	
	@Override
	public int getYearOfPlentyCount() {
		return yearOfPlentyCount;
	}
	
	@Override
	public int getMonopolyCount() {
		return monopolyCount;
	}
	
	@Override
	public int getSoldierCount() {
		return soldierCount;
	}
	
	@Override
	public int getRoadBuildingCount() {
		return roadBuildingCount;
	}
	
	@Override
	public int getMonumentCount() {
		return monumentCount;
	}
	
	@Override
	public int getTotalCount() {
		return monumentCount + roadBuildingCount + soldierCount + monopolyCount + yearOfPlentyCount;
	}
	
}
