package client.model.game.chatlog;

/**
 * @author Lawrence
 */
public class LogMessage extends AbstractMessage implements Message
{
    public LogMessage(String source, String message)
    {
        super(source, message);
    }
}
