package client.model.game.chatlog;

import java.util.List;

/**
 * @author Eric
 */
public class ChatLogger implements ChatLog
{
    private List<Message> chats;
    private List<Message> logs;
    public ChatLogger(List<Message> chats, List<Message> logs)
    {
        this.chats = chats;
        this.logs = logs;
    }

    @Override
    public List<Message> getChats()
    {
        return chats;
    }

    @Override
    public List<Message> getLogs()
    {
        return logs;
    }
}
