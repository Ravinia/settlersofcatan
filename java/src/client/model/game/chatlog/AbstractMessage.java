package client.model.game.chatlog;

/**
 * Abstract implementation of the Message interface.
 * @author Lawrence
 */
public abstract class AbstractMessage implements Message
{
    private String source;
    private String message;

    public AbstractMessage(String source, String message)
    {
        this.source = source;
        this.message = message;
    }

    @Override
    public String getSource()
    {
        return source;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
