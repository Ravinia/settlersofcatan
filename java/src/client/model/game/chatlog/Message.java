package client.model.game.chatlog;


/**
 * Message interface for chats and logs.
 * Each message should have two parts: the source, and the message itself.
 * @author Lawrence
 */
public interface Message
{
    /**
     * Gets the source of this message, i.e. who sent it
     * @return the source of the message
     */
    public String getSource();

    /**
     * Gets the actual message
     * @return the message
     */
    public String getMessage();
}
