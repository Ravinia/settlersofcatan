package client.model.game.chatlog;

/**
 * @author Lawrence
 */
public class ChatMessage extends AbstractMessage implements Message
{
    public ChatMessage(String source, String message)
    {
        super(source, message);
    }
}
