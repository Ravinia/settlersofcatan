package client.model.game.chatlog;

import java.util.List;

/**
 * Container object that holds all of the log and chat messages.
 */
public interface ChatLog
{
    /**
     * Gets all of the chat messages
     * @return list of chat messages
     */
    public List<Message> getChats();

    /**
     * Gets all of the log messages
     * @return list of log messages
     */
    public List<Message> getLogs();

}