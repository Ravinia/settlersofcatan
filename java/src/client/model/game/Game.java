package client.model.game;

/**
 * Master interface for game objects
 */
public interface Game
{
    /**
     * gets the game's ID
     * @return the game's ID
     */
    public int getGameID();

    /**
     * gets the game's name/title
     * @return the name of the game
     */
    public String getGameName();
}
