package client.model.game;

import client.model.game.player.ResourceHand;

/**
 * Class for representing the current trade offer
 * @author Eric
 */
public class TradeOffer
{
	private int senderIndex;
	private int receiverIndex;
	private ResourceHand resources; //convenient for holding five types
	
	
	public TradeOffer(int senderIndex, int receiverIndex, ResourceHand resources)
    {
		
		this.senderIndex = senderIndex;
		this.receiverIndex = receiverIndex;
		this.resources = resources;
	}


	/**
	 * @return the senderIndex
	 */
	public int getSenderIndex()
    {
		return senderIndex;
	}


	/**
	 * @return the receiverIndex
	 */
	public int getReceiverIndex()
    {
		return receiverIndex;
	}


	/**
	 * @return the resources
	 */
	public ResourceHand getResources()
    {
		return resources;
	}

    /**
     * rules enforce no giving resources, and attempt to enforce at least one is required of the recipient
     * @return true if at least one resource is given, false otherwise
     */
	public boolean givesOneResource()
    {
		if(resources.getBrick() < 0)
		{
			return true;
		}
		if(resources.getOre() < 0)
		{
			return true;
		}
		if(resources.getSheep() < 0)
		{
			return true;
		}
		if(resources.getWheat() < 0)
		{
			return true;
		}
		if(resources.getWood() < 0)
		{
			return true;
		}
		return false;
	}
	
}
