package client.model.game;

import shared.definitions.ResourceType;

public class MaritimeTradeOffer {
	
	int resourcesReceived;
	int resourcesGiven;
	ResourceType receivedType;
	ResourceType givenType;
	int ratio = 4;
	
	
	public MaritimeTradeOffer(int resourcesGiven, int resourcesReceived,
			ResourceType receivedType, ResourceType givenType){
		this.resourcesReceived = resourcesReceived;
		this.resourcesGiven = resourcesGiven;
		this.receivedType = receivedType;
		this.givenType = givenType;
		
//		Collection<PortType> thisPlayerPortTypes = board.getPlayersPorts(order.getThisPlayer());         //add ratio checking later possibly in facade
//		if(!thisPlayerPortTypes.isEmpty())
//		{
//			for(PortType i: thisPlayerPortTypes)
//			{
//				setRatio(i, givenType);
//			}
//		}
//		else
//		{
//			ratio = 4;
//		}
				
	}	
//	
//	private void setRatio(PortType type1, ResourceType type2)
//	{
//		switch(type1)
//		{
//			case BRICK:
//				if(type2 == ResourceType.BRICK)
//				{
//					this.ratio = 2;
//				}
//				break;
//			case ORE:
//				if(type2 == ResourceType.ORE)
//				{
//					this.ratio = 2;
//				}
//				break;
//			case SHEEP:
//				if(type2 == ResourceType.SHEEP)
//				{
//					this.ratio = 2;
//				}
//				break;
//			case WHEAT:
//				if(type2 == ResourceType.WHEAT)
//				{
//					this.ratio = 2;
//				}
//				break;
//			case WOOD:
//				if(type2 == ResourceType.WOOD)
//				{
//					this.ratio = 2;
//				}
//				break;
//			case THREE:
//				this.ratio = 3;
//				break;
//		}
//		
//	}


	/**
	 * @return the resourcesReceived
	 */
	public int getResourcesReceived() {
		return resourcesReceived;
	}

	/**
	 * @return the resourcesGiven
	 */
	public int getResourcesGiven() {
		return resourcesGiven;
	}

	/**
	 * @return the receivedType
	 */
	public ResourceType getReceivedType() {
		return receivedType;
	}

	/**
	 * @return the givenType
	 */
	public ResourceType getGivenType() {
		return givenType;
	}

	/**
	 * @return the ratio
	 */
	public int getRatio() {
		return ratio;
	}
}
