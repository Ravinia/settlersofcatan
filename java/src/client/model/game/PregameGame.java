package client.model.game;

/**
 * A place to hold data objects at the game level later like the board, players, and so forth.
 * 
 * @author Eric
 *@author Lawrence
 */
public interface PregameGame extends Game
{
	/**
	 * Retrieves the game's name
	 * @return the game's name
	 */
	public String getGameName();
	
	/**
	 * Retrieves the game's unique ID
	 * @return the game's ID
	 */
	public int getGameID();
	
	/**
	 * Whether or not the game is set up with the random tiles option enabled
	 * @return true if enabled, false if not
	 */
	public boolean isRandomTiles();
	
	/**
	 * Whether or not the game is set up with the random number tokens option enabled
	 * @return true if enabled, false if not
	 */
	public boolean isRandomTokens();
	
	/**
	 * Whether or not the game is set up with the random ports option enabled
	 * @return true if enabled, false if not
	 */
	public boolean isRandomPorts();
}
