package client.model.game.player;

import shared.definitions.CatanColor;
import shared.player.PlayerIdentity;

/**
 * primarily a data holder for later attributes particular to each server.model.player.
 * attached to user so that server.model.player is more accessible throughout client.
 * 
 * @author Eric
 * @author Lawrence
 */
public interface Player
{
	/**
	 * Retrieves the server.model.player's unique ID
	 * @return the server.model.player's ID
	 */
	public int getPlayerId();

    /**
     * Gets the identity
     * @return
     */
    public PlayerIdentity getIdentity();

	/**
	 * Retrieves the server.model.player's color for the current game
	 * @return the server.model.player's color
	 */
	public CatanColor getColor();

	public int getVictoryPoints();

	public int getPlayedSoldiers();

	public boolean isPlayedDevCard();

	public int getPlayedMonuments();

	public boolean isDiscarded();

	public BuildPool getBuildPool();

	public ResourceHand getRecHand();

	public DevelopmentCardHand getDevHand();

	public int getIndex();

	public String getName();

	public void playedDevCard(boolean playedCard);

	public void setDevHand(DevelopmentCardHand devHand);

	public void setRecHand(ResourceHand recHand);
}
