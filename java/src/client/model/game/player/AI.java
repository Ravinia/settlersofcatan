/**
 * 
 */
package client.model.game.player;

/**
 * Data holder for a server.model.player that is controlled by the computer.
 * 
 * @author Lawrence Thatcher
 *
 */
public interface AI extends Player
{

}
