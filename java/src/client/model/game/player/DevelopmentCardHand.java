package client.model.game.player;

import java.util.Collection;

import shared.definitions.DevCardType;
import client.model.game.developmentcard.Development;

/**
 * Handles the storing and playing of development card for a server.model.player
 * @author Eric
 * @author Lawrence
 */
public interface DevelopmentCardHand
{
    /**
     * Retrieves a collection of all the development cards of the current server.model.player
     * @return all of the server.model.player's cards
     */
    public Collection<Development> getAllCards();

    /**
     * Returns a collection of all of the development cards that the current server.model.player has that they can currently play
     * @return all of the cards the server.model.player can currently play
     */
    public Collection<Development> getAllPlayableCards();

	public Collection<DevCardType> getPlayableDevCardTypes();
	
	public Collection<Development> getNonPlayableDevCards();

	public abstract void addNewCard(Development newCard);

	public abstract void addOldCard(Development oldCard);
	
	public int getPlayableNumberOfCardsOfType(DevCardType type);
	
	public int getTotalNumberOfCardsOfType(DevCardType type);
}
