package client.model.game.player;

import shared.definitions.CatanColor;
import shared.player.PlayerIdentity;

public class GamePlayer implements Player
{
	
	private String name;
	private int ID;
	private int Index;
	private DevelopmentCardHand devHand;
	private ResourceHand recHand;
	private BuildPool buildPool;
	private CatanColor color;
	private boolean discarded;
	private int playedMonuments;
	private boolean playedDevCard;
	private int playedSoldiers;
	private int VictoryPoints;
	
	public GamePlayer(String name, int id, int index,
                      DevelopmentCardHand devHand, ResourceHand recHand,
                      BuildPool buildPool, CatanColor color, boolean discarded,
                      int playedMonuments, boolean playedDevCard, int playedSoldiers,
                      int victoryPoints)
    {
		super();
		this.name = name;
		this.ID = id;
		Index = index;
		this.devHand = devHand;
		this.recHand = recHand;
		this.buildPool = buildPool;
		this.color = color;
		this.discarded = discarded;
		this.playedMonuments = playedMonuments;
		this.playedDevCard = playedDevCard;
		this.playedSoldiers = playedSoldiers;
		VictoryPoints = victoryPoints;
	}

	@Override
	public int getPlayerId()
    {
		return ID;
	}

    @Override
    public PlayerIdentity getIdentity()
    {
        return new PlayerIdentity(ID, name);
    }

    @Override
	public CatanColor getColor()
    {
		return color;
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName()
    {
		return name;
	}

	/**
	 * @return the index
	 */
	@Override
	public int getIndex()
    {
		return Index;
	}

	/**
	 * @return the devHand
	 */
	@Override
	public DevelopmentCardHand getDevHand()
    {
		return devHand;
	}

	/**
	 * @return the recHand
	 */
	@Override
	public ResourceHand getRecHand()
    {
		return recHand;
	}

	/**
	 * @return the buildPool
	 */
	@Override
	public BuildPool getBuildPool()
    {
		return buildPool;
	}

	/**
	 * @return whether or not they have discarded
	 */
	@Override
	public boolean isDiscarded()
    {
		return discarded;
	}

	/**
	 * @return the playedMonuments
	 */
	@Override
	public int getPlayedMonuments()
    {
		return playedMonuments;
	}

	/**
	 * @return the playedDevCard
	 */
	@Override
	public boolean isPlayedDevCard()
    {
		return playedDevCard;
	}

	/**
	 * @return the playedSoldiers
	 */
	@Override
	public int getPlayedSoldiers()
    {
		return playedSoldiers;
	}

	/**
	 * @return the victoryPoints
	 */
	@Override
	public int getVictoryPoints()
    {
		return VictoryPoints;
	}
	@Override
	public void playedDevCard(boolean playedCard)   //need this just for testing
	{ 								
		this.playedDevCard = playedCard;
	}
	@Override
	public void setDevHand(DevelopmentCardHand devHand){   //also for testing
		this.devHand = devHand;
	}
	@Override
	public void setRecHand(ResourceHand recHand){ //again for testing
		this.recHand = recHand;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Player))
			return false;
		else
		{
			Player player = (Player)o;
			return this.color == player.getColor();
		}
	}

}
