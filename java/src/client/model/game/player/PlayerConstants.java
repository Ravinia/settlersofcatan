package client.model.game.player;

import shared.definitions.CatanColor;

/**
 * Stores constants relating to players.
 * Used primarily for testing.
 *
 * @author Lawrence
 */
public abstract class PlayerConstants
{
    public static final Player BLUE_PLAYER = new MockPlayer(1, CatanColor.BLUE);
    public static final Player RED_PLAYER = new MockPlayer(2, CatanColor.RED);
    public static final Player ORANGE_PLAYER = new MockPlayer(3, CatanColor.ORANGE);
    public static final Player WHITE_PLAYER = new MockPlayer(4, CatanColor.WHITE);
}
