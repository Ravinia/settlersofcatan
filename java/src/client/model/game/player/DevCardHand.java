package client.model.game.player;


import client.model.game.developmentcard.Development;

import java.util.ArrayList;
import java.util.Collection;

import shared.definitions.DevCardType;

/**
 * later be used for a receptacle of Development Cards particular to this server.model.player.
 * 
 * @author Eric
 * @author Lawrence
 */
public class DevCardHand implements DevelopmentCardHand
{
	
	Collection<Development> oldDevelopmentCards;
	Collection<Development> newDevelopmentCards;
	
	
    public DevCardHand(Collection<Development> oldCards, Collection<Development> newCards)
    {
    	this.oldDevelopmentCards = oldCards;
    	this.newDevelopmentCards = newCards;
    }
    
    @Override
    public Collection<Development> getAllCards()
    {
        Collection<Development> allDevCards = new ArrayList<Development>(oldDevelopmentCards);
        allDevCards.addAll(newDevelopmentCards);
    	
    	return allDevCards;
    }

    @Override
    public Collection<Development> getAllPlayableCards()
    {
    	Collection<Development> oldPlusMonument = new ArrayList<Development>(oldDevelopmentCards);
    	for(Development i: newDevelopmentCards)
    	{
    		if(i.getDevCardType() == DevCardType.MONUMENT)
    		{
    			oldPlusMonument.add(i);
    		}
    	}
    	
    	return oldPlusMonument;
    }

    @Override
    public Collection<DevCardType> getPlayableDevCardTypes()
    {
        Collection<DevCardType> devCardTypes = new ArrayList<DevCardType>();
        
        for(Development i : this.getAllPlayableCards())
        {
        	devCardTypes.add(i.getDevCardType());
        }
        
        return devCardTypes;
    }
    
    @Override
	public void addOldCard(Development oldCard)
    {
    	this.oldDevelopmentCards.add(oldCard);
    }
    
    @Override
	public void addNewCard(Development newCard)
    {
    	this.newDevelopmentCards.add(newCard);
    }

	@Override
	public Collection<Development> getNonPlayableDevCards()
	{
		Collection<Development> allNewDevCards = new ArrayList<Development>(this.newDevelopmentCards);
		return allNewDevCards;
	}
	
	@Override
	public int getPlayableNumberOfCardsOfType(DevCardType type)
	{
		int count = 0;
		for (Development d : getAllPlayableCards())
		{
			if (d.getDevCardType() == type)
				count++;
		}
		return count;
	}
	
	@Override
	public int getTotalNumberOfCardsOfType(DevCardType type)
	{
		int count = 0;
		for (Development d : getAllCards())
		{
			if (d.getDevCardType() == type)
				count++;
		}
		return count;
	}
    
}
