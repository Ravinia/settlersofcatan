package client.model.game.player;

import shared.definitions.CatanColor;
import shared.player.PlayerIdentity;

/**
 * Simple server.model.player object to be used for pregame information.
 * @author Lawrence
 */
public class PregamePlayer implements Player
{
    private String playerName;
    private int playerID;
    private CatanColor playerColor;

    /**
     * Simple constructor for a pregame server.model.player.
     * Pregame players should only be used for conveying pregame information.
     * @param playerName the name of the server.model.player
     * @param playerID the server.model.player's ID
     * @param playerColor the color the server.model.player has chosen
     */
    public PregamePlayer(String playerName, int playerID, CatanColor playerColor)
    {
        this.playerName = playerName;
        this.playerID = playerID;
        this.playerColor = playerColor;
    }

    @Override
    public int getPlayerId()
    {
        return playerID;
    }

    @Override
    public PlayerIdentity getIdentity()
    {
        return new PlayerIdentity(playerID, playerName);
    }

    @Override
    public CatanColor getColor()
    {
        return playerColor;
    }

    @Override
    public String getName()
    {
        return playerName;
    }

    @Override
    public int getVictoryPoints()
    {
        return -1;
    }

    @Override
    public int getPlayedSoldiers()
    {
    	return -1;
    }

    @Override
    public boolean isPlayedDevCard()
    {
        return false;
    }

    @Override
    public int getPlayedMonuments()
    {
    	return -1;
    }

    @Override
    public boolean isDiscarded()
    {
        return false;
    }

    @Override
    public BuildPool getBuildPool()
    {
    	return null;
    }

    @Override
    public ResourceHand getRecHand()
    {
    	return null;
    }

    @Override
    public DevelopmentCardHand getDevHand()
    {
        return null;
    }

    @Override
    public int getIndex()
    {
    	return -1;
    }

    @Override
    public void playedDevCard(boolean playedCard)
    {

    }

    @Override
    public void setDevHand(DevelopmentCardHand devHand)
    {

    }

    @Override
    public void setRecHand(ResourceHand recHand)
    {

    }
}
