/**
 * 
 */
package client.model.game.player;

/**
 * AI type/name class
 * @author Lawrence Thatcher
 *
 */
public interface AIName 
{
	/**
	 * Retrieve's the AI's name
	 * @return the AI's name
	 */
	public String getName();
	
	/**
	 * Sets the AI's name
	 * @param name the name to set the AI's name to
	 */
	public void setName(String name);
}
