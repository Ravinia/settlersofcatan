package client.model.game.player;

/**
 * primarily a data holder for how many roads, settlements, and cities a server.model.player can
 * still purchase and place.
 * 
 * @author Eric
 * @author Lawrence
 */
public class BuildPool
{
	private int roads;
	private int settlements;
	private int cities;
	
	
	public BuildPool()
    {
		roads = 15;
		settlements = 5;
		cities = 4;
	}
	
	public BuildPool(int roads, int settlements, int cities)
    {
		this.roads = roads;
		this.settlements = settlements;
		this.cities = cities;
	}
	/**
	 * @return the roads
	 */
	public int getRoads() {
		return roads;
	}
	/**
	 * @return the settlements
	 */
	public int getSettlements() {
		return settlements;
	}
	/**
	 * @return the cities
	 */
	public int getCities() {
		return cities;
	}

	public void setAll(int roads, int settlements, int cities){ //needed for testing
		this.roads = roads;
		this.settlements = settlements;
		this.cities = cities;
	}
	public void addSettlement()
	{
		this.settlements += 1;
	}
	
}
