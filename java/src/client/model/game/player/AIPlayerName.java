package client.model.game.player;

/**
 * Created by Lawrence on 10/4/2014.
 */
public class AIPlayerName implements AIName
{
    private String name;

    public AIPlayerName(String name)
    {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public void setName(String name)
    {
        this.name = name;
    }
}
