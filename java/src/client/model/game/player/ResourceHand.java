package client.model.game.player;

import java.util.Map;

import shared.definitions.ResourceType;

/**
 * receptacle of resource cards particular to a server.model.player
 * 
 * @author Eric
 * @author Lawrence
 */
public class ResourceHand
{
	private int sheep = 0;
	private int brick = 0;
	private int wood = 0;
	private int ore = 0;
	private int wheat = 0;
	
	
	public ResourceHand()
    { }

	public ResourceHand(int sheep, int brick, int wood, int ore, int wheat)
    {
		this.sheep = sheep;
		this.brick = brick;
		this.wood = wood;
		this.ore = ore;
		this.wheat = wheat;
	}
	
	public ResourceHand(Map<ResourceType, Integer> resources)
	{
		this.sheep = resources.get(ResourceType.SHEEP);
		this.wood = resources.get(ResourceType.WOOD);
		this.brick = resources.get(ResourceType.BRICK);
		this.ore = resources.get(ResourceType.ORE);
		this.wheat = resources.get(ResourceType.WHEAT);
	}
	
	public int getBrick()
    {
		return brick;
	}
	public void addBrick(int brickToAdd)
    {
		this.brick += brickToAdd;
	}
	public int getWood()
    {
		return wood;
	}
	public void addWood(int woodToAdd)
    {
		this.wood += woodToAdd;
	}
	public int getOre()
    {
		return ore;
	}
	public void addOre(int oreToAdd)
    {
		this.ore += oreToAdd;
	}
	public int getWheat()
    {
		return wheat;
	}
	public void addWheat(int wheatToAdd)
    {
		this.wheat += wheatToAdd;
	}
	public int getSheep()
    {
		return sheep;
	}
	public void addSheep(int sheepToAdd)
    {
		this.sheep += sheepToAdd;
	}
	public int getTotalResources()
	{
		return sheep + brick + wood + ore + wheat;
		
	}
	public int getResource(ResourceType resource)
	{
		int numOfResource = 0;
		switch(resource)
		{
			case BRICK:
				numOfResource = this.brick;
				break;
			case WOOD:
				numOfResource = this.wood;
				break;
			case SHEEP:
				numOfResource = this.sheep;
				break;
			case WHEAT:
				numOfResource = this.wheat;
				break;
			case ORE:
				numOfResource = this.ore;
				break;
		}
		return numOfResource;
	}
	
}
