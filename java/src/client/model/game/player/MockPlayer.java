package client.model.game.player;

import shared.definitions.CatanColor;
import shared.player.PlayerIdentity;

/**
 * Implementation of Player used primarily for testing
 * @author Lawrence
 */
public class MockPlayer implements Player
{
    private CatanColor color;
    private int id;

    /**
     * Constructor for creating a mock server.model.player.
     * Mock players are primarily used for testing purposes.
     */
    public MockPlayer(int id, CatanColor color)
    {
        this.id = id;
        this.color = color;
    }

    @Override
    public PlayerIdentity getIdentity()
    {
        return new PlayerIdentity(id, null);
    }

    @Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Player))
			return false;
		else
		{
			Player player = (Player)o;
			return this.color == player.getColor();
		}
	}

    @Override
    public int getPlayerId()
    {
        return id;
    }

    @Override
    public CatanColor getColor()
    {
        return color;
    }

    @Override
    public int getVictoryPoints()
    {
        return -1;
	}

    @Override
	public int getPlayedSoldiers()
    {
    	return -1;
	}

    @Override
	public boolean isPlayedDevCard()
    {
        return false;
	}

    @Override
	public int getPlayedMonuments()
    {
    	return -1;
	}

    @Override
	public boolean isDiscarded()
    {
        return false;
	}

    @Override
	public BuildPool getBuildPool()
    {
        return null;
	}

    @Override
	public ResourceHand getRecHand()
    {
    	return null;
	}

    @Override
	public DevelopmentCardHand getDevHand()
    {
    	return null;
	}

    @Override
	public int getIndex()
    {
    	return -1;
	}

    @Override
	public String getName()
    {
    	return null;
	}

    @Override
	public void setDevHand(DevelopmentCardHand devHand)
    {

	}

	@Override
	public void playedDevCard(boolean playedCard)
    {
		
	}

    @Override
	public void setRecHand(ResourceHand recHand)
    {

	}
}
