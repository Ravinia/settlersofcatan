package client.model.game.developmentcard;

import shared.definitions.DevCardType;
import shared.definitions.ResourceType;

/**
 * Implementation of Year of Plenty development card
 * @author Lawrence
 */
public class YearOfPlentyCard implements YearOfPlenty
{
    private boolean old;
	private ResourceType type1;
	private ResourceType type2;
	private DevCardType  cardType = DevCardType.YEAR_OF_PLENTY;

    public YearOfPlentyCard(boolean old)
    {
        this.old = old;
    }

    @Override
    public void pickResources(ResourceType resource1, ResourceType resource2)
    {
        type1 = resource1;
        type2 = resource2;
    }

    /**
	 * @return the type1
	 */
	@Override
	public ResourceType getType1() {
		return type1;
	}

	/**
	 * @return the type2
	 */
	@Override
	public ResourceType getType2() {
		return type2;
	}

	@Override
    public DevCardType getDevCardType()
    {
        return DevCardType.YEAR_OF_PLENTY;
    }

    @Override
    public boolean isOld()
    {
        return old;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cardType == null) ? 0 : cardType.hashCode());
		result = prime * result + ((type1 == null) ? 0 : type1.hashCode());
		result = prime * result + ((type2 == null) ? 0 : type2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		YearOfPlentyCard other = (YearOfPlentyCard) obj;
		if (cardType != other.cardType)
			return false;
		if (type1 != other.type1)
			return false;
		if (type2 != other.type2)
			return false;
		return true;
	}

}
