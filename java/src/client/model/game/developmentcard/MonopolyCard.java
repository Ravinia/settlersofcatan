package client.model.game.developmentcard;

import java.util.List;

import shared.definitions.DevCardType;
import shared.definitions.ResourceType;

/**
 * Implementation of the Monopoly development card
 * @author Lawrence
 */
public class MonopolyCard implements Monopoly
{
    private ResourceType resourceType;
    private DevCardType cardType = DevCardType.MONOPOLY;
    boolean old = false;

    public MonopolyCard(boolean old)
    {
        this.old = old;
    }

	@Override
    public void pickResource(ResourceType resource)
    {
        this.resourceType = resource;
    }

    @Override
    public List<ResourceType> receiveResources()
    {
        return null;
    }

    @Override
    public DevCardType getDevCardType()
    {
        return DevCardType.MONOPOLY;
    }

    @Override
    public boolean isOld()
    {
        return old;
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((resourceType == null) ? 0 : resourceType.hashCode());
		result = prime * result + ((cardType == null) ? 0 : cardType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonopolyCard other = (MonopolyCard) obj;
		if (resourceType != other.resourceType)
			return false;
		if (cardType != other.cardType)
			return false;
		return true;
	}


}
