package client.model.game.developmentcard;

import shared.definitions.DevCardType;
import shared.locations.HexLocation;

/**
 * Implementation of the Soldier development card
 * @author Lawrence
 */
public class SoldierCard implements Soldier
{
    private boolean old;
    private HexLocation desiredLocation;
    private int robeeIndex;
    private DevCardType cardType = DevCardType.SOLDIER;
    
    public SoldierCard(boolean old)
    {
        this.old = old;
    }

    @Override
    public DevCardType getDevCardType()
    {
        return DevCardType.SOLDIER;
    }

    @Override
    public boolean isOld()
    {
        return old;
    }

	/**
	 * @return the desiredLocation
	 */
	@Override
	public HexLocation getDesiredLocation() {
		return desiredLocation;
	}

	/**
	 * @param desiredLocation the desiredLocation to set
	 */
	@Override
	public void setDesiredLocation(HexLocation desiredLocation) {
		this.desiredLocation = desiredLocation;
	}

	/**
	 * @return the robeeIndex
	 */
	@Override
	public int getRobeeIndex() {
		return robeeIndex;
	}

	/**
	 * @param robeeIndex the robeeIndex to set
	 */
	@Override
	public void setRobeeIndex(int robeeIndex) {
		this.robeeIndex = robeeIndex;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((desiredLocation == null) ? 0 : desiredLocation.hashCode());
		result = prime * result + robeeIndex;
		result = prime * result + ((cardType == null) ? 0 : cardType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SoldierCard other = (SoldierCard) obj;
		if (desiredLocation == null) {
			if (other.desiredLocation != null)
				return false;
		} else if (!desiredLocation.equals(other.desiredLocation))
			return false;
		if (robeeIndex != other.robeeIndex)
			return false;
		if (cardType != other.cardType)
			return false;
		return true;
	}

}
