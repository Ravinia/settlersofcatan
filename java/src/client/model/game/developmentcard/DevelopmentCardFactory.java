package client.model.game.developmentcard;

import client.exceptions.InvalidDevelopmentCardException;

import java.util.ArrayList;
import java.util.List;

/**
 * Generates development cards from their string
 * @author Lawrence
 */
public class DevelopmentCardFactory
{

    public static List<Development> convert(int yearOfPlenty, int monopoly, int soldier, int roadbuilding, int monument, boolean old)
    {
        List<Development> developmentCards = new ArrayList<Development>();
        for (int i = 0; i < yearOfPlenty; i++)
            developmentCards.add(new YearOfPlentyCard(old));
        for (int i = 0; i < monopoly; i++)
            developmentCards.add(new MonopolyCard(old));
        for (int i = 0; i < soldier; i++)
            developmentCards.add(new SoldierCard(old));
        for (int i = 0; i < roadbuilding; i++)
            developmentCards.add(new RoadBuildingCard(old));
        for (int i = 0; i < monument; i++)
            developmentCards.add(new MonumentCard(old));
        return developmentCards;
    }


    //Note: after actually looking at the JSON, we might not ever actually use this function!
    /**
     * Converts a development card from a raw string to its appropriate development card object
     * @param card the string of the card to convertBank
     * @param old whether or not this card is a turn old
     * @return development card
     * @throws InvalidDevelopmentCardException
     */
    public static Development convert(String card, boolean old) throws InvalidDevelopmentCardException
    {
        card = card.toLowerCase();
        card = card.trim();

        if (matches(card, MONOPOLY))
            return new MonopolyCard(old);
        else if (matches(card, MONUMENT))
            return new MonumentCard(old);
        else if (matches(card, ROADBUILDING))
            return new RoadBuildingCard(old);
        else if (matches(card, SOLDIER))
            return new SoldierCard(old);
        else if (matches(card, YEAROFPLENTY))
            return new YearOfPlentyCard(old);
        else
            throw new InvalidDevelopmentCardException(card);
    }

    private static boolean matches(String actual, String expected)
    {
        return actual.equals(expected) || actual.equals(expected + CARD);
    }

    private static final String MONOPOLY = "monopoly";
    private static final String MONUMENT = "monument";
    private static final String ROADBUILDING = "roadbuilding";
    private static final String SOLDIER = "soldier";
    private static final String YEAROFPLENTY = "yearofplenty";
    private static final String CARD = "card";
}
