package client.model.game.developmentcard;

import shared.definitions.ResourceType;

/**
 * functions performed by the DevCard Year Of Plenty----may assist with facade in functionality
 * or may later realize this is unnecessary.
 * 
 * @author eric
 *
 */
public interface YearOfPlenty extends Development
{
	/**
	 * Chooses resources to be gained
	 * May just package these resource choices into a list, provide later functionality,
	 * or could easily be dissolved into the facade at a later time.
	 * 
	 * @param resource1   one of the resources to be gained
	 * @param resource2   the other resource
	 * @return
	 */
	public void pickResources(ResourceType resource1, ResourceType resource2);

	public ResourceType getType2();

	public ResourceType getType1();
}
