package client.model.game.developmentcard;

import shared.exceptions.InvalidLocationException;
import shared.definitions.DevCardType;
import shared.locations.EdgeLocation;

/**
 * Implementation of the Road Building development card
 * @author Lawrence
 */
public class RoadBuildingCard implements RoadBuilding
{
	private boolean old = false;
	private EdgeLocation location1;
	private EdgeLocation location2;
	private DevCardType cardType = DevCardType.ROAD_BUILD;

    public RoadBuildingCard(boolean old)
    {
        this.old = old;
    }

    @Override
    public void pickRoadLocations(EdgeLocation location1, EdgeLocation location2) throws InvalidLocationException
    {
    	this.location1 = location1;
    	this.location2 = location2;
    }

    /**
	 * @return the location1
	 */
	@Override
	public EdgeLocation getLocation1() {
		return location1;
	}

	/**
	 * @return the location2
	 */
	@Override
	public EdgeLocation getLocation2() {
		return location2;
	}

	@Override
    public DevCardType getDevCardType()
    {
        return DevCardType.ROAD_BUILD;
    }

    @Override
    public boolean isOld()
    {
        return old;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((location1 == null) ? 0 : location1.hashCode());
		result = prime * result
				+ ((location2 == null) ? 0 : location2.hashCode());
		result = prime * result + ((cardType == null) ? 0 : cardType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoadBuildingCard other = (RoadBuildingCard) obj;
		if (location1 == null) {
			if (other.location1 != null)
				return false;
		} else if (!location1.equals(other.location1))
			return false;
		if (location2 == null) {
			if (other.location2 != null)
				return false;
		} else if (!location2.equals(other.location2))
			return false;
		if (cardType != other.cardType)
			return false;
		return true;
	}

    
}
