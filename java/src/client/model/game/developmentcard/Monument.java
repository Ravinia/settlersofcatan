package client.model.game.developmentcard;

/**
 * functions performed by the DevCard monument----may assist with facade in functionality
 * or may later realize this is unnecessary.
 * @author Eric
 *
 */
public interface Monument extends Development
{
	/**
	 *  increases the victory points of the server.model.player by one
	 */
	public void incrementVictoryPoint();
}
