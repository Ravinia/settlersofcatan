package client.model.game.developmentcard;

import shared.locations.HexLocation;

/**
 * functions performed by the DevCard Soldier----may assist with facade in functionality
 * or may later realize this is unnecessary.
 * 
 * @author eric
 *
 */
public interface Soldier extends Development
{

	public void setRobeeIndex(int robeeIndex);

	public int getRobeeIndex();

	public void setDesiredLocation(HexLocation desiredLocation);

	public HexLocation getDesiredLocation();
}
