package client.model.game.developmentcard;

import java.util.List;

import shared.definitions.ResourceType;

/**
 * functions performed by the DevCard monopoly----may assist with facade in functionality
 * or may later realize this is unnecessary.
 * 
 * @author eric
 *
 */
public interface Monopoly extends Development
{
	/**
	 * Just the functionality to pick which resourcetype will be stolen from the other players.
	 * 
	 * @param resource   the type of resource
	 */
	public void pickResource(ResourceType resource);
	
	/**
	 * possibly a way to pass the resources received from other players back to this server.model.player
	 * 
	 * @return list or resourcetypes gained from the other players.
	 */
	public List<ResourceType> receiveResources();
}
