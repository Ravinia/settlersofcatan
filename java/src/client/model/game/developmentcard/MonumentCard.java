package client.model.game.developmentcard;

import shared.definitions.DevCardType;

/**
 * Implementation of the Monument development card
 * @author Lawrence
 */
public class MonumentCard implements Monument
{
    private boolean old = false;
    private DevCardType cardType = DevCardType.MONUMENT;
    public MonumentCard(boolean old)
    {
        this.old = old;
    }
	
	@Override
    public void incrementVictoryPoint()
    {

    }

    @Override
    public DevCardType getDevCardType()
    {
        return DevCardType.MONUMENT;
    }

    @Override
    public boolean isOld()
    {
        return old;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (old ? 1231 : 1237);
		result = prime * result + ((cardType == null) ? 0 : cardType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonumentCard other = (MonumentCard) obj;
		if (cardType != other.cardType)
			return false;
		return true;
	}


}
