package client.model.game.developmentcard;

import shared.definitions.DevCardType;

/**
 * mainly a place to later hold attributes common to development cards
 * and possibly functions convenient for early error checking
 * 
 * @author Eric
 * @author Lawrence
 */
public interface Development
{
	/** returns the type of dev card.
	 * @return   the enum type of this DevCard
	 */
	public DevCardType getDevCardType();

    /**
     * returns whether or not this card has existed for more than one turn
     * @return true if it has, false if you just obtained it this turn
     */
    public boolean isOld();
}
