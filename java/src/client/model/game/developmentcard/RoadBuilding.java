package client.model.game.developmentcard;

import shared.exceptions.InvalidLocationException;
import shared.locations.EdgeLocation;

/**
 * functions performed by the DevCard RoadBuilding----may assist with facade in functionality
 * or may later realize this is unnecessary.
 * @author eric
 *
 */
public interface RoadBuilding extends Development
{
	/**
	 * Chooses the two locations for the roads provided by this card
	 * possibly reference for the place road in the facade
	 * 
	 * @param location1
	 * @param location2
	 */
	public void pickRoadLocations(EdgeLocation location1, EdgeLocation location2) throws InvalidLocationException;

	public abstract EdgeLocation getLocation2();

	public abstract EdgeLocation getLocation1();
}
