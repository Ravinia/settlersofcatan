package client.model.game;

import client.model.game.player.Player;

import java.util.List;

/**
 * Pregame game object that contains a list of the current players, the game's name, and the game id.
 * @author Lawrence
 */
public interface JoinableGame extends Game
{
    public List<Player> getPlayersInGame();
}
