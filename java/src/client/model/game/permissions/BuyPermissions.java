package client.model.game.permissions;

/**
 * The rule enforcement of proper resources being spent on the placeables and devcards
 * @author eric
 *@author lawrence
 */
public interface BuyPermissions {
	
	/**
	 * checks for the availability of resources, and build pool availabilty to buy a road
	 * 
	 * @return     true/false on whether or not server.model.player can purchase a Road
	 */
	public boolean canBuyRoad();
	
	/**
	 *  checks for the availability of resources, and build pool availabilty to buy a settlement
	 *  
	 * @return      true/false on whether or not server.model.player can purchase a Settlement
	 */
	public boolean canBuySettlement();
	
	/**
	 *  checks for the availability of resources, and build pool availabilty to buy a city
	 *  
	 * @return      true/false on whether or not server.model.player can purchase a City
	 */
	public boolean canBuyCity();
	
	/**
	 *  checks for the availability of resources, and build pool availabilty to buy a DevCard
	 *  
	 * @return     true/false on whether or not server.model.player can purchase a DevCard
	 */
	public boolean canBuyDevCard();
}
