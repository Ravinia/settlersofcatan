package client.model.game.permissions;

import java.util.Collection;

import shared.locations.HexLocation;
import shared.exceptions.InvalidLocationException;
import client.model.game.player.Player;

/**
 * checker for the proper rule enforcement of the robber
 * 
 * @author Eric
 *@author Lawrence
 */
public interface RobberPermissions
{
	/**
	 * returns a list of the players who can be robbed by the current server.model.player bi-conditionally
	 * --a settlement being nearby and the server.model.player possessing resource cards.
	 * 
	 * @param location the new desired location of the robber
	 * @return  list of players who can be robbed
	 */
	public Collection<Player> canRob(HexLocation location) throws InvalidLocationException;
	
	/**
	 * Checks whether the robber can be moved to the new location or not.
     * The robber cannot move there if it is not a land hex, or if the robber is already at that location.
	 * 
	 * @param location the new desired location of the robber
	 * @return   true if you can, false otherwise
	 */
	public boolean canMoveRobber(HexLocation location);
}
