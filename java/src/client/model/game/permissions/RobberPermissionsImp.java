package client.model.game.permissions;

import shared.exceptions.InvalidLocationException;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.Hex;
import client.model.game.player.Player;
import shared.locations.HexLocation;

import java.util.Collection;

/**
 * Implementation of the Robber Permission interface.
 * @author Lawrence
 */
public class RobberPermissionsImp implements RobberPermissions
{
    private Board board;

    public RobberPermissionsImp(Board board)
    {
        this.board = board;
    }

    @Override
    public Collection<Player> canRob(HexLocation location) throws InvalidLocationException
    {
        Hex hex = board.getHex(location);
        
        return hex.getPlayersOnThisHex();
    }

    @Override
    public boolean canMoveRobber(HexLocation location)
    {
        //System.out.println(board.getRobberLocation().toString());
    	return board.getRobberLocation() != location && board.isLandHex(location);
    }
}
