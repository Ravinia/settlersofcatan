package client.model.game.permissions;

import shared.locations.EdgeLocation;
import shared.locations.VertexLocation;

/**
 * The checker for whether or not a road, settlement, or city can be placed at a location
 * based on the few restrictions for each.
 * 
 * @author Eric
 *@author Lawrence
 */
public interface BuildPermissions
{
	/**
	 * Checks whether or not a road can be placed at the location based on settlement nearness
	 *  and a road already being in the location.
	 * 
	 * @param location        the EdgeLocation where the server.model.player intends to put the road
	 * @return                true/false on whether or not the road can be placed there
	 */
	public boolean canPlaceRoad(EdgeLocation location);
	
	/**
	 * Checks whether or not a settlement can be placed at the location based on
	 * other settlement nearness and  a road being nearby.
	 * 
	 * @param location        the VertexLocation where the server.model.player intends to put the settlement
	 * @return                true/false on whether or not the settlement can be placed there
	 */
	public boolean canPlaceSettlement(VertexLocation location);
	
	/**
	 * checks whether or not the city can be placed at the location based on
	 * a settlement belonging to the server.model.player being on location.
	 * 
	 * @param location       the VertexLocation where the server.model.player intends to put the city
	 * @return               true/false on whether or not the city can be placed there
	 */
	public boolean canPlaceCity(VertexLocation location);
	
	public boolean canPlaceSettlementRoadTest(VertexLocation location);
}
