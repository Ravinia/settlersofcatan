package client.model.game.permissions;

import shared.definitions.ResourceType;
import client.model.game.MaritimeTradeOffer;
import client.model.game.TradeOffer;
import client.model.game.player.ResourceHand;
import shared.definitions.Phase;
import client.model.game.state.TurnOrder;

public class TradePermissionsImp implements TradePermissions
{
	TradeOffer offer;
	MaritimeTradeOffer maritimeOffer;
	TurnOrder order;
	
    /**
     * Constructor for the Trade Permission Implementation
     * @param state A reference to the state object that contains the current state of the game
     */
	public TradePermissionsImp(TradeOffer offer, TurnOrder order, MaritimeTradeOffer maritimeOffer)
	{
		this.offer = offer;
		this.order = order;
		this.maritimeOffer = maritimeOffer;
	}

	@Override
	public boolean canOfferDomesticTrade()
	{
		//A server.model.player may only offer trades on his/her turn.
		if(order.getCurrentPhase() == Phase.PLAYING && offer != null)
		{
			return countResourceSufficiency(order.getThisPlayer().getRecHand()) == 5 && offer.givesOneResource();
		}
		return false;
	}
	
	/**
	 * Counts the resource sufficiency of a server.model.player's cards.
	 * @param playerHand The cards to count.
	 * @return The number of types of resources that are sufficient (up to 5).
	 */
	private int countResourceSufficiency(ResourceHand playerHand)
	{
		int fiveTests = 0;
		if(playerHand.getBrick() >= offer.getResources().getBrick())
		{
			fiveTests += 1;
		}
		if(playerHand.getOre() >= offer.getResources().getOre())
		{
			fiveTests += 1;
		}
		if(playerHand.getSheep() >= offer.getResources().getSheep())
		{
			fiveTests += 1;
		}
		if(playerHand.getWheat() >= offer.getResources().getWheat())
		{
			fiveTests += 1;
		}
		if(playerHand.getWood() >= offer.getResources().getWood())
		{
			fiveTests += 1;
		}
		
		return fiveTests;
	}
	
	@Override
	public boolean canAcceptTrade()
	{
		//A server.model.player may only accept trades on other players' turns.
		if(order.getCurrentPhase() == Phase.WAIT_PHASE && offer != null)
		{
			if(offer.getReceiverIndex() == order.getThisPlayer().getIndex())
			{
				ResourceHand receiverHand = order.getThisPlayer().getRecHand();
				return acceptTradeResourceTests(receiverHand);
			}
		}
		return false;
	}
	
	/**
	 * Checks if the receiver has sufficient resources to accept the trade.
	 * @param receiverHand The cards of the receiver.
	 * @return true/false
	 */
	private boolean acceptTradeResourceTests(ResourceHand receiverHand)
	{
		
		int fiveTestsExpected = 0;
		int fiveTestsActual = 0;
		//determine number of negative resources
		
		if(offer.getResources().getBrick() < 0)
		{
			fiveTestsExpected +=1;
		}
		if(offer.getResources().getOre() < 0)
		{
			fiveTestsExpected +=1;
		}
		if(offer.getResources().getSheep() < 0)
		{
			fiveTestsExpected +=1;
		}
		if(offer.getResources().getWheat() < 0)
		{
			fiveTestsExpected +=1;
		}
		if(offer.getResources().getWood() < 0)
		{
			fiveTestsExpected +=1;
		}
		
		//see if receiver has the resources
		if(offer.getResources().getBrick() < 0 && receiverHand.getBrick() >= Math.abs(offer.getResources().getBrick()))
		{
			fiveTestsActual += 1;
		}
		if(offer.getResources().getOre() < 0 && receiverHand.getOre() >= Math.abs(offer.getResources().getOre()))
		{
			fiveTestsActual += 1;
		}
		if(offer.getResources().getSheep() < 0 && receiverHand.getSheep() >= Math.abs(offer.getResources().getSheep()))
		{
			fiveTestsActual += 1;
		}
		if(offer.getResources().getWheat() < 0 && receiverHand.getWheat() >= Math.abs(offer.getResources().getWheat()))
		{
			fiveTestsActual += 1;
		}
		if(offer.getResources().getWood() < 0 && receiverHand.getWood() >= Math.abs(offer.getResources().getWood()))
		{
			fiveTestsActual += 1;
		}
		
		return fiveTestsExpected == fiveTestsActual;
	}
	
	@Override
	public boolean canMaritimeTrade(MaritimeTradeOffer offer)
	{
		//A server.model.player may only trade with the bank on his/her turn.
		this.maritimeOffer = offer;
		if(order.getCurrentPhase() == Phase.PLAYING && maritimeOffer != null)
		{
			if(maritimeResourceTests(order.getThisPlayer().getRecHand()))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks that the receiver has enough materials to maritime trade.
	 * @param playerHand The cards of the receiver.
	 * @return true/false
	 */
	private boolean maritimeResourceTests(ResourceHand playerHand)
	{
		ResourceType recType = maritimeOffer.getGivenType();
		switch(recType)
		{
			case WOOD:
				if(playerHand.getWood() > maritimeOffer.getResourcesGiven())
				{
					return true;
				}
				break;
			case BRICK:
				if(playerHand.getBrick() > maritimeOffer.getResourcesGiven())
				{
					return true;
				}
				break;
			case SHEEP:
				if(playerHand.getSheep() > maritimeOffer.getResourcesGiven())
				{
					return true;
				}
				break;
			case WHEAT:
				if(playerHand.getWheat() > maritimeOffer.getResourcesGiven())
				{
					return true;
				}
				break;
			case ORE:
				if(playerHand.getOre() > maritimeOffer.getResourcesGiven())
				{
					return true;
				}
				break;
		}
		return false;
	}
}
