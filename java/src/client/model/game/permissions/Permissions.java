package client.model.game.permissions;

import client.model.game.Bank;
import client.model.game.PlayableGame;
import client.model.game.board.immutable.Board;
import client.model.game.developmentcard.Development;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;
import client.model.game.state.TurnOrder;

/**
 * Master permissions interface that includes all other sub-interfaces
 * for permissions.
 * 
 * @author eric
 *@author lawrence
 */
public interface Permissions extends BuildPermissions, BuyPermissions, RobberPermissions,
	TradePermissions
{
	
	/**
	 * A simple function to check quickly whether or not it is a players turn.
	 * 
	 * @return   true/false
	 */
	public boolean isPlayersTurn();
	
	/**
	 * A function to check if a DevCard can be played based on whether the card
	 * is old enough and if the server.model.player has already played a development card this turn.
	 * 
	 * @return
	 */
	public boolean canPlayDevCard(Development Card);
	
	public boolean canDiscardCards(ResourceHand hand);
	
	public boolean canRollNumber();
	
	public boolean canFinishTurn();

	public Board getBoard();

	public Bank getBank();

	public TurnOrder getOwnTurnOrder();

	public Player getPlayer();

	public PlayableGame getGame();

	public abstract void setBank(Bank bank);
	
}
