package client.model.game.permissions;

import client.model.game.MaritimeTradeOffer;

/**
 * checker for rule enforcement of trades.
 * 
 * @author eric
 *@author lawrence
 */
public interface TradePermissions {
	
	/** 
	 * Checks to see if a server.model.player can offer a domestic trade at the time.
	 * 
	 * @return     true/false
	 */
	public boolean canOfferDomesticTrade();
	
	/**
	 * checks to see if a server.model.player can accept an offered trade.
	 * 
	 * @return    true/false
	 */
	public boolean canAcceptTrade();
	
	/**
	 * checks to see if a server.model.player can initiate a maritime trade with the bank.
	 * 
	 * @return     true/false
	 */
	public boolean canMaritimeTrade(MaritimeTradeOffer offer);
}
