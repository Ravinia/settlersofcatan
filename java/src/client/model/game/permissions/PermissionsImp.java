package client.model.game.permissions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import shared.definitions.DevCardType;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.exceptions.InvalidLocationException;
import client.model.game.Bank;
import client.model.game.MaritimeTradeOffer;
import client.model.game.PlayableGame;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.Edge;
import client.model.game.developmentcard.Development;
import client.model.game.developmentcard.RoadBuilding;
import client.model.game.developmentcard.Soldier;
import client.model.game.developmentcard.YearOfPlenty;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;
import shared.definitions.Phase;
import client.model.game.state.TurnOrder;

/**
 * a place for the implementation of the permissions functions
 * 
 * @author Eric
 * @author Lawrence
 */
public class PermissionsImp implements Permissions
{
	PlayableGame game;
	Player player;
	TurnOrder turnOrder;
	Bank bank;
    Board board;
	
	
	
	public PermissionsImp(PlayableGame game, Player player, TurnOrder turnOrder, Bank bank, Board board)
    {
		this.player = player;
		this.turnOrder = turnOrder;
		this.bank = bank;
        this.board = board;
        this.game = game;
	}

	@Override
	public boolean isPlayersTurn() 
	{
		if(turnOrder.getThisPlayer().getIndex() == turnOrder.getCurrentPlayer().getIndex())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean canPlayDevCard(Development card) 
	{
		if(turnOrder.getCurrentPhase() == Phase.PLAYING && this.isPlayersTurn() &&
				player.getDevHand().getAllPlayableCards().contains(card))
		{
			if(card.getDevCardType() == DevCardType.MONUMENT)
			{
				return true;
			}
			if(!player.isPlayedDevCard())
			{
				if(card.getDevCardType() == DevCardType.YEAR_OF_PLENTY)
				{
					return this.canPlayYearOfPlenty(card);
				}
				if(card.getDevCardType() == DevCardType.SOLDIER)
				{
					return this.canPlaySoldier(card);
				}
				if(card.getDevCardType() == DevCardType.ROAD_BUILD)
				{
					return this.canPlayRoadBuilding(card);
				}
				
				return true;
			}
		}
		
		return false;
		
		
	}
	
	private boolean canPlayYearOfPlenty(Development card)
	{
		YearOfPlenty specific = (YearOfPlenty) card;
		ResourceType type1 = specific.getType1();
		ResourceType type2 = specific.getType2();
		
		int max = 1;
		if(type1 == type2)
		{
			max = 2;
			return this.yearOfPlentySwitch(type1, max);
		}
		else
		{
			return (this.yearOfPlentySwitch(type1, max) && this.yearOfPlentySwitch(type2, max));
		}	
	}
	
	private boolean yearOfPlentySwitch(ResourceType type, int max)
    {
		
		switch(type)
		{
			case WOOD:
				return bank.getWood() >= max;
			case BRICK:
				return bank.getBrick() >= max;
			case SHEEP:
				return bank.getSheep() >= max;
			case WHEAT:
				return bank.getWheat() >= max;
			case ORE:
				return bank.getOre() >= max;
		}
		
		return false;
	}
	
	private boolean canPlaySoldier(Development card)
	{
		Soldier soldier = (Soldier) card;
		if(soldier.getDesiredLocation() == board.getRobberLocation() || 
				turnOrder.getTurnOrder().get(soldier.getRobeeIndex()).getRecHand().getTotalResources() <= 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	private boolean canPlayRoadBuilding(Development card)
	{
        //needs to have at least two roads in build pool
        if (player.getBuildPool().getRoads() < 2)
            return false;

        try
        {
            RoadBuilding roads = (RoadBuilding) card;
            EdgeLocation loc1 = roads.getLocation1();
            EdgeLocation loc2 = roads.getLocation2();

            Edge road1 = board.getEdge(loc1);
            Edge road2 = board.getEdge(loc2);

            //first road must be next to another road
            if (!road1.ownsAnAdjacentRoad(player))
                return false;

            //second road must be adjacent to an existing road, or adjacent to first road
            if (road2.ownsAnAdjacentRoad(player) || road2.isAdjacentToEdge(loc1))
                return true;
            return false;
        }
        catch (InvalidLocationException ignore)
        {
            //if either location is invalid, return false
            return false;
        }
	}
	
	@Override
	public boolean canPlaceRoad(EdgeLocation location) 
	{
		BuildPermissions builder = new BuildPermissionsImp(this.board,this.player,this.turnOrder);
		return builder.canPlaceRoad(location);
	}

	@Override
	public boolean canPlaceSettlement(VertexLocation location) 
	{
		BuildPermissions builder = new BuildPermissionsImp(this.board,this.player,this.turnOrder);
		return builder.canPlaceSettlement(location);
	}

	@Override
	public boolean canPlaceCity(VertexLocation location) 
	{
		BuildPermissions builder = new BuildPermissionsImp(this.board,this.player,this.turnOrder);
		return builder.canPlaceCity(location);
	}

	@Override
	public boolean canBuyRoad() 
	{
		BuyPermissions buyer = new BuyPermissionsImp(this.player.getRecHand(),this.player,this.game.getDeck());
		return buyer.canBuyRoad();
	}

	@Override
	public boolean canBuySettlement() 
	{
		BuyPermissions buyer = new BuyPermissionsImp(this.player.getRecHand(),this.player,this.game.getDeck());
		return buyer.canBuySettlement();
	}

	@Override
	public boolean canBuyCity() 
	{
		BuyPermissions buyer = new BuyPermissionsImp(this.player.getRecHand(),this.player,this.game.getDeck());
		return buyer.canBuyCity();
	}

	@Override
	public boolean canBuyDevCard() 
	{
		BuyPermissions buyer = new BuyPermissionsImp(this.player.getRecHand(),this.player,this.game.getDeck());
		return buyer.canBuyDevCard();
	}

	@Override
	public List<Player> canRob(HexLocation location) throws InvalidLocationException
	{
		RobberPermissions robber = new RobberPermissionsImp(this.board);
		List<Player> canRobByResourcesToo = new ArrayList<Player>();
		
		Collection<Player> canRobByLocation = robber.canRob(location);
		for(Player i : canRobByLocation)
		{
			if(i.getRecHand().getTotalResources() > 0)
			{
				canRobByResourcesToo.add(i);
			}
			
		}
		return canRobByResourcesToo;
	}

	@Override
	public boolean canMoveRobber(HexLocation location) 
	{
		RobberPermissions robber = new RobberPermissionsImp(this.board);
		return robber.canMoveRobber(location);
	}

	@Override
	public boolean canOfferDomesticTrade() 
	{
		TradePermissions trader = new TradePermissionsImp(game.getOffer(),game.getTurnOrder(), new MaritimeTradeOffer(0,0,null,null));
		return trader.canOfferDomesticTrade();
	}

	@Override
	public boolean canAcceptTrade() 
	{
		TradePermissions trader = new TradePermissionsImp(game.getOffer(),game.getTurnOrder(), new MaritimeTradeOffer(0,0,null,null));
		return trader.canAcceptTrade();
	}

	@Override
	public boolean canMaritimeTrade(MaritimeTradeOffer offer) 
	{
		TradePermissions trader = new TradePermissionsImp(game.getOffer(),game.getTurnOrder(), new MaritimeTradeOffer(0,0,null,null));
		return trader.canMaritimeTrade(offer);
	}

	@Override
	public boolean canDiscardCards(ResourceHand discards)
    {
		ResourceHand temp = player.getRecHand(); //simplify large if statement
		if(turnOrder.getCurrentPhase() == Phase.DISCARDING && player.getRecHand().getTotalResources() > 7)
		{
			if(discards.getBrick() <= temp.getBrick() && discards.getOre() <= temp.getOre() && discards.getSheep() <= temp.getSheep()
					&& discards.getWheat() <= temp.getWheat() && discards.getWood() <= temp.getWood())
			{
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean canRollNumber()
    {
		
		if(this.isPlayersTurn() && turnOrder.getCurrentPhase() == Phase.ROLLING)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public boolean canFinishTurn()
    {
		
		if(turnOrder.getCurrentPhase() == Phase.PLAYING && this.isPlayersTurn())
		{
			return true;
		}
		
		return false;
	}

	/**
	 * @return the game
	 */
	@Override
	public PlayableGame getGame()
    {
		return game;
	}

	/**
	 * @return the server.model.player
	 */
	@Override
	public Player getPlayer()
    {
		return player;
	}

	/**
	 * @return the turnOrder
	 */
	@Override
	public TurnOrder getOwnTurnOrder()
    {
		return turnOrder;
	}

	/**
	 * @return the bank
	 */
	@Override
	public Bank getBank()
    {
		return bank;
	}

	/**
	 * @param bank the bank to set. Needed for testing
	 */
	@Override
	public void setBank(Bank bank)
    {
		this.bank = bank;
	}

	/**
	 * @return the board
	 */
	@Override
	public Board getBoard()
    {
		return board;
	}

	@Override
	public boolean canPlaceSettlementRoadTest(VertexLocation location) {
		BuildPermissions builder = new BuildPermissionsImp(this.board,this.player,this.turnOrder);
		return builder.canPlaceSettlementRoadTest(location);
	}
}
