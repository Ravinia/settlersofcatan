package client.model.game.permissions;

import shared.exceptions.InvalidLocationException;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.Vertex;
import client.model.game.board.movable.Settlement;
import client.model.game.player.Player;
import shared.definitions.Phase;
import client.model.game.state.State;
import shared.locations.EdgeLocation;
import shared.locations.VertexLocation;

/**
 * Implementation of the Build Permission Interface.
 * Checks to see if a user can place pieces on specified locations.
 */
public class BuildPermissionsImp implements BuildPermissions
{
    private Board board;
    private Player player;
    private State state;

    /**
     * Constructor for the Build Permissions Implementation.
     * @param board a reference to the Board object
     * @param player a reference to the Player object of the current server.model.player
     * @param state a reference to the State object that represents the current state of the game
     */
    public BuildPermissionsImp(Board board, Player player, State state)
    {
        this.board = board;
        this.player = player;
        this.state = state;
    }

    @Override
    public boolean canPlaceRoad(EdgeLocation location)
    {
        boolean hasRoad = board.hasRoad(location);
        if (isFirstTwoRounds())
        {
           try
           {
               return (!hasRoad && !board.getEdge(location).adjacentToSettlement());
           }
           catch (InvalidLocationException ignore)
           {
               return false;
           }
        }
        else if(state.getCurrentPhase() != Phase.PLAYING)
        {
        	return false;
        }
        else
        {
            if (!hasRoad)
            {
                try
                {
                    return board.getEdge(location).ownsAnAdjacentRoad(player);
                }
                catch (InvalidLocationException ignore)
                {
                    return false;
                }
            }
            return false;
        }
    }

    @Override
    public boolean canPlaceSettlement(VertexLocation location)
    {
    	if(state.getCurrentPhase() == Phase.PLAYING || isFirstTwoRounds())
        {
            try
            {
                Vertex vertex = board.getVertex(location);
                boolean neighbor = vertex.hasNeighboringSettlement();
                boolean adjacent = vertex.ownsAdjacentRoad(player);
                return !neighbor && adjacent;
            }
            catch (InvalidLocationException ignore)
            {
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean canPlaceCity(VertexLocation location)
    {
    	if(state.getCurrentPhase() != Phase.PLAYING)
        {
    		return false;
        }
    	try
        {
            if (board.hasSettlement(location))
            {
            	Settlement settlement = board.getSettlement(location);
                if (! settlement.isCity() && settlement.getPlayer().getPlayerId() == player.getPlayerId())
                    return true;
            }
            return false;
        }
        catch (InvalidLocationException ignore)
        {
            return false;
        }
    }

    private boolean isFirstTwoRounds()
    {
        return state.getCurrentPhase() == Phase.ROUND1 || state.getCurrentPhase() == Phase.ROUND2;
    }

	@Override
	public boolean canPlaceSettlementRoadTest(VertexLocation location) {
		if(state.getCurrentPhase() == Phase.PLAYING || isFirstTwoRounds())
        {
            try
            {
                Vertex vertex = board.getVertex(location);
                boolean neighbor = vertex.hasNeighboringSettlement();
                //boolean adjacent = vertex.ownsAdjacentRoad(server.model.player);
                return !neighbor;
            }
            catch (InvalidLocationException ignore)
            {
                return false;
            }
        }
        return false;
	}
}
