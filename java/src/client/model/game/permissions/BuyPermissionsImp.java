package client.model.game.permissions;

import client.model.game.Deck;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;

public class BuyPermissionsImp implements BuyPermissions
{
	private ResourceHand resources;
	private Player owner;
	private Deck deck;
    /**
     * Constructor for Buy Permissions implementation
     * @param r a reference to the ResourceHand object of the server.model.player
     */
	public BuyPermissionsImp(ResourceHand r, Player owner, Deck deck)
	{
		resources = r;
		this.owner = owner;
		this.deck = deck;
	}
	
	@Override
	public boolean canBuyRoad()
    {
		//check if server.model.player has a road
		if(owner.getBuildPool().getRoads() > 0)
		{
			//Road cost: one brick and one wood
			return resources.getBrick() > 0 && resources.getWood() > 0;
		}
		return false;
	}

	@Override
	public boolean canBuySettlement()
    {
		//check if server.model.player has a settlement
		if(owner.getBuildPool().getSettlements() > 0)
		{
			//Settlement cost: one brick, one wood, one wheat, one sheep
			return resources.getBrick() > 0 && resources.getWood() > 0 && resources.getWheat() > 0 && resources.getSheep() > 0;
		}
		return false;
	}

	@Override
	public boolean canBuyCity()
    {
		//check if server.model.player has a city
		if(owner.getBuildPool().getCities() > 0)
		{
			//City cost: three ore and two wheat
			return resources.getOre() > 2 && resources.getWheat() > 1;
		}
		return false;
	}

	@Override
	public boolean canBuyDevCard()
    {
		//check if deck still has a development card to buy
		int totalDeckSize = deck.getMonopolyCount() + deck.getMonumentCount() + 
				deck.getRoadBuildingCount() + deck.getSoldierCount() + deck.getYearOfPlentyCount();
		if(totalDeckSize > 0)
		{
			//Development card cost: one sheep, one wheat, one ore
			return resources.getSheep() > 0 && resources.getWheat() > 0 && resources.getOre() > 0;
		}
		return false;
	}

}