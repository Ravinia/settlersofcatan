package client.model.game;

/**
 * Total bank of resource cards for a game
 * @author Eric
 */
public interface Bank
{
	/**
	 * @return the brick
	 */
	public int getBrick();

	/**
	 * @return the wood
	 */
	public int getWood();

	/**
	 * @return the sheep
	 */
	public int getSheep();

	/**
	 * @return the wheat
	 */
	public int getWheat();

	/**
	 * @return the ore
	 */
	public int getOre();

}