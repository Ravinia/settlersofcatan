package client.model.game;

import shared.exceptions.PlayerNotFoundException;
import client.model.game.chatlog.ChatLog;
import shared.player.PlayerIdentity;
import client.model.game.player.Player;
import client.model.game.state.TurnOrder;
import client.model.game.state.TurnTracker;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import shared.definitions.CatanColor;

/**
 * A game object that represents an actual catan game.
 * This class holds all of the data relevant to the current game.
 * @author Lawrence
 * @author Eric
 */
public interface PlayableGame extends Game
{
    public int getGameID();

    public String getGameName();

    public void setTurnOrder(TurnOrder turnOrder);

    @Deprecated
    public Player getPlayer(int playerID);

    public Player getPlayer(PlayerIdentity identity) throws PlayerNotFoundException;

    public Player getPlayer(int id, String name) throws PlayerNotFoundException;

    public Player getPlayerByIndex(int index) throws PlayerNotFoundException;

	public CatanColor getColorByName(String name) throws PlayerNotFoundException;

    public Player getMyPlayer();
    
    public List<Player> getAllPlayers();

    public void setPlayers(Collection<Player> players);

    public int getVersion();

    public void setVersion(int version);

    public TurnOrder getTurnOrder();

    public Bank getBank();

    public void setBank(Bank bank);

    public Deck getDeck();

    public void setDeck(Deck deck);

    public ChatLog getChatLog();

    public void setChatLog(ChatLog log);

    public void setWinnerID(int winnerID);

    public Player getWinner();

	public void setOffer(TradeOffer offer);

	public TradeOffer getOffer();

    public TurnTracker getTurnTracker();

    public void setTurnTracker(TurnTracker turnTracker);

    public void updateGame(PlayableGame game);
    
    public void setMyId(int id);
    
    public void setGameId(int id);
    
    public void setGameName(String name);

	Map<PlayerIdentity, Player> getActualPlayers();
    

}
