package client.model.game;

import shared.exceptions.PlayerNotFoundException;
import client.model.game.chatlog.ChatLog;
import shared.player.PlayerIdentity;
import client.model.game.player.Player;
import client.model.game.state.State;
import client.model.game.state.TurnOrder;
import client.model.game.state.TurnTracker;
import shared.definitions.CatanColor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

/**
 * Implementation of the PlayableGame interface
 * @author Lawrence
 */
public class PlayableGameImp extends Observable implements PlayableGame
{
    private TradeOffer offer = null;
    private int version;
    private int winnerID = -1;
    private Bank bank = new BankImp();
    private Deck deck;
    private State state;
    private ChatLog chatLog;
    private TurnOrder turnOrder;

    @Deprecated
    private Map<Integer,Player> players = new HashMap<Integer, Player>();

    private Map<PlayerIdentity, Player> actualPlayers = new HashMap<PlayerIdentity, Player>();

    private TurnTracker tracker;

    private int gameID;
    private String gameName;
    private int myID;

    /**
     * Default constructor with no variable reference set for who the current server.model.player is
     */
    public PlayableGameImp()
    {
        myID = -1;
    }

    /**
     * Initializes the Game object with an id reference for the current server.model.player
     * @param myID the current server.model.player's server.model.player ID
     * @param gameID the ID of the game
     * @param gameName the name of the game
     */
    public PlayableGameImp(int myID, int gameID, String gameName)
    {
        this.myID = myID;
        this.gameID = gameID;
        this.gameName = gameName;
    }

    @Override
    public void notifyObservers()
    {
        super.notifyObservers();
    }

    @Override
    public void updateGame(PlayableGame g)
    {
        PlayableGameImp game = (PlayableGameImp) g;
        myID = game.myID;
        offer = game.getOffer();
        version = game.getVersion();
        if (game.getWinner() != null)
            winnerID = game.getWinner().getIndex();
        bank = game.getBank();
        deck = game.getDeck();
        chatLog = game.getChatLog();
        turnOrder = game.getTurnOrder();
        players = game.getPlayers();
        actualPlayers = game.getActualPlayers();
        tracker = game.getTurnTracker();
        notifyObservers();
    }

    @Override
    public int getGameID()
    {
        return gameID;
    }

    @Override
    public String getGameName()
    {
        return gameName;
    }

    @Override
    public Player getPlayer(int id, String name) throws PlayerNotFoundException
    {
    	try
        {
            return actualPlayers.get(new PlayerIdentity(id, name));
        }
        catch (NullPointerException e)
        {
            throw new PlayerNotFoundException(id);
        }
    }
    
    @Override
    public CatanColor getColorByName(String name) throws PlayerNotFoundException
    {
    	for(Player player : actualPlayers.values())
    	{
    		if(player.getName().compareTo(name) == 0)
    			return player.getColor();
    	}
    	throw new PlayerNotFoundException(-1);
    }

    @Override
    public Player getPlayerByIndex(int index) throws PlayerNotFoundException
    {
        for (Player player : actualPlayers.values())
        {
            if (player.getIndex() == index)
                return player;
        }
        throw new PlayerNotFoundException(index);
    }

    @Override
    @Deprecated
    public Player getPlayer(int playerID)
    {
        return players.get(playerID);
    }

    @Override
    public Player getPlayer(PlayerIdentity identity) throws PlayerNotFoundException
    {
        try
        {
            return actualPlayers.get(identity);
        }
        catch (NullPointerException e)
        {
            throw new PlayerNotFoundException(identity);
        }
    }

    @Override
    public Player getMyPlayer()
    {
        return players.get(myID);
    }

    @Override
    public void setPlayers(Collection<Player> players)
    {
        for (Player player : players)
        {
            this.players.put(player.getPlayerId(), player);
            PlayerIdentity identity = player.getIdentity();
            this.actualPlayers.put(identity, player);
        }
    }

    @Override
    public int getVersion()
    {
        return version;
    }

    @Override
    public void setVersion(int version)
    {
        this.version = version;
    }

    @Override
    public TurnOrder getTurnOrder()
    {
        return turnOrder;
    }

    @Override
    public Bank getBank()
    {
        return bank;
    }

    @Override
    public void setBank(Bank bank)
    {
        this.bank = bank;
    }

    @Override
    public Deck getDeck()
    {
        return deck;
    }

    @Override
    public void setDeck(Deck deck)
    {
        this.deck = deck;
    }

    @Override
    public ChatLog getChatLog()
    {
        return chatLog;
    }

    @Override
    public void setChatLog(ChatLog log)
    {
        chatLog = log;
    }

    @Override
    public void setWinnerID(int winnerID)
    {
        this.winnerID = winnerID;
    }

    @Override
    public Player getWinner()
    {
        if (winnerID == -1)
            return null;
        else
            return getPlayer(winnerID);
    }

    @Override
    public void setTurnOrder(TurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }

	@Override
	public TradeOffer getOffer()
    {
		return offer;
	}

    @Override
    public TurnTracker getTurnTracker()
    {
        return tracker;
    }

    @Override
    public void setTurnTracker(TurnTracker turnTracker)
    {
        this.tracker = turnTracker;
    }

    /**
	 * @param offer the offer to set
	 */
	@Override
	public void setOffer(TradeOffer offer)
    {
		this.offer = offer;
	}

    @Deprecated
    protected Map<Integer, Player> getPlayers()
    {
        return players;
    }

    @Override
	public Map<PlayerIdentity, Player> getActualPlayers()
    {
        return actualPlayers;
    }

	@Override
	public void setMyId(int id)
	{
		this.myID = id;
	}

	@Override
	public void setGameId(int id)
	{
		this.gameID = id;
	}

	@Override
	public void setGameName(String name)
	{
		this.gameName = name;
	}

	@Override
	public List<Player> getAllPlayers() 
	{
		List<Player> result = new ArrayList<Player>();
		for (Player player : actualPlayers.values())
		{
			result.add(player);
		}
		return result;
	}
}
