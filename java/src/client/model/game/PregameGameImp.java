package client.model.game;



public class PregameGameImp implements PregameGame
{

	private String gameName;
	private boolean isRandomTiles; 
	private boolean isRandomTokens;
	private boolean isRandomPorts;

    /**
     * Constructor for creating a new game to send to the server.
     * @param gameName the name of the game
     * @param isRandomTiles whether or not random tiles will be used in the game setup
     * @param isRandomTokens whether or not random tokens will be used in the game setup
     * @param isRandomPorts whether or not random ports will be used in the game setup
     */
	public PregameGameImp(String gameName, boolean isRandomTiles, boolean isRandomTokens, boolean isRandomPorts)
	{
		this.gameName = gameName;
		this.isRandomTiles = isRandomTiles;
		this.isRandomTokens = isRandomTokens;
		this.isRandomPorts = isRandomPorts;
	}
	
	@Override
	public String getGameName()
    {
		return gameName;
	}

    @Override
	public int getGameID()
    {
		return -1;
	}

    @Override
	public boolean isRandomTiles()
    {
		return isRandomTiles;
	}

	@Override
	public boolean isRandomTokens()
    {
		return isRandomTokens;
	}
	
	@Override
	public boolean isRandomPorts()
    {
		return isRandomPorts;
	}

}
