package client.model.game.state;

import shared.definitions.Phase;

/**
 * Implementation of the turn tracker interface
 * @author Lawrence
 */
public class TurnTrackerImp implements TurnTracker
{
    private Phase currentPhase;
    private int currentPlayer;
    private int largestArmy = -1;
    private int longestRoad = -1;

    public TurnTrackerImp(Phase currentPhase, int currentPlayer)
    {
        this.currentPhase = currentPhase;
        this.currentPlayer = currentPlayer;
    }

    public void setLargestArmy(int largestArmy)
    {
        this.largestArmy = largestArmy;
    }

    public void setLongestRoad(int longestRoad)
    {
        this.longestRoad = longestRoad;
    }

    @Override
    public Phase getCurrentPhase()
    {
        return this.currentPhase;
    }

    @Override
    public int getIndexOfPlayerWhoseTurnItIs()
    {
        return this.currentPlayer;
    }

    @Override
    public int getIndexOfPlayerWithLargestArmy()
    {
        return this.largestArmy;
    }

    @Override
    public int getIndexOfPlayerWithLongestRoad()
    {
        return this.longestRoad;
    }
}
