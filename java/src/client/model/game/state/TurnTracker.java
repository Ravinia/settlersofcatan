package client.model.game.state;

import shared.definitions.Phase;

/**
 * Turn tracker interface
 * @author Lawrence
 */
public interface TurnTracker
{
    public Phase getCurrentPhase();

    public int getIndexOfPlayerWhoseTurnItIs();

    public int getIndexOfPlayerWithLargestArmy();

    public int getIndexOfPlayerWithLongestRoad();
}
