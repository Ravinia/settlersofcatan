package client.model.game.state;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import client.model.game.player.Player;
import shared.definitions.Phase;

public class TurnOrderImp implements TurnTracker, TurnOrder, State
{

	private List<Player> turnOrder;
    private List<Player> reverseOrder;
	private int currentPlayer;
	private int thisPlayer; // by index
	private int largestArmy = -1;//index of server.model.player
	private int longestRoad = -1;
    private boolean initialized = false;
    private Phase currentPhase;
	
	public TurnOrderImp()
    {

    }

	public TurnOrderImp(List<Player> turnOrder, int thisPlayer)
    {
        this.turnOrder = turnOrder;
        this.reverseOrder = new ArrayList<Player>(turnOrder);
        Collections.reverse(reverseOrder);

        this.thisPlayer = thisPlayer;
    }
	
	/**
	 * @return this Player
	 */
	@Override
	public Player getThisPlayer()
    {
		return turnOrder.get(thisPlayer);
	}

    @Override
    public void update(TurnTracker tracker)
    {
        this.currentPlayer = tracker.getIndexOfPlayerWhoseTurnItIs();
        this.currentPhase = tracker.getCurrentPhase();
        this.largestArmy = tracker.getIndexOfPlayerWithLargestArmy();
        this.longestRoad = tracker.getIndexOfPlayerWithLongestRoad();
    }

    @Override
    public boolean isInitialized()
    {
        return initialized;
    }

    @Override
	public List<Player> getTurnOrder()
    {
		return turnOrder;
	}

	@Override
	public List<Player> getReverseTurnOrder()
    {
		return reverseOrder;
	}

	@Override
	public Player nextPlayerTurn(Player player)
    {
		if (currentPlayer == turnOrder.size() - 1)
            return turnOrder.get(0);
        else
            return turnOrder.get(currentPlayer + 1);
	}

	@Override
	public Player getCurrentPlayer()
    {
		return turnOrder.get(currentPlayer);
	}

    @Override
    public Phase getCurrentPhase()
    {
        return currentPhase;
    }

    @Override
    public void setPhase(Phase phase)
    {
        this.currentPhase = phase;
    }

    @Override
    public int getIndexOfPlayerWhoseTurnItIs()
    {
        return currentPlayer;
    }

    @Override
    public int getIndexOfPlayerWithLargestArmy()
    {
        return largestArmy;
    }

    @Override
    public int getIndexOfPlayerWithLongestRoad()
    {
        return longestRoad;
    }

	@Override
	public void setCurrentPhase(Phase currentPhase) { // I know these two will likely not be used in practice, I need them for testing
		this.currentPhase = currentPhase;
	}

	@Override
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
}
