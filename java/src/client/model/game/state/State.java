package client.model.game.state;

import shared.definitions.Phase;

/**
 * provides for quick reference to the phase of the game as
 * well as changing the phase from one to the next.
 * 
 * @author eric
 * @author lawrence
 */
public interface State
{
	/**
	 *  returns the current phase of the game---will mainly be used for permission functions
	 * 
	 * @return   Enumerated Type (Phase)
	 */
	public Phase getCurrentPhase();
	
	/**
	 * Sets the phase of the game
	 * 
	 * @param phase--Enum
	 */
	public void setPhase(Phase phase);
	
}
