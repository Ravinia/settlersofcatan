package client.model.game.state;

import java.util.List;

import client.model.game.player.Player;
import shared.definitions.Phase;

/**
 * Will keep track of the server.model.player turn order and provide for reverse turn order
 * during round2 of the setup segment.
 * 
 * @author Eric
 * @author Lawrence
 */
public interface TurnOrder extends TurnTracker, State
{
    /**
     * Whether or not this turn order object is currently initialized.
     * This is important for keeping track when updating the object.
     * An uninitialized object can still perform the basic functions of the turn tracker
     * (if values are updated into it) but is unaware of who the server.model.player on this client is.
     * @return
     */
    public boolean isInitialized();

	/**
	 * Returns the turn order of the game
	 * 
	 * @return  the list of players in their order
	 */
	public List<Player> getTurnOrder();
	
	/**
	 * returns the reverse turn order of the game for round 2 of 
	 * the setup segment
	 * 
	 * @return the list of players in reverse order
	 */
	public List<Player> getReverseTurnOrder();
	
	/**
	 * returns the next server.model.player in the turn order.
	 * 
	 * @param   player----the current server.model.player
	 * @return    the next server.model.player
	 */
	public Player nextPlayerTurn(Player player);

    /**
     * Returns the server.model.player who is currently taking their turn
     * @return Player object
     */
	public Player getCurrentPlayer();

    /**
     * Gets the server.model.player who is currently playing on this client
     * @return Player object
     */
	public Player getThisPlayer();

    /**
     * Updates all values based on info from the turn tracker.
     * This is the only method for putting in values for the current state of the game.
     * @param tracker the turn tracker data carrier
     */
    public void update(TurnTracker tracker);

	public void setCurrentPhase(Phase currentPhase);

	public void setCurrentPlayer(int currentPlayer);
}
