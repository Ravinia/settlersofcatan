package client.model.game.state;

import shared.definitions.Phase;

public class StateImp implements State
{
	
	private Phase currentPhase;
	
	public StateImp(){
		
	}
	public StateImp(Phase currentPhase) {
		this.currentPhase = currentPhase;
	}

	@Override
	public Phase getCurrentPhase()
    {
		return currentPhase;
	}

	@Override
	public void setPhase(Phase phase)
    {
		currentPhase = phase;
	}

}
