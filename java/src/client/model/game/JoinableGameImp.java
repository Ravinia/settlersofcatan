package client.model.game;

import client.model.game.player.Player;

import java.util.List;

/**
 * Implementation of Joinable Game interface.
 * @author Lawrence
 */
public class JoinableGameImp implements JoinableGame
{
    private int gameID;
    private List<Player> players;
    private String gameName;

    /**
     * Default constructor for a Joinable Game.
     * This class should primarily be used for pregame game objects.
     * @param gameID the ID of the game
     * @param players the list of known players currently in the game
     * @param gameName the name of the game
     */
    public JoinableGameImp(int gameID, List<Player> players, String gameName)
    {
        this.gameID = gameID;
        this.players = players;
        this.gameName = gameName;
    }

    @Override
    public List<Player> getPlayersInGame()
    {
        return players;
    }

    @Override
    public int getGameID()
    {
        return gameID;
    }

    @Override
    public String getGameName()
    {
        return gameName;
    }
}
