package client.gui.points;

import client.gui.base.*;

/**
 * Interface for the points view, which displays the user's victory points
 */
public interface IPointsView extends View
{
	
	/**
	 * Sets the number of victory points the server.model.player has
	 * 
	 * @param points
	 *            The number of victory points to display
	 */
	void setPoints(int points);
}

