package client.gui.points;

import java.util.Observable;
import java.util.Observer;

import client.gui.base.*;
import client.gui.misc.IMessageView;
import client.gui.misc.MessageView;
import client.model.ClientModelImp;
import client.model.game.player.Player;


/**
 * Implementation for the points controller
 * @author Jameson
 */
public class PointsController extends CatanController implements IPointsController, Observer
{
	private IGameFinishedView finishedView;
	private ClientModelImp model;
	private IMessageView messageView;
	
	/**
	 * PointsController constructor
	 * 
	 * @param view Points view
	 * @param finishedView Game finished view, which is displayed when the game is over
	 */
	public PointsController(IPointsView view, IGameFinishedView finishedView, ClientModelImp model)
    {
		
		super(view);
		
		setFinishedView(finishedView);
		this.model = model;
		this.messageView = new MessageView();
        ((ClientModelImp)this.model).addObserver(this);
	}
	
	public IPointsView getPointsView()
    {
		
		return (IPointsView)super.getView();
	}
	
	public IGameFinishedView getFinishedView()
    {
		return finishedView;
	}
	public void setFinishedView(IGameFinishedView finishedView)
    {
		this.finishedView = finishedView;
	}

	@Override
	public void update(Observable o, Object arg)
    {
		Player player = model.getGame().getMyPlayer();
		int victoryPoints = player.getVictoryPoints();
		getPointsView().setPoints(victoryPoints);
		
		Player winner = model.getGame().getWinner();
		if (winner != null)
		{
			System.out.println("local server.model.player: " + player.getIndex());
			System.out.println("winner: " + winner.getIndex());
			getFinishedView().setWinner(winner.getName(), winner.getIndex() == player.getIndex());
			getFinishedView().showModal();
		}
	}
	
}

