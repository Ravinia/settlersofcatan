package client.gui.points;

import client.gui.base.*;

/**
 * Interface for the game finished view, which is displayed when the game is
 * over
 */
public interface IGameFinishedView extends OverlayView
{
	
	/**
	 * Sets the information about the winner displayed in the view
	 * 
	 * @param name
	 *            The winner's name
	 * @param isLocalPlayer
	 *            Indicates whether or not the winner is the local server.model.player
	 */
	void setWinner(String name, boolean isLocalPlayer);
	
}

