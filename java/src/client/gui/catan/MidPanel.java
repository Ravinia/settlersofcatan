package client.gui.catan;

import java.awt.*;
import javax.swing.*;

import client.gui.map.*;
import client.model.ClientModel;

@SuppressWarnings("serial")
public class MidPanel extends JPanel
{
	
	private TradePanel tradePanel;
	private MapViewImp mapView;
	private RobViewImp robView;
	private MapControllerImp mapController;
	private GameStatePanel gameStatePanel;
	
	public MidPanel(ClientModel model)
	{
		
		this.setLayout(new BorderLayout());
		
		tradePanel = new TradePanel(model);
		
		mapView = new MapViewImp();
		robView = new RobViewImp();
		mapController = new MapControllerImp(mapView, robView, model);
		mapView.setController(mapController);
		robView.setController(mapController);
		
		gameStatePanel = new GameStatePanel();
		
		this.add(tradePanel, BorderLayout.NORTH);
		this.add(mapView, BorderLayout.CENTER);
		this.add(gameStatePanel, BorderLayout.SOUTH);
		
		this.setPreferredSize(new Dimension(800, 700));
	}
	
	public GameStatePanel getGameStatePanel()
	{
		return gameStatePanel;
	}
	
	public MapController getMapController()
	{
		return mapController;
	}
	
}

