package client.gui.catan;

import java.awt.*;

import javax.swing.*;

import client.gui.domestic.*;
import client.gui.maritime.*;
import client.gui.misc.*;
import client.model.ClientModel;

@SuppressWarnings("serial")
public class TradePanel extends JPanel
{
	
	private DomesticTradeViewImp domesticView;
	private DomesticTradeOverlayImp domesticOverlay;
	private WaitView domesticWaitView;
	private AcceptTradeOverlayImp domesticAcceptOverlay;
	private DomesticTradeControllerImp domesticController;
	
	private MaritimeTradeView maritimeView;
	private MaritimeTradeOverlay maritimeOverlay;
	private MaritimeTradeControllerImp maritimeController;
	
	public TradePanel(ClientModel model)
	{
		
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		domesticView = new DomesticTradeViewImp();
		domesticOverlay = new DomesticTradeOverlayImp();
		domesticWaitView = new WaitView();
		domesticWaitView.setMessage("Waiting for Trade to Go Through");
		domesticAcceptOverlay = new AcceptTradeOverlayImp();
		domesticController = new DomesticTradeControllerImp(domesticView,
														 domesticOverlay,
														 domesticWaitView,
														 domesticAcceptOverlay, model);
		domesticView.setController(domesticController);
		domesticOverlay.setController(domesticController);
		domesticWaitView.setController(domesticController);
		domesticAcceptOverlay.setController(domesticController);
		
		maritimeView = new MaritimeTradeView();
		maritimeOverlay = new MaritimeTradeOverlay();
		maritimeController = new MaritimeTradeControllerImp(maritimeView,
														 maritimeOverlay, model);
		maritimeView.setController(maritimeController);
		maritimeOverlay.setController(maritimeController);
		
		this.setOpaque(true);
		this.setBackground(Color.white);
		
		this.add(Box.createHorizontalGlue());
		this.add(domesticView);
		this.add(Box.createRigidArea(new Dimension(3, 0)));
		this.add(maritimeView);
		this.add(Box.createRigidArea(new Dimension(3, 0)));
	}
	
}

