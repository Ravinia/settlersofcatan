package client.gui.catan;

import javax.swing.*;

import client.gui.base.Action;
import shared.definitions.PieceType;
import client.gui.points.*;
import client.gui.resources.*;
import client.gui.map.*;
import client.gui.devcards.*;
import client.model.ClientModelImp;

@SuppressWarnings("serial")
public class RightPanel extends JPanel
{
	
	private PlayDevCardViewImp playCardView;
	private BuyDevCardViewImp buyCardView;
	private DevCardControllerImp devCardController;
	private PointsView pointsView;
	private GameFinishedView finishedView;
	private PointsController pointsController;
	private ResourceBarView resourceView;
	private ResourceBarController resourceController;
	
	public RightPanel(final MapController mapController, ClientModelImp model)
	{
		
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		
		// Initialize development card views and controller
		//
		playCardView = new PlayDevCardViewImp();
		buyCardView = new BuyDevCardViewImp();
		Action soldierAction = new Action() {
			@Override
			public void execute()
			{
				mapController.playSoldierCard();
			}
		};
		Action roadAction = new Action() {
			@Override
			public void execute()
			{
				mapController.playRoadBuildingCard();
			}
		};
		devCardController = new DevCardControllerImp(playCardView, buyCardView, soldierAction, roadAction, model);
		playCardView.setController(devCardController);
		buyCardView.setController(devCardController);
		
		// Initialize victory point view and controller
		//
		pointsView = new PointsView();
		finishedView = new GameFinishedView();
		pointsController = new PointsController(pointsView, finishedView, model);
		pointsView.setController(pointsController);
		
		// Initialize resource bar view and controller
		//
		resourceView = new ResourceBarView();
		setResourceController(new ResourceBarController(resourceView, model));
		getResourceController().setElementAction(ResourceBarElement.ROAD,
											createStartMoveAction(mapController,
																  PieceType.ROAD));
		getResourceController().setElementAction(ResourceBarElement.SETTLEMENT,
											createStartMoveAction(mapController,
																  PieceType.SETTLEMENT));
		getResourceController().setElementAction(ResourceBarElement.CITY,
											createStartMoveAction(mapController,
																  PieceType.CITY));
		getResourceController().setElementAction(ResourceBarElement.BUY_CARD,
											new Action() {
												@Override
												public void execute()
												{
													devCardController.startBuyCard();
												}
											});
		getResourceController().setElementAction(ResourceBarElement.PLAY_CARD,
											new Action() {
												@Override
												public void execute()
												{
													devCardController.startPlayCard();
												}
											});
		resourceView.setController(getResourceController());
		
		this.add(pointsView);
		this.add(resourceView);
	}
	
	private Action createStartMoveAction(final MapController mapController,
										  final PieceType pieceType)
	{
		
		return new Action() {
			
			@Override
			public void execute()
			{
				boolean isFree = false;
				boolean allowDisconnected = false;
				mapController.startMove(pieceType, isFree, allowDisconnected);
			}
		};
	}

	public ResourceBarController getResourceController() {
		return resourceController;
	}

	public void setResourceController(ResourceBarController resourceController) {
		this.resourceController = resourceController;
	}
	
}

