package client.gui.catan;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import client.gui.discard.DiscardControllerImp;
import client.gui.discard.DiscardViewImp;
import client.model.ClientModelImp;
import shared.definitions.ResourceType;
import client.gui.misc.WaitView;
import client.gui.roll.RollController;
import client.gui.roll.RollResultView;
import client.gui.roll.RollView;

@SuppressWarnings("serial")
public class CatanPanel extends JPanel
{
	private TitlePanel titlePanel;
	private LeftPanel leftPanel;
	private MidPanel midPanel;
	private RightPanel rightPanel;
	
	private DiscardViewImp discardView;
	private WaitView discardWaitView;
	private DiscardControllerImp discardController;
	
	private RollView rollView;
	private RollResultView rollResultView;
	private RollController rollController;
	
	public CatanPanel(ClientModelImp model)
	{
		this.setLayout(new BorderLayout());
		
		titlePanel = new TitlePanel();
		midPanel = new MidPanel(model);
		leftPanel = new LeftPanel(titlePanel, midPanel.getGameStatePanel(), model);
		rightPanel = new RightPanel(midPanel.getMapController(), model);
		model.setMapController(midPanel.getMapController()); // Eric added this, an experiment.
		
		this.add(titlePanel, BorderLayout.NORTH);
		this.add(leftPanel, BorderLayout.WEST);
		this.add(midPanel, BorderLayout.CENTER);
		this.add(rightPanel, BorderLayout.EAST);
		
		discardView = new DiscardViewImp();
		discardWaitView = new WaitView();
		discardWaitView.setMessage("Waiting for other Players to Discard");
		discardController = new DiscardControllerImp(discardView, discardWaitView, model);
		discardView.setController(discardController);
		discardWaitView.setController(discardController);
		
		rollView = new RollView();
		rollResultView = new RollResultView();
		rollController = new RollController(rollView, rollResultView, model);
		rollView.setController(rollController);
		rollResultView.setController(rollController);
		
		JButton testButton = new JButton("Test");
		testButton.addActionListener(new ActionListener() {
			
//			 @Override
//			 public void actionPerformed(ActionEvent e) {
//			
//			 new client.gui.points.GameFinishedView().showModal();
//			 }
//			
//			 @Override
//			 public void actionPerformed(ActionEvent e) {
//			
//			 rollView.showModal();
//			 }
//			
//			 @Override
//			 public void actionPerformed(java.awt.event.ActionEvent
//			 e) {
//			
//			 midPanel.getMapController().startMove(PieceType.ROBBER,
//			 false, false);
//			 }
			
			int state = 0;
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				discardView.setResourceMaxAmount(ResourceType.WOOD, 1);
				discardView.setResourceMaxAmount(ResourceType.BRICK, 0);
				discardView.setResourceMaxAmount(ResourceType.SHEEP, 11);
				discardView.setResourceMaxAmount(ResourceType.WHEAT, 1);
				discardView.setResourceMaxAmount(ResourceType.ORE, 0);
				
				discardView.setResourceAmountChangeEnabled(ResourceType.WOOD, true, false);
				discardView.setResourceAmountChangeEnabled(ResourceType.SHEEP, true, false);
				discardView.setResourceAmountChangeEnabled(ResourceType.WHEAT, true, false);
				
				discardView.setStateMessage("0/6");
				
				discardView.setDiscardButtonEnabled(true);
				
				if(state == 0)
				{
					discardView.showModal();
					state = 1;
				}
				else if(state == 1)
				{
					discardWaitView.showModal();
					state = 2;
				}
			}
		});
		this.add(testButton, BorderLayout.SOUTH);
	}
	
}

