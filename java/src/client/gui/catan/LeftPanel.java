package client.gui.catan;

import java.awt.*;

import javax.swing.*;

import client.gui.communication.*;
import client.gui.turntracker.*;
import client.model.ClientModel;


@SuppressWarnings("serial")
public class LeftPanel extends JPanel {

	private JTabbedPane tabPane;
	private GameHistoryViewImp historyView;
	private GameHistoryControllerImp historyController;
	private ChatViewImp chatView;
        private ChatControllerImp chatController;
	private TurnTrackerView turnView;
	private TurnTrackerController turnController;
	
	public LeftPanel(TitlePanel titlePanel, GameStatePanel gameStatePanel, ClientModel model)
    {
		
		this.setLayout(new BorderLayout());
		
		tabPane = new JTabbedPane();
		Font font = tabPane.getFont();
		Font newFont = font.deriveFont(font.getStyle(), 20);
		tabPane.setFont(newFont);
		
		historyView = new GameHistoryViewImp();
		historyController = new GameHistoryControllerImp(historyView, model);
		historyView.setController(historyController);
		
		chatView = new ChatViewImp();
        chatController = new ChatControllerImp(chatView, model);
        chatView.setController(chatController);
		
		turnView = new TurnTrackerView(titlePanel, gameStatePanel);
		turnController = new TurnTrackerController(turnView, model);
		turnView.setController(turnController);
		
//		gameStatePanel.setController(turnController);
		
		tabPane.add("Game History", historyView);
		tabPane.add("Chat Messages", chatView);
		
		this.add(tabPane, BorderLayout.CENTER);
		this.add(turnView, BorderLayout.SOUTH);

		this.setPreferredSize(new Dimension(350, 700));
	}

	public GameHistoryViewImp getHistoryView() {
		return historyView;
	}

	public ChatViewImp getChatView() {
		return chatView;
	}

	public TurnTrackerView getTurnView() {
		return turnView;
	}

}


