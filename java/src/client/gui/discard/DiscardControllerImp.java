package client.gui.discard;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import shared.definitions.*;
import client.exceptions.BadDiscardException;
import shared.exceptions.NotInitializedException;
import client.facade.PlayerFacade;
import client.facade.PlayerFacadeImp;
import client.gui.base.*;
import client.gui.map.state.DiscardingState;
import client.gui.map.state.MapState;
import client.gui.map.state.StatePattern;
import client.gui.misc.*;
import client.model.ClientModel;
import client.model.game.player.ResourceHand;


/**
 * Discard controller implementation
 */
public class DiscardControllerImp extends CatanController implements DiscardController, Observer, StatePattern
{

	private IWaitView waitView;
	private ClientModel model;
	private ResourceHand playerCards;
	private Map<ResourceType, Integer> resourcesToDiscard;
	private int numCardsToDiscard;
	private int totalToDiscard = 0;
	
	/**
	 * DiscardController constructor
	 * 
	 * @param view View displayed to let the user select cards to discard
	 * @param waitView View displayed to notify the user that they are waiting for other players to discard
	 */
	public DiscardControllerImp(DiscardView view, IWaitView waitView, ClientModel model)
    {
		super(view);
		
		this.waitView = waitView;
		this.model = model;
		((Observable)this.model).addObserver(this);
		this.resourcesToDiscard = new HashMap<ResourceType, Integer>();
		this.resetDiscardValues();
	}

	public DiscardView getDiscardView()
    {
		return (DiscardView)super.getView();
	}
	
	public IWaitView getWaitView()
    {
		return waitView;
	}

	@Override
	public void increaseAmount(ResourceType resource)
    {
		int newResourceNum = this.resourcesToDiscard.get(resource) + 1;
		
		this.setIncreaseDecreaseButton(resource, newResourceNum);
		this.getDiscardView().setResourceDiscardAmount(resource, newResourceNum);
		this.resourcesToDiscard.put(resource, newResourceNum);
		this.totalToDiscard++;
		this.setButtonMessage();
		if(this.numCardsToDiscard == this.totalToDiscard)
		{
			this.setDoneDiscarding();
		}
		else
		{
			this.getDiscardView().setDiscardButtonEnabled(false);
		}
	}

	@Override
	public void decreaseAmount(ResourceType resource)
    {
		int newResourceNum = this.resourcesToDiscard.get(resource) - 1;
		
		this.setIncreaseDecreaseButton(resource, newResourceNum);
		this.getDiscardView().setResourceDiscardAmount(resource, newResourceNum);
		this.resourcesToDiscard.put(resource, newResourceNum);
		this.totalToDiscard--;
		this.setButtonMessage();
		if(this.numCardsToDiscard == this.totalToDiscard)
		{
			this.setDoneDiscarding();
		}
		else
		{
			this.getDiscardView().setDiscardButtonEnabled(false);
			this.setIncreaseDecreaseButton(ResourceType.WOOD, this.resourcesToDiscard.get(ResourceType.WOOD));
			this.setIncreaseDecreaseButton(ResourceType.ORE, this.resourcesToDiscard.get(ResourceType.ORE));
			this.setIncreaseDecreaseButton(ResourceType.SHEEP, this.resourcesToDiscard.get(ResourceType.SHEEP));
			this.setIncreaseDecreaseButton(ResourceType.WHEAT, this.resourcesToDiscard.get(ResourceType.WHEAT));
			this.setIncreaseDecreaseButton(ResourceType.BRICK, this.resourcesToDiscard.get(ResourceType.BRICK));
		}
	}
	
	public void setDoneDiscarding()
	{
		this.getDiscardView().setDiscardButtonEnabled(true);
		this.getDiscardView().setResourceAmountChangeEnabled(ResourceType.WOOD, false, this.getNumToDiscardByResource(ResourceType.WOOD)>0);
		this.getDiscardView().setResourceAmountChangeEnabled(ResourceType.WHEAT, false, this.getNumToDiscardByResource(ResourceType.WHEAT)>0);
		this.getDiscardView().setResourceAmountChangeEnabled(ResourceType.BRICK, false, this.getNumToDiscardByResource(ResourceType.BRICK)>0);
		this.getDiscardView().setResourceAmountChangeEnabled(ResourceType.SHEEP, false, this.getNumToDiscardByResource(ResourceType.SHEEP)>0);
		this.getDiscardView().setResourceAmountChangeEnabled(ResourceType.ORE, false, this.getNumToDiscardByResource(ResourceType.ORE)>0);
	}
	
	public int getNumToDiscardByResource(ResourceType resource)
	{
		return this.resourcesToDiscard.get(resource);
	}
	public void setIncreaseDecreaseButton(ResourceType resource, int currentAmount)
	{
		boolean shouldShowDecrease = this.shouldShowDecrease(resource, currentAmount);
		boolean shouldShowIncrease = this.shouldShowIncrease(resource, currentAmount);
		this.getDiscardView().setResourceAmountChangeEnabled(resource, shouldShowIncrease, shouldShowDecrease);
		
	}
	
	public boolean shouldShowDecrease(ResourceType resource, int currentAmount)
	{
		boolean shouldShow = true;
		if(currentAmount == 0)
		{
			shouldShow = false;
		}
		return shouldShow;
	}
	
	public boolean shouldShowIncrease(ResourceType resource, int currentAmount)
	{
		boolean shouldShow = true;
		if(this.playerCards.getResource(resource) == currentAmount)
		{
			shouldShow = false;
		}
		return shouldShow;
	}
	
	@Override
	public void discard()
    {
		try
		{
			PlayerFacade facade = new PlayerFacadeImp(this.model);
			facade.discardCards(this.resourcesToDiscard);
			this.resetDiscardValues();
			getDiscardView().closeModal();
		}
		catch (NotInitializedException | BadDiscardException e)
		{
			e.printStackTrace();
		}
	}

	public void setButtonMessage()
	{
		this.getDiscardView().setStateMessage(this.totalToDiscard + "/" + this.numCardsToDiscard);
	}
	
	public void initDiscard()
	{
		this.getDiscardView().setResourceMaxAmount(ResourceType.WHEAT, this.playerCards.getWheat());
		this.getDiscardView().setResourceMaxAmount(ResourceType.WOOD, this.playerCards.getWood());
		this.getDiscardView().setResourceMaxAmount(ResourceType.SHEEP, this.playerCards.getSheep());
		this.getDiscardView().setResourceMaxAmount(ResourceType.ORE, this.playerCards.getOre());
		this.getDiscardView().setResourceMaxAmount(ResourceType.BRICK, this.playerCards.getBrick());
		this.setIncreaseDecreaseButton(ResourceType.WOOD, 0);
		this.setIncreaseDecreaseButton(ResourceType.BRICK, 0);
		this.setIncreaseDecreaseButton(ResourceType.SHEEP, 0);
		this.setIncreaseDecreaseButton(ResourceType.ORE, 0);
		this.setIncreaseDecreaseButton(ResourceType.WHEAT, 0);
		this.getDiscardView().setDiscardButtonEnabled(false);
		this.setButtonMessage();
	}
	
	private void resetDiscardValues()
	{
		this.resourcesToDiscard.clear();
		this.resourcesToDiscard.put(ResourceType.BRICK, 0);
		this.resourcesToDiscard.put(ResourceType.ORE, 0);
		this.resourcesToDiscard.put(ResourceType.WHEAT, 0);
		this.resourcesToDiscard.put(ResourceType.SHEEP, 0);
		this.resourcesToDiscard.put(ResourceType.WOOD, 0);
        this.totalToDiscard = 0;
	}
	
	@Override
	public void update(Observable arg0, Object arg1)
	{
		this.playerCards = this.model.getGame().getMyPlayer().getRecHand();
		MapState state = model.getCurrentState();
		if(state instanceof DiscardingState)
		{
			int totalResources = this.model.getGame().getMyPlayer().getRecHand().getTotalResources();
			if(totalResources > 7)
			{
				if(totalResources % 2 == 1)
				{
					totalResources--;
				}
				this.numCardsToDiscard = totalResources/2;
				
				this.resetDiscardValues();
				this.initDiscard();
				
				if(!this.getDiscardView().isModalShowing())
				{
					this.getDiscardView().showModal();
				}
			}
			else
			{
				if(!this.getWaitView().isModalShowing())
				{
					this.getWaitView().showModal();
				}
			}
		}
		else
		{
			if(this.getWaitView().isModalShowing())
			{
				this.getWaitView().closeModal();
			}
			if(this.getDiscardView().isModalShowing())
			{
				this.getDiscardView().closeModal();
			}
		}
	}

}

