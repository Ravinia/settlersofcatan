package client.gui.domestic;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import client.gui.base.*;

/**
 * Implementation of the domestic trade view, which contains the
 * "Domestic Trade" button
 */
@SuppressWarnings("serial")
public class DomesticTradeViewImp extends PanelView implements DomesticTradeView
{
	
	private JButton button;
	
	public DomesticTradeViewImp()
	{
		
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		Font font = new JButton().getFont();
		Font newFont = font.deriveFont(font.getStyle(), 20);
		
		button = new JButton("Domestic Trade");
		button.setFont(newFont);
		button.addActionListener(buttonListener);
		
		this.add(button);
	}
	
	@Override
	public DomesticTradeController getController()
	{
		return (DomesticTradeController)super.getController();
	}
	
	@Override
	public void enableDomesticTrade(boolean value)
	{
		
		button.setEnabled(value);
	}
	
	private ActionListener buttonListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e)
		{
			
			getController().startTrade();
		}
	};
	
}

