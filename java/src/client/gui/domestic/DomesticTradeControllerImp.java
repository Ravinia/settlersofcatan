package client.gui.domestic;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import shared.definitions.*;
import client.exceptions.DeserializationException;
import client.exceptions.InvalidDomesticTradeInput;
import shared.exceptions.NotInitializedException;
import shared.exceptions.PlayerNotFoundException;
import client.facade.TradeFacade;
import client.facade.TradeFacadeImp;
import client.gui.base.*;
import client.gui.data.PlayerInfo;
import client.gui.map.state.PlayingState;
import client.gui.misc.*;
import client.model.ClientModel;
import client.model.ClientModelImp;
import client.model.game.PlayableGame;
import client.model.game.TradeOffer;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;


/**
 * Domestic trade controller implementation
 */
public class DomesticTradeControllerImp extends CatanController implements DomesticTradeController, Observer
{

	private DomesticTradeOverlay tradeOverlay;
	private IWaitView waitOverlay;
	private AcceptTradeOverlay acceptOverlay;
	private ClientModelImp model;
	private Map<ResourceType, Integer> maxResources;
	private PlayableGame game;
	private PlayerInfo[] tradablePlayers;
	private int playerToTradeWith;
	private Map<ResourceType, Integer> giveResources;
	private Map<ResourceType, Integer> getResources;
	private TradeOffer tradeOffer;

	/**
	 * DomesticTradeController constructor
	 * 
	 * @param tradeView Domestic trade view (i.e., view that contains the "Domestic Trade" button)
	 * @param tradeOverlay Domestic trade overlay (i.e., view that lets the user propose a domestic trade)
	 * @param waitOverlay Wait overlay used to notify the user they are waiting for another server.model.player to accept a trade
	 * @param acceptOverlay Accept trade overlay which lets the user accept or reject a proposed trade
	 */
	public DomesticTradeControllerImp(DomesticTradeView tradeView, DomesticTradeOverlay tradeOverlay,
                                      IWaitView waitOverlay, AcceptTradeOverlay acceptOverlay, ClientModel model)
    {

		super(tradeView);
		
		setTradeOverlay(tradeOverlay);
		setWaitOverlay(waitOverlay);
		setAcceptOverlay(acceptOverlay);
		this.model = (ClientModelImp) model;
		this.model.addObserver(this);
		this.game = this.model.getGame();
		this.maxResources = new HashMap<ResourceType, Integer>();
		this.giveResources = new HashMap<ResourceType, Integer>();
		this.getResources = new HashMap<ResourceType, Integer>();
		this.playerToTradeWith = -1;
	}
	
	public void resetDomesticTrade()
	{
		if(this.model.getCurrentState() instanceof PlayingState)
		{
			this.getTradeView().enableDomesticTrade(true);
			this.getTradeOverlay().setResourceSelectionEnabled(true);
			this.getTradeOverlay().setTradeEnabled(false);
			this.getTradeOverlay().setPlayerSelectionEnabled(true);
		}
		else
		{
			this.getTradeView().enableDomesticTrade(false);
			this.getTradeOverlay().setResourceSelectionEnabled(false);
			this.getTradeOverlay().setTradeEnabled(false);
			this.getTradeOverlay().setPlayerSelectionEnabled(false);
		}
	}
	
	public DomesticTradeView getTradeView()
    {
		return (DomesticTradeView)super.getView();
	}

	public DomesticTradeOverlay getTradeOverlay()
    {
		return tradeOverlay;
	}

	public void setTradeOverlay(DomesticTradeOverlay tradeOverlay)
    {
		this.tradeOverlay = tradeOverlay;
	}

	public IWaitView getWaitOverlay()
    {
		return waitOverlay;
	}

	public void setWaitOverlay(IWaitView waitView)
    {
		this.waitOverlay = waitView;
	}

	public AcceptTradeOverlay getAcceptOverlay()
    {
		return acceptOverlay;
	}

	public void setAcceptOverlay(AcceptTradeOverlay acceptOverlay)
    {
		this.acceptOverlay = acceptOverlay;
	}

	@Override
	public void startTrade()
    {
		getTradeOverlay().showModal();
		this.setIncreaseDecreaseAmounts();
		if(this.tradablePlayers == null)
		{
			this.setTradablePlayers();
			getTradeOverlay().setPlayers(this.tradablePlayers);
		}
	}

	public void setIncreaseDecreaseAmounts()
	{
		this.setResourceMaxToTrade();
		boolean incWood = this.maxResources.get(ResourceType.WOOD) > 0;
		boolean incOre = this.maxResources.get(ResourceType.ORE) > 0;
		boolean incBrick = this.maxResources.get(ResourceType.BRICK) > 0;
		boolean incWheat = this.maxResources.get(ResourceType.WHEAT) > 0;
		boolean incSheep = this.maxResources.get(ResourceType.SHEEP) > 0;
		
		getTradeOverlay().setResourceAmountChangeEnabled(ResourceType.WOOD, incWood, false);
		getTradeOverlay().setResourceAmountChangeEnabled(ResourceType.WHEAT, incWheat, false);
		getTradeOverlay().setResourceAmountChangeEnabled(ResourceType.BRICK, incBrick, false);
		getTradeOverlay().setResourceAmountChangeEnabled(ResourceType.SHEEP, incSheep, false);
		getTradeOverlay().setResourceAmountChangeEnabled(ResourceType.ORE, incOre, false);
	}
	
	@Override
	public void decreaseResourceAmount(ResourceType resource)
    {
		if(this.giveResources.containsKey(resource))
		{
			this.giveResources.put(resource, this.giveResources.get(resource)-1);
			this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, true);
			if(this.giveResources.get(resource) == 0)
			{
				this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, false);
			}
		}
		else
		{
			this.getResources.put(resource, this.getResources.get(resource)-1);
			if(this.getResources.get(resource) == 0)
			{
				this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, false);
			}
		}
		this.setTradeEnabled();
	}

	@Override
	public void increaseResourceAmount(ResourceType resource)
    {
		this.setResourceMaxToTrade();
		if(this.giveResources.containsKey(resource))
		{
			this.giveResources.put(resource, this.giveResources.get(resource)+1);
			if(this.giveResources.get(resource) == this.maxResources.get(resource))
			{
				this.getTradeOverlay().setResourceAmountChangeEnabled(resource, false, true);
			}
			else
			{
				this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, true);
			}
		}
		else
		{
			this.getResources.put(resource, this.getResources.get(resource)+1);
			this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, true);
		}
		this.setTradeEnabled();
	}

	@Override
	public void sendTradeOffer()
    {
		try
		{
			if(this.giveResources == null || this.getResources == null || this.playerToTradeWith == -1)
			{
				throw new InvalidDomesticTradeInput();
			}
			
				TradeFacade trade = new TradeFacadeImp(this.model);
				
			    Map<ResourceType, Integer> offer = this.constructOffer();
				trade.offerTrade(this.playerToTradeWith, offer);
				this.getResources.clear();
				this.playerToTradeWith = -1;
				this.maxResources.clear();
				this.getTradeOverlay().reset();
				this.getTradeOverlay().closeModal();
		}
		catch(InvalidDomesticTradeInput | NotInitializedException | PlayerNotFoundException | DeserializationException e)
		{
			e.printStackTrace();
		}
		this.getWaitOverlay().showModal();
	}

	public Map<ResourceType, Integer> constructOffer()
	{
		Map<ResourceType, Integer>  offer = new HashMap<ResourceType, Integer>();
		Iterator getIt = this.getResources.entrySet().iterator();
	    while (getIt.hasNext()) {
	        Map.Entry pairs = (Map.Entry)getIt.next();
	        offer.put((ResourceType)pairs.getKey(),-(Integer)pairs.getValue());
	        getIt.remove(); 
	    }
	    
	    Iterator giveIt = this.giveResources.entrySet().iterator();
	    while (giveIt.hasNext()) {
	        Map.Entry pairs = (Map.Entry)giveIt.next();
	        offer.put((ResourceType)pairs.getKey(),(Integer)pairs.getValue());
	        giveIt.remove(); 
	    }
	    return offer;
	}
	@Override
	public void setPlayerToTradeWith(int playerIndex)
    {
		this.playerToTradeWith = playerIndex;
		this.setTradeEnabled();
	}

	public void setTradeEnabled()
	{
		if(this.playerToTradeWith == -1)
		{
			this.getTradeOverlay().setTradeEnabled(false);
		}
		else if(!this.getResources.isEmpty() && !this.giveResources.isEmpty())
		{
			this.getTradeOverlay().setTradeEnabled(true);
		}
		else
		{
			this.getTradeOverlay().setTradeEnabled(false);
		}
	}
	@Override
	public void setResourceToReceive(ResourceType resource)
    {
		this.giveResources.remove(resource);
		this.getResources.put(resource, 0);
		this.getTradeOverlay().setResourceAmount(resource, "0");
		this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, false);
		this.setTradeEnabled();
	}

	@Override
	public void setResourceToSend(ResourceType resource)
    {
		this.setResourceMaxToTrade();
		this.getResources.remove(resource);
		this.giveResources.put(resource, 0);
		this.getTradeOverlay().setResourceAmount(resource, "0");
//		this.setIncreaseDecreaseAmounts();
		this.setTradeEnabled();
	}

	@Override
	public void unsetResource(ResourceType resource)
    {
		this.getResources.remove(resource);
		this.giveResources.remove(resource);
		this.setTradeEnabled();
	}

	@Override
	public void cancelTrade()
    {
		this.getResources.clear();
		this.playerToTradeWith = -1;
		this.maxResources.clear();
		this.getTradeOverlay().reset();
		getTradeOverlay().closeModal();
	}

	@Override
	public void acceptTrade(boolean willAccept)
    {
		try
		{
			TradeFacade tradeFacade = new TradeFacadeImp(this.model);
			if(this.tradeOffer == null)
			{
				throw new InvalidDomesticTradeInput();
			}
			tradeFacade.acceptTrade(willAccept, this.tradeOffer.getReceiverIndex());
			this.getResources.clear();
			this.playerToTradeWith = -1;
			this.maxResources.clear();
			this.getAcceptOverlay().reset();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		this.tradeOffer = null;
		getAcceptOverlay().closeModal();
	}

	public void setResourceMaxToTrade()
	{
		this.maxResources.clear();
		int playerID = -1;
		String playerName = "";
		Player player = null;
		try
		{
			playerID = this.model.getLocalUserID();
			playerName = this.model.getLocalUser().getName();
			player = this.model.getGame().getPlayer(playerID, playerName);
		}
		catch (NotInitializedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (PlayerNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ResourceHand resourceHand = player.getRecHand();
		
		int brick = resourceHand.getBrick();
		int ore = resourceHand.getOre();
		int sheep = resourceHand.getSheep();
		int wood = resourceHand.getWood();
		int wheat = resourceHand.getWheat();
		
		this.maxResources.put(ResourceType.WOOD, wood);
		this.maxResources.put(ResourceType.WHEAT, wheat);
		this.maxResources.put(ResourceType.BRICK, brick);
		this.maxResources.put(ResourceType.ORE, ore);
		this.maxResources.put(ResourceType.SHEEP, sheep);
	}
	
	public void setTradablePlayers()
	{
		try
		{
			int localPlayerId = this.model.getLocalUserID();
			String localPlayerName = this.model.getLocalUser().getName();
			
			this.tradablePlayers = new PlayerInfo[3];
			List<Player> players = this.game.getAllPlayers();
			int arrayIndex = 0;
			for(int i = 0; i < players.size(); i++)
			{
				Player player = players.get(i);
				if(player.getIndex() != this.game.getPlayer(localPlayerId, localPlayerName).getIndex())
				{
					this.tradablePlayers[arrayIndex] = new PlayerInfo(player.getPlayerId(), player.getIndex(), player.getName(), player.getColor());
					arrayIndex++;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public boolean isLocalOffer(int recipientIndex)
	{
		try
		{
			String userName = this.model.getLocalUser().getName();
			int userId = this.model.getLocalUserID();
			int userIndex = this.game.getPlayer(userId, userName).getIndex();
			
			return recipientIndex == userIndex;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}
	public void handleOffer(TradeOffer offer)
	{
		try
		{
			int receiverIndex = offer.getReceiverIndex();
			if(this.isLocalOffer(receiverIndex))
			{
				this.tradeOffer = offer;
				this.showOffer(offer);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void showOffer(TradeOffer offer)
	{
		int brick = offer.getResources().getBrick();
		int wheat = offer.getResources().getWheat();
		int wood = offer.getResources().getWood();
		int sheep = offer.getResources().getSheep();
		int ore = offer.getResources().getOre();
		
		Map<ResourceType, Integer> resources = new HashMap<ResourceType, Integer>();
		Map<ResourceType, Integer> copy = new HashMap<ResourceType, Integer>();
		resources.put(ResourceType.BRICK, brick);
		resources.put(ResourceType.WOOD, wood);
		resources.put(ResourceType.WHEAT, wheat);
		resources.put(ResourceType.SHEEP, sheep);
		resources.put(ResourceType.ORE, ore);
		copy.put(ResourceType.BRICK, brick);
		copy.put(ResourceType.WOOD, wood);
		copy.put(ResourceType.WHEAT, wheat);
		copy.put(ResourceType.SHEEP, sheep);
		copy.put(ResourceType.ORE, ore);
		
		if(!this.getAcceptOverlay().isModalShowing())
		{
			boolean canAcceptOffer = this.canAcceptOffer(copy);
			Iterator it = resources.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        ResourceType key = (ResourceType)pairs.getKey();
		        Integer value = (Integer)pairs.getValue();
		        if(value > 0)
		        {
		        	
		        	this.getAcceptOverlay().addGetResource(key, value);
		        }
		        else if(value < 0)
		        {
		        	this.getAcceptOverlay().addGiveResource(key, -value);
		        }
		        it.remove(); 
		    }
		    try
		    {
		    Player sender = this.game.getPlayerByIndex(offer.getSenderIndex());
		    this.getAcceptOverlay().setPlayerName(sender.getName());
		    }
		    catch (Exception e)
		    {
		    	e.printStackTrace();
		    }
		    this.getAcceptOverlay().setAcceptEnabled(canAcceptOffer);
			this.getAcceptOverlay().showModal();
		}
	}

	public boolean canAcceptOffer(Map<ResourceType, Integer> offer)
	{
		try
		{
			String userName = this.model.getLocalUser().getName();
			int userId = this.model.getLocalUserID();
			Player player = this.game.getPlayer(userId, userName);
			ResourceHand resourceHand = player.getRecHand();
			boolean canAccept = true;
			Iterator it = offer.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        ResourceType key = (ResourceType)pairs.getKey();
		        Integer value = (Integer)pairs.getValue();
		        
		        switch (key)
		        {
		        	case WOOD:
		        		if(value < 0 && resourceHand.getWood() < -value)
		        		{
		        			canAccept = false;
		        		}
		        		break;
		        	case WHEAT:
		        		if(value < 0 && resourceHand.getWheat() < -value)
		        		{
		        			canAccept = false;
		        		}
		        		break;
		        	case BRICK:
		        		if(value < 0 && resourceHand.getBrick() < -value)
		        		{
		        			canAccept = false;
		        		}
		        		break;
		        	case SHEEP:
		        		if(value < 0 && resourceHand.getSheep() < -value)
		        		{
		        			canAccept = false;
		        		}
		        		break;
		        	case ORE:
		        		if(value < 0 && resourceHand.getOre() < -value)
		        		{
		        			canAccept = false;
		        		}
		        		break;
		        }
		        it.remove(); 
		    }
		    return canAccept;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		return false;
	}
	
	@Override
	public void update(Observable arg0, Object arg1)
	{
		this.setResourceMaxToTrade();
		this.resetDomesticTrade();
		TradeOffer offer = this.game.getOffer();
		if(offer != null)
		{
			this.handleOffer(offer);
		}
		if(offer == null && this.getWaitOverlay().isModalShowing())
		{
			this.getWaitOverlay().closeModal();
//			this.getTradeOverlay().closeModal();
			this.getTradeOverlay().reset();
		} else if(offer != null && offer.getSenderIndex() == this.game.getMyPlayer().getIndex() && !this.getWaitOverlay().isModalShowing())
		{
			this.getWaitOverlay().showModal();
		}
	}

}

