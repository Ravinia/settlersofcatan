package client.gui.login;

import client.exceptions.BadPasswordException;
import client.exceptions.BadUsernameException;
import client.exceptions.LoginException;
import client.exceptions.MatchingPasswordException;
import client.facade.LoginFacade;
import client.gui.base.*;
import client.gui.misc.*;
import client.model.ClientModel;


/**
 * Implementation for the login controller
 */
public class LoginController extends CatanController implements ILoginController
{
	private IMessageView messageView;
	private Action loginAction;
    private LoginFacade pregame;
	
	/**
	 * LoginController constructor
	 * 
	 * @param view Login view
	 * @param messageView Message view (used to display error messages that occur during the login process)
	 */
	public LoginController(ILoginView view, IMessageView messageView, ClientModel model)
    {
		super(view);
		
		this.messageView = messageView;
        this.pregame = model.getLoginFacade();
	}
	
	public ILoginView getLoginView()
    {
		
		return (ILoginView)super.getView();
	}
	
	public IMessageView getMessageView()
    {
		return messageView;
	}
	
	/**
	 * Sets the action to be executed when the user logs in
	 * 
	 * @param value The action to be executed when the user logs in
	 */
	public void setLoginAction(Action value)
    {
		loginAction = value;
	}
	
	/**
	 * Returns the action to be executed when the user logs in
	 * 
	 * @return The action to be executed when the user logs in
	 */
	public Action getLoginAction()
    {
		return loginAction;
	}

	@Override
	public void start()
    {
		getLoginView().showModal();
	}

	@Override
	public void signIn()
    {
		try
        {
            String user = getLoginView().getLoginUsername();
            String password = getLoginView().getLoginPassword();

            pregame.login(user, password);

            // If log in succeeded
            getLoginView().closeModal();
            loginAction.execute();
        }
        catch (LoginException e)
        {
            messageView.setTitle("Login Error");
            messageView.setMessage("Your username and/or password are incorrect. Have you registered? If so, make sure you have typed your username and password correctly.");
            messageView.showModal();
        }
	}

	@Override
	public void register()
    {
		try
		{
			String username = getLoginView().getRegisterUsername();
			String password = getLoginView().getRegisterPassword();
			String passwordRepeat = getLoginView().getRegisterPasswordRepeat();
			
			if(!this.pregame.isValidUsername(username))
			{
				throw new BadUsernameException();
			}
			if(!this.pregame.isValidPassword(password))
			{
				throw new BadPasswordException();
			}
			if(!password.equals(passwordRepeat))
			{
				throw new MatchingPasswordException();
			}
			pregame.register(username, password);
			// If register succeeded
			getLoginView().closeModal();
			loginAction.execute();
		}
		catch(LoginException e)
		{
			messageView.setTitle("Registration Error");
            messageView.setMessage("There was some sort of error. Try registering with a different username");
            messageView.showModal();
		}
		catch(MatchingPasswordException e)
		{
			messageView.setTitle("Your Passwords Did Not Match");
            messageView.setMessage("The passwords you entered do not match, please try again.");
            messageView.showModal();
		}
		catch(BadUsernameException e)
		{
			messageView.setTitle("Your Username Was Not In The Right Format");
            messageView.setMessage("Your username must be between 3 and 7 characters and only contain letters, digits, underscores, and dashes");
            messageView.showModal();
		}
		catch(BadPasswordException e)
		{
			messageView.setTitle("Your Password Was Not In The Right Format");
            messageView.setMessage("Your password must be 5 or more characters and only contain letters, digits, underscores, and dashes");
            messageView.showModal();
		}
		
	}

}

