package client.gui.map;

import shared.exceptions.CatanException;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidRobberLocationException;
import client.gui.map.state.*;
import client.model.ClientModel;
import client.model.ClientModelImp;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.Hex;
import client.model.game.board.immutable.ports.Port;
import client.model.game.board.movable.Road;
import client.model.game.board.movable.Settlement;
import client.model.game.board.movable.SettlementPiece;
import client.model.game.player.Player;
import shared.definitions.*;
import shared.locations.*;
import client.gui.base.*;
import client.gui.data.*;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Implementation for the map controller
 */
public class MapControllerImp extends CatanController implements MapController, Observer, StatePattern
{
	private RobView robView;
    private ClientModelImp model;
    private Logger logger;

	public MapControllerImp(MapView view, RobView robView, ClientModel model)
    {
		super(view);
        this.model = (ClientModelImp) model;
		
		setRobView(robView);

		logger = Logger.getLogger("MapControllerLogger");
		logger.setLevel(Level.FINEST);
		
        this.model.addObserver(this);
		
//		initFromModel(); //TODO: is this line needed?
	}
	
	public MapView getView()
    {
		return (MapView)super.getView();
	}
	
	public RobView getRobView()
    {
		return robView;
	}
	private void setRobView(RobView robView)
    {
		this.robView = robView;
	}
	
	protected void initFromModel()
    {
        try
        {
            Board board = model.getBoard();

            //Initialize Hexes
            for (Hex hex : board)
            {
                displayHex(hex);
            }

            //Initialize Ports
            for (Port port : board.getPorts())
            {
                displayPort(port);
            }

            //Place Roads
            for (Road road : board.getRoads())
            {
                displayRoad(road);
            }

            //Place Settlements and Cities
            for (Settlement settlement : board.getSettlements())
            {
                displaySettlement(settlement);
            }

            //Place robber
            displayRobber(board.getRobberLocation());
        }
        catch (CatanException e)
        {
            e.printStackTrace();
        }
	}

	public boolean canPlaceRoad(EdgeLocation edgeLoc)
    {
		return model.getCurrentState().canPlaceRoad(edgeLoc);
	}

	public boolean canPlaceSettlement(VertexLocation vertLoc)
    {
		return model.getCurrentState().canPlaceSettlement(vertLoc);
	}

	public boolean canPlaceCity(VertexLocation vertLoc)
    {
		return model.getCurrentState().canPlaceCity(vertLoc);
	}

	public boolean canPlaceRobber(HexLocation hexLoc)
    {
		return model.getCurrentState().canPlaceRobber(hexLoc);
	}

	public void placeRoad(EdgeLocation edgeLoc)
    {
		model.getCurrentState().placeRoad(edgeLoc);
	}

	public void placeSettlement(VertexLocation vertLoc)
    {
		model.getCurrentState().placeSettlement(vertLoc);
	}

	public void placeCity(VertexLocation vertLoc)
    {
		model.getCurrentState().placeCity(vertLoc);
	}

	public void placeRobber(HexLocation hexLoc)
    {
		try
        {         
			model.getCurrentState().placeRobber(hexLoc);
        }
        catch ( InvalidLocationException e)
        {
            e.printStackTrace();
        }
	}
	
	public void startMove(PieceType pieceType, boolean isFree, boolean allowDisconnected)
    {
		
		model.getCurrentState().startMove(pieceType, isFree, allowDisconnected);
	}
	
	public void cancelMove()
    {
		System.out.println("WARNING! CANCEL MOVE CALLED, AND THIS METHOD IS EMPTY");
	}
	
	public void playSoldierCard()
	{
		getView().startDrop(PieceType.ROBBER, model.getGame().getMyPlayer().getColor(), false);
		try 
		{
			model.getCurrentState().playSoldierCard();
		} 
		catch (InvalidRobberLocationException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void playRoadBuildingCard()
    {
		model.getCurrentState().playRoadBuildingCard();
	}
	
	public void robPlayer(RobPlayerInfo victim)
    {
		try
        {
            model.getCurrentState().robPlayer(victim);
            getRobView().closeModal();
        }
        catch (InvalidRobberLocationException e)
        {
            //TODO: display message?
            //getRobView().showModal();
        	e.printStackTrace();
        }
	}

    @Override
    public void update(Observable o, Object arg)
    {
        if (isGameStarted())
        {
        	Player player = model.getGame().getMyPlayer();
        	logger.info("running update");
            initFromModel();
            System.out.println("Map Controller notified!");
        	
            if (model.getCurrentState() instanceof PlacingRoad)
            {
            	logger.info("starting place road move");
                this.startMove(PieceType.ROAD, true, true);
            }
            else if (model.getCurrentState() instanceof PlacingSettlement)
            {
            	logger.info("starting place settlement move");
            	this.startMove(PieceType.SETTLEMENT, true, true);
            }
            else if (model.getCurrentState() instanceof RobbingState)
            {
            	logger.info("starting robber drop");
            	System.out.println("Robbing!!!!");
                getView().startDrop(PieceType.ROBBER, player.getColor(), false);
            }
            if(model.getCurrentState() instanceof PlayingState)
            {
            	PlayingState temp = (PlayingState) model.getCurrentState();
            	temp.setRobberLocation();
            }
            this.getView().placeRobber(model.getBoard().getRobberLocation());
        }
    }

    //Helper Functions
    private void displayHex(Hex hex)
    {
        getView().addHex(hex.getLocation(), hex.getType());
        if (hex.getType() != HexType.DESERT && hex.getType() != HexType.WATER)
            getView().addNumber(hex.getLocation(), hex.getToken().getValue());
    }

    private void displayPort(Port port)
    {
        EdgeLocation location = port.getLocation().getOriginalEdge();
        PortType type = port.getPortType();
        getView().addPort(location, type);
    }

    private void displayRoad(Road road)
    {
        EdgeLocation location = road.getLocation();
        CatanColor color = road.getPlayer().getColor();
        getView().placeRoad(location, color);
    }

    private void displaySettlement(Settlement settlement)
    {
        VertexLocation location = settlement.getLocation();
        CatanColor color = settlement.getPlayer().getColor();
        if (settlement instanceof SettlementPiece)
            getView().placeSettlement(location, color);
        else
            getView().placeCity(location, color);
    }

    private void displayRobber(HexLocation robber)
    {
        getView().placeRobber(robber);
    }

    /**
     * Checks whether there are 4 players in the game, then the game should have started
     */
    private boolean isGameStarted()
    {
        return model.getGame().getAllPlayers().size() == 4;
    }
}

