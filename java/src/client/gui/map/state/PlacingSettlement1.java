package client.gui.map.state;

import client.gui.map.MapControllerImp;
import client.model.ClientModel;

/**
 * @author Lawrence
 */
public class PlacingSettlement1 extends Setup1State implements PlacingSettlement
{
    public PlacingSettlement1(ClientModel model, MapControllerImp controller)
    {
        super(model, controller);
    }
}
