package client.gui.map.state;

import client.exceptions.*;
import client.model.ClientModel;
import shared.exceptions.InvalidObjectTypeException;

import java.util.Random;

public class RollingState extends MapState
{
	
    public RollingState(ClientModel model)
    {
        super(model);
    }

    @Override
    public int roll() throws CannotPerformOperationException
    {
        try
        {
            int roll = getDieRoll();
            model.getPlayerFacade().rollNumber(roll);
            return roll;
        }
        catch (UnexpectedResponseException | DeserializationException | InvalidObjectTypeException e)
        {
            throw new CannotPerformOperationException("Unable to perform roll due to unexpected error: ",e);
        }

    }
    
//    private int getDieRoll()  //you can dodge a roll of seven with this implementation
//    {
//    	int temp =  rollSingleDie() + rollSingleDie();
//        while(temp == 7)
//        	temp = getDieRoll();
//        return temp;
//    }
    
    private int getDieRoll()
    {
    	return rollSingleDie() + rollSingleDie();
//    	return 7;
    }

    private int rollSingleDie()
    {
        Random random = new Random();
        int result = random.nextInt(6); //between 0 and 5
        result++; //increment to be between 1 and 6
        return result;
    }
}
