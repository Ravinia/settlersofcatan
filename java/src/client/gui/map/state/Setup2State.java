package client.gui.map.state;

import java.util.List;

import shared.definitions.CatanColor;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.VertexLocation;
import client.exceptions.BuildRoadException;
import client.exceptions.BuildSettlementException;
import client.exceptions.FinishTurnException;
import shared.exceptions.InvalidLocationException;
import client.facade.BuildFacade;
import client.facade.PlayerFacade;
import client.gui.map.MapControllerImp;
import client.model.ClientModel;
import client.model.game.board.immutable.Edge;
import client.model.game.board.immutable.Vertex;
import client.model.game.permissions.Permissions;

public class Setup2State extends MapState
{
	Permissions permissions;
	MapControllerImp controller;
	
    public Setup2State(ClientModel model, MapControllerImp controller)
    {
        super(model);
        permissions = model.getPermissions();
        this.controller = controller;
    }
    
    @Override
    public boolean canPlaceRoad(EdgeLocation edgeLoc)
    {
    	EdgeLocation noEdLo = edgeLoc.getNormalizedLocation();
    	boolean canPlaceSettlement = false;
    	try {
			Edge edge = model.getBoard().getEdge(noEdLo);
			List<Vertex> endVertices = edge.getTwoAdjacentVertices();
			for(Vertex i : endVertices)
			{
				if(canPlaceSettlement != true)
				{
					canPlaceSettlement = permissions.canPlaceSettlementRoadTest(i.getLocation());
				}
			}
		} catch (InvalidLocationException e) {
			//e.printStackTrace(); useless to perpetuate---only throws on water tiles.
		}	
    	return permissions.canPlaceRoad(edgeLoc) && canPlaceSettlement;
    }

    @Override
    public boolean canPlaceSettlement(VertexLocation vertLoc)
    {
        return permissions.canPlaceSettlement(vertLoc);
    }

    @Override
    public void placeRoad(EdgeLocation edgeLoc)
    {
    	BuildFacade builder = model.getBuildFacade();
		try 
		{
			builder.buildRoad(edgeLoc, true);
		} 
		catch (BuildRoadException e) 
		{
				e.printStackTrace();
		}
    }

    @Override
    public void placeSettlement(VertexLocation vertLoc)
    {
        BuildFacade builder = model.getBuildFacade();
        PlayerFacade playerer = model.getPlayerFacade();
        try 
		{
			builder.buildSettlement(vertLoc, true);
			playerer.finishTurn();
		} 
		catch (BuildSettlementException | FinishTurnException e) 
		{
				e.printStackTrace();
		}
    }
    @Override
    public void startMove(PieceType pieceType, boolean isFree, boolean allowDisconnected)
    {
    	CatanColor color = model.getGame().getMyPlayer().getColor();
    	controller.getView().startDrop(pieceType, color, false);
    }
}
