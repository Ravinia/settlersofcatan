package client.gui.map.state;

/**
 * Simple interface for reference during the first and second phase, that the server.model.player should be placing a road right now
 */
public interface PlacingRoad
{
}
