package client.gui.map.state;

import client.gui.map.MapControllerImp;
import client.model.ClientModel;

/**
 * @author Lawrence
 */
public class PlacingSettlement2 extends Setup2State implements PlacingSettlement
{
    public PlacingSettlement2(ClientModel model, MapControllerImp controller)
    {
        super(model, controller);
    }
}
