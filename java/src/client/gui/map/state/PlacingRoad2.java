package client.gui.map.state;

import client.gui.map.MapControllerImp;
import client.model.ClientModel;

/**
 * @author Lawrence
 */
public class PlacingRoad2 extends Setup2State implements PlacingRoad
{
    public PlacingRoad2(ClientModel model, MapControllerImp controller)
    {
        super(model, controller);
    }
}
