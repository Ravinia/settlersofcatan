package client.gui.map.state;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.HttpResponse;

import client.exceptions.DeserializationException;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.exceptions.NotInitializedException;
import client.facade.PlayerFacade;
import client.gui.data.RobPlayerInfo;
import client.gui.map.MapControllerImp;
import client.model.ClientModel;
import client.model.game.player.Player;
import shared.locations.HexLocation;

public class RobbingState extends MapState
{
    private PlayerFacade facade;
    private HexLocation robberLocation;
    private MapControllerImp controller;

    public RobbingState(ClientModel model, MapControllerImp controller)
    {
        super(model);
        facade = model.getPlayerFacade();
        this.controller = controller;
    }
    
    public boolean canPlaceRobber(HexLocation hexLoc)
	{
		return model.getPermissions().canMoveRobber(hexLoc);
	}
    @Override
    public void robPlayer(RobPlayerInfo victim) throws InvalidRobberLocationException
    {
        //System.out.println(robberLocation.toString());
    	if (robberLocation == null)
            throw new InvalidRobberLocationException();

        facade.robPlayer(victim.getPlayerIndex(), robberLocation);
        System.out.println(victim.getPlayerIndex());
    }

    @Override
    public void placeRobber(HexLocation hexLoc) throws InvalidRobberLocationException
    {
    	try 
		{
			HttpResponse response = model.getProxy().getGameModel();
			ClientModel temp = model.getDeserializer().deserializeClientModel(response, model.getProxy());
            model.updateModel(temp);
		} 
		catch (NotInitializedException | DeserializationException | InvalidObjectTypeException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		try
		{
	        Collection<Player> players = model.getPermissions().canRob(hexLoc);
	        List<Player> listPlayers = new ArrayList<Player>();
	        listPlayers.addAll(players);
	        int actualSize = listPlayers.size();
	        if(listPlayers.contains(model.getGame().getMyPlayer()))
	        	actualSize -= 1;
	        RobPlayerInfo[] candidates = new RobPlayerInfo[actualSize];
	        
	        for(int i = 0; i < listPlayers.size(); i++)
	        {
	        	Player TP = listPlayers.get(i);
	        	if(listPlayers.get(i).getIndex() != model.getGame().getMyPlayer().getIndex())
	        	{
	        		RobPlayerInfo tempInfo = new RobPlayerInfo(TP.getPlayerId(),
	        				TP.getIndex(), TP.getName(), TP.getColor(), TP.getRecHand().getTotalResources());
	        		candidates[i] = tempInfo;
	        	}
	        }
	        controller.getRobView().setPlayers(candidates);
	        
	        controller.getRobView().showModal();
		}
		catch ( InvalidLocationException e)
		{
			//TODO: is this what I do here? I'm not really sure how to handle a bad location
//			CatanColor color = model.getGame().getMyPlayer().getColor();
//			controller.getView().startDrop(PieceType.ROBBER, color, false);
			e.printStackTrace();
		}
			PlayerFacade facade = model.getPlayerFacade(); 
		
		if (!facade.canMoveRobber(hexLoc))
		{
            throw new InvalidRobberLocationException(hexLoc);
		}
        else
        {
        	robberLocation = hexLoc;
        } 
    }
}
