package client.gui.map.state;

import client.gui.map.MapControllerImp;
import client.model.ClientModel;

/**
 * @author Eric
 */
public class PlacingRoad1 extends Setup1State implements PlacingRoad
{
	public PlacingRoad1(ClientModel model, MapControllerImp controller)
    {
        super(model, controller);
    }
}
