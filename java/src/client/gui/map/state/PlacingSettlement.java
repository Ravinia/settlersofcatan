package client.gui.map.state;

/**
 * Simple interface for reference during the first and second phase, that the server.model.player should be placing a settlement right now
 */
public interface PlacingSettlement
{
}
