package client.gui.map.state;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import shared.definitions.CatanColor;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import client.exceptions.BuildCityException;
import client.exceptions.BuildRoadException;
import client.exceptions.BuildSettlementException;
import client.exceptions.BuyDevCardException;
import client.exceptions.CannotPerformOperationException;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidRobberLocationException;
import client.facade.BuildFacade;
import client.facade.DevCardFacade;
import client.facade.PlayerFacade;
import client.gui.data.RobPlayerInfo;
import client.gui.map.MapControllerImp;
import client.model.ClientModel;
import client.model.game.board.immutable.Edge;
import client.model.game.permissions.Permissions;
import client.model.game.player.Player;

public class PlayingState extends MapState
{
    private	Permissions permissions;
	private HexLocation robberLocation;
	private MapControllerImp controller;
	private int victimIndex = -1;
	private boolean playedRoadBuilding = false;
	private boolean roadBuild2nd = false;
	private EdgeLocation roadBuildingLoc1;
	
	public PlayingState(ClientModel model, MapControllerImp controller)
    {
        super(model);
        permissions = model.getPermissions();
        this.controller = controller;
    }
    public boolean canPlaceRoad(EdgeLocation edgeLoc)
	{
		if(playedRoadBuilding)
		{
			if(roadBuild2nd)
			{
				Edge roadBuild1Edge = new Edge(roadBuildingLoc1, model.getBoard());
				try {
					return permissions.canPlaceRoad(edgeLoc) || (roadBuild1Edge.isAdjacentToEdge(edgeLoc) && !model.getBoard().hasRoad(edgeLoc));
				} catch (InvalidLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return permissions.canPlaceRoad(edgeLoc);
		}
//    	return permissions.canBuyRoad() && permissions.canPlaceRoad(edgeLoc);
		return permissions.canPlaceRoad(edgeLoc);
	}
	
	public boolean canPlaceSettlement(VertexLocation vertLoc)
	{
		return permissions.canBuySettlement() && permissions.canPlaceSettlement(vertLoc);
	}
	
	public boolean canPlaceCity(VertexLocation vertLoc)
	{
		return permissions.canBuyCity() && permissions.canPlaceCity(vertLoc);
	}
	
	public boolean canPlaceRobber(HexLocation hexLoc)
	{
		return permissions.canMoveRobber(hexLoc);
	}
	
	public void placeRoad(EdgeLocation edgeLoc)
	{
		BuildFacade builder = model.getBuildFacade();
		try 
		{
			if(!playedRoadBuilding)
				builder.buildRoad(edgeLoc, false);
			else
			{
				if(!this.roadBuild2nd)
					this.roadBuildingLoc1 = edgeLoc;
			}
		} 
		catch (BuildRoadException e) 
		{
				e.printStackTrace();
		}
		if(roadBuild2nd)
		{
			playedRoadBuilding = false;
			roadBuild2nd = false;
			DevCardFacade devCarder = model.getDevCardFacade();
			try {
				devCarder.playRoadBuilding(roadBuildingLoc1, edgeLoc);
			} catch (BuyDevCardException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(playedRoadBuilding)
		{
			this.startMove(PieceType.ROAD, true, false);
			roadBuild2nd = true;
		}	
	}
	public void placeSettlement(VertexLocation vertLoc)
	{
		BuildFacade builder = model.getBuildFacade();
        try 
		{
			builder.buildSettlement(vertLoc, false);
		} 
		catch (BuildSettlementException e) 
		{
				e.printStackTrace();
		}
	}
	public void placeCity(VertexLocation vertLoc)
	{
		BuildFacade builder = model.getBuildFacade();
        try 
		{
			builder.buildCity(vertLoc);
		} 
		catch (BuildCityException e) 
		{
				e.printStackTrace();
		}
	}

	public void placeRobber(HexLocation hexLoc) throws InvalidRobberLocationException
    {
		try
		{
	        Collection<Player> players = model.getPermissions().canRob(hexLoc);
	        List<Player> listPlayers = new ArrayList<Player>();
	        listPlayers.addAll(players);
	        int actualSize = listPlayers.size();
	        if(listPlayers.contains(model.getGame().getMyPlayer()))
	        	actualSize -= 1;
	        RobPlayerInfo[] candidates = new RobPlayerInfo[actualSize];
	        
	        for(int i = 0; i < listPlayers.size(); i++)
	        {
	        	Player TP = listPlayers.get(i);
	        	if(listPlayers.get(i).getIndex() != model.getGame().getMyPlayer().getIndex())
	        	{
	        		RobPlayerInfo tempInfo = new RobPlayerInfo(TP.getPlayerId(), TP.getIndex(), TP.getName(), TP.getColor(), TP.getRecHand().getTotalResources());
	        		candidates[i] = tempInfo;
	        	}
	        }
	        controller.getRobView().setPlayers(candidates);
	        //TODO: get the players you can rob from here, and tell the rob view to display them
	        
	        controller.getRobView().showModal();
		}
		catch ( InvalidLocationException e)
		{
			//TODO: is this what I do here? I'm not really sure how to handle a bad location
//			CatanColor color = model.getGame().getMyPlayer().getColor();
//			controller.getView().startDrop(PieceType.ROBBER, color, false);
			e.printStackTrace();
		}
		PlayerFacade facade = model.getPlayerFacade(); 
		
		if (!facade.canMoveRobber(hexLoc))
		{
            throw new InvalidRobberLocationException(hexLoc);
		}
        else
        {
        	robberLocation = hexLoc;
        } 	 
    }
	public void robPlayer(RobPlayerInfo victim) throws InvalidRobberLocationException
    {
		PlayerFacade facade = model.getPlayerFacade();
		
		if (robberLocation == null)
		{
				throw new InvalidRobberLocationException();
		}
		else
		{
				victimIndex = victim.getPlayerIndex();
				facade.robPlayer(victimIndex, robberLocation);
		}
    }
	public void playSoldierCard() throws InvalidRobberLocationException
	{
		DevCardFacade  devCarder = model.getDevCardFacade();
		if(robberLocation == null || victimIndex == -1)
		{
			throw new InvalidRobberLocationException();
		}
		try 
		{
			devCarder.playSoldier(robberLocation, victimIndex);
		} 
		catch (BuyDevCardException e) 
		{
			throw new InvalidRobberLocationException(e);
		}
	}
	public void playRoadBuildingCard()
	{
		
		playedRoadBuilding = true;
		this.startMove(PieceType.ROAD,true, false);
	}
    public int roll() throws CannotPerformOperationException
    {
        throw new CannotPerformOperationException("Cannot perform a roll in this state");
    }
    @Override
    public void startMove(PieceType pieceType, boolean isFree, boolean allowDisconnected)
    {
    	CatanColor color = model.getGame().getMyPlayer().getColor();
    	controller.getView().startDrop(pieceType, color, true);
    }
    public void setRobberLocation()
    {
    	robberLocation = model.getBoard().getRobberLocation();
    }
}
