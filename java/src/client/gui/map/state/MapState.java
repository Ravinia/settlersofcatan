package client.gui.map.state;

import client.exceptions.CannotPerformOperationException;
import shared.exceptions.InvalidRobberLocationException;
import client.model.ClientModel;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import client.gui.data.RobPlayerInfo;

public abstract class MapState
{
    protected ClientModel model;

    public MapState(ClientModel model)
    {
        this.model = model;
    }

	public boolean canPlaceRoad(EdgeLocation edgeLoc)
	{
		return false;
	}
	
	public boolean canPlaceSettlement(VertexLocation vertLoc)
	{
		return false;
	}
	
	public boolean canPlaceCity(VertexLocation vertLoc)
	{
		return false;
	}
	
	public boolean canPlaceRobber(HexLocation hexLoc)
	{
		return false;
	}
	
	public void placeRoad(EdgeLocation edgeLoc){}
	
	public void placeSettlement(VertexLocation vertLoc){}

	public void placeCity(VertexLocation vertLoc){}
	
	public void placeRobber(HexLocation hexLoc) throws InvalidRobberLocationException {}
	
	public void startMove(PieceType pieceType, boolean isFree, boolean allowDisconnected){}
	
	public void cancelMove(){}
	
	public void playSoldierCard() throws InvalidRobberLocationException{}
	
	public void playRoadBuildingCard(){}
	
	public void robPlayer(RobPlayerInfo victim) throws InvalidRobberLocationException {}

    public int roll() throws CannotPerformOperationException
    {
        throw new CannotPerformOperationException("Cannot perform a roll in this state");
    }

    public boolean roadsEnabled() {return false;}

    public boolean settlementsEnabled() {return false;}

    public boolean citiesEnabled() {return false;}

    public boolean buyCardsEnabled() {return false;}
    
    @Override
    public String toString()
    {
    	if (this.getClass().equals(DiscardingState.class))
    		return "discarding";
    	else if (this.getClass().equals(PlacingRoad1.class))
    		return "placing road 1";
    	else if (this.getClass().equals(PlacingRoad2.class))
    		return "placing road 2";
    	else if (this.getClass().equals(PlacingRoad.class))
    		return "placing road";
    	else if (this.getClass().equals(PlacingSettlement.class))
    		return "placing settlement";
    	else if (this.getClass().equals(PlacingSettlement1.class))
    		return "placing settlement 1";
    	else if (this.getClass().equals(PlacingSettlement2.class))
    		return "placing settlement 2";
    	else if (this.getClass().equals(PlayingState.class))
    		return "playing";
    	else if (this.getClass().equals(RobbingState.class))
    		return "robbing";
    	else if (this.getClass().equals(RollingState.class))
    		return "rolling";
    	else if (this.getClass().equals(Setup1State.class))
    		return "setup 1";
    	else if (this.getClass().equals(Setup2State.class))
    		return "setup 2";
    	else if (this.getClass().equals(StateHolder.class))
    		return "state holder";
    	else if (this.getClass().equals(StatePattern.class))
    		return "state pattern";
    	else if (this.getClass().equals(WaitingState.class))
    		return "waiting";
    	else
    		return "none";
    }
}
