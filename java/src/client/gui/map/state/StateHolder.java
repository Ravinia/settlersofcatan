package client.gui.map.state;

/**
 * Implementing this interface means that this class implements the State pattern
 * @author Lawrence
 */
public interface StateHolder
{
    /**
     * Gets the current State
     * @return the current state
     */
    public MapState getCurrentState();

    /**
     * Sets the current State to the current state stored in the model
     */
    public void updateCurrentState();
}
