package client.gui.map;

import client.gui.base.*;
import client.gui.data.*;

/**
 * Interface for the rob view, which lets the user select a server.model.player to rob
 */
public interface RobView extends OverlayView
{
	
	void setPlayers(RobPlayerInfo[] candidateVictims);
}

