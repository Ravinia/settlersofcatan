package client.gui.roll;

import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import client.exceptions.CannotPerformOperationException;
import client.gui.base.*;
import client.gui.map.state.RollingState;
import client.gui.map.state.StatePattern;
import client.model.ClientModel;
import client.model.ClientModelImp;


/**
 * Implementation for the roll controller
 */
public class RollController extends CatanController implements IRollController, Observer, StatePattern
{

	private IRollResultView resultView;
	private ClientModel model;
	private Timer timer;
	
	/**
	 * RollController constructor
	 * 
	 * @param view Roll view
	 * @param resultView Roll result view
	 */
	public RollController(IRollView view, IRollResultView resultView, ClientModel model)
    {

		super(view);
		this.model = model;
        ((ClientModelImp)this.model).addObserver(this);
		setResultView(resultView);
	}
	
	public IRollResultView getResultView()
    {
		return resultView;
	}
	public void setResultView(IRollResultView resultView)
    {
		this.resultView = resultView;
	}

	public IRollView getRollView()
    {
		return (IRollView)getView();
	}
	
	@Override
	public void rollDice()
    {
		if (timer == null)
			return;

		timer.cancel();
		timer = null;
		
//		getRollView().closeModal();
//		getResultView().closeModal();
		
		int roll = -1;
        try
        {
            roll = model.getCurrentState().roll();
        }
        catch (CannotPerformOperationException e)
        {
        	e.printStackTrace();
        }
        
    	getResultView().setRollValue(roll);
    	getResultView().showModal();
        
//        getRollView().closeModal();
//		getResultView().closeModal();
		
        System.out.println("rollview up: " + getRollView().isModalShowing());
        System.out.println("resultview up: " + getResultView().isModalShowing());
//        model.updateFromPoller();
	}

	@Override
	public void update(Observable o, Object arg)
    {

        if(model.getCurrentState() instanceof RollingState)
        {
            System.out.println("opening roll view modal");
            getRollView().setMessage("Time to roll the dice!");
            getRollView().showModal();
            if(timer == null)
            {
                timer = new Timer(true);
                timer.schedule(new AutoRoller(), 1000, 1000);
            }
        }
        else
        {
            if (timer != null)
            {
                System.out.println("TURNING OFF TIMER");
                timer.cancel();
                timer = null;
            }
        }

	}
	
	class AutoRoller extends TimerTask
	{
		int timeLeft;
		
		AutoRoller()
		{
			timeLeft = 4;
		}
		
		@Override
		public void run()
        {
			getRollView().setMessage("Rolling automatically in " + timeLeft + "...");
			System.out.println("tl: " + timeLeft);
			timeLeft--;
			if(timeLeft <= -1)
			{
                System.out.println("AUTOMATED ROLLING");
				rollDice();
			}
		}
	}
}

