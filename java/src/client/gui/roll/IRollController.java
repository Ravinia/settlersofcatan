package client.gui.roll;

import client.gui.base.*;

/**
 * Interface for the roll controller
 */
public interface IRollController extends Controller
{
	
	/**
	 * Called when the user clicks the "Roll!" button in the roll view
	 */
	void rollDice();
	
}

