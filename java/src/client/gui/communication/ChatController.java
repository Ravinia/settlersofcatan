package client.gui.communication;

import client.gui.base.*;

/**
 * Chat controller interface
 */
public interface ChatController extends Controller
{
	
	/**
	 * Called by the view when a message is sent
	 * 
	 * @param message
	 *            The message being sent
	 */
	void sendMessage(String message);
}

