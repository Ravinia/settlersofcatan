package client.gui.communication;

import java.util.List;
import java.awt.*;

import javax.swing.*;

import client.gui.base.*;

/**
 * Game history view implementation
 */
@SuppressWarnings("serial")
public class GameHistoryViewImp extends PanelView implements GameHistoryView
{
	
	private LogComponent logPanel;
	private JScrollPane logScroll;
	
	public GameHistoryViewImp()
	{
		
		this.setLayout(new BorderLayout());
		this.setBackground(Color.white);
		
		logPanel = new LogComponent();
		
		logScroll = new JScrollPane(logPanel);
		
		this.add(logScroll, BorderLayout.CENTER);
	}
	
	@Override
	public GameHistoryController getController()
	{
		return (GameHistoryController)super.getController();
	}
	
	@Override
	public void setEntries(final List<LogEntry> entries)
	{
		
		// Can't set entries immediately, because logPanel doesn't
		// have a width or height yet, which messes up the word wrap
		// calculations in LogComponent. Therefore, we call
		// invokeLater.
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				logPanel.setEntries(entries);
				JScrollBar vertical = logScroll.getVerticalScrollBar();
				vertical.setValue(vertical.getMaximum());
			}
		});
	}
	
}

