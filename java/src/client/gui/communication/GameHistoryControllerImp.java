package client.gui.communication;

import java.util.*;

import shared.exceptions.PlayerNotFoundException;
import client.gui.base.*;
import client.model.ClientModel;
import client.model.game.chatlog.Message;


/**
 * Game history controller implementation
 */
public class GameHistoryControllerImp extends CatanController implements GameHistoryController, Observer
{
	ClientModel model;
	
	public GameHistoryControllerImp(GameHistoryView view, ClientModel model)
    {
		super(view);
		this.model = model;
		((Observable) model).addObserver(this);
	}
	
	@Override
	public GameHistoryView getView()
    {
		
		return (GameHistoryView)super.getView();
	}
	
	private void initFromModel()
    {
		List<Message> log = model.getGame().getChatLog().getLogs();
		List<LogEntry> entries = new ArrayList<LogEntry>();
		try
		{
			for(Message message : log)
			{
				entries.add(new LogEntry(model.getGame().getColorByName(message.getSource()), message.getMessage()));
			}
		} catch (PlayerNotFoundException e)	{
			e.printStackTrace();
		}
		
		getView().setEntries(entries);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("GameHistory controller notified...");
		initFromModel();
	}
	
}

