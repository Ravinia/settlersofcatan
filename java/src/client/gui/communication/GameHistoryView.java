package client.gui.communication;

import java.util.List;

import client.gui.base.*;

/**
 * Game history view interface
 */
public interface GameHistoryView extends View
{
	
	/**
	 * Sets the history messages to be displayed in the view.
	 * 
	 * @param entries
	 *            The history messages to display
	 */
	void setEntries(List<LogEntry> entries);
}

