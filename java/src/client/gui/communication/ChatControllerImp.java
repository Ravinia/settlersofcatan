package client.gui.communication;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import shared.exceptions.PlayerNotFoundException;
import client.facade.PlayerFacade;
import client.gui.base.*;
import client.model.ClientModel;
import client.model.ClientModelImp;
import client.model.game.chatlog.Message;
import client.model.game.player.ResourceHand;


/**
 * Chat controller implementation
 */
public class ChatControllerImp extends CatanController implements ChatController, Observer
{
	ClientModelImp model;
	PlayerFacade playerFacade;

	public ChatControllerImp(ChatView view, ClientModel model)
    {
		super(view);
		this.model = (ClientModelImp)model;
		((Observable) model).addObserver(this);
		playerFacade = model.getPlayerFacade();
	}

	@Override
	public ChatView getView()
    {
		return (ChatView)super.getView();
	}

	@Override
	public void sendMessage(String message)
    {
		playerFacade.sendChat(message);
		
		if (message.equals("greedisgood"))
		{
			System.out.println("cheat entered");
			model.getGame().getMyPlayer().setRecHand(new ResourceHand(10,10,10,10,10));
			System.out.println("ore: " + model.getGame().getMyPlayer().getRecHand().getOre());
			model.notifyObservers();
		}
	}

	@Override
	public void update(Observable arg0, Object arg1)
	{
		System.out.println("Chat controller notified...");
		List<Message> chats = model.getGame().getChatLog().getChats();
		List<LogEntry> entries = new ArrayList<LogEntry>();
		try
		{
			for(Message message : chats)
			{
				entries.add(new LogEntry(model.getGame().getColorByName(message.getSource()), message.getMessage()));
			}
		} catch (PlayerNotFoundException e)	{
			e.printStackTrace();
		}
		
		getView().setEntries(entries);
	}

}

