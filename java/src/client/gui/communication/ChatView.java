package client.gui.communication;

import java.util.List;

import client.gui.base.*;

/**
 * Chat view interface
 */
public interface ChatView extends View
{
	
	/**
	 * Sets the chat messages to be displayed in the view.
	 * 
	 * @param entries
	 *            The chat messages to display
	 */
	void setEntries(List<LogEntry> entries);
}

