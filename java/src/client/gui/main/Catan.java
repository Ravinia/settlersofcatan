package client.gui.main;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import client.gui.base.Action;
import client.gui.base.OverlayViewImp;
import client.gui.catan.CatanPanel;
import client.gui.join.JoinGameControllerImp;
import client.gui.join.JoinGameView;
import client.gui.join.NewGameView;
import client.gui.join.PlayerWaitingController;
import client.gui.join.PlayerWaitingView;
import client.gui.join.SelectColorView;
import client.gui.login.LoginController;
import client.gui.login.LoginView;
import client.gui.misc.MessageView;
import client.model.ClientModelImp;
import client.proxyserver.ProxyServer;
import client.proxyserver.ProxyServerImp;
import client.serializer.Deserializer;
import client.serializer.DeserializerImp;
import client.serializer.Serializer;
import client.serializer.SerializerImp;

/**
 * Main entry point for the Catan program
 */
@SuppressWarnings("serial")
public class Catan extends JFrame
{
	private CatanPanel catanPanel;
	private static Deserializer deserializer = new DeserializerImp();
	private static Serializer serializer = new SerializerImp();
    private static ProxyServer proxy = new ProxyServerImp("localhost", "8081", deserializer, serializer);
    private static ClientModelImp model = new ClientModelImp(proxy);
	
	public Catan()
	{
		OverlayViewImp.setWindow(this);
		
		catanPanel = new CatanPanel(model);
		this.setContentPane(catanPanel);
		
		display();
	}
	
	private void display()
	{
		pack();
		setVisible(true);
	}
	
	//
	// Main
	//
	public static void main(final String[] args)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable()
        {
			public void run()
			{
				new Catan();
				
				PlayerWaitingView playerWaitingView = new PlayerWaitingView();
				final PlayerWaitingController playerWaitingController = new PlayerWaitingController(playerWaitingView, model);
				playerWaitingView.setController(playerWaitingController);
				
				JoinGameView joinView = new JoinGameView();
				NewGameView newGameView = new NewGameView();
				SelectColorView selectColorView = new SelectColorView();
				MessageView joinMessageView = new MessageView();
				final JoinGameControllerImp joinController =
                        new JoinGameControllerImp(joinView, newGameView, selectColorView, joinMessageView, model);
				joinController.setJoinAction(new Action()
                {
					@Override
					public void execute()
					{
						playerWaitingController.start();
					}
				});
				joinView.setController(joinController);
				newGameView.setController(joinController);
				selectColorView.setController(joinController);
				joinMessageView.setController(joinController);
				
				LoginView loginView = new LoginView();
				MessageView loginMessageView = new MessageView();
				LoginController loginController = new LoginController(loginView, loginMessageView, model);
				loginController.setLoginAction(new Action()
                {
					@Override
					public void execute()
					{
						joinController.start();
					}
				});
				loginView.setController(loginController);
				loginView.setController(loginController);
				
				loginController.start();
			}
		});
	}
	
}

