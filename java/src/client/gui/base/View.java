package client.gui.base;

/**
 * Base interface for views
 */
public interface View
{
	/**
	 * Controller setter
	 * 
	 * @param controller
	 *            The view's controller
	 */
	void setController(Controller controller);
	
	/**
	 * Controller getter
	 * 
	 * @return The view's controller
	 */
	Controller getController();
}

