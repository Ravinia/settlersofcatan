package client.gui.base;

/**
 * Base class for controllers
 */
public abstract class CatanController implements Controller
{
	
	private View view;
	
	protected CatanController(View view)
	{
		setView(view);
	}
	
	private void setView(View view)
	{
		this.view = view;
	}
	
	@Override
	public View getView()
	{
		return this.view;
	}
	
}

