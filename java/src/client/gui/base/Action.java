package client.gui.base;

/**
 * Generic action interface
 */
public interface Action
{
	
	/**
	 * Execute the action
	 */
	void execute();
}

