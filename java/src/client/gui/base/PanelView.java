package client.gui.base;

import javax.swing.*;

/**
 * Base class for JPanel-based views
 */
@SuppressWarnings("serial")
public class PanelView extends JPanel implements View
{
	
	private Controller controller;
	
	public Controller getController()
	{
		return controller;
	}
	
	public void setController(Controller controller)
	{
		this.controller = controller;
	}
	
}

