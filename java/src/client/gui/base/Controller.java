package client.gui.base;

/**
 * Base controller interface
 */
public interface Controller
{
	
	/**
	 * View getter
	 * 
	 * @return The controller's view
	 */
	View getView();
}

