package client.gui.base;

import javax.swing.*;

/**
 * Base class for JComponent-based views
 */
@SuppressWarnings("serial")
public class ComponentView extends JComponent implements View
{
	
	private Controller controller;
	
	public Controller getController()
	{
		return controller;
	}
	
	public void setController(Controller controller)
	{
		this.controller = controller;
	}
	
}

