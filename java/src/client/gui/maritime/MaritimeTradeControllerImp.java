package client.gui.maritime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import shared.definitions.*;
import client.exceptions.BadMaritimeRequest;
import client.exceptions.DeserializationException;
import shared.exceptions.InvalidLocationException;
import client.exceptions.InvalidMaritimeTradeInput;
import shared.exceptions.NotInitializedException;
import shared.exceptions.PlayerNotFoundException;
import client.facade.TradeFacade;
import client.facade.TradeFacadeImp;
import client.gui.base.*;
import client.gui.map.state.PlayingState;
import client.model.ClientModel;
import client.model.ClientModelImp;
import client.model.game.PlayableGame;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;


/**
 * Implementation for the maritime trade controller
 */
public class MaritimeTradeControllerImp extends CatanController implements MaritimeTradeController, Observer{

	private IMaritimeTradeOverlay tradeOverlay;
	private ClientModelImp model; 
	private PlayableGame game;
	private IMaritimeTradeView view;
	private Map<ResourceType, Integer> minResources;
	private ResourceType giveRec;
	private ResourceType getRec;
	private ResourceType[] tradableResources;
	
	public MaritimeTradeControllerImp(IMaritimeTradeView tradeView, IMaritimeTradeOverlay tradeOverlay, ClientModel model) {
		
		super(tradeView);
		setTradeOverlay(tradeOverlay);
		this.model = (ClientModelImp) model;
		this.model.addObserver(this);
		this.game = this.model.getGame();
		this.view = tradeView;
		this.minResources = new HashMap<ResourceType, Integer>();
	}
	
	public void resetMaritimeTrade()
	{
		if(this.model.getCurrentState() instanceof PlayingState)
		{
			this.view.enableMaritimeTrade(true);
		}
		else
		{
			this.view.enableMaritimeTrade(false);
		}
	}
	
	public IMaritimeTradeView getTradeView() {
		
		return (IMaritimeTradeView)super.getView();
	}
	
	public IMaritimeTradeOverlay getTradeOverlay() {
		return tradeOverlay;
	}

	public void setTradeOverlay(IMaritimeTradeOverlay tradeOverlay) {
		this.tradeOverlay = tradeOverlay;
	}

	@Override
	public void startTrade() {
		
		getTradeOverlay().showModal();
		this.setResourceMinToTrade();
		this.setTradeableResources();
		
		this.tradeOverlay.showGiveOptions(this.tradableResources);
		this.tradeOverlay.setTradeEnabled(false);
	}

	public void setTradeableResources()
	{
		Player player = this.game.getMyPlayer();
		
		List<ResourceType> tradable = new ArrayList<ResourceType>();
		ResourceHand resourceHand = player.getRecHand();
		
		int brick = resourceHand.getBrick();
		int ore = resourceHand.getOre();
		int sheep = resourceHand.getSheep();
		int wood = resourceHand.getWood();
		int wheat = resourceHand.getWheat();
		
		
		if(brick >= this.minResources.get(ResourceType.BRICK))
		{
			tradable.add(ResourceType.BRICK);
		}
		if(wood >= this.minResources.get(ResourceType.WOOD))
		{
			tradable.add(ResourceType.WOOD);
		}
		if(wheat >= this.minResources.get(ResourceType.WHEAT))
		{
			tradable.add(ResourceType.WHEAT);
		}
		if(ore >= this.minResources.get(ResourceType.ORE))
		{
			tradable.add(ResourceType.ORE);
		}
		if(sheep >= this.minResources.get(ResourceType.SHEEP))
		{
			tradable.add(ResourceType.SHEEP);
		}
		
		this.tradableResources = tradable.toArray(new ResourceType[tradable.size()]);
		
	}
	@Override
	public void makeTrade() {
		try
		{
			if(this.getRec == null || this.giveRec == null)
			{
				throw new InvalidMaritimeTradeInput();
			}
			
			try
			{
				TradeFacade trade = new TradeFacadeImp(this.model);
				int minRec = this.minResources.get(this.giveRec);
				trade.maritimeTrade(minRec, this.giveRec, this.getRec);
			}
			catch (NotInitializedException | PlayerNotFoundException | DeserializationException | BadMaritimeRequest e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			getTradeOverlay().closeModal();
		}
		catch(InvalidMaritimeTradeInput e)
		{
			this.getTradeOverlay().setStateMessage("Error");
		}
	}

	public void setResourceMinToTrade()
	{
		this.minResources.clear();
		boolean generalPort = false;
		boolean wheatPort = false;
		boolean brickPort = false;
		boolean woodPort = false;
		boolean orePort = false;
		boolean sheepPort = false;
		try
		{
			Collection<PortType> ports;
			int playerID = this.model.getLocalUserID();
			String playerName = this.model.getLocalUser().getName();
			Player player = this.model.getGame().getPlayer(playerID, playerName);
			ports = this.model.getBoard().getPlayersPorts(player);
			for(PortType port: ports )
			{
				switch(port)
				{
					case WOOD:
						woodPort = true;
						break;
					case BRICK:
						brickPort = true;
						break;
					case SHEEP:
						sheepPort = true;
						break;
					case WHEAT:
						wheatPort = true;
						break;
					case ORE:
						orePort = true;
						break;
					case THREE:
						generalPort = true;
						break;
				}
			}
			
		}
		catch (InvalidLocationException | NotInitializedException | PlayerNotFoundException e)
		{
			e.printStackTrace();
		}
		
		int brickMin = 4;
		int oreMin = 4;
		int wheatMin = 4;
		int woodMin = 4;
		int sheepMin = 4;
		
		if(generalPort)
		{
			brickMin = 3;
			oreMin = 3;
			wheatMin = 3;
			woodMin = 3;
			sheepMin = 3;
		}
		brickMin = brickPort ? 2 : brickMin;
		woodMin = woodPort ? 2 : woodMin;
		sheepMin = sheepPort ? 2 : sheepMin;
		wheatMin = wheatPort ? 2 : wheatMin;
		oreMin = orePort ? 2 : oreMin;
		
		this.minResources.put(ResourceType.WHEAT, wheatMin);
		this.minResources.put(ResourceType.BRICK, brickMin);
		this.minResources.put(ResourceType.ORE, oreMin);
		this.minResources.put(ResourceType.SHEEP, sheepMin);
		this.minResources.put(ResourceType.WOOD, woodMin);
		
	}
	@Override
	public void cancelTrade() {
		getTradeOverlay().reset();
		getTradeOverlay().closeModal();
	}

	@Override
	public void setGetResource(ResourceType resource) {
		this.tradeOverlay.selectGetOption(resource, 1);
		this.tradeOverlay.setTradeEnabled(true);
		this.getRec = resource;
	}

	@Override
	public void setGiveResource(ResourceType resource) {
		this.tradeOverlay.hideGiveOptions();
		this.setResourceMinToTrade();
		int minNumber = this.minResources.get(resource);
		this.tradeOverlay.selectGiveOption(resource, minNumber);
		ResourceType[] getableResources = {ResourceType.WOOD, ResourceType.BRICK, ResourceType.SHEEP, ResourceType.ORE, ResourceType.WHEAT};
		this.tradeOverlay.showGetOptions(getableResources);
		this.giveRec = resource;
//		this.tradeOverlay.selectGetOption(selectedResource, amount);
	}

	@Override
	public void unsetGetValue() {
		ResourceType[] getableResources = {ResourceType.WOOD, ResourceType.BRICK, ResourceType.SHEEP, ResourceType.ORE, ResourceType.WHEAT};
		this.tradeOverlay.showGetOptions(getableResources);
		this.tradeOverlay.setTradeEnabled(false);
		this.getRec = null;
	}

	@Override
	public void unsetGiveValue() {
		this.tradeOverlay.showGiveOptions(this.tradableResources);
		this.tradeOverlay.setTradeEnabled(false);
		this.giveRec = null;
	}

	@Override
	public void update(Observable o, Object arg)
	{
		this.setResourceMinToTrade();
		this.setTradeableResources();
		this.resetMaritimeTrade();
	}

}

