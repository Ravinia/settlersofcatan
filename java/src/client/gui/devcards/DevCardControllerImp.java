package client.gui.devcards;

import client.model.ClientModel;
import client.model.ClientModelImp;
import client.model.game.developmentcard.Development;
import client.model.game.player.DevelopmentCardHand;
import client.model.game.player.Player;
import shared.definitions.DevCardType;
import shared.definitions.ResourceType;
import client.exceptions.BuyDevCardException;
import client.exceptions.DoNotUpdateException;
import client.facade.DevCardFacade;
import client.facade.DevCardFacadeImp;
import client.gui.base.*;

import java.util.Observable;
import java.util.Observer;


/**
 * "Dev card" controller implementation
 */
public class DevCardControllerImp extends CatanController implements DevCardController, Observer
{

	private BuyDevCardView buyCardView;
	private Action soldierAction;
	private Action roadAction;
    private ClientModelImp model;
	
	/**
	 * DevCardController constructor
	 * 
	 * @param view "Play dev card" view
	 * @param buyCardView "Buy dev card" view
	 * @param soldierAction Action to be executed when the user plays a soldier card. 
	 *  It calls "mapController.playSoldierCard()".
	 * @param roadAction Action to be executed when the user plays a road building card.
	 *   It calls "mapController.playRoadBuildingCard()".
	 */
	public DevCardControllerImp(PlayDevCardView view, BuyDevCardView buyCardView,
                                Action soldierAction, Action roadAction, ClientModel model)
    {

		super(view);
		
		this.buyCardView = buyCardView;
		this.soldierAction = soldierAction;
		this.roadAction = roadAction;
        this.model = (ClientModelImp) model;
        ((ClientModelImp)model).addObserver(this);
	}

	public PlayDevCardView getPlayCardView()
    {
		return (PlayDevCardView)super.getView();
	}

	public BuyDevCardView getBuyCardView()
    {
		return buyCardView;
	}

	@Override
	public void startBuyCard()
    {
		getBuyCardView().showModal();
	}

	@Override
	public void cancelBuyCard()
    {
		getBuyCardView().closeModal();
	}

	@Override
	public void buyCard()
    {
		try {
			DevCardFacade dcf = new DevCardFacadeImp(model);
			dcf.buyDevCard();
		} catch (BuyDevCardException e) {
			e.printStackTrace();
		}
		getBuyCardView().closeModal();
	}

	@Override
	public void startPlayCard()
    {
		getPlayCardView().showModal();
	}

	@Override
	public void cancelPlayCard()
    {
		getPlayCardView().closeModal();
	}

	@Override
	public void playMonopolyCard(ResourceType resource)
    {
		try {
			new DevCardFacadeImp(model).playMonopoly(resource);
		} catch (BuyDevCardException e) {
			e.printStackTrace();
		}
		getPlayCardView().closeModal();
	}

	@Override
	public void playMonumentCard()
    {
		try {
			new DevCardFacadeImp(model).playMonument();
		} catch (BuyDevCardException e) {
			e.printStackTrace();
		}
		getPlayCardView().closeModal();
	}

	@Override
	public void playRoadBuildCard()
    {
		getPlayCardView().closeModal();
		roadAction.execute();
	}

	@Override
	public void playSoldierCard()
    {
		getPlayCardView().closeModal();
		soldierAction.execute();
	}

	@Override
	public void playYearOfPlentyCard(ResourceType resource1, ResourceType resource2)
    {
		try {
			new DevCardFacadeImp(model).playYearOfPlenty(resource1, resource2);
		} catch (BuyDevCardException e) {
			e.printStackTrace();
		}
		getPlayCardView().closeModal();
	}

    @Override
    public void update(Observable o, Object arg)
    {
    	 try
         {
             Player player =  getLocalPlayer();
             DevelopmentCardHand devCardHand = player.getDevHand();
             
             boolean soldierEnabled = false;
             boolean yearOfPlentyEnabled = false;
             boolean monopolyEnabled = false;
             boolean roadBuildingEnabled = false;
             boolean monumentEnabled = false;
             
             for (Development dev : devCardHand.getAllPlayableCards())
             {
            	 if (dev.getDevCardType().equals(DevCardType.SOLDIER))
            		 soldierEnabled = true;
            	 if (dev.getDevCardType().equals(DevCardType.YEAR_OF_PLENTY))
            		 yearOfPlentyEnabled = true;
            	 if (dev.getDevCardType().equals(DevCardType.MONOPOLY))
            		 monopolyEnabled = true;
            	 if (dev.getDevCardType().equals(DevCardType.ROAD_BUILD))
            		 roadBuildingEnabled = true;
            	 if (dev.getDevCardType().equals(DevCardType.MONUMENT))
            		 monumentEnabled = true;
             }
             
             // this is done in the controller because the devCardHand doesn't have access to the server.model.player
             if (player.isPlayedDevCard())
             {
            	 soldierEnabled = false;
            	 yearOfPlentyEnabled = false;
            	 monopolyEnabled = false;
            	 roadBuildingEnabled = false;
             }
             
             getPlayCardView().setCardEnabled(DevCardType.SOLDIER, soldierEnabled);
             getPlayCardView().setCardEnabled(DevCardType.YEAR_OF_PLENTY, yearOfPlentyEnabled);
             getPlayCardView().setCardEnabled(DevCardType.MONOPOLY, monopolyEnabled);
             getPlayCardView().setCardEnabled(DevCardType.ROAD_BUILD, roadBuildingEnabled);
             getPlayCardView().setCardEnabled(DevCardType.MONUMENT, monumentEnabled);
             
             //--------------------------------------
             
             getPlayCardView().setCardAmount(DevCardType.SOLDIER,
            		 devCardHand.getTotalNumberOfCardsOfType(DevCardType.SOLDIER));
             
             getPlayCardView().setCardAmount(DevCardType.YEAR_OF_PLENTY, 
            		 devCardHand.getTotalNumberOfCardsOfType(DevCardType.YEAR_OF_PLENTY));
             
             getPlayCardView().setCardAmount(DevCardType.ROAD_BUILD,
            		 devCardHand.getTotalNumberOfCardsOfType(DevCardType.ROAD_BUILD));
             
             getPlayCardView().setCardAmount(DevCardType.MONOPOLY,
            		 devCardHand.getTotalNumberOfCardsOfType(DevCardType.MONOPOLY));
             
             getPlayCardView().setCardAmount(DevCardType.MONUMENT,
            		 devCardHand.getTotalNumberOfCardsOfType(DevCardType.MONUMENT));
         }
         catch (DoNotUpdateException ignore)
         {}
    }
    	 
    private Player getLocalPlayer() throws DoNotUpdateException
    {
        try
        {
           return model.getGame().getMyPlayer();
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            throw new DoNotUpdateException();
        }
    }
}

