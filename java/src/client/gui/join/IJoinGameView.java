package client.gui.join;

import client.gui.base.*;
import client.gui.data.*;

/**
 * Interface for the join game view, which lets the user select a game to join
 */
public interface IJoinGameView extends OverlayView
{
	
	/**
	 * Sets the list of available games to be displayed
	 * 
	 * @param games
	 *            Array of games to be displayed
	 * @param localPlayer
	 *            Information about the local server.model.player
	 */
	void setGames(GameInfo[] games, PlayerInfo localPlayer);
	
}

