package client.gui.join;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import client.exceptions.CannotAddAIException;
import shared.exceptions.NotInitializedException;
import client.gui.base.*;
import client.gui.data.PlayerInfo;
import client.gui.data.PlayerInfoFactory;
import client.model.ClientModel;
import client.model.ClientModelImp;
import client.model.game.player.AIName;
import client.model.game.player.AIPlayerName;
import client.model.game.player.Player;


/**
 * Implementation for the server.model.player waiting controller
 */
public class PlayerWaitingController extends CatanController implements IPlayerWaitingController, Observer
{
    private ClientModel model;

	public PlayerWaitingController(IPlayerWaitingView view, ClientModel model)
    {
		super(view);
		this.model = model;
        ((ClientModelImp)this.model).addObserver(this);
	}

	@Override
	public IPlayerWaitingView getView()
    {

		return (IPlayerWaitingView)super.getView();
	}

	@Override
	public void start()
    {
		
		List<AIName> temp;
		try 
		{
			temp = model.getGameHub().getAIType();
			String[] AIList = new String[temp.size()];
			for(int i = 0; i < temp.size(); i++)
			{
				AIList[i] = temp.get(i).getName();
			}
			
			getView().setAIChoices(AIList);
			
			getView().showModal();
		} 
		catch (NotInitializedException e)
		{
			e.printStackTrace();
		}
		
		List<Player> players = model.getGame().getAllPlayers();
		List<PlayerInfo> infos = PlayerInfoFactory.convert(players);
		PlayerInfo[] playerInfos = new PlayerInfo[infos.size()];
		for(int i = 0; i < infos.size(); i++)
		{
			playerInfos[i] = infos.get(i);
		}
		getView().setPlayers(playerInfos);
		if(playerInfos.length ==4)
		{
			getView().closeModal();
		}
	}

	@Override
	public void addAI()
    {
		AIName AiName = new AIPlayerName(getView().getSelectedAI());
		try 
		{
			model.getGameHub().addAI(AiName);
		} 
		catch (CannotAddAIException e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	public void update(Observable o, Object arg) 
	{
		System.out.println("Player Waiting Controller Notified");
		
		List<Player> players = model.getGame().getAllPlayers();
		List<PlayerInfo> infos = PlayerInfoFactory.convert(players);
		PlayerInfo[] playerInfos = new PlayerInfo[infos.size()];
		//System.out.println(infos.size());
		for(int i = 0; i < infos.size(); i++)
		{
			playerInfos[i] = infos.get(i);
		}
		getView().setPlayers(playerInfos);
		
		if(playerInfos.length == 4)
		{
			getView().closeModal();
		}
		else
			getView().showModal();
	}

}

