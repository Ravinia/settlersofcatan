package client.gui.join;

import client.exceptions.BadTitleException;
import client.exceptions.CannotJoinGameException;
import client.exceptions.CreateGameException;
import client.exceptions.DeserializationException;
import client.exceptions.GameNotSelectedException;
import shared.exceptions.InvalidObjectTypeException;
import shared.player.Cookie;
import client.exceptions.UserNotInitializedException;
import client.facade.GameHub;
import client.model.ClientModel;
import client.model.game.JoinableGame;
import client.proxyserver.ProxyServerImp;
import shared.definitions.CatanColor;
import client.gui.base.*;
import client.gui.data.*;
import client.gui.misc.*;

import java.util.List;
import java.util.Observable;
import java.util.Observer;


/**
 * Implementation for the join game controller
 */
public class JoinGameControllerImp extends CatanController implements JoinGameController, Observer
{
    private static final String GAME_NOT_SELECTED_TITLE = "Game not selected";
    private static final String GAME_NOT_SELECTED_MESSAGE = "There is no record of a selected game. Please try again";
    private static final String CANNOT_JOIN_TITLE = "Cannot Join Game";
    private static final String CANNOT_JOIN_MESSAGE = "You are not allowed to join this game:\n";
    private static final String BAD_TITLE_TITLE = "Duplicate or Blank Title";
    private static final String BAD_TITLE_MESSAGE = "Pick a new game title";
    
	private INewGameView newGameView;
	private ISelectColorView selectColorView;
	private IMessageView messageView;
	private Action joinAction;
    private ClientModel model;
    private GameHub gameHub;
    private boolean modalFree;

    private GameInfo joinedGame;

	/**
	 * JoinGameController constructor
	 * 
	 * @param view Join game view
	 * @param newGameView New game view
	 * @param selectColorView Select color view
	 * @param messageView Message view (used to display error messages that occur while the user is joining a game)
	 */
	public JoinGameControllerImp(IJoinGameView view, INewGameView newGameView,ISelectColorView selectColorView,
                                 IMessageView messageView, ClientModel model)
    {
		super(view);

		setNewGameView(newGameView);
		setSelectColorView(selectColorView);
		setMessageView(messageView);

        this.model = model;
        //((ClientModelImp)this.model).addObserver(this);
        this.gameHub = this.model.getGameHub();
        
        model.initializePoller(this);
        modalFree = true;
	}
	
	public IJoinGameView getJoinGameView()
    {
		
		return (IJoinGameView)super.getView();
	}
	
	/**
	 * Returns the action to be executed when the user joins a game
	 * 
	 * @return The action to be executed when the user joins a game
	 */
	public Action getJoinAction()
    {
		
		return joinAction;
	}

	/**
	 * Sets the action to be executed when the user joins a game
	 * 
	 * @param value The action to be executed when the user joins a game
	 */
	public void setJoinAction(Action value)
    {
		
		joinAction = value;
	}
	
	public INewGameView getNewGameView()
    {
		
		return newGameView;
	}

	public void setNewGameView(INewGameView newGameView)
    {
		
		this.newGameView = newGameView;
	}
	
	public ISelectColorView getSelectColorView()
    {
		
		return selectColorView;
	}
	public void setSelectColorView(ISelectColorView selectColorView)
    {
		
		this.selectColorView = selectColorView;
	}
	
	public IMessageView getMessageView()
    {
		
		return messageView;
	}
	public void setMessageView(IMessageView messageView)
    {
		
		this.messageView = messageView;
	}

	@Override
	public void start()
    {
		getJoinGameView().showModal();
        displayGames();
	}

	@Override
	public void startCreateNewGame()
    {
		modalFree = false;
		getNewGameView().showModal();
	}

	@Override
	public void cancelCreateNewGame()
    {
		getNewGameView().closeModal();
		modalFree = true;
	}

	@Override
	public void createNewGame()
    {
        String name = getNewGameView().getTitle();
        boolean hexes = getNewGameView().getRandomlyPlaceHexes();
        boolean tokens = getNewGameView().getRandomlyPlaceNumbers();
        boolean ports = getNewGameView().getUseRandomPorts();
        
        boolean error = false;
        try
        {
        	
        	if(name.compareTo("") == 0)
             {
             	error = true;
        		 throw new BadTitleException();
             }
             for(JoinableGame i : gameHub.getGames())
             {
             	if(name.compareTo(i.getGameName()) == 0)
             	{
             		error = true;
             		throw new BadTitleException();
             	}
             }
        	gameHub.createGame(name, hexes, tokens, ports);
        }
        catch(CreateGameException e)
        {
        	System.out.println("CreateGameException from JoinGameController");
        }
        catch (BadTitleException e)
        {
        	error = true;
        	 showError(BAD_TITLE_TITLE, BAD_TITLE_MESSAGE);
        }
        catch (DeserializationException e)
        {
        	error = true;
        	System.out.println("Deserialization exception");
        }
        catch (InvalidObjectTypeException e) // says that it's throwing deserialization errors.
        {
        	error = true; //deserialization or invalid object type exception
        	System.out.println("InvalidObjectType Exception");
        	//TODO: display modal with error
        }
        if(error == false)
        {
        	getMessageView().closeModal();
            getNewGameView().closeModal();
            getSelectColorView().closeModal();
            getJoinGameView().closeModal();
        	modalFree = true;
        	displayGames();
        }	
	}

	@Override
	public void startJoinGame(GameInfo game)
    {
		modalFree = false;
		getMessageView().closeModal();
        getNewGameView().closeModal();
        getSelectColorView().closeModal();
        getJoinGameView().closeModal();
        
        joinedGame = game;
        if(game.getPlayers() != null)
        {
        	List<PlayerInfo> allPlayersInfo = game.getPlayers();
        	for(PlayerInfo i : allPlayersInfo)
        	{
        		try {
					if(i.getName().compareTo(model.getLocalUser().getName()) != 0)
					{
						getSelectColorView().setColorEnabled(i.getColor(), false);
					}
				} catch (UserNotInitializedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        }
        
        getSelectColorView().showModal();
	}

	@Override
	public void cancelJoinGame()
    {
	    joinedGame = null;
		
	    getMessageView().closeModal();
        getNewGameView().closeModal();
        getSelectColorView().closeModal();
        getJoinGameView().closeModal();
        
        getJoinGameView().showModal();
        modalFree = true;
	}

	@Override
	public void joinGame(CatanColor color)
    {
		try
        {
            if (joinedGame == null)
                throw new GameNotSelectedException();
           
            int gameID = joinedGame.getId();
            String gameName = joinedGame.getTitle();
            gameHub.joinGame(gameID, color, gameName);
            
            // If join succeeded
            getMessageView().closeModal();
            getNewGameView().closeModal();
            getSelectColorView().closeModal();
            getJoinGameView().closeModal();
            joinAction.execute();
           
        }
        catch(GameNotSelectedException e)
        {
            showError(GAME_NOT_SELECTED_TITLE, GAME_NOT_SELECTED_MESSAGE);
        }
        catch (CannotJoinGameException e)
        {
            showError(CANNOT_JOIN_TITLE, CANNOT_JOIN_MESSAGE + e.getMessage());
        }
	}

    private void displayGames()
    {
        try
        {
            List<JoinableGame> games = gameHub.getGames();
            GameInfo[] info = GameInfoFactory.convert(games);
            PlayerInfo localPlayer = PlayerInfoFactory.convert(model.getLocalUser());
            getJoinGameView().setGames(info, localPlayer);

            ProxyServerImp p =  (ProxyServerImp)model.getProxy();
            Cookie c = p.getUserCookie();
            System.out.println(c.getPlayerID());
            
            getJoinGameView().showModal();
        }
        catch (Exception e)
        {
            getMessageView().setTitle("Error");
            getMessageView().setMessage("An unexpected error occurred: \n" + e.getMessage());
            getMessageView().showModal();
        }
    }

    private void showError(String title, String message)
    {
    	getMessageView().closeModal();
        getNewGameView().closeModal();
        getSelectColorView().closeModal();
        getJoinGameView().closeModal();
        
        getJoinGameView().showModal();

        getMessageView().setTitle(title);
        getMessageView().setMessage(message);
        getMessageView().showModal();
    }

    @Override
    public void update(Observable o, Object arg)
    {
        if(getJoinGameView().isModalShowing() && modalFree)
        {
        	List<JoinableGame> games;
			try {
				games = gameHub.getGames();
				GameInfo[] info = GameInfoFactory.convert(games);
	            PlayerInfo localPlayer = PlayerInfoFactory.convert(model.getLocalUser());
	            getJoinGameView().setGames(info, localPlayer);
	            getJoinGameView().showModal();
			} catch (Exception e) {
				getMessageView().setTitle("Error");
	            getMessageView().setMessage("An unexpected error occurred: \n" + e.getMessage());
	            getMessageView().showModal();
			}
            
        }

    }
}

