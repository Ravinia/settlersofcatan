package client.gui.join;

import client.gui.base.*;

/**
 * Interface for the server.model.player waiting controller
 */
public interface IPlayerWaitingController extends Controller
{
	
	/**
	 * Displays the server.model.player waiting view
	 */
	void start();
	
	/**
	 * Called when the "Add AI" button is clicked in the server.model.player waiting view
	 */
	void addAI();
}

