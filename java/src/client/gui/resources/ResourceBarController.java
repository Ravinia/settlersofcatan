package client.gui.resources;

import java.util.*;

import client.exceptions.DoNotUpdateException;
import shared.exceptions.PlayerNotFoundException;
import client.gui.base.*;
import client.gui.misc.IMessageView;
import client.gui.misc.MessageView;
import client.model.ClientModel;
import client.model.ClientModelImp;
import client.model.game.permissions.BuyPermissions;
import client.model.game.permissions.BuyPermissionsImp;
import client.model.game.player.BuildPool;
import shared.player.PlayerIdentity;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;
import shared.definitions.Phase;


/**
 * Implementation for the resource bar controller
 * @author Jameson
 */
public class ResourceBarController extends CatanController implements IResourceBarController, Observer
{
	private Map<ResourceBarElement, Action> elementActions;
	private ClientModel model;
	private IMessageView messageView;
	
	public ResourceBarController(IResourceBarView view, ClientModel model)
    {

		super(view);
		
		this.model = model;
        ((ClientModelImp)model).addObserver(this);
		messageView = new MessageView();
		
		elementActions = new HashMap<ResourceBarElement, Action>();
	}

	@Override
	public IResourceBarView getView() {
		return (IResourceBarView)super.getView();
	}

	/**
	 * Sets the action to be executed when the specified resource bar element is clicked by the user
	 * 
	 * @param element The resource bar element with which the action is associated
	 * @param action The action to be executed
	 */
	public void setElementAction(ResourceBarElement element, Action action) {

		elementActions.put(element, action);
	}

	@Override
	public void buildRoad() {
		executeElementAction(ResourceBarElement.ROAD);
	}

	@Override
	public void buildSettlement() {
		executeElementAction(ResourceBarElement.SETTLEMENT);
	}

	@Override
	public void buildCity() {
		executeElementAction(ResourceBarElement.CITY);
	}

	@Override
	public void buyCard() {
		executeElementAction(ResourceBarElement.BUY_CARD);
	}

	@Override
	public void playCard() {
		executeElementAction(ResourceBarElement.PLAY_CARD);
	}
	
	private void executeElementAction(ResourceBarElement element) {
		
		if (elementActions.containsKey(element)) {
			
			Action action = elementActions.get(element);
			action.execute();
		}
	}
	
	@Override
	public void update(Observable o, Object arg)
    {
		System.out.println("resources have been updated");
        try
        {
            Player player = model.getGame().getMyPlayer();
            PlayerIdentity identity = player.getIdentity();

            ResourceHand resourceHand = setResources(identity);
            setBuildPool(player);

            int playedSoldiers = player.getPlayedSoldiers();
            getView().setElementAmount(ResourceBarElement.SOLDIERS, playedSoldiers);

            int deckDevCards = model.getGame().getDeck().getTotalCount();
            getView().setElementAmount(ResourceBarElement.BUY_CARD, deckDevCards);

            Phase phase = model.getGame().getTurnTracker().getCurrentPhase();
            boolean disableActions = !(phase == Phase.PLAYING);
            BuyPermissions bp = new BuyPermissionsImp(resourceHand, player, model.getGame().getDeck());

            if (bp.canBuyRoad() && !disableActions)
                getView().setElementEnabled(ResourceBarElement.ROAD, true);
            else
                getView().setElementEnabled(ResourceBarElement.ROAD, false);

            if (bp.canBuySettlement() && !disableActions)
                getView().setElementEnabled(ResourceBarElement.SETTLEMENT, true);
            else
                getView().setElementEnabled(ResourceBarElement.SETTLEMENT, false);

            if (bp.canBuyCity() && !disableActions)
                getView().setElementEnabled(ResourceBarElement.CITY, true);
            else
                getView().setElementEnabled(ResourceBarElement.CITY, false);

            if (bp.canBuyDevCard() && !disableActions)
//            if (true)
                getView().setElementEnabled(ResourceBarElement.BUY_CARD, true);
            else
                getView().setElementEnabled(ResourceBarElement.BUY_CARD, false);
            
            if (!disableActions)
//            if (true)
                getView().setElementEnabled(ResourceBarElement.PLAY_CARD, true);
            else
                getView().setElementEnabled(ResourceBarElement.PLAY_CARD, false);
        }
        catch (DoNotUpdateException ignore)
        {}
	}

    private Player getLocalPlayer() throws DoNotUpdateException
    {
        try
        {
           return model.getGame().getMyPlayer();
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            messageView.setTitle("Game Error");
            messageView.setMessage("Game object has not been properly initialized.");
            messageView.showModal();
            throw new DoNotUpdateException();
        }
    }

    private ResourceHand setResources(PlayerIdentity identity) throws DoNotUpdateException
    {
        try
        {
            ResourceHand resourceHand = model.getGame().getPlayer(identity).getRecHand();
            getView().setElementAmount(ResourceBarElement.BRICK, resourceHand.getBrick());
            getView().setElementAmount(ResourceBarElement.SHEEP, resourceHand.getSheep());
            getView().setElementAmount(ResourceBarElement.WHEAT, resourceHand.getWheat());
            getView().setElementAmount(ResourceBarElement.WOOD, resourceHand.getWood());
            getView().setElementAmount(ResourceBarElement.ORE, resourceHand.getOre());
            return resourceHand;
        }
        catch (PlayerNotFoundException e)
        {
            throw new DoNotUpdateException();
        }
    }

    private void setBuildPool(Player player)
    {
        BuildPool buildPool = player.getBuildPool();
        getView().setElementAmount(ResourceBarElement.ROAD, buildPool.getRoads());
        getView().setElementAmount(ResourceBarElement.SETTLEMENT, buildPool.getSettlements());
        getView().setElementAmount(ResourceBarElement.CITY, buildPool.getCities());
    }

}

