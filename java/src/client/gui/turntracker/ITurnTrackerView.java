package client.gui.turntracker;

import client.gui.base.*;
import shared.definitions.*;

/**
 * Interface for the turn tracker view, which displays whose turn it is, and
 * what state the game is in
 */
public interface ITurnTrackerView extends View
{
	
	/**
	 * Sets the color to display for the local server.model.player
	 * 
	 * @param value
	 *            The local server.model.player's color
	 */
	void setLocalPlayerColor(CatanColor value);
	
	/**
	 * Initializes the properties for a server.model.player in the turn tracker display
	 * 
	 * @param playerIndex
	 *            The server.model.player's index (0 - 3)
	 * @param playerName
	 *            The server.model.player's name
	 * @param playerColor
	 *            The server.model.player's color
	 */
	void initializePlayer(int playerIndex, String playerName,
						  CatanColor playerColor);
	
	/**
	 * Updates the properties for a server.model.player in the turn tracker display
	 * 
	 * @param playerIndex
	 *            The server.model.player's index (0-3)
	 * @param points
	 *            The number of victory points the server.model.player has
	 * @param highlight
	 *            Whether or not the server.model.player's display should be highlighted
	 * @param largestArmy
	 *            Whether or not the server.model.player has the largest army
	 * @param longestRoad
	 *            Whether or not the server.model.player has the longest road
	 */
	void updatePlayer(int playerIndex, int points, boolean highlight,
					  boolean largestArmy, boolean longestRoad);
	
	/**
	 * Updates the game state button's message and enable state
	 * 
	 * @param stateMessage
	 *            The new message to be displayed in the game state button
	 * @param enable
	 *            Whether or not the game state button should be enabled
	 */
	void updateGameState(String stateMessage, boolean enable);
}

