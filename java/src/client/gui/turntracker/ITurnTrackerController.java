package client.gui.turntracker;

import client.gui.base.*;

/**
 * Interface for the turn tracker controller
 */
public interface ITurnTrackerController extends Controller
{
	
	/**
	 * This is called when the local server.model.player ends their turn
	 */
	void endTurn();
}

