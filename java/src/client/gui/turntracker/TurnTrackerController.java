package client.gui.turntracker;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import client.exceptions.FinishTurnException;
import client.facade.PlayerFacade;
import client.gui.base.CatanController;
import client.gui.map.state.PlayingState;
import client.model.ClientModel;
import client.model.ClientModelImp;
import client.model.game.player.Player;


/**
 * Implementation for the turn tracker controller
 */
public class TurnTrackerController extends CatanController implements ITurnTrackerController, Observer
{
    private ClientModel model;
    private Player localPlayer;
    private boolean[] initialized = {false, false, false, false};
    
	public TurnTrackerController(ITurnTrackerView view, ClientModel model)
    {
		
		super(view);
        this.model = model;
        localPlayer = model.getGame().getMyPlayer();
        ((ClientModelImp)model).addObserver(this);
	}
	
	@Override
	public ITurnTrackerView getView()
    {
		
		return (ITurnTrackerView)super.getView();
	}

	@Override
	public void endTurn()
    {
		PlayerFacade facade = model.getPlayerFacade();
		try {
			facade.finishTurn();
		} catch (FinishTurnException e) {
			e.printStackTrace();
			System.out.println("FinishTurn exception, TurnTrackerController");
		}
	}
	
	private void initFromModel()
    {	
		if(model.getGame().getTurnTracker() != null)
        {
        	localPlayer = model.getGame().getMyPlayer();
			getView().setLocalPlayerColor(localPlayer.getColor());
			
			boolean endTurnButton = false;
			String finish = "Finish Turn";
			String waiting = "Waiting for other Players";
			if(model.getCurrentState() instanceof PlayingState)
			{
				endTurnButton = true;
				getView().updateGameState(finish, endTurnButton);
			}
			else
			{
				getView().updateGameState(waiting, endTurnButton);
			}
			
			List<Player> players = model.getGame().getAllPlayers();
        	for(Player p : players)
			{
        		if (!initialized[p.getIndex()])
        		{
        			getView().initializePlayer(p.getIndex(),p.getName(),p.getColor());
        			initialized[p.getIndex()] = true;
        		}
        		
        		boolean highlight = p.getIndex() == model.getGame().getTurnTracker().getIndexOfPlayerWhoseTurnItIs();
        		boolean largestArmy = p.getIndex() == model.getGame().getTurnTracker().getIndexOfPlayerWithLargestArmy();
        		boolean longestRoad = p.getIndex() == model.getGame().getTurnTracker().getIndexOfPlayerWithLongestRoad();
        		
        		getView().updatePlayer(p.getIndex(), p.getVictoryPoints(), highlight, largestArmy, longestRoad);
        		
        		System.out.println("index: " + p.getIndex() 
        				+ " longestRoad: " + longestRoad + " largestArmy: " + largestArmy);
			}
        }
	}

	@Override
	public void update(Observable o, Object arg)
    {
        System.out.println("Turn tracker notified!");
		initFromModel();
	}

}

