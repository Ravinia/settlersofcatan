package client.gui.data;

import client.model.game.player.Player;
import client.model.pregamemodel.user.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Converts a server.model.player object into a server.model.player info object
 * @author Lawrence
 */
public class PlayerInfoFactory
{
    public static PlayerInfo convert(Player player)
    {
        return new PlayerInfo(player.getPlayerId(), player.getIndex(), player.getName(), player.getColor());
    }

    public static List<PlayerInfo> convert(List<Player> players)
    {
        List<PlayerInfo> result = new ArrayList<>();
        for (Player player : players)
            result.add(PlayerInfoFactory.convert(player));
        return result;
    }

    public static PlayerInfo convert(User user)
    {
        return new PlayerInfo(user.getName(), user.getID());
    }
}
