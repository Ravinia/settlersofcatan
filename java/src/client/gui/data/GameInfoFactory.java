package client.gui.data;

import client.model.game.JoinableGame;

import java.util.List;

/**
 * Converts a JoinableGame object into a GameInfo object
 * @author Lawrence
 */
public class GameInfoFactory
{
    public static GameInfo convert(JoinableGame game)
    {
        List<PlayerInfo> players = PlayerInfoFactory.convert(game.getPlayersInGame());
        return new GameInfo(game.getGameID(), game.getGameName(), players);
    }

    public static GameInfo[] convert(List<JoinableGame> games)
    {
        GameInfo[] result = new GameInfo[games.size()];
        for (int i = 0; i < games.size(); i++)
            result[i] = GameInfoFactory.convert(games.get(i));
        return result;
    }
}
