package client.proxyserver;

/**
 * enum class for encapsulation of Server Response Codes
 * @author Lawrence
 */
public enum ServerResponse
{
    OK(200),
    BAD_INPUT(400);

    private int code;

    private ServerResponse(int code)
    {
        this.code = code;
    }

    public int getCode()
    {
        return code;
    }

    public static ServerResponse getResponseCode(int code)
    {
        switch (code)
        {
            case 200:
                return ServerResponse.OK;
            case 400:
                return ServerResponse.BAD_INPUT;
            default:
                return null;
        }
    }
}
