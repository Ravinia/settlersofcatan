package client.proxyserver;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseFactory;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.message.BasicStatusLine;
import org.json.JSONException;

import shared.debug.Command;
import shared.debug.LogLevel;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.player.Cookie;
import client.model.game.PregameGame;
import client.model.game.player.ResourceHand;
import client.model.pregamemodel.user.Password;
import client.model.pregamemodel.user.Username;

public class MockProxyImp implements ProxyServer
{

	private HttpResponse testGameJsonObject()
	{
		HttpResponse response = this.getResponse();
		HttpEntity entity = null;
		try
		{
			entity = new StringEntity("{\"title\": \"test\",\"id\": 4,\"players\": [{},{},{},{}]}");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		response.setEntity(entity);

		return response;
	}

	private HttpResponse getGameCommandsResponse()
	{
		HttpResponse response = this.getResponse();
		HttpEntity entity = null;
		try
		{
			entity = new StringEntity("[{\"number\":5,\"type\":\"rollNumber\",\"playerIndex\":0},{\"content\":\"Hello Chat\",\"type\":\"sendChat\",\"playerIndex\":0}]");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		response.setEntity(entity);

		return response;
	}

	private HttpResponse testAIListJsonObject()
	{
		HttpResponse response = this.getResponse();
		HttpEntity entity = null;
		try
		{
			entity = new StringEntity("[\"LARGEST_ARMY\"]");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		response.setEntity(entity);

		return response;
	}

	private HttpResponse testModelJsonObject()
	{
		HttpResponse response = this.getResponse();
		HttpEntity entity = null;
		try
		{
			entity = new StringEntity("{\"deck\":{\"yearOfPlenty\":2,\"monopoly\":2,\"soldier\":14,\"roadBuilding\":2,\"monument\":5},\"map\":{\"hexes\":[{\"resource\":\"wheat\",\"location\":{\"x\":0,\"y\":-2},\"number\":2},{\"resource\":\"brick\",\"location\":{\"x\":1,\"y\":-2},\"number\":5},{\"resource\":\"sheep\",\"location\":{\"x\":2,\"y\":-2},\"number\":10},{\"resource\":\"wood\",\"location\":{\"x\":-1,\"y\":-1},\"number\":3},{\"resource\":\"wood\",\"location\":{\"x\":0,\"y\":-1},\"number\":6},{\"resource\":\"sheep\",\"location\":{\"x\":1,\"y\":-1},\"number\":12},{\"resource\":\"wheat\",\"location\":{\"x\":2,\"y\":-1},\"number\":11},{\"resource\":\"sheep\",\"location\":{\"x\":-2,\"y\":0},\"number\":9},{\"resource\":\"wood\",\"location\":{\"x\":-1,\"y\":0},\"number\":4},{\"resource\":\"wheat\",\"location\":{\"x\":0,\"y\":0},\"number\":8},{\"resource\":\"wheat\",\"location\":{\"x\":1,\"y\":0},\"number\":6},{\"resource\":\"ore\",\"location\":{\"x\":2,\"y\":0},\"number\":5},{\"resource\":\"ore\",\"location\":{\"x\":-2,\"y\":1},\"number\":3},{\"resource\":\"wood\",\"location\":{\"x\":-1,\"y\":1},\"number\":11},{\"resource\":\"brick\",\"location\":{\"x\":0,\"y\":1},\"number\":4},{\"resource\":\"sheep\",\"location\":{\"x\":1,\"y\":1},\"number\":10},{\"resource\":\"brick\",\"location\":{\"x\":-2,\"y\":2},\"number\":8},{\"location\":{\"x\":-1,\"y\":2}},{\"resource\":\"ore\",\"location\":{\"x\":0,\"y\":2},\"number\":9}],\"roads\":[{\"owner\":\"0\",\"location\":{\"x\":\"2\",\"y\":\"0\",\"direction\":\"SE\"}}],\"cities\":[{\"owner\":1,\"location\":{\"direction\":\"SE\",\"x\":0,\"y\":1}}],\"settlements\":[{\"owner\":\"0\",\"location\":{\"x\":\"0\",\"y\":\"0\",\"direction\":\"SE\"}}],\"radius\":3,\"ports\":[{\"ratio\":3,\"direction\":\"NW\",\"location\":{\"x\":2,\"y\":1}},{\"ratio\":3,\"direction\":\"S\",\"location\":{\"x\":-1,\"y\":-2}},{\"ratio\":2,\"resource\":\"brick\",\"direction\":\"SW\",\"location\":{\"x\":3,\"y\":-3}},{\"ratio\":2,\"resource\":\"sheep\",\"direction\":\"NW\",\"location\":{\"x\":3,\"y\":-1}},{\"ratio\":3,\"direction\":\"S\",\"location\":{\"x\":1,\"y\":-3}},{\"ratio\":2,\"resource\":\"ore\",\"direction\":\"SE\",\"location\":{\"x\":-3,\"y\":0}},{\"ratio\":3,\"direction\":\"NE\",\"location\":{\"x\":-3,\"y\":2}},{\"ratio\":2,\"resource\":\"wood\",\"direction\":\"NE\",\"location\":{\"x\":-2,\"y\":3}},{\"ratio\":2,\"resource\":\"wheat\",\"direction\":\"N\",\"location\":{\"x\":0,\"y\":3}}],\"robber\":{\"x\":-1,\"y\":2}},\"players\":[{\"resources\":{\"brick\":3,\"wood\":10,\"sheep\":4,\"wheat\":1,\"ore\":0},\"oldDevCards\":{\"yearOfPlenty\":1,\"monopoly\":0,\"soldier\":3,\"roadBuilding\":1,\"monument\":1},\"newDevCards\":{\"yearOfPlenty\":3,\"monopoly\":1,\"soldier\":0,\"roadBuilding\":4,\"monument\":0},\"roads\":15,\"cities\":4,\"settlements\":5,\"soldiers\":2,\"victoryPoints\":3,\"monuments\":1,\"playedDevCard\":false,\"discarded\":true,\"playerID\":12,\"playerIndex\":0,\"name\":\"JAM\",\"color\":\"brown\"},{\"resources\":{\"brick\":0,\"wood\":0,\"sheep\":0,\"wheat\":-2,\"ore\":-3},\"oldDevCards\":{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"newDevCards\":{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"roads\":15,\"cities\":3,\"settlements\":6,\"soldiers\":0,\"victoryPoints\":1,\"monuments\":0,\"playedDevCard\":false,\"discarded\":false,\"playerID\":-2,\"playerIndex\":1,\"name\":\"Hannah\",\"color\":\"red\"},null,null],\"log\":{\"lines\":[{\"source\":\"JAM\",\"message\":\"JAM upgraded to a city\"},{\"source\":\"JAM\",\"message\":\"The trade was not accepted\"}]},\"chat\":{\"lines\":[{\"source\":\"JAM\",\"message\":\"This is my message\"}]},\"bank\":{\"brick\":24,\"wood\":24,\"sheep\":24,\"wheat\":26,\"ore\":27},\"tradeOffer\":{\"sender\":0,\"receiver\":1,\"offer\":{\"brick\":0,\"ore\":1,\"sheep\":3,\"wheat\":1,\"wood\":0}},\"turnTracker\":{\"status\":\"FirstRound\",\"currentTurn\":0,\"longestRoad\":2,\"largestArmy\":-1},\"winner\":-1,\"version\":3}");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		response.setEntity(entity);

		return response;
	}

	private HttpResponse testGameListJsonObject()
	{
		HttpResponse response = this.getResponse();
		HttpEntity entity = null;
		try
		{
			entity = new StringEntity("[{\"title\":\"Default Game\",\"id\":0,\"players\":[{\"color\":\"orange\",\"name\":\"Sam\",\"id\":0},{\"color\":\"blue\",\"name\":\"Brooke\",\"id\":1},{\"color\":\"red\",\"name\":\"Pete\",\"id\":10},{\"color\":\"green\",\"name\":\"Mark\",\"id\":11}]},{\"title\":\"AI Game\",\"id\":1,\"players\":[{\"color\":\"orange\",\"name\":\"Pete\",\"id\":10},{\"color\":\"white\",\"name\":\"Squall\",\"id\":-2},{\"color\":\"blue\",\"name\":\"Ken\",\"id\":-2},{\"color\":\"puce\",\"name\":\"Steve\",\"id\":-2}]},{\"title\":\"Empty Game\",\"id\":2,\"players\":[{\"color\":\"orange\",\"name\":\"Sam\",\"id\":0},{\"color\":\"blue\",\"name\":\"Brooke\",\"id\":1},{\"color\":\"red\",\"name\":\"Pete\",\"id\":10},{\"color\":\"green\",\"name\":\"Mark\",\"id\":11}]},{\"title\":\"Jammin Game\",\"id\":3,\"players\":[{\"color\":\"brown\",\"name\":\"JAM\",\"id\":12},{},{},{}]}]");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		response.setEntity(entity);

		return response;
	}

	private HttpResponse getResponse()
	{
		HttpResponseFactory factory = new DefaultHttpResponseFactory();
		return factory.newHttpResponse(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, null), null);
	}

    private HttpResponse getBadResponse()
    {
        HttpResponseFactory factory = new DefaultHttpResponseFactory();
        return factory.newHttpResponse(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_BAD_REQUEST, null), null);
    }

	@Override
	public HttpResponse login(Username username, Password password) throws JSONException
	{
        if (username.getUsername().toLowerCase().equals("bob") && password.getPassword().toLowerCase().equals("bob"))
		    return this.getResponse();
        else
            return this.getBadResponse();
	}

	@Override
	public HttpResponse register(Username username, Password password)
			throws JSONException
	{
		return this.getResponse();
	}

	@Override
	public HttpResponse getGamesList()
	{
		return this.testGameListJsonObject();
	}

	@Override
	public HttpResponse createGame(PregameGame game) throws JSONException
	{
		return this.testGameJsonObject();
	}

	@Override
	public HttpResponse joinGame(int gameId, CatanColor color)
			throws JSONException
	{
		return this.getResponse();
	}

	@Override
	public HttpResponse getGameModel()
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse getGameModel(int version)
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse resetGame()
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse getGameCommands()
	{
		return this.getGameCommandsResponse();
	}

	@Override
	public HttpResponse putGameCommands(List<Command> commands)throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse getAIList()
	{
		return this.testAIListJsonObject();
	}

	@Override
	public HttpResponse addAI(String AIName) throws JSONException
	{
		return this.getResponse();
	}

	@Override
	public HttpResponse changeLogLevel(LogLevel logLevel) throws JSONException
	{
		return this.getResponse();
	}

	@Override
	public HttpResponse sendChat(int currentPlayer, String content)throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse acceptTrade(int currentPlayer, boolean willAccept)
			throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse discardCards(int currentPlayer,ResourceHand discardedCards) throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse rollNumber(int currentPlayer, int number)throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse buildRoad(int currentPlayer, EdgeLocation roadLocation,boolean free) throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse buildSettlement(int currentPlayer,VertexLocation location, boolean free) throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse buildCity(int currentPlayer, VertexLocation location)throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse offerTrade(int currentPlayer, Map<ResourceType, Integer> offer,int receiver) throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse maritimeTrade(int currentPlayer, int ratio,ResourceType inputResource, ResourceType outputResource)throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse finishTurn(int currentPlayer) throws JSONException
	{
		return this.testModelJsonObject();
	}

    @Override
    public HttpResponse robPlayer(int playerIndex, int victimIndex, HexLocation hex)
    {
        return this.testModelJsonObject();
    }

    @Override
	public HttpResponse buyDevCard(int currentPlayer) throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse playRoadBuilding(int currentPlayer, EdgeLocation spot1,EdgeLocation spot2) throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse playYearOfPlenty(int currentPlayer,ResourceType resource1, ResourceType resource2)throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse playSoldier(int currentPlayer, HexLocation location,int victimIndex) throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public HttpResponse playMonopoly(int currentPlayer, ResourceType resource)throws JSONException
	{
		return this.testModelJsonObject();
	}
	
	@Override
	public HttpResponse playMonument(int currentPlayer)throws JSONException
	{
		return this.testModelJsonObject();
	}

	@Override
	public Cookie getUserCookie()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cookie getGameCookie()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
