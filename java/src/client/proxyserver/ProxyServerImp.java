package client.proxyserver;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.exceptions.CatanProxyException;
import client.model.game.PregameGame;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import shared.debug.Command;
import shared.debug.LogLevel;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidHeaderException;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.player.Cookie;
import client.model.game.player.ResourceHand;
import client.model.pregamemodel.user.Password;
import client.model.pregamemodel.user.Username;
import client.serializer.Deserializer;
import client.serializer.Serializer;


public class ProxyServerImp implements ProxyServer
{
	String host;
	String port;
	
	Deserializer deserializer;
	Serializer serializer;
	Logger logger;
	Cookie userCookie;
	Cookie gameCookie;

	@Override
    public Cookie getUserCookie()
    {
        return userCookie;
    }
    
	@Override
    public Cookie getGameCookie()
    {
    	return gameCookie;
    }

    public ProxyServerImp(String host, String port, Deserializer deserializer, Serializer serializer)
	{
		this.host = host;
		this.port = port;
		logger = Logger.getLogger("ProxyServerLogger");
		logger.setLevel(Level.OFF);
		this.deserializer = deserializer;
		this.serializer = serializer;
	}
	
	@Override
	public HttpResponse login(Username username, Password password) throws JSONException, CatanProxyException
	{
		JSONObject postData = serializer.serializeUserNameAndPassword(username, password);
		HttpResponse response = this.httpPost("/user/login", postData);
		if (response.getStatusLine().getStatusCode() == 200)
		{
			try {
				userCookie = deserializer.getCookie(response);
			} catch (InvalidHeaderException e) {
				throw new CatanProxyException();
			}
		}
		return response;
	}
	
	@Override
	public HttpResponse register(Username username, Password password) throws JSONException, CatanProxyException
	{
		JSONObject postData = serializer.serializeUserNameAndPassword(username, password);
		HttpResponse response = this.httpPost("/user/register", postData);
		if (response.getStatusLine().getStatusCode() == 200)
		{
			try
            {
				login(username, password);
			}
            catch (CatanProxyException e)
            {
				throw new CatanProxyException();
			}
		}
		return response;
	}

	@Override
	public HttpResponse getGamesList()
	{
		return this.httpGet("/games/list");
	}

	@Override
	public HttpResponse createGame(PregameGame game) throws JSONException
	{
		JSONObject postData = serializer.serializeGameState(game);
		return this.httpPost("/games/create", postData);
	}

	@Override
	public HttpResponse joinGame(int gameId, CatanColor color) throws JSONException, CatanProxyException
	{
		JSONObject postData = serializer.serializeJoinGameInfo(gameId, color);
		HttpResponse response = this.httpPost("/games/join", postData);
		if (response.getStatusLine().getStatusCode() == 200)
		{
			try
            {
				gameCookie = deserializer.getCookie(response);
			}
            catch (InvalidHeaderException e)
            {
				throw new CatanProxyException();
			}
		}
		return response;
	}

	@Override
	public HttpResponse getGameModel()
	{
		return this.httpGet("/game/model");
	}
	
	@Override
	public HttpResponse getGameModel(int version)
	{
		return this.httpGet("/game/model?version=" + version);
	}

	@Override
	public HttpResponse resetGame()
	{
		return this.httpPost("/game/reset", new JSONObject());
	}

	@Override
	public HttpResponse getGameCommands()
	{
		return this.httpGet("/game/commands");
	}

	@Override
	public HttpResponse putGameCommands(List<Command> commands) throws JSONException
	{
		JSONObject postData = serializer.serializePutGameCommandsInfo(commands);
		return this.httpPost("/games/commands", postData);
	}

	@Override
	public HttpResponse getAIList()
	{
		return this.httpGet("/game/listAI");
	}

	@Override
	public HttpResponse addAI(String AIName) throws JSONException
	{
		JSONObject postData = serializer.serializeAddAIInfo(AIName);
		return this.httpPost("/game/addAI", postData);
	}

	@Override
	public HttpResponse changeLogLevel(LogLevel logLevel) throws JSONException
	{
		JSONObject postData = serializer.serializeChangeLogLevelInfo(logLevel);
		return this.httpPost("/util/changeLogLevel", postData);
	}

	@Override
	public HttpResponse sendChat(int currentPlayer, String content) throws JSONException
	{
		JSONObject postData = serializer.serializeSendChatInfo(currentPlayer, content);
		return this.httpPost("/moves/sendChat", postData);
	}

	@Override
	public HttpResponse acceptTrade(int currentPlayer, boolean willAccept) throws JSONException
	{
		JSONObject postData = serializer.serializeAcceptTradeInfo(currentPlayer, willAccept);
		return this.httpPost("/moves/acceptTrade", postData);
	}

	@Override
	public HttpResponse discardCards(int currentPlayer, ResourceHand discardedCards)
			throws JSONException
	{
		JSONObject postData =
				serializer.serializeDiscardCardsInfo(currentPlayer, discardedCards);
		return this.httpPost("/moves/discardCards", postData);
	}

	@Override
	public HttpResponse rollNumber(int currentPlayer, int number) throws JSONException
	{
		JSONObject postData = serializer.serializeRollNumberInfo(currentPlayer, number);
		return this.httpPost("/moves/rollNumber", postData);
	}

	@Override
	public HttpResponse buildRoad(int currentPlayer, EdgeLocation roadLocation, boolean free)
			throws JSONException
	{
		JSONObject postData = 
				serializer.serializeBuildRoadInfo(currentPlayer, free, roadLocation);
		return this.httpPost("/moves/buildRoad", postData);
	}

	@Override
	public HttpResponse buildSettlement(int currentPlayer, VertexLocation location, boolean free)
			throws JSONException
	{
		JSONObject postData = 
				serializer.serializeBuildSettlementInfo(currentPlayer, location, free);
		return this.httpPost("/moves/buildSettlement", postData);
	}

	@Override
	public HttpResponse buildCity(int currentPlayer, VertexLocation location) throws JSONException
	{
		JSONObject postData = serializer.serializeBuildCityInfo(currentPlayer, location);
		return this.httpPost("/moves/buildCity", postData);
	}

	@Override
	public HttpResponse offerTrade(int currentPlayer, Map<ResourceType, Integer> offer, int receiver)
			throws JSONException
	{
		JSONObject postData = 
				serializer.serializeOfferTradeInfo(currentPlayer, offer, receiver);
		return this.httpPost("/moves/offerTrade", postData);
	}

	@Override
	public HttpResponse maritimeTrade(int currentPlayer, int ratio, ResourceType inputResource,
                                      ResourceType outputResource) throws JSONException
	{
		JSONObject postData = 
				serializer.serializeMaritimeTradeInfo
                        (currentPlayer, ratio, inputResource, outputResource);
		return this.httpPost("/moves/maritimeTrade", postData);
	}

	@Override
	public HttpResponse finishTurn(int currentPlayer) throws JSONException
	{
		JSONObject postData = serializer.serializeFinishTurnInfo(currentPlayer);
		return this.httpPost("/moves/finishTurn", postData);
	}

    @Override
    public HttpResponse robPlayer(int playerIndex, int victimIndex, HexLocation hex)
    {
        JSONObject postData = serializer.serializeRobPlayer(playerIndex, victimIndex, hex);
        return this.httpPost("/moves/robPlayer", postData);
    }

    @Override
	public HttpResponse buyDevCard(int currentPlayer) throws JSONException
	{
		JSONObject postData = serializer.serializeBuyDevCardInfo(currentPlayer);
		return this.httpPost("/moves/buyDevCard", postData);
	}

	@Override
	public HttpResponse playRoadBuilding(int currentPlayer, EdgeLocation spot1,
			EdgeLocation spot2) throws JSONException
	{
		JSONObject postData = 
				serializer.serializePlayRoadBuildingCardInfo(currentPlayer, spot1, spot2);
		return this.httpPost("/moves/Road_Building", postData);
	}

	@Override
	public HttpResponse playYearOfPlenty(int currentPlayer,
                                         ResourceType resource1, ResourceType resource2) throws JSONException
	{
		JSONObject postData = 
				serializer.serializePlayYearOfPlentyCardInfo(currentPlayer, resource1, resource2);
		return this.httpPost("/moves/Year_of_Plenty", postData);
	}

	@Override
	public HttpResponse playSoldier(int currentPlayer, HexLocation location,
			int victimIndex) throws JSONException
	{
		JSONObject postData = 
				serializer.serializePlaySoldierCardInfo(currentPlayer, location, victimIndex);
		return this.httpPost("/moves/Soldier", postData);
	}

	@Override
	public HttpResponse playMonopoly(int currentPlayer, ResourceType resource) throws JSONException
	{
		JSONObject postData = serializer.serializePlayMonopolyCardInfo(currentPlayer, resource);
		return this.httpPost("/moves/Monopoly", postData);
	}
	
	@Override
	public HttpResponse playMonument(int currentPlayer) throws JSONException
	{
		JSONObject postData = serializer.serializePlayMonumentCardInfo(currentPlayer);
		return this.httpPost("/moves/Monument", postData);
	}
	
	public HttpResponse httpGet(String url)
	{
		logger.info(url);
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet("http://" + host + ":" + port + url);
		
		// Attatch cookie as header
		if (userCookie != null)
		{
			request.addHeader("Cookie", userCookie.getEncodedValue());
		}
		if (gameCookie != null)
		{
			request.addHeader("Cookie", gameCookie.getEncodedValue());
		}
		
		logger.info("REQUEST HEADER");
		logger.info(request.toString());
		HttpResponse response = null;
		try
		{
			response = client.execute(request);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return response;
	}

	public HttpResponse httpPost(String url, JSONObject postData)
	{
		logger.info(url);
		logger.info(postData.toString());
		
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost("http://" + host + ":" + port + url);
		
		// Attach cookie as header
		if (userCookie != null)
		{
			request.addHeader("Cookie", userCookie.getEncodedValue());
		}
		if (gameCookie != null)
		{
			request.addHeader("Cookie", gameCookie.getEncodedValue());
		}
		
		HttpResponse response = null;
		try
		{
			StringEntity se = new StringEntity(postData.toString());
			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			request.setEntity(se);
			response = client.execute(request);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return response;
	}
}
