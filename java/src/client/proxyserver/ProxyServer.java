package client.proxyserver;

import java.util.List;
import java.util.Map;

import client.exceptions.CatanProxyException;
import client.model.game.PregameGame;

import org.apache.http.HttpResponse;
import org.json.JSONException;

import shared.debug.Command;
import shared.debug.LogLevel;
//import shared.debug.Command;
//import shared.debug.LogLevel;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.player.Cookie;
import client.model.game.player.ResourceHand;
import client.model.pregamemodel.user.Password;
import client.model.pregamemodel.user.Username;

/**
* Provides a clean interface to interface to interact with the server
* @author Jameson Saunders
* @author Christian Carnley
*/
public interface ProxyServer
{

	/**
	* Upon being called, the proxy server stores your login cookie.
	* @param username The user's username
	* @param password The user's password
	* @return Returns just http code 200 on success. 
	* On an error, it returns code 400 with an error message in the body.
	 * @throws CatanProxyException If there was an error logging in.
	*/
	public HttpResponse login(Username username, Password password) throws JSONException, CatanProxyException;
	
	/**
	* It registers you and then logs you in by setting a cookie.
	* @param username The user's desired username
	* @param password The user's desired password
	* @return Returns just http code 200 on success. 
	* On an error, it returns code 400 with an error message in the body.
	 * @throws CatanProxyException If there was an error registering.
	*/
	public HttpResponse register(Username username, Password password) throws JSONException, CatanProxyException;

	/**
	 * This call returns information about all of the current games.
	 * @return 200 response with a JSON body with information about all the current games. 
	 */
	public HttpResponse getGamesList();
	
	/**
	 * Creates an empty game on the server.
	 * @param game A game object that contains:
	 *   the game's name,
	 *   whether random ports are to be used in the game,
	 *   whether random number tokens are to be used in the game, and
	 *   whether random hexes are to be used in the game
	 * @return A 200 response with and JSON object in the body that has only the game name and id. All server.model.player objects are empty.
	 */
	public HttpResponse createGame(PregameGame game) throws JSONException;
	
	/**
	 * Adds a server.model.player to the game and sets their game cookie
	 * IMPORTANT NOTE REGARDING COOKIES: IF THIS IS THE REAL PROXY SERVER, THEN YOU NEED TO 
	 * USE THE getCookieMethod ON THE RESPONSE OBJECT OF THIS LOGIN METHOD TO RETRIEVE 
	 * THE COOKIE OBJECT. THEN CALL setGameId ON THE USER'S COOKIE USING THE COOKIE 
	 * YOU JUST RETRIEVED.
	 * @param gameId The game ID of the game the server.model.player wishes to join
	 * @param color The color server.model.player the wishes to be
	 * @return A 200 response, the response also contains a "Set-cookie" header setting catan.game to be the game id, with path=�/�
	 * @throws CatanProxyException If there was an error joining the game.
	 */
	public HttpResponse joinGame(int gameId, CatanColor color) throws JSONException, CatanProxyException;
	
	/**
	 * Fetches the JSON for the model of the current game not specifying the version of the current game
	 * @return A 200 response with the JSON game model as the response body
	 */
	public HttpResponse getGameModel();
	
	/**
	 * Fetches the JSON for the model of the current game specifying the version of the current game desired
	 * @param version The version number the client has. If the version number is different from 
	 * the servers, it will return the game model. Otherwise it will just return true
	 * @return A 200 response. If the server has a more recent version of the game model the body will contain the JSON for the game model
	 * otherwise, it will only contain the string "true" 
	 */
	public HttpResponse getGameModel(int version);
	
	/**
	 * Resets the game to how it was after all the players joined
	 * @return A 200 response with a body containing the game model JSON
	 */
	public HttpResponse resetGame();
	
	/**
	 * Gets a list of all the commands played on a game. (GET)
	 * @return A 200 response. The response body contains a JSON list of each command that have been 
	 * executed in the game. 
	 */
	public HttpResponse getGameCommands();
	
	/**
	 * Applies a list of commands to the current game. (POST)
	 * @param commands a list of commands to execute on the server
	 * @return A 200 response if the commands were successfully deserialized. A 400 response if the 
	 * commands could not be deserialized or could not be applied
	 */
	public HttpResponse putGameCommands(List<Command> commands) throws JSONException;
	
	/**
	 * Lists the available AI types that may be added to a game
	 * @return A 200 response. The body contains a JSON list of AI names
	 */
	public HttpResponse getAIList();
	
	/**
	 * Adds an AI to the game
	 * @param AIName The name of the AI to be added
	 * @return A 200 response. The AI with with the AIName passed is added to the game in the next open spot. 
	 * Returns a 400 if the AIName passed is not valid
	 */
	public HttpResponse addAI(String AIName) throws JSONException;
	
	/**
	 * Sets the server's logging level
	 * @param logLevel The log level: SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST
	 * @return A 200 response. The server uses the passed logging level
	 */
	public HttpResponse changeLogLevel(LogLevel logLevel) throws JSONException;
	
	/**
	 * Adds the sent chat to the end of the games chats
	 * @param currentPlayer The ID of the server.model.player who is sending the message
	 * @param content The message that the server.model.player wishes to send
	 * @return A 200 response if sending the chat was successful. A 400
	 * response otherwise 
	 */
	public HttpResponse sendChat(int currentPlayer, String content) throws JSONException;
	
	/**
	 * Accepts or rejects and offered trade 
	 * @param currentPlayer The index of the server.model.player accepting the trade
	 * @param willAccept A boolean saying if the server.model.player will or will not accept the trade
	 * @return A 200 response. If the trade was accepted then players
	 * exchange resources. Otherwise no resources are exchanged. The trade offer is 
	 * then removed.
	 */
	public HttpResponse acceptTrade(int currentPlayer, boolean willAccept) throws JSONException;
	
	/**
	 * Discards cards if a 7 is rolled and you have seven or more cards.
	 * @param currentPlayer The index of the server.model.player who has to discard cards
	 * @param discardedCards The cards the server.model.player has discarded
	 * @return A 200 response if a successful discard. Returns a 400 response otherwise
	 */
	public HttpResponse discardCards(int currentPlayer, ResourceHand discardedCards) throws JSONException;
	
	/**
	 * Passes the rolled number to the server to do the appropriate action
	 * @param currentPlayer The index of the server.model.player who rolled the dice
	 * @param number The number (2-12) that the server.model.player rolled
	 * @return HttpResponse. A 200 response if a successful roll. Returns a 400 response otherwise
	 */
	public HttpResponse rollNumber(int currentPlayer, int number) throws JSONException;
	
	/**
	 * Builds a road for the current server.model.player in the given location
	 * @param currentPlayer The index of the server.model.player building the road
	 * @param free A boolean of whether the road is free or must be paid for
	 * @param roadLocation The edge location of the road
	 * @return A 200 response if the road was build successfully.
	 * Returns a 400 response otherwise
	 */
	public HttpResponse buildRoad(int currentPlayer, EdgeLocation roadLocation, boolean free) throws JSONException;
	
	/**
	 * Builds a settlement for the current server.model.player as long as it is connected to a road
	 * @param currentPlayer The index of the server.model.player building the settlement
	 * @param location The vertex location of the settlement
	 * @param free A boolean of whether the settlement is free or must be paid for
	 * @return A 200 response if the settlement was build successfully.
	 * Returns a 400 response otherwise
	 */
	public HttpResponse buildSettlement(int currentPlayer, VertexLocation location, boolean free) throws JSONException;
	
	/**
	 * Builds a city where you currently have a settlement
	 * @param currentPlayer The index of the server.model.player building the city
	 * @param location The vertex location of the city. Must be where a settlement already
	 * exists.
	 * @return A 200 response if the city was build successfully.
	 * Returns a 400 response otherwise
	 */
	public HttpResponse buildCity(int currentPlayer, VertexLocation location) throws JSONException;
	
	/**
	 * Offer a trade of resource cards to the given server.model.player.
	 * @param currentPlayer The index of the server.model.player offering the trade
	 * @param offer The resource cards that the server.model.player is willing to offer
	 * @param receiver The index of the server.model.player that is to receive the offer
	 * @return A 200 response if the offer was offered successfully.
	 * Returns a 400 response otherwise
	 */
	public HttpResponse offerTrade(int currentPlayer, Map<ResourceType, Integer> offer, int receiver) throws JSONException;
	
	/**
	 * Trade with the resource bank. If the server.model.player is on a port then the ratio may be
	 * 3:1 or 2:1. Otherwise the ratio is 4:1
	 * @param currentPlayer The index of the server.model.player who wants to maritime trade
	 * @param ratio The ratio of the maritime trade. Either 4, 3, or 2
	 * @param inputResource The resource the server.model.player is giving
	 * @param outputResource The resource the server.model.player wants to receive
	 * @return A 200 response if the trade was successful. A 400 response
	 * otherwise
	 */
	public HttpResponse maritimeTrade(int currentPlayer, int ratio, ResourceType inputResource, ResourceType outputResource) throws JSONException;
	
	/**
	 * Tells the server that the given players turn is over
	 * @param currentPlayer The index of the server.model.player who is finishing their turn
	 * @return A 200 response if the call was successful. 
	 * A 400 response otherwise
	 */
	public HttpResponse finishTurn(int currentPlayer) throws JSONException;

    /**
     * Initiates robbing of a server.model.player
     * @param playerIndex your index
     * @param victimIndex the victim's index
     * @param hex the location where you are moving the robber to
     * @return the entire model (200) or error (400)
     */
    public HttpResponse robPlayer(int playerIndex, int victimIndex, HexLocation hex);
	
	/**
	 * Buys a development card for the given server.model.player
	 * @param currentPlayer The index of the server.model.player who wants to buy a development card
	 * @return A 200 response if the development card was successfully
	 * purchased. A 400 response otherwise
	 */
	public HttpResponse buyDevCard(int currentPlayer) throws JSONException;
	
	/**
	 * Plays the "Road Building" development card. This allows the server.model.player to lay two
	 * roads for free.
	 * @param currentPlayer The index of the server.model.player playing the card	 * @param spot1 The edge location for the first road
	 * @param spot2 The edge location for the second road
	 * @return A 200 response if the roads were successfully placed. A
	 * 400 response otherwise
	 */
	public HttpResponse playRoadBuilding(int currentPlayer, EdgeLocation spot1, EdgeLocation spot2) throws JSONException;
	
	/**
	 * Plays the "Year of Plenty" development card. This allows the server.model.player to take any
	 * two resource cards
	 * @param currentPlayer The index of the server.model.player playing the card
	 * @param resource1 The first chosen resource
	 * @param resource2 The second chosen resource
	 * @return A 200 response if the card was played successfully. A 400
	 * response otherwise
	 */
	public HttpResponse playYearOfPlenty(int currentPlayer, ResourceType resource1, ResourceType resource2) throws JSONException;
	
	/**
	 * Plays the "Soldier" development card. This allows the server.model.player to move the robber
	 * to a different location and rob from another affected by the location. It also
	 * adds to the current players army
	 * @param currentPlayer The index of the server.model.player playing the card
	 * @param location The new hex location of the robber
	 * @param victimIndex The index of the person who is to be robbed
	 * @return A 200 response if the card was played successfully. A 400
	 * response otherwise
	 */
	public HttpResponse playSoldier(int currentPlayer, HexLocation location, int victimIndex);
	
	/**
	 * Plays the "Monopoly" development card. This allows the server.model.player to name a resource.
	 * All other players must give the server.model.player all the resource cards they have of the
	 * named type
	 * @param currentPlayer The index server.model.player playing the card
	 * @param resource The named resource that is to be taken
	 * @return A 200 response if the card was played successfully. A 400
	 * response otherwise
	 */
	public HttpResponse playMonopoly(int currentPlayer, ResourceType resource) throws JSONException;
	
	/**
	 * Plays the "Monument" development card. This increments the server.model.player's victory points.
	 * @return A 200 response if the card was played successfully. A 400
	 * response otherwise
	 */
	public HttpResponse playMonument(int currentPlayer) throws JSONException;
	
	public Cookie getUserCookie();
	
	public Cookie getGameCookie();
}
