package client.facade;

import client.exceptions.DeserializationException;
import client.model.ClientModel;
import client.proxyserver.ProxyServer;
import client.serializer.Deserializer;
import org.apache.http.HttpResponse;
import shared.debug.Command;

import java.util.List;

/**
 * Implementation of GameFacade interface
 * @author Lawrence
 */
public class GameFacadeImp implements GameFacade
{
    private ProxyServer proxy;
    private Deserializer deserializer;

    public GameFacadeImp(ProxyServer proxy, Deserializer deserializer)
    {
        this.proxy = proxy;
        this.deserializer = deserializer;
    }

    @Override
    public ClientModel getGameModel() throws DeserializationException
    {
        HttpResponse response = proxy.getGameModel();
        return deserializer.deserializeClientModel(response,proxy );
    }

    @Override
    public void resetGame()
    {

    }

    @Override
    public List<Command> getGameCommands()
    {
        return null;
    }

    @Override
    public void putGameCommands(List<Command> commands)
    {

    }
}
