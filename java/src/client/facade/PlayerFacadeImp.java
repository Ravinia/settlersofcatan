package client.facade;

import java.util.Map;

import org.apache.http.HttpResponse;
import org.json.JSONException;

import shared.definitions.ResourceType;
import shared.locations.HexLocation;
import client.exceptions.BadDiscardException;
import client.exceptions.DeserializationException;
import client.exceptions.FinishTurnException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.exceptions.NotInitializedException;
import client.exceptions.UnexpectedResponseException;
import client.model.ClientModel;
import client.model.game.permissions.Permissions;
import client.model.game.player.Player;
import client.model.game.player.ResourceHand;
import client.proxyserver.ProxyServer;
import client.proxyserver.ServerResponse;
import client.serializer.Deserializer;

public class PlayerFacadeImp implements PlayerFacade
{
	private ProxyServer proxy;
    private Deserializer deserializer;
    private ClientModel model;
	
	public PlayerFacadeImp(ClientModel model) throws NotInitializedException
	{
		this.proxy = model.getProxy();
        this.deserializer = model.getDeserializer();
        this.model = model;
	}
	
	@Override
	public void sendChat(String message)
    {
		try
		{
			// Inform the proxy server that a message needs to be sent
			//User u = model.getLocalUser();
			//System.out.println("user: " + u.getName() + " " + u.getID());
			//Player p = model.getGame().getPlayer(u.getID(), u.getName());
			Player p = model.getGame().getMyPlayer();
			//System.out.println("index: " + p.getIndex());
			HttpResponse http = proxy.sendChat(p.getIndex(), message);
			if(http.getStatusLine().getStatusCode() != 200) {//Should I throw an exception here instead?
				System.out.println("Unable to send message: " + message + "\""); 
				return;
			}
			
			// Update the game with a new model
			http = proxy.getGameModel();
            ClientModel temp = deserializer.deserializeClientModel(http, proxy);
            model.updateModel(temp);
			
		}
        catch (JSONException | InvalidObjectTypeException | DeserializationException e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	public void rollNumber(int numberRolled) throws UnexpectedResponseException, DeserializationException, InvalidObjectTypeException
    {
        int index = model.getGame().getMyPlayer().getIndex();
       HttpResponse response = proxy.rollNumber(index, numberRolled);
        int code = response.getStatusLine().getStatusCode();

        if (code != 200)
            throw new UnexpectedResponseException(ServerResponse.OK, ServerResponse.getResponseCode(code));

        ClientModel temp = deserializer.deserializeClientModel(response, proxy);
        model.updateModel(temp);
	}

	@Override
	public void finishTurn() throws FinishTurnException //need to be able to throw this if they try to click it when permissions says no.
    {
		Player localPlayer = model.getGame().getMyPlayer();
		try {
			HttpResponse response = model.getProxy().finishTurn(localPlayer.getIndex());
			if(response.getStatusLine().getStatusCode() != 200)
			{
				throw new FinishTurnException("error finishing turn, server 400 response");
			}
			response = model.getProxy().getGameModel();
			ClientModel temp = deserializer.deserializeClientModel(response, proxy);
            model.updateModel(temp);
		}
		catch (JSONException | NotInitializedException | InvalidObjectTypeException | DeserializationException e)
		{
			throw new FinishTurnException("error finishing turn, not a server 400", e);
		}
	}

	@Override
	public void robPlayer(int victimIndex, HexLocation location) throws InvalidRobberLocationException
    {
		Permissions  permissions = model.getPermissions();
		if (!permissions.canMoveRobber(location))
            throw new InvalidRobberLocationException(location);
        try
        {
            int myIndex = model.getGame().getMyPlayer().getIndex();
            HttpResponse response = proxy.robPlayer(myIndex, victimIndex, location);
            if(response.getStatusLine().getStatusCode() != 200)
            {
            	response = proxy.robPlayer(myIndex, myIndex, location);
            }
            ClientModel temp = deserializer.deserializeClientModel(response, proxy);
            model.updateModel(temp);
        }
        catch (DeserializationException | InvalidObjectTypeException e)
        {
            throw new InvalidRobberLocationException(e);
        }
	}

    @Override
    public boolean canMoveRobber(HexLocation location)
    {
        Permissions  permissions = model.getPermissions();
    	return permissions.canMoveRobber(location);
    }

    @Override
	public void discardCards(Map<ResourceType, Integer> resources) throws BadDiscardException
    {
    	try
    	{
	    	int currentPlayer = this.model.getGame().getMyPlayer().getIndex();
	    	ResourceHand discardedCards = new ResourceHand(resources);
	    	HttpResponse response = this.proxy.discardCards(currentPlayer, discardedCards);
	    	this.deserializer.deserializeClientModel(response, this.proxy);
    	}
    	catch(DeserializationException e)
    	{
    		throw new BadDiscardException();
    	}
	}

}
