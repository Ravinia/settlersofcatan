/**
 * 
 */
package client.facade;

import java.util.Map;

import client.exceptions.*;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.HexLocation;

/**
 * Contains functions for server.model.player interaction.
 * This should be a subclass of the Model Facade
 * 
 * @author Lawrence Thatcher
 *
 */
public interface PlayerFacade 
{
	/**
	 * Sends a chat message from the current server.model.player.
	 * @param message the message you want to send
	 */
	public void sendChat(String message);
	
	/**
	 * Simulates a dice roll in the game for the current server.model.player.
	 */
	public void rollNumber(int numberRolled) 
            throws UnexpectedResponseException, DeserializationException, InvalidObjectTypeException;
	
	/**
	 * Tells the server that the current server.model.player's turn is over
	 * @throws FinishTurnException 
	 */
	public void finishTurn() throws FinishTurnException;
	
	/**
	 * Initiates the "rob" sequence on another server.model.player
	 * @param victimIndex the server.model.player to rob
	 * @param location the new location of the robber
     * @throws InvalidPlayerException
	 */
	public void robPlayer(int victimIndex, HexLocation location) throws InvalidRobberLocationException;

    /**
     * Checks whether the robber can be moved to the new location or not.
     * The robber cannot move there if it is not a land hex, or if the robber is already at that location.
     *
     * @param location the new desired location of the robber
     * @return   true if you can, false otherwise
     */
    public boolean canMoveRobber(HexLocation location);
	
	/**
	 *  Discards cards if a 7 is rolled and you have seven or more cards.
	 * @param resources A list of the resources to discard. If multiple of one resource is discarded,
	 * then that resource will appear the appropriate number of times in the list.
	 */
	public void discardCards(Map<ResourceType, Integer> resources) throws BadDiscardException;
}
