package client.facade;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.json.JSONException;

import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import client.exceptions.BuyDevCardException;
import client.exceptions.DeserializationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.NotInitializedException;
import client.model.ClientModel;
import client.model.game.player.Player;
import client.proxyserver.ProxyServer;
import client.serializer.Deserializer;

public class DevCardFacadeImp implements DevCardFacade {
	
	private ClientModel model;
	private Player player;
	private ProxyServer proxy;
	private Deserializer deserializer;
	private Logger logger;
	
	public DevCardFacadeImp(ClientModel model)
	{
		this.model = model;
		logger = Logger.getLogger("Dev Card Facade Logger");
		logger.setLevel(Level.FINEST);
		player = model.getGame().getMyPlayer();
		
		try {
			this.proxy = model.getProxy();
		} catch (NotInitializedException e) {
			e.printStackTrace();
		}
		
		player = model.getGame().getMyPlayer();
		
		this.deserializer = model.getDeserializer();
	}

	@Override
	public void buyDevCard() throws BuyDevCardException {
		try {
			HttpResponse response = proxy.buyDevCard(player.getIndex());
			if(response.getStatusLine().getStatusCode() != 200)
			{
				logger.info(response.toString());
				String body = deserializer.getHttpBody(response);
				logger.info(body);
				throw new BuyDevCardException("error buying dev card, server 400 response");
			}
			response = model.getProxy().getGameModel();
			ClientModel temp = deserializer.deserializeClientModel(response, proxy);
            model.updateModel(temp);
		} 
		catch (JSONException | NotInitializedException | InvalidObjectTypeException | DeserializationException e)
		{
			throw new BuyDevCardException("error buying dev card, not a server 400", e);
		}
	}

	@Override
	public void playYearOfPlenty(ResourceType resource1, ResourceType resource2) throws BuyDevCardException{
		try {
			HttpResponse response = proxy.playYearOfPlenty(player.getIndex(), resource1, resource2);
			if(response.getStatusLine().getStatusCode() != 200)
			{
				logger.info(response.toString());
				String body = deserializer.getHttpBody(response);
				logger.info(body);
				throw new BuyDevCardException("error buying dev card, server 400 response");
			}
			response = model.getProxy().getGameModel();
			ClientModel temp = deserializer.deserializeClientModel(response, proxy);
            model.updateModel(temp);
		} 
		catch (JSONException | NotInitializedException | InvalidObjectTypeException | DeserializationException e)
		{
			throw new BuyDevCardException("error buying dev card, not a server 400", e);
		}
	}

	@Override
	public void playRoadBuilding(EdgeLocation location1, EdgeLocation location2)
			throws BuyDevCardException {
		try {
			HttpResponse response = proxy.playRoadBuilding(player.getIndex(), location1, location2);
			if(response.getStatusLine().getStatusCode() != 200)
			{
				logger.info(response.toString());
				String body = deserializer.getHttpBody(response);
				logger.info(body);
				throw new BuyDevCardException("error buying dev card, server 400 response");
			}
			response = model.getProxy().getGameModel();
			ClientModel temp = deserializer.deserializeClientModel(response, proxy);
            model.updateModel(temp);
		} 
		catch (JSONException | NotInitializedException | InvalidObjectTypeException | DeserializationException e)
		{
			throw new BuyDevCardException("error buying dev card, not a server 400", e);
		}
	}

	@Override
	public void playSoldier(HexLocation location, int victimIndex)
			throws BuyDevCardException{
		try {
			HttpResponse response = proxy.playSoldier(player.getIndex(), location, victimIndex);
			if(response.getStatusLine().getStatusCode() != 200)
			{
				logger.info(response.toString());
				String body = deserializer.getHttpBody(response);
				logger.info(body);
				throw new BuyDevCardException("error buying dev card, server 400 response");
			}
			response = model.getProxy().getGameModel();
			ClientModel temp = deserializer.deserializeClientModel(response, proxy);
            model.updateModel(temp);
		} 
		catch (JSONException | NotInitializedException | InvalidObjectTypeException | DeserializationException e)
		{
			throw new BuyDevCardException("error buying dev card, not a server 400", e);
		}
	}

	@Override
	public void playMonopoly(ResourceType resource) throws BuyDevCardException{
		try {
			HttpResponse response = proxy.playMonopoly(player.getIndex(), resource);
			if(response.getStatusLine().getStatusCode() != 200)
			{
				logger.info(response.toString());
				String body = deserializer.getHttpBody(response);
				logger.info(body);
				throw new BuyDevCardException("error buying dev card, server 400 response");
			}
			response = model.getProxy().getGameModel();
			ClientModel temp = deserializer.deserializeClientModel(response, proxy);
            model.updateModel(temp);
		} 
		catch (JSONException | NotInitializedException | InvalidObjectTypeException | DeserializationException e)
		{
			throw new BuyDevCardException("error buying dev card, not a server 400", e);
		}
	}
	
	@Override
	public void playMonument() throws BuyDevCardException{
		try {
			HttpResponse response = proxy.playMonument(player.getIndex());
			if(response.getStatusLine().getStatusCode() != 200)
			{
				logger.info(response.toString());
				String body = deserializer.getHttpBody(response);
				logger.info(body);
				throw new BuyDevCardException("error buying dev card, server 400 response");
			}
			response = model.getProxy().getGameModel();
			ClientModel temp = deserializer.deserializeClientModel(response, proxy);
            model.updateModel(temp);
		} 
		catch (JSONException | NotInitializedException | InvalidObjectTypeException | DeserializationException e)
		{
			throw new BuyDevCardException("error buying dev card, not a server 400", e);
		}
	}

}
