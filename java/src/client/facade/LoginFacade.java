package client.facade;

import client.exceptions.LoginException;
import client.model.pregamemodel.user.User;

public interface LoginFacade
{
	/**
	 * Logs in a user and sets a login cookie on the browser.
	 * @param username The user's user name
	 * @param password The user's password
	 * @throws LoginException Thrown if an error occurs during login
	 */
	public void login(String username, String password) throws LoginException;
	
	/**
	 * Registers a user and logs them in by setting a cookie on the browser.
	 * @param username The user's user name
	 * @param password The user's password
	 * @return A reference to the user's User object
	 * @throws LoginException Thrown if an error occurs during login or registration
	 */
	public User register(String username, String password) throws LoginException;

	public boolean isValidUsername(String username);

	public boolean isValidPassword(String password);
}
