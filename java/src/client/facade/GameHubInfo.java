package client.facade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import client.exceptions.*;
import client.model.ClientModel;
import client.model.game.*;
import client.model.game.player.AIName;

import org.apache.http.HttpResponse;
import org.json.JSONException;

import shared.definitions.CatanColor;
import client.proxyserver.ProxyServer;
import client.serializer.Deserializer;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.NotInitializedException;

/**
 * Mainly this place will provide for the receptacle of data needed by
 * the GameHubs functions in future implementation.
 * 
 * @author Eric
 * @author Lawrence
 */
public class GameHubInfo implements GameHub
{
	private ProxyServer proxy;
    private Deserializer deserializer;
    private ClientModel model;

    public GameHubInfo(ClientModel model) throws NotInitializedException
    {
        this.proxy = model.getProxy();
        this.deserializer = model.getDeserializer();
        this.model = model;
    }

    @Override
	public List<JoinableGame> getGames() throws DeserializationException
    {
        HttpResponse http = proxy.getGamesList();
        Collection<Game> games = deserializer.deserializeGameList(http);
        List<JoinableGame> result = new ArrayList<>();
        for (Game game : games)
            result.add((JoinableGame) game);
        return result;
	}

	@Override
	public void rejoinGame(PregameGame game) throws CannotJoinGameException
    {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadGame(int gameID)
    {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createGame(String gameName, boolean randomTiles, boolean randomNumbers, boolean randomPorts)
            throws DeserializationException, InvalidObjectTypeException, CreateGameException
    {
        PregameGame game = new PregameGameImp(gameName, randomTiles, randomNumbers, randomPorts);
        HttpResponse response = proxy.createGame(game);
        if(response.getStatusLine().getStatusCode() == 200)
        {
        	response = proxy.getGameModel();
        }
        else
        {
        	throw new CreateGameException("Location: class-GameHubInfo, Method createGame");
        }
       

	}

	@Override
	public void joinGame(int gameID, CatanColor color, String gameName) throws CannotJoinGameException
    {
        try
        {
            HttpResponse response = proxy.joinGame(gameID, color);
            if (response.getStatusLine().getStatusCode() != 200)
                throw new CannotJoinGameException("Unable to join game: " + response.getEntity().toString());

            //updates the model with the latest info from the server
            response = proxy.getGameModel();
            ClientModel temp = deserializer.deserializeClientModel(response, proxy);
            if (temp != null)
            	model.updateModel(temp);
        }
        catch (CatanProxyException e)
        {
            throw new CannotJoinGameException("An error occurred while trying to send your request to the server.\n" +
                                                "Please try again later.");
        }
        catch (DeserializationException | InvalidObjectTypeException e)
        {
            throw new CannotJoinGameException("An error occurred while trying to deserialize the client model.\n" +
                                                e.getMessage() + "\n" +
                                                "Warning: the data might be corrupt.");
        }
	}

	@Override
	public void resetGame(int gameID)
    {
		// TODO Auto-generated method stub
		
	}

    @Override
    public List<AIName> getAIType() throws NotInitializedException
    {
		HttpResponse response = model.getProxy().getAIList();
		return (List<AIName>) deserializer.deserializeAIList(response);
    }

    @Override
    public void addAI(AIName name) throws CannotAddAIException
    {
    	try 
    	{
			HttpResponse response = model.getProxy().addAI(name.getName());
			if(response.getStatusLine().getStatusCode() != 200)
			{
				throw new CannotAddAIException("Game was full");
			}
			response = proxy.getGameModel();
			ClientModel temp = deserializer.deserializeClientModel(response,proxy );
			model.updateModel(temp);
		} 
    	catch (NotInitializedException | JSONException | DeserializationException | InvalidObjectTypeException e) 
    	{
			throw new CannotAddAIException("unexpected error", e);
		} 
    	
    }
}
