/**
 * 
 */
package client.facade;

import java.util.List;

import client.exceptions.DeserializationException;
import client.model.ClientModel;

import shared.debug.Command;

/**
 * Contains functions for getting and updating information about the current game.
 * This should be a subclass of the Model Facade.
 * 
 * @author Lawrence Thatcher
 *
 */
public interface GameFacade 
{
	/**
	 * Fetches the JSON for the model of the current game, deserializes it and returns it as an
     * intermediate ClientModel object
	 */
	public ClientModel getGameModel() throws DeserializationException;
	
	/**
	 * Resets the current game to how it was after all the players joined
	 */
	public void resetGame();
	
	/**
	 * Gets a list of all the commands played on a game.
	 * @return a list of commands
	 */
	public List<Command> getGameCommands();
	
	/**
	 * Applies a list of commands to the current game.
	 * Primarily used for debugging purposes.
	 * @param commands
	 */
	public void putGameCommands(List<Command> commands);
}
