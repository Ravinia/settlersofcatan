package client.facade;

import java.util.Map;

import org.apache.http.HttpResponse;

import shared.definitions.ResourceType;
import client.exceptions.BadMaritimeRequest;
import client.exceptions.DeserializationException;
import shared.exceptions.NotInitializedException;
import shared.exceptions.PlayerNotFoundException;
import client.exceptions.UserNotInitializedException;
import client.model.ClientModel;
import client.proxyserver.ProxyServer;
import client.serializer.Deserializer;

public class TradeFacadeImp implements TradeFacade
{
	private ProxyServer proxy;
    private Deserializer deserializer;
    private ClientModel model;
    
	public TradeFacadeImp(ClientModel model) throws NotInitializedException
	{
		this.proxy = model.getProxy();
        this.deserializer = model.getDeserializer();
        this.model = model;
	}
	
	@Override
	public void offerTrade(int otherplayerIndex, Map<ResourceType, Integer> offer) throws PlayerNotFoundException, UserNotInitializedException, DeserializationException
	{
		int currentPlayerIndex = this.model.getGame().getMyPlayer().getIndex();
		HttpResponse response = this.proxy.offerTrade(currentPlayerIndex, offer, otherplayerIndex);
		if(response.getStatusLine().getStatusCode() == 200)
		{
			this.deserializer.deserializeClientModel(response, proxy);
		}
	}

	@Override
	public void acceptTrade(boolean answer, int currentPlayer) throws DeserializationException
	{
		HttpResponse response = this.proxy.acceptTrade(currentPlayer, answer);
		if(response.getStatusLine().getStatusCode() == 200)
		{
			this.deserializer.deserializeClientModel(response, proxy);
		}
		
	}

	@Override
	public void maritimeTrade(int offerNum, ResourceType offer, ResourceType receipt) throws UserNotInitializedException, PlayerNotFoundException, DeserializationException, BadMaritimeRequest
	{
		int currentPlayer = this.model.getGame().getMyPlayer().getIndex();
		HttpResponse response = this.proxy.maritimeTrade(currentPlayer, offerNum, offer, receipt);
		if(response.getStatusLine().getStatusCode() == 200)
		{
			this.deserializer.deserializeClientModel(response, this.proxy);
		}
		else
		{
			throw new BadMaritimeRequest();
		}
    }

}
