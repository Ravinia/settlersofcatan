package client.facade;

import org.apache.http.HttpResponse;

import shared.locations.EdgeLocation;
import shared.locations.VertexLocation;
import client.exceptions.BuildCityException;
import client.exceptions.BuildRoadException;
import client.exceptions.BuildSettlementException;
import client.exceptions.DeserializationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.NotInitializedException;
import client.model.ClientModel;
import client.model.game.player.Player;
import client.proxyserver.ProxyServer;

public class BuildFacadeImp implements BuildFacade {

    private ProxyServer proxy;
    private Player localPlayer;
    private ClientModel model;
	
    
    public BuildFacadeImp(ProxyServer proxy,
			Player localPlayer, ClientModel model) {
		super();
		this.proxy = proxy;
		this.localPlayer = localPlayer;
		this.model = model;
	}
	@Override
	public void buildRoad(EdgeLocation location, boolean free)
			throws BuildRoadException 
	{
		try {
			HttpResponse response = proxy.buildRoad(localPlayer.getIndex(), location, free);
			if(response.getStatusLine().getStatusCode() != 200)
			{
				throw new BuildRoadException("error building road, server 400");
			}
			response = model.getProxy().getGameModel();
			ClientModel temp = model.getDeserializer().deserializeClientModel(response, proxy);
            model.updateModel(temp);
		} 
		catch (NotInitializedException | DeserializationException | InvalidObjectTypeException e) 
		{
			throw new BuildRoadException("error building road, not a server 400", e);
		}
	}

	@Override
	public void buildSettlement(VertexLocation location, boolean free)
			throws BuildSettlementException
	{
		try {
				HttpResponse response = proxy.buildSettlement(localPlayer.getIndex(), location, free);
				if(response.getStatusLine().getStatusCode() != 200)
				{
					throw new BuildSettlementException("error building settlement, server 400");
				}
				response = model.getProxy().getGameModel();
				ClientModel temp = model.getDeserializer().deserializeClientModel(response, proxy);
	            model.updateModel(temp);
			} 
			catch (NotInitializedException | DeserializationException | InvalidObjectTypeException e) 
			{
				throw new BuildSettlementException("error building settlement, not a server 400", e);
			}
	}

	@Override
	public void buildCity(VertexLocation location)
			throws BuildCityException
	{
		try {
			HttpResponse response = proxy.buildCity(localPlayer.getIndex(), location);
			if(response.getStatusLine().getStatusCode() != 200)
			{
				throw new BuildCityException("error building city, server 400");
			}
			response = model.getProxy().getGameModel();
			ClientModel temp = model.getDeserializer().deserializeClientModel(response, proxy);
            model.updateModel(temp);
            model.getGame().getMyPlayer().getBuildPool().addSettlement();
		} 
		catch (NotInitializedException | DeserializationException | InvalidObjectTypeException e) 
		{
			throw new BuildCityException("error building city, not a server 400", e);
		}

	}

}
