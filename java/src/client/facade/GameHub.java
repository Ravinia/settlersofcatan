package client.facade;

import java.util.List;

import shared.exceptions.InvalidObjectTypeException;
import client.model.game.JoinableGame;
import client.model.game.player.AIName;
import shared.definitions.CatanColor;
import client.exceptions.CannotAddAIException;
import client.exceptions.CannotJoinGameException;
import client.exceptions.CreateGameException;
import client.exceptions.DeserializationException;
import shared.exceptions.NotInitializedException;
import client.model.game.PregameGame;

/**
 * A place for the functionality of choosing, creating, joining, and rejoining games.
 * 
 * @author Lawrence
 * @author Eric
 *
 */
public interface GameHub 
{
	/**
	 * Returns the available games on the server
	 * 
	 * @return a list of all of the games
	 */
	public List<JoinableGame> getGames() throws DeserializationException;
	
	/**
	 * will create a game
	 *
	 */
	public void createGame(String gameName, boolean randomTiles, boolean randomNumbers, boolean randomPorts)
            throws DeserializationException, InvalidObjectTypeException, CreateGameException;
	
	/**
	 *  Allows for the joining of a game by a server.model.player
	 * 
	 * @param gameID the ID of the game to join
	 * @throws CannotJoinGameException -- if the game is full.
	 */
	public void joinGame(int gameID, CatanColor color, String gameName) throws CannotJoinGameException;
	
	/**
	 * allows a server.model.player to rejoin a game and change their color
	 * 
	 * @param game the Game object of the game to join
	 * @throws CannotJoinGameException-- if for some reason they weren't in the game before.
	 */
	public void rejoinGame(PregameGame game) throws CannotJoinGameException;
	
	/**
	 * Loads the game with the given id
	 * to be played where you left off
	 * 
	 * @param gameID the game ID of the game to join
	 */
	public void loadGame(int gameID);
	
	/**
	 * resets the game with the given id
	 * 
	 * @param gameID the ID of the game to reset
	 */
	public void resetGame(int gameID);

    /**
     *Lists the available AI types that may be added to a game
     * @return list of AI names
     * @throws NotInitializedException 
     */
    public List<AIName> getAIType() throws NotInitializedException;

    /**
     * Adds an AI to the game
     * @param name the name of the AI server.model.player
     * @return a reference to the newly created AI's server.model.player object
     * @throws CannotAddAIException 
     */
    public void addAI(AIName name) throws CannotAddAIException;
}
