package client.facade;

import shared.locations.EdgeLocation;
import shared.locations.VertexLocation;
import client.exceptions.BuildCityException;
import client.exceptions.BuildRoadException;
import client.exceptions.BuildSettlementException;
import client.exceptions.InsufficientResourcesException;

/**
 * Handles functions that involve building and placing objects on the board.
 * @author Lawrence Thatcher
 * @author Eric Hatch
 *
 */
public interface BuildFacade 
{
	/**
	 * Checks to see if the provided location is valid for placing a road,
	 *  then updates the server with the information.
	 *  
	 *  This method automatically adjusts the valid road placement position
	 *   depending on the current state.
	 *  
	 * @param location location of the edge to place a road on.
	 * @throws InsufficientResourcesException
	 */
	public void buildRoad(EdgeLocation location, boolean free) throws BuildRoadException;
	
	/**
	 * Checks to see if the provided location is valid for placing a settlement,
	 *  then updates the server with the information.
	 *  
	 *  This method automatically adjusts the valid road placement position
	 *   depending on the current state.
	 * 
	 * @param location location of the vertex to place a settlement on.
	 * @throws InsufficientResourcesException
	 * @throws BuildSettlementException 
	 */
	public void buildSettlement(VertexLocation location, boolean free) throws BuildSettlementException;
	
	/**
	 * Checks to see if the provided location is valid for placing a city,
	 *  then updates the server with the information.
	 * 
	 * @param location location of the vertex of the settlement to upgrade to a city.
	 * @throws InsufficientResourcesException
	 */
	public void buildCity(VertexLocation location) throws BuildCityException;
}
