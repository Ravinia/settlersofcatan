package client.facade;

import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import client.exceptions.BuyDevCardException;
import client.exceptions.InsufficientBuildPoolException;
import client.exceptions.InsufficientResourcesException;
import shared.exceptions.InvalidLocationException;

/**
 * The part of the facade that handles buying and playing development cards.
 * @author Lawrence Thatcher
 *
 */
public interface DevCardFacade 
{
	
	/**
	 * Buys a development card for the given server.model.player
	 * Throws an InsufficientResourcesException when the server.model.player does not have enough
	 * resources to buy a development card
	 * @throws InsufficientResourcesException
	 */
	public void buyDevCard() throws BuyDevCardException;
	
	/**
	 * Plays the "Year of Plenty" development card. This allows the server.model.player to take any
	 * two resource cards
	 * @param resource1 The first chosen resource
	 * @param resource2 The second chosen resource
	 */
	public void playYearOfPlenty(ResourceType resource1, ResourceType resource2) throws BuyDevCardException;
	
	/**
	 * Plays the "Road Building" development card. This allows the server.model.player to lay two
	 * roads for free.
	 * @param location1 The edge location for the first road
	 * @param location2 The edge location for the second road
	 * @throws InvalidLocationException
	 * @throws InsufficientBuildPoolException
	 */
	public void playRoadBuilding(EdgeLocation location1, EdgeLocation location2) throws BuyDevCardException;
	
	/**
	 * Plays the "Soldier" development card. This allows the server.model.player to move the robber
	 * to a different location and rob from another affected by the location. It also
	 * adds to the current players army
	 * @param location The new hex location of the robber
	 * @param victimIndex The index of the person who is to be robbed
	 */
	public void playSoldier(HexLocation location, int victimIndex)  throws BuyDevCardException;
	
	/**
	 * Plays the "Monopoly" development card. This allows the server.model.player to name a resource.
	 * All other players must give the server.model.player all the resource cards they have of the
	 * named type
	 * @param resource The named resource that is to be taken
	 */
	public void playMonopoly(ResourceType resource) throws BuyDevCardException;
	
	/**
	 * Plays the "Monument" development card. This increments the victory points of the server.model.player.
	 */
	public void playMonument() throws BuyDevCardException;
}
