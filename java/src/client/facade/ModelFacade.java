package client.facade;

import shared.debug.LogLevel;
import client.model.game.permissions.Permissions;
import client.model.game.state.State;
import client.model.game.state.TurnOrder;

/**
 * Master Facade used for communication with the GUI.
 * This class should include all of the necessary functions for the user to interact
 * with the game and automatically handles server updates and data retrieval.
 * 
 * @author Lawrence Thatcher
 * @author Eric Hatch
 *
 */
public interface ModelFacade extends State, TurnOrder, BuildFacade, DevCardFacade,
        LoginFacade, TradeFacade, PlayerFacade, GameFacade, Permissions
{
	/**
	  * Sets the server's logging level
	 * @param logLevel The log level: SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST
	 */
	public void changeLogLevel(LogLevel loglevel);
}
