package client.facade;

import client.exceptions.*;
import client.model.ClientModel;
import client.model.pregamemodel.user.Password;
import client.model.pregamemodel.user.User;
import client.model.pregamemodel.user.Username;
import client.proxyserver.ProxyServer;
import client.proxyserver.ServerResponse;
import client.serializer.Deserializer;

import org.apache.http.HttpResponse;
import shared.exceptions.NotInitializedException;

/**
 * Created by Lawrence on 10/15/2014.
 */
public class LoginFacadeImp implements LoginFacade
{
    private ProxyServer proxy;
    private Deserializer deserializer;
    private ClientModel model;

    public LoginFacadeImp(ClientModel model) throws NotInitializedException
    {
        this.proxy = model.getProxy();
        this.deserializer = model.getDeserializer();
        this.model = model;
    }

    @Override
    public void login(String username, String password) throws LoginException
    {
        Username name = new Username(username);
        Password pass = new Password(password);

        try
        {
            HttpResponse http = proxy.login(name, pass);
            ServerResponse response = deserializer.deserializeResponse(http);

            if (response != ServerResponse.OK)
                throw new UnexpectedResponseException(ServerResponse.OK, response);
        }
        catch (CatanProxyException | DeserializationException | UnexpectedResponseException e)
        {
            throw new LoginException(e);
        }
    }

    @Override
    public User register(String username, String password) throws LoginException
    {
    	Username name = new Username(username);
        Password pass = new Password(password);

        try
        {
            HttpResponse http = proxy.register(name, pass);
            ServerResponse response = deserializer.deserializeResponse(http);

            if (response != ServerResponse.OK)
                throw new UnexpectedResponseException(ServerResponse.OK, response);
        }
        catch (CatanProxyException | DeserializationException | UnexpectedResponseException e)
        {
            throw new LoginException(e);
        }

        return null;
    }

	@Override
	public boolean isValidUsername(String username)
	{
		boolean valid = username.matches("([a-zA-Z]|_|-|[0-9])*");
		int length = username.length();
		if(length < 3 || length > 7)
		{
			valid = false;
		}
		return valid;
	}

	@Override
	public boolean isValidPassword(String password)
	{
		boolean valid = password.matches("([a-zA-Z]|_|-|[0-9])*");
		int length = password.length();
		if(length < 5)
		{
			valid = false;
		}
		return valid;
	}
}
