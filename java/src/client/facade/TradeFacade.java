package client.facade;

import java.util.Map;

import shared.definitions.ResourceType;
import client.exceptions.BadMaritimeRequest;
import client.exceptions.DeserializationException;
import shared.exceptions.PlayerNotFoundException;
import client.exceptions.UserNotInitializedException;


/**
 * An interface to properly control and facilitate trade in the catan game
 * 
 * @author eric
 *
 */
public interface TradeFacade 
{
	/**
	 * Offers a trade to another server.model.player with a list of the resources desired to be traded
	 * and a list of those desired to be received
	 * 
	 * @param otherplayer    the server.model.player the current server.model.player wants to propose a trade with
	 * @param offer          the list of resources to be given away
	 * @param receipt        the list of resources to be received
	 */
	public void offerTrade(int otherplayerIndex, Map<ResourceType, Integer> offer)throws UserNotInitializedException, PlayerNotFoundException, DeserializationException;
	
	/**
	 * accepts or rejects a trade offered by another server.model.player
	 * 
	 * @param answer      true/false  --whether you accept
	 * @param currentPlayer    index of the current server.model.player
	 */
	public void acceptTrade(boolean answer, int currentPlayer) throws DeserializationException;
	
	/**
	 * Offers a trade to the bank containing the list of resources to be traded
	 * and the list of resources to be received
	 * 
	 * @param offerNum	number of resources given
	 * @param offer    resource to be traded
	 * @param receipt   resource to be received
	 */
	public void maritimeTrade(int offerNum, ResourceType offer, ResourceType receipt) throws UserNotInitializedException, PlayerNotFoundException, DeserializationException, BadMaritimeRequest;
	
}
