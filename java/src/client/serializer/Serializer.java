package client.serializer;

import java.util.List;
import java.util.Map;

import client.model.game.PregameGame;

import org.json.JSONException;
import org.json.JSONObject;

import shared.debug.Command;
import shared.debug.LogLevel;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import client.model.game.player.ResourceHand;
import client.model.pregamemodel.user.Password;
import client.model.pregamemodel.user.Username;

/**
 * Serializes requests into JSON Objects for the server
 * 
 * @author Eric
 * @author Jameson
 *
 */
public interface Serializer {

	/** 
	 * Serializes the game to be passed to the server
	 * for changes to game from the client.
	 * @return JSON object---containing game
	 */
	public JSONObject serializeGameState(PregameGame game) throws JSONException;
	
	/**
	 * Serializes join game info into a list of name/value pairs.
	 * @return The serialized data.
	 */
    public JSONObject serializeRobPlayer(int playerIndex, int victimIndex, HexLocation location)throws JSONException;
	
	public JSONObject serializeUserNameAndPassword(Username username, Password password) throws JSONException;
	
	public JSONObject serializeJoinGameInfo(int gameId, CatanColor color) throws JSONException;
	
	public JSONObject serializePutGameCommandsInfo(List<Command> commands) throws JSONException;
	
	public JSONObject serializeAddAIInfo(String AIName) throws JSONException;
	
	public JSONObject serializeChangeLogLevelInfo(LogLevel logLevel) throws JSONException;
	
	public JSONObject serializeSendChatInfo(int currentPlayer, String content) throws JSONException;
	
	public JSONObject serializeAcceptTradeInfo(int currentPlayer, boolean willAccept) throws JSONException;
	
	public JSONObject serializeDiscardCardsInfo(int currentPlayer, ResourceHand discardedCards) throws JSONException;
	
	public JSONObject serializeRollNumberInfo(int currentPlayer, int number) throws JSONException;
	
	public JSONObject serializeBuildRoadInfo(int currentPlayer, boolean free, EdgeLocation roadLocation) throws JSONException;
	
	public JSONObject serializeBuildSettlementInfo(int currentPlayer, VertexLocation location, boolean free) throws JSONException;
	
	public JSONObject serializeBuildCityInfo(int currentPlayer, VertexLocation location) throws JSONException;
	
	public JSONObject serializeOfferTradeInfo(int currentPlayer, Map<ResourceType, Integer> offer, int receiver) throws JSONException;
	
	public JSONObject serializeMaritimeTradeInfo(int currentPlayer, int ratio, ResourceType inputResource, ResourceType outputResource)
            throws JSONException;
	
	public JSONObject serializePlayYearOfPlentyCardInfo(int currentPlayer, ResourceType resource1, ResourceType resource2)
            throws JSONException;
	
	public JSONObject serializeFinishTurnInfo(int currentPlayer) throws JSONException;
	
	public JSONObject serializeBuyDevCardInfo(int currentPlayer) throws JSONException;
	
	public JSONObject serializePlayMonumentCardInfo(int currentPlayer) throws JSONException;
	
	public JSONObject serializePlayRoadBuildingCardInfo(int currentPlayer, EdgeLocation spot1, EdgeLocation spot2) throws JSONException;
		
	public JSONObject serializePlaySoldierCardInfo(int currentPlayer, HexLocation location, int victimIndex) throws JSONException;
	
	public JSONObject serializePlayMonopolyCardInfo(int currentPlayer, ResourceType resource) throws JSONException;
}
