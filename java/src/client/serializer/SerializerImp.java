package client.serializer;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import client.model.game.PregameGame;

import org.json.JSONObject;

import com.google.gson.Gson;

import shared.debug.Command;
import shared.debug.LogLevel;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import client.model.game.player.ResourceHand;
import client.model.pregamemodel.user.Password;
import client.model.pregamemodel.user.Username;

import org.json.JSONException;

/**
 * 
 * @author Jameson
 *
 */
public class SerializerImp implements Serializer
{
	
	Gson serializer = new Gson();

	@Override
	public JSONObject serializeGameState(PregameGame game) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("randomTiles", String.valueOf(game.isRandomTiles()));
		object.put("randomNumbers", String.valueOf(game.isRandomTokens()));
		object.put("randomPorts", String.valueOf(game.isRandomPorts()));
		object.put("name", game.getGameName());
		
		return object;
	}

    @Override
    public JSONObject serializeRobPlayer(int playerIndex, int victimIndex, HexLocation hex)
    {
        JSONObject object = new JSONObject();
        object.put("type","robPlayer");
        object.put("playerIndex", new Integer(playerIndex).toString());
        object.put("victimIndex", new Integer(victimIndex).toString());

        JSONObject location = new JSONObject();
        location.put("x", hex.getX());
        location.put("y", hex.getY());
        object.put("location", location);

        return object;
    }

    @Override
	public JSONObject serializeUserNameAndPassword(Username username, Password password)
			 throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("username", username.getUsername());
		object.put("password", password.getPassword());
		return object;
	}

	@Override
	public JSONObject serializeJoinGameInfo(int gameId, CatanColor color) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("id", new Integer(gameId).toString());
		object.put("color", color.toString().toLowerCase());
		return object;
	}

	// TODO not sure if this is right
	@Override
	public JSONObject serializePutGameCommandsInfo(List<Command> commands) throws JSONException
	{
		JSONObject object = new JSONObject();
		for (Command command : commands)
		{
			object.put("command", command.getCommand());
		}
		return object;
	}

	@Override
	public JSONObject serializeAddAIInfo(String AIName) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("AIType", AIName);
		return object;
	}

	@Override
	public JSONObject serializeChangeLogLevelInfo(LogLevel logLevel) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("logLevel", logLevel.toString());
		return object;
	}

	@Override
	public JSONObject serializeSendChatInfo(int currentPlayer, String content) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "sendChat");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		object.put("content", content);
		return object;
	}

	@Override
	public JSONObject serializeAcceptTradeInfo(int currentPlayer, 
			boolean willAccept) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "acceptTrade");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		object.put("willAccept", new Boolean(willAccept).toString());
		return object;
	}

	@Override
	public JSONObject serializeDiscardCardsInfo(int currentPlayer,
			ResourceHand discardedCards) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "discardCards");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		
		JSONObject second = new JSONObject();
		second.put("brick", new Integer(discardedCards.getBrick()).toString());
		second.put("ore", new Integer(discardedCards.getOre()).toString());
		second.put("sheep", new Integer(discardedCards.getSheep()).toString());
		second.put("wheat", new Integer(discardedCards.getWheat()).toString());
		second.put("wood", new Integer(discardedCards.getWood()).toString());
		object.put("discardedCards", second);
		
		return object;
	}

	@Override
	public JSONObject serializeRollNumberInfo(int currentPlayer, int number) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "rollNumber");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		object.put("number", new Integer(number).toString());
		return object;
	}

	@Override
	public JSONObject serializeBuildRoadInfo(int currentPlayer,
			boolean free, EdgeLocation roadLocation) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "buildRoad");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		
		JSONObject second = new JSONObject();
		second.put("x", new Integer(roadLocation.getHexLoc().getX()).toString());
		second.put("y", new Integer(roadLocation.getHexLoc().getY()).toString());
		second.put("direction", roadLocation.getDir());
		object.put("roadLocation", second);
		
		object.put("free", new Boolean(free).toString());
		
		return object;
	}

	@Override
	public JSONObject serializeBuildSettlementInfo(int currentPlayer,
			VertexLocation location, boolean free) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "buildSettlement");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		
		JSONObject second = new JSONObject();
		second.put("x", new Integer(location.getHexLoc().getX()).toString());
		second.put("y", new Integer(location.getHexLoc().getY()).toString());
		second.put("direction", location.getDir());
		object.put("vertexLocation", second);
		
		object.put("free", new Boolean(free).toString());
		
		return object;
	}

	@Override
	public JSONObject serializeBuildCityInfo(int currentPlayer,
			VertexLocation location) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "buildCity");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		
		JSONObject second = new JSONObject();
		second.put("x", new Integer(location.getHexLoc().getX()).toString());
		second.put("y", new Integer(location.getHexLoc().getY()).toString());
		second.put("direction", location.getDir());
		object.put("vertexLocation", second);
		
		return object;
	}

	@Override
	public JSONObject serializeOfferTradeInfo(int currentPlayer,
			Map<ResourceType, Integer> offer, int receiver) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "offerTrade");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		
		JSONObject second = new JSONObject();
		
		Iterator it = offer.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        ResourceType key = (ResourceType)pairs.getKey();
	        Integer value = (Integer)pairs.getValue();
	        
	        switch(key)
	        {
	        	case BRICK:
	        		second.put("brick", value);
	        		break;
	        	case WOOD:
	        		second.put("wood", value);
	        		break;
	        	case WHEAT:
	        		second.put("wheat", value);
	        		break;
	        	case SHEEP:
	        		second.put("sheep", value);
	        		break;
	        	case ORE:
	        		second.put("ore", value);
	        		break;
	        }
	        it.remove(); 
	    }
	    
		object.put("offer", second);
		
		object.put("receiver", new Integer(receiver).toString());
		
		return object;
	}
	
	@Override
	public JSONObject serializeMaritimeTradeInfo(int currentPlayer, int ratio, 
			ResourceType inputResource, ResourceType outputResource) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "maritimeTrade");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		object.put("ratio", new Integer(ratio).toString());
		object.put("inputResource", inputResource.toString().toLowerCase());
		object.put("outputResource", outputResource.toString().toLowerCase());
		return object;
	}
	
	@Override
	public JSONObject serializePlayYearOfPlentyCardInfo(int currentPlayer,
                                                    ResourceType inputResource, ResourceType outputResource) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "Year_of_Plenty");
		object.put("playerIndex", new Integer(currentPlayer));
		object.put("resource1", inputResource.toString().toLowerCase());
		object.put("resource2", outputResource.toString().toLowerCase());
		return object;
	}

	@Override
	public JSONObject serializeFinishTurnInfo(int currentPlayer) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "finishTurn");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		return object;
	}
	
	@Override
	public JSONObject serializeBuyDevCardInfo(int currentPlayer) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "buyDevCard");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		return object;
	}
	
	@Override
	public JSONObject serializePlayMonumentCardInfo(int currentPlayer) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "Monument");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		return object;
	}
	
	public JSONObject serializePlayRoadBuildingCardInfo(int currentPlayer,
			EdgeLocation spot1, EdgeLocation spot2) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "Road_Building");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		
		JSONObject second = new JSONObject();
		second.put("x", new Integer(spot1.getHexLoc().getX()).toString());
		second.put("y", new Integer(spot1.getHexLoc().getY()).toString());
		second.put("direction", spot1.getDir());
		object.put("spot1", second);
		
		JSONObject third = new JSONObject();
		third.put("x", new Integer(spot2.getHexLoc().getX()).toString());
		third.put("y", new Integer(spot2.getHexLoc().getY()).toString());
		third.put("direction", spot2.getDir());
		object.put("spot2", third);
		
		return object;
	}

	@Override
	public JSONObject serializePlaySoldierCardInfo(int currentPlayer,
			HexLocation location, int victimIndex) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "Soldier");
		object.put("playerIndex", new Integer(currentPlayer).toString());
		object.put("victimIndex", new Integer(victimIndex).toString());
		
		JSONObject second = new JSONObject();
		second.put("x", new Integer(location.getX()).toString());
		second.put("y", new Integer(location.getY()).toString());
		object.put("location", second);
		
		return object;
	}

	@Override
	public JSONObject serializePlayMonopolyCardInfo(int currentPlayer,
                                                ResourceType resource) throws JSONException
	{
		JSONObject object = new JSONObject();
		object.put("type", "Monopoly");
		object.put("resource", resource.toString().toLowerCase());
		object.put("playerIndex", new Integer(currentPlayer).toString());
		
		return object;
	}
	
}
