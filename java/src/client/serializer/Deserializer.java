package client.serializer;

import client.exceptions.DeserializationException;
import client.model.game.Game;
import client.model.game.player.AIName;
import client.proxyserver.ProxyServer;
import client.proxyserver.ServerResponse;

import org.apache.http.HttpResponse;
import org.json.JSONObject;

import shared.exceptions.InvalidHeaderException;
import shared.player.Cookie;
import client.model.ClientModel;

import java.util.Collection;

/**
 * Deserializes the JSONObject back into the objects used by the Client
 * 
 * @author eric
 *
 */
public interface Deserializer
{
    /**
     * Deserializes server response codes (200 OK, etc...) and returns the appropriate code as ServerResponse enum type
     * @param response the http response to deserialize
     * @return enum of the possible responses
     * @throws DeserializationException
     */
    public ServerResponse deserializeResponse(HttpResponse response) throws DeserializationException;

	/**
	 * deserializes the httpResponse and turns it into a game Object
	 * @param response which is the http response from the server
	 * @return a game object
	 */
	public Game deserializeGame(HttpResponse response) throws DeserializationException;
	
	/**
	 * turns the json object into a Game object
	 * @param json a JSONObject
	 * @return a game object
	 */
	public Game deserializeGame(JSONObject json) throws DeserializationException;
	
	/**
	 * deserializes the client model and returns a ClientModel Object
	 * @param clientModelJson from the http server response
	 * @param proxy
     * @return a ClientModel object
	 */
	public ClientModel deserializeClientModel(HttpResponse clientModelJson, ProxyServer proxy) throws DeserializationException;
    /**
     * deserializes the JSON from the getGamesList function returned from the proxy
     * @param gameListJson the JSON of the game list
     * @return a collection of Games
     */
    public Collection<Game> deserializeGameList(HttpResponse gameListJson) throws DeserializationException;
	
	/**
	 * Builds a cookie object from an HttpResponse. The cookie must be included in 
	 * the HttpResponse as a header.
	 * @param response The HttpResponse containing the header cookie.
	 * @return The JSON data extracted from the response. This data is information about the user.
	 * (e.g.) {"name":"Sam","password":"sam","playerID":0}
	 * @throws InvalidHeaderException  If there is an error getting the cookie out of the header.
	 */
	public Cookie getCookie(HttpResponse response) throws InvalidHeaderException;
	
	/**
	 * Retrieves the HttpBody of an HttpResponse
	 * @param response The HttpRespone from which the HttpBody will be extracted.
	 * @return The extracted HttpResponse.
	 */
	public String getHttpBody(HttpResponse response);
	
	/**
	 * Deserializes a list of available AI's
	 * @param response the HTTP response for the AI list
	 * @return A List of AI's
	 */
	public Collection<AIName> deserializeAIList(HttpResponse response);
}
