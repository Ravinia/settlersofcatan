package client.serializer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import client.exceptions.*;
import client.model.game.*;
import client.proxyserver.MockProxyImp;
import client.proxyserver.ProxyServer;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shared.definitions.CatanColor;
import shared.definitions.HexType;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidHeaderException;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidPortTypeException;
import shared.exceptions.PlayerNotFoundException;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.player.Cookie;
import client.model.ClientModel;
import client.model.ClientModelImp;
import client.model.game.board.immutable.Board;
import client.model.game.board.immutable.GameBoard;
import client.model.game.board.immutable.Hex;
import client.model.game.board.immutable.Immutable;
import client.model.game.board.immutable.Token;
import client.model.game.board.immutable.ports.Port;
import client.model.game.board.immutable.ports.PortFactory;
import client.model.game.board.movable.CityPiece;
import client.model.game.board.movable.Placeable;
import client.model.game.board.movable.Road;
import client.model.game.board.movable.Robber;
import client.model.game.board.movable.Settlement;
import client.model.game.board.movable.SettlementPiece;
import client.model.game.chatlog.ChatLog;
import client.model.game.chatlog.ChatLogger;
import client.model.game.chatlog.ChatMessage;
import client.model.game.chatlog.LogMessage;
import client.model.game.chatlog.Message;
import client.model.game.developmentcard.Development;
import client.model.game.developmentcard.DevelopmentCardFactory;
import client.model.game.player.AIName;
import client.model.game.player.AIPlayerName;
import client.model.game.player.BuildPool;
import client.model.game.player.DevCardHand;
import client.model.game.player.DevelopmentCardHand;
import client.model.game.player.GamePlayer;
import client.model.game.player.Player;
import client.model.game.player.PregamePlayer;
import client.model.game.player.ResourceHand;
import shared.definitions.Phase;
import client.model.game.state.TurnOrder;
import client.model.game.state.TurnOrderImp;
import client.model.game.state.TurnTrackerImp;
import client.proxyserver.ServerResponse;

public class DeserializerImp implements Deserializer
{
	private Board board;
	//private PlayableGame currentGame;
	private Logger logger;
    //private ClientModel clientModel;

	public DeserializerImp()
	{
		logger = Logger.getLogger("Deserializer Logger");
	}

    public ServerResponse deserializeResponse(HttpResponse response) throws DeserializationException
    {
        int code = getHttpHeaderResponseCode(response);
        return ServerResponse.getResponseCode(code);
    }

	@Override
	public Game deserializeGame(HttpResponse response) throws DeserializationException
	{
		JSONObject jsonGame = new JSONObject(this.getHttpBody(response));
		return this.deserializeGame(jsonGame);
	}

	@Override
	public Game deserializeGame(JSONObject json) throws DeserializationException
	{
        try
        {
            String gameTitle = json.getString("title");
            int gameId = json.getInt("id");
            JSONArray players = json.getJSONArray("players");
            List<Player> playerList = new ArrayList<Player>();
            for (int i = 0; i < players.length(); i++)
            {
                try
                {
                    String playerColor = players.getJSONObject(i).getString("color");
                    CatanColor color = CatanColor.getCatanColor(playerColor);
                    String playerName = players.getJSONObject(i).getString("name");
                    int playerId = players.getJSONObject(i).getInt("id");

                    PregamePlayer pregamePlayer = new PregamePlayer(playerName, playerId, color);
                    playerList.add(pregamePlayer);
                }
                catch (Exception ignore)
                {
                    //don't add a server.model.player if there is an error
                }
            }
            return new JoinableGameImp(gameId, playerList, gameTitle);
        }
        catch (JSONException e)
        {
        	e.printStackTrace();
            throw new DeserializationException(e);
        }
	}

	@Override
	public Collection<Game> deserializeGameList(HttpResponse gameListJson) throws DeserializationException
	{
        try
        {
            Collection<Game> games = new ArrayList<Game>();
            JSONArray gameList = new JSONArray(this.getHttpBody(gameListJson));
            for (int i = 0; i < gameList.length(); i++)
            {
                Game game = this.deserializeGame(gameList.getJSONObject(i));
                games.add(game);
            }
            return games;
        }
        catch (JSONException e)
        {
        	e.printStackTrace();
            throw new DeserializationException(e);
        }
	}

	/**
	 * Creates a new client model including a new game and board and populates
	 * them with the data from the JSON. The result of this will be merged with
	 * the existing model.
	 * 
	 * @param clientModelJson from the http server response
	 * @param proxy the proxy server
     * @return a new client model to be merged with the existing one
	 * @throws DeserializationException
	 */
	@Override
	public ClientModel deserializeClientModel(HttpResponse clientModelJson, ProxyServer proxy) throws DeserializationException
	{
		ClientModel clientModel = new ClientModelImp();

        //ensure that your ID is correctly set
        if (!(proxy instanceof MockProxyImp))
        {
            Cookie cookie = proxy.getUserCookie();
            int id = cookie.getPlayerID();
            clientModel.getGame().setMyId(id);
        }
        else
        {
            clientModel.getGame().setMyId(12);
        }

		try
		{
			String httpBody = this.getHttpBody(clientModelJson);
			// if the response header just said "true", no need to update
			if(httpBody.compareTo("\"true\"") == 0)
			{
				return null;
			}

			JSONObject model = new JSONObject(httpBody);
			// model JSON object
			JSONObject deck = model.getJSONObject("deck");
			JSONObject map = model.getJSONObject("map");
			JSONArray playersArray = model.getJSONArray("players");
			JSONObject log = model.getJSONObject("log");
			JSONObject chat = model.getJSONObject("chat");
			JSONObject bank = model.getJSONObject("bank");
			JSONObject turnTracker = model.getJSONObject("turnTracker");

			JSONObject tradeOffer = null;
			if(!model.isNull("tradeOffer"))
			{
				tradeOffer = model.getJSONObject("tradeOffer");
			}
			int winner = model.getInt("winner");
			int version = model.getInt("version");

            // players JSON array Object
            Collection<Player> players = this.getPlayersFromJsonArray(playersArray);
            clientModel.getGame().setPlayers(players);
            // --setting the players

			// --setting the winner and game version
			clientModel.getGame().setVersion(version);
			clientModel.getGame().setWinnerID(winner);

			// deck JSON object
			int yearOfPlenty = deck.getInt("yearOfPlenty");
			int monopoly = deck.getInt("monopoly");
			int soldier = deck.getInt("soldier");
			int roadBuilding = deck.getInt("roadBuilding");
			int monument = deck.getInt("monument");
			Deck modelDeck = new DeckImp(yearOfPlenty, monopoly, soldier, roadBuilding, monument);
			clientModel.getGame().setDeck(modelDeck);

			// map JSON object
			// create a new board object
			int radius = map.getInt("radius");
			this.board = new GameBoard(radius);

			// initialize the board with the hexes and ports
			Collection<Hex> hexes = this.getHexesFromJson(map.getJSONArray("hexes"));
			Collection<Port> ports = this.getPortsFromJson(map.getJSONArray("ports"));
			List<Immutable> boardPieces = new ArrayList<Immutable>(hexes);
			boardPieces.addAll(ports);
			board.initializeBoard(boardPieces);

			// add all the placable objects on the board
			Collection<Road> roads = this.getRoadsFromJson(map.getJSONArray("roads"),clientModel);
			Collection<Settlement> cities = this.getCitiesFromJson(map.getJSONArray("cities"),clientModel);
			Collection<Settlement> settlements = this.getSettlementsFromJson(map.getJSONArray("settlements"),clientModel);
			Robber robber = getRobberFromJson(map.getJSONObject("robber"));
			List<Placeable> gamePieces = new ArrayList<Placeable>(roads);
			gamePieces.addAll(cities);
			gamePieces.addAll(settlements);
			gamePieces.add(robber);
			board.updateBoardObjects(gamePieces);

			// --setting the board
			clientModel.setBoard(board);

			// Chat and Log JSON arrays
			JSONArray logLines = log.getJSONArray("lines");
			List<Message> logMessages = getLogMessagesFromJsonArray(logLines);
			JSONArray chatLines = chat.getJSONArray("lines");
			List<Message> chatMessages = getChatMessagesFromJsonArray(chatLines);

			ChatLog chatLog = new ChatLogger(chatMessages, logMessages);
			clientModel.getGame().setChatLog(chatLog);
			// --setting the ChatLog

			// bank JSON object
			ResourceHand bankResources = this.getResourceHand(bank);
			Bank modelBank = new BankImp(bankResources.getBrick(), bankResources.getWood(), 
					bankResources.getSheep(), bankResources.getWheat(), bankResources.getOre());
			clientModel.getGame().setBank(modelBank);

			// an optional tradeOffer JSON object
			if(tradeOffer != null)
			{
				int sender = tradeOffer.getInt("sender");
				int receiver = tradeOffer.getInt("receiver");
				
				ResourceHand offeredResources = this.getResourceHand(tradeOffer.getJSONObject("offer"));
                TradeOffer offer = new TradeOffer(sender, receiver, offeredResources);
                clientModel.getGame().setOffer(offer);
			}

			// turnTracker JSON object
			String status = turnTracker.getString("status");
			int currentTurn = turnTracker.getInt("currentTurn");
            Phase phase = Phase.getPhase(status);
            TurnTrackerImp tracker = new TurnTrackerImp(phase, currentTurn);


            if(!turnTracker.isNull("longestRoad"))
            {
                int longestRoad = turnTracker.getInt("longestRoad");
                tracker.setLongestRoad(longestRoad);
            }
            if(!turnTracker.isNull("largestArmy"))
            {
                int largestArmy = turnTracker.getInt("largestArmy");
                tracker.setLargestArmy(largestArmy);
            }
            clientModel.getGame().setTurnTracker(tracker);
            TurnOrder order = new TurnOrderImp(clientModel.getGame().getAllPlayers(), clientModel.getGame().getMyPlayer().getIndex());
            order.update(tracker);
            clientModel.getGame().setTurnOrder(order);

            clientModel.setGame(clientModel.getGame());
			return clientModel;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new DeserializationException(e);
		}
	}

	private ResourceHand getResourceHand(JSONObject resourceHandJson)
	{
		int brick = resourceHandJson.getInt("brick");
		int ore = resourceHandJson.getInt("ore");
		int sheep = resourceHandJson.getInt("sheep");
		int wood = resourceHandJson.getInt("wood");
		int wheat = resourceHandJson.getInt("wheat");
		return new ResourceHand(sheep, brick, wood, ore, wheat);
	}
	
	private List<Message> getChatMessagesFromJsonArray(JSONArray chatLines) throws JSONException
	{
		List<Message> messages = new ArrayList<Message>();
		for(int i = 0; i < chatLines.length(); i++)
		{
			JSONObject message = chatLines.getJSONObject(i);
			String source = message.getString("source");
			String content = message.getString("message");

			Message chatMessage = new ChatMessage(source, content);
			messages.add(chatMessage);
		}
		return messages;
	}

	private List<Message> getLogMessagesFromJsonArray(JSONArray logLines) throws JSONException
	{
		List<Message> messages = new ArrayList<Message>();
		for(int i = 0; i < logLines.length(); i++)
		{
			JSONObject message = logLines.getJSONObject(i);
			String source = message.getString("source");
			String content = message.getString("message");

			Message logMessage = new LogMessage(source, content);
			messages.add(logMessage);
		}
		return messages;
	}

	private Collection<Player> getPlayersFromJsonArray(JSONArray players) throws JSONException
	{
		Collection<Player> playerCollection = new ArrayList<Player>();
		for(int i = 0; i < players.length(); i++)
		{
			if(!players.isNull(i))
			{
				Player player = this.getPlayerFromJson(players.getJSONObject(i));
                playerCollection.add(player);
			}
		}
		return playerCollection;
	}

	private Player getPlayerFromJson(JSONObject player) throws JSONException
	{
		JSONObject resources = player.getJSONObject("resources");
		JSONObject oldDevCards = player.getJSONObject("oldDevCards");
		JSONObject newDevCards = player.getJSONObject("newDevCards");

		// resources JSON Object
		int brick = resources.getInt("brick");
		int wood = resources.getInt("wood");
		int sheep = resources.getInt("sheep");
		int wheat = resources.getInt("wheat");
		int ore = resources.getInt("ore");
		ResourceHand resourceHand = new ResourceHand(sheep, brick, wood, ore, wheat);

		// oldDevCards JSON Object
		int oldYearOfPlenty = oldDevCards.getInt("yearOfPlenty");
		int oldMonopoly = oldDevCards.getInt("monopoly");
		int oldSoldier = oldDevCards.getInt("soldier");
		int oldRoadBuilding = oldDevCards.getInt("roadBuilding");
		int oldMonument = oldDevCards.getInt("monument");
		List<Development> oldCards = DevelopmentCardFactory.convert(oldYearOfPlenty, oldMonopoly, oldSoldier,
                oldRoadBuilding, oldMonument, true);

		// newDevCards JSON Object
		int newYearOfPlenty = newDevCards.getInt("yearOfPlenty");
		int newMonopoly = newDevCards.getInt("monopoly");
		int newSoldier = newDevCards.getInt("soldier");
		int newRoadBuilding = newDevCards.getInt("roadBuilding");
		int newMonument = newDevCards.getInt("monument");
		List<Development> newCards = DevelopmentCardFactory.convert( newYearOfPlenty, newMonopoly, newSoldier,
                newRoadBuilding, newMonument, false);

		DevelopmentCardHand developmentCardHand = new DevCardHand(oldCards, newCards);

		// other items on the server.model.player JSON Object
		int roads = player.getInt("roads");
		int cities = player.getInt("cities");
		int settlements = player.getInt("settlements");
		BuildPool buildPool = new BuildPool(roads, settlements, cities);

		int soldiers = player.getInt("soldiers");
		int victoryPoints = player.getInt("victoryPoints");
		int monuments = player.getInt("monuments");
		boolean playedDevCard = player.getBoolean("playedDevCard");
		boolean discarded = player.getBoolean("discarded");
		int playerID = player.getInt("playerID");
		int playerIndex = player.getInt("playerIndex");
		String name = player.getString("name");
		String color = player.getString("color");
        CatanColor catanColor = CatanColor.getCatanColor(color);

		return new GamePlayer(name, playerID, playerIndex, developmentCardHand, resourceHand,
                buildPool, catanColor, discarded, monuments, playedDevCard, soldiers, victoryPoints);
	}

	private Robber getRobberFromJson(JSONObject robber) throws JSONException
	{
		int x = robber.getInt("x");
		int y = robber.getInt("y");
		HexLocation hex = new HexLocation(x, y);
		return new Robber(hex);
	}

	private Collection<Port> getPortsFromJson(JSONArray ports) throws JSONException, InvalidPortTypeException,
            InvalidLocationException
	{
		Collection<Port> portCollection = new ArrayList<Port>();
		PortFactory factory = new PortFactory(board);

		for(int i = 0; i < ports.length(); i++)
		{
			JSONObject port = ports.getJSONObject(i);

			String resource = null;
			if(!port.isNull("resource"))
			{
				resource = port.getString("resource");
			}

			int ratio = port.getInt("ratio");

			String direction = port.getString("direction");
			int x = port.getJSONObject("location").getInt("x");
			int y = port.getJSONObject("location").getInt("y");
			HexLocation hex = new HexLocation(x, y);
			EdgeDirection dir = EdgeDirection.getEdgeDirection(direction);
			EdgeLocation edge = new EdgeLocation(hex, dir);

			Port result = factory.convert(resource, ratio, edge);
			portCollection.add(result);
		}
		return portCollection;
	}

	private Collection<Settlement> getSettlementsFromJson(JSONArray settlements, ClientModel clientModel) throws JSONException, PlayerNotFoundException
	{
		Collection<Settlement> settlementCollection = new ArrayList<Settlement>();
		for(int i = 0; i < settlements.length(); i++)
		{
			JSONObject settlement = settlements.getJSONObject(i);

			int owner = settlement.getInt("owner");
			String direction = settlement.getJSONObject("location").getString("direction");
			int x = settlement.getJSONObject("location").getInt("x");
			int y = settlement.getJSONObject("location").getInt("y");

			VertexDirection dir = VertexDirection.getVertexDirection(direction);
			HexLocation hex = new HexLocation(x, y);
			VertexLocation loc = new VertexLocation(hex, dir);
			Player player = clientModel.getGame().getPlayerByIndex(owner);

			SettlementPiece settlementPiece = new SettlementPiece(loc, player);
			settlementCollection.add(settlementPiece);
		}
		return settlementCollection;
	}

	private Collection<Settlement> getCitiesFromJson(JSONArray cities, ClientModel clientModel) throws JSONException, PlayerNotFoundException
	{
		Collection<Settlement> cityPieces = new ArrayList<Settlement>();
		for(int i = 0; i < cities.length(); i++)
		{
			JSONObject city = cities.getJSONObject(i);

			int owner = city.getInt("owner");
			String direction = city.getJSONObject("location").getString("direction");
			int x = city.getJSONObject("location").getInt("x");
			int y = city.getJSONObject("location").getInt("y");

			VertexDirection dir = VertexDirection.getVertexDirection(direction);
			HexLocation hex = new HexLocation(x, y);
			VertexLocation loc = new VertexLocation(hex, dir);
			Player player = clientModel.getGame().getPlayerByIndex(owner);

			CityPiece cityPiece = new CityPiece(loc, player);
			cityPieces.add(cityPiece);
		}
		return cityPieces;
	}

	private Collection<Road> getRoadsFromJson(JSONArray roads, ClientModel clientModel) throws JSONException, PlayerNotFoundException
	{
		Collection<Road> roadCollection = new ArrayList<Road>();
		for(int i = 0; i < roads.length(); i++)
		{
			JSONObject road = roads.getJSONObject(i);
			int owner = road.getInt("owner");
			String direction = road.getJSONObject("location").getString(
					"direction");
			int x = road.getJSONObject("location").getInt("x");
			int y = road.getJSONObject("location").getInt("y");

			EdgeDirection dir = EdgeDirection.getEdgeDirection(direction);
			HexLocation hex = new HexLocation(x, y);
			EdgeLocation loc = new EdgeLocation(hex, dir);

			Player player = clientModel.getGame().getPlayerByIndex(owner);
			Road r = new Road(loc, player);
			roadCollection.add(r);
		}
		return roadCollection;
	}

	private Collection<Hex> getHexesFromJson(JSONArray hexes) throws JSONException
	{
		Collection<Hex> hexCollection = new ArrayList<Hex>();
		for(int i = 0; i < hexes.length(); i++)
		{
			JSONObject hex = hexes.getJSONObject(i);
			ResourceType type = null;
			if(!hex.isNull("resource"))
			{
				String resource = hex.getString("resource");
				type = ResourceType.getResource(resource);
			}
			HexType hexType = HexType.getHexFromResource(type);

			int x = hex.getJSONObject("location").getInt("x");
			int y = hex.getJSONObject("location").getInt("y");
			HexLocation location = new HexLocation(x, y);

			Token token = null;
			if(!hex.isNull("number"))
			{
				int number = hex.getInt("number");
				token = new Token(number);
			}

			Hex h = new Hex(location, hexType, token, board);
			hexCollection.add(h);
		}
		return hexCollection;
	}

	@Override
	public Cookie getCookie(HttpResponse response) throws InvalidHeaderException
	{
		Cookie cookie = null;
		if(response.containsHeader("Set-cookie"))
		{
			cookie = new Cookie();
			logger.info("SET-COOKIE HEADER FOUND");
			Header headers[] = response.getHeaders("Set-cookie");

			// There should only be one cookie header
			assert 1 == headers.length;

			for(Header header : headers)
			{
				String name = header.getName();
				String encodedValue = header.getValue();
				encodedValue = encodedValue.substring(0, encodedValue.length() - 8);
				cookie.setEncodedValue(encodedValue);

				logger.info("Header : " + name + " ,encodedValue : " + encodedValue);
			}
		}
		else
			throw new InvalidHeaderException();
		return cookie;
	}

	private JSONObject testJsonObject() throws JSONException
	{
		return new JSONObject(
				"{\"deck\":{\"yearOfPlenty\":2,\"monopoly\":2,\"soldier\":14,\"roadBuilding\":2,\"monument\":5},\"map\":{\"hexes\":[{\"resource\":\"wheat\",\"location\":{\"x\":0,\"y\":-2},\"number\":2},{\"resource\":\"brick\",\"location\":{\"x\":1,\"y\":-2},\"number\":5},{\"resource\":\"sheep\",\"location\":{\"x\":2,\"y\":-2},\"number\":10},{\"resource\":\"wood\",\"location\":{\"x\":-1,\"y\":-1},\"number\":3},{\"resource\":\"wood\",\"location\":{\"x\":0,\"y\":-1},\"number\":6},{\"resource\":\"sheep\",\"location\":{\"x\":1,\"y\":-1},\"number\":12},{\"resource\":\"wheat\",\"location\":{\"x\":2,\"y\":-1},\"number\":11},{\"resource\":\"sheep\",\"location\":{\"x\":-2,\"y\":0},\"number\":9},{\"resource\":\"wood\",\"location\":{\"x\":-1,\"y\":0},\"number\":4},{\"resource\":\"wheat\",\"location\":{\"x\":0,\"y\":0},\"number\":8},{\"resource\":\"wheat\",\"location\":{\"x\":1,\"y\":0},\"number\":6},{\"resource\":\"ore\",\"location\":{\"x\":2,\"y\":0},\"number\":5},{\"resource\":\"ore\",\"location\":{\"x\":-2,\"y\":1},\"number\":3},{\"resource\":\"wood\",\"location\":{\"x\":-1,\"y\":1},\"number\":11},{\"resource\":\"brick\",\"location\":{\"x\":0,\"y\":1},\"number\":4},{\"resource\":\"sheep\",\"location\":{\"x\":1,\"y\":1},\"number\":10},{\"resource\":\"brick\",\"location\":{\"x\":-2,\"y\":2},\"number\":8},{\"location\":{\"x\":-1,\"y\":2}},{\"resource\":\"ore\",\"location\":{\"x\":0,\"y\":2},\"number\":9}],\"roads\":[],\"cities\":[{\"owner\":1,\"location\":{\"direction\":\"SE\",\"x\":0,\"y\":1}}],\"settlements\":[],\"radius\":3,\"ports\":[{\"ratio\":3,\"direction\":\"NW\",\"location\":{\"x\":2,\"y\":1}},{\"ratio\":3,\"direction\":\"S\",\"location\":{\"x\":-1,\"y\":-2}},{\"ratio\":2,\"resource\":\"brick\",\"direction\":\"SW\",\"location\":{\"x\":3,\"y\":-3}},{\"ratio\":2,\"resource\":\"sheep\",\"direction\":\"NW\",\"location\":{\"x\":3,\"y\":-1}},{\"ratio\":3,\"direction\":\"S\",\"location\":{\"x\":1,\"y\":-3}},{\"ratio\":2,\"resource\":\"ore\",\"direction\":\"SE\",\"location\":{\"x\":-3,\"y\":0}},{\"ratio\":3,\"direction\":\"NE\",\"location\":{\"x\":-3,\"y\":2}},{\"ratio\":2,\"resource\":\"wood\",\"direction\":\"NE\",\"location\":{\"x\":-2,\"y\":3}},{\"ratio\":2,\"resource\":\"wheat\",\"direction\":\"N\",\"location\":{\"x\":0,\"y\":3}}],\"robber\":{\"x\":-1,\"y\":2}},\"players\":[{\"resources\":{\"brick\":0,\"wood\":0,\"sheep\":0,\"wheat\":0,\"ore\":0},\"oldDevCards\":{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"newDevCards\":{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"roads\":15,\"cities\":4,\"settlements\":5,\"soldiers\":0,\"victoryPoints\":0,\"monuments\":0,\"playedDevCard\":false,\"discarded\":false,\"playerID\":12,\"playerIndex\":0,\"name\":\"JAM\",\"color\":\"brown\"},{\"resources\":{\"brick\":0,\"wood\":0,\"sheep\":0,\"wheat\":-2,\"ore\":-3},\"oldDevCards\":{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"newDevCards\":{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"roads\":15,\"cities\":3,\"settlements\":6,\"soldiers\":0,\"victoryPoints\":1,\"monuments\":0,\"playedDevCard\":false,\"discarded\":false,\"playerID\":-2,\"playerIndex\":1,\"name\":\"Hannah\",\"color\":\"red\"},null,null],\"log\":{\"lines\":[{\"source\":\"JAM\",\"message\":\"JAM upgraded to a city\"},{\"source\":\"JAM\",\"message\":\"The trade was not accepted\"}]},\"chat\":{\"lines\":[]},\"bank\":{\"brick\":24,\"wood\":24,\"sheep\":24,\"wheat\":26,\"ore\":27},\"turnTracker\":{\"status\":\"FirstRound\",\"currentTurn\":0,\"longestRoad\":-1,\"largestArmy\":-1},\"winner\":-1,\"version\":3}");
	}

	@Override
	public String getHttpBody(HttpResponse response)
	{
		BufferedReader rd;
		StringBuilder result = new StringBuilder();
		String line = "";
		try
		{
			rd = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			while((line = rd.readLine()) != null)
			{
				result.append(line);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return result.toString();
	}

    public int getHttpHeaderResponseCode(HttpResponse response)
    {
        StatusLine line = response.getStatusLine();
        return line.getStatusCode();
    }

	@Override
	public Collection<AIName> deserializeAIList(HttpResponse response)
	{
		String body = this.getHttpBody(response);
		JSONArray aiArray = new JSONArray(body);

        Collection<AIName> AIs = new ArrayList<AIName>();
		for(int i = 0; i < aiArray.length(); i++)
		{
			String ai = aiArray.getString(i);
			AIName name = new AIPlayerName(ai);
            AIs.add(name);
		}
		return AIs;
	}
}
