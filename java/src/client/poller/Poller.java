package client.poller;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import client.exceptions.DeserializationException;
import shared.exceptions.InvalidObjectTypeException;
import client.model.ClientModel;
import client.model.game.PlayableGame;
import client.proxyserver.*;
import client.serializer.Deserializer;

import org.apache.http.HttpResponse;


public class Poller extends Observable

{
	private ProxyServer proxyServer;
    private ClientModel model;
	private PlayableGame game;
	private Deserializer deserializer;
	private static final int DELAY_TIME = 3; // three second delay between updates
	
	/**
	 * Initializes a Poller object, using dependency injection to give it either a stub proxy server
	 * or a real proxy server, either of which will implement ProxyServerInterface.
	 * @param proxyServer the proxy to use
	 */
	public Poller(ProxyServer proxyServer, ClientModel newModel, Deserializer deserializer)
	{
        this.model = newModel;
		this.proxyServer = proxyServer;
		this.game = model.getGame();
		this.deserializer = deserializer;
		// setting the timer as a daemon, so it will terminate when the program terminates - hopefully
		Timer timer = new Timer(true);
		timer.schedule(new GameStateRequest(model), DELAY_TIME * 1000, DELAY_TIME * 1000);
	}
	
	private void signalObservers()
	{
		this.setChanged();
		super.notifyObservers();
	}
	
	/**
	 * At regular intervals, a timer will trigger to updateGame the game state by calling the
	 * getGameModel(int version) method on the ProxyServerInterface. If the version number of the 
	 * model is different, this class's run method will call the reformGameState method in the Deserializer
	 * class to updateGame the game state.
	 */
	class GameStateRequest extends TimerTask
	{
        private ClientModel model;
        boolean firstUpdate;

        GameStateRequest(ClientModel model)
        {
            this.model = model;
            firstUpdate = true;
        }

		@Override
		public void run()
        {
			// Fetches the game state using the current game ID
			HttpResponse response;
			int size = model.getGame().getAllPlayers().size();
			if(size == 0)
			{
				signalObservers();
				return;
			}
			if(size < 4)
				response = proxyServer.getGameModel();
			else
				response = proxyServer.getGameModel(game.getVersion());
			// If an updateGame is not necessary, expect the string "true"
			// The deserializer will check for this string, not the poller
            try
            {
            	
            	ClientModel tempModel = deserializer.deserializeClientModel(response, proxyServer);
            	if(tempModel != null)
            		model.updateModel(tempModel);
            	else {
            		//int size = model.getGame().getAllPlayers().size();
            		//System.out.println("players: " + size);
            		if(firstUpdate)
            		{
            			model.updateFromPoller();
            			if(firstUpdate)
            				firstUpdate = false;
            		}
            	}
            }
            catch (InvalidObjectTypeException | DeserializationException ignore)
            {
            	ignore.printStackTrace();
            }
            
//            Phase phase = model.getGame().getTurnTracker().getCurrentPhase();
//            System.out.println("current game phase: " + phase);
//            System.out.println("current game state: " + model.getCurrentState().toString());
		}
	}
	
}
