package shared.locations;

import java.io.Serializable;

/**
 * @author Lawrence
 */
public class PortLocation implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -3291939330052097240L;
	private EdgeLocation originalEdge;
    private EdgeLocation edge;
    private VertexLocation vertex1;
    private VertexLocation vertex2;

    public PortLocation(EdgeLocation edge, VertexLocation vertex1, VertexLocation vertex2)
    {
        this.originalEdge = edge;
        this.edge = edge.getNormalizedLocation();
        this.vertex1 = vertex1.getNormalizedLocation();
        this.vertex2 = vertex2.getNormalizedLocation();
    }

    public EdgeLocation getEdge()
    {
        return edge;
    }

    public EdgeLocation getOriginalEdge()
    {
        return originalEdge;
    }

    public VertexLocation getVertex1()
    {
        return vertex1;
    }

    public VertexLocation getVertex2()
    {
        return vertex2;
    }
    
    

    /* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((edge == null) ? 0 : edge.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PortLocation other = (PortLocation) obj;
		if (edge == null) 
		{
			if (other.edge != null)
				return false;
		} 
		else if (!edge.equals(other.edge))
			return false;
		return true;
	}
}
