package shared.locations;

/**
 * Generic location object that EdgeLocation, HexLocation, and VertexLocation inherit from
 * @author Lawrence
 */
public interface Location
{
    public int hashCode();
    public boolean equals(Object obj);
}
