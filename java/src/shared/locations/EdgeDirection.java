package shared.locations;

import java.io.Serializable;

public enum EdgeDirection implements Serializable
{
	
	NorthWest ("NW"),
	North ("N"),
	NorthEast ("NE"),
	SouthEast ("SE"),
	South ("S"),
	SouthWest ("SW");
	
	private EdgeDirection opposite;
	private final String name; 
	
	static
	{
		NorthWest.opposite = SouthEast;
		North.opposite = South;
		NorthEast.opposite = SouthWest;
		SouthEast.opposite = NorthWest;
		South.opposite = North;
		SouthWest.opposite = NorthEast;
	}
	
	public EdgeDirection getOppositeDirection()
	{
		return opposite;
	}     

    private EdgeDirection(String s) {
        name = s;
    }

    public boolean equalsName(String otherName){
        return (otherName == null)? false:name.equals(otherName);
    }

    public String toString(){
       return name;
    }

    public static EdgeDirection getEdgeDirection(String s)
    {
        s = s.toLowerCase();
        s = s.trim();
        if (s.equals("northwest") || s.equals("nw"))
            return EdgeDirection.NorthWest;
        else if (s.equals("north") || s.equals("n"))
            return EdgeDirection.North;
        else if (s.equals("northeast") || s.equals("ne"))
            return EdgeDirection.NorthEast;
        else if (s.equals("southeast") || s.equals("se"))
            return EdgeDirection.SouthEast;
        else if (s.equals("south") || s.equals("s"))
            return EdgeDirection.South;
        else if (s.equals("southwest") || s.equals("sw"))
            return EdgeDirection.SouthWest;
        else
            return null; //or I could throw an exception?
    }
}

