package shared.locations;

import java.io.Serializable;

public enum VertexDirection implements Serializable
{
	West ("W"),
	NorthWest ("NW"), 
	NorthEast ("NE"), 
	East ("E"), 
	SouthEast ("SE"), 
	SouthWest ("SW");
	
	private VertexDirection opposite;
	private final String name; 
	
	static
	{
		West.opposite = East;
		NorthWest.opposite = SouthEast;
		NorthEast.opposite = SouthWest;
		East.opposite = West;
		SouthEast.opposite = NorthWest;
		SouthWest.opposite = NorthEast;
	}
	
	public VertexDirection getOppositeDirection()
	{
		return opposite;
	}
	
	private VertexDirection(String s) {
        name = s;
    }

    public boolean equalsName(String otherName){
        return (otherName == null)? false:name.equals(otherName);
    }

    public String toString(){
       return name;
    }

    public static VertexDirection getVertexDirection(String s)
    {
        s = s.toLowerCase();
        s = s.trim();
        if (s.equals("west") || s.equals("w"))
            return VertexDirection.West;
        else if (s.equals("northwest") || s.equals("nw"))
            return VertexDirection.NorthWest;
        else if (s.equals("northeast") || s.equals("ne"))
            return VertexDirection.NorthEast;
        else if (s.equals("east") || s.equals("e"))
            return VertexDirection.East;
        else if (s.equals("southeast") || s.equals("se"))
            return VertexDirection.SouthEast;
        else if (s.equals("southwest") || s.equals("sw"))
            return VertexDirection.SouthWest;
        else
            return null;
    }
}

