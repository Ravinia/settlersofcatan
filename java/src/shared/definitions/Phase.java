package shared.definitions;

import java.io.Serializable;

import shared.exceptions.UnrecognizedPhaseTypeException;

/**
 * An enumerated type class for the phases of the game
 * @author Eric
 * @author Lawrence
 */
public enum Phase implements Serializable
{
	ROUND1("firstround"),
	ROUND2("secondround"),
	PLAYING("playing"),
	ROBBING("robbing"),
	ROLLING("rolling"),
	DISCARDING("discarding"),
	WAIT_PHASE("waiting");
	
	private String value;
	
	private Phase(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return value;
	}

    public static Phase getPhase(String phase) throws UnrecognizedPhaseTypeException
    {
        phase = phase.toLowerCase();
        phase = phase.trim();
        if (phase.equals("rolling"))
            return ROLLING;
        else if (phase.equals("robbing"))
            return ROBBING;
        else if (phase.equals("playing"))
            return PLAYING;
        else if (phase.equals("discarding"))
            return DISCARDING;
        else if (phase.equals("firstround"))
            return ROUND1;
        else if (phase.equals("secondround"))
            return ROUND2;
        else
            throw new UnrecognizedPhaseTypeException(phase);
    }
}
