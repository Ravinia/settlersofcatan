package shared.definitions;

import java.awt.Color;
import java.io.Serializable;

public enum CatanColor implements Serializable
{
	RED, ORANGE, YELLOW, BLUE, GREEN, PURPLE, PUCE, WHITE, BROWN;
	
	private Color color;
	
	static
	{
		RED.color = new Color(227, 66, 52);
		ORANGE.color = new Color(255, 165, 0);
		YELLOW.color = new Color(253, 224, 105);
		BLUE.color = new Color(111, 183, 246);
		GREEN.color = new Color(109, 192, 102);
		PURPLE.color = new Color(157, 140, 212);
		PUCE.color = new Color(204, 136, 153);
		WHITE.color = new Color(223, 223, 223);
		BROWN.color = new Color(161, 143, 112);
	}
	
	public Color getJavaColor()
	{
		return color;
	}
	
    public static CatanColor getCatanColor(String s)
    {
        s = s.toLowerCase();
        s = s.trim();
        if (s.equals("red"))
            return RED;
        else if (s.equals("orange"))
            return ORANGE;
        else if (s.equals("yellow"))
            return YELLOW;
        else if (s.equals("blue"))
            return BLUE;
        else if (s.equals("green"))
            return GREEN;
        else if (s.equals("purple"))
            return PURPLE;
        else if (s.equals("puce"))
            return PUCE;
        else if (s.equals("white"))
            return WHITE;
        else if (s.equals("brown"))
            return BROWN;
        else
            return null;
    }
}

