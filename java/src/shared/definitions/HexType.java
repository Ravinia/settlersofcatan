package shared.definitions;

import java.io.Serializable;

public enum HexType implements Serializable
{
	
	WOOD, BRICK, SHEEP, WHEAT, ORE, DESERT, WATER;

    public static HexType getHexFromResource(ResourceType type)
    {
        if (type == null)
            return DESERT;
        switch (type)
        {
            case BRICK:
                return HexType.BRICK;
            case WOOD:
                return HexType.WOOD;
            case SHEEP:
                return HexType.SHEEP;
            case WHEAT:
                return HexType.WHEAT;
            case ORE:
                return HexType.ORE;
        }
        return null;
    }
}

