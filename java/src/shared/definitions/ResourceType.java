package shared.definitions;

import java.io.Serializable;

public enum ResourceType implements Serializable
{
	WOOD, BRICK, SHEEP, WHEAT, ORE;


    public static ResourceType getResource(String s)
    {
        s = s.toLowerCase();
        s = s.trim();

        if (s.equals("wood"))
            return WOOD;
        else if (s.equals("brick"))
            return BRICK;
        else if (s.equals("wheat"))
            return WHEAT;
        else if (s.equals("sheep"))
            return SHEEP;
        else if (s.equals("ore"))
            return ORE;
        else
            return null;
    }
    
    public static ResourceType getResourceFromHex(HexType hex)
    {
    	switch(hex)
    	{
    		case WOOD:
    			return WOOD;
    		case BRICK:
    			return BRICK;
    		case SHEEP:
    			return SHEEP;
    		case WHEAT:
    			return WHEAT;
    		case ORE:
    			return ORE;
    	}
    	return null;
    }
    
    public static String getResourceName(ResourceType type)
    {
    	switch(type)
    	{
    	case WOOD:
			return "wood";
		case BRICK:
			return "brick";
		case SHEEP:
			return "sheep";
		case WHEAT:
			return "wheat";
		case ORE:
			return "ore";
    	}
    	return "";
    }
}

