/**
 * 
 */
package shared.debug;

import client.exceptions.InvalidCommandException;

/**
 * Stores a command to send to the server.
 * 
 * @author Lawrence Thatcher
 *
 */
public abstract class Command 
{
	/**
	 * The String version of the command
	 */
	protected String command;
	
	public Command(String command) throws InvalidCommandException
	{
		if (!isValid(command))
		{
			throw new InvalidCommandException();
		}
		this.command = command;
	}
	
	/**
	 * Retrieves the server command in String format
	 * @return the command to execute on the server
	 */
	public abstract String getCommand();
	
	/**
	 * Checks t
	 * @param command
	 * @return
	 */
	public abstract boolean isValid(String command);
}
