package shared.debug;

import client.exceptions.InvalidCommandException;

public class CommandImp extends Command {

	public CommandImp(String command) throws InvalidCommandException
    {
		super(command);
	}

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public boolean isValid(String command)
    {
		return true;
	}

}
