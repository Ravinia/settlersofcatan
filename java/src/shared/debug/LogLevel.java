/**
 * 
 */
package shared.debug;

import java.io.Serializable;

/**
 * Log level for debugging purposes.
 * 
 * @author Lawrence Thatcher
 *
 */
public enum LogLevel implements Serializable
{
	SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST, OFF;
	
	public static LogLevel getLogLevel(String s)
    {
        s = s.toLowerCase();
        s = s.trim();
        if (s.equals("severe"))
            return SEVERE;
        else if (s.equals("warning"))
            return WARNING;
        else if (s.equals("info"))
            return INFO;
        else if (s.equals("config"))
            return CONFIG;
        else if (s.equals("fine"))
            return FINE;
        else if (s.equals("finer"))
            return FINER;
        else if (s.equals("finest"))
            return FINEST;
        else if (s.equals("off"))
            return OFF;
        else
            return null;
    }
}
