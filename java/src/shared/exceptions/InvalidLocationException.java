/**
 * 
 */
package shared.exceptions;

import client.model.game.board.immutable.Hex;
import shared.exceptions.CatanException;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;

/**
 * An exception thrown for trying to place a settlement, city, or road
 * at a location that the rules of the game wouldn't allow.
 * @author Lawrence Thatcher
 *
 */
public class InvalidLocationException extends CatanException
{
	private static final long serialVersionUID = 7737478034169846201L;

    public InvalidLocationException(HexLocation hexLocation, String message)
    {
        super("invalid location: [" + hexLocation.toString() + "]\n" + message);
    }

    public InvalidLocationException(Hex hex, String message)
    {
        super("invalid location: [" + hex.getLocation().toString() + "]\n" + message);
    }

    public InvalidLocationException(EdgeLocation edge)
    {
        super("the edge [" + edge.toString() + "] is not on the map");
    }

    public InvalidLocationException(EdgeLocation edge, String message)
    {
        super("invalid location: [" + edge.toString() + "]\n" + message);
    }

    public InvalidLocationException(VertexLocation vertex)
    {
        super("the vertex [" + vertex.toString() + "] is not on the map");
    }

    public InvalidLocationException(VertexLocation vertex, String message)
    {
        super("invalid location: [" + vertex.toString() + "]\n" + message);
    }
    
    public InvalidLocationException(Exception e)
    {
    	super("invalid location", e);
    }

    public InvalidLocationException(String message, Exception e)
    {
        super(message, e);
    }

    public InvalidLocationException(String message)
    {
        super(message);
    }

}
