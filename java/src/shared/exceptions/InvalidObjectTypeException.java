package shared.exceptions;

import client.model.game.board.immutable.Immutable;
import client.model.game.board.movable.Placeable;
import shared.exceptions.CatanException;

/**
 * Exception thrown for attempting to use the wrong catan object in a given situation
 * @author Lawrence
 */
public class InvalidObjectTypeException extends CatanException
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1618076753610123892L;

	public InvalidObjectTypeException()
    {
        super("Unrecognized Object");
    }

    public InvalidObjectTypeException(Placeable piece)
    {
        super("Unrecognized Object: [" + piece.getType().toString() + "] was not expected in this context");
    }

    public InvalidObjectTypeException(server.model.board.placeable.Placeable piece)
    {
        super("Unrecognized Object: [" + piece.getType().toString() + "] was not expected in this context");
    }

    public InvalidObjectTypeException(Immutable piece)
    {
        super("Unrecognized Object: [" + piece.toString() + "] was expected to be either a Port or a Hex");
    }

    public InvalidObjectTypeException(server.model.board.immutable.Immutable piece)
    {
        super("Unrecognized Object: [" + piece.toString() + "] was expected to be either a Port or a Hex");
    }
}
