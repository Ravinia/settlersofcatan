package shared.exceptions;

import shared.exceptions.InvalidLocationException;
import shared.locations.HexLocation;

/**
 * Thrown when a robber is tried to be placed at an invalid location
 * @author Lawrence
 */
public class InvalidRobberLocationException extends InvalidLocationException
{
    public InvalidRobberLocationException(HexLocation location)
    {
        super(location, "Cannot place robber at this location");
    }

    public InvalidRobberLocationException(Exception e)
    {
        super("Error in placing robber at this location: ", e);
    }

    public InvalidRobberLocationException()
    {
        super("Robber location has not been properly initialized");
    }
}
