/**
 * 
 */
package shared.exceptions;

/**
 * Generic Exception class that all other custom game exceptions extend from.
 * @author Lawrence Thatcher
 *
 */
public class CatanException extends Exception 
{
	private static final long serialVersionUID = 2450963517657839223L;

    public CatanException()
    {
        super();
    }

    public CatanException(String message)
    {
        super(message);
    }

    public CatanException(String message, Exception e)
    {
        super(message, e);
    }
}
