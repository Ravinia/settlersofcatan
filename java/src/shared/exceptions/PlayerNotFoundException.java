package shared.exceptions;

import shared.player.PlayerIdentity;

/**
 * Exception thrown when a given server.model.player cannot be found by the given means
 * @author Lawrence
 */
public class PlayerNotFoundException extends CatanException
{
    public PlayerNotFoundException(int index)
    {
        super("Unable to find server.model.player with index " + index);
    }

    public PlayerNotFoundException(PlayerIdentity identity)
    {
        super("Unable to find server.model.player with id " + identity.getId() + " and name " + identity.getName());
    }
}
