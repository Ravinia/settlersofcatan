package shared.exceptions;


/**
 * Exception thrown when an error occurs with deserializing a cookie from an http header.
 * @author Jameson
 */
public class InvalidHeaderException extends CatanException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4900422359318351987L;

	public InvalidHeaderException()
    {
        super("An error occurred during the deserialization process involving an http header:");
    }
}
