package shared.exceptions;

import shared.definitions.PortType;
import shared.exceptions.CatanException;

/**
 * @author Lawrence
 */
public class InvalidPortTypeException extends CatanException
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1869733992316179548L;

	public InvalidPortTypeException(String type)
    {
        super("The port type '" + type + "' is not a valid port type.");
    }

    public InvalidPortTypeException(PortType type)
    {
        super("The port type '" + type.toString() + "' is not a valid port type.");
    }
}
