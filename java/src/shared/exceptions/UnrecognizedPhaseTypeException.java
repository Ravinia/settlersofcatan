package shared.exceptions;

import shared.exceptions.CatanException;

/**
 * Exception thrown when a phase type is passed in is not an expected one.
 * @author Lawrence
 */
public class UnrecognizedPhaseTypeException extends CatanException
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 8317515596625024965L;

	public UnrecognizedPhaseTypeException(String phase)
    {
        super("The phase type [" + phase + "] is not a recognized Phase");
    }
}
