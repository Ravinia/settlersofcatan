package shared.exceptions;

import shared.exceptions.CatanException;

/**
 * thrown when a high level container object has a method call on a sub-unit of the class
 * that has not yet been properly initialized.
 * @author Lawrence
 */
public class NotInitializedException extends CatanException
{
    public NotInitializedException(String message)
    {
        super(message);
    }

    public NotInitializedException(String message, Exception e)
    {
        super(message,e);
    }
}
