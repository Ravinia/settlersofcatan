package shared.exceptions;

import shared.exceptions.CatanException;

/**
 * Exception thrown if the board object has already been initialized and someone tries to initialize it again.
 */
public class BoardAlreadyInitializedException extends CatanException
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 8087768246335780655L;

	public BoardAlreadyInitializedException()
    {
        super("The board has already been initialized. You are not allowed to try to initialize it again.");
    }
}
