package shared.player;

import java.io.Serializable;

/**
 * @author Lawrence
 */
public class PlayerIdentity implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 4407239749629121776L;
	private int id;
    private String name;

    public PlayerIdentity(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlayerIdentity identity = (PlayerIdentity) o;

        if (id != identity.id) return false;
        if (!name.equals(identity.name)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = id;
        result = 31 * result + name.hashCode();
        return result;
    }
}
