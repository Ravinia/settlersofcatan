package shared.player;

import java.io.Serializable;
import java.net.URLDecoder;

import org.json.JSONObject;

/**
 * Cookie class for sending over to the Server
 * @author Jameson
 */
public class Cookie implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4952900478240568741L;
	private String encodedValue;
	protected int gameId = -1;
	
	public String getDecodedValue()
	{
		@SuppressWarnings("deprecation")
		String decodedValue = URLDecoder.decode(encodedValue);
		return decodedValue;
	}
	
	public String getEncodedValue()
    {
		return encodedValue;
	}

	public void setEncodedValue(String encodedValue)
    {
		this.encodedValue = encodedValue;
		this.parseEncodedValue();
	}

    private void parseEncodedValue()
	{
		String[] twoCookies = this.encodedValue.split("catan.game=");
		if(twoCookies.length == 2)
		{
			this.gameId = Integer.parseInt(twoCookies[1]);
		}
		if(!twoCookies[0].equals(""))
		{
			this.encodedValue = twoCookies[0];
		}
	}
//
//	/**
//     * Sets the game's ID
//     * @param cookie the authentication cookie
//     */
//	public void setGameId(Cookie cookie)
//    {
////		encodedValue = "catan.user=%7B%22name%22%3A%22Sam%22%2C%22password%2"
////				+ "2%3A%22sam%22%2C%22playerID%22%3A0%7D; catan.game=3";
//		// catan.user=%7B%22name%22%3A%22Sam%22%2C%2
//		// 2password%22%3A%22sam%22%2C% 22playerID%22%3A0%7D; catan.game=NN
//		
//		String newEncodedValue = "";
//		StringTokenizer st = new StringTokenizer(encodedValue, "; ");
//		while (st.hasMoreTokens())
//        {
//			String token = st.nextToken();
//			if (!token.contains("catan.game"))
//				newEncodedValue += token;
//		}
//		
//		newEncodedValue += "; " + cookie.getEncodedValue();
//		encodedValue = newEncodedValue;
//	}

    public int getPlayerID()
    {
//        String delimiters = "[.,={\":}]+";
//        String[] parts = getDecodedValue().split(delimiters);
//        Map<String, String> params = getParamMap(parts);
//        return Integer.parseInt(params.get("playerID"));
    	String jsonString = getDecodedValue().substring(getDecodedValue().indexOf('{'));
    	JSONObject json = new JSONObject(jsonString);
    	return json.getInt("playerID");
    }

    public String getPlayerName()
    {
//        String delimiters = "[.,={\":}]+";
//        String[] parts = getDecodedValue().split(delimiters);
//        Map<String, String> params = getParamMap(parts);
//        return params.get("name");
    	String jsonString = getDecodedValue().substring(getDecodedValue().indexOf('{'));
    	JSONObject json = new JSONObject(jsonString);
    	return json.getString("name");
    }

    public String getPassword()
    {
//        String delimiters = "[.,={\":}]+";
//        String[] parts = getDecodedValue().split(delimiters);
//        Map<String, String> params = getParamMap(parts);
//        return params.get("password");
    	String jsonString = getDecodedValue().substring(getDecodedValue().indexOf('{'));
    	JSONObject json = new JSONObject(jsonString);
    	return json.getString("password");
    }

//    private Map<String, String> getParamMap(String[] parts)
//    {
//    	Map<String, String> cookieParams = new HashMap<String, String>();
//    	for(int i = 0; i < parts.length; i+=2)
//    	{
//    		cookieParams.put(parts[i], parts[i+1]);
//    	}
//    	return cookieParams;
//    }

	public int getGameID()
	{
		return this.gameId;
	}
}
