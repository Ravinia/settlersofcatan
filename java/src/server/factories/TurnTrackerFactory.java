package server.factories;

import server.courier.serializer.TurnTrackerInfo;
import server.model.GameModel;
import server.model.GameModelImp;
import server.model.game.PlayableGame;
import server.model.game.state.TurnOrder;
import shared.definitions.Phase;

/**
 * @author Lawrence
 */
public class TurnTrackerFactory
{
    public static TurnTrackerInfo convertTurnTracker(GameModel model)
    {
        TurnOrder order = ((PlayableGame)model.getGame()).getTurnOrder();
        int currentPlayer = order.getIndexOfPlayerWhoseTurnItIs();
        Phase currentPhase = order.getCurrentPhase();
        int longestRoad = ((GameModelImp)model).getIndexOfLongestRoad();
        int largestArmy= ((GameModelImp)model).getIndexOfLargestArmy();
        return new TurnTrackerInfo(currentPlayer, currentPhase, longestRoad, largestArmy);
    }
}
