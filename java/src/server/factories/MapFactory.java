package server.factories;

import server.courier.serializer.*;
import server.exceptions.PlayerNotInGameException;
import server.model.GameModel;
import server.model.board.Board;
import server.model.board.immutable.Hex;
import server.model.board.immutable.ports.Port;
import server.model.board.placeable.Road;
import server.model.board.placeable.Settlement;
import shared.locations.HexLocation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Converts board object into map info object
 * @author Lawrence
 */
public class MapFactory
{
    public static MapInfo convertMap(GameModel model) throws PlayerNotInGameException
    {
        Board board = model.getBoard();
        List<HexInfo> hexes = convertHexes(board.getHexes());
        List<PortInfo> ports = convertPorts(board.getPorts());
        List<RoadInfo> roads = convertRoads(board.getRoads(), model);
        Collection<SettlementInfo> settlements = convertSettlements(board.getSettlements(), model);
        HexLocation robberLocation = board.getRobberLocation();
        return new MapInfoImp(hexes, ports, roads, settlements, robberLocation);
    }

    private static List<HexInfo> convertHexes(Collection<Hex> hexes)
    {
        List<HexInfo> result = new ArrayList<>();
        for (Hex hex : hexes)
        {
            result.add(new HexInfo(hex));
        }
        return result;
    }

    private static List<PortInfo> convertPorts(Collection<Port> ports)
    {
        List<PortInfo> result = new ArrayList<>();
        for (Port port : ports)
        {
            result.add(new PortInfo(port));
        }
        return result;
    }

    private static List<RoadInfo> convertRoads(Collection<Road> roads, GameModel model) throws PlayerNotInGameException
    {
        List<RoadInfo> result = new ArrayList<>();
        for (Road road : roads)
        {
            result.add(convertRoad(road, model));
        }
        return result;
    }

    private static RoadInfo convertRoad(Road road, GameModel model) throws PlayerNotInGameException
    {
        int owner = model.getIndexOfPlayer(road.getPlayer());
        return new RoadInfo(owner, road.getLocation());
    }

    private static Collection<SettlementInfo> convertSettlements(Collection<Settlement> settlements, GameModel model)
            throws PlayerNotInGameException
    {
        List<SettlementInfo> result = new ArrayList<>();
        for (Settlement settlement : settlements)
        {
            result.add(convertSettlement(settlement, model));
        }
        return result;
    }

    private static SettlementInfo convertSettlement(Settlement settlement, GameModel model) throws PlayerNotInGameException
    {
        int owner = model.getIndexOfPlayer(settlement.getPlayer());
        return new SettlementInfo(owner, settlement.getLocation(), settlement.isCity());
    }
}
