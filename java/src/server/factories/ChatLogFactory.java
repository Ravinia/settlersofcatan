package server.factories;

import server.courier.serializer.ChatInfo;
import server.courier.serializer.LogInfo;
import server.model.game.Game;
import server.model.game.PlayableGame;
import server.model.game.chatlog.Message;

import java.util.List;

/**
 * Converts logs and chats to courier objects
 * @author Lawrence
 */
public class ChatLogFactory
{
    public static ChatInfo convertChat(Game game)
    {
        List<Message> chats = ((PlayableGame)game).getChatLog().getChats();
        return new ChatInfo(chats);
    }

    public static LogInfo convertLog(Game game)
    {
        List<Message> logs = ((PlayableGame)game).getChatLog().getLogs();
        return new LogInfo(logs);
    }
}
