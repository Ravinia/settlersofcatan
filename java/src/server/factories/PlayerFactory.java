package server.factories;

import server.courier.serializer.PlayerInfo;
import server.courier.serializer.PlayerInfoImp;
import server.courier.serializer.PregamePlayerInfo;
import server.courier.serializer.PregamePlayerInfoImp;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.game.Game;
import server.model.game.PlayableGame;
import server.model.game.developmentcard.DevelopmentCardHand;
import server.model.game.resources.BuildPool;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.definitions.CatanColor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Factory class for converting objects that represent players from one object type to another.
 * All methods should be static.
 * @author Lawrence
 */
public class PlayerFactory
{
    public static List<PlayerInfo> convertPlayers(Collection<Player> players, GameModel model)
                                throws UninitializedDataException, PlayerNotInGameException
    {
        List<PlayerInfo> result = new ArrayList<>();
        for (Player player : players)
        {
            result.add(convertPlayer(player, model));
        }
        return result;
    }

    public static PlayerInfo convertPlayer(Player player, GameModel model)
                                throws UninitializedDataException, PlayerNotInGameException
    {
        int ID = player.getId();
        String name = player.getName();
        CatanColor color = model.getColorOfPlayer(player);
        int index = model.getIndexOfPlayer(player);
        int settlements = 5;
        int cities = 4;
        int roads = 12;
        int armies = 0;
        int monuments = 0;
        int vPoints = 0;
        boolean discarded = false;
        boolean played = false;
        ResourceHand hand = null;
        DevelopmentCardHand devHand = null;
        
    	if (model.getGame() instanceof PlayableGame)
    	{
	        PlayableGame game = (PlayableGame)model.getGame();
	        BuildPool buildPool = getBuildPool(player, game);
	
	        settlements = buildPool.getSettlements();
	        cities = buildPool.getCities();
	        roads = buildPool.getRoads();
	        armies = game.getNumberOfArmiesPlayed(player);
	        monuments = game.getNumberOfMonumentsPlayed(player);
	        vPoints = game.getVictoryPoints(player);
	        discarded = game.hasPlayerDiscardedThisTurn(player);
	        played = game.hasCurrentPlayerPlayedDevCardThisTurn();
	        hand = game.getResourceHandOfPlayer(player);
	        devHand = game.getDevelopmentCardHandOfPlayer(player);
	        
	        return new PlayerInfoImp(ID,name,color,index,settlements,cities,roads,armies,monuments,vPoints,discarded,played,hand,devHand);
    	}
    	else // PregameGame
    	{
    		return new PlayerInfoImp(ID,name,color,index,settlements,cities,roads,armies,monuments,vPoints,discarded,played,hand,devHand);
    	}
    }

    public static List<PregamePlayerInfo> convertPregamePlayers(Collection<Player> players, Game game) throws UninitializedDataException
    {
        List<PregamePlayerInfo> result = new ArrayList<>();
        for (Player player : players)
        {
        	CatanColor color = game.getColor(player);
            result.add(new PregamePlayerInfoImp(player.getName(),player.getId(), color));
        }
        return result;
    }

    //Helper Methods
    private static BuildPool getBuildPool(Player player, Game game) throws PlayerNotInGameException
    {
        return ((PlayableGame)game).getBuildPoolOfPlayer(player);
    }

}
