package server.factories;

import server.courier.serializer.GameInfo;
import server.courier.serializer.GameInfoImp;
import server.courier.serializer.PregamePlayerInfo;
import server.exceptions.UninitializedDataException;
import server.model.game.PlayableGame;
import server.model.game.PregameGame;

import java.util.List;

/**
 * @author Lawrence
 */
public class GameFactory
{
    public static GameInfo convertPregameGame(PregameGame game) throws UninitializedDataException
    {
        List<PregamePlayerInfo> players = PlayerFactory.convertPregamePlayers(game.getPlayers(), game);
        return new GameInfoImp(game.getName(), game.getID(),players);
    }

    public static GameInfo convertPlayableGame(PlayableGame game) throws UninitializedDataException
    {
        List<PregamePlayerInfo> players = PlayerFactory.convertPregamePlayers(game.getPlayers(), game);
        return new GameInfoImp(game.getName(), game.getID(), players);
    }
}
