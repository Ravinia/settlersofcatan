package server.factories;

import server.courier.serializer.DeckInfo;
import server.model.game.Game;
import server.model.game.PlayableGame;
import server.model.game.bank.Deck;

/**
 * Factory for converting Deck objects to DeckInfo objects
 * @author Lawrence
 */
public class DeckFactory
{
    public static DeckInfo convertDeck(Game game)
    {
        Deck deck = ((PlayableGame)game).getDeck();
        return new DeckInfo(deck.getMonopolyCount(), deck.getMonumentCount(), deck.getRoadBuildingCount(), deck.getSoldierCount(), deck.getYearOfPlentyCount());
    }
}
