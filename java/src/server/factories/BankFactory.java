package server.factories;

import server.courier.serializer.BankInfo;
import server.model.game.Game;
import server.model.game.PlayableGame;
import server.model.game.bank.Bank;

/**
 * Converts a Bank object into a BankInfo object
 * @author Lawrence
 */
public class BankFactory
{
    public static BankInfo convertBank(Game game)
    {
        Bank bank = ((PlayableGame)game).getBank();
        return new BankInfo(bank.getBrick(), bank.getOre(), bank.getSheep(), bank.getWheat(), bank.getWood());
    }
}
