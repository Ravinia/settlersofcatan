package server.factories;

import server.courier.serializer.DevCardHandInfo;
import server.courier.serializer.DevCardListInfo;
import server.model.game.developmentcard.DevelopmentCardHand;
import shared.definitions.DevCardType;

import java.util.Collection;

/**
 * @author Lawrence
 */
public class DevelopmentCardFactory
{
    public static DevCardHandInfo convertHand(DevelopmentCardHand developmentCardHand)
    {
        DevCardListInfo newCards = convertDevCards(developmentCardHand.getNewDevelopmentCards());
        DevCardListInfo oldCards = convertDevCards(developmentCardHand.getOldDevelopmentCards());
        return new DevCardHandInfo(newCards, oldCards);
    }

    public static DevCardListInfo convertDevCards(Collection<DevCardType> developments)
    {
        int monopoly = 0;
        int monument = 0;
        int roadBuilding = 0;
        int soldier = 0;
        int yearOfPlenty = 0;
        for (DevCardType type : developments)
        {
            switch (type)
            {
                case MONOPOLY:
                    monopoly++;
                    break;
                case MONUMENT:
                    monument++;
                    break;
                case ROAD_BUILD:
                    roadBuilding++;
                    break;
                case SOLDIER:
                    soldier++;
                    break;
                case YEAR_OF_PLENTY:
                    yearOfPlenty++;
                    break;
            }
        }
        return new DevCardListInfo(monopoly,monument,roadBuilding,soldier,yearOfPlenty);
    }
}
