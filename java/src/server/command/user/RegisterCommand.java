package server.command.user;

import server.command.CommandObject;
import server.model.ServerModel;
import server.model.player.ServerUser;

public class RegisterCommand implements CommandObject
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8836761035303330976L;
	private String username;
	private String password;
	private ServerModel serverM;
	private ServerUser player;
	/**
	 * A fully initializing constructor for this class
	 * @param username
	 * @param password
	 */
	public RegisterCommand(String username, String password, ServerModel server)
	{
		super();
		this.username = username;
		this.password = password;
		serverM = server;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the Register function
	 */
	@Override
	public void executeCommand()
	{
		player = serverM.createUser(username, password);
	}
	
	public ServerUser getPlayer()
	{
		return player;
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverM = serverModel;
	}
}
