package server.command;

import server.exceptions.GameDoesNotExistException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.game.Game;


public abstract class VersionCommand {

	public void changeGameVersion(int gameID, ServerModel model)throws GameDoesNotExistException
	{	
		GameModel modelToBeChanged = model.getGame(gameID);
		Game game = modelToBeChanged.getGame();
		game.incrementGameVersion();
	}
}
