package server.command;

import java.io.IOException;
import java.io.Serializable;

import server.exceptions.BankOutOfResourceException;
import server.exceptions.CatanServerException;
import server.model.ServerModel;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;

/**
 * @author Lawrence
 * @author Eric
 */
public interface CommandObject extends Serializable
{	
	/** 
	 * Executes needed changes to the model determined by the facade for any of its implementations.
	 * The implementations match the functions of the facade.
	 * @throws InvalidObjectTypeException 
	 * @throws InvalidRobberLocationException 
	 * @throws InvalidLocationException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws BankOutOfResourceException 
	 */
	public void executeCommand() throws CatanServerException, InvalidRobberLocationException, InvalidObjectTypeException, InvalidLocationException, IOException, ClassNotFoundException, BankOutOfResourceException;
	
	public void setServerModel(ServerModel serverModel);
}
