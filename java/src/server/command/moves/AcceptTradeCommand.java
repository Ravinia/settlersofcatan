/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.ServerModel;
import server.model.game.PlayableGameImp;
import server.model.game.resources.ResourceHand;
import server.model.game.trade.TradeOffer;
import server.model.player.Player;
import shared.definitions.ResourceType;

/**
 * @author Eric
 *
 */
public class AcceptTradeCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3850887651319579927L;
	private int playerIndex;
	private boolean willAccept;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param willAccept
	 */
	public AcceptTradeCommand(int playerIndex, boolean willAccept, int gameID, ServerModel serverModel) {
		super();
		this.playerIndex = playerIndex;
		this.willAccept = willAccept;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the AcceptTrade function
	 * @throws GameDoesNotExistException 
	 * @throws NoCurrentTradesException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, NoCurrentTradesException, InvalidIndexException, PlayerNotInGameException, UninitializedDataException
	{
		PlayableGameImp game = (PlayableGameImp) serverModel.getGame(gameID).getGame();
		Player accepter = serverModel.getGame(gameID).getPlayerByIndex(playerIndex);
		if(willAccept)
		{
			TradeOffer tradeOffer = game.getTradeOffer();
			Player proposer = serverModel.getGame(gameID).getPlayerByIndex(tradeOffer.getSenderIndex());
			ResourceHand receiverHand = game.getResourceHandOfPlayer(accepter);
			ResourceHand senderHand = game.getResourceHandOfPlayer(proposer);
			this.ResourceMods(tradeOffer.getOffer(), receiverHand, senderHand);
			game.addLogMessage(accepter.getName(), accepter.getName() + " accepted a trade.");
		}
		else
			game.addLogMessage(accepter.getName(), accepter.getName() + " declined a trade.");
		game.setTradeOffer(null);
		this.changeGameVersion(gameID, serverModel);	
	}
	private void ResourceMods(ResourceHand o, ResourceHand rH, ResourceHand sH)
	{
		if(o.getBrick() != 0)
		{
			System.out.println("brick: " + o.getBrick());
			sH.incrementResourceByType(ResourceType.BRICK, -o.getBrick());
			rH.incrementResourceByType(ResourceType.BRICK, o.getBrick());
		}
		if(o.getOre() != 0)
		{
			System.out.println("ore: " + o.getOre());
			sH.incrementResourceByType(ResourceType.ORE, -o.getOre());
			rH.incrementResourceByType(ResourceType.ORE, o.getOre());
		}
		if(o.getSheep() != 0)
		{
			System.out.println("sheep: " + o.getSheep());
			sH.incrementResourceByType(ResourceType.SHEEP, -o.getSheep());
			rH.incrementResourceByType(ResourceType.SHEEP, o.getSheep());
		}
		if(o.getWheat() != 0)
		{
			System.out.println("wheat: " + o.getWheat());
			sH.incrementResourceByType(ResourceType.WHEAT, -o.getWheat());
			rH.incrementResourceByType(ResourceType.WHEAT, o.getWheat());
		}
		if(o.getWood() != 0)
		{
			System.out.println("wood: " + o.getWood());
			sH.incrementResourceByType(ResourceType.WOOD, -o.getWood());
			rH.incrementResourceByType(ResourceType.WOOD, o.getWood());
		}
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
