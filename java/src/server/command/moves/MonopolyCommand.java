/**
 * 
 */
package server.command.moves;

import java.util.List;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.game.PlayableGame;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.definitions.DevCardType;
import shared.definitions.ResourceType;

/**
 * @author Eric
 *
 */
public class MonopolyCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -419954551522404726L;
	private int playerIndex;
	private ResourceType resource;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param resource
	 */
	public MonopolyCommand(int playerIndex, ResourceType resource, int gameID, ServerModel serverModel)
	{
		super();
		this.playerIndex = playerIndex;
		this.resource = resource;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the Monopoly function
	 * @throws GameDoesNotExistException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, PlayerNotInGameException, UninitializedDataException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		List<Player> players = gameModel.getPlayers();
		PlayableGame game = (PlayableGame) gameModel.getGame();
		if(! game.hasCurrentPlayerPlayedDevCardThisTurn())
		{
			int resToSteal = 0;
			for(int i = 0; i < players.size(); i++)
			{
				if(i != playerIndex)
				{
					ResourceHand rh = game.getResourceHandOfPlayer(players.get(i));
					resToSteal += rh.getResourceCount(resource);
					rh.setResourceByType(resource, 0);
				}
			}
			game.getResourceHandOfPlayer(players.get(playerIndex)).incrementResourceByType(resource, resToSteal);
			game.getDevelopmentCardHandOfPlayer(players.get(playerIndex)).removeCard(DevCardType.MONOPOLY);
			game.setCurrentPlayerHasPlayedDevCardThisTurn(true);
			game.addLogMessage(players.get(playerIndex).getName(), players.get(playerIndex).getName() + 
					" played a Monopoly card and stole " + ResourceType.getResourceName(resource) + ".");
			this.changeGameVersion(gameID, serverModel);
		}
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
