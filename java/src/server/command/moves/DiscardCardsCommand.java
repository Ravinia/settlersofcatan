/**
 * 
 */
package server.command.moves;

import java.util.List;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.ServerModel;
import server.model.game.PlayableGame;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.definitions.Phase;
import shared.definitions.ResourceType;

/**
 * @author Eric
 *
 */
public class DiscardCardsCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7856885448453001236L;
	private int playerIndex;
	private List<ResourceType> discardedCards;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param discardedCards
	 */
	public DiscardCardsCommand(int playerIndex,
			List<ResourceType> discardedCards, int gameID, ServerModel serverModel) {
		super();
		this.playerIndex = playerIndex;
		this.discardedCards = discardedCards;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the DiscardCards function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, UninitializedDataException 
	{
		PlayableGame playableGame = (PlayableGame) serverModel.getGame(gameID).getGame();
		Player player = serverModel.getGame(gameID).getPlayerByIndex(playerIndex);
		ResourceHand recHand = playableGame.getResourceHandOfPlayer(player);
		recHand.DiscardResources(discardedCards);
		playableGame.addPlayerWhoHasDiscarded(player);
		if(playableGame.removePlayerWhoDiscarded(player) == 0)
			playableGame.getTurnOrder().setCurrentPhase(Phase.ROBBING);
		playableGame.addLogMessage(player.getName(), player.getName() + " discarded.");
		this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
