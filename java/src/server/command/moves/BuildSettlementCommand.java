/**
 * 
 */
package server.command.moves;

import java.util.List;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlaceOccupiedException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.board.ServerBoard;
import server.model.board.immutable.Hex;
import server.model.board.placeable.Settlement;
import server.model.board.placeable.SettlementPiece;
import server.model.game.PlayableGame;
import server.model.game.resources.BuildPool;
import server.model.game.resources.ResourceHand;
import server.model.game.state.TurnOrder;
import server.model.player.Player;
import shared.definitions.Phase;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.VertexLocation;

/**
 * @author Eric
 *
 */
public class BuildSettlementCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2830323152235530189L;
	private int playerIndex;
	private VertexLocation location;
	private boolean free;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param location
	 * @param free
	 */
	public BuildSettlementCommand(int playerIndex, VertexLocation location,
			boolean free, int gameID, ServerModel serverModel)
	{
		super();
		this.playerIndex = playerIndex;
		this.location = location;
		this.free = free;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the BuildSettlement function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws InvalidObjectTypeException 
	 * @throws InvalidRobberLocationException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 * @throws PlaceOccupiedException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidIndexException, InvalidRobberLocationException, InvalidObjectTypeException, PlayerNotInGameException, UninitializedDataException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		ServerBoard board = (ServerBoard) gameModel.getBoard();
		Player player = gameModel.getPlayerByIndex(playerIndex);
		Settlement settlement = new SettlementPiece(location, player);
		PlayableGame game = (PlayableGame)gameModel.getGame();
		
		board.updateBoardObjects(settlement);
		if(!free)
		{
			ResourceHand recHand = game.getResourceHandOfPlayer(player);
			int brick = recHand.getBrick();
			int wood = recHand.getWood();
			int sheep = recHand.getSheep();
			int wheat = recHand.getWheat();
			recHand.setBrick(brick -1);
			recHand.setWood(wood -1);
			recHand.setSheep(sheep -1);
			recHand.setWheat(wheat -1);
		}
		BuildPool buildPool = game.getBuildPoolOfPlayer(player);
		int settlements = buildPool.getSettlements();
		buildPool.setSettlements(settlements -1);
		TurnOrder turnOrder = game.getTurnOrder();
		//this is for turn control in our own client. You must have this uncommented if you want to run our client.
		//However, this will subtly break the demo-client.
//		if(turnOrder.getCurrentPhase() == Phase.ROUND1 || turnOrder.getCurrentPhase() == Phase.ROUND2 )
//		{
//			turnOrder.advancePhase();
//			turnOrder.advancePlayer();
//			
//		}
		if(turnOrder.getCurrentPhase() == Phase.ROUND1 || turnOrder.getCurrentPhase() == Phase.ROUND2 )
		{
			if(turnOrder.getCurrentPhase() == Phase.ROUND2)
			{
				ResourceHand resHand = game.getResourceHandOfPlayer(player);
				List<Hex> hexes;
				try {
					hexes = board.getHexesAdjacentTo(location.getNormalizedLocation());
					for(Hex hex : hexes)
					{
						ResourceType type = hex.getResourceType();
						if(type != null)
							resHand.incrementResourceByType(type, 1);
					}
				} catch (InvalidLocationException e) {
					e.printStackTrace();
				}
			}
			turnOrder.advancePhase();
			if((turnOrder.getIndexOfPlayerWhoseTurnItIs() != 3) && turnOrder.getCurrentPhase() == Phase.ROUND2)
			{
				turnOrder.advancePlayer();
			}
			
		}
		game.addLogMessage(player.getName(), player.getName() + " built a settlement.");
		game.setVictoryPoints(player, gameModel.checkWinner(player));
		this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
