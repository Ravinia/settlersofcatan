/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.game.PlayableGame;
import server.model.player.Player;
import shared.definitions.DevCardType;

/**
 * @author Eric
 *
 */
public class MonumentCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3621947477109015823L;
	private int playerIndex;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 */
	public MonumentCommand(int playerIndex, int gameID, ServerModel serverModel)
	{
		super();
		this.playerIndex = playerIndex;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the Monument function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, UninitializedDataException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		PlayableGame game = (PlayableGame)gameModel.getGame();
		Player player = gameModel.getPlayerByIndex(playerIndex);
		game.playMonument(player);
		game.getDevelopmentCardHandOfPlayer(player).removeCard(DevCardType.MONUMENT);
		game.addLogMessage(player.getName(), player.getName() + " played a monument.");
		game.setVictoryPoints(player, gameModel.checkWinner(player));
		this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
