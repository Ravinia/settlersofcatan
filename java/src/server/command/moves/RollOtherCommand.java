package server.command.moves;

import java.util.List;
import java.util.Map;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.game.PlayableGame;
import server.model.player.Player;
import shared.definitions.Phase;
import shared.definitions.ResourceType;

public class RollOtherCommand extends VersionCommand implements CommandObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5804091786908543862L;
	private List<Map<ResourceType, Integer>> resources;
	private int rollValue, playerIndex, gameID;
	private ServerModel serverM;
	/**
	 * A fully initializing constructor for this class
	 * @param rollerIndex
	 * @param roll
	 */
	public RollOtherCommand(List<Map<ResourceType, Integer> > resources, int rollValue, int playerIndex, int gameID, ServerModel server)
	{
		super();
		this.resources = resources;
		this.rollValue = rollValue;
		this.playerIndex = playerIndex;
		this.gameID = gameID;
		serverM = server;
	}
	@Override
	public void executeCommand() throws GameDoesNotExistException, PlayerNotInGameException, InvalidIndexException, UninitializedDataException
	{
		GameModel gameModel = serverM.getGame(gameID);
		List<Player> players = gameModel.getPlayers();
		PlayableGame game = (PlayableGame) gameModel.getGame();
		for(int i = 0; i<resources.size(); i++)
			game.incrementResources(players.get(i), resources.get(i));
		
		Player player = gameModel.getPlayerByIndex(playerIndex);
		game.addLogMessage(player.getName(), player.getName() + " rolled a " + rollValue + ".");
		//Now change the game phase
		game.getTurnOrder().setCurrentPhase(Phase.PLAYING);
		this.changeGameVersion(gameID, serverM);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverM = serverModel;
	}
}
