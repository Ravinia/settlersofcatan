/**
 * 
 */
package server.command.moves;

import java.util.Map;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.game.PlayableGameImp;
import server.model.game.resources.ResourceHand;
import server.model.game.resources.ResourceHandImp;
import server.model.game.trade.TradeOffer;
import server.model.game.trade.TradeOfferImp;
import server.model.player.Player;
import shared.definitions.ResourceType;

/**
 * @author Eric
 *
 */
public class OfferTradeCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2775896429093895639L;
	private int senderIndex;
	private Map<ResourceType, Integer> offer;
	private int receiverIndex;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param senderIndex
	 * @param offer
	 * @param receiverIndex
	 */
	public OfferTradeCommand(int senderIndex, Map<ResourceType, Integer> offer,
			int receiverIndex, int gameID, ServerModel serverModel)
	{
		super();
		this.senderIndex = senderIndex;
		this.offer = offer;
		this.receiverIndex = receiverIndex;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the OfferTrade function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		PlayableGameImp game = (PlayableGameImp) gameModel.getGame();
		ResourceHand offer = new ResourceHandImp(this.offer);
		TradeOffer tradeOffer = new TradeOfferImp(this.senderIndex, this.receiverIndex, offer);
		game.setTradeOffer(tradeOffer);
		Player player = gameModel.getPlayerByIndex(senderIndex);
		game.addLogMessage(player.getName(), player.getName() + " offered a trade to " + gameModel.getPlayerByIndex(receiverIndex).getName() + ".");
		this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
