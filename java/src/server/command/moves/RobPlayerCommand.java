/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.board.placeable.RobberPiece;
import server.model.game.PlayableGame;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.definitions.Phase;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.HexLocation;

/**
 * @author Eric
 *
 */
public class RobPlayerCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6902709402463595615L;
	private int robberIndex;
	private int victimIndex;
	private HexLocation robberLocation;
	private ResourceType typeStolen;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param robberIndex
	 * @param victimIndex
	 * @param location
	 */
	public RobPlayerCommand(int robberIndex, int victimIndex,
			HexLocation robberLocation, ResourceType typeStolen, int gameID, ServerModel serverModel)
	{
		super();
		this.robberIndex = robberIndex;
		this.victimIndex = victimIndex;
		this.robberLocation = robberLocation;
		this.typeStolen = typeStolen;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}

	/**
	 * Executes needed changes to the model determined by the facade for the RobPlayer function
	 * This method should decrement the victim's resources of type @param typeStolen by one, 
	 * increment the robbing player's resources of that type by one, and move the robber to the
	 * new location.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidObjectTypeException 
	 * @throws InvalidRobberLocationException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, PlayerNotInGameException, InvalidIndexException, InvalidRobberLocationException, InvalidObjectTypeException, UninitializedDataException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		PlayableGame game = (PlayableGame) gameModel.getGame();
		game.getTurnOrder().setCurrentPhase(Phase.PLAYING);
		gameModel.getBoard().updateBoardObjects(new RobberPiece(robberLocation));
		Player robber = gameModel.getPlayerByIndex(robberIndex);
		if(victimIndex == -1)
		{
			game.addLogMessage(robber.getName(), robber.getName() + " robbed no one");
			this.changeGameVersion(gameID, serverModel);
			return;
		}
		ResourceHand victimRes = game.getResourceHandOfPlayer(gameModel.getPlayerByIndex(victimIndex));
		ResourceHand robberRes = game.getResourceHandOfPlayer(gameModel.getPlayerByIndex(robberIndex));
		victimRes.incrementResourceByType(typeStolen, -1);
		robberRes.incrementResourceByType(typeStolen, 1);
		
		
		game.addLogMessage(robber.getName(), robber.getName() + " robbed from " + 
				gameModel.getPlayerByIndex(victimIndex).getName() + ".");
		this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
