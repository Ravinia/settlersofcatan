/**
 * 
 */
package server.command.moves;

import java.util.List;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.game.PlayableGame;
import server.model.player.Player;
import shared.definitions.Phase;

/**
 * @author Eric
 *
 */
public class Roll7Command extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1631935319585955478L;
	private List<Player> playersWhoMustDiscard;
	private int playerIndex, gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param rollerIndex
	 * @param roll
	 */
	public Roll7Command(List<Player> playersWhoDiscard, int playerIndex, int gameID, ServerModel serverModel)
	{
		super();
		playersWhoMustDiscard = playersWhoDiscard;
		this.playerIndex = playerIndex;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}

	/**
	 * Executes needed changes to the model determined by the facade for the RollNumber function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidIndexException, UninitializedDataException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		PlayableGame game = (PlayableGame) gameModel.getGame();
		if(playersWhoMustDiscard.size() > 0)
		{
			game.setPlayersWhoMustDiscard(playersWhoMustDiscard);
			game.getTurnOrder().setCurrentPhase(Phase.DISCARDING);
		}
		else
		{
			game.getTurnOrder().setCurrentPhase(Phase.ROBBING);
		}
		
		Player player = gameModel.getPlayerByIndex(playerIndex);
		game.addLogMessage(player.getName(), player.getName() + " rolled a 7!");
		this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
