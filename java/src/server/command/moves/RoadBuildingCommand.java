/**
 * 
 */
package server.command.moves;

import java.util.ArrayList;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.board.Board;
import server.model.board.placeable.Placeable;
import server.model.board.placeable.RoadPiece;
import server.model.game.PlayableGame;
import server.model.game.resources.BuildPool;
import server.model.player.Player;
import shared.definitions.DevCardType;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.EdgeLocation;

/**
 * @author Eric
 *
 */
public class RoadBuildingCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6528014767895453860L;
	private int playerIndex;
	private EdgeLocation location1;
	private EdgeLocation location2;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param location1
	 * @param location2
	 */
	public RoadBuildingCommand(int playerIndex, EdgeLocation location1,
			EdgeLocation location2, int gameID, ServerModel serverModel)
	{
		super();
		this.playerIndex = playerIndex;
		this.location1 = location1;
		this.location2 = location2;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/** 
	 * Executes needed changes to the model determined by the facade for the RoadBuilding function
	 * @throws GameDoesNotExistException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidIndexException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, PlayerNotInGameException, InvalidIndexException, UninitializedDataException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		PlayableGame game = (PlayableGame)serverModel.getGame(gameID).getGame();
		if(! game.hasCurrentPlayerPlayedDevCardThisTurn())
		{
			Board board = gameModel.getBoard();
			Player player = gameModel.getPlayerByIndex(playerIndex);
			ArrayList<Placeable> buildList = new ArrayList<Placeable>();
			buildList.add(new RoadPiece(location1, player));
			buildList.add(new RoadPiece(location2, player));
			try
			{
				board.updateBoardObjects(buildList);
			}
			catch (InvalidRobberLocationException | InvalidObjectTypeException ignore) { }
		
			game.getDevelopmentCardHandOfPlayer(player).removeCard(DevCardType.ROAD_BUILD);
			BuildPool buildPool = game.getBuildPoolOfPlayer(player);
			int roads = buildPool.getRoads();
			buildPool.setRoads(roads-2);
			gameModel.roadAdded();
			game.addLogMessage(player.getName(), player.getName() + " played a Road Building card.");
			game.setVictoryPoints(player, gameModel.checkWinner(player));
			game.setCurrentPlayerHasPlayedDevCardThisTurn(true);
			this.changeGameVersion(gameID, serverModel);
		}
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}	
