/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.ServerModel;
import server.model.game.PlayableGame;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.definitions.DevCardType;
import shared.definitions.ResourceType;

/**
 * @author Eric
 *
 */
public class YearOfPlentyCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2272426745439021164L;
	private int playerIndex;
	private ResourceType resource1;
	private ResourceType resource2;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param resource1
	 * @param resource2
	 */
	public YearOfPlentyCommand(int playerIndex, ResourceType resource1,
			ResourceType resource2, int gameID, ServerModel serverModel) {
		super();
		this.playerIndex = playerIndex;
		this.resource1 = resource1;
		this.resource2 = resource2;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the YearOfPlenty function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, UninitializedDataException
	{
		PlayableGame game = (PlayableGame)serverModel.getGame(gameID).getGame();
		if(! game.hasCurrentPlayerPlayedDevCardThisTurn())
		{
			Player player = serverModel.getGame(gameID).getPlayerByIndex(playerIndex);
			game.getDevelopmentCardHandOfPlayer(player).removeCard(DevCardType.YEAR_OF_PLENTY);
			ResourceHand resHand = game.getResourceHandOfPlayer(player);
		
			resHand.incrementResourceByType(resource1, 1);
			resHand.incrementResourceByType(resource2, 1);
		
			game.addLogMessage(player.getName(), player.getName() + " played a Year of Plenty card.");
			game.setCurrentPlayerHasPlayedDevCardThisTurn(true);
			this.changeGameVersion(gameID, serverModel);
		}
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
