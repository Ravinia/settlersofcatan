/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.BankOutOfResourceException;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.ServerModel;
import server.model.game.PlayableGameImp;
import server.model.game.bank.BankImp;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.definitions.ResourceType;

/**
 * @author Eric
 *
 */
public class MaritimeTradeCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7005527414459091207L;
	private int playerIndex;
	private int ratio;
	private ResourceType inputType;
	private ResourceType outputType;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param ratio
	 * @param inputType
	 * @param outputType
	 */
	public MaritimeTradeCommand(int playerIndex, int ratio,
			ResourceType inputType, ResourceType outputType, int gameID, ServerModel serverModel) 
	{
		super();
		this.playerIndex = playerIndex;
		this.ratio = ratio;
		this.inputType = inputType;
		this.outputType = outputType;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the MaritimeTrade function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws BankOutOfResourceException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, BankOutOfResourceException, UninitializedDataException
	{
		Player player = serverModel.getGame(gameID).getPlayerByIndex(playerIndex);
		PlayableGameImp game = (PlayableGameImp) serverModel.getGame(gameID).getGame();
		ResourceHand recHand = game.getResourceHandOfPlayer(player);
		BankImp bank = (BankImp)game.getBank();
		if(bank.getResourceByType(outputType) > 0)
		{
			recHand.incrementResourceByType(inputType, -ratio);
			bank.incrementResourceByType(inputType, ratio);
			recHand.incrementResourceByType(outputType, 1);
			bank.incrementResourceByType(outputType, -1);
			game.addLogMessage(player.getName(), player.getName() + " traded with the bank.");
		}
		else
		{
			throw new BankOutOfResourceException("bank doesn't have any" + outputType.toString() + " to give you");
		}
		this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
