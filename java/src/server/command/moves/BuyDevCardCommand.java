/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.ServerModel;
import server.model.game.PlayableGame;
import server.model.game.bank.Deck;
import server.model.game.developmentcard.DevelopmentCardHand;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.definitions.DevCardType;

/**
 * @author Eric
 *
 */
public class BuyDevCardCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 635891367647144432L;
	private int playerIndex;
	DevCardType cardToBuy;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 */
	public BuyDevCardCommand(int playerIndex, DevCardType cardType, int gameID, ServerModel serverModel)
	{
		super();
		this.playerIndex = playerIndex;
		cardToBuy = cardType;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the BuyDevCard function
	 * Increments the players dev card hand with the given card and decrements it from the deck.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, UninitializedDataException
	{
		 PlayableGame game = (PlayableGame)serverModel.getGame(gameID).getGame(); //this doesn't check availability of the card or if the player has the resources
		 Player player = serverModel.getGame(gameID).getPlayerByIndex(playerIndex); //the function simply manipulates the end values assuming they work
		 Deck deck = game.getDeck();
		 DevelopmentCardHand devCardHand = game.getDevelopmentCardHandOfPlayer(player);
		 ResourceHand recHand = game.getResourceHandOfPlayer(player);
		 deck.buyCard(cardToBuy);
		 if(cardToBuy == DevCardType.MONUMENT)
			 devCardHand.addOldCard(cardToBuy);
		 else
			 devCardHand.addNewCard(cardToBuy);
		 int ore = recHand.getOre();
		 int sheep = recHand.getSheep();
		 int wheat = recHand.getWheat();
		 recHand.setOre(ore -1);
		 recHand.setSheep(sheep -1);
		 recHand.setWheat(wheat -1);
		 game.addLogMessage(player.getName(), player.getName() + " bought a development card.");
		 this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
