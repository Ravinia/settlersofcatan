/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.game.PlayableGame;

/**
 * @author Eric
 *
 */
public class SendChatCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4110129827943682150L;
	private int senderIndex;
	private String content;
	private int gameID;
	private ServerModel serverM;
	/**
	 * A fully initializing constructor for this class
	 * @param senderIndex
	 * @param content
	 */
	public SendChatCommand(int senderIndex, String content, int ID, ServerModel server)
	{
		super();
		this.senderIndex = senderIndex;
		this.content = content;
		gameID = ID;
		serverM = server;
	}

	/**
	 * Executes needed changes to the model determined by the facade for the SendChat function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException
	{
		GameModel gameModel = serverM.getGame(gameID);
		if(!(gameModel.getGame() instanceof PlayableGame))
			return;
		((PlayableGame) gameModel.getGame()).addChatMessage(gameModel.getPlayerByIndex(senderIndex).getName(), content);
		
		this.changeGameVersion(gameID, serverM);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverM = serverModel;
	}
}
