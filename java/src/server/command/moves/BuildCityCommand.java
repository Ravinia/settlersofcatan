/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlaceOccupiedException;
import server.exceptions.PlaceablesNotInitializedException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.board.ServerBoard;
import server.model.board.placeable.CityPiece;
import server.model.game.PlayableGame;
import server.model.game.resources.BuildPool;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.locations.VertexLocation;

/**
 * @author Eric
 *
 */
public class BuildCityCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8363514054541505029L;
	private int playerIndex;
	private VertexLocation location;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param location
	 */
	public BuildCityCommand(int playerIndex, VertexLocation location, int gameID, ServerModel serverModel)
	{
		super();
		this.playerIndex = playerIndex;
		this.location = location;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the BuildCity function.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlaceablesNotInitializedException 
	 * @throws InvalidLocationException 
	 * @throws InvalidObjectTypeException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 * @throws PlaceOccupiedException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidIndexException, PlaceablesNotInitializedException, InvalidLocationException, InvalidObjectTypeException, PlayerNotInGameException, UninitializedDataException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		ServerBoard board = (ServerBoard) gameModel.getBoard();
		Player player = gameModel.getPlayerByIndex(playerIndex);
		CityPiece city = new CityPiece(location, player);
		PlayableGame game = (PlayableGame)gameModel.getGame();
		
		board.updateBoardObjects(city);
		ResourceHand recHand = game.getResourceHandOfPlayer(player);
		int ore = recHand.getOre();
		int wheat = recHand.getWheat();
		recHand.setOre(ore -3);
		recHand.setWheat(wheat -2);
		
		BuildPool buildPool = game.getBuildPoolOfPlayer(player);
		int cities = buildPool.getCities();
		int settlements = buildPool.getSettlements();
		buildPool.setCities(cities -1);
		buildPool.setSettlements(settlements +1);
		game.addLogMessage(player.getName(), player.getName() + " built a city.");
		game.setVictoryPoints(player, gameModel.checkWinner(player));
		this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
