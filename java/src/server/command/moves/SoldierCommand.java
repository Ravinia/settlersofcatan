/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.board.placeable.RobberPiece;
import server.model.game.PlayableGame;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.definitions.DevCardType;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.HexLocation;

/**
 * @author Eric
 *
 */
public class SoldierCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6908052456027482086L;
	private int playerIndex;
	private int victimIndex;
	private HexLocation robberLocation;
	private ResourceType typeStolen;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param victimIndex
	 * @param robberLocation
	 */
	public SoldierCommand(int playerIndex, int victimIndex,	HexLocation robberLocation, 
			ResourceType typeStolen, int gameID, ServerModel serverModel)
	{
		super();
		this.playerIndex = playerIndex;
		this.victimIndex = victimIndex;
		this.robberLocation = robberLocation;
		this.typeStolen = typeStolen;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the Soldier function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidObjectTypeException 
	 * @throws InvalidRobberLocationException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidRobberLocationException, PlayerNotInGameException, InvalidIndexException, UninitializedDataException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		PlayableGame game = (PlayableGame) gameModel.getGame();
		if(!game.hasCurrentPlayerPlayedDevCardThisTurn())
		{
			Player player = gameModel.getPlayerByIndex(playerIndex);
			game.getDevelopmentCardHandOfPlayer(player).removeCard(DevCardType.SOLDIER);
			try {
				gameModel.getBoard().updateBoardObjects(new RobberPiece(robberLocation));
			} catch (InvalidObjectTypeException ignore) { }
		
			ResourceHand victimRes = game.getResourceHandOfPlayer(gameModel.getPlayerByIndex(victimIndex));
			ResourceHand robberRes = game.getResourceHandOfPlayer(player);
			victimRes.incrementResourceByType(typeStolen, -1);
			robberRes.incrementResourceByType(typeStolen, 1);
		
			game.playArmy(player);
			game.addLogMessage(player.getName(), player.getName() + " played a Soldier card and robbed " 
					+ gameModel.getPlayerByIndex(victimIndex).getName() + ".");
			gameModel.armyAdded();
			game.setVictoryPoints(player, gameModel.checkWinner(player));
			game.setCurrentPlayerHasPlayedDevCardThisTurn(true);
			this.changeGameVersion(gameID, serverModel);
		}
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
