/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlaceOccupiedException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.board.ServerBoard;
import server.model.board.placeable.Road;
import server.model.board.placeable.RoadPiece;
import server.model.game.PlayableGame;
import server.model.game.resources.BuildPool;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.EdgeLocation;

/**
 * @author Eric
 *
 */
public class BuildRoadCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -470190686627521456L;
	private int playerIndex;
	private EdgeLocation roadLocation;
	private boolean free;
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 * @param roadLocation
	 * @param free
	 */
	public BuildRoadCommand(int playerIndex, EdgeLocation roadLocation,
			boolean free, int gameID, ServerModel serverModel)
	{
		super();
		this.playerIndex = playerIndex;
		this.roadLocation = roadLocation;
		this.free = free;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the BuildRoad function
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws InvalidObjectTypeException 
	 * @throws InvalidRobberLocationException 
	 * @throws PlaceOccupiedException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, InvalidIndexException, InvalidRobberLocationException, InvalidObjectTypeException, PlayerNotInGameException, UninitializedDataException
	{
		GameModel gameModel = serverModel.getGame(gameID);
		ServerBoard board = (ServerBoard) gameModel.getBoard();
		Player player = gameModel.getPlayerByIndex(playerIndex);
		Road road = new RoadPiece(roadLocation, player);
		PlayableGame game = (PlayableGame)gameModel.getGame();
		board.updateBoardObjects(road);
		if(!free)
		{
			ResourceHand recHand = game.getResourceHandOfPlayer(player);
			int brick = recHand.getBrick();
			int wood = recHand.getWood();
			recHand.setBrick(brick -1);
			recHand.setWood(wood -1);
		}
		BuildPool buildPool = game.getBuildPoolOfPlayer(player);
		int roads = buildPool.getRoads();
		buildPool.setRoads(roads-1);
		game.addLogMessage(player.getName(), player.getName() + " built a road.");
		gameModel.roadAdded();
		game.setVictoryPoints(player, gameModel.checkWinner(player));
		this.changeGameVersion(gameID, serverModel);
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
