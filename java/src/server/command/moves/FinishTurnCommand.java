/**
 * 
 */
package server.command.moves;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.ServerModel;
import server.model.game.PlayableGameImp;
import server.model.game.developmentcard.DevelopmentCardHand;
import server.model.game.state.TurnOrder;
import server.model.player.Player;

/**
 * @author Eric
 *
 */
public class FinishTurnCommand extends VersionCommand implements CommandObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3703670678451309891L;
	private int playerIndex; // we may not actually need this data field.
	private int gameID;
	private ServerModel serverModel;
	/**
	 * A fully initializing constructor for this class
	 * @param playerIndex
	 */
	public FinishTurnCommand(int playerIndex, int gameID, ServerModel serverModel) {
		super();
		this.playerIndex = playerIndex;
		this.gameID = gameID;
		this.serverModel = serverModel;
	}
	/**
	 * Executes needed changes to the model determined by the facade for the FinishTurn function.
	 * @throws UninitializedDataException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException, PlayerNotInGameException 
	{
		PlayableGameImp game = (PlayableGameImp) serverModel.getGame(gameID).getGame();
		TurnOrder turnOrder = game.getTurnOrder();
		if(turnOrder.getIndexOfPlayerWhoseTurnItIs() == playerIndex)
		{
			turnOrder.advancePhase(); //advances through the normal phases of the game
			turnOrder.advancePlayer(); // advances the current player
			
			Player player = serverModel.getGame(gameID).getPlayerByIndex(playerIndex);
			DevelopmentCardHand devCardHand = game.getDevelopmentCardHandOfPlayer(player);
			devCardHand.ageCards();
			game.setCurrentPlayerHasPlayedDevCardThisTurn(false);
			game.clearPlayersWhoDiscarded();
			game.addLogMessage(player.getName(), player.getName() + "'s turn ended.");
			this.changeGameVersion(gameID, serverModel);
		}
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
