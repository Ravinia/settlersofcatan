package server.command.games;

import server.command.CommandObject;
import server.exceptions.CatanServerException;
import server.model.ServerModel;

public class CreateGameCommand implements CommandObject
{
	//Note: Currently the GamesFacade isn't using this command object
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8431657944286060132L;
	private boolean randomTiles;
	private boolean randomNumbers;
	private boolean randomPorts;
	private String name;
	private ServerModel serverModel;
	/** 	 
	 * A fully initializing constructor for this class
	 * @param randomTiles
	 * @param randomNumbers
	 * @param randomPorts
	 * @param name
	 */
	public CreateGameCommand(boolean randomTiles, boolean randomNumbers,
			boolean randomPorts, String name, ServerModel serverModel) {
		super();
		this.randomTiles = randomTiles;
		this.randomNumbers = randomNumbers;
		this.randomPorts = randomPorts;
		this.name = name;
		this.serverModel = serverModel;
	}

	/**
	 * Executes needed changes to the model determined by the facade for the CreateGame function.
	 */
	@Override
	public void executeCommand() throws CatanServerException {
		try {
			serverModel.createGame(randomTiles, randomNumbers, randomPorts, name);
		} catch (CatanServerException e) {
			throw new CatanServerException("CreateGameCommand", e);
		}
	}

	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}

}
