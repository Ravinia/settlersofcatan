package server.command.games;

import server.command.CommandObject;
import server.command.VersionCommand;
import server.exceptions.GameDoesNotExistException;
import server.model.ServerModel;
import server.model.game.Game;
import server.model.game.PlayableGame;
import server.model.game.PlayableGameImp;
import server.model.game.PregameGame;
import server.model.player.Player;
import shared.definitions.CatanColor;

public class JoinGameCommand extends VersionCommand implements CommandObject
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7032900480168591472L;
	private int gameID, playerID;
	private CatanColor color;
	private String name;
	private ServerModel serverM;
	boolean successful = false;
	
	/**
	 * A fully initializing constructor for this class
	 * @param gameID
	 * @param playerID
	 * @param color
	 */
	public JoinGameCommand(int gameID, int playerID, String name, CatanColor color, ServerModel server)
	{
		super();
		this.gameID = gameID;
		this.playerID = playerID;
		this.name = name;
		this.color = color;
		serverM = server;
	}

	/**
	 * Executes needed changes to the model determined by the facade for the JoinGame function.
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException
    {
		Game g = serverM.getGame(gameID).getGame();
		
		if(g == null)
		{
			return;
		}
		
		// now a player will join or rejoin a game
		successful = true;
		this.changeGameVersion(gameID, serverM);
		
		if (g instanceof PlayableGame)
		{
			// Rejoining a playable game: must reset the player color
			PlayableGameImp playableGame = (PlayableGameImp)g;
			
			Player player = playableGame.getPlayerWithID(playerID);
			playableGame.getPlayerColors().remove(player);
			playableGame.getPlayerColors().put(player, color);
			
		}
		else // is pregamegame
		{
			PregameGame preg = (PregameGame)g;
			if(preg.containsPlayerWithID(playerID))
				preg.removePlayer(name, playerID);
			preg.addPlayer(name, playerID, color);
			
			
			if(preg.getNumberOfPlayersInGame() == 4)
			{
				serverM.startGame(gameID);
			}
		}
	}

	public boolean wasSuccessful()
	{
		return successful;
	}
	
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverM = serverModel;
	}
}
