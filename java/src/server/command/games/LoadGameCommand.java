package server.command.games;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

import server.command.CommandObject;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.ServerModelImp;
import server.model.game.PlayableGame;
import server.model.game.PlayableGameImp;
import server.model.game.PregameGameImp;

public class LoadGameCommand implements CommandObject
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1637969642170557228L;
	private String name;
	private ServerModel serverModel;
	private GameModel gameModel;
	/**
	 * A fully initializing constructor for this class
	 * @param name
	 */
	public LoadGameCommand(String name, ServerModel serverModel) 
	{
		super();
		this.name = name;
		this.serverModel = serverModel;
	}

	/**
	 * Executes needed changes to the model determined by the facade for the LoadGame function.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	@Override
	public void executeCommand() throws IOException, ClassNotFoundException
	{
		InputStream file = new FileInputStream("saves/"+ name +".ser");
		InputStream buffer = new BufferedInputStream(file);
		ObjectInput input = new ObjectInputStream (buffer);
		GameModel gameModel = (GameModel) input.readObject();
		input.close();
		if(gameModel.getGame() instanceof PlayableGame)
		{
			PlayableGameImp game = (PlayableGameImp) gameModel.getGame();
			ServerModelImp temp = (ServerModelImp) this.serverModel;
			game.setID(temp.giveValidID());
			temp.addGameFromLoadCommand(gameModel);
		}
		else
		{
			PregameGameImp game = (PregameGameImp) gameModel.getGame();
			ServerModelImp temp = (ServerModelImp) this.serverModel;
			game.setID(temp.giveValidID());
			temp.addGameFromLoadCommand(gameModel);
		}
		this.gameModel = gameModel;
		
	}
	
	public GameModel getGameModel()
	{
		return gameModel;
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
