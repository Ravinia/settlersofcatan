package server.command.games;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import server.command.CommandObject;
import server.exceptions.GameDoesNotExistException;
import server.model.GameModel;
import server.model.ServerModel;

public class SaveGameCommand implements CommandObject
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3870622359537126396L;
	private int ID;
	private String name;
	private ServerModel serverModel;
	
	/**
	 * A fully initializing constructor for this class
	 * @param iD
	 * @param name
	 */
	public SaveGameCommand(int iD, String name, ServerModel serverModel) 
	{
		super();
		ID = iD;
		this.name = name;
		this.serverModel = serverModel;
	}
	
	/**
	 * Executes needed changes to the model determined by the facade for the SaveGame function
	 * @throws GameDoesNotExistException, IOException 
	 */
	@Override
	public void executeCommand() throws GameDoesNotExistException, IOException {
		GameModel gameModel = serverModel.getGame(ID);
		OutputStream file = new FileOutputStream("saves/"+ name + ".ser");
		OutputStream buffer = new BufferedOutputStream(file);
		ObjectOutput output = new ObjectOutputStream(buffer);
		output.writeObject(gameModel);
		output.close();
	}
	@Override
	public void setServerModel(ServerModel serverModel) {
		this.serverModel = serverModel;
	}
}
