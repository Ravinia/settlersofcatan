package server.command;

import server.model.ServerModel;
import shared.debug.LogLevel;

/**
 * @author Lawrence
 * @author Eric
 */
public class ChangeLogCommand implements CommandObject
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1570540569470202017L;
	private LogLevel logLevel;
	/**
	 * A fully initializing constructor for this class
	 * @param logLevel
	 */
	public ChangeLogCommand(LogLevel logLevel) {
		super();
		this.logLevel = logLevel;
	}
	/**
     * This method updates the current setting for the log level for the entire server, which is stored in the server model.
     */
    @Override
    public void executeCommand()
    {

    }
	@Override
	public void setServerModel(ServerModel serverModel) {
		
	}
}
