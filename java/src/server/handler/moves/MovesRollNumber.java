package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.RollNumberInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlaceablesNotInitializedException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.exceptions.InvalidLocationException;

public class MovesRollNumber extends CatanMovesHandler
{
	public MovesRollNumber(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		RollNumberInfo info = this.jsonDeserial.deserializeRollNumberInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.rollNumber(gameID,
					info.getCurrentPlayer(), info.getNumber());
		} catch (GameDoesNotExistException | PlaceablesNotInitializedException | PlayerNotInGameException | InvalidIndexException | InvalidLocationException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
