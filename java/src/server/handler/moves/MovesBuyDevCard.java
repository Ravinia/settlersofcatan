package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.BuyDevCardInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class MovesBuyDevCard extends CatanMovesHandler
{
	public MovesBuyDevCard(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		BuyDevCardInfo info = this.jsonDeserial.deserializeBuyDevCardInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.buyDevCard(gameID,
					info.getCurrentPlayer());
		} catch (GameDoesNotExistException | InvalidIndexException | PlayerNotInGameException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
