package server.handler.moves;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.json.JSONObject;

import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidCookie;
import server.exceptions.InvalidIndexException;
import server.exceptions.UninitializedDataException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.exceptions.InvalidHeaderException;
import shared.player.Cookie;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * 
 * @author jameson
 *
 * This class is a super class for all moves handlers
 */
public abstract class CatanMovesHandler implements HttpHandler
{
	
	protected MovesFacade facade;
	protected ServerJsonDeserializer jsonDeserial;
	protected ServerHttpDeserializer httpDeserial;
	protected ServerSerializer serializer;
	
	public CatanMovesHandler(MovesFacade facade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		this.facade = facade;
		this.jsonDeserial = jsonDeserial;
		this.httpDeserial = httpDeserial;
		this.serializer = serializer;
	}

	/**
	 * handles requests to /moves/*
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange)
	{
		String message = "";
		int httpCode = HttpURLConnection.HTTP_OK;
		try
		{
			Cookie cookie = httpDeserial.getCookie(exchange);
			this.facade.validateCookie(cookie.getPlayerName(),
					cookie.getPassword(), cookie.getPlayerID(), cookie.getGameID());
			
			JSONObject json = httpDeserial.getJSONFromResponse(exchange);
			GameModel gameModel = performRequest(json, cookie.getGameID());
			
			String serializedModel = serializer.serializeGameModel(gameModel).toString();
			message = serializedModel;
		}
		catch (InvalidHeaderException e)
		{
			message = "It appears you did not have the proper cookies. COOKIES!";
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
		}
		catch (InvalidCookie e)
		{
			message = "Your cookie is invalid.";
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
		}
		catch (GameDoesNotExistException e)
		{
			message = "The game your cookie reffers to does not exist.";
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
		}
		finally
		{
			if (message == "")
			{
				message = "A syntactically correct command was sent to the server.";
			}
			try {
				exchange.sendResponseHeaders(httpCode, message.length());
				exchange.getResponseBody().write(message.getBytes());
				exchange.getResponseBody().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	}
	
	/**
	 * Uses the moves facade to perform the appropriate action requested by the user.
	 * @param json The JSON of the request.
	 * @param gameID The game ID of the user who sent the request.
	 * @return The game model that resulted form the request.
	 * @throws InvalidIndexException 
	 * @throws UninitializedDataException 
	 * @throws GameDoesNotExistException 
	 * @throws IOException 
	 */
	abstract protected GameModel performRequest(JSONObject json, int gameID)
			throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException, IOException;
	
}
