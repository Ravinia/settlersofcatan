package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.FinishTurnInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class MovesFinishTurn extends CatanMovesHandler
{
	public MovesFinishTurn(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		FinishTurnInfo info = this.jsonDeserial.deserializeFinishTurnInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.finishTurn(gameID,
					info.getCurrentPlayer());
		} catch (GameDoesNotExistException | UninitializedDataException | InvalidIndexException | PlayerNotInGameException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
