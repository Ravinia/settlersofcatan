package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.AcceptTradeInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class MovesAcceptTrade extends CatanMovesHandler
{
	public MovesAcceptTrade(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		AcceptTradeInfo info = this.jsonDeserial.deserializeAcceptTradeInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.acceptTrade(gameID,
					info.getCurrentPlayer(), info.isWillAccept());
		} catch (GameDoesNotExistException | NoCurrentTradesException | InvalidIndexException | PlayerNotInGameException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
