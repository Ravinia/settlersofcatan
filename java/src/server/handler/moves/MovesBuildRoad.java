package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.BuildRoadInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlaceOccupiedException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;

public class MovesBuildRoad extends CatanMovesHandler
{
	public MovesBuildRoad(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		BuildRoadInfo info = this.jsonDeserial.deserializeBuildRoadInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.buildRoad(gameID,
					info.getCurrentPlayer(), info.getRoadLocation(), info.isFree());
		} catch (GameDoesNotExistException | InvalidIndexException | InvalidRobberLocationException | PlaceOccupiedException | PlayerNotInGameException | InvalidObjectTypeException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
