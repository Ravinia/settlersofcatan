package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.PlayYearOfPlentyCardInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class MovesYearOfPlenty extends CatanMovesHandler
{
	public MovesYearOfPlenty(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		PlayYearOfPlentyCardInfo info = this.jsonDeserial.deserializePlayYearOfPlentyInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.yearOfPlenty(gameID,
                    info.getCurrentPlayer(), info.getResource1(), info.getResource2());
		} catch (GameDoesNotExistException | InvalidIndexException | PlayerNotInGameException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
