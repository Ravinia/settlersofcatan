package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.OfferTradeInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.UninitializedDataException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class MovesOfferTrade extends CatanMovesHandler
{
	public MovesOfferTrade(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		OfferTradeInfo info = this.jsonDeserial.deserializeOfferTradeInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.offerTrade(gameID,
					info.getCurrentPlayer(), info.getOffer(), info.getReceiver());
		} catch (GameDoesNotExistException | UninitializedDataException | InvalidIndexException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
