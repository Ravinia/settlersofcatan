package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.PlaySoldierCardInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.exceptions.InvalidRobberLocationException;

public class MovesSoldier extends CatanMovesHandler
{
	public MovesSoldier(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		PlaySoldierCardInfo info = this.jsonDeserial.deserializePlaySoldierCardInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.soldier(gameID,
					info.getCurrentPlayer(), info.getVictimIndex(), info.getLocation());
		} catch (GameDoesNotExistException | PlayerNotInGameException | InvalidIndexException | InvalidRobberLocationException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
