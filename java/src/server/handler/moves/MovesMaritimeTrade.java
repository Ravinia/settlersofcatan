package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.MaritimeTradeInfo;
import server.exceptions.BankOutOfResourceException;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class MovesMaritimeTrade extends CatanMovesHandler
{
	public MovesMaritimeTrade(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		MaritimeTradeInfo info = this.jsonDeserial.deserializeMaritimeTradeInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.maritimeTrade(gameID,
					info.getCurrentPlayer(), info.getRatio(), info.getInputResource(),
					info.getOutputResource());
		} catch (GameDoesNotExistException | InvalidIndexException
				| PlayerNotInGameException | BankOutOfResourceException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
