package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.PlayMonopolyCardInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class MovesMonopoly extends CatanMovesHandler
{
	public MovesMonopoly(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		PlayMonopolyCardInfo info = this.jsonDeserial.deserializePlayMonopolyCardInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.monopoly(gameID,
					info.getCurrentPlayer(), info.getResource());
		} catch (GameDoesNotExistException | PlayerNotInGameException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
