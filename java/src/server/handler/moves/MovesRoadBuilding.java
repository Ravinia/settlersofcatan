package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.PlayRoadBuildingCardInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class MovesRoadBuilding extends CatanMovesHandler
{
	public MovesRoadBuilding(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		PlayRoadBuildingCardInfo info = this.jsonDeserial.deserializePlayRoadBuildingCardInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.roadBuilding(gameID, info.getCurrentPlayer(), info.getSpot1(),
					info.getSpot2());
		} catch (GameDoesNotExistException | InvalidIndexException | PlayerNotInGameException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
