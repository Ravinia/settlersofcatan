package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.BuildSettlementInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;

public class MovesBuildSettlement extends CatanMovesHandler
{
	public MovesBuildSettlement(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		BuildSettlementInfo info = this.jsonDeserial.deserializeBuildSettlementInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.buildSettlement(gameID,
					info.getCurrentPlayer(), info.getLocation(), info.isFree());
		} catch (GameDoesNotExistException | InvalidIndexException | InvalidRobberLocationException | InvalidObjectTypeException | PlayerNotInGameException | UninitializedDataException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
