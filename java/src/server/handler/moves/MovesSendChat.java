package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.SendChatInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.UninitializedDataException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class MovesSendChat extends CatanMovesHandler
{
	public MovesSendChat(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
			throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException
	{
		SendChatInfo info = this.jsonDeserial.deserializeSendChatInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.sendChat(gameID,
					info.getCurrentPlayer(), info.getContent());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
