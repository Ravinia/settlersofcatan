package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.BuildCityInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlaceablesNotInitializedException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;

public class MovesBuildCity extends CatanMovesHandler
{
	public MovesBuildCity(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		BuildCityInfo info = this.jsonDeserial.deserializeBuildCityInfo(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.buildCity(gameID,
					info.getCurrentPlayer(), info.getLocation());
		} catch (GameDoesNotExistException | InvalidIndexException | PlaceablesNotInitializedException | InvalidLocationException | InvalidObjectTypeException | PlayerNotInGameException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
