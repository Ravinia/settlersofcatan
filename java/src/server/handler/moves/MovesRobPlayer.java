package server.handler.moves;

import java.io.IOException;

import org.json.JSONObject;

import server.courier.deserializer.RobPlayerInfo;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidIndexException;
import server.exceptions.PlayerNotInGameException;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;

public class MovesRobPlayer extends CatanMovesHandler
{
	public MovesRobPlayer(MovesFacade movesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(movesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected GameModel performRequest(JSONObject json, int gameID)
	{
		RobPlayerInfo info = this.jsonDeserial.deserializeRobPlayer(json);
		GameModel gameModel;
		try {
			gameModel = this.facade.robPlayer(gameID,
					info.getPlayerIndex(), info.getVictimIndex(), info.getLocation());
		} catch (GameDoesNotExistException | InvalidIndexException
				| PlayerNotInGameException | InvalidRobberLocationException | InvalidObjectTypeException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return gameModel;
	}
}
