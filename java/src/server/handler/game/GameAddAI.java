package server.handler.game;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import org.json.JSONObject;

import server.courier.deserializer.AddAIInfo;
import server.exceptions.InvalidCookie;
import server.facade.GameFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.exceptions.InvalidHeaderException;
import shared.player.Cookie;

import com.sun.net.httpserver.*;


public class GameAddAI extends CatanGameHandler
{

	public GameAddAI(GameFacade gameFacade, ServerJsonDeserializer jsonDeserial, ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(gameFacade, jsonDeserial, httpDeserial, serializer);
	}

	/**
	 * handles requests to the /game/addAI url
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		int httpCode = HttpURLConnection.HTTP_OK;
		String response = "Seccess";
		try
		{
			Headers h = exchange.getResponseHeaders();
		    h.add("Content-Type", "application/json");
		    
			Cookie cookie = this.httpDeserial.getCookie(exchange);
			this.facade.validateUser(cookie.getPlayerName(), cookie.getPassword(), cookie.getPlayerID(), cookie.getGameID());
			
			JSONObject body = this.httpDeserial.getJSONFromResponse(exchange);
			AddAIInfo aiInfo = this.jsonDeserial.deserializeAddAIInfo(body);
			boolean aiAdded = this.facade.addAI(cookie.getGameID(), aiInfo.getAIName());
			if(!aiAdded)
			{
				response = "AI not added";
			}
		}
		catch(InvalidCookie e)
		{
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
			response = "You do not have a valid cookie";
		}
		catch(InvalidHeaderException e)
		{
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
			response = "There has been an error on the server";
		}
		finally
		{
			exchange.sendResponseHeaders(httpCode, response.length());
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

}
