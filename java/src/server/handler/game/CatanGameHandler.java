package server.handler.game;

import server.facade.GameFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

import com.sun.net.httpserver.HttpHandler;

public abstract class CatanGameHandler implements HttpHandler
{
	protected GameFacade facade;
	protected ServerJsonDeserializer jsonDeserial;
	protected ServerHttpDeserializer httpDeserial;
	protected ServerSerializer serializer;
	
	public CatanGameHandler(GameFacade facade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		this.facade = facade;
		this.jsonDeserial = jsonDeserial;
		this.httpDeserial = httpDeserial;
		this.serializer = serializer;
	}
}
