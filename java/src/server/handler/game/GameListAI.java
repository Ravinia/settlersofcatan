package server.handler.game;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.List;

import org.json.JSONArray;
import server.facade.GameFacade;
import server.model.ai.AIType;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

import com.sun.net.httpserver.HttpExchange;

public class GameListAI extends CatanGameHandler
{

	public GameListAI(GameFacade gameFacade, ServerJsonDeserializer deserializer, ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
        super(gameFacade, deserializer, httpDeserial, serializer);
	}

	/**
	 * handles requests to the /game/listAI url
	 * @param exchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
        int httpCode = HttpURLConnection.HTTP_OK;
        String response = "";
        try
        {
            List<AIType> AIs = facade.listAI();
            JSONArray json = serializer.serializeAIList(AIs);
            response = json.toString();
        }
        catch (Exception e)
        {
            httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
            response = "There has been an error on the server";
            e.printStackTrace();
        }
        finally
        {
            exchange.sendResponseHeaders(httpCode, response.length());
            OutputStream os = exchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
	}

}
