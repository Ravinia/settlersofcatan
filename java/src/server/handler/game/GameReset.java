package server.handler.game;

import java.io.IOException;

import server.facade.GameFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class GameReset implements HttpHandler
{

	public GameReset(GameFacade gameFacade, ServerJsonDeserializer deserializer, ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
//		assert false;
	}

	/**
	 * handles requests to the /game/reset url
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange arg0) throws IOException
	{
//		assert false;
	}

}
