package server.handler.game;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import org.json.JSONObject;

import server.exceptions.CatanServerException;
import server.exceptions.InvalidCookie;
import server.facade.GameFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.exceptions.InvalidHeaderException;
import shared.player.Cookie;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class GameModel extends CatanGameHandler
{

	public GameModel(GameFacade gameFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(gameFacade, jsonDeserial, httpDeserial, serializer);
	}

	/**
	 * handles requests to the /game/model and /game/model/x urls
	 * where x is an integer greater than 0 
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		int httpCode = HttpURLConnection.HTTP_OK;
		String response = "";
		try
		{
			Headers h = exchange.getResponseHeaders();
		    h.add("Content-Type", "application/json");
		    
			Cookie cookie = this.httpDeserial.getCookie(exchange);
			this.facade.validateUser(cookie.getPlayerName(),
					cookie.getPassword(), cookie.getPlayerID(), cookie.getGameID());
			
			int versionNumber = -1;
			String[] url = exchange.getRequestURI().toString().split("/");
			
			String lastPathinURL = url[url.length-1];
			
			if(lastPathinURL.matches("\\d*"))
			{
				versionNumber = Integer.parseInt(lastPathinURL);
			}
			
			server.model.GameModel gameModel = this.facade.model(cookie.getGameID(), versionNumber);
			if (gameModel == null)
			{
				response = "\"true\"";
			}
			else
			{
				JSONObject gameModelJson = this.serializer.serializeGameModel(gameModel);
				response = gameModelJson.toString();
			}
		}
		catch(InvalidCookie e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
		}
		catch(CatanServerException | InvalidHeaderException e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
		}
		finally
		{
			exchange.sendResponseHeaders(httpCode, response.length());
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}
}
