package server.handler.user;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import server.courier.deserializer.LoginOrRegisterInfo;
import server.courier.serializer.PregamePlayerInfo;
import server.facade.UserFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class UserLogin implements HttpHandler
{
	ServerJsonDeserializer jsonDeserial;
	ServerHttpDeserializer httpDeserial;
	UserFacade facade;
	
	public UserLogin(UserFacade facade,ServerJsonDeserializer deserializer
			,ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		this.jsonDeserial = deserializer;
		this.facade = facade;
		this.httpDeserial = httpDeserial;
	}
	
	/**
	 * handles requests to the /user/login url
	 * @param exchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		int httpCode = HttpURLConnection.HTTP_OK;
		String response = "";
		
		try
		{
			Headers h = exchange.getResponseHeaders();
		    h.add("Content-Type", "application/json");
			
		    String body = this.httpDeserial.getHttpBody(exchange);
			JSONObject request = new JSONObject(body);
	
			LoginOrRegisterInfo courier = this.jsonDeserial.deserializeUserNameAndPassword(request);
		
			PregamePlayerInfo playerInfo =
					this.facade.login(courier.getUsername(), courier.getPassword());
			
			String cookie = this.httpDeserial.buildUserCookie(playerInfo.getName(),
					courier.getPassword(), playerInfo.getId());
			h.add("Set-cookie", cookie);
		}
		catch (JSONException e)
		{
			response = "Your JSON is corrupt.";
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
		}
		catch (Exception e)
		{
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
		}
		finally
		{
			if (response == "")
			{
				response = "A syntactically correct command was sent to the server.";
			}
			exchange.sendResponseHeaders(httpCode, 0);
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

}
