package server.handler.util;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.json.JSONObject;

import server.exceptions.LogLevelChangeFailedException;
import server.facade.UtilFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * 
 * @author jameson
 *
 * This class is a super class for all util handlers
 */
public abstract class CatanUtilHandler implements HttpHandler
{
	
	protected UtilFacade facade;
	protected ServerJsonDeserializer jsonDeserial;
	protected ServerHttpDeserializer httpDeserial;
	protected ServerSerializer serializer;
	
	public CatanUtilHandler(UtilFacade facade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		this.facade = facade;
		this.jsonDeserial = jsonDeserial;
		this.httpDeserial = httpDeserial;
		this.serializer = serializer;
	}

	/**
	 * handles requests to /util/*
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		int httpCode = HttpURLConnection.HTTP_OK;
		try
		{
			JSONObject json = httpDeserial.getJSONFromResponse(exchange);
			performRequest(json);
		}
		catch (LogLevelChangeFailedException e) {
			exchange.getResponseBody().write(e.getMessage().getBytes());
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
		}
		finally
		{
			exchange.sendResponseHeaders(httpCode, 0);
			exchange.getResponseBody().close();
		}
	}
	
	/**
	 * Uses the util facade to perform the appropriate action requested by the user.
	 * @param json The JSON of the request.
	 * @throws LogLevelChangeFailedException If there was an error changing the log level.
	 */
	abstract protected void performRequest(JSONObject json) throws LogLevelChangeFailedException;
}
