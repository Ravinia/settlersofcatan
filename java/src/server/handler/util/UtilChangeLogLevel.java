package server.handler.util;

import org.json.JSONObject;

import server.courier.deserializer.ChangeLogLevelInfo;
import server.exceptions.LogLevelChangeFailedException;
import server.facade.UtilFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

public class UtilChangeLogLevel extends CatanUtilHandler
{
	public UtilChangeLogLevel(UtilFacade utilFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(utilFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	protected void performRequest(JSONObject json) throws LogLevelChangeFailedException
	{
		ChangeLogLevelInfo info = this.jsonDeserial.deserializeChangeLogLevelInfo(json);
		boolean success = this.facade.changeLogLevel(info.getLogLevel());
		if (!success)
			throw new LogLevelChangeFailedException("server model error, changing log level failed");
	}
}
