package server.handler.games;

import server.facade.GamesFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import com.sun.net.httpserver.HttpHandler;

/**
 * 
 * @author jameson
 *
 * This class is a super class for all games handlers
 */
public abstract class CatanGamesHandler implements HttpHandler
{
	
	protected GamesFacade facade;
	protected ServerJsonDeserializer jsonDeserial;
	protected ServerHttpDeserializer httpDeserial;
	protected ServerSerializer serializer;
	
	public CatanGamesHandler(GamesFacade facade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		this.facade = facade;
		this.jsonDeserial = jsonDeserial;
		this.httpDeserial = httpDeserial;
		this.serializer = serializer;
	}
}
