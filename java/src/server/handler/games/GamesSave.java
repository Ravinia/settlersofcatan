package server.handler.games;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import org.json.JSONObject;

import server.courier.deserializer.SaveGameInfo;
import server.exceptions.CatanServerException;
import server.exceptions.InvalidCookie;
import server.facade.GamesFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class GamesSave extends CatanGamesHandler
{

	public GamesSave(GamesFacade gamesFacade,
			ServerJsonDeserializer jsonDeserial, ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(gamesFacade, jsonDeserial, httpDeserial, serializer);
	}

	/**
	 * handles requests to the /games/save url
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		int httpCode = HttpURLConnection.HTTP_OK;
		String response = "";
		try
		{
			Headers h = exchange.getResponseHeaders();
		    h.add("Content-Type", "application/json");
		    
//			Cookie userCookie = this.httpDeserial.getCookie(exchange);
//			this.facade.validateUser(userCookie.getPlayerName(), userCookie.getPassword(), userCookie.getPlayerID());
			
			JSONObject body = this.httpDeserial.getJSONFromResponse(exchange);
			SaveGameInfo saveGameInfo = this.jsonDeserial.deserializeSaveGame(body);
			
			boolean successful = this.facade.saveGame(saveGameInfo.getGameId(), saveGameInfo.getGameName());
			if(!successful)
			{
				throw new CatanServerException("Save game was not successful");
			}
		}
		catch(InvalidCookie e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
		}
		catch(CatanServerException e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
		}
		finally
		{
			exchange.sendResponseHeaders(httpCode, response.length());
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

}
