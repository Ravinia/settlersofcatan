package server.handler.games;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import server.courier.deserializer.JoinGameInfo;
import server.courier.serializer.GameInfo;
import server.exceptions.InvalidCookie;
import server.facade.GamesFacade;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;
import shared.player.Cookie;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class GamesJoin extends CatanGamesHandler
{
	public GamesJoin(GamesFacade gamesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(gamesFacade, jsonDeserial, httpDeserial, serializer);
	}

	/**
	 * handles requests to the /games/join url
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		int httpCode = HttpURLConnection.HTTP_OK;
		String response = "Success";
		try
		{
			Headers h = exchange.getResponseHeaders();
		    h.add("Content-Type", "application/json");
		    
			Cookie userCookie = this.httpDeserial.getCookie(exchange);
			this.facade.validateUser(userCookie.getPlayerName(), userCookie.getPassword(), userCookie.getPlayerID());
			
			JSONObject body = this.httpDeserial.getJSONFromResponse(exchange);
			JoinGameInfo joinGameInfo = this.jsonDeserial.deserializeJoinGameInfo(body);
			
			GameInfo gameInfo = this.facade.joinGame(userCookie.getPlayerID(), userCookie.getPlayerName(),joinGameInfo.getGameId(), joinGameInfo.getColor());
			String gameCookie = this.httpDeserial.buildGameCookie(gameInfo.getGameID());
		    
		    List<String> headers = new ArrayList<String>();
			headers.add(gameCookie);
			exchange.getResponseHeaders().put("Set-cookie", headers);
		}
		catch(InvalidCookie e)
		{
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
			e.printStackTrace();
		}
		catch (Exception e)
		{
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
			e.printStackTrace();
		}
		finally
		{
			exchange.sendResponseHeaders(httpCode, response.length());
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

}
