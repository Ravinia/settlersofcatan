package server.handler.games;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import server.courier.serializer.GameInfo;
import server.facade.GamesFacade;
import server.model.game.Game;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class GamesList extends CatanGamesHandler
{
	public GamesList(GamesFacade gamesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(gamesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	/**
	 * handles requests to /games/list
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		int httpCode = HttpURLConnection.HTTP_OK;
		String response = "";
		
		try
		{
			Headers h = exchange.getResponseHeaders();
			h.add("Content-Type", "application/json");
			
//			Cookie cookie = this.httpDeserial.getCookie(exchange);
//			this.facade.validateUser(cookie.getPlayerName(), cookie.getPassword(), cookie.getPlayerID());
			
			List<Game> gamesList = facade.listGames();
			List<GameInfo> gameInfoList = new ArrayList<GameInfo>();
			for (Game game : gamesList)
			{
				gameInfoList.add(game.toCourier());
			}
			
			response = serializer.serializeGameList(gameInfoList).toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
		}
		finally
		{
			exchange.sendResponseHeaders(httpCode, response.length());
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}
	
}
