package server.handler.games;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import org.json.JSONObject;

import server.courier.deserializer.LoadGameInfo;
import server.exceptions.CatanServerException;
import server.exceptions.InvalidCookie;
import server.facade.GamesFacade;
import server.model.GameModel;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class GamesLoad extends CatanGamesHandler
{

	public GamesLoad(GamesFacade gamesFacade, ServerJsonDeserializer jsonDeserial, ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(gamesFacade, jsonDeserial, httpDeserial, serializer);
	}

	/**
	 * handles requests to the /games/load url
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		int httpCode = HttpURLConnection.HTTP_OK;
		String response = "";
		try
		{
			Headers h = exchange.getResponseHeaders();
		    h.add("Content-Type", "application/json");
		    
//			Cookie userCookie = this.httpDeserial.getCookie(exchange);
//			this.facade.validateUser(userCookie.getPlayerName(), userCookie.getPassword(), userCookie.getPlayerID());
			
			JSONObject body = this.httpDeserial.getJSONFromResponse(exchange);
			LoadGameInfo loadGameInfo = this.jsonDeserial.deserializeLoadGame(body);
			
			GameModel gameModel = this.facade.loadGame(loadGameInfo.getGameName());
			if(gameModel == null)
			{
				throw new CatanServerException("Load game was not successful");
			}
		}
		catch(InvalidCookie e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
		}
		finally
		{
			exchange.sendResponseHeaders(httpCode, response.length());
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

}
