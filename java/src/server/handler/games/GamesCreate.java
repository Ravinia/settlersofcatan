package server.handler.games;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import org.json.JSONObject;

import server.courier.deserializer.GameStateInfo;
import server.courier.serializer.GameInfo;
import server.facade.GamesFacade;
import server.model.game.Game;
import server.model.game.PregameGame;
import server.serializer.ServerHttpDeserializer;
import server.serializer.ServerJsonDeserializer;
import server.serializer.ServerSerializer;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class GamesCreate extends CatanGamesHandler
{
	public GamesCreate(GamesFacade gamesFacade, ServerJsonDeserializer jsonDeserial,
			ServerHttpDeserializer httpDeserial, ServerSerializer serializer)
	{
		super(gamesFacade, jsonDeserial, httpDeserial, serializer);
	}
	
	/**
	 * handles requests to /games/create
	 * @param HttpExchange - the http request object made to the url
	 */
	@Override
	public void handle(HttpExchange exchange) throws IOException
	{
		int httpCode = HttpURLConnection.HTTP_OK;
		String response = "";
		
		try
		{
//			Cookie userCookie = this.httpDeserial.getCookie(exchange);
//			this.facade.validateUser(userCookie.getPlayerName(),
//					userCookie.getPassword(), userCookie.getPlayerID());
			
			Headers h = exchange.getResponseHeaders();
		    h.add("Content-Type", "application/json");
			
		    JSONObject json = httpDeserial.getJSONFromResponse(exchange);
			GameStateInfo info = jsonDeserial.deserializeGameState(json);
			
			Game game = facade.createGame(info.isRandomTiles(),
					info.isRandomNumbers(), info.isRandomPorts(), info.getName());
			PregameGame playableGame = (PregameGame) game;
			GameInfo gameInfo = playableGame.toCourier();
			
			response = serializer.serializeGameInfo(gameInfo).toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
		}
		finally
		{
			exchange.sendResponseHeaders(httpCode, response.length());
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}
	
}
