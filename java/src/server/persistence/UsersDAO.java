package server.persistence;

import java.io.IOException;
import java.util.List;

import server.exceptions.UserDoesNotExistException;
import server.model.player.ServerUser;
/**
 * the DAO needed to access or modify the game files held in the java file system
 * @author Eric
 *
 */
public interface UsersDAO {
	/**
	 * Uses the filemanager to open a stream, create a file, and serialize the user object to the file structure
	 * @param user
	 * @throws IOException 
	 */
	public void addUser(ServerUser user) throws IOException; 
	/**
	 * uses the filemanager to open a stream and get all the users
	 * @return
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public List<ServerUser> getUsers() throws IOException, ClassNotFoundException;
	/**
	 * uses the filemanager to get a user with the given id
	 * @param id
	 * @return
	 * @throws UserDoesNotExistException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public ServerUser getUserByID(int id) throws UserDoesNotExistException, IOException, ClassNotFoundException;
	void deleteUser(int id) throws UserDoesNotExistException;
}
