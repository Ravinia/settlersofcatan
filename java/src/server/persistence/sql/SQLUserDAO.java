package server.persistence.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import server.model.player.ServerUser;
import server.model.player.ServerUserImp;
import server.persistence.UsersDAO;

public class SQLUserDAO implements UsersDAO
{
	SQLPersistenceImp db;
	public SQLUserDAO(SQLPersistenceImp sqlPersistenceImp)
	{
		this.db = sqlPersistenceImp;
	}
	
	@Override
	public void addUser(ServerUser user)
	{
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet results = null;
//		int userId = -1;
		try {
			String addsql = "INSERT INTO User (id, username, password) VALUES (?,?,?)";
			pstmt = conn.prepareStatement(addsql);
			pstmt.setInt(1, user.getUserID());
			pstmt.setString(2, user.getUsername());
			pstmt.setString(3, user.getPassword());

			if (pstmt.executeUpdate() != 1) {
				throw new SQLException();
			}
		} catch (SQLException e) {
		}

		finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (stmt != null)
					stmt.close();
				if (results != null)
					results.close();
			} catch (Exception e) {
			}
		}
		
	}

	@Override
	public void deleteUser(int id)
	{
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		try
		{
			String updateStmt = "DELETE FROM User WHERE id=?";
			pstmt = conn.prepareStatement(updateStmt);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			
		}
		catch(SQLException e)
		{
			
		}finally {
			try {
				if (pstmt != null)
					pstmt.close();
				// if(stmt!=null)
				// stmt.close();
				// if(results!=null)
				// results.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public List<ServerUser> getUsers()
	{
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ServerUser user = null;
		List<ServerUser> listOfUsers = new ArrayList<ServerUser>();
		try
		{
			String updateStmt = "SELECT * FROM User";
			pstmt = conn.prepareStatement(updateStmt);
	
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				// username, password, firstname, lastname, email, batchID, numRec)
				user = new ServerUserImp(rs.getString(2), rs.getString(3), rs.getInt(1));
				listOfUsers.add(user);
			}
		}
		catch(SQLException e)
		{
			
		}finally {
			try {
				if (pstmt != null)
					pstmt.close();
				// if(stmt!=null)
				// stmt.close();
				// if(results!=null)
				// results.close();
			} catch (Exception e) {
			}
		}
		return listOfUsers;
	}

	@Override
	public ServerUser getUserByID(int id)
	{
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ServerUser user = null;
		try
		{
			// username, password
			String updateStmt = "SELECT * FROM User WHERE id = ?";
			pstmt = conn.prepareStatement(updateStmt);
	
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
	
			while (rs.next()) {
				// username, password, firstname, lastname, email, batchID, numRec)
				user = new ServerUserImp(rs.getString(2), rs.getString(3), rs.getInt(1));
			}
		}
		catch(SQLException e)
		{
			
		}finally {
			try {
				if (pstmt != null)
					pstmt.close();
				// if(stmt!=null)
				// stmt.close();
				// if(results!=null)
				// results.close();
			} catch (Exception e) {
			}}
		return user;
	}
	
	public void createTable() throws SQLException
	{
		Connection conn = db.getConnection();
		Statement pstmt = null;
		String deleteStmt = "DROP TABLE IF EXISTS User";
		pstmt = conn.createStatement();

		pstmt.executeUpdate(deleteStmt);
		pstmt.close();

		Statement stmt = null;
		String createStmt = "CREATE TABLE User ("
				+ "id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , "
				+ "username TEXT  , password TEXT)";

		stmt = conn.createStatement();
		stmt.executeUpdate(createStmt);
		stmt.close();
	}
}
