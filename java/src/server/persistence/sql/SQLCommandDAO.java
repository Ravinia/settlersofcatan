package server.persistence.sql;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import server.command.CommandObject;
import server.persistence.CommandsDAO;

public class SQLCommandDAO implements CommandsDAO
{
	SQLPersistenceImp db;
	public SQLCommandDAO(SQLPersistenceImp sqlPersistenceImp)
	{
		this.db = sqlPersistenceImp;
	}
	@Override
	public void addCommand(CommandObject command, int gameID)
	{
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet results = null;
		try
		{
//			ByteArrayOutputStream file = new ByteArrayOutputStream();
//			ObjectOutput output = new ObjectOutputStream(file);
//			output.writeObject(command);
			byte[] commandBytes = this.toByteArray((Object) command);
//			output.close();
			
			Connection conn = db.getConnection();
			
		
			String addsql = "INSERT INTO Command (gameId, commandInfo) VALUES (?,?)";
			pstmt = conn.prepareStatement(addsql);
			pstmt.setInt(1, gameID);
			ByteArrayInputStream bais = new ByteArrayInputStream(commandBytes);
			pstmt.setBinaryStream(2, bais, commandBytes.length);
//			pstmt.setString(2, byteString);

			if (pstmt.executeUpdate() != 1) {
				throw new SQLException();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (stmt != null)
					stmt.close();
				if (results != null)
					results.close();
			} catch (Exception e) {
			}
		}
	}
	@Override
	public List<CommandObject> getCommands(int gameID)
	{
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet results = null;
		List<CommandObject> commandList = new ArrayList<CommandObject>();
		
		try {
			String addsql = "SELECT * FROM Command WHERE gameId = ?";
			pstmt = conn.prepareStatement(addsql);
			pstmt.setInt(1, gameID);
			results = pstmt.executeQuery();

			while (results.next()) {
//				String byteString = results.getString(3);
//				byte[] b = byteString.getBytes(Charset.forName("UTF-8"));
				
				CommandObject command = null;
//			    ByteArrayInputStream gameByteStream = new ByteArrayInputStream(b);
//			    ObjectInput in = new ObjectInputStream(gameByteStream);
//			    command = (CommandObject) in.readObject();
			    
			    
				byte[] b = (byte[]) results.getObject(3);
				ByteArrayInputStream baip = new ByteArrayInputStream(b);
				ObjectInputStream ois = new ObjectInputStream(baip);
				
				command = (CommandObject) ois.readObject();
			    
			    commandList.add(command);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (stmt != null)
					stmt.close();
				if (results != null)
					results.close();
			} catch (Exception e) {
			}
		}
		
		return commandList;
	}
	public void createTable() throws SQLException
	{
		Connection conn = db.getConnection();
		Statement pstmt = null;
		String deleteStmt = "DROP TABLE IF EXISTS Command";
		pstmt = conn.createStatement();

		pstmt.executeUpdate(deleteStmt);
		pstmt.close();

		Statement stmt = null;
		String createStmt = "CREATE TABLE Command ("
				+ "id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , "
				+ "gameId INTEGER  , commandInfo BLOB)";

		stmt = conn.createStatement();
		stmt.executeUpdate(createStmt);
		stmt.close();
	}
	@Override
	public void deleteCommands(int gameID) 
	{
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet results = null;
		
		try {
			String addsql = "DELETE FROM Command WHERE gameId = ?";
			pstmt = conn.prepareStatement(addsql);
			pstmt.setInt(1, gameID);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally 
		{
			try 
			{
				if (pstmt != null)
					pstmt.close();
				if (stmt != null)
					stmt.close();
				if (results != null)
					results.close();
			} 
			catch (Exception e) 
			{
				
			}
		}		
	}
	public byte[] toByteArray(Object obj) throws IOException {
        byte[] bytes = null;
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            bytes = bos.toByteArray();
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (bos != null) {
                bos.close();
            }
        }
        return bytes;
    }
}
