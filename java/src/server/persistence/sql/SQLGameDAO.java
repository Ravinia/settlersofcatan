package server.persistence.sql;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import server.exceptions.GameDoesNotExistException;
import server.model.GameModel;
import server.persistence.GamesDAO;

public class SQLGameDAO implements GamesDAO
{
	SQLPersistenceImp db;
	public SQLGameDAO(SQLPersistenceImp sqlPersistenceImp)
	{
		this.db = sqlPersistenceImp;
	}
	@Override
	public void addGame(GameModel game) throws GameDoesNotExistException, IOException
	{
//		ByteArrayOutputStream file = new ByteArrayOutputStream();
//		ObjectOutputStream output = new ObjectOutputStream(file);
//		output.writeObject(game);
		
		byte[] gameBytes = this.toByteArray((Object) game);
//		output.close();
		
		Connection conn = db.getConnection();
		
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet results = null;
		
		try {
			String addsql = "INSERT INTO Game (id, gameInfo) VALUES (?,?)";
			pstmt = conn.prepareStatement(addsql);
			pstmt.setInt(1, game.getID());
			ByteArrayInputStream bais = new ByteArrayInputStream(gameBytes);
			pstmt.setBinaryStream(2, bais, gameBytes.length);

			if (pstmt.executeUpdate() != 1) {
				throw new SQLException();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (stmt != null)
					stmt.close();
				if (results != null)
					results.close();
			} catch (Exception e) {
			}
		}
	}
	
	@Override
	public void deleteGame(int id)
	{
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet results = null;
		
		try {
			String addsql = "DELETE FROM Game WHERE id = ?";
			pstmt = conn.prepareStatement(addsql);
			pstmt.setInt(1, id);

			if (pstmt.executeUpdate() != 1) {
				throw new SQLException();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (stmt != null)
					stmt.close();
				if (results != null)
					results.close();
			} catch (Exception e) {
			}
	}		
	}
	@Override
	public List<GameModel> getGames()
	{
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet results = null;
		List<GameModel> gameList = new ArrayList<GameModel>();
		
		try {
			String addsql = "SELECT * FROM Game";
			pstmt = conn.prepareStatement(addsql);
			results = pstmt.executeQuery();

			while (results.next()) {
				GameModel game = null;
				byte[] b = (byte[]) results.getObject(2);
				ByteArrayInputStream baip = new ByteArrayInputStream(b);
				ObjectInputStream ois = new ObjectInputStream(baip);
				
			    game = (GameModel) ois.readObject();
			    gameList.add(game);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (stmt != null)
					stmt.close();
				if (results != null)
					results.close();
			} catch (Exception e) {
			}
		}
		
		return gameList;
	}
	@Override
	public GameModel getGameByID(int id) throws GameDoesNotExistException, IOException, ClassNotFoundException
	{
		Connection conn = db.getConnection();
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet results = null;
		GameModel game = null;
		
		try {
			String addsql = "SELECT * FROM Game WHERE id = ?";
			pstmt = conn.prepareStatement(addsql);
			pstmt.setInt(1, id);
			results = pstmt.executeQuery();

			if(results.isAfterLast())
			{
				throw new GameDoesNotExistException(id);
			}
			
			while (results.next()) {
				byte[] b = (byte[]) results.getObject(2);
				ByteArrayInputStream baip = new ByteArrayInputStream(b);
				ObjectInputStream ois = new ObjectInputStream(baip);
				game = (GameModel) ois.readObject();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (stmt != null)
					stmt.close();
				if (results != null)
					results.close();
			} catch (Exception e) {
			}
		}
		
		return game;
	}
	
	public void createTable() throws SQLException
	{
		Connection conn = db.getConnection();
		Statement pstmt = null;
		String deleteStmt = "DROP TABLE IF EXISTS Game";
		pstmt = conn.createStatement();

		pstmt.executeUpdate(deleteStmt);
		pstmt.close();

		Statement stmt = null;
		String createStmt = "CREATE TABLE Game ("
				+ "id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , "
				+ "gameinfo BLOB)";

		stmt = conn.createStatement();
		stmt.executeUpdate(createStmt);
		stmt.close();
	}
	
	 public Object toObject(byte[] bytes) throws IOException, ClassNotFoundException {
	        Object obj = null;
	        ByteArrayInputStream bis = null;
	        ObjectInputStream ois = null;
	        try {
	            bis = new ByteArrayInputStream(bytes);
	            ois = new ObjectInputStream(bis);
	            obj = ois.readObject();
	        } finally {
	            if (bis != null) {
	                bis.close();
	            }
	            if (ois != null) {
	                ois.close();
	            }
	        }
	        return obj;
	    }
	 public byte[] toByteArray(Object obj) throws IOException {
	        byte[] bytes = null;
	        ByteArrayOutputStream bos = null;
	        ObjectOutputStream oos = null;
	        try {
	            bos = new ByteArrayOutputStream();
	            oos = new ObjectOutputStream(bos);
	            oos.writeObject(obj);
	            oos.flush();
	            bytes = bos.toByteArray();
	        } finally {
	            if (oos != null) {
	                oos.close();
	            }
	            if (bos != null) {
	                bos.close();
	            }
	        }
	        return bytes;
	    }
}
