package server.persistence.sql;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import server.persistence.CommandsDAO;
import server.persistence.GamesDAO;
import server.persistence.PersistenceProvider;
import server.persistence.UsersDAO;

/**
 * This is the SQL plugin that interfaces with the persistence layer
 * of the model and uses a SQL database to maintain persistence
 * 
 * @author Carnley
 */
public class SQLPersistenceImp implements PersistenceProvider
{

	private Connection connection;
	private UsersDAO userDAO;
	private GamesDAO gameDAO;
	private CommandsDAO commandDAO;
	
	public SQLPersistenceImp()
	{
		this.userDAO = new SQLUserDAO(this);
		this.gameDAO = new SQLGameDAO(this);
		this.commandDAO = new SQLCommandDAO(this);
		try
		{
			boolean createdDatabase = this.createDatabase();
			this.loadDatabaseDriver();
			if(createdDatabase)
			{
				this.startTX();
				this.loadTables();
				this.endTX(true);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public boolean createDatabase() throws IOException
	{
		File sqlFolder = new File("persistence" + File.separator + "sql");
		File sqlFile = new File("persistence" + File.separator + "sql" + File.separator + "SettlersOfCatan.sqlite");
		if(!sqlFile.exists())
		{
			sqlFolder.mkdirs();
			sqlFile.createNewFile();
			return true;
		}
		return false;
	}
	
	public void loadTables() throws SQLException
	{
		((SQLUserDAO)this.userDAO).createTable();
		((SQLGameDAO)this.gameDAO).createTable();
		((SQLCommandDAO)this.commandDAO).createTable();
	}
	
	/**
	 * Loads the Database Driver
	 */
	public void loadDatabaseDriver() {
		try {
			final String driver = "org.sqlite.JDBC";
			Class.forName(driver);
		} catch (Exception e) {
			// ERROR! Could not load database driver
			System.out.println("NO DRIVER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Opens the connection to the database
	 */
	public void openConnection() {
		String dbName = "persistence" + File.separator + "sql" + File.separator + "SettlersOfCatan.sqlite";
		String connectionURL = "jdbc:sqlite:" + dbName;

		this.connection = null;

		try {
			this.connection = DriverManager.getConnection(connectionURL);

			connection.setAutoCommit(false);
		} catch (SQLException e) {

		}
	}

	/**
	 * Closes the connection to the database
	 */
	public void closeConnection() {
		try {
			if (!this.connection.isClosed()) {
				this.connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * commits the transaction
	 */
	public void commit() {
		try {
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			// System.out.println(e.toString());
		}
	}
	
	public Connection getConnection()
	{
		return this.connection;
	}
	
	/**
	 * starts the transaction to the database
	 */
	@Override
	public void startTX() {
		this.openConnection();
	}

	/**
	 * ends the trasaction and then closes the connection
	 * @param commit a boolean to say whether to commit the transaction or not
	 */
	@Override
	public void endTX(boolean commit) {
		if (commit) {
			this.commit();
		}

		this.closeConnection();
	}
	@Override
	public CommandsDAO getCommandsDAO() {
		return this.commandDAO;
	}
	@Override
	public GamesDAO getGamesDAO() {
		return this.gameDAO;
	}
	@Override
	public UsersDAO getUsersDAO() {
		return this.userDAO;
	}
}
