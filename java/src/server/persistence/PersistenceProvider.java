package server.persistence;

/**
 * the interface that the serverModel will use to communicate with either database implementation
 * @author Eric
 *
 */
public interface PersistenceProvider {
	
	public void startTX();
	
	public void endTX(boolean commit);
	
	public CommandsDAO getCommandsDAO();
	
	public GamesDAO getGamesDAO();
	
	public UsersDAO getUsersDAO();
}
