package server.persistence;

import java.io.IOException;
import java.util.List;

import server.command.CommandObject;
/**
 * The DAO to access or modify the commands held in the Java File system
 * @author Eric
 *
 */
public interface CommandsDAO {
	
	/**
	 * Uses the filemanager to open a stream, create a file, and serialize the user object to the file structure
	 * @param command
	 * @throws IOException 
	 */
	public void addCommand(CommandObject command, int gameID) throws IOException;
	/**
	 * uses the filemanager to open a stream and get all the commands for a particular game
	 * @return
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public List<CommandObject> getCommands(int gameID) throws IOException, ClassNotFoundException;

	public void deleteCommands(int gameID);
}
