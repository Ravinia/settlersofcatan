package server.persistence;

import java.io.IOException;
import java.util.List;

import server.exceptions.GameDoesNotExistException;
import server.model.GameModel;
/**
 * the DAO needed to access or modify the game files held in the java file system
 * @author Eric
 *
 */
public interface GamesDAO {
	
	/**
	 * Uses the filemanager to open a stream, create a file, and serialize the game object to the file structure
	 * @param game
	 */
	public void addGame(GameModel game)throws GameDoesNotExistException, IOException;
	
	/**
	 * uses the filemanager to open a stream and get all the games
	 * @return
	 */
	public List<GameModel> getGames() throws IOException, ClassNotFoundException;
	
	/**
	 * uses the filemanager to get a game with the given id
	 * @param id
	 * @return
	 * @throws GameDoesNotExistException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public GameModel getGameByID(int id) throws GameDoesNotExistException, IOException, ClassNotFoundException;
	
	void deleteGame(int id) throws GameDoesNotExistException;
}
