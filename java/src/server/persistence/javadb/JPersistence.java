package server.persistence.javadb;

import server.persistence.CommandsDAO;
import server.persistence.GamesDAO;
import server.persistence.UsersDAO;
import server.persistence.PersistenceProvider;
/**
 * The persistence implementation for the Java file system plug in
 * @author Eric
 *
 */
public class JPersistence implements PersistenceProvider {
	

	@Override
	public void startTX() {	}

	@Override
	public void endTX(boolean commit) {	}

	@Override
	public CommandsDAO getCommandsDAO() {
		return JCommandsDAO.getInstance();
	}

	@Override
	public GamesDAO getGamesDAO() {
		return JGamesDAO.getInstance();
	}

	@Override
	public UsersDAO getUsersDAO() {
		return JUsersDAO.getInstance();
	}
	

}
