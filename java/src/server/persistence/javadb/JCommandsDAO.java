package server.persistence.javadb;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import server.command.CommandObject;
import server.persistence.CommandsDAO;

public class JCommandsDAO implements CommandsDAO {
	
	private static JCommandsDAO instance = null;
	Integer commandID = 0;
	protected JCommandsDAO() {
	      
	   }
	
	@Override
	public void addCommand(CommandObject command, int gameID) throws IOException 
	{
		// TODO Auto-generated method stub
		Integer temp = gameID;
		File commandsFolder = new File("persistence/jcommands/" + temp.toString() + "/");
		if(!commandsFolder.exists())
		{
			commandsFolder.mkdirs();
		}
		commandID++;
		OutputStream file = new FileOutputStream("persistence/jcommands/" + temp.toString() + "/" + commandID.toString() + ".ser");
		OutputStream buffer = new BufferedOutputStream(file);
		ObjectOutput output = new ObjectOutputStream(buffer);
		output.writeObject(command);
		output.close();
	}

	@Override
	public List<CommandObject> getCommands(int gameID) throws IOException, ClassNotFoundException 
	{
		Integer temp = gameID;
		File commandsFolder = new File("persistence/jcommands/" + temp.toString() + "/");
		if(!commandsFolder.exists())
		{
			commandsFolder.mkdirs();
		}
		File[] commandFiles = commandsFolder.listFiles();
		CommandComparator comp = new CommandComparator();
		Arrays.sort(commandFiles, comp);
		ArrayList<CommandObject> commands = new ArrayList<CommandObject>(); 
		for(int i = 0; i < commandFiles.length; i++)
		{
			InputStream file = new FileInputStream(commandFiles[i]);
			InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream (buffer);
			CommandObject command = (CommandObject) input.readObject();
			commands.add(command);
			input.close();
		}
		return commands;
	}

	public void deleteCommands(int gameID)
	{
		Integer temp = gameID;
		File commandsFolder = new File("persistence/jcommands/" + temp.toString() + "/");
		if(commandsFolder.exists())
		{
			File[] commands = commandsFolder.listFiles();
			for(File f : commands)
			{
				f.delete();
			}
			commandsFolder.delete();
		}
	}
	public static JCommandsDAO getInstance() 
	{
	      if(instance == null) 
	      {
	         instance = new JCommandsDAO();
	      }
	      return instance;
	}
	
	private class CommandComparator implements Comparator<File>{

		@Override
		public int compare(File o1, File o2) {
			// TODO Auto-generated method stub
			StringBuilder f1 = new StringBuilder(o1.getName());
			StringBuilder f2 = new StringBuilder(o2.getName());
			f1.delete(f1.length()-4, f1.length());
			f2.delete(f2.length()-4, f2.length());
			int f1id = Integer.parseInt(f1.toString());
			int f2id = Integer.parseInt(f2.toString());
			if(f1id < f2id)
				return -1;
			else if(f1id > f2id)
				return 1;
			else
				return 0;
		}
		
	}
}
