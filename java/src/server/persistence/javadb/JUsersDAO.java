package server.persistence.javadb;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import server.exceptions.UserDoesNotExistException;
import server.model.player.ServerUser;
import server.persistence.UsersDAO;

public class JUsersDAO implements UsersDAO {
	
	private static JUsersDAO instance = null;
	protected JUsersDAO() {
	      
	   }
	@Override
	public void addUser(ServerUser user) throws IOException {
		File usersFolder = new File("persistence/users/");
		if(!usersFolder.exists())
		{
			usersFolder.mkdirs();
		}
		OutputStream file = new FileOutputStream("persistence/users/"+ user.getUserID() + ".ser");
		OutputStream buffer = new BufferedOutputStream(file);
		ObjectOutput output = new ObjectOutputStream(buffer);
		output.writeObject(user);
		output.close();
	}

	@Override
	public List<ServerUser> getUsers() throws IOException, ClassNotFoundException {
		File usersFolder = new File("persistence/users");
		if(!usersFolder.exists())
		{
			usersFolder.mkdirs();
		}
		File[] userfiles = usersFolder.listFiles();
		ServerUser[] sortable = new ServerUser[userfiles.length];
		ArrayList<ServerUser> users = new ArrayList<ServerUser>(); 
		for(int i = 0; i < userfiles.length; i++)
		{
				InputStream file = new FileInputStream(userfiles[i]);
				InputStream buffer = new BufferedInputStream(file);
				ObjectInput input = new ObjectInputStream (buffer);
				ServerUser serverUser = (ServerUser) input.readObject();
				sortable[i] = serverUser;
				input.close();
		}
		Arrays.sort(sortable);
		for(ServerUser g: sortable)
		{
			users.add(g);
		}
		return users;
	}

	@Override
	public ServerUser getUserByID(int id) throws UserDoesNotExistException, IOException, ClassNotFoundException {
	
		File usersFolder = new File("persistence/jusers/");
		if(!usersFolder.exists())
		{
			usersFolder.mkdirs();
		}
		File[] users = usersFolder.listFiles();
		Integer tempid = id;
		String matchFile = tempid.toString() + ".ser";
		File usertofetch = new File("");
		for(File f: users)
		{
			if(f.getName().compareTo(matchFile) == 0)
				usertofetch = f;
		}
		if(usertofetch.getName().compareTo("") == 0)
		{
			throw new UserDoesNotExistException("dbquery fail, don't have username but id is " + id);
		}
		
		InputStream file = new FileInputStream("persistence/jusers/"+ usertofetch.getName());
		InputStream buffer = new BufferedInputStream(file);
		ObjectInput input = new ObjectInputStream (buffer);
		ServerUser user = (ServerUser) input.readObject();
		input.close();
		return user;
	}
	public static JUsersDAO getInstance() 
	{
	      if(instance == null) 
	      {
	         instance = new JUsersDAO();
	      }
	      return instance;
	}
	@Override
	public void deleteUser(int id) throws UserDoesNotExistException {
		File usersFolder = new File("persistence/jusers/");
		if(!usersFolder.exists())
		{
			usersFolder.mkdirs();
		}
		File[] users = usersFolder.listFiles();
		Integer tempid = id;
		String matchFile = tempid.toString() + ".ser";
		File usertofetch = new File("");
		for(File f: users)
		{
			if(f.getName().compareTo(matchFile) == 0)
				usertofetch = f;
		}
		if(usertofetch.getName().compareTo("") == 0)
		{
			throw new UserDoesNotExistException("dbquery fail, don't have username but id is " + id);
		}
		usertofetch.delete();
	}
}
