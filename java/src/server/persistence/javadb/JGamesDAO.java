package server.persistence.javadb;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import server.exceptions.GameDoesNotExistException;
import server.model.GameModel;
import server.persistence.GamesDAO;

public class JGamesDAO implements GamesDAO {
	
	private static JGamesDAO instance = null;
	protected JGamesDAO() {
	      
	   }
	@Override
	public void addGame(GameModel game) throws GameDoesNotExistException, IOException
	{	
		File savesFolder = new File("persistence/jgames/");
		if(!savesFolder.exists())
		{
			savesFolder.mkdirs();
		}
		OutputStream file = new FileOutputStream("persistence/jgames/"+ game.getID() + ".ser");
		OutputStream buffer = new BufferedOutputStream(file);
		ObjectOutput output = new ObjectOutputStream(buffer);
		output.writeObject(game);
		output.close();
	}
	
	@Override
	public List<GameModel> getGames() throws IOException, ClassNotFoundException {
		
		File savesFolder = new File("persistence/jgames/");
		if(!savesFolder.exists())
		{
			savesFolder.mkdirs();
		}
		File[] saveFiles = savesFolder.listFiles();
		GameComparator comp = new GameComparator();
		Arrays.sort(saveFiles, comp);
		ArrayList<GameModel> saves = new ArrayList<GameModel>(); 
		for(int i = 0; i < saveFiles.length; i++)
		{
			InputStream file = new FileInputStream(saveFiles[i]);
			InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream (buffer);
			GameModel game = (GameModel) input.readObject();
			saves.add(game);
			input.close();
		}
		return saves;
	}

	@Override
	public GameModel getGameByID(int id) throws GameDoesNotExistException, IOException, ClassNotFoundException {
		File savesFolder = new File("persistence/jgames/");
		if(!savesFolder.exists())
		{
			savesFolder.mkdirs();
		}
		File[] games = savesFolder.listFiles();
		Integer tempid = id;
		String matchFile = tempid.toString() + ".ser";
		File gametofetch = new File("");
		for(File f: games)
		{
			if(f.getName().compareTo(matchFile) == 0)
				gametofetch = f;
		}
		if(gametofetch.getName().compareTo("") == 0)
		{
			throw new GameDoesNotExistException(id);
		}
		
		InputStream file = new FileInputStream("persistence/jgames/"+ gametofetch.getName());
		InputStream buffer = new BufferedInputStream(file);
		ObjectInput input = new ObjectInputStream (buffer);
		GameModel gameModel = (GameModel) input.readObject();
		input.close();
		return gameModel;
		
	}
	public static JGamesDAO getInstance() 
	{
	      if(instance == null) 
	      {
	         instance = new JGamesDAO();
	      }
	      return instance;
	}
	@Override
	public void deleteGame(int id) throws GameDoesNotExistException {
		File savesFolder = new File("persistence/jgames/");
		if(!savesFolder.exists())
		{
			savesFolder.mkdirs();
		}
		File[] games = savesFolder.listFiles();
		Integer tempid = id;
		String matchFile = tempid.toString() + ".ser";
		File gametofetch = new File("");
		for(File f: games)
		{
			if(f.getName().compareTo(matchFile) == 0)
				gametofetch = f;
		}
		if(gametofetch.getName().compareTo("") == 0)
		{
			throw new GameDoesNotExistException(id);
		}
		gametofetch.delete();
	}
	private class GameComparator implements Comparator<File>{

		@Override
		public int compare(File o1, File o2) {
			// TODO Auto-generated method stub
			StringBuilder f1 = new StringBuilder(o1.getName());
			StringBuilder f2 = new StringBuilder(o2.getName());
			f1.delete(f1.length()-4, f1.length());
			f2.delete(f2.length()-4, f2.length());
			int f1id = Integer.parseInt(f1.toString());
			int f2id = Integer.parseInt(f2.toString());
			if(f1id < f2id)
				return -1;
			else if(f1id > f2id)
				return 1;
			else
				return 0;
		}
		
	}
}
