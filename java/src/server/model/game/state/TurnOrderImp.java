package server.model.game.state;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import server.exceptions.UninitializedDataException;
import server.model.player.Player;
import shared.definitions.Phase;

public class TurnOrderImp implements TurnOrder, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1849133056691235979L;
	private List<Player> turnOrder;
    private List<Player> reverseOrder;
	private int currentPlayerIndex = 0; //this makes sense to me to get the ball rolling.
    private Phase currentPhase;
    
    private int lastPlayerHasNotPlacedSecondRoad = 1;

    public int getCurrentPlayerIndex() {
		return currentPlayerIndex;
	}

	public TurnOrderImp(List<Player> players)
    {
        turnOrder = players;
        reverseOrder = new ArrayList<>(players);
        Collections.reverse(reverseOrder);
        currentPhase = Phase.ROUND1;
    }

	@Override
	public Player getNextPlayer()
	{
        if (currentPhase == Phase.ROUND2)
        {
        	int indexOfPlayerWhoseTurnItIs = currentPlayerIndex - 1 + lastPlayerHasNotPlacedSecondRoad;
        	if (lastPlayerHasNotPlacedSecondRoad == 1)
        		lastPlayerHasNotPlacedSecondRoad--;
            return turnOrder.get(indexOfPlayerWhoseTurnItIs);
        }
        else
        {
            if (currentPlayerIndex == turnOrder.size() - 1)
                return turnOrder.get(0);
            else
            {
            	int indexOfPlayerWhoseTurnItIs = currentPlayerIndex + 1;
                return turnOrder.get(indexOfPlayerWhoseTurnItIs);
            }
        }
	}

    @Override
	public List<Player> getTurnOrder()
    {
		return turnOrder;
	}

    @Override
	public List<Player> getReverseOrder()
    {
		return reverseOrder;
	}

    @Override
	public Player getCurrentPlayer()
    {
		return turnOrder.get(currentPlayerIndex);
	}

    @Override
    public int getIndexOfPlayerWhoseTurnItIs()
    {
        return currentPlayerIndex;
    }

    @Override
    public Phase getCurrentPhase()
    {
        return currentPhase;
    }

	@Override
	public void setCurrentPlayerIndex(int currentPlayerIndex)
    {
		this.currentPlayerIndex = currentPlayerIndex;
	}

	public void setCurrentPhase(Phase currentPhase)
    {
		this.currentPhase = currentPhase;
	}
	
	@Override
	public int getPlayerIndexByPlayerObject(Player player) throws UninitializedDataException
	{
		for(int i = 0; i < turnOrder.size(); i++)
		{
			if(turnOrder.get(i).getIdentity().equals(player.getIdentity()))
			{
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public void advancePhase()
	{
		switch(currentPhase)
		{
			case ROUND1:
				if(currentPlayerIndex == 3)
				{
					currentPhase = Phase.ROUND2;
				}
				break;
			case ROUND2:
				if(currentPlayerIndex == 0)
				{
					currentPhase = Phase.ROLLING;
				}
				break;
			default:
				currentPhase = Phase.ROLLING;
		}
	}
	
	//this function advances the current player to the next player
	@Override
	public void advancePlayer() throws UninitializedDataException
	{
		Player nextPlayer = this.getNextPlayer();
		int nextPlayerIndex = this.getPlayerIndexByPlayerObject(nextPlayer);
		currentPlayerIndex = nextPlayerIndex;
	}
}
