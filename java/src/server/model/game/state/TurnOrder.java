package server.model.game.state;



import server.exceptions.UninitializedDataException;
import server.model.player.Player;
import shared.definitions.Phase;

import java.util.List;

/**
 * Will keep track of the server.model.player turn order and provide for reverse turn order
 * during round2 of the setup segment.
 * 
 * @author Eric
 * @author Lawrence
 */
public interface TurnOrder
{
	/**
	 * Returns the turn order of the game
	 *
	 * @return  the list of players in their order
	 */
	public List<Player> getTurnOrder();

	/**
	 * returns the next server.model.player in the turn order.
	 *
	 * @return the next server.model.player
	 */
	public Player getNextPlayer();

    /**
     * Returns the server.model.player who is currently taking their turn
     * @return Player object
     */
	public Player getCurrentPlayer();
	

	public List<Player> getReverseOrder();

	public Phase getCurrentPhase();

	public abstract int getIndexOfPlayerWhoseTurnItIs();

	public void setCurrentPhase(Phase currentPhase);

	public void setCurrentPlayerIndex(int currentPlayerIndex);
	
	public int getPlayerIndexByPlayerObject(Player player) throws UninitializedDataException;

	public void advancePlayer() throws UninitializedDataException;

	public void advancePhase();
}
