package server.model.game;

import server.courier.serializer.GameInfo;
import server.exceptions.UninitializedDataException;
import server.model.game.Game;
import server.model.player.Player;
import shared.definitions.CatanColor;

/**
 * Used for games that have been created on the server, but not all players have joined yet, and thus the game has
 * not officially started yet.
 *
 * @author Lawrence
 */
public interface PregameGame extends Game
{
    /**
     * Returns the number of players that have joined this game thus far
     * @return the number of players in this game
     */
    public int getNumberOfPlayersInGame();

	public boolean containsPlayerWithID(int playerID);

    public void addPlayer(String name, int playerID, CatanColor color);

    public void addPlayer(Player player, CatanColor color);
    
    public void removePlayer(String playerName, int playerID);

    public GameInfo toCourier() throws UninitializedDataException;
}
