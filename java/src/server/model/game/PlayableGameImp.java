package server.model.game;

import java.io.Serializable;
import java.util.*;

import server.courier.serializer.GameInfo;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlayerDoesNotHaveColorException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.factories.GameFactory;
import server.model.game.bank.Bank;
import server.model.game.bank.BankImp;
import server.model.game.bank.Deck;
import server.model.game.bank.DeckImp;
import server.model.game.chatlog.ChatLog;
import server.model.game.chatlog.MessageImp;
import server.model.game.developmentcard.DevelopmentCardHand;
import server.model.game.resources.BuildPool;
import server.model.game.resources.ResourceHand;
import server.model.game.resources.ResourceHandImp;
import server.model.game.state.TurnOrder;
import server.model.game.state.TurnOrderImp;
import server.model.game.trade.TradeOffer;
import server.model.player.Player;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;

public class PlayableGameImp extends AbstractGame implements PlayableGame, Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5574207233864112721L;
	private Map<Player,ResourceHand> resourceHands = new HashMap<>();
	private Map<Player,DevelopmentCardHand> devCardHands = new HashMap<>();
	private Map<Player,BuildPool> buildPools = new HashMap<>();
    private Map<Player,Integer> armiesPlayed = new HashMap<>();
    private Map<Player,Integer> monumentsPlayed = new HashMap<>();
    private Map<Player, Integer> victoryPoints = new HashMap<>();
    private Map<Player, Boolean> hasDiscardedThisTurn = new HashMap<>(); //field should be reset somehow...
    private List<Player> playersWhoMustDiscard;
	private TradeOffer tradeOffer = null;
	private Bank bank;
	private Deck deck;
	private ChatLog chatLog;
	private TurnOrder turnOrder;
    private boolean currentPlayerHasPlayedDevCardThisTurn = false;

    public PlayableGameImp(int gameID, String name, List<Player> players, Map<Player, CatanColor> playerColors)
    {
        super(gameID, name);
        this.playerColors = playerColors;

        initialize(players);
    }

    public PlayableGameImp(PregameGame pregameGame)
    {
        super(pregameGame.getID(), pregameGame.getName());
        this.playerColors = ((PregameGameImp)pregameGame).playerColors;

        initialize(new ArrayList<>(pregameGame.getPlayers()));
    }

	@Override
	public Collection<Player> getPlayers() 
	{
		return playerColors.keySet();
	}

	@Override
	public int getCurrentVersion() 
	{
		return gameVersion;
	}

	@Override
	public CatanColor getColorOfPlayer(Player player) throws PlayerNotInGameException
	{
		try
        {
			player.getName();
		}
        catch (UninitializedDataException e)
        {
			throw new PlayerNotInGameException(this, e);
		}
		
		if(playerColors.containsKey(player))
			return playerColors.get(player);
		else
			throw new PlayerDoesNotHaveColorException(name, this);
	}

	@Override
	public ResourceHand getResourceHandOfPlayer(Player player) throws PlayerNotInGameException
	{
		try
        {
			player.getName();
		}
        catch (UninitializedDataException e)
        {
			throw new PlayerNotInGameException(this, e);
		}
		if(resourceHands.containsKey(player))
			return resourceHands.get(player);
		else
			throw new PlayerNotInGameException(name, this);
	}
	
	@Override
	public void incrementResources(Player player,
			Map<ResourceType, Integer> resources) throws PlayerNotInGameException
	{
		ResourceHand hand = getResourceHandOfPlayer(player);
		if(resources.containsKey(ResourceType.WOOD))
			hand.setWood(hand.getWood() + resources.get(ResourceType.WOOD));
		if(resources.containsKey(ResourceType.BRICK))
			hand.setBrick(hand.getBrick() + resources.get(ResourceType.BRICK));
		if(resources.containsKey(ResourceType.WHEAT))
			hand.setWheat(hand.getWheat() + resources.get(ResourceType.WHEAT));
		if(resources.containsKey(ResourceType.SHEEP))
			hand.setSheep(hand.getSheep() + resources.get(ResourceType.SHEEP));
		if(resources.containsKey(ResourceType.ORE))
			hand.setOre(hand.getOre() + resources.get(ResourceType.ORE));
	}

	@Override
	public DevelopmentCardHand getDevelopmentCardHandOfPlayer(Player player) throws PlayerNotInGameException
	{
		try
        {
			player.getName();
		}
        catch (UninitializedDataException e)
        {
			throw new PlayerNotInGameException(this, e);
		}
		if(devCardHands.containsKey(player))
			return devCardHands.get(player);
		else
			throw new PlayerNotInGameException(name, this);
	}

	@Override
	public BuildPool getBuildPoolOfPlayer(Player player) throws PlayerNotInGameException
	{
		try
        {
			player.getName();
		}
        catch (UninitializedDataException e)
        {
			throw new PlayerNotInGameException(this, e);
		}
		if(buildPools.containsKey(player))
			return buildPools.get(player);
		else
			throw new PlayerNotInGameException(name, this);
	}

	@Override
	public TradeOffer getCurrentTradeOffer() throws NoCurrentTradesException 
	{
		if(tradeOffer == null)
//			throw new NoCurrentTradesException(this);
			return null;
		else
			return tradeOffer;
	}

	@Override
	public Bank getBank() {
		return bank;
	}

	@Override
	public Deck getDeck() {
		return deck;
	}

	@Override
	public ChatLog getChatLog() {
		return chatLog;
	}
	
	@Override
	public void addChatMessage(String source, String message)
	{
		chatLog.addChat(new MessageImp(source, message));
	}
	
	@Override
	public void addLogMessage(String source, String message)
	{
		chatLog.addLog(new MessageImp(source, message));
	}

	@Override
	public TurnOrder getTurnOrder()
    {
		return turnOrder;
	}

    @Override
    public TradeOffer getTradeOffer() throws NoCurrentTradesException
    {
        if (tradeOffer == null)
            throw new NoCurrentTradesException();
        return tradeOffer;
    }
    
    @Override
    public void playArmy(Player player)
    {
    	int armPlayed = armiesPlayed.get(player);
    	armPlayed++;
    	armiesPlayed.put(player, armPlayed);
    }

    @Override
    public int getNumberOfArmiesPlayed(Player player)
    {
        return armiesPlayed.get(player);
    }
    
    @Override
    public void playMonument(Player player)
    {
    	int monPlayed = monumentsPlayed.get(player);
    	monPlayed++;
    	monumentsPlayed.put(player, monPlayed);
    }

    @Override
    public int getNumberOfMonumentsPlayed(Player player)
    {
        return monumentsPlayed.get(player);
    }

    /**
     * Whether or not the given player has discarded yet this turn
     * This is used when the phase is set to discard.
     * If the player does not have to discard, this should automatically be set to true.
     */
    @Override
    public boolean hasPlayerDiscardedThisTurn(Player player)
    {
        return hasDiscardedThisTurn.containsKey(player);
    }

    /**
     * Has current player played a development card yet their turn
     */
    @Override
    public boolean hasCurrentPlayerPlayedDevCardThisTurn()
    {
        return currentPlayerHasPlayedDevCardThisTurn;
    }

    @Override
    public void setCurrentPlayerHasPlayedDevCardThisTurn(boolean currentPlayerHasPlayedDevCardThisTurn)
    {
        this.currentPlayerHasPlayedDevCardThisTurn = currentPlayerHasPlayedDevCardThisTurn;
    }

    //Helper Methods

    private void initialize(List<Player> players)
    {
        bank = new BankImp();
        chatLog = new ChatLog();
        deck = new DeckImp();
        turnOrder = new TurnOrderImp(players);

        initializeArmies(players);
        initializeMonuments(players);
        initializeDiscarded(players);
        initializeBuildPools(players);
        initializeResourceHands(players);
        initializeDevCardHands(players);
        initializeVictoryPoints(players);
    }

    private void initializeVictoryPoints(List<Player> players)
    {
		for(Player player : players)
		{
			victoryPoints.put(player, 0);
		}
	}

	private void initializeDevCardHands(List<Player> players)
    {
        for (Player player : players)
        {
            devCardHands.put(player,new DevelopmentCardHand());
        }
    }

    private void initializeResourceHands(List<Player> players)
    {
        for (Player player : players)
        {
            resourceHands.put(player,new ResourceHandImp());
        }
    }

    private void initializeArmies(List<Player> players)
    {
        for (Player player : players)
        {
            armiesPlayed.put(player,0);
        }
    }

    private void initializeMonuments(List<Player> players)
    {
        for (Player player : players)
        {
            monumentsPlayed.put(player,0);
        }
    }

    private void initializeDiscarded(List<Player> players)
    {
        for (Player player : players)
        {
            hasDiscardedThisTurn.put(player, true);
        }
    }

    private void initializeBuildPools(List<Player> players)
    {
        for (Player player : players)
        {
            buildPools.put(player, new BuildPool());
        }
    }

	@Override
	public Player getPlayerWithID(int playerID)
	{
		List<Player> players = turnOrder.getTurnOrder();
		for (Player player : players)
		{
			if (player.getId() == playerID)
				return player;
		}
		return null;
	}

    @Override
    public GameInfo toCourier() throws UninitializedDataException
    {
        return GameFactory.convertPlayableGame(this);
    }

    @Override
	public void setPlayersWhoMustDiscard(List<Player> playersWhoMustDiscard)
	{
		this.playersWhoMustDiscard = playersWhoMustDiscard;
	}
	
	@Override
	public int removePlayerWhoDiscarded(Player player)
	{
		playersWhoMustDiscard.remove(player);
		return playersWhoMustDiscard.size();
	}
	
	@Override
	public void addPlayerWhoHasDiscarded(Player player)
	{
		this.hasDiscardedThisTurn.put(player, true);
	}
	
	@Override
	public void clearPlayersWhoDiscarded()
	{
		hasDiscardedThisTurn.clear();
	}

	public void setTradeOffer(TradeOffer tradeOffer)
	{
		this.tradeOffer = tradeOffer;
	}

	@Override
	public int getVictoryPoints(Player player)
	{
		return victoryPoints.get(player);
	}
	

	@Override
	public void setVictoryPoints(Player player, int numPoints)
	{
		victoryPoints.put(player, numPoints);
	}

}
