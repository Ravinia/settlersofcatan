package server.model.game.bank;

import java.io.Serializable;
import java.util.Random;

import shared.definitions.DevCardType;

public class DeckImp implements Deck, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1200812124174255553L;
	int yearOfPlenty = 2;
	int monopoly = 2;
	int roadBuild = 2;
	int soldier = 14;
	int monument = 5;
	
	@Override
	public int getYearOfPlentyCount() 
	{
		return this.yearOfPlenty;
	}

	@Override
	public int getMonopolyCount() 
	{
		return this.monopoly;
	}

	@Override
	public int getSoldierCount() 
	{
		return this.soldier;
	}

	@Override
	public int getRoadBuildingCount() 
	{
		return this.roadBuild;
	}

	@Override
	public int getMonumentCount() 
	{
		return this.monument;
	}

	@Override
	public int getTotalCount() 
	{
		return yearOfPlenty + monopoly + roadBuild + soldier + monument;
	}
	
	@Override
	public DevCardType chooseCardToDraw()
	{
		Random randomGen = new Random();
		int cardToDraw = randomGen.nextInt(getTotalCount());
		
		DevCardType dev;
		if((cardToDraw -= this.monument) <0)
		{
			dev = DevCardType.MONUMENT;
		}
		else if((cardToDraw -= this.yearOfPlenty) < 0)
		{
			dev = DevCardType.YEAR_OF_PLENTY;
		}
		else if((cardToDraw -= this.monopoly) < 0)
		{
			dev = DevCardType.MONOPOLY;
		}
		else if((cardToDraw -= this.roadBuild) < 0)
		{
			dev = DevCardType.ROAD_BUILD;
		}
		else
			dev = DevCardType.SOLDIER;
		
		return dev;
	}
	
	@Override
	public void buyCard(DevCardType type)
	{
		switch(type)
		{
			case SOLDIER:
			{
				soldier -=1;
				if(soldier <0)
				{
					soldier = 0;
				}
				break;
			}
			case YEAR_OF_PLENTY:
			{
				yearOfPlenty -=1;
				if(yearOfPlenty <0)
				{
					yearOfPlenty = 0;
				}
				break;
			}
			case MONOPOLY:
			{
				monopoly -=1;
				if(monopoly <0)
				{
					monopoly = 0;
				}
				break;
			}
			case ROAD_BUILD:
			{
				roadBuild -=1;
				if(roadBuild <0)
				{
					roadBuild = 0;
				}
				break;
			}
			case MONUMENT:
			{
				monument -=1;
				if(monument <0)
				{
					monument = 0;
				}
				break;
			}
		}
	}

}
