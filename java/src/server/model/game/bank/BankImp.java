package server.model.game.bank;

import java.io.Serializable;

import shared.definitions.ResourceType;

public class BankImp implements Bank, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3696544553514269647L;
	private int brick = 19;
	private int wood = 19;
	private int sheep = 19;
	private int wheat = 19;
	private int ore = 19;
	

	public int getBrick() 
	{
		return brick;
	}

	public int getWood() 
	{
		return wood;
	}

	public int getSheep() 
	{
		return sheep;
	}

	public int getWheat() 
	{
		return wheat;
	}

	public int getOre() 
	{
		return ore;
	}

	public void setBrick(int brick) 
	{
		this.brick = brick;
	}

	public void setWood(int wood) 
	{
		this.wood = wood;
	}

	public void setSheep(int sheep) 
	{
		this.sheep = sheep;
	}

	public void setWheat(int wheat) 
	{
		this.wheat = wheat;
	}

	public void setOre(int ore) 
	{
		this.ore = ore;
	}

	@Override
	public int getResourceByType(ResourceType resource) 
	{
		switch(resource)
		{
			case WOOD:
				return this.wood;
			case BRICK:
				return this.brick;
			case WHEAT:
				return this.wheat;
			case SHEEP:
				return this.sheep;
			case ORE:
				return this.ore;
		}
		return 0;
	}
	public void incrementResourceByType(ResourceType resource, int i)
	{
		switch(resource)
		{
			case WOOD:
				wood += i;
				if(this.wood <0)
					this.wood = 0;
				break;
			case BRICK:
				brick += i;
				if(this.brick <0)
					this.brick = 0;
				break;
			case WHEAT:
				wheat += i;
				if(this.wheat <0)
					this.wheat = 0;
				break;
			case SHEEP:
				sheep += i;
				if(this.sheep <0)
					this.sheep = 0;
				break;
			case ORE:
				ore += i;
				if(this.ore <0)
					this.ore = 0;
				break;
		}
	}
}
