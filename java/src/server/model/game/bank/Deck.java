package server.model.game.bank;

import shared.definitions.DevCardType;

/**
 * Total deck of development cards for a game
 * @author Eric
 */
public interface Deck
{
	/**
	 * @return the yearOfPlentyCount
	 */
	public  int getYearOfPlentyCount();

	/**
	 * @return the monopolyCount
	 */
	public  int getMonopolyCount();

	/**
	 * @return the soldier
	 */
	public  int getSoldierCount();

	/**
	 * @return the roadBuilding
	 */
	public  int getRoadBuildingCount();

	/**
	 * @return the monumentCount
	 */
	public  int getMonumentCount();
	
	/**
	 * @return the total number of development cards
	 */
	public int getTotalCount();
	
	public DevCardType chooseCardToDraw();
	
	public void buyCard(DevCardType type);
}