package server.model.game;

import java.util.List;
import java.util.Map;

import server.courier.serializer.GameInfo;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.game.Game;
import server.model.game.bank.Bank;
import server.model.game.bank.Deck;
import server.model.game.chatlog.ChatLog;
import server.model.game.developmentcard.DevelopmentCardHand;
import server.model.game.resources.BuildPool;
import server.model.game.resources.ResourceHand;
import server.model.game.state.TurnOrder;
import server.model.game.trade.TradeOffer;
import server.model.player.Player;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;

/**
 * A game object of a game that has already started
 * @author Lawrence
 */
public interface PlayableGame extends Game
{
    /**
     * Gets the current version of the game
     * @return the game version
     */
    public int getCurrentVersion();

    /**
     * Gets the color for a specified server.model.player
     * @param player the server.model.player to get the color of
     * @return the color of the server.model.player
     * @throws PlayerNotInGameException
     */
    public CatanColor getColorOfPlayer(Player player) throws PlayerNotInGameException;

    /**
     * Gets the resource hand for a given server.model.player
     * @param player the server.model.player to get the resource hand of
     * @return the server.model.player's resource hand
     * @throws PlayerNotInGameException
     */
    public ResourceHand getResourceHandOfPlayer(Player player) throws PlayerNotInGameException;

    /**
     * Gets the development card hand of a specified server.model.player
     * @param player the server.model.player to get the development card hand of
     * @return the server.model.player's development card hand
     * @throws PlayerNotInGameException
     */
    public DevelopmentCardHand getDevelopmentCardHandOfPlayer(Player player) throws PlayerNotInGameException;

    /**
     * Gets the build pool for a specified server.model.player
     * @param player the server.model.player to get the build pool of
     * @return the server.model.player's build pool
     * @throws PlayerNotInGameException
     */
    public BuildPool getBuildPoolOfPlayer(Player player) throws PlayerNotInGameException;

    /**
     * Gets the current pending trade offer, if any.
     * If there is no trade currently being offered, throws a NoCurrentTradesException
     * @return the trade offer object which includes the sender, the recipient, and the resources being offered
     * @throws NoCurrentTradesException
     */
    public TradeOffer getCurrentTradeOffer() throws NoCurrentTradesException;

    /**
     * Retrieves the bank of total resources
     * @return the Bank object for this game
     */
    public Bank getBank();

    /**
     * Retrieves the deck of total development cards
     * @return the Deck object for this game
     */
    public Deck getDeck();

    /**
     * Retrieves the chat log object for this game
     * @return the chat log object
     */
    public ChatLog getChatLog();

    /**
     * Retrieves the TurnOrder object which handles the info for which order players take their turns in
     * @return the TurnOrder object
     */
    public TurnOrder getTurnOrder();

    public TradeOffer getTradeOffer() throws NoCurrentTradesException;

    public int getNumberOfArmiesPlayed(Player player);

    public int getNumberOfMonumentsPlayed(Player player);

    /**
     * Whether or not the given player has discarded yet this turn
     * This is used when the phase is set to discard.
     * If the player does not have to discard, this should automatically be set to true.
     */
    public boolean hasPlayerDiscardedThisTurn(Player player);

    /**
     * Has current player played a development card yet their turn
     */
    public boolean hasCurrentPlayerPlayedDevCardThisTurn();

	public void addChatMessage(String source, String message);

	public void incrementResources(Player player, Map<ResourceType, Integer> resources) throws PlayerNotInGameException;

	public void setPlayersWhoMustDiscard(List<Player> playersWhoMustDiscard);

	/**
	 * Signals the model that a player has discarded and returns the size of the 
	 * list of players who still need to discard.
	 * @param player
	 * @return
	 */
	public int removePlayerWhoDiscarded(Player player);

	public void addPlayerWhoHasDiscarded(Player player);

	public void clearPlayersWhoDiscarded();

	public void playMonument(Player player);

	public void playArmy(Player player);

    public GameInfo toCourier() throws UninitializedDataException;

	public void addLogMessage(String source, String message);

	public void setVictoryPoints(Player player, int numPoints);

	public int getVictoryPoints(Player player);

	public void setCurrentPlayerHasPlayedDevCardThisTurn(
			boolean currentPlayerHasPlayedDevCardThisTurn);

}
