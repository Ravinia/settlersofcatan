package server.model.game;

import server.model.player.Player;
import shared.definitions.CatanColor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract class that is inherited by PregameGame and PlayableGame
 * @author Lawrence
 */
public abstract class AbstractGame implements Game, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5771588579159180508L;
	protected int ID;
    protected String name;
    protected Map<Player,CatanColor> playerColors;
    protected int gameVersion = 1;

    public AbstractGame(int ID, String name)
    {
        this.ID = ID;
        this.name = name;
        playerColors = new HashMap<>();
    }

    @Override
    public int getID()
    {
        return ID;
    }

    @Override
    public String getName()
    {
        return name;
    }
    public void incrementGameVersion(){
		gameVersion += 1;
	}
    @Override
	public CatanColor getColor(Player player)
    {
    	return this.playerColors.get(player);
    }

	@Override
	public int getGameVersion()
	{
		return gameVersion;
	}

	public Map<Player, CatanColor> getPlayerColors()
	{
		return playerColors;
	}
    public void setID(int ID)
    {
    	this.ID = ID;
    }
    
}
