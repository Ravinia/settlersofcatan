package server.model.game.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import shared.definitions.ResourceType;

/**
 * receptacle of resource cards particular to a server.model.player
 * 
 * @author Eric
 * @author Lawrence
 */
public class ResourceHandImp implements ResourceHand, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1806974533665362183L;
	private int sheep = 0;
	private int brick = 0;
	private int wood = 0;
	private int ore = 0;
	private int wheat = 0;
	
	
	public ResourceHandImp()
    { }

	public ResourceHandImp(int sheep, int brick, int wood, int ore, int wheat)
    {
		this.sheep = sheep;
		this.brick = brick;
		this.wood = wood;
		this.ore = ore;
		this.wheat = wheat;
	}
	
	public ResourceHandImp(Map<ResourceType, Integer> resources)
	{
		this.sheep = resources.get(ResourceType.SHEEP);
		this.wood = resources.get(ResourceType.WOOD);
		this.brick = resources.get(ResourceType.BRICK);
		this.ore = resources.get(ResourceType.ORE);
		this.wheat = resources.get(ResourceType.WHEAT);
	}
	
	@Override
	public int getBrick()
    {
		return brick;
	}
	
	@Override
	public int getWood()
    {
		return wood;
	}

	@Override
	public int getOre()
    {
		return ore;
	}
	
	@Override
	public int getWheat()
    {
		return wheat;
	}
	
	@Override
	public int getSheep()
    {
		return sheep;
	}
	
	@Override
	public int getTotalResourceCount()
	{
		return sheep + brick + wood + ore + wheat;
	}
	
	@Override
	public int getResourceCount(ResourceType resource)
	{
		int numOfResource = 0;
		switch(resource)
		{
			case BRICK:
				numOfResource = this.brick;
				break;
			case WOOD:
				numOfResource = this.wood;
				break;
			case SHEEP:
				numOfResource = this.sheep;
				break;
			case WHEAT:
				numOfResource = this.wheat;
				break;
			case ORE:
				numOfResource = this.ore;
				break;
		}
		return numOfResource;
	}

	@Override
	public void setBrick(int brick) {
		this.brick = brick;
	}

	@Override
	public void setWood(int wood) {
		this.wood = wood;
	}

	@Override
	public void setOre(int ore) {
		this.ore = ore;
	}

	@Override
	public void setWheat(int wheat) {
		this.wheat = wheat;
	}

	@Override
	public void setSheep(int sheep) {
		this.sheep = sheep;
	}

    public boolean equals(Object o)
    {
        if (!(o instanceof ResourceHand))
            return false;
        ResourceHandImp hand = (ResourceHandImp)o;
        if (hand.brick == brick && hand.ore == ore && hand.wood == wood && hand.wheat == wheat && hand.sheep == sheep)
            return true;
        return false;
    }

	@Override
	public void incrementBrick(int brick)
	{
		this.brick += brick;
		if(this.brick < 0)
			this.brick = 0;
	}

	@Override
	public void incrementWood(int wood)
	{
		this.wood += wood;
		if(this.wood < 0)
			this.wood = 0;
	}

	@Override
	public void incrementOre(int ore)
	{
		this.ore += ore;
		if(this.wood < 0)
			this.ore = 0;
	}

	@Override
	public void incrementWheat(int wheat)
	{
		this.wheat += wheat;
		if(this.wheat < 0)
			this.wheat = 0;
	}

	@Override
	public void incrementSheep(int sheep)
	{
		this.sheep += sheep;
		if(this.sheep < 0)
			this.sheep = 0;
	}

	@Override
	public void DiscardResources(Collection<ResourceType> resources) {
		for(ResourceType type : resources)
		{
			switch(type)
			{
				case ORE:
					this.ore --;
					break;
				case WHEAT:
					this.wheat --;
					break;
				case BRICK:
					this.brick --;
					break;
				case WOOD:
					this.wood --;
					break;
				case SHEEP:
					this.sheep -- ;
					break;
			}
		}
	}

	@Override
	public void setResourceByType(ResourceType resource, int i)
	{
		switch(resource)
		{
			case WOOD:
				wood = i;
				break;
			case BRICK:
				brick = i;
				break;
			case WHEAT:
				wheat = i;
				break;
			case SHEEP:
				sheep = i;
				break;
			case ORE:
				ore = i;
		}
	}
	
	@Override
	public void incrementResourceByType(ResourceType resource, int i)
	{
		switch(resource)
		{
			case WOOD:
				wood += i;
				if(this.wood <0)
					this.wood = 0;
				break;
			case BRICK:
				brick += i;
				if(this.brick <0)
					this.brick = 0;
				break;
			case WHEAT:
				wheat += i;
				if(this.wheat <0)
					this.wheat = 0;
				break;
			case SHEEP:
				sheep += i;
				if(this.sheep <0)
					this.sheep = 0;
				break;
			case ORE:
				ore += i;
				if(this.ore <0)
					this.ore = 0;
				break;
		}
	}

	@Override
	public List<ResourceType> getResourceList()
	{
		List<ResourceType> resources = new ArrayList<ResourceType>();
		int i;
		for(i = 0; i<wood; i++)
			resources.add(ResourceType.WOOD);
		for(i = 0; i<brick; i++)
			resources.add(ResourceType.BRICK);
		for(i = 0; i<wheat; i++)
			resources.add(ResourceType.WHEAT);
		for(i = 0; i<sheep; i++)
			resources.add(ResourceType.SHEEP);
		for(i = 0; i<ore; i++)
			resources.add(ResourceType.ORE);
		return resources;
	}
	
	
}
