package server.model.game.resources;

import java.io.Serializable;

/**
 * Primarily a data holder for how many roads, settlements, and cities a server.model.player can
 * still purchase and place.
 *
 * @author Lawrence
 */
public class BuildPool implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -1130210775217822427L;
	public static final int DEFAULT_ROAD_COUNT = 15;
    public static final int DEFAULT_SETTLEMENT_COUNT = 5;
    public static final int DEFAULT_CITY_COUNT = 4;

	private int roads;
	private int settlements;
	private int cities;

    public BuildPool()
    {
        roads = DEFAULT_ROAD_COUNT;
        settlements = DEFAULT_SETTLEMENT_COUNT;
        cities = DEFAULT_CITY_COUNT;
    }
	
	
    public int getRoads() 
    {
		return roads;
	}

	public void setRoads(int roads) 
	{
		this.roads = roads;
	}

	public int getSettlements() 
	{
		return settlements;
	}

	public int getSettlementsPlayed()
	{
		return DEFAULT_SETTLEMENT_COUNT - settlements;
	}
	
	public void setSettlements(int settlements) 
	{
		this.settlements = settlements;
	}

	public int getCities() 
	{
		return cities;
	}
	
	public int getCitiesPlayed()
	{
		return DEFAULT_CITY_COUNT - cities;
	}
	
	public void setCities(int cities)
	{
		this.cities = cities;
	}

	/**
     * decrements the road count
     */
    public void placeRoad()
    {
    	roads -= 1;
    }

    /**
     * decrement the settlement count
     */
    public void placeSettlement()
    {
    	settlements -= 1;
    }

    /**
     * decrements the city count and increments the settlement count again
     */
    public void placeCity()
    {
    	cities -=1;
    	settlements +=1;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BuildPool pool = (BuildPool) o;

        if (cities != pool.cities) return false;
        if (roads != pool.roads) return false;
        if (settlements != pool.settlements) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = roads;
        result = 31 * result + settlements;
        result = 31 * result + cities;
        return result;
    }
}
