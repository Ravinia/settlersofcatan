package server.model.game.resources;

import java.util.Collection;
import java.util.List;

import shared.definitions.ResourceType;

/**
 * Handles the storing and playing of development card for a server.model.player
 * @author Eric
 * @author Lawrence
 */
public interface ResourceHand
{

	/**
     * Gets the number of bricks in this resource hand
     * @return the number of brick resource cards
     */
    public int getBrick();
    /**
     * Gets the amount of wood in this resource hand
     * @return the number of wood resource cards
     */
    public int getWood();
    /**
     * Gets the amount of ore in this resource hand
     * @return the number of ore resource cards
     */
    public int getOre();
    /**
     * Gets the amount of wheat in this resource hand
     * @return the number of wheat resource cards
     */
    public int getWheat();

    /**
     * Gets the number of sheep in this resource hand
     * @return the number of sheep resource cards
     */
    public int getSheep();
    
    public void setBrick(int brick);
    
	public void setWood(int wood);
	
	public void setOre(int ore);
	
	public void setWheat(int wheat);
	
	public void setSheep(int sheep);
	
	public void incrementBrick(int brick);
	
	public void incrementWood(int wood);
	
	public void incrementOre(int ore);
	
	public void incrementWheat(int wheat);
	
	public void incrementSheep(int sheep);
	
	public void DiscardResources(Collection<ResourceType> resources);
	
	/**
     * Gets the total number of resource cards in this resource hand
     * @return total resource count
     */
    public int getTotalResourceCount();
    
    /**
     * Gets the total number of resource cards of a specified type in this resource hand
     * @param resource the resource type
     * @return count of specified resource
     */
    public int getResourceCount(ResourceType resource);
    
	public void setResourceByType(ResourceType resource, int i);
	
	public void incrementResourceByType(ResourceType resource, int i);
	
	public List<ResourceType> getResourceList();
}
