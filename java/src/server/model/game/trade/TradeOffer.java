package server.model.game.trade;

import server.model.game.resources.ResourceHand;

/**
 * Object representing a current domestic trade offer
 * @author Lawrence
 * @author Eric
 */
public interface TradeOffer 
{
    /**
     * @return the sender's index for the current game
     */
    public int getSenderIndex();

    /**
     * @return the receiver's index for the current game
     */
    public int getReceiverIndex();

    /**
     * Gets the resources being offered by the sender.
     * All values should be positive.
     * @return the resources being offered
     */
    public ResourceHand getOffer();
}
