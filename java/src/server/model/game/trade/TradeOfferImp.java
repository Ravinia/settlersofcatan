package server.model.game.trade;

import java.io.Serializable;

import server.courier.serializer.TradeOfferInfo;
import server.model.game.resources.ResourceHand;

public class TradeOfferImp implements TradeOffer, TradeOfferInfo, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8172570000176373254L;
	private int senderIndex;
	private int receiverIndex;
	private ResourceHand resources;
	
	public TradeOfferImp(int senderIndex, int receiverIndex, ResourceHand resources)
    {
		
		this.senderIndex = senderIndex;
		this.receiverIndex = receiverIndex;
		this.resources = resources;
	}

	@Override
	public int getSenderIndex() {
		return senderIndex;
	}

	@Override
	public int getReceiverIndex() {
		return receiverIndex;
	}

	@Override
	public ResourceHand getOffer() {
		return resources;
	}

}
