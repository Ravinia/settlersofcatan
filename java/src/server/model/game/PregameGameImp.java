package server.model.game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import server.courier.serializer.GameInfo;
import server.exceptions.UninitializedDataException;
import server.factories.GameFactory;
import server.model.player.Player;
import server.model.player.ServerPlayer;
import shared.definitions.CatanColor;

public class PregameGameImp extends AbstractGame implements PregameGame, Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8920261178664296169L;
	private List<Player> players = new ArrayList<>();

	public PregameGameImp(int ID, String name)
    {
		super(ID, name);
	}

	@Override
	public Collection<Player> getPlayers()
    {
		return players;
	}
	
	@Override
	public boolean containsPlayerWithID(int playerID)
	{
		for(Player p : players)
		{
			if(p.getId() == playerID)
				return true;
		}
		return false;
	}
	
	@Override
	public Player getPlayerWithID(int playerID) {
		for(Player p : players)
		{
			if(p.getId() == playerID)
				return p;
		}
		return null;
	}

	@Override
	public int getNumberOfPlayersInGame()
	{
		return players.size();
	}
	
	@Override
	public void addPlayer(String name, int playerID, CatanColor color)
	{
        Player player = new ServerPlayer(playerID, name);
		players.add(player);
        playerColors.put(player,color);
	}

    @Override
    public void addPlayer(Player player, CatanColor color)
    {
        players.add(player);
        playerColors.put(player,color);
    }
    
	@Override
    public void removePlayer(String playerName, int playerID)
    {
		Player toRemove = null;
    	for (Player p : players)
    	{
    		try {
				if (p.getId() == playerID && p.getName().equals(playerName));
				{
					toRemove = p;
				}
					
			} catch (UninitializedDataException e) {
				e.printStackTrace();
			}
    	}
    	
    	players.remove(toRemove);
		playerColors.remove(toRemove);
    }
    
    @Override
    public GameInfo toCourier() throws UninitializedDataException
    {
        return GameFactory.convertPregameGame(this);
    }

}
