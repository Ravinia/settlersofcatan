package server.model.game.chatlog;

import java.io.Serializable;

/**
 * Abstract implementation of the Message interface.
 * @author Lawrence
 */
public class MessageImp implements Message, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 4910580387946160468L;
	private String source;
    private String message;

    public MessageImp(String source, String message)
    {
        this.source = source;
        this.message = message;
    }

    @Override
    public String getSource()
    {
        return source;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
