package server.model.game.chatlog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Container object that holds all of the log and chat messages.
 */
public class ChatLog implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 8420640761801892188L;
	private List<Message> chats;
    private List<Message> logs;

    public ChatLog()
    {
        chats = new ArrayList<>();
        logs = new ArrayList<>();
    }

	/**
     * Gets all of the chat messages
     * @return list of chat messages
     */
    public List<Message> getChats()
    {
    	return chats;
    }

    /**
     * Gets all of the log messages
     * @return list of log messages
     */
    public List<Message> getLogs()
    {
    	return logs;
    }
    
    public void addChat(Message message)
    {
    	chats.add(message);
    }
    
    public void addLog(Message message)
    {
    	logs.add(message);
    }
}