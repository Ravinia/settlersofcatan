package server.model.game.developmentcard;

import shared.definitions.DevCardType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Handles the storing and playing of development card for a player
 * @author Eric
 * @author Lawrence
 */
public class DevelopmentCardHand implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7056207876473036006L;
	Collection<DevCardType> oldDevelopmentCards;
	Collection<DevCardType> newDevelopmentCards;

    public DevelopmentCardHand()
    {
        oldDevelopmentCards = new ArrayList<>();
        newDevelopmentCards = new ArrayList<>();
    }

	/**
     * Retrieves a collection of all the development cards of the current player
     * @return all of the player's cards
     */
	 public DevelopmentCardHand(Collection<DevCardType> oldCards, Collection<DevCardType> newCards)
	    {
	    	this.oldDevelopmentCards = oldCards;
	    	this.newDevelopmentCards = newCards;
	    }
	public Collection<DevCardType> getAllCards()
    {
		Collection<DevCardType> allDevCards = new ArrayList<DevCardType>(oldDevelopmentCards);
        allDevCards.addAll(newDevelopmentCards);
    	
    	return allDevCards;
    }

    /**
     * Returns a collection of all of the development cards that the current player has that they can currently play
     * @return all of the cards the player can currently play
     */
    public Collection<DevCardType> getAllPlayableCards()
    {
    	Collection<DevCardType> oldPlusMonument = new ArrayList<DevCardType>(oldDevelopmentCards);
    	for(DevCardType i: newDevelopmentCards)
    	{
    		if(i == DevCardType.MONUMENT)
    		{
    			oldPlusMonument.add(i);
    		}
    	}
    	
    	return oldPlusMonument;
    }

    /**
     * Gets all the types of development cards the player that this belongs to can play
     * @return all playable card types
     */
	public Collection<DevCardType> getPlayableDevCardTypes()
	{
		Collection<DevCardType> devCardTypes = new ArrayList<DevCardType>();
        
        for(DevCardType i : this.getAllPlayableCards())
        {
        	devCardTypes.add(i);
        }
        
        return devCardTypes;
	}

    /**
     * Returns all the cards that the player has that they cannot play yet
     * @return collection of development cards
     */
	public Collection<DevCardType> getNonPlayableDevCards()
	{
		Collection<DevCardType> allNewDevCards = new ArrayList<DevCardType>(this.newDevelopmentCards);
		return allNewDevCards;
	}

    /**
     * Adds a card as a new card
     * @param newCard the card to add
     */
	public void addNewCard(DevCardType newCard)
	{
		this.newDevelopmentCards.add(newCard);
	}

    /**
     * Adds a card as an old card
     * @param oldCard the card to add
     */
	public void addOldCard(DevCardType oldCard)
	{
		this.oldDevelopmentCards.add(oldCard);
	}

    /**
     * Gets the number of cards that the player can play for a given type of card
     * @param type the card type
     * @return zero if none
     */
	public int getPlayableNumberOfCardsOfType(DevCardType type)
	{
		int count = 0;
		for (DevCardType d : getAllPlayableCards())
		{
			if (d == type)
				count++;
		}
		return count;
	}

    /**
     * Gets the total number of cards that a player holds of a certain type
     * @param type the card type
     * @return zero if none
     */
	public int getTotalNumberOfCardsOfType(DevCardType type)
	{
		int count = 0;
		for (DevCardType d : getAllCards())
		{
			if (d == type)
				count++;
		}
		return count;
	}
	
	public void removeCard(DevCardType type)
	{
		switch(type)
		{
			case SOLDIER:
			{
				if(this.oldDevelopmentCards.contains(DevCardType.SOLDIER))
				{
					oldDevelopmentCards.remove(DevCardType.SOLDIER);
				}
				break;
			}
			case YEAR_OF_PLENTY:
			{
				if(this.oldDevelopmentCards.contains(DevCardType.YEAR_OF_PLENTY))
				{
					oldDevelopmentCards.remove(DevCardType.YEAR_OF_PLENTY);
				}
				break;
			}
			case MONOPOLY:
			{
				if(this.oldDevelopmentCards.contains(DevCardType.MONOPOLY))
				{
					oldDevelopmentCards.remove(DevCardType.MONOPOLY);
				}
				break;
			}
			case ROAD_BUILD:
			{
				if(this.oldDevelopmentCards.contains(DevCardType.ROAD_BUILD))
				{
					oldDevelopmentCards.remove(DevCardType.ROAD_BUILD);
				}
				break;
			}
			case MONUMENT:
			{
				if(this.oldDevelopmentCards.contains(DevCardType.MONUMENT))
				{
					oldDevelopmentCards.remove(DevCardType.MONUMENT);
				}
				else if(this.newDevelopmentCards.contains(DevCardType.MONUMENT))
				{
					newDevelopmentCards.remove(DevCardType.MONUMENT);
				}
				break;
			}
		}
	}

    public Collection<DevCardType> getOldDevelopmentCards()
    {
        return oldDevelopmentCards;
    }

    public Collection<DevCardType> getNewDevelopmentCards()
    {
        return newDevelopmentCards;
    }
    /*
     * Moves cards from the new Cards to the old cards collections.
     */
    public void ageCards()
    {
    	oldDevelopmentCards.addAll(newDevelopmentCards);
    	newDevelopmentCards.clear();
    }
}
