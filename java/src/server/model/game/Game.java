package server.model.game;

import server.courier.serializer.GameInfo;
import server.exceptions.UninitializedDataException;
import server.model.player.Player;
import shared.definitions.CatanColor;

import java.util.Collection;

/**
 * @author Lawrence
 */
public interface Game
{
    /**
     * Retrieves the unique numerical ID that identifies this game
     * @return the ID
     */
    public int getID();

    /**
     * Retrieves the name of the game
     * @return the name
     */
    public String getName();

    /**
     * Retrieves the players that are in a game thus far.
     * For a pregame game with no players yet in it this collection should be empty.
     * @return player objects in this game
     */
    public Collection<Player> getPlayers();
    
    public void incrementGameVersion();
    
    /**
     * @param playerID
     * @return Returns null if no player with that ID exists, otherwise returns the player object.
     */
    public Player getPlayerWithID(int playerID);

	public abstract CatanColor getColor(Player player);

	public abstract int getGameVersion();

    public GameInfo toCourier() throws UninitializedDataException;
}
