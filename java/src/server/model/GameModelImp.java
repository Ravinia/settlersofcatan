package server.model;

import server.courier.serializer.*;
import server.exceptions.CatanServerException;
import server.exceptions.InvalidIndexException;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.factories.*;
import server.model.board.Board;
import server.model.board.ServerBoard;
import server.model.game.*;
import server.model.game.resources.BuildPool;
import server.model.player.Player;
import shared.definitions.CatanColor;
import shared.exceptions.CatanException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static server.model.player.DefaultPlayer.*;

public class GameModelImp implements GameModel, Comparable<GameModel>, Serializable
{
	private static final long serialVersionUID = -7696947113866733952L;
	private Game game;
	private Board board;

    private int longestRoadLength = 4;
    private Player longestRoadOwner = DEFAULT_PLAYER;
    private int largestArmySize = 2;
    private Player largestArmyOwner = DEFAULT_PLAYER;

    private Player winner = DEFAULT_PLAYER;

    public GameModelImp(boolean randomHexes, boolean randomPorts, boolean randomTokens, int gameID, String gameName) throws CatanServerException
    {
        try
        {
            board = new ServerBoard(randomHexes, randomPorts, randomTokens);
            game = new PregameGameImp(gameID, gameName);
        }
        catch (CatanException e)
        {
            throw new CatanServerException("Exception encountered in Server:",e);
        }
    }

	public Game getGame()
    {
		return game;
	}

	@Override
	public Board getBoard()
    {
		return board;
	}

	@Override
	public boolean isGameStarted()
    {
		return this.game instanceof PlayableGame;
	}

	@Override
	public CatanColor getColorOfPlayer(Player player) throws PlayerNotInGameException
    {
		if(game instanceof PlayableGame)
		{
			PlayableGame temp = (PlayableGame)game;
			return temp.getColorOfPlayer(player);
		} 
		else
			return game.getColor(player);
	}

    @Override
    public boolean isSameVersion(int version)
    {
        return ((PlayableGame)game).getCurrentVersion() == version;
    }

    @Override
	public int getID() {
		return game.getID();
	}

    @Override
    public List<Player> getPlayers()
    {
        if (game instanceof PregameGame)
        {
            return new ArrayList<>(game.getPlayers());
        }
        else // instance of PlayableGame
        {
            return ((PlayableGame)game).getTurnOrder().getTurnOrder();
        }
    }
    
    @Override
    public Player getPlayerByIndex(int index) throws InvalidIndexException
    {
    	List<Player> players = getPlayers();
    	if(index < 0 || index >= players.size())
    		throw new InvalidIndexException(index);
    	return players.get(index);
    	
    }

    @Override
    public int getIndexOfPlayer(Player player) throws PlayerNotInGameException
    {
    	System.out.println("getting index of player... class:" + player.getClass());
        for (int i = 0; i < getPlayers().size(); i++)
        {
            if (player.equals(getPlayers().get(i)))
                return i;
        }
        try
        {
            throw new PlayerNotInGameException(player.getName(), this.game);
        }
        catch (UninitializedDataException e)
        {
            throw new PlayerNotInGameException(this.game, e);
        }
    }

    @Override
    public GameModelInfo toCourier() throws PlayerNotInGameException, UninitializedDataException, NoCurrentTradesException
    {
    	BankInfo bank = null;
        DeckInfo deck = null;
        ChatInfo chats = null;
        LogInfo logs = null;
        MapInfo map = null;
        List<PlayerInfo> players = convertPlayers();
        TurnTrackerInfo tracker = null;
        TradeOfferInfo offer = null;
        
    	if(game instanceof PlayableGame)
    	{
	        bank = convertBank();
	        deck = convertDeck();
	        chats = convertChats();
	        logs = convertLogs();
	        map = convertMap();
	        tracker = convertTurnTracker();
	        offer = (TradeOfferInfo) ((PlayableGame)game).getCurrentTradeOffer();
    	}
    	
        int version = game.getGameVersion();
        
        GameModelInfoImp modelInfo = new GameModelInfoImp(bank, deck, chats, logs, map, players, tracker, version);

        if (!winner.equals(DEFAULT_PLAYER))
            modelInfo.setWinner(getIndexOfPlayer(winner));

        if (offer != null)
            modelInfo.setTradeOffer(offer);

        return modelInfo;
    }

    //Hidden Methods
    public void startGame()
    {
        game = new PlayableGameImp((PregameGame)game);
    }

    /**
     * Should be called whenever a road is added (from command classes?)
     */
    @Override
    public void roadAdded()
    {
        for (Player player : getPlayers())
        {
            int size = board.getLongestRoadSize(player);
            if (size > longestRoadLength)
            {
            	Player oldOwner = longestRoadOwner;
                longestRoadOwner = player;
                longestRoadLength = size;
                if(! oldOwner.equals(DEFAULT_PLAYER) && ! oldOwner.equals(player))
                {
                	((PlayableGame) game).setVictoryPoints(oldOwner, ((PlayableGame) game).getVictoryPoints(oldOwner) - 2);
                }
            }
        }
    }

    @Override
    public void armyAdded()
    {
        for (Player player : getPlayers())
        {
            int size = ((PlayableGame)game).getNumberOfArmiesPlayed(player);
            if (size > largestArmySize)
            {
            	Player oldOwner = largestArmyOwner;
                largestArmyOwner = player;
                largestArmySize = size;
                if(! oldOwner.equals(DEFAULT_PLAYER) && ! oldOwner.equals(player))
                {
                	((PlayableGame) game).setVictoryPoints(oldOwner, ((PlayableGame) game).getVictoryPoints(oldOwner) - 2);
                }
            }
        }
    }

    public void setWinner(Player winner)
    {
        this.winner = winner;
    }

    public int getIndexOfLongestRoad()
    {
        try
        {
            if (longestRoadLength >= 5)
                return getIndexOfPlayer(longestRoadOwner);
            else
                return DEFAULT_PLAYER_INDEX;
        }
        catch (PlayerNotInGameException ignore)
        {
            return DEFAULT_PLAYER_INDEX;
        }
    }

    public int getIndexOfLargestArmy()
    {
        try
        {
            if (largestArmySize >= 3)
                return getIndexOfPlayer(largestArmyOwner);
            else
                return DEFAULT_PLAYER_INDEX;
        }
        catch (PlayerNotInGameException ignore)
        {
            return DEFAULT_PLAYER_INDEX;
        }
    }

    //Helper Methods
    private BankInfo convertBank()
    {
        return BankFactory.convertBank(game);
    }

    private DeckInfo convertDeck()
    {
        return DeckFactory.convertDeck(game);
    }

    private ChatInfo convertChats()
    {
        return ChatLogFactory.convertChat(game);
    }

    private LogInfo convertLogs()
    {
        return ChatLogFactory.convertLog(game);
    }

    private MapInfo convertMap() throws PlayerNotInGameException
    {
        return MapFactory.convertMap(this);
    }

    private List<PlayerInfo> convertPlayers() throws UninitializedDataException, PlayerNotInGameException
    {
        return PlayerFactory.convertPlayers(getPlayers(),this);
    }

    private TurnTrackerInfo convertTurnTracker()
    {
        return TurnTrackerFactory.convertTurnTracker(this);
    }

	@Override
	public void setGame(Game game) {
		this.game = game;
	}

	@Override
	public int checkWinner(Player player) throws PlayerNotInGameException
	{
		//Add up settlements, cities, roads, largest army, longest road, and monuments played
		int points = 0;
		if(longestRoadOwner.equals(player))
			points += 2;
		if(largestArmyOwner.equals(player))
			points+=2;
		BuildPool buildPool = ((PlayableGame)game).getBuildPoolOfPlayer(player);
		points += buildPool.getSettlementsPlayed();
		points += buildPool.getCitiesPlayed() * 2;
		points += ((PlayableGame)game).getNumberOfMonumentsPlayed(player);

		if(points >= 10)
			setWinner(player);
		return points;
	}
	@Override
	public int compareTo(GameModel game) {
		//System.out.println(this.getID());
		//System.out.println(game.getID());
		if(this.getID() < game.getID())
			return -1;
		else if(this.getID() > game.getID())
			return 1;
		else
			return 0;
	}
}
