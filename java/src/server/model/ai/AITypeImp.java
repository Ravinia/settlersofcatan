package server.model.ai;

/**
 * @author Lawrence
 */
public enum AITypeImp implements AIType
{
    DUMB_AI("dumb"),
    SMART_AI("smart");

    private String type;

    AITypeImp(String type)
    {
        this.type = type;
    }

    @Override
    public String getAIType()
    {
        return type;
    }
}
