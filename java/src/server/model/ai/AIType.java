package server.model.ai;

/**
 * Current carrying object for AI types
 * @author Lawrence
 */
public interface AIType
{
    /**
     * The string value of the AI type that should show up in the list of available AIs
     * @return the AI Type
     */
    public String getAIType();
}
