package server.model.ai;

/**
 * @author Lawrence
 */
public class AITypeMock implements AIType
{
    private String type;

    public AITypeMock(String type)
    {
        this.type = type;
    }

    @Override
    public String getAIType()
    {
        return type;
    }
}
