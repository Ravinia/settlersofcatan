package server.model;

import server.courier.serializer.GameModelInfo;
import server.exceptions.InvalidIndexException;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.board.Board;
import server.model.game.Game;
import server.model.player.Player;
import shared.definitions.CatanColor;

import java.util.List;

/**
 * @author Eric
 * @author Lawrence
 */
public interface GameModel
{
    /**
     * Gets the Game associated with this game model
     * If the game has started, it will return a PlayableGame,
     * if it has not started it will return a PregameGame
     * @return Game object for this model
     */
    public Game getGame();

    /**
     * Gets the Board object associated with this game model
     * If the game has not been started, it will not have any placeables
     * @return the Board object for this game model
     */
    public Board getBoard();

    /**
     * Whether or not the game has already been started.
     * This function is important for casting the game to a PregameGame or a PlayableGame
     *
     * @return true if all four players have joined and the game has started, false if the game has been created but
     * there are not sufficient players in the game.
     */
    public boolean isGameStarted();

    /**
     * Returns the currently selected color for a given server.model.player
     * @param player the server.model.player to ask the color of
     * @return current color of server.model.player
     * @throws PlayerNotInGameException
     */
    public CatanColor getColorOfPlayer(Player player) throws PlayerNotInGameException;

    /**
     * Checks to see if a given version is the same version as the current version on the model
     * If true, the server.handler should not send back the entire model but rather simply return "True"
     */
    public boolean isSameVersion(int version);
    
    public int getID();

    public List<Player> getPlayers();

    public int getIndexOfPlayer(Player player) throws PlayerNotInGameException;

    public GameModelInfo toCourier() throws PlayerNotInGameException, UninitializedDataException, NoCurrentTradesException;

	public Player getPlayerByIndex(int index) throws InvalidIndexException;

	public abstract void setGame(Game game);

	public int checkWinner(Player player) throws PlayerNotInGameException;

	public void roadAdded();

	public void armyAdded();
}
