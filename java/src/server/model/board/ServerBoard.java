package server.model.board;


import server.model.board.placeable.*;
import shared.exceptions.*;
import server.exceptions.PlaceablesNotInitializedException;
import server.model.board.immutable.*;
import server.model.board.immutable.ports.Port;
import server.model.player.Player;
import shared.definitions.HexType;
import shared.definitions.PortType;
import shared.locations.*;

import java.io.Serializable;
import java.util.*;

/**
 * Server's implementation of the Board object.
 * @author Lawrence
 */
public class ServerBoard implements Board, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 7693957768825946818L;
	private ValidLocationDistributor distributor = new ValidLocationDistributor();
    private Map<HexLocation, Hex> hexBoard = new HashMap<HexLocation, Hex>();
    private Map<PortLocation, Port> ports = new HashMap<PortLocation, Port>();
    private Map<EdgeLocation, Road> roads  = new HashMap<EdgeLocation, Road>();
    private Map<VertexLocation, Settlement> settlements = new HashMap<VertexLocation, Settlement>();
    private HexLocation robberLocation = null;

    public ServerBoard(boolean randomHexes, boolean randomPorts, boolean randomTokens)
            throws InvalidLocationException, InvalidObjectTypeException, InvalidPortTypeException, NotInitializedException
    {
        Collection<Immutable> boardPieces = BoardInitializer.generateBoard(this, randomHexes, randomPorts, randomTokens);
        initializeBoard(boardPieces);
        robberLocation = getDesertLocation();
    }

    private void initializeBoard(Collection<Immutable> boardPieces) throws InvalidLocationException, InvalidObjectTypeException
    {
        for (Immutable piece : boardPieces)
        {
            handleImmutable(piece);
        }
    }

    @Override
    public Hex getHex(HexLocation hex) throws InvalidLocationException
    {
        if (!distributor.isValidHexLocation(hex)) //is outside the map
        {
            throw new InvalidLocationException(hex, "The location given is outside the bounds of the board");
        }
        else if (hexBoard.containsKey(hex))  //is a land hex
        {
            return hexBoard.get(hex);
        }
        else                            //is a water hex
        {
            return new HexTile(hex, HexType.WATER, null, this);
        }
    }

    @Override
    public Edge getEdge(EdgeLocation edge) throws InvalidLocationException
    {
        if (!distributor.isValidEdge(edge))
            throw new InvalidLocationException(edge);

        return new EdgeTile(edge, this);
    }

    @Override
    public Vertex getVertex(VertexLocation vertex) throws InvalidLocationException
    {
        if (!distributor.isValidVertex(vertex))
            throw new InvalidLocationException(vertex);

        return new VertexTile(vertex, this);
    }

    @Override
    public Token getToken(HexLocation hex) throws InvalidLocationException
    {
        Token token = getHex(hex).getToken();
        if (token == null)
            throw new InvalidLocationException(hex, "There is no token on this location");
        else
            return token;
    }

    @Override
    public boolean hasSettlement(VertexLocation vertex)
    {
        return settlements.containsKey(vertex);
    }

    @Override
    public Settlement getSettlement(VertexLocation vertex) throws InvalidLocationException, PlaceablesNotInitializedException
    {
        if (distributor.isValidVertex(vertex))
        {
            if (settlements.containsKey(vertex))
                return settlements.get(vertex);
            else
                throw new InvalidLocationException(vertex, "There is no settlement at this location");
        }
        else
            throw new InvalidLocationException(vertex);
    }

    @Override
    public boolean hasRoad(EdgeLocation edge)
    {
        return roads.containsKey(edge);
    }

    @Override
    public Road getRoad(EdgeLocation edge) throws InvalidLocationException, PlaceablesNotInitializedException
    {
        if (distributor.isValidEdge(edge))
        {
            if (roads.containsKey(edge))
                return roads.get(edge);
            else
                throw new InvalidLocationException(edge, "There is no road at this location");
        }
        else
            throw new InvalidLocationException(edge);
    }

    @Override
    public Hex getNeighboringHex(Hex hex, EdgeDirection direction) throws InvalidLocationException
    {
        HexLocation neighborLoc = hex.getNeighborLocation(direction);
        if (!distributor.isValidHexLocation(neighborLoc))
            throw new InvalidLocationException(hex.getLocation(), "The location given is outside the bounds of the board");
        return getHex(neighborLoc);
    }

    @Override
    public HexLocation getRobberLocation()
    {
        return robberLocation;
    }

    @Override
    public boolean isLandHex(HexLocation location)
    {
        return distributor.isLandHex(location);
    }

    @Override
    public Collection<PortType> getPlayersPorts(Player player) throws InvalidLocationException
    {
        Set<PortLocation> locations = this.ports.keySet();
        Collection<PortType> thisPlayerPorts = new ArrayList<>();
        for(PortLocation i: locations)
        {
            if(this.ports.get(i).getOwner() != null)
            {
                if(this.ports.get(i).getOwner() == player)
                {
                    thisPlayerPorts.add(this.ports.get(i).getPortType());
                }
            }
        }

        return thisPlayerPorts;
    }

    @Override
    public Port getPort(PortLocation port) throws InvalidLocationException
    {
        try
        {
            return ports.get(port);
        }
        catch (Exception e)
        {
            throw new InvalidLocationException(e);
        }
    }

    @Override
    public int getLongestRoadSize(Player player)
    {
        Collection<Road> playersRoads = getPlayersRoads(player);
        int best = 0;
        for (Road road : playersRoads)
        {
            RoadPath path = new RoadPath(road.getLocation());
            best = Math.max(best, getLongestRoadFromThisLocation(path, player));
        }
        return best;
    }

    @Override
    public Collection<Road> getRoads()
    {
        return roads.values();
    }

    @Override
    public Collection<Port> getPorts()
    {
        return ports.values();
    }

    @Override
    public Collection<Settlement> getSettlements()
    {
        return settlements.values();
    }

    @Override
    public Collection<Hex> getHexes()
    {
        return hexBoard.values();
    }
    
    @Override
    public Collection<Hex> getHexesByTokenValue(int value)
    {
    	Collection<Hex> ret = new ArrayList<Hex>();
    	for(Hex h : hexBoard.values())
    	{
    		Token token = h.getToken();
    		if (token == null)
    			continue;
    			
    		if (token.getValue() == value)
    			ret.add(h);
    	}
    	return ret;
    }

    //Update Methods
    @Override
    public void updateBoardObjects(Collection<Placeable> boardPieces) throws InvalidObjectTypeException, InvalidRobberLocationException
    {
        for (Placeable piece : boardPieces)
        {
            handlePlacable(piece);
        }
    }

    @Override
    public void updateBoardObjects(Placeable boardPiece) throws InvalidObjectTypeException, InvalidRobberLocationException
    {
        List<Placeable> placeables = new ArrayList<>();
        placeables.add(boardPiece);
        updateBoardObjects(placeables);
    }

    //Hidden Methods
    public Hex getDesertHex()
    {
        for (Hex hex : hexBoard.values())
        {
            if (hex.getType() == HexType.DESERT)
                return hex;
        }
        return null;
    }

    public Collection<EdgeLocation> getEdgesWithRoad(Player player, Collection<EdgeLocation> edgeLocations)
    {
        List<EdgeLocation> locations = new ArrayList<>();
        for (EdgeLocation location : edgeLocations)
        {
            try
            {
                Edge edge = getEdge(location);
                if (edge.hasRoad(player))
                    locations.add(location);
            }
            catch (InvalidLocationException | PlaceablesNotInitializedException ignore)
            { }
        }
        return locations;
    }

    public Collection<EdgeLocation> getValidAdjacentEdges(RoadPath path)
    {
        EdgeLocation location = path.currentEdge();
        List<EdgeLocation> edges = new ArrayList<>();
        try
        {
            Edge edge = getEdge(location);
            Collection<Edge> neighbors = ((EdgeTile)edge).getAdjacentEdges();
            for (Edge neighbor : neighbors)
            {
                try
                {
                    if (path.isValidDirection(neighbor.getLocation()))
                        edges.add(neighbor.getLocation());
                }
                catch (NullPointerException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        catch (InvalidLocationException ex)
        {
            ex.printStackTrace();
        }
        return edges;
    }

    public int getNumberOfSettlements()
    {
        return settlements.size();
    }

    //Helper Methods
    private int getLongestRoadFromThisLocation(RoadPath path, Player player)
    {
        Collection<EdgeLocation> edges = getValidAdjacentEdges(path);
        edges = getEdgesWithRoad(player, edges);

        if (edges.size() == 0)
            return path.length();

        int best = 0;
        for (EdgeLocation location : edges)
        {
            if (!path.contains(location))   //avoid loops
            {
                RoadPath newPath = path.getNewPath(location);
                best = Math.max(best, getLongestRoadFromThisLocation(newPath, player));
            }
        }

        return Math.max(best, path.length());
    }

    private Collection<Road> getPlayersRoads(Player player)
    {
        List<Road> playersRoads = new ArrayList<>();
        for (Road road : roads.values())
        {
            if (road.getPlayer().equals(player))
            {
                playersRoads.add(road);
            }
        }
        return playersRoads;
    }

    private HexLocation getDesertLocation() throws NotInitializedException
    {
        try
        {
            return getDesertHex().getLocation();
        }
        catch (NullPointerException e)
        {
            throw new NotInitializedException("Desert hex not initialized on board", e);
        }
    }

    private void handlePlacable(Placeable piece) throws InvalidObjectTypeException, InvalidRobberLocationException
    {
        switch (piece.getType())
        {
            case ROBBER:
                if (robberLocation.equals(piece.getLocation()))
                    throw new InvalidRobberLocationException(((HexLocation)piece.getLocation()));
                robberLocation = (HexLocation) piece.getLocation();
                break;
            case ROAD:
                EdgeLocation roadLoc = (EdgeLocation)piece.getLocation();
                roadLoc = roadLoc.getNormalizedLocation();
                roads.put(roadLoc, (Road)piece);
                break;
            case SETTLEMENT:
                VertexLocation settlementLoc = (VertexLocation)piece.getLocation();
                settlementLoc = settlementLoc.getNormalizedLocation();
                settlements.put(settlementLoc, (Settlement)piece);
                break;
            case CITY:
                VertexLocation cityLoc = (VertexLocation)piece.getLocation();
                cityLoc = cityLoc.getNormalizedLocation();
                settlements.put(cityLoc, (Settlement)piece);
                break;
            default:
                throw new InvalidObjectTypeException(piece);
        }
    }

    private void handleImmutable(Immutable piece) throws InvalidObjectTypeException
    {
        if (piece instanceof Hex)
        {
            Hex hex = (Hex) piece;
            hexBoard.put(hex.getLocation(), hex);
        }
        else if (piece instanceof Port)
        {
            Port port = (Port) piece;
            ports.put(port.getLocation(), port);
        }
        else
            throw new InvalidObjectTypeException(piece);
    }

	public List<Hex> getHexesAdjacentTo(VertexLocation location) throws InvalidLocationException
	{
		VertexDirection dir = location.getDir();
		List<Hex> hexes = new ArrayList<Hex>();
		hexes.add(getHex(location.getHexLoc()));
		hexes.add(getHex(location.getHexLoc().getNeighborLoc(EdgeDirection.North)));
		switch(dir)
		{
		case NorthWest:
			hexes.add(getHex(location.getHexLoc().getNeighborLoc(EdgeDirection.NorthWest)));
			break;
		case NorthEast:
			hexes.add(getHex(location.getHexLoc().getNeighborLoc(EdgeDirection.NorthEast)));
		}
		
		return hexes;
	}
}
