package server.model.board.placeable;

import shared.locations.HexLocation;

/**
 * Interface for robber piece. Used for updating the position of the robber on the board.
 * @author Lawrence
 */
public interface Robber extends Placeable
{
    @Override
    HexLocation getLocation();
}
