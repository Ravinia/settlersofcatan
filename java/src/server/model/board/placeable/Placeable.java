package server.model.board.placeable;

import server.model.player.Player;
import shared.definitions.PieceType;
import shared.locations.Location;

public interface Placeable
{
    /**
     * Returns the piece type of the placable object
     * @return the piece type
     */
    public PieceType getType();

    /**
     * Returns a uniquely identifying location object.
     * This object will need to be casted to its proper type to use.
     * @return the location of the piece
     */
    public Location getLocation();

    /**
     * Returns the server.model.player that this object belongs to.
     * Returns null if the piece is the robber.
     * @return server.model.player that owns this piece
     */
    public Player getPlayer();
}
