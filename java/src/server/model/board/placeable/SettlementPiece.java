package server.model.board.placeable;

import java.io.Serializable;

import server.model.player.Player;
import shared.definitions.PieceType;
import shared.locations.VertexLocation;

/**
 * Implementation of the settlement piece
 * @author Lawrence
 */
public class SettlementPiece implements Settlement, Serializable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = -7051652761776006398L;
	private VertexLocation location;
    Player owner;

    public SettlementPiece(VertexLocation location, Player owner)
    {
        this.location = location;
        this.owner = owner;
    }

    @Override
    public VertexLocation getLocation()
    {
        return location;
    }

    @Override
    public boolean isCity()
    {
        return false;
    }

    @Override
    public int getVictoryPoints()
    {
        return 1;
    }

    @Override
    public PieceType getType()
    {
        return PieceType.SETTLEMENT;
    }

    @Override
    public Player getPlayer()
    {
        return owner;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((owner == null) ? 0 : owner.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SettlementPiece other = (SettlementPiece) obj;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        if (owner == null) {
            if (other.owner != null)
                return false;
        } else if (!owner.equals(other.owner))
            return false;
        return true;
    }
}
