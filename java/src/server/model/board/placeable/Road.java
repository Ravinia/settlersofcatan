package server.model.board.placeable;

import shared.locations.EdgeLocation;

/**
 * Road interface for a placeable road
 * @author Lawrence
 */
public interface Road extends Placeable
{
    public EdgeLocation getLocation();
}
