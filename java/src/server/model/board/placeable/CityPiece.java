package server.model.board.placeable;

import java.io.Serializable;

import server.model.player.Player;
import shared.definitions.PieceType;
import shared.locations.VertexLocation;

/**
 * Implementation of the City piece
 * @author Lawrence
 */
public class CityPiece extends SettlementPiece implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 7798062400286812849L;

	public CityPiece(VertexLocation location, Player owner)
    {
        super(location, owner);
    }

    @Override
    public PieceType getType()
    {
        return PieceType.CITY;
    }

    @Override
    public boolean isCity()
    {
        return true;
    }

    @Override
    public int getVictoryPoints()
    {
        return 2;
    }
}
