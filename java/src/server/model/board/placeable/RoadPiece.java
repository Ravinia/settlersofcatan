package server.model.board.placeable;

import java.io.Serializable;

import server.model.player.Player;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;

/**
 * Implementation of the Road interface
 * @author Lawrence
 */
public class RoadPiece implements Road, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 771012462975531330L;
	private EdgeLocation location;
    Player owner;

    public RoadPiece(EdgeLocation location, Player owner)
    {
        this.location = location;
        this.owner = owner;
    }

    @Override
    public PieceType getType()
    {
        return PieceType.ROAD;
    }

    @Override
    public EdgeLocation getLocation()
    {
        return location;
    }

    @Override
    public Player getPlayer()
    {
        return owner;
    }
}
