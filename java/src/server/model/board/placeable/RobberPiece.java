package server.model.board.placeable;

import java.io.Serializable;

import server.model.player.Player;
import shared.definitions.PieceType;
import shared.locations.HexLocation;

/**
 * Implementation of the Robber
 * @author Lawrence
 */
public class RobberPiece implements Robber, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5140128736302458201L;
	HexLocation location;

    public RobberPiece(HexLocation location)
    {
        this.location = location;
    }

    @Override
    public PieceType getType()
    {
        return PieceType.ROBBER;
    }

    @Override
    public HexLocation getLocation()
    {
        return location;
    }

    @Override
    public Player getPlayer()
    {
        return null;
    }
}
