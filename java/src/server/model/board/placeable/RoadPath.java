package server.model.board.placeable;

import shared.exceptions.InvalidLocationException;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains a list of road objects.
 * This class is used to help calculate the longest road.
 * @author Lawrence
 */
public class RoadPath implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -3692217407279121878L;
	private List<EdgeLocation> roadPath = new ArrayList<>();

    //Constructors
    public RoadPath(EdgeLocation location)
    {
        roadPath.add(location);
    }

    private RoadPath(List<EdgeLocation> locations)
    {
        roadPath = new ArrayList<>(locations);
    }

    //Methods
    public int length()
    {
        return roadPath.size();
    }

    public RoadPath getNewPath(EdgeLocation location)
    {
        List<EdgeLocation> newPath = getRoadPath();
        newPath.add(location);
        return new RoadPath(newPath);
    }

    public EdgeLocation previousEdge() throws InvalidLocationException
    {
        if (roadPath.size() < 2)
            throw new InvalidLocationException(currentEdge(), "There is only one edge in this path");
        return roadPath.get(roadPath.size() - 2);
    }

    public EdgeLocation currentEdge()
    {
        return roadPath.get(roadPath.size() - 1);
    }

    public boolean contains(EdgeLocation location)
    {
        return roadPath.contains(location);
    }

    public boolean isValidDirection(EdgeLocation destination)
    {
        EdgeLocation current = currentEdge();
        HexLocation hex;
        EdgeLocation loc1;
        EdgeLocation loc2;
        switch (getDirection())
        {
            case West:
                hex = current.getHexLoc();
                loc1 = new EdgeLocation(hex, EdgeDirection.NorthWest);
                loc2 = new EdgeLocation(hex.getNeighborLoc(EdgeDirection.NorthWest), EdgeDirection.NorthEast);
                return destination.equals(loc1) || destination.equals(loc2);
            case East:
                hex = current.getHexLoc();
                loc1 = new EdgeLocation(hex, EdgeDirection.NorthEast);
                loc2 = new EdgeLocation(hex.getNeighborLoc(EdgeDirection.NorthEast), EdgeDirection.NorthWest);
                return destination.equals(loc1) || destination.equals(loc2);
            case NorthEast:
                hex = current.getHexLoc();
                loc1 = new EdgeLocation(hex, EdgeDirection.North);
                loc2 = new EdgeLocation(hex.getNeighborLoc(EdgeDirection.NorthWest), EdgeDirection.NorthEast);
                return destination.equals(loc1) || destination.equals(loc2);
            case SouthWest:
                hex = current.getHexLoc().getNeighborLoc(EdgeDirection.SouthWest);
                loc1 = new EdgeLocation(hex, EdgeDirection.North);
                loc2 = new EdgeLocation(hex, EdgeDirection.NorthEast);
                return destination.equals(loc1) || destination.equals(loc2);
            case NorthWest:
                hex = current.getHexLoc();
                loc1 = new EdgeLocation(hex, EdgeDirection.North);
                loc2 = new EdgeLocation(hex.getNeighborLoc(EdgeDirection.NorthEast), EdgeDirection.NorthWest);
                return destination.equals(loc1) || destination.equals(loc2);
            case SouthEast:
                hex = current.getHexLoc().getNeighborLoc(EdgeDirection.SouthEast);
                loc1 = new EdgeLocation(hex, EdgeDirection.North);
                loc2 = new EdgeLocation(hex, EdgeDirection.NorthWest);
                return destination.equals(loc1) || destination.equals(loc2);
            case None:
                return true;
            default:
                return false;
        }
    }

    //Helper Methods
    public String toString()
    {
        return "Size: " + length() + " Dir: " + getDirection().toString() + " " + currentEdge().toString();
    }

    private PathDirection getDirection()
    {
        return PathDirection.getPathDirection(this);
    }

    private List<EdgeLocation> getRoadPath()
    {
        return new ArrayList<>(roadPath);
    }
}
