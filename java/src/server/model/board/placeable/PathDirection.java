package server.model.board.placeable;

import shared.exceptions.InvalidLocationException;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;

import java.io.Serializable;

/**
 * Special enum class used for determining which direction a path is going, and which edges are valid as adjacent edges
 * @author Lawrence
 */
public enum PathDirection implements Serializable
{
    East,
    West,
    NorthEast,
    SouthEast,
    NorthWest,
    SouthWest,
    None;

    public static PathDirection getPathDirection(RoadPath path)
    {
        if (path.length() <= 1)
            return None;

        if (curr(path) == EdgeDirection.North)
        {
            HexLocation hex = loc(path).getHexLoc();
            if (prev(path).equals(new EdgeLocation(hex, EdgeDirection.NorthWest))
                    || prev(path).equals(new EdgeLocation(hex.getNeighborLoc(EdgeDirection.NorthWest), EdgeDirection.NorthEast)))
            {
                return East;
            }
            else
                return West;
        }
        else if (curr(path) == EdgeDirection.NorthEast)
        {
            HexLocation hex = loc(path).getHexLoc();
            if (prev(path).equals(new EdgeLocation(hex, EdgeDirection.North))
                    || prev(path).equals(new EdgeLocation(hex.getNeighborLoc(EdgeDirection.NorthEast), EdgeDirection.NorthWest)))
            {
                return SouthEast;
            }
            else
                return NorthWest;
        }
        else // if (curr(path) == EdgeDirection.NorthWest)
        {
            HexLocation hex = loc(path).getHexLoc();
            if (prev(path).equals(new EdgeLocation(hex, EdgeDirection.North))
                    || prev(path).equals(new EdgeLocation(hex.getNeighborLoc(EdgeDirection.NorthWest), EdgeDirection.NorthEast)))
            {
                return SouthWest;
            }
            else
                return NorthEast;
        }
    }

    //Helper Methods
    private static EdgeLocation loc(RoadPath path)
    {
        return path.currentEdge().getNormalizedLocation();
    }

    private static EdgeLocation prev(RoadPath path)
    {
        try
        {
            return path.previousEdge().getNormalizedLocation();
        }
        catch (InvalidLocationException e)
        {
            return null;
        }
    }

    private static EdgeDirection curr(RoadPath path)
    {
        return path.currentEdge().getNormalizedLocation().getDir();
    }
}
