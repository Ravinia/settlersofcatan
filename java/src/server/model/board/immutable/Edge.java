package server.model.board.immutable;

import server.exceptions.PlaceablesNotInitializedException;
import server.model.player.Player;
import shared.exceptions.InvalidLocationException;
import shared.locations.EdgeLocation;

import java.util.List;

/**
 * Interface for board edge object
 * @author Lawrence
 */
public interface Edge extends Immutable
{
    public boolean adjacentToSettlement() throws InvalidLocationException;

    /**
     * Whether or not the specified server.model.player owns a road on any of the edges adjacent to this edge
     * @param player the server.model.player
     * @return true if they do, false otherwise
     */
    public boolean ownsAnAdjacentRoad(Player player) throws PlaceablesNotInitializedException;

    /**
     * Checks to see if a given edge location is adjacent to the current edge
     * @param location edge location of the edge
     * @return true if the specified location is adjacent to this edge, false otherwise
     * @throws InvalidLocationException
     */
    public boolean isAdjacentToEdge(EdgeLocation location) throws InvalidLocationException;

    /**
     * Returns true if the specified server.model.player owns a road on this edge
     * @param player the server.model.player
     * @return false if there is no road on this location, or if there is a road, but the road does not belong to the server.model.player
     */
    public boolean hasRoad(Player player) throws PlaceablesNotInitializedException;

    /**
     * Returns whether or not there is a road on this edge
     * @return true if there is a road belonging to any server.model.player on this edge, false otherwise
     */
    public boolean hasRoad();

    /**
     * Gets the two vertices that surround this edge
     * @return a list of two vertices
     */
    public List<Vertex> getTwoAdjacentVertices() throws InvalidLocationException;

    /**
     * Gets the edge's location
     * @return the EdgeLocation of the edge
     */
    public EdgeLocation getLocation();
}
