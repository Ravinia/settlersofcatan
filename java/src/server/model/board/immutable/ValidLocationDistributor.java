package server.model.board.immutable;

import shared.locations.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Helper class that specifies which locations are valid map locations
 * @author Lawrence
 */
public class ValidLocationDistributor implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -3140430499588166413L;
	private Set<HexLocation> landHexes;
    private Set<HexLocation> waterHexes;

    private Set<EdgeLocation> edges;
    private Set<VertexLocation> vertices;

    /**
     * Initializes valid locations. These values should always be the same
     */
    public ValidLocationDistributor()
    {
        landHexes = new HashSet<HexLocation>();
        waterHexes = new HashSet<HexLocation>();
        edges = new HashSet<EdgeLocation>();
        vertices= new HashSet<VertexLocation>();


        //initialize landHexes
        landHexes.add(new HexLocation(0, -2));
        landHexes.add(new HexLocation(0, -1));
        landHexes.add(new HexLocation(0, 0));
        landHexes.add(new HexLocation(0, 1));
        landHexes.add(new HexLocation(0, 2));
        landHexes.add(new HexLocation(-1, -1));
        landHexes.add(new HexLocation(-1, 0));
        landHexes.add(new HexLocation(-1, 1));
        landHexes.add(new HexLocation(-1, 2));
        landHexes.add(new HexLocation(-2, 0));
        landHexes.add(new HexLocation(-2, 1));
        landHexes.add(new HexLocation(-2, 2));
        landHexes.add(new HexLocation(1, -2));
        landHexes.add(new HexLocation(1, -1));
        landHexes.add(new HexLocation(1, 0));
        landHexes.add(new HexLocation(1, 1));
        landHexes.add(new HexLocation(2, -2));
        landHexes.add(new HexLocation(2, -1));
        landHexes.add(new HexLocation(2, 0));

        //initialize waterHexes
        waterHexes.add(new HexLocation(0, -3));
        waterHexes.add(new HexLocation(1, -3));
        waterHexes.add(new HexLocation(2, -3));
        waterHexes.add(new HexLocation(3, -3));
        waterHexes.add(new HexLocation(3, -2));
        waterHexes.add(new HexLocation(3, -1));
        waterHexes.add(new HexLocation(3, 0));
        waterHexes.add(new HexLocation(2, 1));
        waterHexes.add(new HexLocation(1, 2));
        waterHexes.add(new HexLocation(0, 3));
        waterHexes.add(new HexLocation(-1, 3));
        waterHexes.add(new HexLocation(-2, 3));
        waterHexes.add(new HexLocation(-3, 3));
        waterHexes.add(new HexLocation(-3, 2));
        waterHexes.add(new HexLocation(-3, 1));
        waterHexes.add(new HexLocation(-3, 0));
        waterHexes.add(new HexLocation(-2, -1));
        waterHexes.add(new HexLocation(-1, -2));


        for (HexLocation hex : landHexes)
        {
            //add all vertices
            for (VertexDirection dir : VertexDirection.values())
            {
                vertices.add(new VertexLocation(hex, dir).getNormalizedLocation());
            }
            //add all edges
            for (EdgeDirection dir : EdgeDirection.values())
            {
                edges.add(new EdgeLocation(hex, dir).getNormalizedLocation());
            }
        }

    }

    public boolean isValidHexLocation(Hex hex)
    {
        return landHexes.contains(hex.getLocation()) || waterHexes.contains(hex.getLocation());
    }

    public boolean isValidHexLocation(HexLocation hex)
    {
        return landHexes.contains(hex) || waterHexes.contains(hex);
    }

    public boolean isLandHex(HexLocation hex)
    {
        return landHexes.contains(hex);
    }

    public boolean isWaterHex(HexLocation hex)
    {
        if (waterHexes.contains(hex))
            return true;
        else
            return false;
    }

    public boolean isValidEdge(EdgeLocation edge)
    {
        edge = edge.getNormalizedLocation();
        return edges.contains(edge);

    }

    public boolean isValidVertex(VertexLocation vertex)
    {
        vertex = vertex.getNormalizedLocation();
        return vertices.contains(vertex);
    }

    protected Set<VertexLocation> getVertices()
    {
        return vertices;
    }

    protected Set<EdgeLocation> getEdges()
    {
        return edges;
    }
}
