package server.model.board.immutable.ports;

import server.model.board.Board;
import shared.definitions.PortType;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidPortTypeException;
import shared.locations.PortLocation;

/**
 * Factory for converting raw information into a Port object
 * @author Lawrence
 */
public class PortFactory
{
    private Board board;

    public PortFactory(Board board)
    {
        this.board = board;
    }


    public Port convert(PortType type,PortLocation location) throws InvalidPortTypeException, InvalidLocationException
    {
        Port port;
        switch (type)
        {
            case WOOD:
                port = new WoodPort(board, location);
                break;
            case BRICK:
                port = new BrickPort(board, location);
                break;
            case SHEEP:
                port = new SheepPort(board, location);
                break;
            case WHEAT:
                port = new WheatPort(board, location);
                break;
            case ORE:
                port = new OrePort(board, location);
                break;
            case THREE:
                port = new GenericPort(board, location);
                break;
            default:
                throw new InvalidPortTypeException(type);
        }
        return port;
    }

    private static PortType getPortType(String type) throws InvalidPortTypeException
    {
        if (type == null)
            return PortType.THREE;
        type = type.toLowerCase();
        type = type.trim();
        if (type.equals("wood"))
            return PortType.WOOD;
        else if (type.equals("wheat"))
            return PortType.WHEAT;
        else if (type.equals("brick"))
            return PortType.BRICK;
        else if (type.equals("sheep"))
            return PortType.SHEEP;
        else if (type.equals("ore"))
            return PortType.ORE;
        else if (type.equals("three") || type.equals("generic"))
            return PortType.THREE;
        else
            throw new InvalidPortTypeException(type);
    }
}
