package server.model.board.immutable.ports;


import java.io.Serializable;

import server.model.board.Board;
import shared.definitions.PortType;
import shared.exceptions.InvalidLocationException;
import shared.locations.PortLocation;

public class GenericPort extends AbstractPort implements Port, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -659533663122141078L;

	public GenericPort(Board board, PortLocation location) throws InvalidLocationException
    {
        super(board, location);
        this.setRatio(3);
    }

	@Override
	public PortType getPortType()
    {
		return PortType.THREE;
	}
}
