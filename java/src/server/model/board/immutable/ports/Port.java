package server.model.board.immutable.ports;

import server.model.board.immutable.Immutable;
import server.model.player.Player;
import shared.definitions.PortType;
import shared.exceptions.InvalidLocationException;
import shared.locations.PortLocation;

/**
 * @author Lawrence
 * @author Eric
 */
public interface Port extends Immutable
{
    /**
     * Gets the type of port this is
     * @return the port type
     */
	public PortType getPortType();

    /**
     * Returns the two vertices that this port is on
     * @return the two vertices to which this port corresponds to
     */
    public PortLocation getLocation();

    /**
     * Returns the Player object of the server.model.player (if any) who has built a settlement on this port
     * @return null if no Player owns this port
     */
    public Player getOwner() throws InvalidLocationException;

    /**
     * Whether or not the specified server.model.player owns this port
     * @param player the server.model.player to check against
     * @return true if they do, false otherwise
     */
    public boolean ownsPort(Player player) throws InvalidLocationException;

    /**
     * Gets the port's trade ratio.
     * Ex: if the ratio were 3, it would be a 3:1 port
     * @return the ratio
     */
    public int ratio();
}
