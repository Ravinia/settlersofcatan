package server.model.board.immutable.ports;

import java.io.Serializable;

import server.model.board.Board;
import shared.definitions.PortType;
import shared.exceptions.InvalidLocationException;
import shared.locations.PortLocation;

public class WheatPort extends AbstractPort implements Port, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -4690592441711994739L;

	public WheatPort(Board board, PortLocation location) throws InvalidLocationException
    {
        super(board, location);
    }

	@Override
	public PortType getPortType()
    {
		return PortType.WHEAT;
	}
}
