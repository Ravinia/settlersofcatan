package server.model.board.immutable.ports;

import java.io.Serializable;

import server.model.board.Board;
import shared.definitions.PortType;
import shared.exceptions.InvalidLocationException;
import shared.locations.PortLocation;

public class WoodPort extends AbstractPort implements Port, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 3362819234183547613L;

	public WoodPort(Board board, PortLocation location) throws InvalidLocationException
    {
        super(board, location);
    }

	@Override
	public PortType getPortType()
    {
		return PortType.WOOD;
	}
}
