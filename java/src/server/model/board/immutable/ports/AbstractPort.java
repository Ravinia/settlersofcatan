package server.model.board.immutable.ports;

import server.exceptions.PlaceablesNotInitializedException;
import server.model.board.Board;
import server.model.board.immutable.Vertex;
import server.model.player.Player;
import shared.exceptions.InvalidLocationException;
import shared.locations.PortLocation;

import java.io.Serializable;

/**
 * Abstract port class for containing some of the common functions of the port objects
 */
public abstract class AbstractPort implements Port, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9091200232650440005L;
	private Board board;
    private PortLocation location = null;
    private int ratio = 2;

    public AbstractPort(Board board, PortLocation location)
    {
        this.board = board;
        this.location = location;

        //TODO: tell vertexes to set ports?
    }

    @Override
    public PortLocation getLocation()
    {
        return location;
    }

    @Override
    public Player getOwner() throws InvalidLocationException
    {
        try
        {
            Vertex vertex1 = board.getVertex(location.getVertex1());
            Vertex vertex2 = board.getVertex(location.getVertex2());

            if (vertex1.hasSettlement())
                return board.getSettlement(vertex1.getLocation()).getPlayer();
            else if (vertex2.hasSettlement())
                return board.getSettlement(vertex2.getLocation()).getPlayer();
            else
                return null;
        }
        catch (PlaceablesNotInitializedException ignore)
        {
            return null;
        }
    }


    @Override
    public boolean ownsPort(Player player) throws InvalidLocationException
    {
        return getOwner() == player;
    }

    @Override
    public int ratio()
    {
        return ratio;
    }

    protected void setRatio(int ratio)
    {
        this.ratio = ratio;
    }
    
    @Override
    public boolean equals(Object o)
    {
    	if (!(o instanceof Port))
    		return false;
    	Port port = (Port)o;
    	return this.location.equals(port.getLocation());
    }

}
