package server.model.board.immutable;

import server.exceptions.PlaceablesNotInitializedException;
import server.model.board.Board;
import server.model.player.Player;
import shared.definitions.HexType;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidLocationException;
import shared.locations.EdgeDirection;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * Implementation of hex interface
 * @author Lawrence
 */
public class HexTile implements Hex, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -7229361820541647640L;
	private HexLocation location;
    private HexType type;
    private Token token;
    private Board board;

    public HexTile(HexLocation location, HexType type, Token token, Board board)
    {
        this.location = location;
        this.type = type;
        this.token = token;
        this.board = board;
    }

    @Override
    public HexLocation getNeighborLocation(EdgeDirection direction)
    {
        return location.getNeighborLoc(direction);
    }

    @Override
    public Collection<Player> getPlayersOnThisHex() throws InvalidLocationException, PlaceablesNotInitializedException
    {
        Collection<Player> players = new HashSet<Player>();
        Collection<Vertex> vertices = getVertices();
        for (Vertex vertex : vertices)
        {
            if (vertex.hasSettlement())
            {
                Player player = board.getSettlement(vertex.getLocation()).getPlayer();
                players.add(player);
            }
        }
        return players;
    }
    
    @Override
    public Collection<VertexLocation> getSettlementsByThisHex() throws InvalidLocationException
    {
    	Collection<VertexLocation> locations = new ArrayList<VertexLocation>();
    	Collection<Vertex> vertices = getVertices();
    	for(Vertex vertex : vertices)
    	{
    		if(vertex.hasSettlement())
    			locations.add(vertex.getLocation());
    	}
    	return locations;
    }

    @Override
    public HexLocation getLocation()
    {
        return location;
    }

    @Override
    public HexType getType()
    {
        return type;
    }
    
    @Override public ResourceType getResourceType()
    {
    	return ResourceType.getResourceFromHex(type);
    }

    @Override
    public Token getToken()
    {
        return token;
    }

    //Helper Methods
    private Collection<Vertex> getVertices() throws InvalidLocationException
    {
        Collection<Vertex> vertices = new ArrayList<Vertex>();
        Collection<VertexLocation> locations = new ArrayList<VertexLocation>();
        for (VertexDirection direction : VertexDirection.values())
        {
            VertexLocation loc = new VertexLocation(this.location, direction);
            loc = loc.getNormalizedLocation();
            locations.add(loc);
        }
        for (VertexLocation loc : locations)
        {
            vertices.add(board.getVertex(loc));
        }
        return vertices;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((board == null) ? 0 : board.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HexTile other = (HexTile) obj;
		if (board == null) {
			if (other.board != null)
				return false;
		} else if (!board.equals(other.board))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

}
