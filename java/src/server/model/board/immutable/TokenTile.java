package server.model.board.immutable;

import java.io.Serializable;

/**
 * @author Lawrence
 */
public class TokenTile implements Token, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 2512886731352076955L;
	private int tokenValue;

    public TokenTile(int tokenValue)
    {
        this.tokenValue = tokenValue;
    }

    public int getValue()
    {
        return tokenValue;
    }

    @Override
    public int getFrequency()
    {
        return 0;
    }
}
