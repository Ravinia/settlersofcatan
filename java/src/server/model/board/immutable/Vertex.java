package server.model.board.immutable;

import server.exceptions.DoesNotHavePortException;
import server.exceptions.PlaceablesNotInitializedException;
import server.model.board.immutable.ports.Port;
import server.model.player.Player;
import shared.locations.VertexLocation;

/**
 * Interface for board vertex object
 * @author Lawrence
 */
public interface Vertex extends Immutable
{
    /**
     * Checks if there is a settlement on any of the neighboring vertices.
     * @return true if there is, false otherwise
     */
    public boolean hasNeighboringSettlement();

    /**
     * Checks if the server.model.player owns a road on any of the adjacent edges to this vertex.
     * @param player The server.model.player to check if they own any adjacent roads
     * @return true if there is, false otherwise
     */
    public boolean ownsAdjacentRoad(Player player) throws PlaceablesNotInitializedException;

    /**
     * Whether or not there is a settlement at this vertex
     * @return true if there is a settlement belonging to any server.model.player at this vertex
     */
    public boolean hasSettlement();

    /**
     * Whether or not this vertex has a port on it
     * @return true if it does, false otherwise
     */
    public boolean hasPort();

    /**
     * Gets the vertex location of this vertex
     * @return the vertex location
     */
    public VertexLocation getLocation();

    /**
     * Gets the port object associated with this vertex
     * @return the port object
     */
    public Port getPort() throws DoesNotHavePortException;

    /**
     * Sets a reference to a port that this vertex corresponds to after the vertex has already been created
     * @param port the port
     */
    public void setPort(Port port);
}
