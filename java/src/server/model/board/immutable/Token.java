package server.model.board.immutable;

/**
 * Interface for board token object
 * @author Lawrence
 */
public interface Token extends Immutable
{
    /**
     * Gets the numerical value on this token
     * @return number value (2 - 12)
     */
    public int getValue();

    /**
     * Gets the frequency or likelihood of this token being rolled out of a normal two-die roll
     * @return int frequency
     */
    public int getFrequency();
}
