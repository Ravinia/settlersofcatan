package server.model.board.immutable;

import server.exceptions.PlaceablesNotInitializedException;
import server.model.player.Player;
import shared.definitions.HexType;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidLocationException;
import shared.locations.EdgeDirection;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;

import java.util.Collection;

/**
 * Interface for the Hex immutable object
 * @author Lawrence
 */
public interface Hex extends Immutable
{
    /**
     * Returns the hex location value of the hex's neighbor on the specified side
     * @param direction which side of the hex to look for its neighbor
     * @return the hex location of the neighbor. This value might be an invalid location.
     */
    public HexLocation getNeighborLocation(EdgeDirection direction);

    /**
     * Gets all of the players who have a settlement on this hex
     * @return set of all the players
     * @throws shared.exceptions.InvalidLocationException
     */
    public Collection<Player> getPlayersOnThisHex() throws InvalidLocationException, PlaceablesNotInitializedException;

    /**
     * @return the location
     */
    public HexLocation getLocation();

    /**
     * @return the type of Hex
     */
    public HexType getType();

    /**
     * @return the token
     */
    public Token getToken();

	public Collection<VertexLocation> getSettlementsByThisHex() throws InvalidLocationException;

	public ResourceType getResourceType();

}
