package server.model.board.immutable;

import server.exceptions.PlaceablesNotInitializedException;
import server.model.board.Board;
import server.model.board.placeable.Road;
import server.model.player.Player;
import shared.exceptions.InvalidLocationException;
import shared.locations.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Lawrence
 */
public class EdgeTile extends AdjacentEdgeFinder implements Edge, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -7208070067285702459L;
	private EdgeLocation location;

    /**
     * A valid edge on a game board
     * @param location edge location of the edge
     * @param board the parent board that the edge is on
     */
    public EdgeTile(EdgeLocation location, Board board)
    {
        super(board);
        this.location = location;
    }

    @Override
    public boolean adjacentToSettlement() throws InvalidLocationException
    {
        List<Vertex> neighbors = getTwoAdjacentVertices();
        for (Vertex vertex : neighbors)
        {
            if (vertex.hasSettlement())
                return true;
        }
        return false;
    }

    @Override
    public boolean ownsAnAdjacentRoad(Player player) throws PlaceablesNotInitializedException
    {
        Collection<Edge> edges = getAdjacentEdges();
        for (Edge edge : edges)
        {
            if (edge.hasRoad(player))
                return true;
        }
        return false;
    }

    @Override
    public boolean isAdjacentToEdge(EdgeLocation location) throws InvalidLocationException
    {
        Edge neighbor = board.getEdge(location);
        Collection<Edge> edges = ((EdgeTile)neighbor).getAdjacentEdges();
        //iterate through all of the neighbors of the proposed edge, and if any of them are this edge, return true
        for (Edge edge : edges)
        {
            if (edge.getLocation().equals(this.location))
                return true;
        }
        return false;
    }

    @Override
    public boolean hasRoad(Player player) throws PlaceablesNotInitializedException
    {
        try
        {
            if (hasRoad())
            {
                Road road = board.getRoad(location);
                return road.getPlayer().equals(player);
            }
            else
                return false;
        }
        catch (InvalidLocationException ignore)
        {
            return false;
        }
    }

    @Override
    public boolean hasRoad()
    {
        return board.hasRoad(location);
    }

    @Override
    public List<Vertex> getTwoAdjacentVertices() throws InvalidLocationException
    {
        HexLocation hex = location.getHexLoc();
        EdgeDirection direction = location.getDir();
        List<VertexLocation> vertices = new ArrayList<VertexLocation>();
        if (direction == EdgeDirection.North)
        {
            vertices.add(new VertexLocation(hex, VertexDirection.NorthEast));
            vertices.add(new VertexLocation(hex, VertexDirection.NorthWest));
        }
        else if (direction == EdgeDirection.NorthEast)
        {
            vertices.add(new VertexLocation(hex, VertexDirection.NorthEast));
            vertices.add((new VertexLocation(hex, VertexDirection.East)).getNormalizedLocation());
        }
        else //direction == NorthWest
        {
            vertices.add((new VertexLocation(hex, VertexDirection.West)).getNormalizedLocation());
            vertices.add(new VertexLocation(hex, VertexDirection.NorthWest));
        }

        List<Vertex> result = new ArrayList<Vertex>();
        result.add(board.getVertex(vertices.get(0)));
        result.add(board.getVertex(vertices.get(1)));

        return result;
    }

    @Override
    public EdgeLocation getLocation()
    {
        return location;
    }

    //Helper Methods
    public Collection<Edge> getAdjacentEdges()
    {
        EdgeLocation loc = location.getNormalizedLocation();
        Collection<Edge> neighbors = new ArrayList<Edge>();
        HexLocation hex = loc.getHexLoc();

        if (loc.getDir() == EdgeDirection.North)
        {
            addAdjacentEdge(neighbors, hex, EdgeDirection.NorthEast);
            addAdjacentEdge(neighbors, hex, EdgeDirection.NorthWest);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.North), EdgeDirection.SouthEast);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.North), EdgeDirection.SouthWest);
        }
        else if (loc.getDir() == EdgeDirection.NorthWest)
        {
            addAdjacentEdge(neighbors, hex, EdgeDirection.North);
            addAdjacentEdge(neighbors, hex, EdgeDirection.SouthWest);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthWest), EdgeDirection.NorthEast);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthWest), EdgeDirection.South);
        }
        else    //EdgeDirection.NorthEast
        {
            addAdjacentEdge(neighbors, hex, EdgeDirection.North);
            addAdjacentEdge(neighbors, hex, EdgeDirection.SouthEast);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthEast), EdgeDirection.NorthWest);
            addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthEast), EdgeDirection.South);
        }
        return neighbors;
    }

    @Override
    public boolean equals(Object edge)
    {
        if (edge instanceof Edge)
        {
            Edge e = (Edge) edge;
            return e.getLocation().equals(location);
        }
        else
            return false;
    }

    public String toString()
    {
        return location.toString();
    }
}
