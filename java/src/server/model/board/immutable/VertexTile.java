package server.model.board.immutable;

import server.exceptions.PlaceablesNotInitializedException;
import server.model.board.Board;
import server.model.board.immutable.ports.Port;
import server.model.player.Player;
import shared.exceptions.InvalidLocationException;
import shared.locations.EdgeDirection;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Lawrence
 */
public class VertexTile extends AdjacentEdgeFinder implements Vertex, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -447433482414678530L;
	private VertexLocation location;
    private Port port = null;

    /**
     * A valid vertex on a game board
     * @param location the vertex location of the vertex
     * @param board the parent board that the vertex is on
     */
    public VertexTile(VertexLocation location, Board board)
    {
        super(board);
        this.location = location;
    }

    //Public Methods
    /**
     * Checks if there is a settlement on any of the neighboring vertices.
     * @return true if there is, false otherwise
     */
    public boolean hasNeighboringSettlement()
    {
        Collection<Vertex> neighbors = getNeighboringVertices();
        for (Vertex neighbor : neighbors)
        {
            if (neighbor.hasSettlement())
                return true;
        }
        return false;
    }

    /**
     * Checks if the server.model.player owns a road on any of the adjacent edges to this vertex.
     * @param player The server.model.player to check if they own any adjacent roads
     * @return true if there is, false otherwise
     */
    public boolean ownsAdjacentRoad(Player player) throws PlaceablesNotInitializedException
    {
        Collection<Edge> edges = getNeighboringEdges();
        for (Edge edge : edges)
        {
            if (edge.hasRoad(player))
                return true;
        }
        return false;
    }

    //Helper Methods

    /**
     * Whether or not there is a settlement at this vertex
     * @return true if there is a settlement belonging to any server.model.player at this vertex
     */
    public boolean hasSettlement()
    {
        return board.hasSettlement(location);
    }

    /**
     * Whether or not this vertex has a port on it
     * @return true if it does, false otherwise
     */
    public boolean hasPort()
    {
        return port == null;
    }

    private Collection<Vertex> getNeighboringVertices()
    {
        VertexLocation loc = this.getLocation().getNormalizedLocation();
        Collection<Vertex> neighbors = new ArrayList<Vertex>();
        HexLocation hex = loc.getHexLoc();

        // Note: modifying this to also add the vertex being tested. This will prevent
        // settlements from being placed on top of settlements.
        if (loc.getDir() == VertexDirection.NorthEast)
        {
            neighbors = addNeighbor(neighbors, hex, VertexDirection.NorthEast);
            neighbors = addNeighbor(neighbors, hex, VertexDirection.NorthWest);
            neighbors = addNeighbor(neighbors, hex.getNeighborLoc(EdgeDirection.SouthEast), VertexDirection.NorthWest);
            neighbors = addNeighbor(neighbors, hex.getNeighborLoc(EdgeDirection.NorthEast), VertexDirection.NorthWest);
            return neighbors;
        }
        else // Northwest case
        {
            neighbors = addNeighbor(neighbors, hex, VertexDirection.NorthWest);
            neighbors = addNeighbor(neighbors, hex, VertexDirection.NorthEast);
            neighbors = addNeighbor(neighbors, hex.getNeighborLoc(EdgeDirection.SouthWest), VertexDirection.NorthEast);
            neighbors = addNeighbor(neighbors, hex.getNeighborLoc(EdgeDirection.NorthWest), VertexDirection.NorthEast);
            return neighbors;
        }
    }

    private Collection<Edge> getNeighboringEdges()
    {
        VertexLocation loc = this.getLocation().getNormalizedLocation();
        Collection<Edge> neighbors = new ArrayList<Edge>();
        HexLocation hex = loc.getHexLoc();
        if (loc.getDir() == VertexDirection.NorthWest)
        {
            neighbors = addAdjacentEdge(neighbors, hex, EdgeDirection.North);
            neighbors = addAdjacentEdge(neighbors, hex, EdgeDirection.NorthWest);
            neighbors = addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthWest), EdgeDirection.NorthEast);

            return neighbors;
        }
        else // NorthEast vertex case
        {
            neighbors = addAdjacentEdge(neighbors, hex, EdgeDirection.North);
            neighbors = addAdjacentEdge(neighbors, hex, EdgeDirection.NorthEast);
            neighbors = addAdjacentEdge(neighbors, hex.getNeighborLoc(EdgeDirection.NorthEast), EdgeDirection.NorthWest);
            return neighbors;
        }
    }

    private Collection<Vertex> addNeighbor(Collection<Vertex> neighbors, HexLocation hex, VertexDirection direction)
    {
        Vertex neighbor = getNeighbor(hex, direction);
        if (neighbor != null)
            neighbors.add(neighbor);
        return neighbors;
    }

    private Vertex getNeighbor(HexLocation hex, VertexDirection direction)
    {
        try
        {
            return board.getVertex(new VertexLocation(hex, direction));
        }
        catch (InvalidLocationException ignore)
        {
            return null;
        }
    }

    //Getters
    public VertexLocation getLocation()
    {
        return location;
    }

    public Port getPort()
    {
        return port;
    }

    //Setters
    public void setPort(Port port)
    {
        this.port = port;
    }
}
