package server.model.board;

import server.exceptions.PlaceablesNotInitializedException;
import server.model.board.immutable.*;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.PortLocation;
import shared.definitions.PortType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import server.model.board.immutable.ports.Port;
import server.model.board.placeable.*;
import server.model.player.Player;

import java.util.Collection;

/**
 * @author Lawrence
 */
public interface Board
{
    /**
     * Retrieves the hex at a specified location
     * @param hex the HexLocation object that specifies the unique location of the hex
     * @return the Hex object
     */
    public Hex getHex(HexLocation hex) throws InvalidLocationException;

    /**
     * Retrieves a unique edge at a specified location.
     * Does not accept water edges or edges outside the map
     * @param edge the location of the edge to get
     * @return the Edge object
     */
    public Edge getEdge(EdgeLocation edge) throws InvalidLocationException;

    /**
     * Retrieves a unique vertex at a specified location
     * Does not accept water vertices or vertices outside of the map
     * @param vertex the location of the vertex to get
     * @return the Vertex object
     */
    public Vertex getVertex(VertexLocation vertex) throws InvalidLocationException;

    /**
     * Retrieves the token for a specified hex
     * @param hex the hex to get the token of
     * @return the Token object
     */
    public Token getToken(HexLocation hex) throws InvalidLocationException;

    /**
     * Whether or not there is a settlement on this vertex
     * @param vertex the location to look for a settlement
     * @return true if there is, false otherwise
     */
    public boolean hasSettlement(VertexLocation vertex);

    /**
     * Retrieves a settlement at a specified vertex
     * @param vertex the vertex where the settlement should be
     * @return the Settlement object
     */
    public Settlement getSettlement(VertexLocation vertex) throws InvalidLocationException, PlaceablesNotInitializedException;

    /**
     * Whether or not there is a road placed on this edge
     * @param edge the edge where the road should be
     * @return true if there is, false otherwise
     */
    public boolean hasRoad(EdgeLocation edge);

    /**
     * Retrieves a road at a specified edge
     * @param edge the edge where the road should be
     * @return the Road object
     */
    public Road getRoad(EdgeLocation edge) throws InvalidLocationException, PlaceablesNotInitializedException;

    /**
     * Returns the specified neighbor of a hex
     * @param hex the hex
     * @param direction the direction of the neighbor
     * @return the neighboring hex
     */
    public Hex getNeighboringHex(Hex hex, EdgeDirection direction) throws InvalidLocationException;

    /**
     * Returns the hex location of the hex that the robber is at
     * @return location of the robber
     */
    public HexLocation getRobberLocation();

    /**
     * Checks whether the given location is a valid land hex
     * @param location the hex location to test
     * @return true if it is, false otherwise
     */
    public boolean isLandHex(HexLocation location);

    /**
     * Gets all the ports a given server.model.player is on
     * @param player the server.model.player
     * @return collection of port types
     * @throws InvalidLocationException
     */
    public Collection<PortType> getPlayersPorts(Player player) throws InvalidLocationException;

    /**
     * Gets the specified port
     * @param port the location of the port to get
     * @return the port
     * @throws InvalidLocationException
     */
    public Port getPort(PortLocation port) throws InvalidLocationException;

    /**
     * Returns the longest road that this server.model.player has
     * @param player the server.model.player to check
     * @return the longest possible road size of this server.model.player
     */
    public int getLongestRoadSize(Player player);

    public Collection<Road> getRoads();

    public Collection<Port> getPorts();

    public Collection<Settlement> getSettlements();

    public Collection<Hex> getHexes();

    /**
     * Updates all the movable board pieces on the board
     * @param boardPieces a collection of pieces that represent the current state of the board
     */
    public void updateBoardObjects(Collection<Placeable> boardPieces) throws InvalidObjectTypeException, InvalidRobberLocationException;

    /**
     * Updates a single movable board piece on the board
     * @param boardPiece a board piece to add to the board
     */
    public void updateBoardObjects(Placeable boardPiece) throws InvalidObjectTypeException, InvalidRobberLocationException;

	public Collection<Hex> getHexesByTokenValue(int value);
}
