package server.model.board;

import server.model.board.immutable.Hex;
import server.model.board.immutable.HexTile;
import server.model.board.immutable.Immutable;
import server.model.board.immutable.Token;
import server.model.board.immutable.ports.Port;
import server.model.board.immutable.ports.PortFactory;
import shared.definitions.HexType;
import shared.definitions.PortType;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidPortTypeException;
import shared.locations.HexLocation;
import shared.locations.PortLocation;

import java.io.Serializable;
import java.util.*;

/**
 * Class that takes in the boolean parameter values for the board setup,
 * then returns a list of Immutables for initializing the board.
 * @author Lawrence
 */
public class BoardInitializer implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5578753685442805580L;

	public static Collection<Immutable> generateBoard(Board board, boolean randomHexes, boolean randomPorts, boolean randomTokens)
            throws InvalidLocationException, InvalidPortTypeException
    {
        List<Immutable> result = new ArrayList<>();

        List<HexLocation> locations = LocationConstants.getHexList();
        List<HexType> resources = getHexes(randomHexes);
        List<Token> tokens = getTokens(randomTokens);

        List<PortLocation> portLocations = LocationConstants.getPortLocationsList();
        List<PortType> portTypes = getPorts(randomPorts);

        ListIterator<Token> it = tokens.listIterator();
        for (int i = 0; i < locations.size(); i++)
        {
            HexLocation location = locations.get(i);
            HexType resource = resources.get(i);


            Token token;
            if (resource == HexType.DESERT)
            {
                token = null;
            }
            else
            {
                token = it.next();
            }

            Hex tile = new HexTile(location, resource, token, board);
            result.add(tile);
        }

        for (int i = 0; i < portLocations.size(); i++)
        {
            PortType type = portTypes.get(i);
            PortLocation location = portLocations.get(i);
            PortFactory factory = new PortFactory(board);
            Port port = factory.convert(type, location);
            result.add(port);
        }

        return result;
    }

    private static List<HexType> getHexes(boolean shuffled)
    {
        List<HexType> hexes = LocationConstants.getHexTypeList();
        if (!shuffled)
            return hexes;
        else
        {
            Collections.shuffle(hexes);
            return hexes;
        }
    }

    private static List<Token> getTokens(boolean shuffled)
    {
        List<Token> tokens = LocationConstants.getTokenList();
        if (!shuffled)
            return tokens;
        else
        {
            Collections.shuffle(tokens);
            return tokens;
        }
    }

    private static List<PortType> getPorts(boolean shuffled)
    {
        List<PortType> ports = LocationConstants.getPortTypeList();
        if (!shuffled)
            return ports;
        else
        {
            Collections.shuffle(ports);
            return ports;
        }
    }
}
