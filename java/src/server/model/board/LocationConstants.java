package server.model.board;

import shared.definitions.HexType;
import shared.definitions.PortType;
import shared.locations.*;

import server.model.board.immutable.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class that contains some common constants to be used in setup and testing.
 *
 * @author Lawrence
 */
public abstract class LocationConstants
{
    public static final HexLocation CENTER_HEX = new HexLocation(0, 0);
    public static final HexLocation HEX_0_N1 = new HexLocation(0, -1);
    public static final HexLocation HEX_0_N2 = new HexLocation(0, -2);
    public static final HexLocation HEX_0_1 = new HexLocation(0, 1);
    public static final HexLocation HEX_0_2 = new HexLocation(0, 2);
    public static final HexLocation HEX_1_N2 = new HexLocation(1, -2);
    public static final HexLocation HEX_1_N1 = new HexLocation(1, -1);
    public static final HexLocation HEX_1_0 = new HexLocation(1, 0);
    public static final HexLocation HEX_1_1 = new HexLocation(1, 1);
    public static final HexLocation HEX_2_N2 = new HexLocation(2, -2);
    public static final HexLocation HEX_2_N1 = new HexLocation(2, -1);
    public static final HexLocation HEX_2_0 = new HexLocation(2, 0);
    public static final HexLocation HEX_N1_N1 = new HexLocation(-1, -1);
    public static final HexLocation HEX_N1_0 = new HexLocation(-1, 0);
    public static final HexLocation HEX_N1_1 = new HexLocation(-1, 1);
    public static final HexLocation HEX_N1_2 = new HexLocation(-1, 2);
    public static final HexLocation HEX_N2_0 = new HexLocation(-2, 0);
    public static final HexLocation HEX_N2_1 = new HexLocation(-2, 1);
    public static final HexLocation HEX_N2_2 = new HexLocation(-2, 2);

    public static final HexLocation DESERT_HEX = HEX_0_2;

    public static final HexLocation NORTH_WATER_HEX = new HexLocation(0, -3);
    public static final HexLocation WATER_N1_N2 = new HexLocation(-1, -2);
    public static final HexLocation WATER_N2_N1 = new HexLocation(-2, -1);
    public static final HexLocation WATER_N3_0 = new HexLocation(-3, 0);
    public static final HexLocation WATER_N3_1 = new HexLocation(-3, 1);
    public static final HexLocation WATER_N3_2 = new HexLocation(-3, 2);
    public static final HexLocation WATER_N3_3 = new HexLocation(-3, 3);
    public static final HexLocation WATER_N2_3 = new HexLocation(-2, 3);
    public static final HexLocation WATER_N1_3 = new HexLocation(-1, 3);
    public static final HexLocation WATER_0_3 = new HexLocation(0, 3);
    public static final HexLocation WATER_1_2 = new HexLocation(1, 2);
    public static final HexLocation WATER_2_1 = new HexLocation(2, 1);
    public static final HexLocation WATER_3_0 = new HexLocation(3, 0);
    public static final HexLocation WATER_3_N1 = new HexLocation(3, -1);
    public static final HexLocation WATER_3_N2 = new HexLocation(3, -2);
    public static final HexLocation WATER_3_N3 = new HexLocation(3, -3);
    public static final HexLocation WATER_2_N3 = new HexLocation(2, -3);
    public static final HexLocation WATER_1_N3 = new HexLocation(1, -3);

    public static final EdgeLocation CENTER_N_EDGE = new EdgeLocation(CENTER_HEX, EdgeDirection.North);
    public static final EdgeLocation CENTER_NE_EDGE = new EdgeLocation(CENTER_HEX, EdgeDirection.NorthEast);
    public static final EdgeLocation CENTER_NW_EDGE = new EdgeLocation(CENTER_HEX, EdgeDirection.NorthWest);
    public static final EdgeLocation EDGE_N1_0_NE = new EdgeLocation(HEX_N1_0, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_1_N1_NW = new EdgeLocation(HEX_1_N1, EdgeDirection.NorthWest);

    public static final EdgeLocation EDGE_0_N2_N = new EdgeLocation(HEX_0_N2, EdgeDirection.North);
    public static final EdgeLocation EDGE_0_N2_NE = new EdgeLocation(HEX_0_N2, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_0_N2_NW = new EdgeLocation(HEX_0_N2, EdgeDirection.NorthWest);

    public static final EdgeLocation EDGE_1_0_N = new EdgeLocation(HEX_1_0, EdgeDirection.North);
    public static final EdgeLocation EDGE_1_0_NE = new EdgeLocation(HEX_1_0, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_1_0_NW = new EdgeLocation(HEX_1_0, EdgeDirection.NorthWest);

    public static final EdgeLocation EDGE_0_1_N = new EdgeLocation(HEX_0_1, EdgeDirection.North);
    public static final EdgeLocation EDGE_0_N1_N = new EdgeLocation(HEX_0_N1, EdgeDirection.North);
    public static final EdgeLocation EDGE_1_1_N = new EdgeLocation(HEX_1_1, EdgeDirection.North);
    public static final EdgeLocation EDGE_N1_1_N = new EdgeLocation(HEX_N1_1, EdgeDirection.North);
    public static final EdgeLocation EDGE_N1_1_NE = new EdgeLocation(HEX_N1_1, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_N2_0_NE = new EdgeLocation(HEX_N2_0, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_N2_2_NE = new EdgeLocation(HEX_N2_2, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_1_N1_NE = new EdgeLocation(HEX_1_N1, EdgeDirection.NorthEast);
    public static final EdgeLocation EDGE_2_N1_NE = new EdgeLocation(HEX_2_N1, EdgeDirection.NorthEast);
    public static final EdgeLocation INVALID_EDGE = new EdgeLocation(NORTH_WATER_HEX, EdgeDirection.North);

    
    public static final VertexLocation CENTER_NE_VERTEX = new VertexLocation(CENTER_HEX, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_N1_1_NE = new VertexLocation(HEX_N1_1, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_1_N1_NE = new VertexLocation(HEX_1_N1, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_2_N1_NE = new VertexLocation(HEX_2_N1, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_1_1_NE = new VertexLocation(HEX_1_1, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_N1_2_NW = new VertexLocation(HEX_N1_2, VertexDirection.NorthWest);
    public static final VertexLocation VERTEX_N2_0_NE = new VertexLocation(HEX_N2_0, VertexDirection.NorthEast);
    public static final VertexLocation VERTEX_0_N1_NW = new VertexLocation(HEX_0_N1, VertexDirection.NorthWest);
    public static final VertexLocation INVALID_VERTEX = new VertexLocation(NORTH_WATER_HEX, VertexDirection.NorthEast);

    /**
     * List of all of the hex locations for the board.
     * Defines the location order pattern important for the board.
     * @return ordered list of locations
     */
    public static List<HexLocation> getHexList()
    {
        List<HexLocation> hexes = new ArrayList<>();

        //Spiral Order:
        hexes.add(HEX_2_N2);
        hexes.add(HEX_1_N2);
        hexes.add(HEX_0_N2);

        hexes.add(HEX_N1_N1);
        hexes.add(HEX_N2_0);

        hexes.add(HEX_N2_1);
        hexes.add(HEX_N2_2);

        hexes.add(HEX_N1_2);
        hexes.add(HEX_0_2);

        hexes.add(HEX_1_1);
        hexes.add(HEX_2_0);

        hexes.add(HEX_2_N1);
        hexes.add(HEX_1_N1);

        hexes.add(HEX_0_N1);
        hexes.add(HEX_N1_0);

        hexes.add(HEX_N1_1);
        hexes.add(HEX_0_1);

        hexes.add(HEX_1_0);
        hexes.add(CENTER_HEX);
        return hexes;
    }

    /**
     * List of all of the tokens for the board.
     * Follows the location order pattern.
     * @return ordered list of tokens. There is no desert token
     */
    public static List<Token> getTokenList()
    {
        List<Token> tokens = new ArrayList<>();

        tokens.add(new TokenTile(6));
        tokens.add(new TokenTile(3));
        tokens.add(new TokenTile(8));
        tokens.add(new TokenTile(10));
        tokens.add(new TokenTile(9));
        tokens.add(new TokenTile(12));
        tokens.add(new TokenTile(11));
        tokens.add(new TokenTile(4));
        //DESERT HEX
        tokens.add(new TokenTile(8));
        tokens.add(new TokenTile(5));
        tokens.add(new TokenTile(2));
        tokens.add(new TokenTile(9));
        tokens.add(new TokenTile(4));
        tokens.add(new TokenTile(5));
        tokens.add(new TokenTile(6));
        tokens.add(new TokenTile(3));
        tokens.add(new TokenTile(10));
        tokens.add(new TokenTile(11));

        return tokens;
    }

    public static List<HexType> getHexTypeList()
    {
        List<HexType> types = new ArrayList<>();

        //Spiral pattern
        types.add(HexType.WOOD);
        types.add(HexType.ORE);
        types.add(HexType.WHEAT);

        types.add(HexType.SHEEP);
        types.add(HexType.WHEAT);

        types.add(HexType.SHEEP);
        types.add(HexType.WOOD);

        types.add(HexType.BRICK);
        types.add(HexType.DESERT);

        types.add(HexType.BRICK);
        types.add(HexType.ORE);
        types.add(HexType.WHEAT);

        types.add(HexType.SHEEP);
        types.add(HexType.WOOD);

        types.add(HexType.BRICK);
        types.add(HexType.ORE);

        types.add(HexType.WOOD);
        types.add(HexType.SHEEP);
        types.add(HexType.WHEAT);

        return types;
    }

    public static List<PortType> getPortTypeList()
    {
        List<PortType> ports = new ArrayList<>();

        //Port 1
        ports.add(PortType.WOOD);

        //Port 2
        ports.add(PortType.THREE);

        //Port 3
        ports.add(PortType.WHEAT);

        //Port 4
        ports.add(PortType.ORE);

        //Port 5
        ports.add(PortType.THREE);

        //Port 6
        ports.add(PortType.SHEEP);

        //Port 7
        ports.add(PortType.THREE);

        //Port 8
        ports.add(PortType.THREE);

        //Port 9
        ports.add(PortType.BRICK);

        return ports;
    }

    public static List<PortLocation> getPortLocationsList()
    {
        List<PortLocation> ports = new ArrayList<>();

        //Port 1
        int x = -3;
        int y = 2;
        EdgeDirection dir = EdgeDirection.NorthEast;
        VertexDirection vDir1 = VertexDirection.NorthEast;
        VertexDirection vDir2 = VertexDirection.East;

        HexLocation hex = new HexLocation(x,y);
        EdgeLocation edge = new EdgeLocation(hex, dir);

        VertexLocation v1 = new VertexLocation(hex, vDir1);
        v1 = v1.getNormalizedLocation();

        VertexLocation v2 = new VertexLocation(hex, vDir2);
        v2 = v2.getNormalizedLocation();

        ports.add(new PortLocation(edge, v1, v2));

        //Port 2
        x = -4;
        y = 0;
        dir = EdgeDirection.SouthEast;
        vDir1 = VertexDirection.East;
        vDir2 = VertexDirection.SouthEast;

        hex = new HexLocation(x,y);
        edge = new EdgeLocation(hex, dir);

        v1 = new VertexLocation(hex, vDir1);
        v1 = v1.getNormalizedLocation();

        v2 = new VertexLocation(hex, vDir2);
        v2 = v2.getNormalizedLocation();

        ports.add(new PortLocation(edge, v1, v2));

        //Port 3
        x = -1;
        y = -3;
        dir = EdgeDirection.South;
        vDir1 = VertexDirection.SouthEast;
        vDir2 = VertexDirection.SouthWest;

        hex = new HexLocation(x,y);
        edge = new EdgeLocation(hex, dir);

        v1 = new VertexLocation(hex, vDir1);
        v1 = v1.getNormalizedLocation();

        v2 = new VertexLocation(hex, vDir2);
        v2 = v2.getNormalizedLocation();

        ports.add(new PortLocation(edge, v1, v2));

        //Port 4
        x = 1;
        y = -4;
        dir = EdgeDirection.South;
        vDir1 = VertexDirection.SouthEast;
        vDir2 = VertexDirection.SouthWest;

        hex = new HexLocation(x,y);
        edge = new EdgeLocation(hex, dir);

        v1 = new VertexLocation(hex, vDir1);
        v1 = v1.getNormalizedLocation();

        v2 = new VertexLocation(hex, vDir2);
        v2 = v2.getNormalizedLocation();

        ports.add(new PortLocation(edge, v1, v2));

        //Port 5
        x = 4;
        y = -4;
        dir = EdgeDirection.SouthWest;
        vDir1 = VertexDirection.West;
        vDir2 = VertexDirection.SouthWest;

        hex = new HexLocation(x,y);
        edge = new EdgeLocation(hex, dir);

        v1 = new VertexLocation(hex, vDir1);
        v1 = v1.getNormalizedLocation();

        v2 = new VertexLocation(hex, vDir2);
        v2 = v2.getNormalizedLocation();

        ports.add(new PortLocation(edge, v1, v2));

        //Port 6
        x = 3;
        y = -1;
        dir = EdgeDirection.NorthWest;
        vDir1 = VertexDirection.West;
        vDir2 = VertexDirection.NorthWest;

        hex = new HexLocation(x,y);
        edge = new EdgeLocation(hex, dir);

        v1 = new VertexLocation(hex, vDir1);
        v1 = v1.getNormalizedLocation();

        v2 = new VertexLocation(hex, vDir2);
        v2 = v2.getNormalizedLocation();

        ports.add(new PortLocation(edge, v1, v2));

        //Port 7
        x = 2;
        y = 1;
        dir = EdgeDirection.NorthWest;
        vDir1 = VertexDirection.West;
        vDir2 = VertexDirection.NorthWest;

        hex = new HexLocation(x,y);
        edge = new EdgeLocation(hex, dir);

        v1 = new VertexLocation(hex, vDir1);
        v1 = v1.getNormalizedLocation();

        v2 = new VertexLocation(hex, vDir2);
        v2 = v2.getNormalizedLocation();

        ports.add(new PortLocation(edge, v1, v2));

        //Port 8
        x = 0;
        y = 3;
        dir = EdgeDirection.North;
        vDir1 = VertexDirection.NorthWest;
        vDir2 = VertexDirection.NorthEast;

        hex = new HexLocation(x,y);
        edge = new EdgeLocation(hex, dir);

        v1 = new VertexLocation(hex, vDir1);
        v1 = v1.getNormalizedLocation();

        v2 = new VertexLocation(hex, vDir2);
        v2 = v2.getNormalizedLocation();

        ports.add(new PortLocation(edge, v1, v2));

        //Port 9
        x = -2;
        y = 3;
        dir = EdgeDirection.NorthEast;
        vDir1 = VertexDirection.NorthEast;
        vDir2 = VertexDirection.East;

        hex = new HexLocation(x,y);
        edge = new EdgeLocation(hex, dir);

        v1 = new VertexLocation(hex, vDir1);
        v1 = v1.getNormalizedLocation();

        v2 = new VertexLocation(hex, vDir2);
        v2 = v2.getNormalizedLocation();

        ports.add(new PortLocation(edge, v1, v2));

        return ports;
    }
}
