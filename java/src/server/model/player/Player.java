package server.model.player;

import server.exceptions.UninitializedDataException;
import shared.player.PlayerIdentity;

/**
 * Stores the basic information for an individual server.model.player independent of any games the server.model.player may be associated with.
 * This class has less information stored in it than its client model counter-type since it only stores information
 * about the server.model.player themselves, where information about the server.model.player respective to a certain game is stored directly in
 * the Game class.
 *
 * @author Lawrence
 */
public interface Player
{
    /**
     * Gets the server.model.player's unique ID
     * Negative number below -1 represent an AI server.model.player
     * @return the server.model.player's ID
     */
    public int getId();

    /**
     * Returns the server.model.player's name
     * @return the server.model.player's name
     */
    public String getName() throws UninitializedDataException;

    /**
     * Retrieves a unique identity object based on the server.model.player's name and ID
     * @return the server.model.player's identity object
     */
    public PlayerIdentity getIdentity() throws UninitializedDataException;
}
