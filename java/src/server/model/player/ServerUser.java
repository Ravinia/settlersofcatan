package server.model.player;

/**
 * @author Lawrence
 */
public interface ServerUser
{
    /**
     * The username of the specified user
     * @return the username
     */
    public String getUsername();

    /**
     * The user's password
     * @return the user's password
     */
    public String getPassword();

    /**
     * Returns a cookie object that would be the expected cookie value for this user  upon login.
     * This is used to verify the cookie sent to the server, as well as to return to the client upon login.
     * @return the cookie object for the user
     */
    //public Cookie getLoginCookie();

	public int getUserID();
}
