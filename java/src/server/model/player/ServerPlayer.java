package server.model.player;

import java.io.Serializable;

import server.exceptions.UninitializedDataException;
import shared.player.PlayerIdentity;

/**
 * Implementation of the Player class
 * @author Lawrence
 */
public class ServerPlayer implements Player, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1302998211895573308L;
	private int id;
    private String name;

    public ServerPlayer(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    @Override
    public int getId()
    {
        return id;
    }

    @Override
    public String getName() throws UninitializedDataException
    {
        return name;
    }

    @Override
    public PlayerIdentity getIdentity()
    {
        return new PlayerIdentity(id, name);
    }

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		ServerPlayer other = (ServerPlayer) obj;
		if(id != other.id)
			return false;
		if(name == null)
		{
			if(other.name != null)
				return false;
		}
		else if(!name.equals(other.name))
			return false;
		return true;
	}

//    @Override
//    public boolean equals(Object o)
//    {
//        if (id == DEFAULT_PLAYER_INDEX)
//            return DEFAULT_PLAYER.equals(o);
//        else
//        {
//            if (o instanceof ServerPlayer)
//            {
//                try
//                {
//                    ServerPlayer player = (ServerPlayer) o;
//                    return getIdentity().equals(player.getIdentity());
//                }
//                catch (Exception e)
//                {
//                    return false;
//                }
//            }
//            else
//                return false;
//        }
//    }
}
