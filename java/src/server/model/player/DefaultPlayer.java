package server.model.player;

import java.io.Serializable;

import server.courier.serializer.PregamePlayerInfo;
import server.exceptions.UninitializedDataException;
import shared.definitions.CatanColor;
import shared.player.PlayerIdentity;

/**
 * Class for creating the default server.model.player object.
 * This is primarily useful for comparing suspected default server.model.player values to this using the equals function.
 * @author Lawrence
 */
public class DefaultPlayer implements Player, PregamePlayerInfo, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -4222238818959390808L;

	private DefaultPlayer()
    {}

    /**
     * Static reference to the default server.model.player whose id is -1
     */
    public static final DefaultPlayer DEFAULT_PLAYER = new DefaultPlayer();

    public static final int DEFAULT_PLAYER_INDEX = -1;

    /**
     * @return the standard ID for a default server.model.player: -1
     */
    @Override
    public int getId()
    {
        return DEFAULT_PLAYER_INDEX;
    }

    /**
     * Should not be called
     * @throws UninitializedDataException
     */
    @Override
    public String getName() throws UninitializedDataException
    {
        throw new UninitializedDataException("the name cannot be accessed on the default server.model.player");
    }

    @Override
    public CatanColor getColor() throws UninitializedDataException
    {
        throw new UninitializedDataException("the default server.model.player cannot have a color");
    }

    /**
     * Should not be called
     * @throws UninitializedDataException
     */
    @Override
    public PlayerIdentity getIdentity() throws UninitializedDataException
    {
        throw new UninitializedDataException("there cannot be an identity object for the default server.model.player");
    }

    /**
     * Overrides the equals function.
     *
     * @param o Player, PlayerInfo object to compare to
     * @return true if the given object representing a 'server.model.player' is functionally equivalent to the default server.model.player
     */
    @Override
    public boolean equals(Object o)
    {
        if (o instanceof PregamePlayerInfo)
        {
            PregamePlayerInfo player = (PregamePlayerInfo) o;
            return player.getId() == -1;
        }
        else if (o instanceof Player)
        {
            Player player = (Player) o;
            return player.getId() == -1;
        }
        return false;
    }
}
