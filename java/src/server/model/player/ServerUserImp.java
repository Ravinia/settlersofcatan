package server.model.player;

import java.io.Serializable;

public class ServerUserImp implements ServerUser, Serializable, Comparable<ServerUser>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1195773253664567616L;
	String username, password;
	int userID;
	
	public ServerUserImp(String username, String password, int userID)
	{
		this.username = username;
		this.password = password;
		this.userID = userID;
	}

	@Override
	public String getUsername()
	{
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public int getUserID()
	{
		return userID;
	}

	@Override
	public int compareTo(ServerUser arg0) {
		if(this.getUserID() < arg0.getUserID())
			return -1;
		else if(this.getUserID() > arg0.getUserID())
			return 1;
		else
			return 0;
	}

	/*@Override
	public Cookie getLoginCookie() {
		// TODO Auto-generated method stub
		return null;
	}*/

}
