package server.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import server.exceptions.CatanServerException;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.PasswordDoesNotMatchException;
import server.exceptions.UserDoesNotExistException;
import server.model.game.Game;
import server.model.game.PlayableGame;
import server.model.game.PlayableGameImp;
import server.model.game.PregameGame;
import server.model.player.ServerUser;
import server.model.player.ServerUserImp;
import shared.debug.LogLevel;

public class ServerModelImp implements ServerModel, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2715850292245614600L;
	private List<GameModel> gameModels;
	private List<ServerUser> users;
	private LogLevel logLevel;
	int nextGameID, nextUserID;
	
	public ServerModelImp()
	{
		nextGameID = 0;
		nextUserID = 0;
		gameModels = new ArrayList<GameModel>();
		users = new ArrayList<ServerUser>();
	}
	
	public ServerModelImp(List<GameModel> games, List<ServerUser> users)
	{
		int i;
		nextGameID = 0;
		nextUserID = 0;
		for(i = 0; i<games.size(); i++)
		{
			if(games.get(i).getID() > nextGameID)
				nextGameID = games.get(i).getID();
		}
		nextGameID++;
		for(i = 0; i<users.size(); i++)
		{
			if(users.get(i).getUserID() > nextUserID)
				nextUserID = users.get(i).getUserID();
		}
		nextUserID++;
		gameModels = games;
		this.users = users;
	}
	
	@Override
	public GameModel getGame(int gameID) throws GameDoesNotExistException {
		for(GameModel g : gameModels)
		{
			if(g.getID() == gameID)
				return g;
		}
		throw new GameDoesNotExistException(gameID);
	}
	public GameModel getGameByName(String name) throws GameDoesNotExistException
	{
		for(GameModel g : gameModels)
		{
			if(g.getGame().getName().equals(name))
				return g;
		}
		throw new GameDoesNotExistException(name);
	}
	@Override
	public List<Game> getGames() {
		List<Game> games = new ArrayList<Game>();
		for(int i = 0; i < gameModels.size(); i++)
		{
			games.add(gameModels.get(i).getGame());
		}
		return games;
	}
	
	@Override
	public PregameGame getPregameGame(int gameID) throws GameDoesNotExistException
	{
		for(GameModel g : gameModels)
		{
			if( g.getGame() instanceof PregameGame && g.getID() == gameID)
			{
				PregameGame temp = (PregameGame) g.getGame();
				return temp;
			}
		}
		throw new GameDoesNotExistException(gameID);
	}

	@Override
	public void changeLogLevel(LogLevel logLevel) {
		this.logLevel = logLevel;
	}

	/*
	 * Should create a new Game object as well as a new GameModel object and append them to the
	 * lists in this class, then return the Game object. Note: how is it supposed to figure out 
	 * the proper game ID to pick when the GameModel object is created?
	 */
	@Override
	public GameModel createGame(boolean randomTiles, boolean randomNumbers,
			boolean randomPorts, String gameName) throws CatanServerException
	{
		GameModel newGame = new GameModelImp(randomTiles, randomPorts, randomNumbers, nextGameID++, gameName);
		gameModels.add(newGame);
		return newGame;
	}
	
	/*
	 * Used to verify cookies.
	 */
	@Override
	public int getPlayerID(String username, String password) throws UserDoesNotExistException, PasswordDoesNotMatchException
	{
		for(ServerUser su : users)
		{
			if(su.getUsername().compareTo(username) == 0)
			{
				if(su.getPassword().compareTo(password) == 0)
					return su.getUserID();
				else
					throw new PasswordDoesNotMatchException(username, su.getPassword(), password);
			}
		}
		throw new UserDoesNotExistException(username);
	}
	
	/*
	 * Should make a new user with a new PlayerID, then return the PlayerID.
	 * 
	 * If the user already exists, return a -1 as the ID.
	 */
	@Override
	public ServerUser createUser(String username, String password)
	{
		for(ServerUser su : users)
		{
			if(su.getUsername().compareTo(username) == 0)
				return null;
		}
		ServerUser newUser = new ServerUserImp(username, password, nextUserID++);
		users.add(newUser);
		return newUser;
	}

	/*
	 * This method is called whenever a PregameGame reaches four players and needs 
	 * to be converted to a PlayableGame.
	 */
	@Override
	public void startGame(int gameID) throws GameDoesNotExistException
	{
		PregameGame g = this.getPregameGame(gameID);
		PlayableGame game = (PlayableGame) new PlayableGameImp(g);
		this.getGame(gameID).setGame(game);
	}
	public void addGameFromLoadCommand(GameModel model){
//		if(gameModels.contains(model))
//		{
//			int index = gameModels.indexOf(model);
//			gameModels.set(index, model);
//		}
//		else
//		{
//			gameModels.add(model);
//		}
		gameModels.add(model);
	}
	public void removeGameModel(GameModel model)
	{
		this.gameModels.remove(model);
	}
	public int giveValidID()
	{
		return nextGameID++;
	}

	public void setNextGameID(int nextGameID) {
		this.nextGameID = nextGameID;
	}

	public void setNextUserID(int nextUserID) {
		this.nextUserID = nextUserID;
	}
}
