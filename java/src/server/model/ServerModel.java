package server.model;

import server.exceptions.CatanServerException;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.PasswordDoesNotMatchException;
import server.exceptions.UserDoesNotExistException;
import server.model.game.Game;
import server.model.game.PregameGame;
import server.model.player.ServerUser;
import shared.debug.LogLevel;

import java.util.List;

/**
 * Wrapper for the entire server model.
 * This contains a list of all the games in the server, stored as GameModel objects.
 * It also contains a list of all of the users registered to this server.
 *
 * @author Lawrence
 */
public interface ServerModel
{
    /**
     * Retrieves a GameModel object for a certain game based on the game's game ID
     * @param gameID the unique ID for the game
     * @return the GameModel object for that game, which contains both the Board and Game objects
     * @throws GameDoesNotExistException
     */
    public GameModel getGame(int gameID) throws GameDoesNotExistException;

    /**
     * Updates the current log level with
     * @param logLevel the new log level to set it to
     */
    public void changeLogLevel(LogLevel logLevel);
    
    public List<Game> getGames();
    
    public GameModel createGame(boolean randomTiles, boolean randomNumbers, boolean randomPorts, String gameName) throws CatanServerException;

	public int getPlayerID(String username, String password) throws UserDoesNotExistException, PasswordDoesNotMatchException;

	public ServerUser createUser(String username, String password);

	public PregameGame getPregameGame(int gameID) throws GameDoesNotExistException;

	/**
	 * Used for when a PregameGame reaches four players and needs to be converted to a PlayableGame.
	 * @param gameID
	 * @throws GameDoesNotExistException 
	 */
	public void startGame(int gameID) throws GameDoesNotExistException;
}
