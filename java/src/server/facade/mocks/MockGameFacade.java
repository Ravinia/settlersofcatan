package server.facade.mocks;

import java.util.ArrayList;
import java.util.List;

import server.command.CommandObject;
import server.exceptions.InvalidCookie;
import server.facade.GameFacade;
import server.model.GameModel;
import server.model.ai.AIType;
import server.model.ai.AITypeMock;

public class MockGameFacade implements GameFacade
{

	@Override
	public GameModel model(int gameID, int versionNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GameModel reset(int gameID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GameModel commands(int gameID, List<CommandObject> toExecute) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CommandObject> commands(int gameID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addAI(int gameID, String aiName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<AIType> listAI()
    {
        List<AIType> AIs = new ArrayList<>();
        AIs.add(new AITypeMock("Type 1"));
        AIs.add(new AITypeMock("Type 2"));
        AIs.add(new AITypeMock("Type 3"));
        return AIs;
	}

	@Override
	public void validateUser(String playerName, String password, int playerID, int gameID) throws InvalidCookie
	{
		// TODO Auto-generated method stub
		
	}

}
