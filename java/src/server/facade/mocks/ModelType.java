package server.facade.mocks;

/**
 * Helper class used to help determine which model to return from the Moves facade mock
 * @author Lawrence
 */
public enum ModelType
{
    DEFAULT(1),
    ROAD(2),
    SETTLEMENT(3);

    private int id;

    private ModelType(int id)
    {
        this.id = id;
    }

    public int getID()
    {
        return id;
    }
}
