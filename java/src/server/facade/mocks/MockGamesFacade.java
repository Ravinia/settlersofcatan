package server.facade.mocks;

import java.util.ArrayList;
import java.util.List;

import server.courier.serializer.GameInfo;
import server.courier.serializer.GameInfoImp;
import server.courier.serializer.PregamePlayerInfo;
import server.courier.serializer.PregamePlayerInfoImp;
import server.exceptions.InvalidCookie;
import server.facade.GamesFacade;
import server.model.GameModel;
import server.model.game.Game;
import server.model.game.PlayableGame;
import server.model.game.PlayableGameImp;
import server.model.game.PregameGame;
import server.model.game.PregameGameImp;
import server.model.player.Player;
import server.model.player.ServerPlayer;
import shared.definitions.CatanColor;

public class MockGamesFacade implements GamesFacade
{
	protected static final String RED_PLAYER_NAME = "Sam";
    protected static final String BLUE_PLAYER_NAME = "Brooke";
    protected static final String ORANGE_PLAYER_NAME = "Mark";
    protected static final String WHITE_PLAYER_NAME = "Jack";
	
	protected static final String RED_PLAYER_USERNAME = RED_PLAYER_NAME;
    protected static final String RED_PLAYER_PASSWORD = RED_PLAYER_USERNAME;
    protected static final String BLUE_PLAYER_USERNAME = BLUE_PLAYER_NAME;
    protected static final String BLUE_PLAYER_PASSWORD = BLUE_PLAYER_USERNAME;
    protected static final String ORANGE_PLAYER_USERNAME = ORANGE_PLAYER_NAME;
    protected static final String ORANGE_PLAYER_PASSWORD = ORANGE_PLAYER_USERNAME;
    protected static final String WHITE_PLAYER_USERNAME = WHITE_PLAYER_NAME;
    protected static final String WHITE_PLAYER_PASSWORD = WHITE_PLAYER_USERNAME;

    protected static final int RED_PLAYER_INDEX = 0;
    protected static final int BLUE_PLAYER_INDEX = 1;
    protected static final int ORANGE_PLAYER_INDEX = 2;
    protected static final int WHITE_PLAYER_INDEX = 3;
	
    private List<Player> getUsers()
    {
        List<Player> users = new ArrayList<>();
        users.add(new ServerPlayer(0, RED_PLAYER_USERNAME));
        users.add(new ServerPlayer(1, BLUE_PLAYER_USERNAME));
        users.add(new ServerPlayer(2, ORANGE_PLAYER_USERNAME));
        users.add(new ServerPlayer(3, WHITE_PLAYER_USERNAME));
        return users;
    }
	
    private List<Game> getGames()
    {
    	List<Player> players = this.getUsers();
		
		List<Game> games = new ArrayList<>();
		PregameGame preGame = new PregameGameImp(2, "AWESOMENESS");
		
		preGame.addPlayer(players.get(0), CatanColor.BLUE);
		preGame.addPlayer(players.get(1), CatanColor.RED);
		preGame.addPlayer(players.get(2), CatanColor.YELLOW);

		PregameGame preGameforPlay = new PregameGameImp(3, "COOLness");
		preGame.addPlayer(players.get(0), CatanColor.BROWN);
		preGame.addPlayer(players.get(1), CatanColor.BLUE);
		preGame.addPlayer(players.get(2), CatanColor.PUCE);
		preGame.addPlayer(players.get(3), CatanColor.PURPLE);
		
		PlayableGame playGame = new PlayableGameImp(preGameforPlay);
		
		games.add(preGame);
		games.add(playGame);
		return games;
    }
    
	@Override
	public List<Game> listGames() 
	{
		return this.getGames();
	}

	@Override
	public Game createGame(boolean randomTiles, boolean randomNumbers, boolean randomPorts, String gameName) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GameInfo joinGame(int playerId, String playerName, int gameID, CatanColor color) {
		List<PregamePlayerInfo> players = new ArrayList<PregamePlayerInfo>();
		players.add(new PregamePlayerInfoImp(playerName, gameID, color));
		GameInfo gameInfo = new GameInfoImp(playerName, gameID, players);
		return gameInfo;
	}

	@Override
	public boolean saveGame(int gameID, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public GameModel loadGame(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void validateUser(String name, String password, int id)
			throws InvalidCookie
	{
//		List<ServerUser> players = this.getUsers();
//		for(ServerUser player: players)
//		{
//			String userName = player.getUsername();
//			String userPasword = player.getPassword();
//			int userId = player.getUserID();
//			if(userName.equals(name) && userPasword.equals(password) && userId == id)
//			{
//				return;
//			}
//		}
//		throw new InvalidCookie("This is a bad user cookie");
	}

}
