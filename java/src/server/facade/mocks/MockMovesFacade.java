package server.facade.mocks;

import java.util.Map;

import server.command.CommandObject;
import server.facade.MovesFacade;
import server.model.GameModel;
import server.model.GameModelImp;
import server.model.board.placeable.RoadPiece;
import server.model.board.placeable.SettlementPiece;
import server.model.game.PregameGame;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import server.model.player.ServerPlayer;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.exceptions.CatanException;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import static server.model.board.LocationConstants.*;


public class MockMovesFacade implements MovesFacade
{
    protected static final int DEFAULT_GAME_ID = 1;
    protected static final String DEFAULT_GAME_NAME = "fun";
    protected static final int ROAD_MODEL_ID = 2;
    protected static final String ROAD_MODEL_NAME = "roads";
    protected static final int SETTLEMENT_MODEL_ID = 3;
    protected static final String SETTLEMENT_MODEL_NAME = "settlement";


    protected static final String RED_PLAYER_NAME = "Red";
    protected static final String BLUE_PLAYER_NAME = "Blue";
    protected static final String ORANGE_PLAYER_NAME = "Orange";
    protected static final String WHITE_PLAYER_NAME = "White";
    protected static final Player RED_PLAYER = new ServerPlayer(1,RED_PLAYER_NAME);
    protected static final Player BLUE_PLAYER = new ServerPlayer(2,BLUE_PLAYER_NAME);
    protected static final Player ORANGE_PLAYER = new ServerPlayer(3,ORANGE_PLAYER_NAME);
    protected static final Player WHITE_PLAYER = new ServerPlayer(4,WHITE_PLAYER_NAME);

    public static final int RED_PLAYER_INDEX = 0;
    public static final int BLUE_PLAYER_INDEX = 1;
    public static final int ORANGE_PLAYER_INDEX = 2;
    public static final int WHITE_PLAYER_INDEX = 3;


    private GameModelImp defaultModel;
    private GameModelImp roadModel;
    private GameModelImp settlementModel;

    private ModelType resultModel = ModelType.DEFAULT;

    public MockMovesFacade() throws CatanException
    {
        initializeModels();
    }

    public void setResultModel(ModelType model)
    {
        resultModel = model;
    }

    //Helper Methods
    private GameModel returnModel()
    {
        switch (resultModel)
        {
            case ROAD:
                return roadModel;
            case SETTLEMENT:
                return settlementModel;
            case DEFAULT:
                return defaultModel;
            default:
                return defaultModel;
        }
    }

    private void initializeModels() throws CatanException
    {
        defaultModel = new GameModelImp(false, false, false, DEFAULT_GAME_ID, DEFAULT_GAME_NAME);
        PregameGame pre = (PregameGame)defaultModel.getGame();
        pre.addPlayer(RED_PLAYER, CatanColor.RED);
        pre.addPlayer(BLUE_PLAYER, CatanColor.BLUE);
        pre.addPlayer(ORANGE_PLAYER, CatanColor.ORANGE);
        pre.addPlayer(WHITE_PLAYER, CatanColor.WHITE);
        defaultModel.startGame();

        roadModel = new GameModelImp(false, false, false, ROAD_MODEL_ID, ROAD_MODEL_NAME);
        pre = (PregameGame)roadModel.getGame();
        pre.addPlayer(RED_PLAYER, CatanColor.RED);
        pre.addPlayer(BLUE_PLAYER, CatanColor.BLUE);
        pre.addPlayer(ORANGE_PLAYER, CatanColor.ORANGE);
        pre.addPlayer(WHITE_PLAYER, CatanColor.WHITE);
        roadModel.startGame();
        roadModel.getBoard().updateBoardObjects(new RoadPiece(CENTER_N_EDGE,RED_PLAYER));

        settlementModel = new GameModelImp(false, false, false, SETTLEMENT_MODEL_ID, SETTLEMENT_MODEL_NAME);
        pre = (PregameGame)settlementModel.getGame();
        pre.addPlayer(RED_PLAYER, CatanColor.RED);
        pre.addPlayer(BLUE_PLAYER, CatanColor.BLUE);
        pre.addPlayer(ORANGE_PLAYER, CatanColor.ORANGE);
        pre.addPlayer(WHITE_PLAYER, CatanColor.WHITE);
        settlementModel.startGame();
        settlementModel.getBoard().updateBoardObjects(new SettlementPiece(CENTER_NE_VERTEX, RED_PLAYER));
    }

    //Mock Methods
	@Override
	public GameModel sendChat(int gameID, int playerIndex, String message)
	{
		return returnModel();
	}

	@Override
	public GameModel rollNumber(int gameID, int playerIndex, int rollValue)
	{
        return returnModel();
	}

	@Override
	public GameModel robPlayer(int gameID, int playerIndex, int victimIndex, HexLocation location)
	{
        return returnModel();
	}

	@Override
	public GameModel finishTurn(int gameID, int playerIndex)
    {
        return returnModel();
	}

	@Override
	public GameModel buyDevCard(int gameID, int playerIndex)
    {
        return returnModel();
	}

	@Override
	public GameModel yearOfPlenty(int gameID, int playerIndex, ResourceType resource1, ResourceType resource2)
    {
        return returnModel();
	}

	@Override
	public GameModel roadBuilding(int gameID, int playerIndex, EdgeLocation edge1, EdgeLocation edge2)
    {
        return returnModel();
	}

	@Override
	public GameModel soldier(int gameID, int playerIndex, int victimIndex, HexLocation location)
    {
        return returnModel();
	}

	@Override
	public GameModel monopoly(int gameID, int playerIndex, ResourceType resource)
    {
        return returnModel();
	}

	@Override
	public GameModel monument(int gameID, int playerIndex)
    {
        return returnModel();
	}

	@Override
	public GameModel buildRoad(int gameID, int playerIndex, EdgeLocation edge, boolean free)
    {
        return returnModel();
	}

	@Override
	public GameModel buildSettlement(int gameID, int playerIndex, VertexLocation vertex, boolean free)
    {
        return returnModel();
	}

	@Override
	public GameModel buildCity(int gameID, int playerIndex, VertexLocation vertex)
    {
        return returnModel();
	}

	@Override
	public GameModel offerTrade(int gameID, int playerIndex, Map<ResourceType, Integer> offer, int receiverIndex)
    {
        return returnModel();
	}

	@Override
	public GameModel acceptTrade(int gameID, int playerIndex, boolean accepted)
    {
        return returnModel();
	}

	@Override
	public GameModel maritimeTrade(int gameID, int playerIndex, int ratio, ResourceType inputResource, ResourceType outputResource)
    {
        return returnModel();
	}

	@Override
	public GameModel discardCards(int gameID, int playerIndex, ResourceHand toDiscard)
    {
        return returnModel();
	}

	@Override
	public void validateCookie(String playerName, String password, int playerID, int gameID)
	{ }

	@Override
	public void addCommand(CommandObject co, int gameID) {
		// TODO Auto-generated method stub
		
	}

}
