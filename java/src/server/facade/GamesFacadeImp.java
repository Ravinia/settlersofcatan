package server.facade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import server.command.games.JoinGameCommand;
import server.command.games.LoadGameCommand;
import server.command.games.SaveGameCommand;
import server.courier.serializer.GameInfo;
import server.courier.serializer.GameInfoImp;
import server.courier.serializer.PregamePlayerInfo;
import server.courier.serializer.PregamePlayerInfoImp;
import server.exceptions.CatanServerException;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidCookie;
import server.exceptions.PasswordDoesNotMatchException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.exceptions.UserDoesNotExistException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.game.Game;
import server.model.game.PregameGame;
import server.model.player.Player;
import server.persistence.GamesDAO;
import server.persistence.PersistenceProvider;
import shared.definitions.CatanColor;

public class GamesFacadeImp implements GamesFacade
{
	
	private ServerModel serverM;
	private PersistenceProvider persistence;
	private GamesDAO gamesDAO;
	private MovesFacade movesFacade;
	
	public GamesFacadeImp(ServerModel server)
	{
		serverM = server;
		gamesDAO = null;
	}

	public GamesFacadeImp(ServerModel server, PersistenceProvider pers, GamesDAO games, MovesFacade movesFacade)
	{
		serverM = server;
		persistence = pers;
		gamesDAO = games;
		this.movesFacade = movesFacade;
	}
	
	@Override
	public List<Game> listGames()
	{
		return serverM.getGames();
	}

	@Override
	public PregameGame createGame(boolean randomTiles, boolean randomNumbers,
			boolean randomPorts, String gameName) throws CatanServerException, IOException
	{
		GameModel g = serverM.createGame(randomTiles, randomNumbers, randomPorts, gameName);
		if(gamesDAO != null)
		{
			persistence.startTX();
			gamesDAO.addGame(g);
			persistence.endTX(true);
		}
		return (PregameGame) g.getGame();
	}

	@Override
	public GameInfo joinGame(int playerId, String playerName, int gameID, CatanColor color) throws UserDoesNotExistException, PasswordDoesNotMatchException, GameDoesNotExistException, UninitializedDataException, IOException
	{
		JoinGameCommand jgc = new JoinGameCommand(gameID, playerId, playerName, color, serverM);
		jgc.executeCommand();
		if(gamesDAO != null)
			movesFacade.addCommand(jgc, gameID);
		if(! jgc.wasSuccessful())
			return null;
		
//		PregameGame g = serverM.getPregameGame(gameID);
		GameModel g = serverM.getGame(gameID);
		Collection<Player> players = g.getPlayers();
		ArrayList<PregamePlayerInfo> playerInfo = new ArrayList<PregamePlayerInfo>();
		for(Player p : players)
		{
			try {
				playerInfo.add(new PregamePlayerInfoImp(p.getName(), p.getId(), g.getColorOfPlayer(p)));
			} catch (PlayerNotInGameException e) {
				e.printStackTrace();
				assert false;
			}
		}
		return new GameInfoImp(g.getGame().getName(), gameID, playerInfo);
	}

	@Override
	public boolean saveGame(int gameID, String name) throws GameDoesNotExistException, IOException {
		SaveGameCommand sgc = new SaveGameCommand(gameID, name, serverM);
		sgc.executeCommand();
		return true;
	}

	@Override
	public GameModel loadGame(String name) throws ClassNotFoundException, IOException
	{
		LoadGameCommand lgc = new LoadGameCommand(name, serverM);
		lgc.executeCommand();
		return lgc.getGameModel();
	}

	@Override
	public void validateUser(String name, String password, int userID) throws InvalidCookie, UserDoesNotExistException, PasswordDoesNotMatchException
	{
		if(serverM.getPlayerID(name, password) != userID)
			throw new InvalidCookie("The user ID " + userID + " did not match the expected value.");
	}

}
