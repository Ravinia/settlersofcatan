package server.facade;

import java.io.IOException;
import java.util.Map;

import server.command.CommandObject;
import server.exceptions.BankOutOfResourceException;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidCookie;
import server.exceptions.InvalidIndexException;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlaceOccupiedException;
import server.exceptions.PlaceablesNotInitializedException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.game.resources.ResourceHand;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;

/**
 * 
 * @author Isaac
 *
 */
public interface MovesFacade
{
	/**
	 * Sends a chat from the specified server.model.player in the specified game.
	 * @param gameID
	 * @param playerIndex
	 * @param message
	 * @return A model representing the game after the chat was sent.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws UninitializedDataException 
	 * @throws IOException 
	 */
	public GameModel sendChat(int gameID, int playerIndex, String message) throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex just rolled
	 * the number rollValue.
	 * @param gameID
	 * @param playerIndex
	 * @param rollValue
	 * @return A model representing the game after the number was rolled.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidLocationException 
	 * @throws PlaceablesNotInitializedException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidIndexException 
	 * @throws IOException 
	 */
	public GameModel rollNumber(int gameID, int playerIndex, int rollValue) throws GameDoesNotExistException, InvalidLocationException, PlaceablesNotInitializedException, PlayerNotInGameException, InvalidIndexException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is robbing the server.model.player at
	 * victimIndex and moving the robber to location.
	 * @param gameID
	 * @param playerIndex The server.model.player robbing.
	 * @param victimIndex The server.model.player being robbed.
	 * @param location The new location of the robber.
	 * @return A model representing the game after the robbing takes place.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidObjectTypeException 
	 * @throws InvalidRobberLocationException 
	 * @throws IOException 
	 */
	public GameModel robPlayer(int gameID, int playerIndex, int victimIndex, HexLocation location) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, InvalidRobberLocationException, InvalidObjectTypeException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerID has finished his or her turn.
	 * @param gameID
	 * @param playerIndex
	 * @return A model representing the game after the turn ends.
	 * @throws GameDoesNotExistException 
	 * @throws UninitializedDataException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidIndexException 
	 * @throws IOException 
	 */
	public GameModel finishTurn(int gameID, int playerIndex) throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException, PlayerNotInGameException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is buying a development card.
	 * @param gameID
	 * @param playerIndex
	 * @return A model representing the game after the server.model.player receives the card and pays resources.
	 * @throws GameDoesNotExistException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidIndexException 
	 * @throws IOException 
	 */
	public GameModel buyDevCard(int gameID, int playerIndex) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is playing a Year of
	 * Plenty card.
	 * @param gameID
	 * @param playerIndex
	 * @param resource1 The first resource to add to the server.model.player's hand.
	 * @param resource2 The second resource to add to the server.model.player's hand.
	 * @return A model representing the game after the server.model.player has received his or her resources.
	 * @throws GameDoesNotExistException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidIndexException 
	 * @throws IOException 
	 */
	public GameModel yearOfPlenty(int gameID, int playerIndex, ResourceType resource1, ResourceType resource2) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is playing a Road
	 * Building card.
	 * @param gameID
	 * @param playerIndex
	 * @param edge1 First edge to build a road on.
	 * @param edge2 Second edge to build a road on.
	 * @return A model representing the game after the roads are built.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws IOException 
	 */
	public GameModel roadBuilding(int gameID, int playerIndex, EdgeLocation edge1, EdgeLocation edge2) throws GameDoesNotExistException, PlayerNotInGameException, InvalidIndexException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is playing a soldier card.
	 * @param gameID
	 * @param playerIndex The server.model.player robbing.
	 * @param victimIndex The server.model.player being robbed.
	 * @param location The new location of the robber.
	 * @return A model representing the game after the robbing takes place.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidRobberLocationException 
	 * @throws IOException 
	 */
	public GameModel soldier(int gameID, int playerIndex, int victimIndex, HexLocation location) throws GameDoesNotExistException, PlayerNotInGameException, InvalidIndexException, InvalidRobberLocationException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is playing a Monopoly card.
	 * @param gameID
	 * @param playerIndex The server.model.player taking everyone else's resources.
	 * @param resource The type of resource being taken.
	 * @return A model representing the game after resources have been redistributed.
	 * @throws GameDoesNotExistException 
	 * @throws PlayerNotInGameException 
	 * @throws IOException 
	 */
	public GameModel monopoly(int gameID, int playerIndex, ResourceType resource) throws GameDoesNotExistException, PlayerNotInGameException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is playing a monument card.
	 * @param gameID
	 * @param playerIndex
	 * @return A model representing the game after the server.model.player's victory points have been incremented.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws IOException 
	 */
	public GameModel monument(int gameID, int playerIndex) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is building a road.
	 * @param gameID
	 * @param playerIndex
	 * @param edge The location of the road to be built.
	 * @param free True if this is one of the free roads placed during the setup rounds.
	 * @return A model representing the game after the road is built and the server.model.player pays resources.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidObjectTypeException 
	 * @throws PlayerNotInGameException 
	 * @throws PlaceOccupiedException 
	 * @throws InvalidRobberLocationException 
	 * @throws InvalidIndexException 
	 * @throws IOException 
	 */
	public GameModel buildRoad(int gameID, int playerIndex, EdgeLocation edge, boolean free) throws GameDoesNotExistException, InvalidIndexException, InvalidRobberLocationException, PlaceOccupiedException, PlayerNotInGameException, InvalidObjectTypeException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is building a settlement.
	 * @param gameID
	 * @param playerIndex
	 * @param vertex The location of the settlement to be built.
	 * @param free True if this is one of the free settlements placed during the setup rounds.
	 * @return A model representing the game after the settlement is built and the server.model.player pays resources.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidObjectTypeException 
	 * @throws InvalidRobberLocationException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws UninitializedDataException 
	 * @throws IOException 
	 */
	public GameModel buildSettlement(int gameID, int playerIndex, VertexLocation vertex, boolean free) throws GameDoesNotExistException, InvalidIndexException, InvalidRobberLocationException, InvalidObjectTypeException, PlayerNotInGameException, UninitializedDataException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex is building a city.
	 * @param gameID
	 * @param playerIndex
	 * @param vertex The location of the city to be built.
	 * @return A model representing the game after the city is built and the server.model.player pays resources.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidObjectTypeException 
	 * @throws InvalidLocationException 
	 * @throws PlaceablesNotInitializedException 
	 * @throws InvalidIndexException 
	 * @throws PlayerNotInGameException 
	 * @throws IOException 
	 */
	public GameModel buildCity(int gameID, int playerIndex, VertexLocation vertex) throws GameDoesNotExistException, InvalidIndexException, PlaceablesNotInitializedException, InvalidLocationException, InvalidObjectTypeException, PlayerNotInGameException, IOException;
	
	/**
	 * Offers a trade to a server.model.player.
	 * 
	 * A note on the functionality of offerTrade: The integer values in the map @param offer should be positive if 
	 * the player offering the trade expects to give those resources to the player at @param receiverIndex, and 
	 * negative if the offering playing expects to receive those resources.
	 * 
	 * @param gameID
	 * @param playerIndex The server.model.player offering the trade.
	 * @param offer The resources to give and receive.
	 * @param receiverIndex The server.model.player being offered the trade.
	 * @return A model representing the game after the offer is sent.
	 * @throws GameDoesNotExistException 
	 * @throws InvalidIndexException 
	 * @throws UninitializedDataException 
	 * @throws IOException 
	 */
	public GameModel offerTrade(int gameID, int playerIndex, Map<ResourceType, Integer> offer, int receiverIndex) throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException, IOException;
	
	/**
	 * The server.model.player receiving the trade accepts or rejects it.
	 * @param gameID
	 * @param playerIndex The server.model.player responding to a trade offer.
	 * @param accepted True if the trade is accepted.
	 * @return A model representing the game after the trade is finished.
	 * @throws GameDoesNotExistException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidIndexException 
	 * @throws NoCurrentTradesException 
	 * @throws IOException 
	 */
	public GameModel acceptTrade(int gameID, int playerIndex, boolean accepted) throws GameDoesNotExistException, NoCurrentTradesException, InvalidIndexException, PlayerNotInGameException, IOException;
	
	/**
	 * A server.model.player trades with the bank.
	 * @param gameID
	 * @param playerIndex The server.model.player doing the trading.
	 * @param ratio The ratio of resources to trade (2, 3, or 4 for 2:1, 3:1, or 4:1).
	 * @param inputResource The resource which the server.model.player is giving up.
	 * @param outputResource The resource which the server.model.player is trading for.
	 * @return A model representing the game after the trade is finished.
	 * @throws BankOutOfResourceException 
	 * @throws PlayerNotInGameException 
	 * @throws InvalidIndexException 
	 * @throws GameDoesNotExistException 
	 * @throws IOException 
	 */
	public GameModel maritimeTrade(int gameID, int playerIndex, int ratio, ResourceType inputResource, ResourceType outputResource) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, BankOutOfResourceException, IOException;
	
	/**
	 * Alerts the game that the server.model.player at playerIndex has chosen which cards to discard
	 * after a seven was rolled.
	 * @param gameID
	 * @param playerIndex The server.model.player discarding.
	 * @param toDiscard The resources being discarded.
	 * @return A model representing the game after the resources are removed from the server.model.player's hand.
	 * @throws PlayerNotInGameException 
	 * @throws InvalidIndexException 
	 * @throws GameDoesNotExistException 
	 * @throws IOException 
	 */
	public GameModel discardCards(int gameID, int playerIndex, ResourceHand toDiscard) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, IOException;

	public void validateCookie(String playerName, String password, int playerID, int gameID) throws InvalidCookie, GameDoesNotExistException, UninitializedDataException;
	
	public void addCommand(CommandObject co, int gameID) throws IOException, GameDoesNotExistException;
}
