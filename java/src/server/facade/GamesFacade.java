package server.facade;

import java.io.IOException;
import java.util.List;

import server.courier.serializer.GameInfo;
import server.exceptions.CatanServerException;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidCookie;
import server.exceptions.PasswordDoesNotMatchException;
import server.exceptions.UninitializedDataException;
import server.exceptions.UserDoesNotExistException;
import server.model.GameModel;
import server.model.game.Game;
import shared.definitions.CatanColor;

/**
 * 
 * @author Isaac
 *
 */
public interface GamesFacade {
	
	/**
	 * 
	 * @return A list of the games currently running on this server.
	 */
	public List<Game> listGames();
	
	/**
	 * Creates a new, empty game with the setup specified by the three booleans and the
	 * name specified by gameName.
	 * @param randomTiles
	 * @param randomNumbers
	 * @param randomPorts
	 * @param gameName
	 * @return A game object with no players representing the game that was just created.
	 * @throws CatanServerException 
	 * @throws IOException 
	 */
	public Game createGame(boolean randomTiles, boolean randomNumbers, boolean randomPorts, String gameName) throws CatanServerException, IOException;
	
	/**
	 * Attempts to join the game specified by the gameID as the server.model.player specified by the
	 * cookie. If the join is successful, your color will be set to the given color.
	 * @param playerId
	 * @param playerName
	 * @param gameID
	 * @param color
	 * @return Null if the join was unsuccessful
	 * @throws PasswordDoesNotMatchException 
	 * @throws UserDoesNotExistException 
	 * @throws GameDoesNotExistException 
	 * @throws UninitializedDataException 
	 * @throws IOException 
	 */
	public GameInfo joinGame(int playerId, String playerName, int gameID, CatanColor color) throws UserDoesNotExistException, PasswordDoesNotMatchException, GameDoesNotExistException, UninitializedDataException, IOException;
	
	/**
	 * Saves the game specified by gameID in a file with the given name.
	 * @param gameID
	 * @param name
	 * @return True if the game saved successfully.
	 * @throws IOException 
	 * @throws GameDoesNotExistException 
	 */
	public boolean saveGame(int gameID, String name) throws GameDoesNotExistException, IOException;
	
	/**
	 * Attempts to load a game saved in a file with the given name.
	 * @param name
	 * @return A model representing the loaded game if the name was found, or null otherwise.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public GameModel loadGame(String name) throws ClassNotFoundException, IOException;
	
	public void validateUser(String name, String password, int userID) throws InvalidCookie, UserDoesNotExistException, PasswordDoesNotMatchException;
}
