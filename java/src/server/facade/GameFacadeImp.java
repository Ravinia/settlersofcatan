package server.facade;

import java.util.ArrayList;
import java.util.List;

import server.command.CommandObject;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidCookie;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.ai.AIType;
import server.model.ai.AITypeImp;
import server.model.game.PlayableGame;

public class GameFacadeImp implements GameFacade
{
	private ServerModel serverM;

	
	public GameFacadeImp(ServerModel server)
	{
		serverM = server;
	}
	
	/* Notes on usage:
	 * If the version number is different than the current version number for 
	 * the game specified by the id number, returns the model representing that game. 
	 * If the version number is the same, returns null, and the server.handler will need
	 * to return the string "true".
	 * If the version number is -1, returns the model representing the game no 
	 * matter what. 
	 */
	@Override
	public GameModel model(int gameID, int versionNumber) throws GameDoesNotExistException {
		
		GameModel game = serverM.getGame(gameID);
		if(versionNumber == -1)
			return game;
		// get game version number
		if(versionNumber == ((PlayableGame) game.getGame()).getCurrentVersion())
			return null;
		
		return game;
	}

	// Not used by professor's orders
	@Override
	public GameModel reset(int gameID) {
		assert false;
		return null;
	}

	// Same with the rest of these methods - not used (except validate user)
	@Override
	public GameModel commands(int gameID, List<CommandObject> toExecute) {
		assert false;
		return null;
	}

	@Override
	public List<CommandObject> commands(int gameID) {
		assert false;
		return null;
	}

	@Override
	public boolean addAI(int gameID, String aiName) {
		assert false;
		return false;
	}

	@Override
	public List<AIType> listAI()
    {
        List<AIType> result = new ArrayList<>();
		for (AIType type : AITypeImp.values())
        {
            result.add(type);
        }
        return result;
	}

	@Override
	public void validateUser(String playerName, String password, int playerID, int gameID) throws InvalidCookie
	{
		// TODO Auto-generated method stub
		
	}

}
