package server.facade;

import java.util.List;

import server.command.CommandObject;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidCookie;
import server.model.GameModel;
import server.model.ai.AIType;

/**
 * 
 * @author Isaac
 *
 */
public interface GameFacade
{
	/**
	 * If the version number is different than the current version number for 
	 * the game specified by the id number, returns the model representing that game. 
	 * If the version number is the same, returns null, and the server.handler will need
	 * to return the string "TRUE".
	 * If the version number is -1, returns the model representing the game no 
	 * matter what. 
	 * Returns null if the game hasn't started yet.
	 * @param gameID
	 * @param versionNumber
	 * @return
	 * @throws GameDoesNotExistException 
	 */
	public GameModel model(int gameID, int versionNumber) throws GameDoesNotExistException;
	
	/**
	 * Deletes the list of commands for the given game and returns a GameModel object 
	 * representing the game state before those commands were executed.
	 * 
	 * Not actually sure what that means - check with TAs or professor.
	 * @param gameID
	 * @return
	 */
	public GameModel reset(int gameID);
	
	/**
	 * Executes the commands in toExecute in the order that they are given for the 
	 * game specified by the id number, then returns a GameModel object representing 
	 * the game after those commands were executed.
	 * @param gameID
	 * @param toExecute
	 * @return
	 */
	public GameModel commands(int gameID, List<CommandObject> toExecute);
	
	/**
	 * Returns a list of all of the commands that have been executed on the game since 
	 * it was created. This list is deleted whenever the reset method is called.
	 * @param gameID
	 * @return
	 */
	public List<CommandObject> commands(int gameID);
	
	/**
	 * Adds an AI server.model.player with the name represented by aiName. The name must be one of the names
	 * from the listAI method.
	 * @param gameID
	 * @param aiName
	 * @return True if the AI server.model.player was successfully added, false otherwise.
	 */
	public boolean addAI(int gameID, String aiName);
	
	/**
	 * 
	 * @return A list of all the available AI players that may be added to the game.
	 */
	public List<AIType> listAI();

	public void validateUser(String playerName, String password, int playerID, int gameID) throws InvalidCookie;
}
