package server.facade;

import shared.debug.LogLevel;

/**
 * Server facade that covers handling server util functions from the client
 */
public interface UtilFacade
{
    /**
     * Sets the log level for the entire server.
     *
     * @param level the new log level to set the server logger to
     * @return whether or not changing the log level succeeded.
     */
    public boolean changeLogLevel(LogLevel level);
}
