package server.facade;

import java.io.IOException;

import server.command.user.RegisterCommand;
import server.courier.serializer.PregamePlayerInfo;
import server.courier.serializer.PregamePlayerInfoImp;
import server.exceptions.PasswordDoesNotMatchException;
import server.exceptions.UserDoesNotExistException;
import server.model.ServerModel;
import server.persistence.PersistenceProvider;
import server.persistence.UsersDAO;

public class UserFacadeImp implements UserFacade
{
	ServerModel serverM;
	PersistenceProvider persistence;
	UsersDAO usersDAO;
	
	public UserFacadeImp(ServerModel server)
	{
		serverM = server;
	}
	
	public UserFacadeImp(ServerModel server, PersistenceProvider pers, UsersDAO users)
	{
		serverM = server;
		persistence = pers;
		usersDAO = users;
	}
	
	@Override
	public PregamePlayerInfo login(String username, String password) throws UserDoesNotExistException, PasswordDoesNotMatchException
	{
		PregamePlayerInfoImp ppii = new PregamePlayerInfoImp(username, serverM.getPlayerID(username, password), null);
		
		return ppii;
	}

	@Override
	public PregamePlayerInfo register(String username, String password) throws IOException
	{
		RegisterCommand rc = new RegisterCommand(username, password, serverM);
		rc.executeCommand();
		if(rc.getPlayer() == null)
			return null;
		else if(usersDAO != null)
		{
			persistence.startTX();
			usersDAO.addUser(rc.getPlayer());
			persistence.endTX(true);
		}
		return new PregamePlayerInfoImp(username, rc.getPlayer().getUserID(), null);
	}

}
