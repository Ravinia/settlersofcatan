package server.facade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import server.command.CommandObject;
import server.command.moves.AcceptTradeCommand;
import server.command.moves.BuildCityCommand;
import server.command.moves.BuildRoadCommand;
import server.command.moves.BuildSettlementCommand;
import server.command.moves.BuyDevCardCommand;
import server.command.moves.DiscardCardsCommand;
import server.command.moves.FinishTurnCommand;
import server.command.moves.MaritimeTradeCommand;
import server.command.moves.MonopolyCommand;
import server.command.moves.MonumentCommand;
import server.command.moves.OfferTradeCommand;
import server.command.moves.RoadBuildingCommand;
import server.command.moves.RobPlayerCommand;
import server.command.moves.Roll7Command;
import server.command.moves.RollOtherCommand;
import server.command.moves.SendChatCommand;
import server.command.moves.SoldierCommand;
import server.command.moves.YearOfPlentyCommand;
import server.exceptions.BankOutOfResourceException;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.InvalidCookie;
import server.exceptions.InvalidIndexException;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlaceOccupiedException;
import server.exceptions.PlaceablesNotInitializedException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.board.Board;
import server.model.board.immutable.Hex;
import server.model.board.placeable.Settlement;
import server.model.game.Game;
import server.model.game.PlayableGame;
import server.model.game.bank.Deck;
import server.model.game.resources.ResourceHand;
import server.model.player.Player;
import server.persistence.CommandsDAO;
import server.persistence.GamesDAO;
import server.persistence.PersistenceProvider;
import shared.definitions.ResourceType;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;
import shared.exceptions.InvalidRobberLocationException;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;

public class MovesFacadeImp implements MovesFacade
{
	private ServerModel serverM;
	private PersistenceProvider persistence;
	private GamesDAO gamesDAO;
	private CommandsDAO commandsDAO;
	int threshold;
	Map<Integer, Integer> thresholdCounter;
	
	public MovesFacadeImp(ServerModel server)
	{
		serverM = server;
	}
	
	public MovesFacadeImp(ServerModel server, PersistenceProvider pers, GamesDAO games, CommandsDAO commands, int threshold)
	{
		serverM = server;
		persistence = pers;
		gamesDAO = games;
		commandsDAO = commands;
		this.threshold = threshold;
		// maps the gameID to the number of commands added to that game
		thresholdCounter = new HashMap<Integer, Integer>();
	}

	@Override
	public GameModel sendChat(int gameID, int playerIndex, String message) throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException, IOException
	{
		SendChatCommand c = new SendChatCommand(playerIndex, message, gameID, serverM);
		c.executeCommand();
		if(commandsDAO != null)
			addCommand(c, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel rollNumber(int gameID, int playerIndex, int rollValue) throws GameDoesNotExistException, 
			InvalidLocationException, PlaceablesNotInitializedException, PlayerNotInGameException, InvalidIndexException, IOException
	{
		GameModel gameModel = serverM.getGame(gameID);
		if(!(gameModel.getGame() instanceof PlayableGame))
			return null;
		if(rollValue != 7)
		{
			Board board = gameModel.getBoard();
			Collection<Hex> hexes = board.getHexesByTokenValue(rollValue);
			
			// The list will be in order of player index
			List<Map<ResourceType, Integer> > resources = new ArrayList<Map<ResourceType, Integer> >();
			List<Player> players = gameModel.getPlayers();
			for(int i = 0; i<players.size(); i++)
				resources.add(new HashMap<ResourceType, Integer>());
			
			for(Hex h : hexes)
			{
				Collection<VertexLocation> settlementLocations = h.getSettlementsByThisHex();
				ResourceType rType = h.getResourceType();
				for(VertexLocation v : settlementLocations)
				{
					Settlement s = board.getSettlement(v);
					for(int j = 0; j<players.size(); j++)
					{
						if(players.get(j).equals(s.getPlayer()))
						{
							Map<ResourceType, Integer> playerResources = resources.get(j);
							if(! playerResources.containsKey(rType))
								playerResources.put(rType, 0);
							if(s.isCity())
								playerResources.put(rType, playerResources.get(rType) + 2);
							else
								playerResources.put(rType, playerResources.get(rType) + 1);
						}
					}
				}
			}
			
			RollOtherCommand roc = new RollOtherCommand(resources, rollValue, playerIndex, gameID, serverM);
			try {
				roc.executeCommand();
			} catch (UninitializedDataException e) {
				e.printStackTrace();
			}
			if(commandsDAO != null)
				addCommand(roc, gameID);
		}
		else // Handle rolling a seven
		{
			List<Player> playersToDiscard = new ArrayList<Player>();
			PlayableGame game = (PlayableGame) gameModel.getGame();
			for(Player p : gameModel.getPlayers())
			{
				ResourceHand rh = game.getResourceHandOfPlayer(p);
				if(rh.getTotalResourceCount() > 7)
					playersToDiscard.add(p);
			}

			Roll7Command rsc = new Roll7Command(playersToDiscard, playerIndex, gameID, serverM);
			try {
				rsc.executeCommand();
			} catch (UninitializedDataException e) {
				e.printStackTrace();
			}
			if(commandsDAO != null)
				addCommand(rsc, gameID);
		}
		return serverM.getGame(gameID);
	}

	// Steal a card from another player and move the robber
	@Override
	public GameModel robPlayer(int gameID, int playerIndex, int victimIndex,
			HexLocation location) throws GameDoesNotExistException, InvalidIndexException, 
			PlayerNotInGameException, InvalidRobberLocationException, InvalidObjectTypeException, IOException
	{
		GameModel gameModel = serverM.getGame(gameID);
		PlayableGame game = (PlayableGame) gameModel.getGame();
		if(victimIndex == -1)
		{
			try {
				RobPlayerCommand rpc = new RobPlayerCommand(playerIndex, victimIndex, location, 
						ResourceType.BRICK, gameID, serverM);
				rpc.executeCommand();
				if(commandsDAO != null)
					addCommand(rpc, gameID);
			} catch (UninitializedDataException e) {
				e.printStackTrace();
			}
		}
		else
		{
			Player victim = gameModel.getPlayerByIndex(victimIndex);
			ResourceHand rh = game.getResourceHandOfPlayer(victim);
			RobPlayerCommand rpc = new RobPlayerCommand(playerIndex, victimIndex, location, 
				getResourceToRob(rh), gameID, serverM);
			try 
			{
				rpc.executeCommand();
				if(commandsDAO != null)
					addCommand(rpc, gameID);
			} 
			catch(UninitializedDataException e) 
			{
				e.printStackTrace();
			}
			
		}
		return serverM.getGame(gameID);
	}

	private ResourceType getResourceToRob(ResourceHand rh)
	{
		int resCount = rh.getTotalResourceCount();
		Random randomGen = new Random();
		int cardToDiscard = randomGen.nextInt() % resCount;
		
		if((cardToDiscard -= rh.getBrick()) < 0)
		{
			return ResourceType.BRICK;
		}
		else if((cardToDiscard -= rh.getWood()) < 0)
		{
			return ResourceType.WOOD;
		}
		else if((cardToDiscard -= rh.getWheat()) < 0)
		{
			return ResourceType.WHEAT;
		}
		else if((cardToDiscard -= rh.getSheep()) < 0)
		{
			return ResourceType.SHEEP;
		}
		else
			return ResourceType.ORE;
	}

	@Override
	public GameModel finishTurn(int gameID, int playerIndex) throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException, PlayerNotInGameException, IOException
	{
		FinishTurnCommand ftc = new FinishTurnCommand(playerIndex, gameID, serverM);
		ftc.executeCommand();
		if(commandsDAO != null)
			addCommand(ftc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel buyDevCard(int gameID, int playerIndex) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, IOException
	{
		Deck deck = ((PlayableGame) serverM.getGame(gameID).getGame()).getDeck();
		BuyDevCardCommand bdc = new BuyDevCardCommand(playerIndex, deck.chooseCardToDraw(), gameID, serverM);
		try {
			bdc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(bdc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel yearOfPlenty(int gameID, int playerIndex,
                                  ResourceType resource1, ResourceType resource2) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, IOException
	{
		YearOfPlentyCommand yop = new YearOfPlentyCommand(playerIndex, resource1, resource2, gameID, serverM);
		try {
			yop.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(yop, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel roadBuilding(int gameID, int playerIndex,
			EdgeLocation edge1, EdgeLocation edge2) throws GameDoesNotExistException, PlayerNotInGameException, InvalidIndexException, IOException
	{
		RoadBuildingCommand rbc = new RoadBuildingCommand(playerIndex, edge1, edge2, gameID, serverM);
		try {
			rbc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(rbc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel soldier(int gameID, int playerIndex, int victimIndex,
			HexLocation location) throws GameDoesNotExistException, PlayerNotInGameException, InvalidIndexException, InvalidRobberLocationException, IOException
	{
		GameModel gameModel = serverM.getGame(gameID);
		ResourceHand rh = ((PlayableGame) gameModel.getGame()).getResourceHandOfPlayer(gameModel.getPlayerByIndex(playerIndex));
		SoldierCommand sc = new SoldierCommand(playerIndex, victimIndex, location, getResourceToRob(rh), gameID, serverM);
		try {
			sc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(sc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel monopoly(int gameID, int playerIndex, ResourceType resource) throws GameDoesNotExistException, PlayerNotInGameException, IOException
	{
		MonopolyCommand monoc = new MonopolyCommand(playerIndex, resource, gameID, serverM);
		try {
			monoc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(monoc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel monument(int gameID, int playerIndex) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, IOException
	{
		MonumentCommand monuc = new MonumentCommand(playerIndex, gameID, serverM);
		try {
			monuc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(monuc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel buildRoad(int gameID, int playerIndex, EdgeLocation edge,
			boolean free) throws GameDoesNotExistException, InvalidIndexException, InvalidRobberLocationException, PlaceOccupiedException, PlayerNotInGameException, InvalidObjectTypeException, IOException
	{
		BuildRoadCommand brc = new BuildRoadCommand(playerIndex, edge, free, gameID, serverM);
		try {
			brc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(brc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel buildSettlement(int gameID, int playerIndex,
			VertexLocation vertex, boolean free) throws GameDoesNotExistException, InvalidIndexException, InvalidRobberLocationException, InvalidObjectTypeException, PlayerNotInGameException, UninitializedDataException, IOException
	{
		BuildSettlementCommand bsc = new BuildSettlementCommand(playerIndex, vertex, free, gameID, serverM);
		bsc.executeCommand();
		if(commandsDAO != null)
			addCommand(bsc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel buildCity(int gameID, int playerIndex,
			VertexLocation vertex) throws GameDoesNotExistException, InvalidIndexException, PlaceablesNotInitializedException, InvalidLocationException, InvalidObjectTypeException, PlayerNotInGameException, IOException
	{
		BuildCityCommand bcc = new BuildCityCommand(playerIndex, vertex, gameID, serverM);
		try {
			bcc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(bcc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel offerTrade(int gameID, int playerIndex,
			Map<ResourceType, Integer> offer, int receiverIndex) throws GameDoesNotExistException, UninitializedDataException, InvalidIndexException, IOException
	{
		OfferTradeCommand otc = new OfferTradeCommand(playerIndex, offer, receiverIndex, gameID, serverM);
		otc.executeCommand();
		if(commandsDAO != null)
			addCommand(otc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel acceptTrade(int gameID, int playerIndex, boolean accepted) throws GameDoesNotExistException, NoCurrentTradesException, InvalidIndexException, PlayerNotInGameException, IOException
	{
		AcceptTradeCommand atc = new AcceptTradeCommand(playerIndex, accepted, gameID, serverM);
		try {
			atc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(atc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel maritimeTrade(int gameID, int playerIndex, int ratio,
			ResourceType inputResource, ResourceType outputResource) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, BankOutOfResourceException, IOException
	{
		MaritimeTradeCommand mtc = new MaritimeTradeCommand(playerIndex, ratio, inputResource, outputResource, gameID, serverM);
		try {
			mtc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(mtc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public GameModel discardCards(int gameID, int playerIndex,
			ResourceHand toDiscard) throws GameDoesNotExistException, InvalidIndexException, PlayerNotInGameException, IOException
	{
		List<ResourceType> cards = toDiscard.getResourceList();
		DiscardCardsCommand dcc = new DiscardCardsCommand(playerIndex, cards, gameID, serverM);
		try {
			dcc.executeCommand();
		} catch (UninitializedDataException e) {
			e.printStackTrace();
		}
		if(commandsDAO != null)
			addCommand(dcc, gameID);
		return serverM.getGame(gameID);
	}

	@Override
	public void validateCookie(String playerName, String password, int playerID, int gameID)
			throws InvalidCookie, GameDoesNotExistException, UninitializedDataException
	{
		Game game = serverM.getGame(gameID).getGame();
		Player player = game.getPlayerWithID(playerID);
		if(player == null)
			throw new InvalidCookie("No player with ID " + playerID + " exists in game " + gameID);
		if(player.getName().compareTo(playerName) != 0)
			throw new InvalidCookie("The player name " + playerName + " did not match the name " + player.getName());
	}

	@Override
	public void addCommand(CommandObject co, int gameID) throws IOException, GameDoesNotExistException
	{
		if(! thresholdCounter.containsKey(gameID))
			thresholdCounter.put(gameID, 1);
		else
			thresholdCounter.put(gameID, thresholdCounter.get(gameID) + 1);
		
		persistence.startTX();
		if(thresholdCounter.get(gameID) >= threshold)
		{
			commandsDAO.deleteCommands(gameID);
			gamesDAO.deleteGame(gameID);
			gamesDAO.addGame(serverM.getGame(gameID));
			thresholdCounter.put(gameID, 0);
		}
		else
			commandsDAO.addCommand(co, gameID);
		persistence.endTX(true);
	}

}
