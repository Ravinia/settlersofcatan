package server.facade;

import server.model.ServerModel;
import shared.debug.LogLevel;

public class UtilFacadeImp implements UtilFacade
{
	ServerModel serverM;
	
	public UtilFacadeImp(ServerModel server)
	{
		serverM = server;
	}
	
	@Override
	public boolean changeLogLevel(LogLevel level) {
		serverM.changeLogLevel(level);
		return true;
	}

}
