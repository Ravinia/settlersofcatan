package server.facade;

import java.io.IOException;

import server.courier.serializer.PregamePlayerInfo;
import server.exceptions.PasswordDoesNotMatchException;
import server.exceptions.UserDoesNotExistException;

/**
 * 
 * @author Isaac
 *
 */
public interface UserFacade
{
	/**
	 * Attempts to log in with the given username and password.
	 * @param username
	 * @param password
	 * @return A courier object with data on the server.model.player.
	 * @throws PasswordDoesNotMatchException 
	 * @throws UserDoesNotExistException 
	 */
	public PregamePlayerInfo login(String username, String password) throws UserDoesNotExistException, PasswordDoesNotMatchException;
	
	/**
	 * Attempts to create a new user with the given username and password. If 
	 * successful, logs in that user.
	 * @param username
	 * @param password
	 * @return Null if the registration was unsuccessful.
	 * @throws IOException 
	 */
	public PregamePlayerInfo register(String username, String password) throws IOException;
}
