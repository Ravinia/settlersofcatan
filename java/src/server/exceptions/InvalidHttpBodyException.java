package server.exceptions;

/**
 * Exception is thrown when the server deserializer is given an invalid http body
 * @author Jameson
 */
public class InvalidHttpBodyException extends CatanServerException
{
	private static final long serialVersionUID = 5110754785173503526L;

	public InvalidHttpBodyException()
    {
        super("You have given the deserializer an invalid http body");
    }
}
