package server.exceptions;

/**
 * @author Lawrence
 */
public class DesertException extends CatanServerException
{
    public DesertException()
    {
        super("Operation not valid: this is a desert hex");
    }
}
