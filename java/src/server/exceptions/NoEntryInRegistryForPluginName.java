package server.exceptions;

/**
 * Thrown when parsing XML and not finding the corresponding plugin name.
 * @author Jameson
 */
public class NoEntryInRegistryForPluginName extends CatanServerException
{
    public NoEntryInRegistryForPluginName(String name)
    {
        super("There is no entry for " + name + " in the file: registry.xml");
    }
}
