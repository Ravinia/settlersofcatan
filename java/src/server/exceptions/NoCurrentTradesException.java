package server.exceptions;

import server.model.game.Game;

/**
 * Thrown when someone tries to retrieve the current trade offer when there are no trades currently being offered in a specified game
 * @author Lawrence
 */
public class NoCurrentTradesException extends CatanServerException
{
    public NoCurrentTradesException(Game game)
    {
        super("There are currently no trades being offered in game[" + game.getID() + "] '" + game.getName() + "'");
    }

    public NoCurrentTradesException()
    {
        super("There are currently no trades offered for this game model object");
    }
}
