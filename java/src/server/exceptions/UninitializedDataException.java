package server.exceptions;

/**
 * Thrown for known null data problems on the server
 * @author Lawrence
 */
public class UninitializedDataException extends CatanServerException
{
    public UninitializedDataException(String message)
    {
        super("This data was not initialized: " + message);
    }
}
