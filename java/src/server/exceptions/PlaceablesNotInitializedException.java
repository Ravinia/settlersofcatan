package server.exceptions;

import server.model.board.placeable.Placeable;

/**
 * Thrown when trying to retrieve or reference a placeable piece before the game has been started
 * @author Lawrence
 */
public class PlaceablesNotInitializedException extends CatanServerException
{
    public PlaceablesNotInitializedException(Placeable piece)
    {
        super("Unable to retrieve piece of type '" + piece.getType() + "':" +
                " placeable pieces are not allowed on the board until after the game has started.");
    }
}
