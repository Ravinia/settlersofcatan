package server.exceptions;

/**
 * Thrown when someone tries to retrieve a game that does not exist
 * @author Lawrence
 */
public class GameDoesNotExistException extends CatanServerException
{
    public GameDoesNotExistException(int gameID)
    {
        super("There are currently no games with the ID: " + gameID);
    }
    public GameDoesNotExistException(String name)
    {
        super("There are currently no games with the name: " + name);
    }
}
