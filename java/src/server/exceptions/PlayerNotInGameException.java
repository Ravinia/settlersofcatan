package server.exceptions;

import server.model.game.Game;

/**
 * Exception thrown when someone attempts to reference a server.model.player in game that they are not currently in
 * @author Lawrence
 */
public class PlayerNotInGameException extends CatanServerException
{
    public PlayerNotInGameException(String playerName, Game game)
    {
        super("The player named " + playerName + " cannot be found in game[" + game.getID() + "] '" + game.getName() + "'");
    }
    public PlayerNotInGameException(Game game, Exception e)
    {
    	super("player cannot be found in game[" + game.getID() + "] '" + game.getName() + "'");
    }

    public PlayerNotInGameException(String name, Game game, String message)
    {
        super("Player '" + name + "' could not be found in game[" + game.getID() + "] '" + game.getName() + "': " + message);
    }
}
