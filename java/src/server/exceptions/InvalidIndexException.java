package server.exceptions;

public class InvalidIndexException extends CatanServerException
{
	public InvalidIndexException(int index)
	{
		super("Not a valid index: " + index);
	}
}
