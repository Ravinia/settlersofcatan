package server.exceptions;

import server.model.game.Game;

/**
 * @author Lawrence
 */
public class PlayerDoesNotHaveColorException extends PlayerNotInGameException
{
    public PlayerDoesNotHaveColorException(String playerName, Game game)
    {
        super(playerName, game, "The player specified does not have a color specified in this game");
    }
}
