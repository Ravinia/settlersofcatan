package server.exceptions;

import shared.exceptions.CatanException;

public class BankOutOfResourceException extends CatanException{
	
	 public BankOutOfResourceException(String message)
	 {
		 super(message);
	 }
	 public BankOutOfResourceException(String message, Exception e)
	 {
		 super(message, e);
	 }
}
