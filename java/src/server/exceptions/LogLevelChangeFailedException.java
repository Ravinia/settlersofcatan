package server.exceptions;

public class LogLevelChangeFailedException extends CatanServerException
{

	public LogLevelChangeFailedException(String message)
	{
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8292029436473360423L;

}
