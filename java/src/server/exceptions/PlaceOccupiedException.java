package server.exceptions;

import server.model.board.placeable.Placeable;
import server.model.game.Game;

public class PlaceOccupiedException extends CatanServerException{
	 
	public PlaceOccupiedException(Placeable placeable, int playerIndex, Game game)
	{
		 super("The Location for this" +placeable.getType() +"in Game" +game.getName() +"[" +game.getID()+ "]" +
	"placed by player" + playerIndex);
	}
}
