package server.exceptions;

import shared.locations.VertexLocation;

/**
 * Thrown when trying to reference a port for a given location on a board that does not have a port object on it
 * @author Lawrence
 */
public class DoesNotHavePortException extends CatanServerException
{
    public DoesNotHavePortException(VertexLocation location)
    {
        super("There is no port located at " + location.toString() + " on this board");
    }
}
