package server.exceptions;

/**
 * Exception thrown when the model is given a request for a user with a username that does not exist
 * @author Lawrence
 */
public class UserDoesNotExistException extends CatanServerException
{
    public UserDoesNotExistException(String username)
    {
        super("There does not exist a user registered to the server with username: '" + username + "'");
    }
}
