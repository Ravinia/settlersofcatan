package server.exceptions;

/**
 * Exception thrown when a request is given to the model with a password that does not match the provided username.
 * This exception is only thrown when the username is correct, but the password isn't.
 * If the username is not correct, throw UserDoesNotExistException instead.
 * @author Lawrence
 */
public class PasswordDoesNotMatchException extends CatanServerException
{
    public PasswordDoesNotMatchException(String user, String expectedPassword, String actualPassword)
    {
        super("Error: password mismatch for user '" + user +
                "'. Expected: '" + expectedPassword + "', Actual: '" + actualPassword + "'");
    }
}
