package server.exceptions;

import shared.exceptions.CatanException;

/**
 * Exception specific to the server side in settlers of catan
 * @author Lawrence
 */
public class CatanServerException extends CatanException
{
    public CatanServerException(String message)
    {
        super(message);
    }

    public CatanServerException(String message, Exception e)
    {
        super(message, e);
    }
}
