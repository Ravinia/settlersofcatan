package server.exceptions;

public class InvalidCookie extends CatanServerException
{

	public InvalidCookie(String message)
	{
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8292029436473360423L;

}
