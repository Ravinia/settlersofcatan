package server.courier.deserializer;

import shared.definitions.CatanColor;

/**
 * @author jameson
 */
public class JoinGameInfo {

	private int gameId;
	private CatanColor color;
	
	public JoinGameInfo(int gameId, CatanColor color) {
		super();
		this.gameId = gameId;
		this.color = color;
	}
	
	public int getGameId() {
		return gameId;
	}
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	public CatanColor getColor() {
		return color;
	}
	public void setColor(CatanColor color) {
		this.color = color;
	}
	
}
