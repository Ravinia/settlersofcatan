package server.courier.deserializer;

import shared.definitions.ResourceType;

/**
 * 
 * @author jami
 *
 */
public class MaritimeTradeInfo {
	
	private int currentPlayer;
	private int ratio;
	private ResourceType inputResource;
	private ResourceType outputResource;
	
	public MaritimeTradeInfo(int currentPlayer, int ratio,
			ResourceType inputResource, ResourceType outputResource) {
		super();
		this.currentPlayer = currentPlayer;
		this.ratio = ratio;
		this.inputResource = inputResource;
		this.outputResource = outputResource;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public int getRatio() {
		return ratio;
	}
	public void setRatio(int ratio) {
		this.ratio = ratio;
	}
	public ResourceType getInputResource() {
		return inputResource;
	}
	public void setInputResource(ResourceType inputResource) {
		this.inputResource = inputResource;
	}
	public ResourceType getOutputResource() {
		return outputResource;
	}
	public void setOutputResource(ResourceType outputResource) {
		this.outputResource = outputResource;
	}

}
