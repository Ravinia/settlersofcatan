package server.courier.deserializer;

public class LoadGameInfo
{
	private String gameName;
	
	public LoadGameInfo(String name)
	{
		this.gameName = name;
	}
	
	public String getGameName()
	{
		return gameName;
	}

	public void setGameName(String name)
	{
		this.gameName = name;
	}
}
