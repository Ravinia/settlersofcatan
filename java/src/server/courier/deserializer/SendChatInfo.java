package server.courier.deserializer;

/**
 * 
 * @author jameson
 *
 */
public class SendChatInfo {

	private int currentPlayer;
	private String content;
	
	public SendChatInfo(int currentPlayer, String content) {
		super();
		this.currentPlayer = currentPlayer;
		this.content = content;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
