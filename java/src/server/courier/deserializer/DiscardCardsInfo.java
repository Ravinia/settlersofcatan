package server.courier.deserializer;

import server.model.game.resources.ResourceHand;

/**
 * 
 * @author jameson
 *
 */
public class DiscardCardsInfo {

	private int currentPlayer;
	private ResourceHand discardedCards;
	
	public DiscardCardsInfo(int currentPlayer, ResourceHand discardedCards) {
		super();
		this.currentPlayer = currentPlayer;
		this.discardedCards = discardedCards;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public ResourceHand getDiscardedCards() {
		return discardedCards;
	}
	public void setDiscardedCards(ResourceHand discardedCards) {
		this.discardedCards = discardedCards;
	}

}
