package server.courier.deserializer;

/**
 * 
 * @author jameson
 *
 */
public class AddAIInfo {

	private String AIName;
	
	public AddAIInfo(String aIName) {
		super();
		AIName = aIName;
	}

	public String getAIName() {
		return AIName;
	}

	public void setAIName(String aIName) {
		AIName = aIName;
	}

}
