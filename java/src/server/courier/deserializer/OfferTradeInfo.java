package server.courier.deserializer;

import java.util.Map;

import shared.definitions.ResourceType;

public class OfferTradeInfo {

	private int currentPlayer;
	private Map<ResourceType, Integer> offer;
	private int receiver;
	
	public OfferTradeInfo(int currentPlayer, Map<ResourceType, Integer> offer,
			int receiver) {
		super();
		this.currentPlayer = currentPlayer;
		this.offer = offer;
		this.receiver = receiver;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public Map<ResourceType, Integer> getOffer() {
		return offer;
	}
	public void setOffer(Map<ResourceType, Integer> offer) {
		this.offer = offer;
	}
	public int getReceiver() {
		return receiver;
	}
	public void setReceiver(int receiver) {
		this.receiver = receiver;
	}
	
}
