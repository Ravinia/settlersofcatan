package server.courier.deserializer;

import shared.locations.VertexLocation;

/**
 * 
 * @author jameson
 *
 */
public class BuildCityInfo {

	private int currentPlayer;
	private VertexLocation location;
	
	public BuildCityInfo(int currentPlayer, VertexLocation location) {
		super();
		this.currentPlayer = currentPlayer;
		this.location = location;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public VertexLocation getLocation() {
		return location;
	}
	public void setLocation(VertexLocation location) {
		this.location = location;
	}
	
}
