package server.courier.deserializer;

import java.util.List;

import shared.debug.Command;

/**
 * 
 * @author jameson
 *
 */
public class PutGameCommandsInfo {

	private List<Command> commands;

	public PutGameCommandsInfo(List<Command> commands) {
		super();
		this.commands = commands;
	}

	public List<Command> getCommands() {
		return commands;
	}

	public void setCommands(List<Command> commands) {
		this.commands = commands;
	}
	
}
