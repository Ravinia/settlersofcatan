package server.courier.deserializer;

/**
 * 
 * @author jameson
 *
 */
public class AcceptTradeInfo {
	
	private int currentPlayer;
	private boolean willAccept;
	
	public AcceptTradeInfo(int currentPlayer, boolean willAccept) {
		super();
		this.currentPlayer = currentPlayer;
		this.willAccept = willAccept;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public boolean isWillAccept() {
		return willAccept;
	}
	public void setWillAccept(boolean willAccept) {
		this.willAccept = willAccept;
	}
	
}
