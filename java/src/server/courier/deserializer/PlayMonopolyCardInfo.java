package server.courier.deserializer;

import shared.definitions.ResourceType;

/**
 * 
 * @author jameson
 *
 */
public class PlayMonopolyCardInfo {

	private int currentPlayer;
	private ResourceType resource;
	
	public PlayMonopolyCardInfo(int currentPlayer, ResourceType resource) {
		super();
		this.currentPlayer = currentPlayer;
		this.resource = resource;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public ResourceType getResource() {
		return resource;
	}
	public void setResource(ResourceType resource) {
		this.resource = resource;
	}
	
}
