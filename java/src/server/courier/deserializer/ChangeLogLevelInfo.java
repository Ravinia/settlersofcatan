package server.courier.deserializer;

import shared.debug.LogLevel;

/**
 * 
 * @author jameson
 *
 */
public class ChangeLogLevelInfo {

	private LogLevel logLevel;
	
	public ChangeLogLevelInfo(LogLevel logLevel) {
		super();
		this.logLevel = logLevel;
	}

	public LogLevel getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(LogLevel logLevel) {
		this.logLevel = logLevel;
	}
	
}
