package server.courier.deserializer;

import shared.locations.EdgeLocation;

/**
 * 
 * @author jameson
 *
 */
public class BuildRoadInfo {

	private int currentPlayer;
	private boolean free;
	private EdgeLocation roadLocation;
	
	public BuildRoadInfo(int currentPlayer, boolean free,
			EdgeLocation roadLocation) {
		super();
		this.currentPlayer = currentPlayer;
		this.free = free;
		this.roadLocation = roadLocation;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public boolean isFree() {
		return free;
	}
	public void setFree(boolean free) {
		this.free = free;
	}
	public EdgeLocation getRoadLocation() {
		return roadLocation;
	}
	public void setRoadLocation(EdgeLocation roadLocation) {
		this.roadLocation = roadLocation;
	}
	
	
}
