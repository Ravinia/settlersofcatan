package server.courier.deserializer;

import shared.locations.HexLocation;

/**
 * 
 * @author jameson
 *
 */
public class PlaySoldierCardInfo {

	private int currentPlayer;
	private HexLocation location;
	private int victimIndex;
	
	public PlaySoldierCardInfo(int currentPlayer, HexLocation location,
			int victimIndex) {
		super();
		this.currentPlayer = currentPlayer;
		this.location = location;
		this.victimIndex = victimIndex;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public HexLocation getLocation() {
		return location;
	}
	public void setLocation(HexLocation location) {
		this.location = location;
	}
	public int getVictimIndex() {
		return victimIndex;
	}
	public void setVictimIndex(int victimIndex) {
		this.victimIndex = victimIndex;
	}
	
}
