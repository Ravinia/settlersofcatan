package server.courier.deserializer;

/**
 * 
 * @author jami
 *
 */
public class PlayMonumentCardInfo {

	private int currentPlayer;

	public PlayMonumentCardInfo(int currentPlayer) {
		super();
		this.currentPlayer = currentPlayer;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	
}
