package server.courier.deserializer;

/**
 * 
 * @author jameson
 *
 */
public class RollNumberInfo {
	
	private int currentPlayer;
	private int number;
	
	public RollNumberInfo(int currentPlayer, int number) {
		super();
		this.currentPlayer = currentPlayer;
		this.number = number;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
}
