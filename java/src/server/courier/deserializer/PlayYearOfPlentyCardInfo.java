package server.courier.deserializer;

import shared.definitions.ResourceType;

/**
 * 
 * @author jameson
 *
 */
public class PlayYearOfPlentyCardInfo {

	private int currentPlayer;
	private ResourceType resource1;
	private ResourceType resource2;
	
	public PlayYearOfPlentyCardInfo(int currentPlayer, ResourceType resource1,
			ResourceType resource2) {
		super();
		this.currentPlayer = currentPlayer;
		this.resource1 = resource1;
		this.resource2 = resource2;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public ResourceType getResource1() {
		return resource1;
	}
	public void setResource1(ResourceType resource1) {
		this.resource1 = resource1;
	}
	public ResourceType getResource2() {
		return resource2;
	}
	public void setResource2(ResourceType resource2) {
		this.resource2 = resource2;
	}
	
}
