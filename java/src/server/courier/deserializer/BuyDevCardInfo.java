package server.courier.deserializer;

/**
 * 
 * @author jameson
 *
 */
public class BuyDevCardInfo {

	private int currentPlayer;
	
	public BuyDevCardInfo(int currentPlayer) {
		super();
		this.currentPlayer = currentPlayer;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	
}
