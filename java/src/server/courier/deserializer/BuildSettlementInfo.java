package server.courier.deserializer;

import shared.locations.VertexLocation;

/**
 * 
 * @author jameson
 *
 */
public class BuildSettlementInfo {

	private int currentPlayer;
	private VertexLocation location;
	private boolean free;
	
	public BuildSettlementInfo(int currentPlayer, VertexLocation location,
			boolean free) {
		super();
		this.currentPlayer = currentPlayer;
		this.location = location;
		this.free = free;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public VertexLocation getLocation() {
		return location;
	}
	public void setLocation(VertexLocation location) {
		this.location = location;
	}
	public boolean isFree() {
		return free;
	}
	public void setFree(boolean free) {
		this.free = free;
	}
}
