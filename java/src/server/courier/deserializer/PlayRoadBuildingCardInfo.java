package server.courier.deserializer;

import shared.locations.EdgeLocation;

/**
 * 
 * @author jameson
 *
 */
public class PlayRoadBuildingCardInfo {

	private int currentPlayer;
	private EdgeLocation spot1;
	private EdgeLocation spot2;
	
	public PlayRoadBuildingCardInfo(int currentPlayer, EdgeLocation spot1,
			EdgeLocation spot2) {
		super();
		this.currentPlayer = currentPlayer;
		this.spot1 = spot1;
		this.spot2 = spot2;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public EdgeLocation getSpot1() {
		return spot1;
	}
	public void setSpot1(EdgeLocation spot1) {
		this.spot1 = spot1;
	}
	public EdgeLocation getSpot2() {
		return spot2;
	}
	public void setSpot2(EdgeLocation spot2) {
		this.spot2 = spot2;
	}
	
}
