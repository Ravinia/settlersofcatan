package server.courier.deserializer;

import shared.locations.HexLocation;

/**
 * @author jameson
 */
public class RobPlayerInfo {

	private int currentPlayer;
	private int victimIndex;
	private HexLocation location;
	
	public RobPlayerInfo(int playerIndex, int victimIndex, HexLocation location) {
		super();
		this.currentPlayer = playerIndex;
		this.victimIndex = victimIndex;
		this.location = location;
	}
	
	public int getPlayerIndex() {
		return currentPlayer;
	}
	public void setPlayerIndex(int playerIndex) {
		this.currentPlayer = playerIndex;
	}
	public int getVictimIndex() {
		return victimIndex;
	}
	public void setVictimIndex(int victimIndex) {
		this.victimIndex = victimIndex;
	}
	public HexLocation getLocation() {
		return location;
	}
	public void setLocation(HexLocation location) {
		this.location = location;
	}
}
