package server.courier.deserializer;

/**
 * 
 * @author jameson
 *
 */
public class FinishTurnInfo {

	private int currentPlayer;
	
	public FinishTurnInfo(int currentPlayer) {
		super();
		this.currentPlayer = currentPlayer;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	
}
