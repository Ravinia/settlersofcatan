package server.courier.deserializer;

/**
 * 
 * @author jami
 *
 */
public class PlayDevCardInfo {

	private int currentPlayer;

	public PlayDevCardInfo(int currentPlayer) {
		super();
		this.currentPlayer = currentPlayer;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	
}
