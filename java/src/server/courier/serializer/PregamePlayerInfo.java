package server.courier.serializer;

import server.exceptions.UninitializedDataException;
import shared.definitions.CatanColor;

/**
 * Courier class for information for pregame server.model.player information purposes.
 *
 * @author Lawrence
 */
public interface PregamePlayerInfo
{
    public int getId();

    public CatanColor getColor() throws UninitializedDataException;

    public String getName() throws UninitializedDataException;
}
