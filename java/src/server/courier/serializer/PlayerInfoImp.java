package server.courier.serializer;

import server.factories.DevelopmentCardFactory;
import server.model.game.developmentcard.DevelopmentCardHand;
import server.model.game.resources.ResourceHand;
import shared.definitions.CatanColor;

/**
 * @author Lawrence
 */
public class PlayerInfoImp implements PlayerInfo
{
    private int ID = 12;
    private String name = "";
    private CatanColor color;
    private int index = 0;
    private int settlements = 5;
    private int cities = 4;
    private int roads = 15;
    private int soldiers = 0;
    private int monuments = 0;
    private boolean discarded = false;
    private boolean playedDevCard = false;
    private ResourceHand resources;
    private DevCardHandInfo devCardHand;
    private int victoryPoints = 0;


    public PlayerInfoImp(int ID, String name, CatanColor color, int index, int settlements, int cities, int roads,
                         int soldiers, int monuments, int victoryPoints, boolean discarded, boolean playedDevCard, ResourceHand resources,
                         DevelopmentCardHand devCardHand)
    {
        this.ID = ID;
        this.name = name;
        this.color = color;
        this.index = index;
        this.settlements = settlements;
        this.cities = cities;
        this.roads = roads;
        this.soldiers = soldiers;
        this.monuments = monuments;
        this.victoryPoints = victoryPoints;
        this.discarded = discarded;
        this.playedDevCard = playedDevCard;
        this.resources = resources;
        if (devCardHand != null)
        	this.devCardHand = DevelopmentCardFactory.convertHand(devCardHand);
        else
        	this.devCardHand = null;
    }

    @Override
    public int getId()
    {
        return ID;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public CatanColor getColor()
    {
        return color;
    }

    @Override
    public int getIndex()
    {
        return index;
    }

    @Override
    public int getNumRemainingCities()
    {
        return cities;
    }

    @Override
    public int getNumRemainingSettlements()
    {
        return settlements;
    }

    @Override
    public int getNumRemainingRoads()
    {
        return roads;
    }

    @Override
    public int getNumSoldiersPlayed()
    {
        return soldiers;
    }

    @Override
    public int getNumMonumentsPlayed()
    {
        return monuments;
    }

    @Override
    public boolean hasDiscarded()
    {
        return discarded;
    }

    @Override
    public boolean hasPlayedDevCardThisTurn()
    {
        return playedDevCard;
    }

    @Override
    public ResourceHand getResourceHand()
    {
        return resources;
    }

    @Override
    public DevCardHandInfo getDevCardHand()
    {
        return devCardHand;
    }

    @Override
    public int getVictoryPoints()
    {
        return victoryPoints;
    }
}
