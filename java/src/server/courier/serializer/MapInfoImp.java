package server.courier.serializer;

import shared.locations.HexLocation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Implementation of map info class
 * @author Lawrence
 */
public class MapInfoImp implements MapInfo
{
    public static final int MAP_RADIUS = 3;

    private List<HexInfo> hexes;
    private List<PortInfo> ports;
    private List<RoadInfo> roads;
    private List<SettlementInfo> settlements;
    private List<SettlementInfo> cities;
    private HexLocation robberLocation;

    public MapInfoImp(List<HexInfo> hexes, List<PortInfo> ports, List<RoadInfo> roads, Collection<SettlementInfo> allSettlements, HexLocation robberLocation)
    {
        this.hexes = hexes;
        this.ports = ports;
        this.roads = roads;
        setSettlements(allSettlements);
        setCities(allSettlements);
        this.robberLocation = robberLocation;
    }

    @Override
    public List<HexInfo> getHexes()
    {
        return hexes;
    }

    @Override
    public List<PortInfo> getPorts()
    {
        return ports;
    }

    @Override
    public List<RoadInfo> getRoads()
    {
        return roads;
    }

    @Override
    public List<SettlementInfo> getSettlements()
    {
        return settlements;
    }

    @Override
    public List<SettlementInfo> getCities()
    {
        return cities;
    }

    @Override
    public int getRadius()
    {
        return MAP_RADIUS;
    }

    @Override
    public HexLocation getRobberLocation()
    {
        return robberLocation;
    }

    //Helper Methods
    private void setSettlements(Collection<SettlementInfo> pieces)
    {
        List<SettlementInfo> result = new ArrayList<>();
        for (SettlementInfo piece : pieces)
        {
            if (!piece.isCity())
                result.add(piece);
        }
        settlements = result;
    }

    private void setCities(Collection<SettlementInfo> pieces)
    {
        List<SettlementInfo> result = new ArrayList<>();
        for (SettlementInfo piece : pieces)
        {
            if (piece.isCity())
                result.add(piece);
        }
        cities = result;
    }
}
