package server.courier.serializer;

import server.exceptions.NoCurrentTradesException;

import java.util.List;

/**
 * Model that contains information for easy serialization
 * @author Lawrence
 */
public interface GameModelInfo
{
    /**
     * Returns an object that represents the game's bank
     */
    public BankInfo getBank();

    /**
     * Returns an object that represents the deck
     */
    public DeckInfo getDeck();

    /**
     * Gets an object that contains all of the chats
     */
    public ChatInfo getChats();

    /**
     * Gets an object that contains all of the logs
     */
    public LogInfo getLogs();

    /**
     * Gets the map info object.
     */
    public MapInfo getMap();

    /**
     * Returns a list of the players
     */
    public List<PlayerInfo> getPlayers();

    /**
     * Returns the current trade offer, if any.
     * If there is no current trade, throws a NoCurrentTradesException
     * @throws NoCurrentTradesException
     */
    public TradeOfferInfo getTradeOffer() throws NoCurrentTradesException;

    /**
     * Returns the turn tracker info object
     */
    public TurnTrackerInfo getTurnTracker();

    /**
     * Returns the current version
     */
    public int getVersion();

    /**
     * Returns the index of the winner
     * @return -1 if no winner, otherwise the winner's index
     */
    public int getWinnerIndex();
}
