package server.courier.serializer;

import shared.definitions.Phase;

/**
 * @author Lawrence
 */
public class TurnTrackerInfo
{
    private int currentPlayerIndex;
    private Phase currentPhase;
    private int longestRoadIndex;
    private int largestArmyIndex;

    public TurnTrackerInfo(int currentPlayerIndex, Phase currentPhase, int longestRoadIndex, int largestArmyIndex)
    {
        this.currentPlayerIndex = currentPlayerIndex;
        this.currentPhase = currentPhase;
        this.longestRoadIndex = longestRoadIndex;
        this.largestArmyIndex = largestArmyIndex;
    }

    public int getCurrentPlayerIndex()
    {
        return currentPlayerIndex;
    }

    public Phase getCurrentPhase()
    {
        return currentPhase;
    }

    public int getLongestRoadIndex()
    {
        return longestRoadIndex;
    }

    public int getLargestArmyIndex()
    {
        return largestArmyIndex;
    }
}
