package server.courier.serializer;

import shared.locations.EdgeLocation;

/**
 * Holds courier information for the road
 * @author Lawrence
 */
public class RoadInfo
{
    private int ownerIndex;
    private EdgeLocation location;

    public RoadInfo(int ownerIndex, EdgeLocation location)
    {
        this.ownerIndex = ownerIndex;
        this.location = location;
    }

    public int getOwnerIndex()
    {
        return ownerIndex;
    }

    public EdgeLocation getLocation()
    {
        return location;
    }
}
