package server.courier.serializer;

import server.model.game.chatlog.Message;

import java.util.Iterator;
import java.util.List;

/**
 * Abstract class that is implemented by chat info and log info.
 * Implements iterable so you can do a for each loop on it.
 * @author Lawrence
 */
public abstract class MessageInfo implements Iterable<Message>
{
    private List<Message> messages;

    public MessageInfo(List<Message> messages)
    {
        this.messages = messages;
    }

    @Override
    public Iterator<Message> iterator()
    {
        return messages.iterator();
    }

    public Message getMessage(int index)
    {
        return messages.get(index);
    }

    public int size()
    {
        return messages.size();
    }
}
