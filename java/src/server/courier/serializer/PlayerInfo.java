package server.courier.serializer;

import server.model.game.resources.ResourceHand;
import shared.definitions.CatanColor;

/**
 *Courier class used to store the server.model.player info for a server.model.player currently in a game that models the json deliverable format.
 *
 * @author Lawrence
 */
public interface PlayerInfo extends PregamePlayerInfo
{
    /**
     * The ID of the player
     * @return the player's unique ID
     */
    public int getId();

    /**
     * Gets the server.model.player's user name
     * @return the server.model.player's username
     */
    public String getName();

    /**
     * Gets the color of the server.model.player
     * @return server.model.player's color
     */
    public CatanColor getColor();

    /**
     * Gets the index of the server.model.player in this game
     * @return the server.model.player's index
     */
    public int getIndex();

    /**
     * Retrieves the number of remaining cities this server.model.player has in their build pool
     * @return number of cities left
     */
    public int getNumRemainingCities();

    /**
     * Retrieves the number of remaining settlements this server.model.player has in their build pool
     * @return number of settlements left
     */
    public int getNumRemainingSettlements();

    /**
     * Retrieves the number of remaining roads this server.model.player has in their build pool
     * @return number of roads left
     */
    public int getNumRemainingRoads();

    /**
     * Gets the number of soldier cards this server.model.player has played
     * @return number of soldier cards played
     */
    public int getNumSoldiersPlayed();

    /**
     * Gets the number of monument cards this server.model.player has played
     * @return number of monument cards played
     */
    public int getNumMonumentsPlayed();

    /**
     * Whether or not the server.model.player has discarded in this discard phase
     * @return true if so, false otherwise
     */
    public boolean hasDiscarded();

    /**
     * Whether or not the server.model.player has played a development card yet this turn
     * @return true if so, false otherwise
     */
    public boolean hasPlayedDevCardThisTurn();

    /**
     * Retrieves the ResourceHand object that contains all the resources this server.model.player has in their hand
     * @return the server.model.player's resource hand
     */
    public ResourceHand getResourceHand();

    /**
     * Retrieves the courier object that contains both the old and new dev cards
     * @return the player's development cards
     */
    public DevCardHandInfo getDevCardHand();

    /**
     * Returns the number of victory points that this player has
     */
    public int getVictoryPoints();
}
