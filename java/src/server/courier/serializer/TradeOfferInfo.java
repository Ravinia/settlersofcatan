package server.courier.serializer;

import server.model.game.resources.ResourceHand;

/**
 * Courier class that contains the info for convenient converting to JSON of a trade offer
 *
 * @author Lawrence
 */
public interface TradeOfferInfo
{
    /**
     * The sender's index for the game they are a part of
     * @return the sender's index
     */
    public int getSenderIndex();

    /**
     * The recipient's index for the game they are a part of
     * @return the receiver's index
     */
    public int getReceiverIndex();

    /**
     * Gets the offer where positive values are resources being offered, and negative values are resources being received
     * @return the offer
     */
    public ResourceHand getOffer();
}
