package server.courier.serializer;

import java.util.List;

/**
 * Courier class for a pregame game object that mimics the information stored in JSON.
 *
 * @author Lawrence
 */
public interface GameInfo
{
    /**
     * Retrieves the game's title
     * @return the game's title
     */
    public String getGameName();

    /**
     * Retrieves the game's unique ID
     * @return the game's ID
     */
    public int getGameID();

    /**
     * Returns a list of the players who are currently in the game.
     * The length of this list should always equal 4, however for each empty spot in the game where a server.model.player has not
     * joined the default server.model.player is used (empty server.model.player with index of -1)
     *
     * @return list of players in the game
     */
    public List<PregamePlayerInfo> getPlayers();
}
