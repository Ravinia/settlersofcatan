package server.courier.serializer;

import server.model.board.immutable.ports.Port;
import shared.definitions.PortType;
import shared.locations.EdgeDirection;
import shared.locations.HexLocation;

/**
 * Serializer info for a port
 * @author Lawrence
 */
public class PortInfo
{
    private EdgeDirection direction;
    private int ratio = 3;
    private PortType resourceType;
    private HexLocation location;

    public PortInfo(Port port)
    {
        direction = port.getLocation().getOriginalEdge().getDir();
        location = port.getLocation().getEdge().getHexLoc();
        resourceType = port.getPortType();
        if (resourceType != PortType.THREE)
            ratio = 2;
    }

    public EdgeDirection getDirection()
    {
        return direction;
    }

    public int getRatio()
    {
        return ratio;
    }

    public PortType getResourceType()
    {
        return resourceType;
    }

    public HexLocation getLocation()
    {
        return location;
    }
}
