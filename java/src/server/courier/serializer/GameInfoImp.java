package server.courier.serializer;

import java.util.List;

public class GameInfoImp implements GameInfo
{
	String gameName;
	int gameID;
	List<PregamePlayerInfo> info;
	
	public GameInfoImp(String gameName, int gameID, List<PregamePlayerInfo> info)
	{
		this.gameName = gameName;
		this.gameID = gameID;
		this.info = info;
	}
	
	@Override
	public String getGameName() {
		return gameName;
	}

	@Override
	public int getGameID() {
		return gameID;
	}

	@Override
	public List<PregamePlayerInfo> getPlayers() {
		return info;
	}

}
