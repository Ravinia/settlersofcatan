package server.courier.serializer;

import shared.locations.VertexLocation;

/**
 * Class for reference info for both settlements and cities
 * @author Lawrence
 */
public class SettlementInfo
{
    private int ownerIndex;
    private VertexLocation location;
    private boolean city;

    public SettlementInfo(int ownerIndex, VertexLocation location, boolean city)
    {
        this.ownerIndex = ownerIndex;
        this.location = location;
        this.city = city;
    }

    public int getOwnerIndex()
    {
        return ownerIndex;
    }

    public VertexLocation getLocation()
    {
        return location;
    }

    public boolean isCity()
    {
        return city;
    }
}
