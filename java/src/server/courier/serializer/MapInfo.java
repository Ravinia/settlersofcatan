package server.courier.serializer;

import shared.locations.HexLocation;

import java.util.List;

/**
 * Courier class for holding serializer info for the map
 * @author Lawrence
 */
public interface MapInfo
{
    public List<HexInfo> getHexes();

    public List<PortInfo> getPorts();

    public List<RoadInfo> getRoads();

    public List<SettlementInfo> getSettlements();

    public List<SettlementInfo> getCities();

    public int getRadius();

    public HexLocation getRobberLocation();
}
