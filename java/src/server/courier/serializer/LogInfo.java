package server.courier.serializer;

import server.model.game.chatlog.Message;

import java.util.List;

/**
 * Contains iterable list of log messages
 * @author Lawrence
 */
public class LogInfo extends MessageInfo
{
    public LogInfo(List<Message> chats)
    {
        super(chats);
    }
}
