package server.courier.serializer;

/**
 * @author Lawrence
 */
public class DeckInfo extends DevCardListInfo
{
    public DeckInfo(int monopoly, int monument, int roadBuilding, int soldier, int yearOfPlenty)
    {
        super(monopoly, monument, roadBuilding, soldier, yearOfPlenty);
    }
}
