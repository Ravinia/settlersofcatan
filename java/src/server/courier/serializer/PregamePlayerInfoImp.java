package server.courier.serializer;

import server.exceptions.UninitializedDataException;
import shared.definitions.CatanColor;
import static server.model.player.DefaultPlayer.*;

public class PregamePlayerInfoImp implements PregamePlayerInfo
{
	private int id;
	private String name;
	private CatanColor color;
	
//	public PregamePlayerInfoImp(String name, int id)
//	{
//		this.name = name;
//		this.id = id;
//	}
	
	public PregamePlayerInfoImp(String name, int id, CatanColor color)
	{
		this.name = name;
		this.id = id;
		this.color = color;
	}
	
	@Override
	public String getName() throws UninitializedDataException
	{
		if(name == null)
			throw new UninitializedDataException("server.model.player name");
		return name;
	}

	@Override
	public int getId()
	{
		return id;
	}

	@Override
	public CatanColor getColor() throws UninitializedDataException
    {
		if(color == null)
			throw new UninitializedDataException("server.model.player color");
		return color;
	}

    @Override
    public boolean equals(Object o)
    {
        if (id == DEFAULT_PLAYER_INDEX)
            return DEFAULT_PLAYER.equals(o);
        else
        {
            if (o instanceof PregamePlayerInfo)
            {
                try
                {
                    PregamePlayerInfo player = (PregamePlayerInfo) o;
                    return id == player.getId() && name.equals(player.getName());
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            else
                return false;
        }
    }
}
