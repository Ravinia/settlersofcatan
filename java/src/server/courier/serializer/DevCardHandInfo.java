package server.courier.serializer;

/**
 * @author Lawrence
 */
public class DevCardHandInfo
{
    private DevCardListInfo newDevCards;
    private DevCardListInfo oldDevCards;

    public DevCardHandInfo(DevCardListInfo newDevCards, DevCardListInfo oldDevCards)
    {
        this.newDevCards = newDevCards;
        this.oldDevCards = oldDevCards;
    }

    public DevCardListInfo getNewDevCards()
    {
        return newDevCards;
    }

    public DevCardListInfo getOldDevCards()
    {
        return oldDevCards;
    }
}
