package server.courier.serializer;

import server.model.game.chatlog.Message;

import java.util.List;

/**
 * Contains iterable list of chat messages
 * @author Lawrence
 */
public class ChatInfo extends MessageInfo
{
    public ChatInfo(List<Message> chats)
    {
        super(chats);
    }
}
