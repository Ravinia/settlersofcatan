package server.courier.serializer;

import server.exceptions.NoCurrentTradesException;

import java.util.List;

/**
 * @author Lawrence
 */
public class GameModelInfoImp implements GameModelInfo
{
    private BankInfo bank;
    private DeckInfo deck;
    private ChatInfo chats;
    private LogInfo logs;
    private MapInfo map;
    private List<PlayerInfo> players;
    private TradeOfferInfo tradeOffer = null;
    private TurnTrackerInfo turnTracker;
    private int version;
    private int winnerIndex = -1;

    public GameModelInfoImp(BankInfo bank, DeckInfo deck, ChatInfo chats, LogInfo logs, MapInfo map, List<PlayerInfo> players, TurnTrackerInfo turnTracker, int version)
    {
        this.bank = bank;
        this.deck = deck;
        this.chats = chats;
        this.logs = logs;
        this.map = map;
        this.players = players;
        this.turnTracker = turnTracker;
        this.version = version;
    }

    @Override
    public BankInfo getBank()
    {
        return bank;
    }

    @Override
    public DeckInfo getDeck()
    {
        return deck;
    }

    @Override
    public ChatInfo getChats()
    {
        return chats;
    }

    @Override
    public LogInfo getLogs()
    {
        return logs;
    }

    @Override
    public MapInfo getMap()
    {
        return map;
    }

    @Override
    public List<PlayerInfo> getPlayers()
    {
        return players;
    }

    @Override
    public TradeOfferInfo getTradeOffer() throws NoCurrentTradesException
    {
        if (tradeOffer == null)
            throw new NoCurrentTradesException();
        return tradeOffer;
    }

    @Override
    public TurnTrackerInfo getTurnTracker()
    {
        return turnTracker;
    }

    @Override
    public int getVersion()
    {
        return version;
    }

    @Override
    public int getWinnerIndex()
    {
        return winnerIndex;
    }

    public void setWinner(int winnerIndex)
    {
        this.winnerIndex = winnerIndex;
    }

    public void setTradeOffer(TradeOfferInfo tradeOffer)
    {
        this.tradeOffer = tradeOffer;
    }
}
