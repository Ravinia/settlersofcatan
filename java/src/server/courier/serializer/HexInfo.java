package server.courier.serializer;

import server.exceptions.DesertException;
import server.model.board.immutable.Hex;
import shared.definitions.HexType;
import shared.locations.HexLocation;

/**
 * Serializer info for a hex tile
 * @author Lawrence
 */
public class HexInfo
{
    private HexLocation location;
    private HexType resource;
    private int token;
    private boolean desert = false;

    public HexInfo(Hex hex)
    {
        this.location = hex.getLocation();
        if (hex.getType() == HexType.DESERT)
        {
            desert = true;
        }
        else
        {
            resource = hex.getType();
            token = hex.getToken().getValue();
        }
    }

    public HexLocation getLocation()
    {
        return location;
    }

    public HexType getResource() throws DesertException
    {
        if (desert)
            throw new DesertException();
        return resource;
    }

    public int getNumber() throws DesertException
    {
        if (desert)
            throw new DesertException();
        return token;
    }

    public boolean isDesert()
    {
        return desert;
    }
}
