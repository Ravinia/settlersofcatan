package server.httpserver;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import server.exceptions.NoEntryInRegistryForPluginName;

public class RegistryReader {

	public String getPackageOfPlugin(String pluginName) throws ParserConfigurationException, SAXException, IOException, NoEntryInRegistryForPluginName
	{
		File fXmlFile = new File("registry.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
	 
		doc.getDocumentElement().normalize();
	 
//		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	 
		NodeList plugins = doc.getElementsByTagName("plugin");
		for (int temp = 0; temp < plugins.getLength(); temp++) {
			 
			Node plugin = plugins.item(temp);
	 
//			System.out.println("\nCurrent Element :" + plugin.getNodeName());
	 
			if (plugin.getNodeType() == Node.ELEMENT_NODE) {
	 
				Element eElement = (Element) plugin;
	 
				if (eElement.getAttribute("name").equals(pluginName))
					return eElement.getAttribute("package");
			}
		}
		throw new NoEntryInRegistryForPluginName(pluginName);
	}
	
}
