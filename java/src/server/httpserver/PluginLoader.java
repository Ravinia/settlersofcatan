package server.httpserver;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import server.exceptions.NoEntryInRegistryForPluginName;
import server.persistence.PersistenceProvider;

public class PluginLoader {

	public PersistenceProvider loadPersistenceProviderPlugin(String pluginName, int threshold)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException, ParserConfigurationException, SAXException, IOException, NoEntryInRegistryForPluginName
	{
		File loc = new File("plugins");

        File[] flist = loc.listFiles(new FileFilter() {
            public boolean accept(File file) {return file.getPath().toLowerCase().endsWith(".jar");}
        });
        URL[] urls = new URL[flist.length];
        for (int i = 0; i < flist.length; i++)
            urls[i] = flist[i].toURI().toURL();
		
		URLClassLoader child = new URLClassLoader (urls, this.getClass().getClassLoader());
		RegistryReader rr = new RegistryReader();
		String packageName = rr.getPackageOfPlugin(pluginName);
		Class<?> classToLoad = Class.forName (packageName, true, child);
		return (PersistenceProvider) classToLoad.newInstance();
	}
	
}
