package server.httpserver;

import com.sun.net.httpserver.HttpHandler;

/**
 * This class represents the top level server. It has an HttpServer that
 * will listen for requests on certain urls and then call certian HttpHandlers
 * based on those urls.
 * @author Christian 
 *
 */
public interface Server
{
	/**
	 * Runs the server. This maps urls to their corresponding handlers.
	 * Also sets the domain and port that the server will run on
	 */
	public void run();
	
	/**
	 * Getter for the port
	 * @return the integer that represents the port
	 */
	public Integer getPort();
	
	/**
	 * Getter for the host
	 * @return the string that represents the host
	 */
	public String getHost();
	
	/**
	 * Setter for the host
	 * @param host the string that represents the host
	 */
	public void setHost(String host);
	
	/**
	 * Setter for the port
	 * @param port the integer that represents the port
	 */
	public void setPort(Integer port);

	/**
	 * Setter for the UserLoginHandler
	 * @param handler the server.handler that is to be the UserLoginHandler
	 */
	public void setUserLoginHandler(HttpHandler handler);
	
	/**
	 * Getter for the UserLoginHandler
	 * @return the httphandler that is the UserLoginHandler
	 */
	public HttpHandler getUserLoginHandler();
	
	/**
	 * Getter for the UserRegisterHandler
	 * @return the httphandler that is the UserRegisterHandler
	 */	
	public HttpHandler getUserRegisterHandler();
	
	/**
	 * Setter for the UserRegisterHandler
	 * @param handler the server.handler that is to be the UserRegisterHandler
	 */
	public void setUserRegisterHandler(HttpHandler handler);
	
	/**
	 * Setter for the GamesCreateHandler
	 * @param handler the server.handler that is to be the GamesCreateHandler
	 */
	public void setGamesCreateHandler(HttpHandler handler);
	
	/**
	 * Getter for the GamesCreateHandler
	 * @return the GamesCreateHandler
	 */
	public HttpHandler getGamesCreateHandler();
	
	/**
	 * Setter for the GamesJoinHandler
	 * @param handler 
	 */
	public void setGamesJoinHandler(HttpHandler handler);
	
	/**
	 * Getter for the GamesJoinHandler
	 * @return GamesJoinHandler
	 */
	public HttpHandler getGamesJoinHandler();
	
	/**
	 * Setter for the GamesListHandler
	 * @param handler
	 */
	public void setGamesListHandler(HttpHandler handler);
	
	/**
	 * Getter for the GamesListHandler
	 * @return GamesListHandler
	 */
	public HttpHandler getGamesListHandler();
	
	/**
	 * Setter for the GamesLoadHandler
	 * @param handler
	 */
	public void setGamesLoadHandler(HttpHandler handler);
	
	/**
	 * Getter for the GamesLoadHandler
	 * @return the GamesLoadHandler
	 */
	public HttpHandler getGamesLoadHandler();
	
	/**
	 * Setter for the GamesSaveHandler
	 * @param handler
	 */
	public void setGamesSaveHandler(HttpHandler handler);
	
	/**
	 * Getter for the GamesSaveHandler
	 * @return GamesSaveHandler
	 */
	public HttpHandler getGamesSaveHandler();
	
	/**
	 * Setter for the GameAddAIHandler
	 * @param handler
	 */
	public void setGameAddAIHandler(HttpHandler handler);
	
	/**
	 * Getter for the GameAddAIHandler
	 * @return GameAddAIHandler
	 */
	public HttpHandler getGameAddAIHandler();
	
	/**
	 * Setter for the GameCommandsHandler
	 * @param handler
	 */
	public void setGameCommandsHandler(HttpHandler handler);
	
	/**
	 * Getter for the GamesCommandsHandler
	 * @return GamesCommandsHandler
	 */
	public HttpHandler getGameCommandsHandler();
	
	/**
	 * Setter for the GameModelHandler
	 * @param handler
	 */
	public void setGameModelHandler(HttpHandler handler);
	
	/**
	 * Getter for the GameModelHandler
	 * @return GameModelHandler
	 */
	public HttpHandler getGameModelHandler();
	
	/**
	 * Setter for the GameResetHandler
	 * @param handler
	 */
	public void setGameResetHandler(HttpHandler handler);
	
	/**
	 * Getter for the GameResetHandler
	 * @return GameResetHandler
	 */
	public HttpHandler getGameResetHandler();
	
	/**
	 * Setter for the MovesAcceptTradeHandler
	 * @param handler
	 */
	public void setMovesAcceptTradeHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesAcceptTradeHandler
	 * @return MovesAcceptTradeHandler
	 */
	public HttpHandler getMovesAcceptTradeHandler();
	
	/**
	 * Setter for the MovesBuildCityHandler
	 * @param handler
	 */
	public void setMovesBuildCityHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesBuildCityHandler
	 * @return MovesBuildCityHandler
	 */
	public HttpHandler getMovesBuildCityHandler();
	
	/**
	 * Setter for the MovesBuildRoadHandler
	 * @param handler
	 */
	public void setMovesBuildRoadHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesBuildRoadHandler
	 * @return MovesBuildRoadHandler
	 */
	public HttpHandler getMovesBuildRoadHandler();
	
	/**
	 * Setter for the MovesBuildSettlementHandler
	 * @param handler
	 */
	public void setMovesBuildSettlementHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesBuildSettlementHandler
	 * @return MovesBuildSettlementHandler
	 */
	public HttpHandler getMovesBuildSettlementHandler();
	
	/**
	 * Setter for the MovesBuyDevCardHandler
	 * @param handler
	 */
	public void setMovesBuyDevCardHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesBuyDevCardHandler
	 * @return MovesBuyDevCardHandler
	 */
	public HttpHandler getMovesBuyDevCardHandler();
	
	/**
	 * Setter for the MovesDiscardCardsHandler
	 * @param handler
	 */
	public void setMovesDiscardCardsHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesDiscardCardsHandler
	 * @return MovesDiscardCardsHandler
	 */
	public HttpHandler getMovesDiscardCardsHandler();
	
	/**
	 * Setter for the MovesFinishTurnHandler
	 * @param handler
	 */
	public void setMovesFinishTurnHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesFinishTurnHandler
	 * @return MovesFinishTurnHandler
	 */
	public HttpHandler getMovesFinishTurnHandler();
	
	/**
	 * Setter for the MovesMaritimeTradeHandler
	 * @param handler
	 */
	public void setMovesMaritimeTradeHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesMaritimeTradeHandler
	 * @return MovesMaritimeTradeHandler
	 */
	public HttpHandler getMovesMaritimeTradeHandler();
	
	/**
	 * Setter for the MovesMonopolyHandler
	 * @param handler
	 */
	public void setMovesMonopolyHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesMonopolyHandler
	 * @return MovesMonopolyHandler
	 */
	public HttpHandler getMovesMonopolyHandler();
	
	/**
	 * Setter for the MovesMonumentHandler
	 * @param handler
	 */
	public void setMovesMonumentHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesMonumentHandler
	 * @return MovesMonumentHandler
	 */
	public HttpHandler getMovesMonumentHandler();
	
	/**
	 * Setter for the MovesOfferTradeHandler
	 * @param handler
	 */
	public void setMovesOfferTradeHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesOfferTradeHandler
	 * @return MovesOfferTradeHandler
	 */
	public HttpHandler getMovesOfferTradeHandler();
	
	/**
	 * Setter for the MovesRoadBuildingHandler
	 * @param handler
	 */
	public void setMovesRoadBuildingHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesRoadBuildingHandler
	 * @return MovesRoadBuildingHandler
	 */
	public HttpHandler getMovesRoadBuildingHandler();
	
	/**
	 * Setter for the MovesRobPlayerHandler
	 * @param handler
	 */
	public void setMovesRobPlayerHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesRobPlayerHandler
	 * @return MovesRobPlayerHandler
	 */
	public HttpHandler getMovesRobPlayerHandler();
	
	/**
	 * Setter for the MovesRollNumberHandler
	 * @param handler
	 */
	public void setMovesRollNumberHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesRollNumberHandler
	 * @return MovesRollNumberHandler
	 */
	public HttpHandler getMovesRollNumberHandler();
	
	/**
	 * Setter for the MovesSendChatHandler
	 * @param handler
	 */
	public void setMovesSendChatHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesSendChatHandler
	 * @return MovesSendChatHandler
	 */
	public HttpHandler getMovesSendChatHandler();
	
	/**
	 * Setter for the MovesSoldierHandler
	 * @param handler
	 */
	public void setMovesSoldierHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesSoldierHandler
	 * @return MovesSoldierHandler
	 */
	public HttpHandler getMovesSoldierHandler();
	
	/**
	 * Setter for the MovesYearOfPlentyHandler
	 * @param handler
	 */
	public void setMovesYearOfPlentyHandler(HttpHandler handler);
	
	/**
	 * Getter for the MovesYearOfPlentyHandler
	 * @return MovesYearOfPlentyHandler
	 */
	public HttpHandler getMovesYearOfPlentyHandler();
	
	/**
	 * Getter for the UtilChangeLogLevel
	 * @return UtilChangeLogLevel
	 */
	public HttpHandler getUtilChangeLogLevel();
	
	/**
	 * Setter for the UtilChangeLogLevel
	 * @param handler
	 */
	public void setUtilChangeLogLevel(HttpHandler handler);
	
	public void setGameListAI(HttpHandler handler);
	
	public HttpHandler getGameListAI();
}
