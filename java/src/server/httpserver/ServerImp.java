package server.httpserver;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import server.command.CommandObject;
import server.courier.serializer.PregamePlayerInfo;
import server.exceptions.BankOutOfResourceException;
import server.exceptions.CatanServerException;
import server.exceptions.GameDoesNotExistException;
import server.exceptions.NoEntryInRegistryForPluginName;
import server.facade.*;
import server.handler.swagger.Handlers;
import server.handler.user.*;
import server.handler.game.*;
import server.handler.games.*;
import server.handler.moves.*;
import server.handler.util.*;
import server.model.GameModel;
import server.model.ServerModel;
import server.model.ServerModelImp;
import server.model.game.Game;
import server.persistence.CommandsDAO;
import server.persistence.GamesDAO;
import server.persistence.PersistenceProvider;
import server.persistence.UsersDAO;
import server.serializer.*;
import shared.definitions.CatanColor;
import shared.exceptions.InvalidLocationException;
import shared.exceptions.InvalidObjectTypeException;

public class ServerImp implements Server
{
	private static final int MAX_WAITING_CONNECTIONS = 10;
	private String host;
	private int port;
	private HttpServer server;
	
	private HttpHandler userLogin;
	private HttpHandler userRegister;
	private HttpHandler gamesList;
	private HttpHandler gamesCreate;
	private HttpHandler gamesJoin;
	private HttpHandler gamesSave;
	private HttpHandler gamesLoad;
	private HttpHandler gameModel;
	private HttpHandler gameReset;
	private HttpHandler gameCommands;
	private HttpHandler gameAddAI;
	private HttpHandler gamelistAI;
	private HttpHandler utilChangeLogLevel;
	private HttpHandler movesSendChat;
	private HttpHandler movesRollNumber;
	private HttpHandler movesRobPlayer;
	private HttpHandler movesFinishTurn;
	private HttpHandler movesBuyDevCard;
	private HttpHandler movesYearOfPlenty;
	private HttpHandler movesRoadBuilding;
	private HttpHandler movesSoldier;
	private HttpHandler movesMonopoly;
	private HttpHandler movesMonument;
	private HttpHandler movesBuildRoad;
	private HttpHandler movesBuildSettlement;
	private HttpHandler movesBuildCity;
	private HttpHandler movesOfferTrade;
	private HttpHandler movesAcceptTrade;
	private HttpHandler movesMaritimeTrade;
	private HttpHandler movesDiscardCards;
	
	PersistenceProvider persistenceProvider;
	GamesDAO gamesDAO;
	CommandsDAO commandsDAO;
	UsersDAO usersDAO;
	
	public ServerImp(String host, int port, String pluginName, int threshold)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException, ParserConfigurationException, SAXException, NoEntryInRegistryForPluginName, GameDoesNotExistException
	{
		this.host = host;
		this.port = port;
		
		PluginLoader pl = new PluginLoader();
		persistenceProvider = pl.loadPersistenceProviderPlugin(pluginName, threshold);
		
		this.setHandlers(threshold);
	}
	
	private ServerModel bulkLoadServerModel() throws ClassNotFoundException, IOException, GameDoesNotExistException
	{
		gamesDAO = persistenceProvider.getGamesDAO();
		usersDAO = persistenceProvider.getUsersDAO();
		commandsDAO = persistenceProvider.getCommandsDAO();
		
		persistenceProvider.startTX();
		ServerModelImp serverModel = new ServerModelImp(gamesDAO.getGames(), usersDAO.getUsers());
		for (Game gm : serverModel.getGames())
		{
			int gameID = gm.getID();
			List<CommandObject> commands = commandsDAO.getCommands(gameID);
			
			for (CommandObject command : commands)
			{
				try {
					command.setServerModel(serverModel);
					command.executeCommand();
				} catch (CatanServerException
						| InvalidObjectTypeException | InvalidLocationException
						| BankOutOfResourceException e) {
					// We don't really care.
				}
			}
			
			commandsDAO.deleteCommands(gameID);
			GameModel gameModel = serverModel.getGame(gameID);
			gamesDAO.deleteGame(gameID);
			gamesDAO.addGame(gameModel);
		}
		serverModel.setNextGameID(gamesDAO.getGames().size());
		persistenceProvider.endTX(true);
		//this code updates the id's held in the serverModel to match the ones from the bulk load
		
		//serverModel.setNextUserID(usersDAO.getUsers().size());
		
		return serverModel;
	}
	
	private void setHandlers(int threshold) throws ClassNotFoundException, IOException, GameDoesNotExistException
	{
		ServerModel model = bulkLoadServerModel();
		
		ServerJsonDeserializer jsonDeserial = new ServerJsonDeserializerImp();
		ServerHttpDeserializer httpDeserial = new ServerHttpDeserializerImp();
		ServerSerializer serializer = new ServerSerializerImp();
		
		UserFacade userFacade = new UserFacadeImp(model, persistenceProvider, usersDAO);
		GameFacade gameFacade = new GameFacadeImp(model);
		MovesFacade movesFacade = new MovesFacadeImp(model, persistenceProvider, gamesDAO, commandsDAO, threshold);
		GamesFacade gamesFacade = new GamesFacadeImp(model, persistenceProvider, gamesDAO, movesFacade);
		UtilFacade utilFacade = new UtilFacadeImp(model);
		
		// Create a default game
		if (model.getGames().size() == 0)
		{
			try {
				Game game = gamesFacade.createGame(true, true, true, "perginant unfutibility");
				PregamePlayerInfo sam = userFacade.register("Sam", "sam");
				PregamePlayerInfo brooke =userFacade.register("Brooke", "brooke");
				PregamePlayerInfo jam =userFacade.register("Jam", "jam");
				PregamePlayerInfo jack =userFacade.register("Jack", "jack");
				if(sam != null)
					gamesFacade.joinGame(sam.getId(), sam.getName(), game.getID(), CatanColor.BLUE);
				if(brooke != null)
					gamesFacade.joinGame(brooke.getId(), brooke.getName(), game.getID(), CatanColor.GREEN);
				if(jam != null)
					gamesFacade.joinGame(jam.getId(), jam.getName(), game.getID(), CatanColor.PURPLE);
				if(jack != null)
					gamesFacade.joinGame(jack.getId(), jack.getName(), game.getID(), CatanColor.ORANGE);
			} catch (CatanServerException | IOException e) {
				e.printStackTrace();
			}
		}

		this.setGameAddAIHandler(new GameAddAI(gameFacade, jsonDeserial, httpDeserial, serializer));
		this.setGameCommandsHandler(new GameCommands(gameFacade, jsonDeserial, httpDeserial, serializer));
		this.setGameListAI(new GameListAI(gameFacade, jsonDeserial, httpDeserial, serializer));
		this.setGameModelHandler(new server.handler.game.GameModel(gameFacade, jsonDeserial, httpDeserial, serializer));
		this.setGameResetHandler(new GameReset(gameFacade, jsonDeserial, httpDeserial, serializer));
		
		this.setGamesCreateHandler(new GamesCreate(gamesFacade, jsonDeserial, httpDeserial, serializer));
		this.setGamesJoinHandler(new GamesJoin(gamesFacade, jsonDeserial, httpDeserial, serializer));
		this.setGamesListHandler(new GamesList(gamesFacade, jsonDeserial, httpDeserial, serializer));
		this.setGamesLoadHandler(new GamesLoad(gamesFacade, jsonDeserial, httpDeserial, serializer));
		this.setGamesSaveHandler(new GamesSave(gamesFacade, jsonDeserial, httpDeserial, serializer));
		
		this.setUserLoginHandler(new UserLogin(userFacade, jsonDeserial, httpDeserial, serializer));
		this.setUserRegisterHandler(new UserRegister(userFacade, jsonDeserial, httpDeserial, serializer));

		this.setUtilChangeLogLevel(new UtilChangeLogLevel(utilFacade, jsonDeserial, httpDeserial, serializer));
		
		this.setMovesAcceptTradeHandler(new MovesAcceptTrade(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesBuildCityHandler(new MovesBuildCity(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesBuildRoadHandler(new MovesBuildRoad(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesBuildSettlementHandler(new MovesBuildSettlement(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesBuyDevCardHandler(new MovesBuyDevCard(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesDiscardCardsHandler(new MovesDiscardCards(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesFinishTurnHandler(new MovesFinishTurn(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesMaritimeTradeHandler(new MovesMaritimeTrade(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesMonopolyHandler(new MovesMonopoly(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesMonumentHandler(new MovesMonument(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesOfferTradeHandler(new MovesOfferTrade(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesRoadBuildingHandler(new MovesRoadBuilding(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesRobPlayerHandler(new MovesRobPlayer(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesRollNumberHandler(new MovesRollNumber(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesSendChatHandler(new MovesSendChat(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesSoldierHandler(new MovesSoldier(movesFacade, jsonDeserial, httpDeserial, serializer));
		this.setMovesYearOfPlentyHandler(new MovesYearOfPlenty(movesFacade, jsonDeserial, httpDeserial, serializer));
	}
	
	@Override
	public void run()
	{
		
		try {
			server = HttpServer.create(new InetSocketAddress(this.port),
					MAX_WAITING_CONNECTIONS);
		} catch (IOException e) {

			e.printStackTrace();
			return;
		}

		server.setExecutor(null); // use the default executor

		server.createContext("/util/changeLogLevel", this.utilChangeLogLevel);

		server.createContext("/user/login", this.userLogin);
		server.createContext("/user/register", this.userRegister);
		
		server.createContext("/game/model", this.gameModel);
		server.createContext("/game/reset", this.gameReset);
		server.createContext("/game/commands", this.gameCommands);
		server.createContext("/game/addAI", this.gameAddAI);
		server.createContext("/game/listAI", this.gamelistAI);
		
		server.createContext("/games/list", this.gamesList);
		server.createContext("/games/create", this.gamesCreate);
		server.createContext("/games/join", this.gamesJoin);
		server.createContext("/games/save", this.gamesSave);
		server.createContext("/games/load", this.gamesLoad);
		
		server.createContext("/moves/sendChat", this.movesSendChat);
		server.createContext("/moves/rollNumber", this.movesRollNumber);
		server.createContext("/moves/robPlayer", this.movesRobPlayer);
		server.createContext("/moves/finishTurn", this.movesFinishTurn);
		server.createContext("/moves/buyDevCard", this.movesBuyDevCard);
		server.createContext("/moves/Year_of_Plenty", this.movesYearOfPlenty);
		server.createContext("/moves/Road_Building", this.movesRoadBuilding);
		server.createContext("/moves/Soldier", this.movesSoldier);
		server.createContext("/moves/Monopoly", this.movesMonopoly);
		server.createContext("/moves/Monument", this.movesMonument);
		server.createContext("/moves/buildRoad", this.movesBuildRoad);
		server.createContext("/moves/buildSettlement", this.movesBuildSettlement);
		server.createContext("/moves/buildCity", this.movesBuildCity);
		server.createContext("/moves/offerTrade", this.movesOfferTrade);
		server.createContext("/moves/acceptTrade", this.movesAcceptTrade);
		server.createContext("/moves/maritimeTrade", this.movesMaritimeTrade);
		server.createContext("/moves/discardCards", this.movesDiscardCards);
		
		server.createContext("/docs/api/data", new Handlers.JSONAppender(""));
		server.createContext("/docs/api/view", new Handlers.BasicFile(""));
		
		server.start();
	}

	@Override
	public Integer getPort()
	{
		return this.port;
	}

	@Override
	public String getHost()
	{
		return this.host;
	}

	@Override
	public void setHost(String host)
	{
		this.host = host;
	}

	@Override
	public void setPort(Integer port)
	{
		this.port = port;
	}

	@Override
	public void setUserLoginHandler(HttpHandler handler)
	{
		this.userLogin = handler;
	}

	@Override
	public HttpHandler getUserLoginHandler()
	{
		return this.userLogin;
	}

	@Override
	public HttpHandler getUserRegisterHandler()
	{
		return this.userRegister;
	}

	@Override
	public void setUserRegisterHandler(HttpHandler handler)
	{
		this.userRegister = handler;
	}

	@Override
	public void setGamesCreateHandler(HttpHandler handler)
	{
		this.gamesCreate = handler;
	}

	@Override
	public HttpHandler getGamesCreateHandler()
	{
		return this.gamesCreate;
	}

	@Override
	public void setGamesJoinHandler(HttpHandler handler)
	{
		this.gamesJoin = handler;
	}

	@Override
	public HttpHandler getGamesJoinHandler()
	{
		return this.gamesJoin;
	}

	@Override
	public void setGamesListHandler(HttpHandler handler)
	{
		this.gamesList = handler;
	}

	@Override
	public HttpHandler getGamesListHandler()
	{
		return this.gamesList;
	}

	@Override
	public void setGamesLoadHandler(HttpHandler handler)
	{
		this.gamesLoad = handler;
	}

	@Override
	public HttpHandler getGamesLoadHandler()
	{
		return this.gamesLoad;
	}

	@Override
	public void setGamesSaveHandler(HttpHandler handler)
	{
		this.gamesSave = handler;
	}

	@Override
	public HttpHandler getGamesSaveHandler()
	{
		return this.gamesSave;
	}

	@Override
	public void setGameAddAIHandler(HttpHandler handler)
	{
		this.gameAddAI = handler;
	}

	@Override
	public HttpHandler getGameAddAIHandler()
	{
		return this.gameAddAI;
	}

	@Override
	public void setGameCommandsHandler(HttpHandler handler)
	{
		this.gameCommands = handler;
	}

	@Override
	public HttpHandler getGameCommandsHandler()
	{
		return this.gameCommands;
	}

	@Override
	public void setGameModelHandler(HttpHandler handler)
	{
		this.gameModel = handler;
	}

	@Override
	public HttpHandler getGameModelHandler()
	{
		return this.gameModel;
	}

	@Override
	public void setGameResetHandler(HttpHandler handler)
	{
		this.gameReset = handler;
	}

	@Override
	public HttpHandler getGameResetHandler()
	{
		return this.gameReset;
	}

	@Override
	public void setMovesAcceptTradeHandler(HttpHandler handler)
	{
		this.movesAcceptTrade = handler;
	}

	@Override
	public HttpHandler getMovesAcceptTradeHandler()
	{
		return this.movesAcceptTrade;
	}

	@Override
	public void setMovesBuildCityHandler(HttpHandler handler)
	{
		this.movesBuildCity = handler;
	}

	@Override
	public HttpHandler getMovesBuildCityHandler()
	{
		return this.movesBuildCity;
	}

	@Override
	public void setMovesBuildRoadHandler(HttpHandler handler)
	{
		this.movesBuildRoad = handler;
	}

	@Override
	public HttpHandler getMovesBuildRoadHandler()
	{
		return this.movesBuildRoad;
	}

	@Override
	public void setMovesBuildSettlementHandler(HttpHandler handler)
	{
		this.movesBuildSettlement = handler;
	}

	@Override
	public HttpHandler getMovesBuildSettlementHandler()
	{
		return this.movesBuildSettlement;
	}

	@Override
	public void setMovesBuyDevCardHandler(HttpHandler handler)
	{
		this.movesBuyDevCard = handler;
	}

	@Override
	public HttpHandler getMovesBuyDevCardHandler()
	{
		return this.movesBuyDevCard;
	}

	@Override
	public void setMovesDiscardCardsHandler(HttpHandler handler)
	{
		this.movesDiscardCards = handler;
	}

	@Override
	public HttpHandler getMovesDiscardCardsHandler()
	{
		return this.movesDiscardCards;
	}

	@Override
	public void setMovesFinishTurnHandler(HttpHandler handler)
	{
		this.movesFinishTurn = handler;
	}

	@Override
	public HttpHandler getMovesFinishTurnHandler()
	{
		return this.movesFinishTurn;
	}

	@Override
	public void setMovesMaritimeTradeHandler(HttpHandler handler)
	{
		this.movesMaritimeTrade = handler;
	}

	@Override
	public HttpHandler getMovesMaritimeTradeHandler()
	{
		return this.movesMaritimeTrade;
	}

	@Override
	public void setMovesMonopolyHandler(HttpHandler handler)
	{
		this.movesMonopoly = handler;
	}

	@Override
	public HttpHandler getMovesMonopolyHandler()
	{
		return this.movesMonopoly;
	}

	@Override
	public void setMovesMonumentHandler(HttpHandler handler)
	{
		this.movesMonument = handler;
	}

	@Override
	public HttpHandler getMovesMonumentHandler()
	{
		return this.movesMonument;
	}

	@Override
	public void setMovesOfferTradeHandler(HttpHandler handler)
	{
		this.movesOfferTrade = handler;
	}

	@Override
	public HttpHandler getMovesOfferTradeHandler()
	{
		return this.movesOfferTrade;
	}

	@Override
	public void setMovesRoadBuildingHandler(HttpHandler handler)
	{
		this.movesRoadBuilding = handler;
	}

	@Override
	public HttpHandler getMovesRoadBuildingHandler()
	{
		return this.movesRoadBuilding;
	}

	@Override
	public void setMovesRobPlayerHandler(HttpHandler handler)
	{
		this.movesRobPlayer = handler;
	}

	@Override
	public HttpHandler getMovesRobPlayerHandler()
	{
		return this.movesRobPlayer;
	}

	@Override
	public void setMovesRollNumberHandler(HttpHandler handler)
	{
		this.movesRollNumber = handler;
	}

	@Override
	public HttpHandler getMovesRollNumberHandler()
	{
		return this.movesRollNumber;
	}

	@Override
	public void setMovesSendChatHandler(HttpHandler handler)
	{
		this.movesSendChat = handler;
	}

	@Override
	public HttpHandler getMovesSendChatHandler()
	{
		return this.movesSendChat;
	}

	@Override
	public void setMovesSoldierHandler(HttpHandler handler)
	{
		this.movesSoldier = handler;
	}

	@Override
	public HttpHandler getMovesSoldierHandler()
	{
		return this.movesSoldier;
	}

	@Override
	public void setMovesYearOfPlentyHandler(HttpHandler handler)
	{
		this.movesYearOfPlenty = handler;
	}

	@Override
	public HttpHandler getMovesYearOfPlentyHandler()
	{
		return this.movesYearOfPlenty;
	}

	@Override
	public HttpHandler getUtilChangeLogLevel()
	{
		return this.utilChangeLogLevel;
	}

	@Override
	public void setUtilChangeLogLevel(HttpHandler handler)
	{
		this.utilChangeLogLevel = handler;
	}

	@Override
	public void setGameListAI(HttpHandler handler)
	{
		this.gamelistAI = handler;
	}

	@Override
	public HttpHandler getGameListAI()
	{
		return this.gamelistAI;
	}
	
	private static void printAvailablePlugins()
	{
		Path dir = Paths.get("./plugins");
		
		List<Path> result = new ArrayList<>();
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.{jar}"))
		{
			for (Path entry: stream)
			{
				result.add(entry);
			}
		} catch (DirectoryIteratorException | IOException e)
		{
			// I/O error encountered during the iteration, the cause is an IOException
			e.printStackTrace();
		}
		
		System.out.println("available plugins: ");
		for (Path path : result)
		{
			System.out.println("\t"+path.getFileName());
		}
	}
	
	public static final String usage = "usage: server <persisistence plugin> <checkpoint threshold>";
	
	public static void main(String[] args) throws IOException
	{
		Server server = null;
		
		if (args.length == 2)
		{
			try {
				server = new ServerImp("localhost", 8081, args[0], Integer.parseInt(args[1]));
			} catch (InstantiationException
					| IllegalAccessException | ClassNotFoundException | MalformedURLException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException | ParserConfigurationException | SAXException | NoEntryInRegistryForPluginName | GameDoesNotExistException e) {
				e.printStackTrace();
				return;
			}
			server.run();
		}
		else
		{
			System.out.println(usage);
			printAvailablePlugins();
		}
	}



}
