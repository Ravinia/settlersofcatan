package server.serializer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import server.courier.serializer.GameInfo;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ai.AIType;

import java.util.List;

/**
 * Serializer that serializes server data classes into JSON
 */
public interface ServerSerializer
{
    /**
     * Serializes the game model of a game into its JSON format
     * @param model the game model to serialize
     * @return JSON representation
     * @throws PlayerNotInGameException
     * @throws UninitializedDataException 
     * @throws NoCurrentTradesException 
     */
    public JSONObject serializeGameModel(GameModel model) throws PlayerNotInGameException, UninitializedDataException, NoCurrentTradesException;

    /**
     * Serializes the pregame games list
     * @param games the list of pregame games
     * @return JSON representation
     * @throws JSONException 
     * @throws UninitializedDataException 
     */
    public JSONArray serializeGameList(List<GameInfo> games) throws UninitializedDataException, JSONException;

    /**
     * Serializes the list of possible AI types
     * @param types the list of possible AI types
     * @return JSON representation
     */
    public JSONArray serializeAIList(List<AIType> types);
    
    /**
	 * Serializes the Game into json
	 * @param response which is the http response from the server
	 * @return JSON representation
     * @throws UninitializedDataException 
	 */
	public JSONObject serializeGameInfo(GameInfo game) throws JSONException, UninitializedDataException;
}
