package server.serializer;

import org.json.JSONException;
import org.json.JSONObject;

import server.courier.deserializer.*;

/**
 * Server deserializer interface.
 * 
 * @author jameson
 */
public interface ServerJsonDeserializer
{
	/**
	 * Creates and returns a GameStateInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public GameStateInfo deserializeGameState(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a LoginOrRegisterInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public RobPlayerInfo deserializeRobPlayer(JSONObject json)throws JSONException;
	
	/**
	 * Creates and returns a LoginOrRegisterInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public LoginOrRegisterInfo deserializeUserNameAndPassword(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a JoinGameInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public JoinGameInfo deserializeJoinGameInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a PutGameCommandsInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public PutGameCommandsInfo deserializePutGameCommandsInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a AddAIInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public AddAIInfo deserializeAddAIInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a ChangeLogLevelInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public ChangeLogLevelInfo deserializeChangeLogLevelInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a SendChatInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public SendChatInfo deserializeSendChatInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a AcceptTradeInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public AcceptTradeInfo deserializeAcceptTradeInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a DiscardCardsInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public DiscardCardsInfo deserializeDiscardCardsInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a RollNumberInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public RollNumberInfo deserializeRollNumberInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a BuildSettlementInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public BuildRoadInfo deserializeBuildRoadInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a BuildSettlementInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public BuildSettlementInfo deserializeBuildSettlementInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a BuildCityInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public BuildCityInfo deserializeBuildCityInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a OfferTradeInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public OfferTradeInfo deserializeOfferTradeInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a MaritimeTradeInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public MaritimeTradeInfo deserializeMaritimeTradeInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a PlayYearOfPlentyCardInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public PlayYearOfPlentyCardInfo deserializePlayYearOfPlentyInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a FinishTurnInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public FinishTurnInfo deserializeFinishTurnInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a BuyDevCardInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public BuyDevCardInfo deserializeBuyDevCardInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a PlayMonumentCardInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public PlayMonumentCardInfo deserializePlayMonumentCardInfo(JSONObject json) throws JSONException;
	
	/**
	 * Creates and returns a PlayRoadBuildingCardInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public PlayRoadBuildingCardInfo deserializePlayRoadBuildingCardInfo(JSONObject json) throws JSONException;
		
	/**
	 * Creates and returns a PlaySoldierCardInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public PlaySoldierCardInfo deserializePlaySoldierCardInfo(JSONObject json) throws JSONException;

	/**
	 * Creates and returns a PlayMonopolyCardInfo courier object from a JSON object.
	 * @param json The JSON object from which to create the courier.
	 * @return The courier.
	 * @throws JSONException If there is an error with the JSON parsing, for example
	 * if you pass it JSON that doesn't actually fit the desired courier object.
	 */
	public PlayMonopolyCardInfo deserializePlayMonopolyCardInfo(JSONObject json) throws JSONException;

	public LoadGameInfo deserializeLoadGame(JSONObject json) throws JSONException;

	public SaveGameInfo deserializeSaveGame(JSONObject json) throws JSONException;
	
}
