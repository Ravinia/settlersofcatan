package server.serializer;

import shared.player.Cookie;

/**
 * @author Lawrence
 */
public class MockCookie extends Cookie
{
    private int gameID = 1;

    public MockCookie(int gameID)
    {
        this.gameID = gameID;
        gameId = gameID;
    }


    @Override
    public int getPlayerID()
    {
        return 1;
    }

    @Override
    public String getPlayerName()
    {
        return "player name";
    }

    @Override
    public String getPassword()
    {
        return "password";
    }

    @Override
    public int getGameID()
    {
        return gameId;
    }
}
