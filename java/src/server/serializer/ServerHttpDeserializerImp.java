package server.serializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.json.JSONObject;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import shared.exceptions.InvalidHeaderException;
import shared.player.Cookie;

public class ServerHttpDeserializerImp implements ServerHttpDeserializer {
	
	Logger logger;
	
	public ServerHttpDeserializerImp()
	{
		logger = Logger.getLogger("Server Http Deserializer Logger");
	}

	@Override
	public Cookie getCookie(HttpExchange exchange) throws InvalidHeaderException
	{
		Cookie cookie = null;
		Headers headers = exchange.getRequestHeaders();
		if(headers.containsKey("Cookie"))
		{
			cookie = new Cookie();
			logger.info("COOKIE HEADER FOUND");
			List<String> attributes = headers.get("Cookie");

			logger.info("ATTRIBUTES: " + attributes);
			
//			String name = "Cookie";
//			Pattern patternUser = Pattern.compile("(catan.user=.*D)");
//			Pattern patternGame = Pattern.compile("(catan.game=(\\d)+)");
//			Matcher matcherUser = patternUser.matcher(attributes.get(0));
//			Matcher matcherGame = patternGame.matcher(attributes.get(1));
//			
//			String encodedValue = null;
//			StringBuilder buildEncoded = new StringBuilder();
//			
//			while(matcher.find())
//			{
//				buildEncoded.append(matcher.group());
//			}
//			
//			encodedValue = buildEncoded.toString();
//			
//			if(encodedValue == null)
//			{
//				throw new InvalidHeaderException();
//			}
			
			String encodedValue = attributes.toString().replace(", ",";").replace("[","").replace("]","");
			
			cookie.setEncodedValue(encodedValue);
			logger.info("ENCODED: " + cookie.getEncodedValue());
			logger.info("DECODED: " + cookie.getDecodedValue());
			logger.info("USERNAME: " + cookie.getPlayerName());
			logger.info("PASSWORD: " + cookie.getPassword());
			logger.info("GAME ID: " + cookie.getGameID());
			logger.info("PLAYER ID: " + cookie.getPlayerID());
		}
		else
			throw new InvalidHeaderException();
		return cookie;
	}

	@Override
	public String getHttpBody(HttpExchange exchange) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));
		String inputLine;
		while ((inputLine = in.readLine()) != null)
		    sb.append(inputLine);
		in.close();
		  
		  
		return sb.toString();
	}

	@Override
	public JSONObject getJSONFromResponse(HttpExchange exchange) throws IOException
	{
		String body = getHttpBody(exchange);
		
		JSONObject model = new JSONObject(body);
		
		return model;
	}
	
	public int getHttpHeaderResponseCode(HttpResponse response)
    {
        StatusLine line = response.getStatusLine();
        return line.getStatusCode();
    }

	@Override
	public String buildUserCookie(String name, String password, int id) throws UnsupportedEncodingException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("{\"name\":\"");
		sb.append(name);
		sb.append("\",\"password\":\"");
		sb.append(password);
		sb.append("\",\"playerID\":");
		sb.append(id);
		sb.append("}");
		
		String encodedValue = URLEncoder.encode(sb.toString(), "UTF-8");
		StringBuilder cookieValue = new StringBuilder("catan.user=");
		cookieValue.append(encodedValue);
		cookieValue.append(";Path=/;");
		
		return cookieValue.toString();
	}

	@Override
	public String buildGameCookie(int id) throws UnsupportedEncodingException
	{
		String cookieValue = "catan.game=" + id + ";Path=/;";
		return cookieValue;		
	}

}
