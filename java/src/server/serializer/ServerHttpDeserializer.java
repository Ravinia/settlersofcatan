package server.serializer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import shared.exceptions.InvalidHeaderException;
import shared.player.Cookie;

public interface ServerHttpDeserializer {

	/**
	 * Builds a cookie object from an HttpResponse. The cookie must be included in 
	 * the HttpResponse as a header.
	 * @param response The HttpResponse containing the header cookie.
	 * @return The JSON data extracted from the response. This data is information about the user.
	 * (e.g.) {"name":"Sam","password":"sam","playerID":0}
	 * @throws InvalidHeaderException  If there is an error getting the cookie out of the header.
	 */
	public Cookie getCookie(HttpExchange exchange)  throws InvalidHeaderException;
	
	/**
	 * Retrieves the HttpBody of an HttpResponse
	 * @param response The HttpRespone from which the HttpBody will be extracted.
	 * @return The extracted HttpResponse.
	 */
	public String getHttpBody(HttpExchange exchange) throws IOException;
	
	/**
	 * Retrieves the JSON object within the body of an HttpResponse
	 * @param response The HttpRespone from which the HttpBody will be extracted.
	 * @return The extracted HttpResponse.
	 */
	public JSONObject getJSONFromResponse(HttpExchange exchange) throws IOException;
	
	/**
	 * Retrieves the response code from an http response.
	 * @param response The response to retrieve the response code from.
	 * @return The response code retrieved.
	 */
	public int getHttpHeaderResponseCode(HttpResponse response);
	
	public String buildUserCookie(String name, String password, int id) throws UnsupportedEncodingException;

	public String buildGameCookie(int id) throws UnsupportedEncodingException;
}
