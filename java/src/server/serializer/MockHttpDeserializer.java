package server.serializer;

import com.sun.net.httpserver.HttpExchange;
import org.json.JSONException;
import org.json.JSONObject;
import server.facade.mocks.ModelType;
import shared.exceptions.InvalidHeaderException;
import shared.locations.EdgeLocation;
import shared.locations.VertexLocation;
import shared.player.Cookie;

import java.io.IOException;
import static server.facade.mocks.MockMovesFacade.*;
import static server.model.board.LocationConstants.*;

/**
 * @author Lawrence
 */
public class MockHttpDeserializer extends ServerHttpDeserializerImp
{
    private ModelType resultModel = ModelType.DEFAULT;
    private JSONObject json = new JSONObject();

    public void setResultModel(ModelType model)
    {
        resultModel = model;
    }

    @Override
    public Cookie getCookie(HttpExchange exchange) throws InvalidHeaderException
    {
        return new MockCookie(getGameID());
    }

    @Override
    public JSONObject getJSONFromResponse(HttpExchange exchange) throws IOException
    {
        return json;
    }

    //Helper Methods
    private int getGameID()
    {
        return resultModel.getID();
    }

    public void serializeBuildRoadInfo(boolean free) throws JSONException
    {
        int currentPlayer = RED_PLAYER_INDEX;
        EdgeLocation roadLocation = CENTER_N_EDGE;
        json = new JSONObject();
        json.put("type", "buildRoad");
        json.put("playerIndex", new Integer(currentPlayer).toString());

        JSONObject second = new JSONObject();
        second.put("x", new Integer(roadLocation.getHexLoc().getX()).toString());
        second.put("y", new Integer(roadLocation.getHexLoc().getY()).toString());
        second.put("direction", roadLocation.getDir());
        json.put("roadLocation", second);

        json.put("free", new Boolean(free).toString());
    }

    public void serializeBuildSettlementInfo(boolean free) throws JSONException
    {
        int currentPlayer = RED_PLAYER_INDEX;
        VertexLocation location = CENTER_NE_VERTEX;
        json = new JSONObject();
        json.put("type", "buildSettlement");
        json.put("playerIndex", new Integer(currentPlayer).toString());

        JSONObject second = new JSONObject();
        second.put("x", new Integer(location.getHexLoc().getX()).toString());
        second.put("y", new Integer(location.getHexLoc().getY()).toString());
        second.put("direction", location.getDir());
        json.put("vertexLocation", second);

        json.put("free", new Boolean(free).toString());
    }
    public void serializeJoinGameInfo()throws JSONException
    {
    	String currentGame = "0";
    	String color = "blue";
    	
    	json = new JSONObject();
    	json.put("id", currentGame);
    	json.put("color", color);
    }
}
