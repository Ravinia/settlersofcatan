package server.serializer;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import server.courier.deserializer.*;
import server.model.game.resources.ResourceHand;
import server.model.game.resources.ResourceHandImp;
import shared.debug.LogLevel;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;

public class ServerJsonDeserializerImp implements ServerJsonDeserializer
{

	@Override
	public GameStateInfo deserializeGameState(JSONObject json)
			throws JSONException
	{
		boolean randomTiles = json.getBoolean("randomTiles");
		boolean randomNumbers = json.getBoolean("randomNumbers");
		boolean randomPorts = json.getBoolean("randomPorts");
		String name = json.getString("name");
		return new GameStateInfo(randomTiles, randomNumbers, randomPorts, name);
	}
	
	@Override
	public LoginOrRegisterInfo deserializeUserNameAndPassword(JSONObject json)
			throws JSONException
	{
		String username = json.getString("username");
		String password = json.getString("password");
		return new LoginOrRegisterInfo(username, password);
	}

	@Override
	public JoinGameInfo deserializeJoinGameInfo(JSONObject json)
			throws JSONException
	{
		int id = json.getInt("id");
		String colorString = json.getString("color");
		CatanColor color = CatanColor.getCatanColor(colorString);
		return new JoinGameInfo(id, color);
	}

	@Override
	public PutGameCommandsInfo deserializePutGameCommandsInfo(JSONObject json)
			throws JSONException
	{
		assert false; // We aren't supposed to implement "put commands"
		return null;
	}

	@Override
	public AddAIInfo deserializeAddAIInfo(JSONObject json) throws JSONException
	{
		String AIName = json.getString("AIType");
		return new AddAIInfo(AIName);
	}

	@Override
	public ChangeLogLevelInfo deserializeChangeLogLevelInfo(JSONObject json)
			throws JSONException
	{
		String level = json.getString("logLevel");
		LogLevel logLevel = LogLevel.getLogLevel(level);
		return new ChangeLogLevelInfo(logLevel);
	}

	@Override
	public SendChatInfo deserializeSendChatInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		String content = json.getString("content");
		return new SendChatInfo(playerIndex, content);
	}

	@Override
	public AcceptTradeInfo deserializeAcceptTradeInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		boolean willAccept = json.getBoolean("willAccept");
		return new AcceptTradeInfo(playerIndex, willAccept);
	}

	@Override
	public DiscardCardsInfo deserializeDiscardCardsInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		JSONObject discardedCards = json.getJSONObject("discardedCards");
		int brick = discardedCards.getInt("brick");
		int ore = discardedCards.getInt("ore");
		int sheep = discardedCards.getInt("sheep");
		int wheat = discardedCards.getInt("wheat");
		int wood = discardedCards.getInt("wood");
		ResourceHand resourceHand = new ResourceHandImp(sheep, brick, wood, ore, wheat);
		
		return new DiscardCardsInfo(playerIndex, resourceHand);
	}

	@Override
	public RollNumberInfo deserializeRollNumberInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		int number = json.getInt("number");
		return new RollNumberInfo(playerIndex, number);
	}

	@Override
	public BuildRoadInfo deserializeBuildRoadInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		JSONObject roadLocation = json.getJSONObject("roadLocation");
		
		int x = roadLocation.getInt("x");
		int y = roadLocation.getInt("y");
		HexLocation hexLocation = new HexLocation(x,y);
		
		String direction = roadLocation.get("direction").toString();
		
		EdgeDirection edgeDirection = EdgeDirection.getEdgeDirection(direction);
		EdgeLocation edgeLocation = new EdgeLocation(hexLocation, edgeDirection);
		
		boolean free = json.getBoolean("free");
		
		return new BuildRoadInfo(playerIndex, free, edgeLocation);
	}

	@Override
	public BuildSettlementInfo deserializeBuildSettlementInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		JSONObject vertexLocation = json.getJSONObject("vertexLocation");
		
		int x = vertexLocation.getInt("x");
		int y = vertexLocation.getInt("y");
		HexLocation hexLocation = new HexLocation(x,y);
		
		String direction = vertexLocation.get("direction").toString();
		
		VertexDirection dir = VertexDirection.getVertexDirection(direction);
		VertexLocation loc = new VertexLocation(hexLocation, dir);
		
		boolean free = json.getBoolean("free");
		
		return new BuildSettlementInfo(playerIndex, loc, free);
	}

	@Override
	public BuildCityInfo deserializeBuildCityInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		JSONObject vertexLocation = json.getJSONObject("vertexLocation");
		
		int x = vertexLocation.getInt("x");
		int y = vertexLocation.getInt("y");
		HexLocation hexLocation = new HexLocation(x,y);
		
		String direction = vertexLocation.getString("direction");
		
		VertexDirection dir = VertexDirection.getVertexDirection(direction);
		VertexLocation loc = new VertexLocation(hexLocation, dir);
		
		return new BuildCityInfo(playerIndex, loc);
	}

	@Override
	public OfferTradeInfo deserializeOfferTradeInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		JSONObject offer = json.getJSONObject("offer");
		int brick = offer.getInt("brick");
		int ore = offer.getInt("ore");
		int sheep = offer.getInt("sheep");
		int wheat = offer.getInt("wheat");
		int wood = offer.getInt("wood");
		
		Map<ResourceType, Integer> tradeOffer = new HashMap<ResourceType, Integer>();
		tradeOffer.put(ResourceType.BRICK, brick);
		tradeOffer.put(ResourceType.ORE, ore);
		tradeOffer.put(ResourceType.SHEEP, sheep);
		tradeOffer.put(ResourceType.WHEAT, wheat);
		tradeOffer.put(ResourceType.WOOD, wood);
		
		int receiver = json.getInt("receiver");
		
		return new OfferTradeInfo(playerIndex, tradeOffer, receiver);
	}

	@Override
	public MaritimeTradeInfo deserializeMaritimeTradeInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		int ratio = json.getInt("ratio");
		
		String input = json.getString("inputResource");
		ResourceType inputResource = ResourceType.getResource(input);
		
		String output = json.getString("outputResource");
		ResourceType outputResource = ResourceType.getResource(output);
		
		return new MaritimeTradeInfo(playerIndex, ratio, inputResource, outputResource);
	}

	@Override
	public PlayYearOfPlentyCardInfo deserializePlayYearOfPlentyInfo(
			JSONObject json) throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		String one = json.getString("resource1");
		ResourceType resource1 = ResourceType.getResource(one);
		
		String two = json.getString("resource2");
		ResourceType resource2 = ResourceType.getResource(two);
		
		return new PlayYearOfPlentyCardInfo(playerIndex, resource1, resource2);
	}

	@Override
	public FinishTurnInfo deserializeFinishTurnInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		return new FinishTurnInfo(playerIndex);
	}

	@Override
	public BuyDevCardInfo deserializeBuyDevCardInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		return new BuyDevCardInfo(playerIndex);
	}

	@Override
	public PlayMonumentCardInfo deserializePlayMonumentCardInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		return new PlayMonumentCardInfo(playerIndex);
	}

	@Override
	public PlayRoadBuildingCardInfo deserializePlayRoadBuildingCardInfo(
			JSONObject json) throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		
		JSONObject spot1 = json.getJSONObject("spot1");
		int x1 = spot1.getInt("x");
		int y1 = spot1.getInt("y");
		HexLocation hexLocation1 = new HexLocation(x1,y1);
		String direction1 = spot1.getString("direction");
		EdgeDirection edgeDirection1 = EdgeDirection.getEdgeDirection(direction1);
		EdgeLocation edgeLocation1 = new EdgeLocation(hexLocation1, edgeDirection1);
		
		
		JSONObject spot2 = json.getJSONObject("spot2");
		int x2 = spot2.getInt("x");
		int y2 = spot2.getInt("y");
		HexLocation hexLocation2 = new HexLocation(x2,y2);
		String direction2 = spot2.getString("direction");
		EdgeDirection edgeDirection2 = EdgeDirection.getEdgeDirection(direction2);
		EdgeLocation edgeLocation2 = new EdgeLocation(hexLocation2, edgeDirection2);
		
		return new PlayRoadBuildingCardInfo(playerIndex, edgeLocation1, edgeLocation2);
	}

	@Override
	public PlaySoldierCardInfo deserializePlaySoldierCardInfo(JSONObject json)
			throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		int victimIndex = json.getInt("victimIndex");
		
		JSONObject spot = json.getJSONObject("location");
		
		int x = spot.getInt("x");
		int y = spot.getInt("y");
		HexLocation hexLocation = new HexLocation(x,y);
		
		return new PlaySoldierCardInfo(playerIndex, hexLocation, victimIndex);
	}

	@Override
	public PlayMonopolyCardInfo deserializePlayMonopolyCardInfo(JSONObject json)
			throws JSONException
	{
		String resource = json.getString("resource");
		int playerIndex = json.getInt("playerIndex");
		ResourceType resourceType = ResourceType.getResource(resource);
		
		return new PlayMonopolyCardInfo(playerIndex, resourceType);
	}
	
	@Override
	public RobPlayerInfo deserializeRobPlayer(JSONObject json) throws JSONException
	{
		int playerIndex = json.getInt("playerIndex");
		int victimIndex = json.getInt("victimIndex");
		
		JSONObject spot = json.getJSONObject("location");
		
		int x = spot.getInt("x");
		int y = spot.getInt("y");
		HexLocation hexLocation = new HexLocation(x,y);
		
		return new RobPlayerInfo(playerIndex, victimIndex, hexLocation);
	}
	
	@Override
	public LoadGameInfo deserializeLoadGame(JSONObject json) throws JSONException
	{
		String gameName = json.getString("name");
		
		return new LoadGameInfo(gameName);
	}
	
	@Override
	public SaveGameInfo deserializeSaveGame(JSONObject json) throws JSONException
	{
		int gameId = json.getInt("id");
		String gameName = json.getString("name");
		
		return new SaveGameInfo(gameId, gameName);
	}
}
