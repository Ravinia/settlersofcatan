package server.serializer;

import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import server.courier.serializer.BankInfo;
import server.courier.serializer.DeckInfo;
import server.courier.serializer.DevCardHandInfo;
import server.courier.serializer.DevCardListInfo;
import server.courier.serializer.GameInfo;
import server.courier.serializer.GameModelInfo;
import server.courier.serializer.HexInfo;
import server.courier.serializer.MapInfo;
import server.courier.serializer.MessageInfo;
import server.courier.serializer.PlayerInfo;
import server.courier.serializer.PortInfo;
import server.courier.serializer.PregamePlayerInfo;
import server.courier.serializer.RoadInfo;
import server.courier.serializer.SettlementInfo;
import server.courier.serializer.TradeOfferInfo;
import server.courier.serializer.TurnTrackerInfo;
import server.exceptions.DesertException;
import server.exceptions.NoCurrentTradesException;
import server.exceptions.PlayerNotInGameException;
import server.exceptions.UninitializedDataException;
import server.model.GameModel;
import server.model.ai.AIType;
import server.model.game.chatlog.Message;
import server.model.game.resources.ResourceHand;
import server.model.game.resources.ResourceHandImp;
import server.model.player.DefaultPlayer;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;

public class ServerSerializerImp implements ServerSerializer {

	@Override
	public JSONObject serializeGameModel(GameModel model)
			throws PlayerNotInGameException, UninitializedDataException,
			NoCurrentTradesException {
		GameModelInfo gmi = model.toCourier();

		JSONObject jsonModel = new JSONObject();

		try {
			jsonModel.put("tradeOffer",
					serializeTradeOffer(gmi.getTradeOffer()));
		} catch (NoCurrentTradesException e) {
			// no trade, so no trade JSON
		}

		jsonModel.put("winner", gmi.getWinnerIndex());
		jsonModel.put("version", gmi.getVersion());
		jsonModel.put("players", serializePlayers(gmi.getPlayers()));
		jsonModel.put("deck", serializeDeck(gmi.getDeck()));
		jsonModel.put("map", serializeMap(gmi.getMap()));
		jsonModel.put("log", serializeMessages(gmi.getLogs()));
		jsonModel.put("chat", serializeMessages(gmi.getChats()));

		BankInfo bi = gmi.getBank();
		ResourceHand rh;
		if (bi != null)
			rh = new ResourceHandImp(bi.getSheep(), bi.getBrick(),
					bi.getWood(), bi.getOre(), bi.getWheat());
		else
			rh = new ResourceHandImp(0, 0, 0, 0, 0);
		jsonModel.put("bank", serializeResourceHand(rh));

		jsonModel
				.put("turnTracker", serializeTurnTracker(gmi.getTurnTracker()));

		return jsonModel;
	}

	private JSONObject serializeTradeOffer(TradeOfferInfo tradeOfferInfo) {
		JSONObject jsonTradeOffer = new JSONObject();
		jsonTradeOffer.put("sender", tradeOfferInfo.getSenderIndex());
		jsonTradeOffer.put("receiver", tradeOfferInfo.getReceiverIndex());

		JSONObject jsonOffer = serializeResourceHand(tradeOfferInfo.getOffer());
		jsonTradeOffer.put("offer", jsonOffer);

		return jsonTradeOffer;
	}

	private JSONObject serializeDeck(DeckInfo deck) {
		JSONObject jsonDeck = new JSONObject();
		if (deck != null) {
			jsonDeck.put("yearOfPlenty", deck.getYearOfPlenty());
			jsonDeck.put("monopoly", deck.getMonopoly());
			jsonDeck.put("soldier", deck.getSoldier());
			jsonDeck.put("roadBuilding", deck.getRoadBuilding());
			jsonDeck.put("monument", deck.getMonument());
		} else {
			jsonDeck.put("yearOfPlenty", 0);
			jsonDeck.put("monopoly", 0);
			jsonDeck.put("soldier", 0);
			jsonDeck.put("roadBuilding", 0);
			jsonDeck.put("monument", 0);
		}

		return jsonDeck;
	}

	private JSONObject serializeMap(MapInfo map) {
		JSONObject jsonMap = new JSONObject();
		if (map != null) {
			jsonMap.put("radius", map.getRadius());
			jsonMap.put("hexes", serializeHexes(map.getHexes()));
			jsonMap.put("ports", serializePorts(map.getPorts()));
			jsonMap.put("roads", serializeRoads(map.getRoads()));
			jsonMap.put("cities", serializeSettlements(map.getCities()));
			jsonMap.put("settlements",
					serializeSettlements(map.getSettlements()));
			jsonMap.put("robber", serializeHexLocation(map.getRobberLocation()));
		} else {
			String defaultMap = "{\"hexes\":[{\"location\":{\"x\":0,\"y\":-2}},{\"resource\":\"brick\",\"location\":{\"x\":1,\"y\":-2},\"number\":4},{\"resource\":\"wood\",\"location\":{\"x\":2,\"y\":-2},\"number\":11},{\"resource\":\"brick\",\"location\":{\"x\":-1,\"y\":-1},\"number\":8},{\"resource\":\"wood\",\"location\":{\"x\":0,\"y\":-1},\"number\":3},{\"resource\":\"ore\",\"location\":{\"x\":1,\"y\":-1},\"number\":9},{\"resource\":\"sheep\",\"location\":{\"x\":2,\"y\":-1},\"number\":12},{\"resource\":\"ore\",\"location\":{\"x\":-2,\"y\":0},\"number\":5},{\"resource\":\"sheep\",\"location\":{\"x\":-1,\"y\":0},\"number\":10},{\"resource\":\"wheat\",\"location\":{\"x\":0,\"y\":0},\"number\":11},{\"resource\":\"brick\",\"location\":{\"x\":1,\"y\":0},\"number\":5},{\"resource\":\"wheat\",\"location\":{\"x\":2,\"y\":0},\"number\":6},{\"resource\":\"wheat\",\"location\":{\"x\":-2,\"y\":1},\"number\":2},{\"resource\":\"sheep\",\"location\":{\"x\":-1,\"y\":1},\"number\":9},{\"resource\":\"wood\",\"location\":{\"x\":0,\"y\":1},\"number\":4},{\"resource\":\"sheep\",\"location\":{\"x\":1,\"y\":1},\"number\":10},{\"resource\":\"wood\",\"location\":{\"x\":-2,\"y\":2},\"number\":6},{\"resource\":\"ore\",\"location\":{\"x\":-1,\"y\":2},\"number\":3},{\"resource\":\"wheat\",\"location\":{\"x\":0,\"y\":2},\"number\":8}],\"roads\":[],\"cities\":[],\"settlements\":[],\"radius\":3,\"ports\":[{\"ratio\":2,\"resource\":\"ore\",\"direction\":\"S\",\"location\":{\"x\":1,\"y\":-3}},{\"ratio\":3,\"direction\":\"NW\",\"location\":{\"x\":2,\"y\":1}},{\"ratio\":2,\"resource\":\"wheat\",\"direction\":\"S\",\"location\":{\"x\":-1,\"y\":-2}},{\"ratio\":2,\"resource\":\"wood\",\"direction\":\"NE\",\"location\":{\"x\":-3,\"y\":2}},{\"ratio\":2,\"resource\":\"brick\",\"direction\":\"NE\",\"location\":{\"x\":-2,\"y\":3}},{\"ratio\":3,\"direction\":\"SE\",\"location\":{\"x\":-3,\"y\":0}},{\"ratio\":3,\"direction\":\"SW\",\"location\":{\"x\":3,\"y\":-3}},{\"ratio\":3,\"direction\":\"N\",\"location\":{\"x\":0,\"y\":3}},{\"ratio\":2,\"resource\":\"sheep\",\"direction\":\"NW\",\"location\":{\"x\":3,\"y\":-1}}],\"robber\":{\"x\":0,\"y\":-2}}";
			jsonMap = new JSONObject(defaultMap); 
		}

		return jsonMap;
	}

	private JSONObject serializeTurnTracker(TurnTrackerInfo turnTracker) {
		JSONObject jsonTurnTracker = new JSONObject();
		if (turnTracker != null) {
			jsonTurnTracker.put("currentTurn",
					turnTracker.getCurrentPlayerIndex());
			jsonTurnTracker.put("status", turnTracker.getCurrentPhase()
					.getValue());
			jsonTurnTracker.put("largestArmy",
					turnTracker.getLargestArmyIndex());
			jsonTurnTracker.put("longestRoad",
					turnTracker.getLongestRoadIndex());
		} else {
			jsonTurnTracker.put("currentTurn", 0);
			jsonTurnTracker.put("status", "firstround");
			jsonTurnTracker.put("largestArmy", 0);
			jsonTurnTracker.put("longestRoad", 0);
		}

		return jsonTurnTracker;
	}

	private JSONObject serializeResourceHand(ResourceHand resourceHand) {
		JSONObject jsonResourceHand = new JSONObject();
		if (resourceHand != null) {
			jsonResourceHand.put("brick", resourceHand.getBrick());
			jsonResourceHand.put("ore", resourceHand.getOre());
			jsonResourceHand.put("sheep", resourceHand.getSheep());
			jsonResourceHand.put("wood", resourceHand.getWood());
			jsonResourceHand.put("wheat", resourceHand.getWheat());
		} else {
			jsonResourceHand.put("brick", 0);
			jsonResourceHand.put("ore", 0);
			jsonResourceHand.put("sheep", 0);
			jsonResourceHand.put("wood", 0);
			jsonResourceHand.put("wheat", 0);
		}

		return jsonResourceHand;
	}

	private JSONArray serializePlayers(List<PlayerInfo> players) {
		JSONArray jsonPlayers = new JSONArray();
		if (players == null)
			return jsonPlayers;

		for (int i = 0; i < 4; i++) {
			if (i >= players.size()) {
				jsonPlayers.put(JSONObject.NULL);
				continue;
			}
			PlayerInfo player = players.get(i);
			if (player == DefaultPlayer.DEFAULT_PLAYER)
				continue;
			jsonPlayers.put(serializePlayer(player));
		}
		return jsonPlayers;
	}

	private JSONObject serializePlayer(PlayerInfo player) {
		JSONObject jsonPlayer = new JSONObject();
		if (player != null) {
			jsonPlayer.put("resources",
					serializeResourceHand(player.getResourceHand()));

			DevCardHandInfo devCardHand = player.getDevCardHand();
			if (devCardHand != null) {
				DevCardListInfo newCards = devCardHand.getNewDevCards();
				jsonPlayer.put("newDevCards", serializeDevCards(newCards));
				DevCardListInfo oldCards = devCardHand.getOldDevCards();
				jsonPlayer.put("oldDevCards", serializeDevCards(oldCards));
			} else {
				jsonPlayer.put("newDevCards", serializeDevCards(null));
				jsonPlayer.put("oldDevCards", serializeDevCards(null));
			}

			jsonPlayer.put("roads", player.getNumRemainingRoads());
			jsonPlayer.put("cities", player.getNumRemainingCities());
			jsonPlayer.put("settlements", player.getNumRemainingSettlements());

			jsonPlayer.put("soldiers", player.getNumSoldiersPlayed());
			jsonPlayer.put("victoryPoints", player.getVictoryPoints());
			jsonPlayer.put("monuments", player.getNumMonumentsPlayed());
			jsonPlayer.put("playedDevCard", player.hasPlayedDevCardThisTurn());
			jsonPlayer.put("discarded", player.hasDiscarded());
			jsonPlayer.put("playerID", player.getId());
			jsonPlayer.put("playerIndex", player.getIndex());
			jsonPlayer.put("name", player.getName());
			jsonPlayer.put("color", player.getColor());
		} else {
			jsonPlayer.put("resources", serializeResourceHand(null));

			jsonPlayer.put("newDevCards", serializeDevCards(null));
			jsonPlayer.put("oldDevCards", serializeDevCards(null));

			jsonPlayer.put("roads", 0);
			jsonPlayer.put("cities", 0);
			jsonPlayer.put("settlements", 0);

			jsonPlayer.put("soldiers", 0);
			jsonPlayer.put("victoryPoints", 0);
			jsonPlayer.put("monuments", 0);
			jsonPlayer.put("playedDevCard", false);
			jsonPlayer.put("discarded", false);
			jsonPlayer.put("playerID", 0);
			jsonPlayer.put("playerIndex", 0);
			jsonPlayer.put("name", "");
			jsonPlayer.put("color", "");
		}
		return jsonPlayer;
	}

	public JSONArray serializeHexes(List<HexInfo> hexes) {
		JSONArray jsonHexes = new JSONArray();
		if (hexes == null)
			return jsonHexes;

		for (int i = 0; i < hexes.size(); i++) {
			HexInfo hex = hexes.get(i);
			JSONObject jsonHex = new JSONObject();

			try {
				jsonHex.put("resource", hex.getResource());
			} catch (DesertException | JSONException e) {
				// this means it's the desert hex, and resource is omitted in
				// the JSON
			}

			HexLocation location = hex.getLocation();
			jsonHex.put("location", serializeHexLocation(location));

			try {
				jsonHex.put("number", hex.getNumber());
			} catch (DesertException | JSONException e) {
				// this means it's the desert hex, and number is omitted in the
				// JSON
			}

			jsonHexes.put(jsonHex);
		}

		return jsonHexes;
	}

	public JSONArray serializePorts(List<PortInfo> ports) {
		JSONArray jsonPorts = new JSONArray();
		if (ports == null)
			return jsonPorts;

		for (int i = 0; i < ports.size(); i++) {
			PortInfo port = ports.get(i);
			JSONObject jsonPort = new JSONObject();

			jsonPort.put("resource", port.getResourceType());

			jsonPort.put("ratio", port.getRatio());
			jsonPort.put("direction", port.getDirection());

			HexLocation location = port.getLocation();
			jsonPort.put("location", serializeHexLocation(location));

			jsonPorts.put(jsonPort);
		}

		return jsonPorts;
	}

	public JSONArray serializeRoads(List<RoadInfo> roads) {
		JSONArray jsonRoads = new JSONArray();
		if (roads == null)
			return jsonRoads;

		for (int i = 0; i < roads.size(); i++) {
			RoadInfo road = roads.get(i);
			JSONObject jsonRoad = new JSONObject();

			jsonRoad.put("owner", road.getOwnerIndex());

			EdgeLocation location = road.getLocation();
			HexLocation hexLocation = location.getHexLoc();
			JSONObject jsonLocation = serializeHexLocation(hexLocation);
			jsonLocation.put("direction", location.getDir());
			jsonRoad.put("location", jsonLocation);

			jsonRoads.put(jsonRoad);
		}

		return jsonRoads;
	}

	/**
	 * This can be used for both cities and settlements
	 */
	public JSONArray serializeSettlements(List<SettlementInfo> settlements) {
		JSONArray jsonSettlements = new JSONArray();
		if (settlements == null)
			return jsonSettlements;

		for (int i = 0; i < settlements.size(); i++) {
			SettlementInfo settlement = settlements.get(i);
			JSONObject jsonSettlement = new JSONObject();

			jsonSettlement.put("owner", settlement.getOwnerIndex());

			VertexLocation location = settlement.getLocation();
			HexLocation hexLocation = location.getHexLoc();
			JSONObject jsonLocation = serializeHexLocation(hexLocation);
			jsonLocation.put("direction", location.getDir());
			jsonSettlement.put("location", jsonLocation);

			jsonSettlements.put(jsonSettlement);
		}

		return jsonSettlements;
	}

	public JSONObject serializeHexLocation(HexLocation hexLocation) {
		JSONObject jsonLocation = new JSONObject();
		if (hexLocation != null) {
			jsonLocation.put("x", hexLocation.getX());
			jsonLocation.put("y", hexLocation.getY());
		} else {
			jsonLocation.put("x", 0);
			jsonLocation.put("y", 0);
		}

		return jsonLocation;
	}

	public JSONObject serializeMessages(MessageInfo info) {
		JSONObject jsonLines = new JSONObject();
		JSONArray jsonLogMessages = new JSONArray();

		if (info != null) {
			Iterator<Message> iterator = info.iterator();
			while (iterator.hasNext()) {
				Message m = iterator.next();

				JSONObject jsonLogMessage = new JSONObject();
				jsonLogMessage.put("source", m.getSource());
				jsonLogMessage.put("message", m.getMessage());

				jsonLogMessages.put(jsonLogMessage);
			}
		}

		jsonLines.put("lines", jsonLogMessages);

		return jsonLines;
	}

	public JSONObject serializeDevCards(DevCardListInfo info) {
		JSONObject jsonCards = new JSONObject();
		if (info != null) {
			jsonCards.put("monopoly", info.getMonopoly());
			jsonCards.put("monument", info.getMonument());
			jsonCards.put("roadBuilding", info.getRoadBuilding());
			jsonCards.put("soldier", info.getSoldier());
			jsonCards.put("yearOfPlenty", info.getYearOfPlenty());
		} else {
			jsonCards.put("monopoly", 0);
			jsonCards.put("monument", 0);
			jsonCards.put("roadBuilding", 0);
			jsonCards.put("soldier", 0);
			jsonCards.put("yearOfPlenty", 0);
		}

		return jsonCards;
	}

	@Override
	public JSONArray serializeGameList(List<GameInfo> games)
			throws UninitializedDataException, JSONException {
		JSONArray jsonGamesList = new JSONArray();
		if (games == null)
			return jsonGamesList;

		for (GameInfo game : games) {
			jsonGamesList.put(serializeGameInfo(game));
		}

		return jsonGamesList;
	}

	@Override
	public JSONArray serializeAIList(List<AIType> types) {
		JSONArray jsonAIList = new JSONArray();
		if (types == null)
			return jsonAIList;

		for (int i = 0; i < types.size(); i++) {
			AIType type = types.get(i);
			jsonAIList.put(i, type.getAIType());
		}

		return jsonAIList;
	}

	@Override
	public JSONObject serializeGameInfo(GameInfo game) throws JSONException,
			UninitializedDataException {
		JSONObject jsonGame = new JSONObject();
		if (game != null) {
			jsonGame.put("title", game.getGameName());
			jsonGame.put("id", game.getGameID());

			JSONArray jsonPlayers = new JSONArray();
			for (PregamePlayerInfo player : game.getPlayers()) {
				if (player.equals(DefaultPlayer.DEFAULT_PLAYER))
					continue;

				JSONObject jsonPlayer = new JSONObject();
				jsonPlayer.put("color", player.getColor());
				jsonPlayer.put("name", player.getName());
				jsonPlayer.put("id", player.getId());

				jsonPlayers.put(jsonPlayer);
			}
			jsonGame.put("players", jsonPlayers);
		} else {
			jsonGame.put("title", "");
			jsonGame.put("id", 0);

			JSONArray jsonPlayers = new JSONArray();
			jsonGame.put("players", jsonPlayers);
		}

		return jsonGame;
	}

}
